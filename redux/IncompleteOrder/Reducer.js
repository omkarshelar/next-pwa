import { LoginActions } from "../Login/Action";
import { incompleteOrderActions } from "./Action";

const initState = {
  incompleteOrderData: null,
  incompleteOrderError: null,
};

export const incompleteOrderReducer = (
  state = initState,
  { type, payload }
) => {
  switch (type) {
    case incompleteOrderActions.INCOMPLETE_ORDER_SUCCESS:
      return {
        ...state,
        incompleteOrderData: payload,
        incompleteOrderError: null,
      };

    case incompleteOrderActions.INCOMPLETE_ORDER_FAILURE:
      return {
        ...state,
        incompleteOrderData: null,
        incompleteOrderError: payload,
      };
    case LoginActions.USER_LOGOUT:
      return {
        ...initState,
      };

    default:
      return state;
  }
};

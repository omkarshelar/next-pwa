import { OrderManagementService_URL } from "../../constants/Urls";
import { makeApiCall } from "../makeApiCall";

export const incompleteOrderActions = {
  INCOMPLETE_ORDER_SUCCESS: "INCOMPLETE_ORDER_SUCCESS",
  INCOMPLETE_ORDER_FAILURE: "INCOMPLETE_ORDER_FAILURE",
};

// Fetch Address Details

export function incompleteOrderSuccess(props) {
  return (dispatch) => {
    dispatch({
      type: incompleteOrderActions.INCOMPLETE_ORDER_SUCCESS,
      payload: props,
    });
  };
}

export function incompleteOrderError(error) {
  return {
    type: incompleteOrderActions.INCOMPLETE_ORDER_FAILURE,
    payload: error,
  };
}

export function incompleteOrderThunk(props) {
  const reqHeader = {
    "Content-Type": "application/json",
    Authorization: "Bearer " + props.accessToken,
  };
  const getUrl = `${OrderManagementService_URL}/replaceMedForIncompleteOrder?orderId=${props.orderIds}&offerId=${props.offerId}&pincode=${props.pincode}`;
  return makeApiCall(
    getUrl,
    incompleteOrderSuccess,
    incompleteOrderError,
    "POST",
    props.medSet,
    reqHeader,
    props.history
  );
}

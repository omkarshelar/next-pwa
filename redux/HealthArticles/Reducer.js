import { articleActions } from "./Action";

const initState = {
  articleList: null,
  articleError: null,
  articleDetailSuccess: null,
  articleDetailError: null,
};

export const articleReducer = (state = initState, { type, payload }) => {
  switch (type) {
    case articleActions.FETCH_ARTICLE_SUCCESS:
      return {
        ...state,
        articleList: payload.result,
        articleError: null,
      };

    case articleActions.FETCH_ARTICLE_ERROR:
      return {
        ...state,
        articleList: null,
        articleError: payload,
      };
    case articleActions.ARTICLE_DETAIL_SUCCESS:
      return {
        ...state,
        articleDetailSuccess: payload,
        articleDetailError: null,
      };

    case articleActions.ARTICLE_DETAIL_ERROR:
      return {
        ...state,
        articleDetailSuccess: null,
        articleDetailError: payload,
      };
    default:
      return state;
  }
};

import { Article_Service_URL } from "../../constants/Urls";
import { makeApiCall } from "../makeApiCall";

export const articleActions = {
  FETCH_ARTICLE_SUCCESS: "FETCH_ARTICLE_SUCCESS",
  FETCH_ARTICLE_ERROR: "FETCH_ARTICLE_ERROR",
  ARTICLE_DETAIL_SUCCESS: "ARTICLE_DETAIL_SUCCESS",
  ARTICLE_DETAIL_ERROR: "ARTICLE_DETAIL_ERROR",
};

// Fetch Articles.
export function fetchArticleSuccess(props) {
  return (dispatch) => {
    dispatch({
      type: articleActions.FETCH_ARTICLE_SUCCESS,
      payload: props,
    });
  };
}

export function fetchArticleError(error) {
  return {
    type: articleActions.FETCH_ARTICLE_ERROR,
    payload: error,
  };
}

export function fetchArticleThunk(history, loaderType, access_token) {
  const reqHeader = {
    transactionId: "a",
    "Content-Type": "application/json",
  };
  const reqHeaderWithToken = {
    transactionId: "a",
    "Content-Type": "application/json",
    Authorization: "Bearer " + access_token,
  };
  const getUrl = `${Article_Service_URL}/getArticleListing`;
  return makeApiCall(
    getUrl,
    fetchArticleSuccess,
    fetchArticleError,
    "POST",
    null,
    access_token ? reqHeaderWithToken : reqHeader,
    history,
    loaderType
  );
}

//Fetch specific articles details

export function articleDetailSuccess(props) {
  return (dispatch) => {
    dispatch({
      type: articleActions.ARTICLE_DETAIL_SUCCESS,
      payload: props,
    });
  };
}

export function articleDetailError(error) {
  return {
    type: articleActions.ARTICLE_DETAIL_ERROR,
    payload: error,
  };
}

export function articleDetailThunk(props) {
  const reqHeader = {
    transactionId: "a",
    "Content-Type": "application/json",
  };
  const getUrl = `${Article_Service_URL}/getArticleDetails?id=${props.id}`;
  return makeApiCall(
    getUrl,
    articleDetailSuccess,
    articleDetailError,
    "POST",
    null,
    reqHeader,
    props.history
  );
}

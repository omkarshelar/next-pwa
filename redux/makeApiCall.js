import axios from "axios";
import HashMap from "hashmap";
import {
  startLoading,
  stopLoading,
  startLoading1,
  stopLoading1,
  startLoading2,
  stopLoading2,
} from "./Loader/Action";
import Router from "next/router";
let map = new HashMap();

export function makeApiCall(
  apiUrl,
  successCall,
  failureCall,
  methodName,
  reqBody,
  reqHeader,
  history,
  type
) {
  let axiosBody = null;
  if (methodName === "GET") {
    axiosBody = {
      method: methodName,
      url: apiUrl,
      headers: reqHeader,
    };
  } else if (methodName === "POST") {
    axiosBody = {
      method: methodName,
      url: apiUrl,
      data: reqBody,
      headers: reqHeader,
    };
  }
  return async (dispatch) => {
    switch (type) {
      case 1:
      case "1":
        dispatch(startLoading1());
        break;
      case 2:
      case "2":
        dispatch(startLoading2());
        break;
      default:
        dispatch(startLoading());
        break;
    }

    await axios(axiosBody)
      .then((response) => {
        if (response.status >= 200 && response.status < 300) {
          dispatch(successCall(response.data));

          // dispatch(stopLoading());
          switch (type) {
            case 1:
            case "1":
              dispatch(stopLoading1());
              break;
            case 2:
            case "2":
              dispatch(stopLoading2());
              break;
            default:
              dispatch(stopLoading());
              break;
          }
        } else if (response.status === 401) {
          switch (type) {
            case 1:
            case "1":
              dispatch(stopLoading1());
              break;
            case 2:
            case "2":
              dispatch(stopLoading2());
              break;
            default:
              dispatch(stopLoading());
              break;
          }
          // dispatch(stopLoading());
        } else {
          switch (type) {
            case 1:
            case "1":
              dispatch(stopLoading1());
              break;
            case 2:
            case "2":
              dispatch(stopLoading2());
              break;
            default:
              dispatch(stopLoading());
              break;
          }
          // dispatch(stopLoading());
          const error = new Error(response.statusText);
          dispatch(failureCall(error));
          throw error;
        }
      })
      .catch((err) => {
        switch (type) {
          case 1:
          case "1":
            dispatch(stopLoading1());
            break;
          case 2:
          case "2":
            dispatch(stopLoading2());
            break;
          default:
            dispatch(stopLoading());
            break;
        }

        if (err.response) {
          switch (err.response.status) {
            case 400:
              err = handleError(err);
              dispatch(failureCall(err));
              break;
            case 404:
              err = handleError(err);
              dispatch(failureCall(err));
              break;
            case 500:
              err = handleError(err);
              dispatch(failureCall(err));
              break;
            default:
              err = handleError(err);
              dispatch(failureCall(err));
              break;
          }
        } else if (history) {
          // Logout action will go here.
          // history.push("/");
        }
      });
  };
}
export function handleError(err) {
  map = objToStrMap(JSON.parse(JSON.stringify(err.response.data)));
  map.forEach((ele) => (err = ele));
  return err;
}
export function objToStrMap(obj) {
  let strMap = new Map();
  for (let k of Object.keys(obj)) {
    strMap.set(k, obj[k]);
  }
  return strMap;
}

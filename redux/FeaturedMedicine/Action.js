import Axios from "axios";
import { CustomerService_URL } from "../../constants/Urls";

export const SuggestMedActions = {
  FEATURED_MED_SUCCESS: "FEATURED_MED_SUCCESS",
  FEATURED_MED_ERROR: "FEATURED_MED_ERROR",
  PAST_PURCHASE_SUCCESS: "PAST_PURCHASE_SUCCESS",
  PAST_PURCHASE_ERROR: "PAST_PURCHASE_ERROR",
};

export function FeaturedMedSuccess(props) {
  return (dispatch) => {
    dispatch({
      type: SuggestMedActions.FEATURED_MED_SUCCESS,
      payload: props,
    });
  };
}

export function FeaturedMedError(error) {
  return {
    type: SuggestMedActions.FEATURED_MED_ERROR,
    payload: error,
  };
}

const separator = (urlArray) => {
  let temparray = [];
  let one = urlArray.find((data) => {
    let lastIndexOfSlash = data.lastIndexOf("/");
    return data.substring(lastIndexOfSlash + 1).includes("_1");
  });

  let two = urlArray.find((data) => {
    let lastIndexOfSlash = data.lastIndexOf("/");
    return data.substring(lastIndexOfSlash + 1).includes("_2");
  });
  let three = urlArray.find((data) => {
    let lastIndexOfSlash = data.lastIndexOf("/");
    return data.substring(lastIndexOfSlash + 1).includes("_3");
  });
  let four = urlArray.find((data) => {
    let lastIndexOfSlash = data.lastIndexOf("/");
    return data.substring(lastIndexOfSlash + 1).includes("_4");
  });
  if (one) {
    temparray.push(one);
  }
  if (two) {
    temparray.push(two);
  }
  if (three) {
    temparray.push(three);
  }
  if (four) {
    temparray.push(four);
  }
  return temparray;
};

const SeparateImages = (response) => {
  let data = response.map((data) => {
    return {
      ...data,
      product_image_urls: data._source.product_image_urls
        ? data._source.product_image_urls.split(",").length > 0
          ? separator(data._source.product_image_urls.split(","))
          : data._source.product_image_urls
        : "",
    };
  });
  return data;
};

export function FeaturedMedThunk(props) {
  return async (dispatch) => {
    // const getUrl = `${ElasticSearch_URL}/orgsubsmeds/_search`;
    const getUrl = `${CustomerService_URL}/callElasticSearch?warehouseId=${props.warehouseId}`;
    const config = {
      headers: {
        Authorization: "Bearer " + props.access_token,
      },
    };
    await Axios.post(
      getUrl,
      {
        size: 50,
        query: {
          range: {
            top_product_rank: {
              gt: 0,
            },
          },
        },
        sort: [{ top_product_rank: "asc" }],
        post_filter: {
          term: {
            original_is_searchable: "true",
          },
        },
      },
      props.access_token ? config : null
    )
      .then((response) => {
        if (response.status === 200) {
          dispatch(
            FeaturedMedSuccess({
              hits: SeparateImages(response.data.hits.hits),
            })
          );
        }
      })
      .catch((err) => {
        dispatch(FeaturedMedError(err.response));
      });
  };
}

export function PastPurchaseSuccess(props) {
  return (dispatch) => {
    dispatch({
      type: SuggestMedActions.PAST_PURCHASE_SUCCESS,
      payload: props,
    });
  };
}

export function PastPurchaseError(error) {
  return {
    type: SuggestMedActions.PAST_PURCHASE_ERROR,
    payload: error,
  };
}

const addPastOrderMedicineHandler = (cartItems) => {
  let modifyArray = cartItems.map((item) => {
    return {
      _source: {
        original_available: item.available,
        original_base_discount: item.discountPercentage,
        original_company_nm: item.companyName,
        original_sku_name: item.medicineName,
        original_mrp: item.latestMrp,
        original_supplied_bytm: item.suppliedByTM,
        subs_found: false,
        subs_selling_price: item.subsSellingPrice,
        original_product_code: item.productCode,
        subs_product_code: null,
        subs_mrp: 0,
        subs_sku_name: null,
        subs_company_nm: null,
        savings_percentage: 0,
      },
    };
  });
  return modifyArray;
};

export function PastPurchaseThunk(props) {
  return (dispatch) => {
    const getUrl = `${CustomerService_URL}/getCustomerDeliveredMedicines?customerId=${props.customerId}&page=1&noOfRecords=10`;
    const reqHeader = {
      transactionId: "a",
      "Content-Type": "application/json",
      Authorization: "Bearer " + props.accessToken,
    };
    let axiosBody = {
      method: "GET",
      url: getUrl,
      headers: reqHeader,
    };
    Axios(axiosBody)
      .then((response) => {
        if (response.status === 200) {
          dispatch(
            PastPurchaseSuccess(addPastOrderMedicineHandler(response.data))
          );
        }
      })
      .catch((err) => {
        if (err.response) {
          dispatch(
            PastPurchaseError("An error occurred while saving the changes")
          );
        } else {
          props.history.push("/");
        }
      });
  };
}

import { combineReducers } from "redux";
import { PincodeSectionReducer } from "../Pincode/Reducers";
import { MedResultReducer, MedDataReducerUpdate } from "../MedSearch/reducer";
import { FAQReducer } from "../FAQCall/reducer";
import { tmWalletReducer } from "../TMWallet/reducer";
import { recentSearchReducer } from "../RecentSearch/reducer";
import { loginReducer } from "../Login/Reducer";
import { myOrderReducer } from "../MyOrder/Reducer";
import { TruemedStepperReducer } from "../Timeline/Reducer";
import { AddressReducer } from "../AddressDetails/Reducer";
import {
  cancelOrderReducer,
  confirmorderReducer,
  fetchPatientDetailsReducer,
  fetchReasonReducer,
} from "../ConfirmOrder/Reducer";
import { searchMedicineReducer } from "../SearchMedicine/Reducer";
import { cartReducer } from "../Cart/Reducer";
import { savingsReducer } from "../MySaving/Reducer";
import { reorderReducer } from "../Reorder/Reducer";
import { TrackOrderReducer } from "../OrderTracker/Reducer";
import { DynamicBannersReducer } from "../BannerImages/Reducer";
import { doctorReducer } from "../Doctor/Reducer";
import { articleHlReducer } from "../HeadlessHealth/Reducer";
import { LoaderReducer } from "../Loader/Reducer";
import { legalThunkReducer } from "../Legals/reducer";
import { helpReducer, RaiseIssueReducer } from "../Help/reducer";
import { patientReducer } from "../PatientDetails/Reducer";
import { paymentReducer } from "../Payment/Reducer";
import {
  UploadImageReducer,
  UploadImageReducer2,
} from "../UploadImage/Reducer";
import { confirmMedicineReducer } from "../ConfirmMedicine/Reducer";
import { OrderDetailsReducer } from "../OrderDetail/Reducer";
import { incompleteOrderReducer } from "../IncompleteOrder/Reducer";
import { suggestMedReducer } from "../FeaturedMedicine/Reducer";
import { doctorRatingReducer } from "../DoctorRating/Reducer";
import {
  AboutusReducer,
  MasterReducer,
  MySubsReducer,
  sidebarReducer,
} from "../SliderService/Reducer";
import { ProductDetailsReducer } from "../ProductDetails/Reducer";
import { compareMedsElasticReducer } from "../CompareMedsElastic/Reducer";
import { storeDataReducer } from "../storeData/Reducer";
import { RNEReducer } from "../ReferEarn/reducer";

const rootReducer = combineReducers({
  MedResult: MedResultReducer,
  medUpdate: MedDataReducerUpdate,
  pincodeData: PincodeSectionReducer,
  faqData: FAQReducer,
  wallet: tmWalletReducer,
  recentSearch: recentSearchReducer,
  loginReducer,
  myOrder: myOrderReducer,
  sidebar: sidebarReducer,
  stepper: TruemedStepperReducer,
  addressData: AddressReducer,
  pincodeSelctedAddress: PincodeSectionReducer,
  currentPage: PincodeSectionReducer,
  confirmOrder: confirmorderReducer,
  cancelOrder: cancelOrderReducer,
  searchMedicine: searchMedicineReducer,
  cartData: cartReducer,
  savingsReducer,
  reorderReducer,
  trackOrder: TrackOrderReducer,
  bannerImages: DynamicBannersReducer,
  doctorReducer,
  articleHlDetails: articleHlReducer,
  loader: LoaderReducer,
  legal: legalThunkReducer,
  help: helpReducer,
  RaiseIssue: RaiseIssueReducer,
  patientData: patientReducer,
  uploadImage: UploadImageReducer,
  uploadImg: UploadImageReducer2,
  paymentReducer,
  confirmMedicineReducer: confirmMedicineReducer,
  orderStatusUpdate: OrderDetailsReducer,
  incompleteOrderReducer,
  reasonList: fetchReasonReducer,
  doctorRatings: doctorRatingReducer,
  patientOrderDetails: fetchPatientDetailsReducer,
  suggestMed: suggestMedReducer,
  masterDetails: MasterReducer,
  mySubs: MySubsReducer,
  about: AboutusReducer,
  productDetails: ProductDetailsReducer,
  compareMedsElastic: compareMedsElasticReducer,
  storeData: storeDataReducer,
  referEarn: RNEReducer,
});

export default rootReducer;

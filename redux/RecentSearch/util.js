export const recentSearchHandler = (searchedHistory, searchText) => {
  const existingCartItem = searchedHistory.find((item) => item === searchText);

  if (existingCartItem) {
    return searchedHistory.map((item) => item);
  }
  return [...searchedHistory, searchText];
};

export const deleteRecentSearchHandler = (searchedHistory, searchText) => {
  const existingCartItem = searchedHistory.filter(
    (item) => item !== searchText
  );

  return [...existingCartItem];
};

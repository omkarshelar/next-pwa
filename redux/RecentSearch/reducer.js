import { recentSearchActions } from "./action";
import { deleteRecentSearchHandler, recentSearchHandler } from "./util";

const initState = {
  searchHistory: [],
};

export const recentSearchReducer = (state = initState, action) => {
  switch (action.type) {
    case recentSearchActions.ADD_RECENT_SEARCH:
      return {
        ...state,
        searchHistory: recentSearchHandler(state.searchHistory, action.payload),
      };

    case recentSearchActions.CLEAR_RECENT_SEARCH:
      return {
        ...state,
        searchHistory: [],
      };
    case recentSearchActions.DELETE_RECENT_SEARCH:
      return {
        ...state,
        searchHistory: deleteRecentSearchHandler(
          state.searchHistory,
          action.payload
        ),
      };

    default:
      return state;
  }
};

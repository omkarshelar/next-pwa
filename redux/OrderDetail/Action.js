import { CustomerService_URL } from "../../constants/Urls";
import { makeApiCall } from "../makeApiCall";

export const OrderStatusActions = {
  ORDER_DETAIL_SUCCESS: "ORDER_DETAIL_SUCCESS",
  ORDER_DETAIL_FAILURE: "ORDER_DETAIL_FAILURE",
  UPDATE_ORDER_DETAILS: "UPDATE_ORDER_DETAILS",
};

export function OrderStatusSuccessAction(props) {
  return (dispatch) => {
    dispatch({
      type: OrderStatusActions.ORDER_DETAIL_SUCCESS,
      payload: props,
    });
  };
}

export function OrderStatusFailureAction(error) {
  return {
    type: OrderStatusActions.ORDER_DETAIL_FAILURE,
    payload: error,
  };
}

//Order details
export function orderStatusThunk(props) {
  const reqHeader = {
    "Content-Type": "application/json",
    Authorization: "Bearer " + props.access_token,
    "Access-Control-Allow-Origin": "*",
  };
  const postUrl = `${CustomerService_URL}/getOrderDetails?orderId=${props.orderIds}&customerId=${props.customerId}`;
  return makeApiCall(
    postUrl,
    OrderStatusSuccessAction,
    OrderStatusFailureAction,
    "GET",
    null,
    reqHeader,
    props.history
  );
}

export function updateOrderDetails(props) {
  return (dispatch) => {
    dispatch({
      type: OrderStatusActions.UPDATE_ORDER_DETAILS,
      payload: props,
    });
  };
}

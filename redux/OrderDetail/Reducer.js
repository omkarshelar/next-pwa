import { OrderStatusActions } from "./Action";
import { LoginActions } from "../Login/Action";

let OrderDetailState = {
  OrderStatusData: null,
  error: null,
};

export const OrderDetailsReducer = (
  state = OrderDetailState,
  { type, payload }
) => {
  switch (type) {
    case OrderStatusActions.ORDER_DETAIL_SUCCESS:
      return {
        ...state,
        OrderStatusData: payload,
        error: null,
      };

    case OrderStatusActions.ORDER_DETAIL_FAILURE:
      return {
        ...state,
        OrderStatusData: null,
        error: payload,
      };
    case LoginActions.USER_LOGOUT:
      return {
        ...OrderDetailState,
      };
    case OrderStatusActions.UPDATE_ORDER_DETAILS:
      return {
        ...state,
        OrderStatusData: payload,
      };
    default:
      return state;
  }
};

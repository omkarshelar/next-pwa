import { LoaderActions } from "./Action";

const initialState = {
  isLoading: false,
  isLoading1: false,
  isLoading2: false,
};

export const LoaderReducer = (state = initialState, action) => {
  switch (action.type) {
    case LoaderActions.LOADING_START:
      return { ...state, isLoading: true };
    case LoaderActions.LOADING_STOP:
      return { ...state, isLoading: false };
    case LoaderActions.LOADING_START_1:
      return { ...state, isLoading1: true };
    case LoaderActions.LOADING_STOP_1:
      return { ...state, isLoading1: false };
    case LoaderActions.LOADING_START_2:
      return { ...state, isLoading2: true };
    case LoaderActions.LOADING_STOP_2:
      return { ...state, isLoading2: false };
    default:
      return state;
  }
};

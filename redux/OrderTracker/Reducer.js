import { TrackOrderActions } from "./Action";
import { LoginActions } from "../Login/Action";

let TrackOrderState = {
  TrackOrderData: null,
  error: null,
};

export const TrackOrderReducer = (
  state = TrackOrderState,
  { type, payload }
) => {
  switch (type) {
    case TrackOrderActions.TRACK_ORDER_SUCCESS:
      return {
        ...state,
        TrackOrderData: payload.liveOrders,
        error: null,
      };

    case TrackOrderActions.TRACK_ORDER_FAILURE:
      return { ...state, TrackOrderData: null, error: payload };

    case LoginActions.VERIFY_OTP_SUCCESS:
      return { ...state, TrackOrderData: null, error: null };

    case LoginActions.USER_LOGOUT:
      return { ...state, TrackOrderData: null, error: 401 };
    default:
      return state;
  }
};

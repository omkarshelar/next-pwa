import { LoginActions } from "../Login/Action";
import { myorderActions } from "./Action";

let myorderState = {
  orderTrackerSuccess: null,
  orderTrackerError: null,
  allOrdersSuccess: null,
  allOrdersError: null,
  orderStatusSuccess: null,
  orderStatusError: null,
  orderBillSuccess: null,
  orderBillError: null,
  changePaymentModeSuccess: null,
  changePaymentModeError: null,
  setPaymentModeError: null,
};

export const myOrderReducer = (initialState = myorderState, action) => {
  switch (action.type) {
    // Order Tracker
    case myorderActions.ORDER_TRACKER_HOME_SUCCESS:
      return {
        ...initialState,
        orderTrackerSuccess: action.payload,
        orderTrackerError: null,
      };
    case myorderActions.ORDER_TRACKER_HOME_FAILURE:
      return {
        ...initialState,
        orderTrackerSuccess: null,
        orderTrackerError: action.payload,
      };
    // Fetch All Orders
    case myorderActions.ALL_ORDERS_SUCCESS:
      return {
        ...initialState,
        allOrdersSuccess: action.payload,
        allOrdersError: null,
      };
    case myorderActions.ALL_ORDERS_FAILURE:
      return {
        ...initialState,
        allOrdersSuccess: null,
        allOrdersError: action.payload,
      };
    // Fetch order status
    case myorderActions.FETCH_ORDER_STATUS_SUCCESS:
      return {
        ...initialState,
        orderStatusSuccess: action.payload,
        orderStatusError: null,
      };
    case myorderActions.FETCH_ORDER_STATUS_FAILURE:
      return {
        ...initialState,
        orderStatusSuccess: null,
        orderStatusError: action.payload,
      };
    // Fetch order bill.
    case myorderActions.FETCH_ORDER_BILL_SUCCESS:
      return {
        ...initialState,
        orderBillSuccess: action.payload,
        orderBillError: null,
      };
    case myorderActions.FETCH_ORDER_BILL_FAILURE:
      return {
        ...initialState,
        orderBillSuccess: null,
        orderBillError: action.payload,
      };
    // Change payment mode.
    case myorderActions.CHANGE_PAYMENT_MODE_SUCCESS:
      return {
        ...initialState,
        changePaymentModeSuccess: action.payload,
        changePaymentModeError: null,
      };
    case myorderActions.CHANGE_PAYMENT_MODE_FAILURE:
      return {
        ...initialState,
        changePaymentModeSuccess: null,
        changePaymentModeError: action.payload,
      };
    case myorderActions.SET_PAYMENT_MODE_SUCCESS:
      return {
        ...initialState,
        setPaymentModeError: null,
      };
    case myorderActions.SET_PAYMENT_MODE_FAILURE:
      return {
        ...initialState,
        setPaymentModeError: action.payload,
      };
    case LoginActions.USER_LOGOUT:
      return {
        ...myorderState,
      };

    default:
      return initialState;
  }
};

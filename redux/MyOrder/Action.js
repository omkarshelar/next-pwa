import axios from "axios";
import {
  CustomerService_URL,
  ThirdPartySevice_URL,
} from "../../constants/Urls";
import { makeApiCall } from "../makeApiCall";

export const myorderActions = {
  ORDER_TRACKER_HOME_SUCCESS: "ORDER_TRACKER_HOME_SUCCESS",
  ORDER_TRACKER_HOME_FAILURE: "ORDER_TRACKER_HOME_FAILURE",
  ALL_ORDERS_SUCCESS: "ALL_ORDERS_SUCCESS",
  ALL_ORDERS_FAILURE: "ALL_ORDERS_FAILURE",
  FETCH_ORDER_STATUS_SUCCESS: "FETCH_ORDER_STATUS_SUCCESS",
  FETCH_ORDER_STATUS_FAILURE: "FETCH_ORDER_STATUS_FAILURE",
  FETCH_ORDER_BILL_SUCCESS: "FETCH_ORDER_BILL_SUCCESS",
  FETCH_ORDER_BILL_FAILURE: "FETCH_ORDER_BILL_FAILURE",
  CHANGE_PAYMENT_MODE_SUCCESS: "CHANGE_PAYMENT_MODE_SUCCESS",
  CHANGE_PAYMENT_MODE_FAILURE: "CHANGE_PAYMENT_MODE_FAILURE",
  SET_PAYMENT_MODE_SUCCESS: "SET_PAYMENT_MODE_SUCCESS",
  SET_PAYMENT_MODE_FAILURE: "SET_PAYMENT_MODE_FAILURE",
};

// ORDER TRACKER ON HOME TRACKER SECTION.

export function orderTrackerSuccess(props) {
  return (dispatch) => {
    dispatch({
      type: myorderActions.ORDER_TRACKER_HOME_SUCCESS,
      payload: props,
    });
  };
}

export function orderTrackerError(error) {
  return {
    type: myorderActions.ORDER_TRACKER_HOME_FAILURE,
    payload: error,
  };
}

export function orderTrackerThunk(props) {
  return (dispatch) => {
    const postUrl = `${CustomerService_URL}/getOrderTrackOnHome`;
    let reqHeader = {
      transactionId: "a",
      // "Content-Type": "application/json",
      Authorization: props.accessToken ? "Bearer " + props.accessToken : "",
      // "Access-Control-Allow-Origin": "*",
    };
    let axiosBody = {
      method: "POST",
      url: postUrl,
      data: null,
      headers: reqHeader,
    };
    return axios(axiosBody)
      .then((response) => {
        if (response.status === 200) {
          dispatch(orderTrackerSuccess(response.data.liveOrders));
        }
      })
      .catch((err) => {
        if (err.response) {
          dispatch(orderTrackerError("something went wrong"));
        } else {
          dispatch(orderTrackerError(401));
        }
      });
  };
}

// Fetch all orders

export function allOrdersSuccess(props) {
  return (dispatch) => {
    dispatch({
      type: myorderActions.ALL_ORDERS_SUCCESS,
      payload: props,
    });
  };
}

export function allOrdersError(error) {
  return {
    type: myorderActions.ALL_ORDERS_FAILURE,
    payload: error,
  };
}

export function allOrdersThunk(props) {
  const reqHeader = {
    "Content-Type": "application/json",
    Authorization: "Bearer " + props.accessToken,
  };
  const getUrl = `${CustomerService_URL}/getAllOrders`;
  return makeApiCall(
    getUrl,
    allOrdersSuccess,
    allOrdersError,
    "POST",
    null,
    reqHeader,
    props.history
  );
}

// Fetch Order Status.

export function fetchOrderStatusSuccess(props) {
  return (dispatch) => {
    dispatch({
      type: myorderActions.FETCH_ORDER_STATUS_SUCCESS,
      payload: props,
    });
  };
}

export function fetchOrderStatusError(error) {
  return {
    type: myorderActions.FETCH_ORDER_STATUS_FAILURE,
    payload: error,
  };
}

export function fetchOrderStatusThunk(props) {
  const reqHeader = {
    "Content-Type": "application/json",
    Authorization: "Bearer " + props.accessToken,
  };
  const getUrl = `${CustomerService_URL}/getOrderStatus?orderId=${props.orderId}`;
  return makeApiCall(
    getUrl,
    fetchOrderStatusSuccess,
    fetchOrderStatusError,
    "POST",
    null,
    reqHeader,
    props.history
  );
}

// Fetch order bill.

export function fetchOrderBillSuccess(props) {
  return (dispatch) => {
    dispatch({
      type: myorderActions.FETCH_ORDER_BILL_SUCCESS,
      payload: props,
    });
  };
}

export function fetchOrderBillError(error) {
  return {
    type: myorderActions.FETCH_ORDER_BILL_FAILURE,
    payload: error,
  };
}

export function fetchOrderBillThunk(props) {
  const reqHeader = {
    "Content-Type": "application/json",
    Authorization: "Bearer " + props.accessToken,
  };
  const getUrl = `${CustomerService_URL}/getOrderBill?orderId=${props.orderId}`;
  return makeApiCall(
    getUrl,
    fetchOrderBillSuccess,
    fetchOrderBillError,
    "POST",
    null,
    reqHeader,
    props.history
  );
}

// Change payment mode

export function changePaymentModeSuccess(props) {
  return (dispatch) => {
    dispatch({
      type: myorderActions.CHANGE_PAYMENT_MODE_SUCCESS,
      payload: props,
    });
  };
}

export function changePaymentModeError(error) {
  return {
    type: myorderActions.CHANGE_PAYMENT_MODE_FAILURE,
    payload: error,
  };
}

export function changePaymentModeThunk(props) {
  const reqHeader = {
    "Content-Type": "application/json",
    Authorization: "Bearer " + props.accessToken,
  };
  const getUrl = `${ThirdPartySevice_URL}/changePaymentMode?orderId=${
    props.orderId
  }&paymentModeId=${props.paymentModeId}&transactionId=${
    props.orderId + props.paymentModeId
  }`;
  return makeApiCall(
    getUrl,
    changePaymentModeSuccess,
    changePaymentModeError,
    "GET",
    null,
    reqHeader,
    props.history
  );
}

// set payment mode

export function setPaymentModeSuccess(props) {
  return (dispatch) => {
    dispatch({
      type: myorderActions.SET_PAYMENT_MODE_SUCCESS,
      payload: props,
    });
  };
}

export function setPaymentModeError(error) {
  return {
    type: myorderActions.SET_PAYMENT_MODE_FAILURE,
    payload: error,
  };
}

export function setPaymentModeThunk(props) {
  const reqHeader = {
    "Content-Type": "application/json",
    Authorization: "Bearer " + props.access_token,
  };
  const postUrl = `${CustomerService_URL}/setPaymentModeForAnOrder?orderId=${props.orderId}&paymentId=${props.paymentId}`;
  return makeApiCall(
    postUrl,
    setPaymentModeSuccess,
    setPaymentModeError,
    "POST",
    null,
    reqHeader,
    props.history
  );
}

import { compareMedsElasticActions } from "./Action";

const initState = {
  compareMedsElasticSuccess: null,
  compareMedsElasticError: null,
};

export const compareMedsElasticReducer = (
  state = initState,
  { type, payload }
) => {
  switch (type) {
    case compareMedsElasticActions.COMPARE_MEDS_ELASTIC_SUCCESS:
      return {
        ...state,
        compareMedsElasticSuccess: payload,
        compareMedsElasticError: null,
      };

    case compareMedsElasticActions.COMPARE_MEDS_ELASTIC_FAILURE:
      return {
        ...state,
        compareMedsElasticSuccess: null,
        compareMedsElasticError: payload,
      };

    default:
      return state;
  }
};

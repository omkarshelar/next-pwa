import Axios from "axios";
import { CustomerService_URL } from "../../constants/Urls";
import { startLoading, stopLoading } from "../Loader/Action";
import { makeApiCall } from "../makeApiCall";

export const compareMedsElasticActions = {
  COMPARE_MEDS_ELASTIC_SUCCESS: "COMPARE_MEDS_ELASTIC_SUCCESS",
  COMPARE_MEDS_ELASTIC_FAILURE: "COMPARE_MEDS_ELASTIC_FAILURE",
};

///////////////////////////////////////
//? Elastic search for compare meds
///////////////////////////////////////

export function compareMedsElasticSuccess(props) {
  return (dispatch) => {
    dispatch({
      type: compareMedsElasticActions.COMPARE_MEDS_ELASTIC_SUCCESS,
      payload: props,
    });
  };
}

export function compareMedsElasticError(error) {
  return {
    type: compareMedsElasticActions.COMPARE_MEDS_ELASTIC_FAILURE,
    payload: error,
  };
}

export function compareMedsElasticThunk(props) {
  return (dispatch) => {
    dispatch(startLoading());
    // const getUrl = `${ElasticSearch_URL}/orgsubsmeds/_search`;
    const getUrl = `${CustomerService_URL}/callElasticSearch?warehouseId=${props.warehouseId}`;
    const config = {
      headers: {
        Authorization: "Bearer " + props.access_token,
      },
    };
    Axios.post(getUrl, props.medicine, props.access_token ? config : null)
      .then((response) => {
        dispatch(stopLoading());
        if (response.status === 200) {
          dispatch(
            compareMedsElasticSuccess({
              hits: response?.data?.hits?.hits,
              suggestion: response?.data?.suggest?.skuNameSuggester
                ? response.data.suggest.skuNameSuggester
                : null,
            })
          );
        }
      })
      .catch((err) => {
        dispatch(stopLoading());
        dispatch(compareMedsElasticError(err.response));
      });
  };
}

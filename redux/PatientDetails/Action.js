import { CustomerService_URL, TrackingService_URL } from "../../constants/Urls";
import { makeApiCall } from "../makeApiCall";

export const patientActions = {
  FETCH_PATIENT_SUCCESS: "FETCH_PATIENT_SUCCESS",
  FETCH_PATIENT_FAILURE: "FETCH_PATIENT_FAILURE",
  DELETE_PATIENT_SUCCESS: "DELETE_PATIENT_SUCCESS",
  DELETE_PATIENT_FAILURE: "DELETE_PATIENT_FAILURE",
  NEW_PATIENT_SUCCESS: "NEW_PATIENT_SUCCESS",
  NEW_PATIENT_FAILURE: "NEW_PATIENT_FAILURE",
  SAVE_PATIENT_SUCCESS: "SAVE_PATIENT_SUCCESS",
  SAVE_PATIENT_FAILURE: "SAVE_PATIENT_FAILURE",
  UPDATE_PROFILE_SUCCESS: "UPDATE_PROFILE_SUCCESS",
  UPDATE_PROFILE_FAILURE: "UPDATE_PROFILE_FAILURE",
  SELECT_PATIENT_FOR_ORDER: "SELECT_PATIENT_FOR_ORDER",
  FETCH_PATIENT_BY_ID_SUCCESS: "FETCH_PATIENT_BY_ID_SUCCESS",
  FETCH_PATIENT_BY_ID_FAILURE: "FETCH_PATIENT_BY_ID_FAILURE",
  SAVE_ALT_MOBILE_SUCCESS: "SAVE_ALT_MOBILE_SUCCESS",
  SAVE_ALT_MOBILE_FAILURE: "SAVE_ALT_MOBILE_FAILURE",
  GET_COMPARE_ORDERDETAILS_SUCCESS: "GET_COMPARE_ORDERDETAILS_SUCCESS",
  GET_COMPARE_ORDERDETAILS_FAILURE: "GET_COMPARE_ORDERDETAILS_FAILURE",
  APPLY_PATIENT_FILTER: "APPLY_PATIENT_FILTER",
  CLEAR_PATIENT_FILTER: "CLEAR_PATIENT_FILTER",
};

// Fetch Patient Details
export function fetchpatientSuccess(props) {
  return (dispatch) => {
    dispatch({
      type: patientActions.FETCH_PATIENT_SUCCESS,
      payload: props,
    });
  };
}

export function fetchpatientError(error) {
  return {
    type: patientActions.FETCH_PATIENT_FAILURE,
    payload: error,
  };
}

export function fetchpatientThunk(props) {
  const reqHeader = {
    transactionId: "a",
    "Content-Type": "application/json",
    Authorization: "Bearer " + props.accessToken,
  };
  const getUrl = `${CustomerService_URL}/getAllPatient?showMyself=true`;
  return makeApiCall(
    getUrl,
    fetchpatientSuccess,
    fetchpatientError,
    "POST",
    null,
    reqHeader,
    props.history
  );
}

// Add New patient

export function addPatientSuccess(props) {
  return (dispatch) => {
    dispatch({
      type: patientActions.NEW_PATIENT_SUCCESS,
      payload: props,
    });
  };
}

export function addPatientError(error) {
  return {
    type: patientActions.NEW_PATIENT_FAILURE,
    payload: error,
  };
}

export function addPatientThunk(props) {
  const reqHeader = {
    transactionId: "a",
    "Content-Type": "application/json",
    Authorization: "Bearer " + props.accessToken,
  };
  const getUrl = `${CustomerService_URL}/addPatient`;
  return makeApiCall(
    getUrl,
    addPatientSuccess,
    addPatientError,
    "POST",
    props.patientData,
    reqHeader,
    props.history
  );
}

// Delete Patient

export function deletePatientSuccess(props) {
  return (dispatch) => {
    dispatch({
      type: patientActions.DELETE_PATIENT_SUCCESS,
      payload: props,
    });
  };
}

export function deletePatientError(error) {
  return {
    type: patientActions.DELETE_PATIENT_FAILURE,
    payload: error,
  };
}

export function deletePatientThunk(props) {
  const reqHeader = {
    transactionId: "a",
    "Content-Type": "application/json",
    Authorization: "Bearer " + props.accessToken,
  };
  const getUrl = `${CustomerService_URL}/deletePatient?patientId=${props.patientId}&customerId=${props.customerId}`;
  return makeApiCall(
    getUrl,
    deletePatientSuccess,
    deletePatientError,
    "POST",
    null,
    reqHeader,
    props.history
  );
}

// Update User Profile

export function updateProfileSuccess(props) {
  return (dispatch) => {
    dispatch({
      type: patientActions.UPDATE_PROFILE_SUCCESS,
      payload: props,
    });
  };
}

export function updateProfileError(error) {
  return {
    type: patientActions.UPDATE_PROFILE_FAILURE,
    payload: error,
  };
}

export function updateProfileThunk(props) {
  const reqHeader = {
    transactionId: "a",
    "Content-Type": "application/json",
    Authorization: "Bearer " + props.accessToken,
  };
  const getUrl = `${CustomerService_URL}/updateProfile`;
  return makeApiCall(
    getUrl,
    updateProfileSuccess,
    updateProfileError,
    "POST",
    props.latestData,
    reqHeader,
    props.history
  );
}

// Save patient for checkout

export function savePatientCheckoutSuccess(props) {
  return (dispatch) => {
    dispatch({
      type: patientActions.SAVE_PATIENT_SUCCESS,
      payload: props,
    });
  };
}

export function savePatientCheckoutError(error) {
  return {
    type: patientActions.SAVE_PATIENT_FAILURE,
    payload: error,
  };
}

export function savePatientCheckoutThunk(props) {
  const reqHeader = {
    transactionId: "a",
    "Content-Type": "application/json",
    Authorization: "Bearer " + props.accessToken,
  };
  const getUrl = `${CustomerService_URL}/updatePatientIdInSubOrder?patientId=${props.patientId}&orderId=${props.orderId}`;
  return makeApiCall(
    getUrl,
    savePatientCheckoutSuccess,
    savePatientCheckoutError,
    "GET",
    null,
    reqHeader,
    props.history
  );
}

export function selectPatinetForOrderAction(props) {
  return (dispatch) => {
    dispatch({
      type: patientActions.SELECT_PATIENT_FOR_ORDER,
      payload: props,
    });
  };
}

// Fetch Patient Details by id
export function fetchPatientByIdSuccess(props) {
  return (dispatch) => {
    dispatch({
      type: patientActions.FETCH_PATIENT_BY_ID_SUCCESS,
      payload: props,
    });
  };
}

export function fetchPatientByIdError(error) {
  return {
    type: patientActions.FETCH_PATIENT_BY_ID_FAILURE,
    payload: error,
  };
}

export function fetchPatientByIdThunk(props) {
  const reqHeader = {
    transactionId: "a",
    "Content-Type": "application/json",
    Authorization: "Bearer " + props.accessToken,
  };
  const postUrl = `${CustomerService_URL}/getPatientById?patientId=${props.patientId}&orderId=${props.orderId}`;
  return makeApiCall(
    postUrl,
    fetchPatientByIdSuccess,
    fetchPatientByIdError,
    "POST",
    null,
    reqHeader,
    props.history
  );
}

///////////////////////////////////////
//? Fetch Patient Details by id
///////////////////////////////////////

export function saveAltMobSuccess(props) {
  return (dispatch) => {
    dispatch({
      type: patientActions.SAVE_ALT_MOBILE_SUCCESS,
      payload: props,
    });
  };
}

export function saveAltMobError(error) {
  return {
    type: patientActions.SAVE_ALT_MOBILE_FAILURE,
    payload: error,
  };
}

export function saveAltMobThunk(props) {
  const reqHeader = {
    transactionId: "a",
    "Content-Type": "application/json",
    Authorization: "Bearer " + props.access_token,
  };
  const postUrl = `${CustomerService_URL}/saveAlternateNumberForOrder?orderId=${props.orderId}&alternateNumber=${props.alternateNumber}`;
  return makeApiCall(
    postUrl,
    saveAltMobSuccess,
    saveAltMobError,
    "GET",
    null,
    reqHeader,
    props.history
  );
}

///////////////////////////////////////
//? Compare Order details
///////////////////////////////////////

export function compareOrderDetailsSuccess(props) {
  return (dispatch) => {
    console.log(props, "props");
    dispatch({
      type: patientActions.GET_COMPARE_ORDERDETAILS_SUCCESS,
      payload: props,
    });
  };
}

export function compareOrderDetailsError(error) {
  return {
    type: patientActions.GET_COMPARE_ORDERDETAILS_FAILURE,
    payload: error,
  };
}

export function compareOrderDetailsThunk(props) {
  const reqHeader = {
    transactionId: "a",
    "Content-Type": "application/json",
    Authorization: "Bearer " + props.access_token,
  };
  const postUrl = `${TrackingService_URL}/fetchDoctorOrderConfirmdAndBoxVerifiedDetails?orderId=${props.orderId}`;
  return makeApiCall(
    postUrl,
    compareOrderDetailsSuccess,
    compareOrderDetailsError,
    "GET",
    null,
    reqHeader,
    props.history
  );
}

export function applyPatientFilterAction(props) {
  return (dispatch) => {
    dispatch({
      type: patientActions.APPLY_PATIENT_FILTER,
      payload: props,
    });
  };
}

export function clearPatientFilterAction() {
  return (dispatch) => {
    dispatch({
      type: patientActions.CLEAR_PATIENT_FILTER,
    });
  };
}

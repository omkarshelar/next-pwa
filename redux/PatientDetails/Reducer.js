import { selectPatientUtil } from "../PatientData/Util/PatientDetail";
import { LoginActions } from "../Login/Action";
import { patientActions } from "./Action";

let patientState = {
  fetchPatientSuccess: null,
  fetchPatientError: null,
  newPatientSuccess: null,
  newPatientError: null,
  updateProfileSuccess: null,
  updateProfileError: null,
  deletePatientSuccess: null,
  deletePatientError: null,
  savePatientSuccess: null,
  savePatientError: null,
  fetchPatientByIdSuccess: null,
  fetchPatientByIdError: null,
  saveAltMobSuccess: null,
  saveAltMobError: null,
  compareOrderDetailsSuccess: null,
  compareOrderDetailsError: null,
  patientFilter: null,
};

export const patientReducer = (initialState = patientState, action) => {
  switch (action.type) {
    case patientActions.FETCH_PATIENT_SUCCESS:
      return {
        ...initialState,
        fetchPatientSuccess: action.payload.PatientList,
        fetchPatientError: null,
      };
    case patientActions.FETCH_PATIENT_FAILURE:
      return {
        ...initialState,
        fetchPatientSuccess: null,
        fetchPatientError: action.payload,
      };
    case patientActions.FETCH_PATIENT_BY_ID_SUCCESS:
      return {
        ...initialState,
        fetchPatientByIdSuccess: action.payload.PatientDto,
        fetchPatientByIdError: null,
      };
    case patientActions.FETCH_PATIENT_BY_ID_FAILURE:
      return {
        ...initialState,
        fetchPatientByIdSuccess: null,
        fetchPatientByIdError: action.payload,
      };
    case patientActions.SAVE_ALT_MOBILE_SUCCESS:
      return {
        ...initialState,
        saveAltMobSuccess: action.payload,
        saveAltMobError: null,
      };
    case patientActions.SAVE_ALT_MOBILE_FAILURE:
      return {
        ...initialState,
        saveAltMobSuccess: null,
        saveAltMobError: action.payload,
      };
    case patientActions.GET_COMPARE_ORDERDETAILS_SUCCESS:
      return {
        ...initialState,
        compareOrderDetailsSuccess: action.payload,
        compareOrderDetailsError: null,
      };
    case patientActions.GET_COMPARE_ORDERDETAILS_FAILURE:
      return {
        ...initialState,
        compareOrderDetailsSuccess: null,
        compareOrderDetailsError: action.payload,
      };
    case patientActions.DELETE_PATIENT_SUCCESS:
      return {
        ...initialState,
        deletePatientSuccess: action.payload,
        deletePatientError: null,
      };
    case patientActions.DELETE_PATIENT_FAILURE:
      return {
        ...initialState,
        deletePatientSuccess: null,
        deletePatientError: action.payload,
      };
    case patientActions.NEW_PATIENT_SUCCESS:
      return {
        ...initialState,
        newPatientSuccess: action.payload,
        newPatientError: null,
      };
    case patientActions.NEW_PATIENT_FAILURE:
      return {
        ...initialState,
        newPatientSuccess: null,
        newPatientError: action.payload,
      };
    case patientActions.UPDATE_PROFILE_SUCCESS:
      return {
        ...initialState,
        updateProfileSuccess: action.payload,
        updateProfileError: null,
      };
    case patientActions.UPDATE_PROFILE_FAILURE:
      return {
        ...initialState,
        updateProfileSuccess: null,
        updateProfileError: action.payload,
      };
    case patientActions.SAVE_PATIENT_SUCCESS:
      return {
        ...initialState,
        savePatientSuccess: selectPatientUtil(
          initialState.fetchPatientSuccess,
          action.payload
        ),
        savePatientError: null,
      };
    case patientActions.SAVE_PATIENT_FAILURE:
      return {
        ...initialState,
        savePatientSuccess: null,
        savePatientError: action.payload,
      };
    case patientActions.SELECT_PATIENT_FOR_ORDER:
      return {
        ...initialState,
        fetchPatientSuccess: selectPatientUtil(
          initialState.fetchPatientSuccess,
          action.payload
        ),
      };
    case patientActions.APPLY_PATIENT_FILTER:
      return {
        ...initialState,
        patientFilter: action.payload,
      };

    case patientActions.CLEAR_PATIENT_FILTER:
      return {
        ...initialState,
        patientFilter: null,
      };

    case LoginActions.USER_LOGOUT:
      return {
        ...patientState,
      };

    default:
      return initialState;
  }
};

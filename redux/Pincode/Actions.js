import Axios from "axios";
import { CustomerService_URL } from "../../constants/Urls";
import { updateCartItems } from "../Cart/Action";

export const pincodeAction = {
  PINCODE_SECTION: "PINCODE_SECTION",
  FETCH_POST_SELECT_MEDICINE_ERROR: "FETCH_POST_SELECT_MEDICINE_ERROR",
  FETCH_POST_SELECT_MEDICINE_SUCCESS: "FETCH_POST_SELECT_MEDICINE_SUCCESS",
  SELECTED_ADDRESS: "SELECTED_ADDRESS",
  SET_CURRENT_PAGE: "SET_CURRENT_PAGE",
};

export const pincodeSectionAction = (data) => ({
  type: pincodeAction.PINCODE_SECTION,
  payload: data,
});

export const setCurrentPage = (data) => ({
  type: pincodeAction.SET_CURRENT_PAGE,
  payload: data,
});

export const saveSelectedAddress = (data) => ({
  type: pincodeAction.SELECTED_ADDRESS,
  payload: data,
});

export function fetchMedicineThunkPostSelect(props) {
  return (dispatch) => {
    // const getUrl = `${ElasticSearch_URL}/orgsubsmeds/_search`;
    const getUrl = `${CustomerService_URL}/callElasticSearch?warehouseId=${props.warehouseId}`;
    const config = {
      headers: {
        Authorization: "Bearer " + props.access_token,
      },
    };
    Axios.post(getUrl, props.medicine, props.access_token ? config : null)
      .then((response) => {
        if (response.status === 200) {
          let newCart = [];
          let cartArray = [];
          let cartQtyArray = [];
          props.cartContent.map((f) => {
            cartArray.push(f._source.original_product_code);
            cartQtyArray.push({
              newQty: f.newQty,
              quantity: f.quantity,
              sort: f.sort,
              product_image_urls: f.product_image_urls,
              Test: 11,
            });
          });

          //Used for Duplication giving an issue - OMKAR

          // let hits = [];
          // let hitsDuplicate = [];
          // let hitsData = [];
          // let hitsDuplicateData = [];

          // let finalHits = [];

          // let arrayTemp = [];
          // let tempHits = response.data.hits.hits;
          // response.data.hits.hits.map((e, i) => {
          //   if (arrayTemp.includes(e._source.original_product_code)) {
          //     hitsDuplicate.push(i);
          //   } else {
          //     hits.push(i);
          //   }
          //   arrayTemp.push(e._source.original_product_code);
          // });

          // console.log(hits, "hitsData");
          // console.log(hitsDuplicate, "hitsDuplicateData");

          // console.log(
          //   response.data.hits.hits.filter((e, i) => hits.includes(i)),
          //   "Filterrr"
          // );
          // hits.map((e) => {
          //   console.log("E", e);
          //   hitsData.push(response.data.hits.hits[e]);
          //   console.log(response.data.hits.hits[e], e);
          // });

          // hitsDuplicate.map((e) => {
          //   hitsDuplicateData.push(response.data.hits.hits[e]);
          // });

          // console.log(hitsData, "hitsData");
          // console.log(hitsDuplicateData, "hitsDuplicateData");

          // finalHits = hitsData;
          // if (hitsDuplicateData.length > 0) {
          //   hitsData.map((e, i) => {
          //     hitsDuplicateData.map((f, j) => {
          //       if (
          //         e._source.original_product_code ==
          //         f._source.original_product_code
          //       ) {
          //         if (
          //           new Date(e._source.modified_on) >
          //           new Date(f._source.modified_on)
          //         ) {
          //           finalHits[i] = e;
          //         } else {
          //           finalHits[i] = f;
          //         }
          //       }
          //     });
          //   });
          // }

          // console.log(finalHits, "finalHits");
          response.data.hits.hits.map((e, i) => {
            if (cartArray.includes(e._source.original_product_code)) {
              let tempArray = e;
              let cnt = cartArray.indexOf(e._source.original_product_code);
              tempArray["newQty"] = cartQtyArray[cnt].newQty;
              tempArray["quantity"] = cartQtyArray[cnt].quantity;
              tempArray["sort"] = cartQtyArray[cnt].sort;
              tempArray["product_image_urls"] =
                cartQtyArray[cnt].product_image_urls;
              tempArray["Test1"] = 11;

              newCart.push(tempArray);
            }
          });
          dispatch(updateCartItems(newCart));
        }
      })
      .catch((err) => {
        console.log(err);
      });
  };
}

import { getCookie, setCookie } from "../../components/Helper/parseCookies";
import { pincodeAction } from "./Actions";

let PincodeSectionState = {
  pincodeData: null,
  pincodeSelectedAddress: null,
  currentPage: null,
};

let cookieData = getCookie("pincodeData");
export const PincodeSectionReducer = (
  initialState = cookieData ? cookieData : PincodeSectionState,
  action
) => {
  switch (action.type) {
    case pincodeAction.PINCODE_SECTION:
      setCookie("pincodeData", {
        ...initialState,
        pincodeData: action.payload,
      });
      return {
        ...initialState,
        pincodeData: action.payload,
      };

    case pincodeAction.SELECTED_ADDRESS:
      return {
        ...initialState,
        pincodeSelectedAddress: action.payload,
      };

    case pincodeAction.SET_CURRENT_PAGE:
      return {
        ...initialState,
        currentPage: action.payload,
      };

    default:
      return initialState;
  }
};

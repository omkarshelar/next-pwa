import { CustomerService_URL } from "../../constants/Urls";
import { makeApiCall } from "../makeApiCall";

export const tmWalletActions = {
  WALLET_SUCCESS: "WALLET_SUCCESS",
  WALLET_FAILURE: "WALLET_FAILURE",
  WALLET_HISTORY_SUCCESS: "WALLET_HISTORY_SUCCESS",
  WALLET_HISTORY_FAILURE: "WALLET_HISTORY_FAILURE",
};

export function walletSuccessAction(props) {
  return (dispatch) => {
    dispatch({
      type: tmWalletActions.WALLET_SUCCESS,
      payload: props,
    });
  };
}

export function WalletFailureAction(error) {
  return {
    type: tmWalletActions.WALLET_FAILURE,
    payload: error,
  };
}

// wallet action
export function walletThunk(props) {
  const reqHeader = {
    transactionId: "a",
    "Content-Type": "application/json",
    Authorization: "Bearer " + props.access_token,
  };
  const postUrl = `${CustomerService_URL}/wallet?customerId=${props.customerId}`;
  return makeApiCall(
    postUrl,
    walletSuccessAction,
    WalletFailureAction,
    "POST",
    null,
    reqHeader,
    props.history
  );
}

export function WalletHistorySuccessAction(props) {
  return (dispatch) => {
    dispatch({
      type: tmWalletActions.WALLET_HISTORY_SUCCESS,
      payload: props,
    });
  };
}

export function WalletHistoryFailureAction(error) {
  return {
    type: tmWalletActions.WALLET_HISTORY_FAILURE,
    payload: error,
  };
}

// wallet history action
export function walletHistoryThunk(props) {
  const reqHeader = {
    transactionId: "a",
    "Content-Type": "application/json",
    Authorization: "Bearer " + props.access_token,
  };
  const postUrl = `${CustomerService_URL}/wallet/info?customerId=${props.customerId}`;
  return makeApiCall(
    postUrl,
    WalletHistorySuccessAction,
    WalletHistoryFailureAction,
    "POST",
    null,
    reqHeader,
    props.history
  );
}

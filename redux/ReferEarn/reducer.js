import { RNEActions } from "./action";

let RNEState = {
  referralLink: null,
  referralerror: null,
  redeem: null,
  redeemError: null,
  referralStatusData: null,
  referralStatusError: null,
};

export const RNEReducer = (state = RNEState, { type, payload }) => {
  switch (type) {
    case RNEActions.REFFERAL_SUCCESS:
      return {
        ...state,
        referralLink: payload,
        referralerror: null,
      };

    case RNEActions.REFFERAL_FAILURE:
      return {
        ...state,
        referralLink: null,
        referralerror: payload,
      };
    case RNEActions.REDEEM_SUCCESS:
      return {
        ...state,
        redeem: payload,
        redeemError: null,
      };

    case RNEActions.REDEEM_FAILURE:
      return {
        ...state,
        redeem: null,
        redeemError: payload,
      };

    case RNEActions.REFERRAL_STATUS_SUCCESS:
      return {
        ...state,
        referralStatusData: payload,
        referralStatusError: null,
      };

    case RNEActions.REFERRAL_STATUS_FAILURE:
      return {
        ...state,
        referralStatusData: null,
        referralStatusError: payload,
      };

    case RNEActions.closeRedeemModalPopup:
    case RNEActions.closeclaimedPopup:
    case RNEActions.closeconflictPopup:
      return {
        ...state,
        redeem: null,
      };
    case RNEActions.closerestrictPopup:
      return {
        ...state,
        redeemError: null,
      };
    default:
      return state;
  }
};

let RedeemModalStateReducer = {
  RedeemModalState: false,
  backdrop: false,
};

export const RedeemModalReducer = (
  initialState = RedeemModalStateReducer,
  action
) => {
  switch (action.type) {
    case RNEActions.redeemModal:
      return {
        ...initialState,
        RedeemModalState: true,
        backdrop: true,
      };
    case RNEActions.closeRedeemModalPopup:
      return {
        ...initialState,
        RedeemModalState: false,
        backdrop: false,
      };
    default:
      return initialState;
  }
};

let ConflictClaimReducer = {
  claimArised: false,
  conflictArised: false,
  restrictArised: false,
};

export const ConflictRedeemReducer = (
  initialState = ConflictClaimReducer,
  action
) => {
  switch (action.type) {
    case RNEActions.conflictPopup:
      return {
        ...initialState,
        conflictArised: true,
      };
    case RNEActions.closeconflictPopup:
      return {
        ...initialState,
        conflictArised: false,
      };
    case RNEActions.claimedPopup:
      return {
        ...initialState,
        claimArised: true,
      };

    case RNEActions.closeclaimedPopup:
      return {
        ...initialState,
        claimArised: false,
      };
    case RNEActions.restrictPopup:
      return {
        ...initialState,
        restrictArised: true,
      };
    case RNEActions.closerestrictPopup:
      return {
        ...initialState,
        restrictArised: false,
      };
    default:
      return initialState;
  }
};

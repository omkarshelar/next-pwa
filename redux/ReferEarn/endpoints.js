export const EndPoints = {
  Wallet: "/wallet",
  Redeem: "/redeem",
  ReferralStatus: "/getReferralStatus",
};

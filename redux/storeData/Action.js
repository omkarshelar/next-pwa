export const allActions = {
  ADD_ORDER_STATUS_DETAILS_DATA: "ADD_ORDER_STATUS_DETAILS_DATA",
  CLEAR_ORDER_STATUS_DETAILS_DATA: "CLEAR_ORDER_STATUS_DETAILS_DATA",
};

export const getOrderStatusDetailsDataAction = (props) => (dispatch) => {
  dispatch({
    type: allActions.ADD_ORDER_STATUS_DETAILS_DATA,
    payload: props,
  });
  return Promise.resolve();
};

export const clearOrderStatusDetailsDataAction = () => (dispatch) => {
  dispatch({
    type: allActions.CLEAR_ORDER_STATUS_DETAILS_DATA,
    payload: null,
  });
  return Promise.resolve();
};

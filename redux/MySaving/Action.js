import { CustomerService_URL } from "../../constants/Urls";
import { makeApiCall } from "../makeApiCall";

export const savingsActions = {
  SAVINGS_SUCCESS: "SAVINGS_SUCCESS",
  SAVINGS_FAILURE: "SAVINGS_FAILURE",
};
// Fetch My savings

export function mySavingsSuccess(props) {
  return (dispatch) => {
    dispatch({
      type: savingsActions.SAVINGS_SUCCESS,
      payload: props,
    });
  };
}

export function mySavingsError(error) {
  return {
    type: savingsActions.SAVINGS_FAILURE,
    payload: error,
  };
}

export function mySavingsThunk(props) {
  const reqHeader = {
    "Content-Type": "application/json",
    Authorization: "Bearer " + props.accessToken,
  };
  const getUrl = `${CustomerService_URL}/getCustomerDetails`;
  return makeApiCall(
    getUrl,
    mySavingsSuccess,
    mySavingsError,
    "POST",
    null,
    reqHeader,
    props.history
  );
}

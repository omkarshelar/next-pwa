import { LoginActions } from "../Login/Action";
import { savingsActions } from "./Action";

const initState = {
  savingsData: null,
  savingsError: null,
};

export const savingsReducer = (state = initState, { type, payload }) => {
  switch (type) {
    case savingsActions.SAVINGS_SUCCESS:
      return {
        ...state,
        savingsData: payload,
        savingsError: null,
      };

    case savingsActions.SAVINGS_FAILURE:
      return {
        ...state,
        savingsData: null,
        savingsError: payload,
      };
    case LoginActions.USER_LOGOUT:
      return {
        ...initState,
      };
    default:
      return state;
  }
};

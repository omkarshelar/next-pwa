import { productDetailsActions } from "./Action";

let ProductDetailState = {
  allProductDetails: null,
  errorMessage: null,
};

export const ProductDetailsReducer = (
  state = ProductDetailState,
  { type, payload }
) => {
  switch (type) {
    case productDetailsActions.FETCH_PRODUCT_DETAILS_SUCCESS:
      return {
        ...state,
        allProductDetails: payload,
        errorMessage: null,
      };

    case productDetailsActions.FETCH_PRODUCT_DETAILS_FAILURE:
      return {
        ...state,
        allProductDetails: null,
        errorMessage: payload,
      };
    default:
      return state;
  }
};

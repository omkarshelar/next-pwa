export const EndPoints = {
  Get_Order_Track_On_Home: "/getOrderTrackOnHome",
  Get_All_Patients_Order_Details: "/getAllPatientsOrderDetails",
  Confirm_Order: "/confirmOrder",
  Discard_Order: "/discardOrder",
  Order_Detail_Update: "/getOrderDetails",
};

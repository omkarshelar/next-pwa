import {
  CustomerService_URL,
  OrderManagementService_URL,
} from "../../constants/Urls";
import { makeApiCall } from "../makeApiCall";
import { EndPoints } from "./EndPoints";

export const ConfirmOrderActions = {
  CONFIRM_ORDER_SUCCESS: "CONFIRM_ORDER_SUCCESS",
  CONFIRM_ORDER_FAILURE: "CONFIRM_ORDER_FAILURE",
  DISCARD_ORDER_SUCCESS: "DISCARD_ORDER_SUCCESS",
  DISCARD_ORDER_FAILURE: "DISCARD_ORDER_FAILURE",
  CANCEL_ORDER_SUCCESS: "CANCEL_ORDER_SUCCESS",
  CANCEL_ORDER_FAILURE: "CANCEL_ORDER_FAILURE",
  FETCH_REASON_SUCCESS: "FETCH_REASON_SUCCESS",
  FETCH_REASON_FAILURE: "FETCH_REASON_FAILURE",
  FETCH_PATIENT_DETAILS_SUCCESS: "FETCH_PATIENT_DETAILS_SUCCESS",
  FETCH_PATIENT_DETAILS_FAILURE: "FETCH_PATIENT_DETAILS_FAILURE",
};

export function ConfirmOrderSuccessAction(props) {
  return (dispatch) => {
    dispatch({
      type: ConfirmOrderActions.CONFIRM_ORDER_SUCCESS,
      payload: props,
    });
  };
}

export function ConfirmOrderFailureAction(error) {
  return {
    type: ConfirmOrderActions.CONFIRM_ORDER_FAILURE,
    payload: error,
  };
}

// Confirm Order
export function confirmOrderThunk(props) {
  const reqHeader = {
    transactionId: "a",
    "Content-Type": "application/json",
    Authorization: "Bearer " + props.access_token,
  };
  const postUrl = `${CustomerService_URL}${EndPoints.Confirm_Order}?orderId=${props.orderid}&paymentId=${props.paymentid}&offerId=${props.offerId}&pincode=${props.pincode}&orderConfirmSrc=WEBSITE&sourceVersion=TM_WEBSITE_V_2.0.9`;
  return makeApiCall(
    postUrl,
    ConfirmOrderSuccessAction,
    ConfirmOrderFailureAction,
    "POST",
    null,
    reqHeader,
    props.history,
    1
  );
}

export const GoToOrderStatus = (orderid) => ({
  type: "GoToOrderStatus",
  payload: orderid,
});

export function DiscardOrderSuccessAction(props) {
  return (dispatch) => {
    dispatch({
      type: ConfirmOrderActions.DISCARD_ORDER_SUCCESS,
      payload: props,
    });
  };
}

export function DiscardOrderFailureAction(error) {
  return {
    type: ConfirmOrderActions.DISCARD_ORDER_FAILURE,
    payload: error,
  };
}

// Discard Order
export function getAllPatientsOrderDetails(props) {
  const reqHeader = {
    transactionId: "a",
    "Content-Type": "application/json",
    Authorization: "Bearer " + props.access_token,
  };
  const postUrl = `${OrderManagementService_URL}${EndPoints.Get_All_Patients_Order_Details}?orderId=${props.orderid}`;
  return makeApiCall(
    postUrl,
    FetchPatientDetailsSuccessAction,
    FetchPatientDetailsFailureAction,
    "GET",
    null,
    reqHeader,
    props.history
  );
}

export function FetchPatientDetailsSuccessAction(props) {
  return (dispatch) => {
    dispatch({
      type: ConfirmOrderActions.FETCH_PATIENT_DETAILS_SUCCESS,
      payload: props,
    });
  };
}

export function FetchPatientDetailsFailureAction(error) {
  return {
    type: ConfirmOrderActions.FETCH_PATIENT_DETAILS_FAILURE,
    payload: error,
  };
}

// Discard Order
export function discardOrderThunk(props) {
  const reqHeader = {
    transactionId: "a",
    "Content-Type": "application/json",
    Authorization: "Bearer " + props.access_token,
  };
  const postUrl = `${CustomerService_URL}${EndPoints.Discard_Order}?orderId=${props.orderid}`;
  return makeApiCall(
    postUrl,
    DiscardOrderSuccessAction,
    DiscardOrderFailureAction,
    "POST",
    null,
    reqHeader,
    props.history
  );
}

export function CancelOrderSuccessAction(props) {
  return (dispatch) => {
    dispatch({
      type: ConfirmOrderActions.CANCEL_ORDER_SUCCESS,
      payload: props,
    });
  };
}

export function CancelOrderFailureAction(error) {
  return {
    type: ConfirmOrderActions.CANCEL_ORDER_FAILURE,
    payload: error,
  };
}

// Cancel Order
export function cancelOrderThunk(props) {
  const reqHeader = {
    transactionId: "a",
    "Content-Type": "application/json",
    Authorization: "Bearer " + props.access_token,
  };
  const postUrl = `${OrderManagementService_URL}/cancelOrder?orderId=${props.orderid}&reasonId=${props.reasonId}&notes=${props.notes}`;
  return makeApiCall(
    postUrl,
    CancelOrderSuccessAction,
    CancelOrderFailureAction,
    "POST",
    null,
    reqHeader,
    props.history
  );
}

export function FetchReasonSuccessAction(props) {
  return (dispatch) => {
    dispatch({
      type: ConfirmOrderActions.FETCH_REASON_SUCCESS,
      payload: props,
    });
  };
}

export function FetchReasonFailureAction(error) {
  return {
    type: ConfirmOrderActions.FETCH_REASON_FAILURE,
    payload: error,
  };
}

// Fetch Reason List
export function fetchReasonThunkOld(props) {
  const reqHeader = {
    "Content-Type": "application/json",
    Authorization: "Bearer " + props.access_token,
    "Access-Control-Allow-Origin": "*",
  };
  const postUrl = `${OrderManagementService_URL}/getReasonsForOrderCancellation`;
  return makeApiCall(
    postUrl,
    FetchReasonSuccessAction,
    FetchReasonFailureAction,
    "POST",
    null,
    reqHeader,
    props.history
  );
}

// Fetch Reason List
export function fetchReasonThunk(props) {
  const reqHeader = {
    "Content-Type": "application/json",
    Authorization: "Bearer " + props.access_token,
    "Access-Control-Allow-Origin": "*",
  };
  const getUrl = `${OrderManagementService_URL}/getReasonsList?reasonType=CUSTOMER_PORTAL_CANCEL_REASON`;
  return makeApiCall(
    getUrl,
    FetchReasonSuccessAction,
    FetchReasonFailureAction,
    "GET",
    null,
    reqHeader,
    props.history
  );
}

import { LoginActions } from "../Login/Action";
import { ConfirmOrderActions } from "./Action";

let confirmorderState = {
  confirmation: null,
  error: null,
  discardOrder: null,
  discardErr: null,
};

export const confirmorderReducer = (
  state = confirmorderState,
  { type, payload }
) => {
  switch (type) {
    case ConfirmOrderActions.CONFIRM_ORDER_SUCCESS:
      return {
        ...state,
        confirmation: payload,
        error: null,
      };
    case ConfirmOrderActions.CONFIRM_ORDER_FAILURE:
      return {
        ...state,
        confirmation: null,
        error: payload,
      };
    case ConfirmOrderActions.DISCARD_ORDER_SUCCESS:
      return {
        ...state,
        discardOrder: payload,
        discardErr: null,
      };
    case ConfirmOrderActions.DISCARD_ORDER_FAILURE:
      return {
        ...state,
        discardOrder: null,
        discardErr: payload,
      };

    case LoginActions.USER_LOGOUT:
      return {
        ...confirmorderState,
      };

    default:
      return state;
  }
};

let cancelOrderState = {
  cancelMsg: null,
  cancelError: null,
};

export const cancelOrderReducer = (
  state = cancelOrderState,
  { type, payload }
) => {
  switch (type) {
    case ConfirmOrderActions.CANCEL_ORDER_SUCCESS:
      return {
        ...state,
        cancelMsg: payload,
        cancelError: null,
      };

    case ConfirmOrderActions.CANCEL_ORDER_FAILURE:
      return {
        ...state,
        cancelMsg: null,
        cancelError: payload,
      };

    case LoginActions.USER_LOGOUT:
      return {
        ...cancelOrderState,
      };

    default:
      return state;
  }
};

// Fetch Reason List
let fetchReasonState = {
  reasonList: null,
  reasonError: null,
};

export const fetchReasonReducer = (
  state = fetchReasonState,
  { type, payload }
) => {
  switch (type) {
    case ConfirmOrderActions.FETCH_REASON_SUCCESS:
      return {
        ...state,
        reasonList: payload,
        reasonError: null,
      };

    case ConfirmOrderActions.FETCH_REASON_FAILURE:
      return {
        ...state,
        reasonList: null,
        reasonError: payload,
      };

    case LoginActions.USER_LOGOUT:
      return {
        ...fetchReasonState,
      };

    default:
      return state;
  }
};

// Fetch Reason List
let fetchPatientDetailsState = {
  patientOrderDetails: null,
  patientOrderDetailsError: null,
};

export const fetchPatientDetailsReducer = (
  state = fetchPatientDetailsState,
  { type, payload }
) => {
  switch (type) {
    case ConfirmOrderActions.FETCH_PATIENT_DETAILS_SUCCESS:
      return {
        ...state,
        patientOrderDetails: payload,
        patientOrderDetailsError: null,
      };

    case ConfirmOrderActions.FETCH_PATIENT_DETAILS_FAILURE:
      return {
        ...state,
        patientOrderDetails: null,
        patientOrderDetailsError: payload,
      };

    default:
      return state;
  }
};

import { createStore, applyMiddleware } from "redux";
import thunkMiddleware from "redux-thunk";
import { composeWithDevTools } from "redux-devtools-extension";
import { persistStore, persistReducer } from "redux-persist";
import storage from "redux-persist/lib/storage";
import rootReducer from "./reducers/rootReducer";
import { createWrapper, HYDRATE } from "next-redux-wrapper";

const combinedReducer = rootReducer;

const reducer = (state, action) => {
  if (action.type === HYDRATE) {
    const nextState = {
      ...state, // use previous state
      ...action.payload, // apply delta from hydration
    };
    return nextState;
  } else {
    return combinedReducer(state, action);
  }
};

const makeStore = () => {
  const persistConfig = {
    key: "user",
    storage: storage,
    whitelist: [
      "loginReducer",
      "uploadImage",
      "cartData",
      "confirmMedicineReducer",
      "stepper",
      "paymentReducer",
      "recentSearch",
      "pincodeData",
      "pincodeSelectedAddress",
      "masterDetails",
      "saveMore",
      "uploadImg",
      "storeData",
    ],
  };
  const persistedReducer = persistReducer(persistConfig, reducer);
  const store = createStore(
    persistedReducer,
    composeWithDevTools(applyMiddleware(thunkMiddleware))
  );
  store.__persisitor = persistStore(store);
  return store;
};
// export an assembled wrapper
export const wrapper = createWrapper(makeStore);

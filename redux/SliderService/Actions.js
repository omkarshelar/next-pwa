import { CustomerService_URL } from "../../constants/Urls";
import { makeApiCall } from "../makeApiCall";
import { EndPoints } from "./EndPoints";

export let SliderActions = {
  ABOUT_US_SUCCESS: "ABOUT_US_SUCCESS",
  ABOUT_US_FAILURE: "ABOUT_US_FAILURE",
  MY_SUBS_SUCCESS: "MY_SUBS_SUCCESS",
  MY_SUBS_FAILURE: "MY_SUBS_FAILURE",
  MY_ORDER_SUBS_SUCCESS: "MY_ORDER_SUBS_SUCCESS",
  MY_ORDER_SUBS_FAILURE: "MY_ORDER_SUBS_FAILURE",
  TOGGLE_SIDEBAR: "TOGGLE_SIDEBAR",
  CLOSE_SIDEBAR: "CLOSE_SIDEBAR",
  GET_MASTER_DETAILS: "GET_MASTER_DETAILS",
  GET_MASTER_DETAILS_ERROR: "GET_MASTER_DETAILS_ERROR",
};

export function AboutUsSuccessAction(props) {
  return (dispatch) => {
    dispatch({
      type: SliderActions.ABOUT_US_SUCCESS,
      payload: props,
    });
  };
}

export function AboutUsFailureAction(error) {
  return {
    type: SliderActions.ABOUT_US_FAILURE,
    payload: error,
  };
}

// About Us
export function aboutUsThunk(props) {
  const reqHeader = {
    transactionId: "a",
    "Content-Type": "application/json",
  };
  const getUrl = `${CustomerService_URL}${EndPoints.About_Us}`;
  return makeApiCall(
    getUrl,
    AboutUsSuccessAction,
    AboutUsFailureAction,
    "POST",
    null,
    reqHeader,
    props.history
  );
}

export function MyOrderSubsSuccessAction(props) {
  return (dispatch) => {
    dispatch({
      type: SliderActions.MY_ORDER_SUBS_SUCCESS,
      payload: props,
    });
  };
}

export function MyOrderSubsFailureAction(error) {
  return {
    type: SliderActions.MY_ORDER_SUBS_FAILURE,
    payload: error,
  };
}

// My Order Subs
export function myOrderSubsThunk(props) {
  const reqHeader = {
    transactionId: "a",
    "Content-Type": "application/json",
    Authorization: "Bearer " + props.access_token,
  };
  const getUrl = `${CustomerService_URL}${EndPoints.Get_My_Susbstitutes_Info_By_Order_Id}?orderId=${props.orderid}`;
  return makeApiCall(
    getUrl,
    MyOrderSubsSuccessAction,
    MyOrderSubsFailureAction,
    "GET",
    null,
    reqHeader,
    props.history
  );
}

export function MySubsSuccessAction(props) {
  return (dispatch) => {
    dispatch({
      type: SliderActions.MY_SUBS_SUCCESS,
      payload: props,
    });
  };
}

export function MySubsFailureAction(error) {
  return {
    type: SliderActions.MY_SUBS_FAILURE,
    payload: error,
  };
}

// My Subs
export function mySubsThunk(props) {
  const reqHeader = {
    transactionId: "a",
    "Content-Type": "application/json",
    Authorization: "Bearer " + props.access_token,
    "Access-Control-Allow-Origin": "*",
  };
  const getUrl = `${CustomerService_URL}${EndPoints.Get_Customer_Order_Listing}`;
  return makeApiCall(
    getUrl,
    MySubsSuccessAction,
    MySubsFailureAction,
    "GET",
    null,
    reqHeader,
    props.history
  );
}

// TOGGLE SIDEBAR

export const toggleSidebarAction = () => ({
  type: SliderActions.TOGGLE_SIDEBAR,
});

export function getMasterDetailsSuccessAction(props) {
  return (dispatch) => {
    dispatch({
      type: SliderActions.GET_MASTER_DETAILS,
      payload: props,
    });
  };
}

export function getMasterDetailsFailureAction(error) {
  return {
    type: SliderActions.GET_MASTER_DETAILS_ERROR,
    payload: error,
  };
}

//New Service
export function getMasterDetailsThunkNew(props) {
  let reqHeader = {
    transactionId: "a",
    "Content-Type": "application/json",
    "Access-Control-Allow-Origin": "*",
  };

  let getUrl = `${CustomerService_URL}/getCustomerDeliveryChargeData`;
  if (props?.accessToken) {
    reqHeader = {
      transactionId: "a",
      "Content-Type": "application/json",
      Authorization: "Bearer " + props.accessToken,
    };
    if (props.customerId) {
      getUrl += `?customerId=${props.customerId}`;
    }
  }
  return makeApiCall(
    getUrl,
    getMasterDetailsSuccessAction,
    getMasterDetailsFailureAction,
    "GET",
    null,
    reqHeader,
    props.history
  );
}

// OLD service
export function getMasterDetailsThunk(props) {
  const reqHeader = {
    transactionId: "a",
    "Content-Type": "application/json",
    "Access-Control-Allow-Origin": "*",
  };
  const getUrl = `${CustomerService_URL}/mobileMaster`;
  return makeApiCall(
    getUrl,
    getMasterDetailsSuccessAction,
    getMasterDetailsFailureAction,
    "POST",
    null,
    reqHeader,
    props.history
  );
}

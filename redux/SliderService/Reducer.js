import { SliderActions } from "./Actions";

let AboutusState = {
  aboutUs: null,
  error: null,
};

export const AboutusReducer = (state = AboutusState, { type, payload }) => {
  switch (type) {
    case SliderActions.ABOUT_US_SUCCESS:
      return { ...state, aboutUs: payload, error: null };

    case SliderActions.ABOUT_US_FAILURE:
      return { ...state, aboutUs: null, error: payload };

    default:
      return state;
  }
};

let MySubsState = {
  MySubs: null,
  error: null,
  MySubsOrder: null,
  MySubsOrdererror: null,
};

export const MySubsReducer = (state = MySubsState, { type, payload }) => {
  switch (type) {
    case SliderActions.MY_SUBS_SUCCESS:
      return { ...state, MySubs: payload, error: null };

    case SliderActions.MY_SUBS_FAILURE:
      return { ...state, MySubs: null, error: payload };

    case SliderActions.MY_ORDER_SUBS_SUCCESS:
      return {
        ...state,
        MySubsOrder: payload,
        MySubsOrdererror: null,
      };

    case SliderActions.MY_ORDER_SUBS_FAILURE:
      return {
        ...state,
        MySubsOrder: null,
        MySubsOrdererror: payload,
      };

    case SliderActions.GET_MASTER_DETAILS:
      return {
        ...state,
        MasterDetails: payload,
        MasterDetailsError: null,
      };

    case SliderActions.GET_MASTER_DETAILS_ERROR:
      return {
        ...state,
        MasterDetails: null,
        MasterDetailsError: payload,
      };

    default:
      return state;
  }
};

let MasterState = {
  MasterDetails: null,
  MasterDetailsError: null,
};

export const MasterReducer = (state = MasterState, { type, payload }) => {
  switch (type) {
    case SliderActions.GET_MASTER_DETAILS:
      return {
        ...state,
        MasterDetails: payload,
        MasterDetailsError: null,
      };

    case SliderActions.GET_MASTER_DETAILS_ERROR:
      return {
        ...state,
        MasterDetails: null,
        MasterDetailsError: payload,
      };

    default:
      return state;
  }
};

let sidebarState = {
  toggleSidebar: false,
  backdrop: false,
};

export const sidebarReducer = (state = sidebarState, { type }) => {
  switch (type) {
    case SliderActions.TOGGLE_SIDEBAR:
      return {
        ...state,
        toggleSidebar: !state.toggleSidebar,
        backdrop: !state.backdrop,
      };
    default:
      return state;
  }
};

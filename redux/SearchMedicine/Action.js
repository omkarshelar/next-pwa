import Axios from "axios";
import { CustomerService_URL } from "../../constants/Urls";

export const searchMedicineActions = {
  FETCH_MEDICINE_SUCCESS: "FETCH_MEDICINE_SUCCESS",
  FETCH_MEDICINE_FAILURE: "FETCH_MEDICINE_FAILURE",
  FETCH_MEDICINE_NEW_SUCCESS: "FETCH_MEDICINE_NEW_SUCCESS",
  FETCH_MEDICINE_NEW_FAILURE: "FETCH_MEDICINE_NEW_FAILURE",
  FETCH_MEDICINE_WITH_IMG_SUCCESS: "FETCH_MEDICINE_WITH_IMG_SUCCESS",
  FETCH_MEDICINE_WITH_IMG_FAILURE: "FETCH_MEDICINE_WITH_IMG_FAILURE",
};

export function fetchMedicineSuccess(props) {
  return (dispatch) => {
    dispatch({
      type: searchMedicineActions.FETCH_MEDICINE_SUCCESS,
      payload: props,
    });
  };
}

export function fetchMedicineError(error) {
  return {
    type: searchMedicineActions.FETCH_MEDICINE_FAILURE,
    payload: error,
  };
}

export function fetchMedicineThunk(props) {
  return async (dispatch) => {
    const getUrl = `${CustomerService_URL}/callElasticSearch?warehouseId=${props.warehouseId}`;
    const config = {
      headers: {
        Authorization: "Bearer " + props.access_token,
      },
    };
    await Axios.post(getUrl, props.medicine, props.access_token ? config : null)
      .then((response) => {
        if (response.status === 200) {
          dispatch(
            fetchMedicineSuccess({
              hits: response?.data?.hits?.hits,
              suggestion: response?.data?.suggest?.skuNameSuggester
                ? response.data.suggest.skuNameSuggester
                : null,
            })
          );
        }
      })
      .catch((err) => {
        dispatch(fetchMedicineError(err.response));
      });
  };
}

export function fetchMedicineSuccessOnSearch(props) {
  return (dispatch) => {
    dispatch({
      type: searchMedicineActions.FETCH_MEDICINE_NEW_SUCCESS,
      payload: props,
    });
  };
}

export function fetchMedicineErrorOnSearch(error) {
  return {
    type: searchMedicineActions.FETCH_MEDICINE_NEW_FAILURE,
    payload: error,
  };
}

export function fetchMedicineThunkOnSearch(props) {
  return (dispatch) => {
    let getUrl = `${CustomerService_URL}/fetchFromElasticSearch?warehouseId=${props.warehouseId}`;
    if (props.elasticSearchType) {
      getUrl += `&elasticSearchType=${props.elasticSearchType}`;
    }
    if (props.searchString) {
      getUrl += `&searchString=${encodeURIComponent(props.searchString)}`;
    }
    if (props.isMultiSearch != undefined) {
      getUrl += `&isMultiSearch=${props.isMultiSearch}`;
    }
    const config = {
      headers: {
        Authorization: "Bearer " + props.access_token,
      },
    };
    return Axios.get(getUrl, props.access_token ? config : null)
      .then((response) => {
        if (response.status === 200) {
          let motherBrandHits = [];
          let tempMBArray = [];
          if (props.elasticSearchType === "SKU_BRAND_SEARCH") {
            if (response.data.responses[0].hits.hits.length > 0) {
              response.data.responses[0].hits.hits.map((e) => {
                if (!tempMBArray.includes(e._source.original_mother_brand)) {
                  tempMBArray.push(e._source.original_mother_brand);
                  motherBrandHits.push(e);
                }
              });
            }
            dispatch(
              fetchMedicineSuccessOnSearch({
                motherBrandHits: motherBrandHits,
                hits: response.data.responses[1].hits.hits,
                suggestion: response.data.responses[1].suggest.skuNameSuggester,
              })
            );
          } else {
            dispatch(
              fetchMedicineSuccessOnSearch({
                hits: response.data.hits.hits,
                suggestion: response.data.suggest.skuNameSuggester,
              })
            );
          }
        }
      })
      .catch((err) => {
        dispatch(
          fetchMedicineErrorOnSearch(
            "Something went wrong...Please try again in sometime / Re-login and try again"
          )
        );
      });
  };
}

export function fetchMedicineWithImagesSuccess(props) {
  return (dispatch) => {
    dispatch({
      type: searchMedicineActions.FETCH_MEDICINE_WITH_IMG_SUCCESS,
      payload: props,
    });
  };
}

export function fetchMedicineWithImagesError(error) {
  return {
    type: searchMedicineActions.FETCH_MEDICINE_WITH_IMG_FAILURE,
    payload: error,
  };
}

export function fetchMedicineWithImagesThunk(props) {
  return async (dispatch) => {
    const getUrl = `${CustomerService_URL}/callElasticSearch?warehouseId=${props.warehouseId}`;
    const config = {
      headers: {
        Authorization: "Bearer " + props.access_token,
      },
    };
    await Axios.post(getUrl, props.medicine, props.access_token ? config : null)
      .then((response) => {
        if (response.status === 200) {
          dispatch(
            fetchMedicineWithImagesSuccess({
              hits: response?.data?.hits?.hits,
              suggestion: response?.data?.suggest?.skuNameSuggester
                ? response.data.suggest.skuNameSuggester
                : null,
            })
          );
        }
      })
      .catch((err) => {
        dispatch(fetchMedicineWithImagesError(err.response));
      });
  };
}

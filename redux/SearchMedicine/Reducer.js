import { searchMedicineActions } from "./Action";

const initState = {
  medDataSuccess: null,
  medDataError: null,
  medDataSearchSuccess: null,
  medDataSearchFailure: null,
};

export const searchMedicineReducer = (state = initState, { type, payload }) => {
  switch (type) {
    case searchMedicineActions.FETCH_MEDICINE_SUCCESS:
      return {
        ...state,
        medDataSuccess: payload,
        medDataError: null,
      };

    case searchMedicineActions.FETCH_MEDICINE_FAILURE:
      return {
        ...state,
        medDataSuccess: null,
        medDataError: payload,
      };
    case searchMedicineActions.FETCH_MEDICINE_NEW_SUCCESS:
      return {
        ...state,
        medDataSearchSuccess: payload,
        medDataSearchFailure: null,
      };

    case searchMedicineActions.FETCH_MEDICINE_NEW_FAILURE:
      return {
        ...state,
        medDataSearchSuccess: null,
        medDataSearchFailure: payload,
      };

    case searchMedicineActions.FETCH_MEDICINE_WITH_IMG_SUCCESS:
      return {
        ...state,
        medDataWithImgSuccess: payload,
        medDataWithImgError: null,
      };

    case searchMedicineActions.FETCH_MEDICINE_WITH_IMG_FAILURE:
      return {
        ...state,
        medDataWithImgSuccess: null,
        medDataWithImgError: payload,
      };

    default:
      return state;
  }
};

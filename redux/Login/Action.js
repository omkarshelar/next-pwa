import { CustomerService_URL } from "../../constants/Urls";
import { makeApiCall } from "../makeApiCall";

export const LoginActions = {
  FETCH_SESSION_TOKEN_SUCCESS: "FETCH_SESSION_TOKEN_SUCCESS",
  FETCH_SESSION_TOKEN_FAILURE: "FETCH_SESSION_TOKEN_FAILURE",
  FETCH_DEFAULT_TOKEN_SUCCESS: "FETCH_DEFAULT_TOKEN_SUCCESS",
  FETCH_DEFAULT_TOKEN_FAILURE: "FETCH_DEFAULT_TOKEN_FAILURE",
  GENERATE_OTP_SUCCESS: "GENERATE_OTP_SUCCESS",
  GENERATE_OTP_FAILURE: "GENERATE_OTP_FAILURE",
  VERIFY_OTP_SUCCESS: "VERIFY_OTP_SUCCESS",
  VERIFY_OTP_FAILURE: "VERIFY_OTP_FAILURE",
  USER_LOGOUT: "USER_LOGOUT",
  FETCH_CUSTOMER_DETAILS: "FETCH_CUSTOMER_DETAILS",
  FETCH_CUSTOMER_DETAILS_FAILURE: "FETCH_CUSTOMER_DETAILS_FAILURE",
  UPDATE_CUSTOMER_DETAILS: "UPDATE_CUSTOMER_DETAILS",
  UPDATE_CUSTOMER_DETAILS_FAILURE: "UPDATE_CUSTOMER_DETAILS_FAILURE",
  CHECK_PP_OR_TNC_STATUS: "CHECK_PP_OR_TNC_STATUS",
  CHECK_PP_OR_TNC_STATUS_FAILURE: "CHECK_PP_OR_TNC_STATUS_FAILURE",
  ACCEPT_PP_OR_TNC: "ACCEPT_PP_OR_TNC",
  ACCEPT_PP_OR_TNC_FAILURE: "ACCEPT_PP_OR_TNC_FAILURE",
};

// Fetch session token from fcm token before generating OTP

export function fetchSessionTokenSuccess(props) {
  return (dispatch) => {
    dispatch({
      type: LoginActions.FETCH_SESSION_TOKEN_SUCCESS,
      payload: props,
    });
  };
}

export function fetchSessionTokenError(error) {
  return {
    type: LoginActions.FETCH_SESSION_TOKEN_FAILURE,
    payload: error,
  };
}

export function fetchSessionTokenThunk(props) {
  let reqHeader = {
    "Content-Type": "application/json",
  };
  const postUrl = `${CustomerService_URL}/fetchSessionToken`;
  return makeApiCall(
    postUrl,
    fetchSessionTokenSuccess,
    fetchSessionTokenError,
    "POST",
    // fcmToken, client (TM_WEBSITE_V_2.0.9)
    props.sessionTokenReq,
    reqHeader,
    props.history
  );
}

// Fetch fcm token from default token

export function fetchDefaultTokenSuccess(props) {
  return (dispatch) => {
    dispatch({
      type: LoginActions.FETCH_DEFAULT_TOKEN_SUCCESS,
      payload: props,
    });
  };
}

export function fetchDefaultTokenError(error) {
  return {
    type: LoginActions.FETCH_DEFAULT_TOKEN_FAILURE,
    payload: error,
  };
}

export function fetchDefaultTokenThunk(props) {
  let reqHeader = {
    "Content-Type": "application/json",
  };
  const postUrl = `${CustomerService_URL}/fetchDefaultToken`;
  return makeApiCall(
    postUrl,
    fetchDefaultTokenSuccess,
    fetchDefaultTokenError,
    "POST",
    // default token
    props.defaultTokenRequest,
    reqHeader,
    props.history
  );
}

// Send OTP To user.

export function generateOtpSuccess(props) {
  return (dispatch) => {
    dispatch({
      type: LoginActions.GENERATE_OTP_SUCCESS,
      payload: props,
    });
  };
}

export function generateOtpError(error) {
  return {
    type: LoginActions.GENERATE_OTP_FAILURE,
    payload: error,
  };
}

export function generateOtpThunk(props) {
  let reqHeader = {
    transactionId: "a",
    "Content-Type": "application/json",
  };
  const postUrl = `${CustomerService_URL}/sendMobileOtp`;
  return makeApiCall(
    postUrl,
    generateOtpSuccess,
    generateOtpError,
    "POST",
    // mobileNum, sessionToken
    props.mobileOtpRequest,
    reqHeader,
    props.history
  );
}

// export function generateOtpThunk(props) {
//   return async (dispatch) => {
//     const postUrl = `${CustomerService_URL}/sendOtp?mobileNo=${props.mobileNo}`;
//     let reqHeader = {
//       transactionId: "a",
//       "Content-Type": "application/json",
//     };
//     let axiosBody = {
//       method: "POST",
//       url: postUrl,
//       data: null,
//       headers: reqHeader,
//     };
//     return Axios(axiosBody)
//       .then((response) => {
//         if (response.status === 200) {
//           dispatch(generateOtpSuccess(response.data));
//         }
//       })
//       .catch((err) => {
//         if (err.response) {
//           dispatch(generateOtpError("Something went wrong"));
//         } else {
//           dispatch(generateOtpError("Something went wrong"));
//         }
//       });
//   };
// }

// Verify OTP

export function verifyOtpSuccess(props) {
  return (dispatch) => {
    dispatch({
      type: LoginActions.VERIFY_OTP_SUCCESS,
      payload: props,
    });
  };
}

export function verifyOtpError(error) {
  return {
    type: LoginActions.VERIFY_OTP_FAILURE,
    payload: error,
  };
}

export function verifyOtpThunk(props) {
  const reqHeader = {
    transactionId: "a",
  };
  const getUrl = `${CustomerService_URL}/verifyOtp?mobileNo=${props.mobileNo}&otp=${props.otp}&deviceKey=abcd&isIos=false&source=TM_WEBSITE_V_2.0.9`;
  return makeApiCall(
    getUrl,
    verifyOtpSuccess,
    verifyOtpError,
    "POST",
    null,
    reqHeader,
    props.history
  );
}

// Logout action.

export const LogoutAction = () => ({
  type: LoginActions.USER_LOGOUT,
});

// fetch customer details
export function fetchCustomerDetailsSuccess(props) {
  return (dispatch) => {
    dispatch({
      type: LoginActions.FETCH_CUSTOMER_DETAILS,
      payload: props,
    });
  };
}

export function fetchCustomerDetailsError(error) {
  return {
    type: LoginActions.FETCH_CUSTOMER_DETAILS_FAILURE,
    payload: error,
  };
}

export function fetchCustomerDetailsThunk(props) {
  const reqHeader = {
    "Content-Type": "application/json",
    transactionId: "a",
    Authorization: "Bearer " + props.access_token,
  };
  const postUrl = `${CustomerService_URL}/getCustomerDetails?customerId=${props.customerId}`;
  return makeApiCall(
    postUrl,
    fetchCustomerDetailsSuccess,
    fetchCustomerDetailsError,
    "POST",
    null,
    reqHeader,
    props.history
  );
}

// Update customer details
export function updateCustomerDetailsSuccess(props) {
  return (dispatch) => {
    dispatch({
      type: LoginActions.UPDATE_CUSTOMER_DETAILS,
      payload: props,
    });
  };
}

export function updateCustomerDetailsError(error) {
  return {
    type: LoginActions.UPDATE_CUSTOMER_DETAILS_FAILURE,
    payload: error,
  };
}

export function updateCustomerDetailsThunk(props) {
  const reqHeader = {
    "Content-Type": "application/json",
    transactionId: "a",
    Authorization: "Bearer " + props.access_token,
  };
  const postUrl = `${CustomerService_URL}/updateProfile`;
  return makeApiCall(
    postUrl,
    updateCustomerDetailsSuccess,
    updateCustomerDetailsError,
    "POST",
    props,
    reqHeader,
    props.history
  );
}

// check if customer accepted privacy policy and terms & conditions
export function checkIfCustomerAcceptedPPAndTNCSuccess(props) {
  return (dispatch) => {
    dispatch({
      type: LoginActions.CHECK_PP_OR_TNC_STATUS,
      payload: props,
    });
  };
}

export function checkIfCustomerAcceptedPPAndTNCError(error) {
  return {
    type: LoginActions.CHECK_PP_OR_TNC_STATUS_FAILURE,
    payload: error,
  };
}

export function checkIfCustomerAcceptedPPAndTNCThunk(props) {
  const reqHeader = {
    "Content-Type": "application/json",
    transactionId: "a",
    Authorization: "Bearer " + props.access_token,
  };
  const getUrl = `${CustomerService_URL}/checkIfCustomerAcceptedPPAndTNC?customerId=${props.customerId}&transactionId=aa`;
  return makeApiCall(
    getUrl,
    checkIfCustomerAcceptedPPAndTNCSuccess,
    checkIfCustomerAcceptedPPAndTNCError,
    "GET",
    null,
    reqHeader,
    props.history
  );
}

//To accept customer privacy policy and terms and conditions
// value for type is TERMS_AND_CONDITIONS or PRIVACY_AND_POLICY
export function acceptPPOrTNCSuccess(props) {
  return (dispatch) => {
    dispatch({
      type: LoginActions.ACCEPT_PP_OR_TNC,
      payload: props,
    });
  };
}

export function acceptPPOrTNCError(error) {
  return {
    type: LoginActions.ACCEPT_PP_OR_TNC_FAILURE,
    payload: error,
  };
}

export function acceptPPOrTNCThunk(props) {
  const reqHeader = {
    "Content-Type": "application/json",
    transactionId: "a",
    Authorization: "Bearer " + props.access_token,
  };
  let type = "";
  switch (props.type) {
    case 1:
      type = "PRIVACY_AND_POLICY";
      break;
    case 2:
      type = "TERMS_AND_CONDITIONS";
      break;
    default:
      acceptPPOrTNCError();
      break;
  }
  const postUrl = `${CustomerService_URL}/acceptPPOrTNC?customerId=${props.customerId}&type=${type}&transactionId=aa`;
  return makeApiCall(
    postUrl,
    acceptPPOrTNCSuccess,
    acceptPPOrTNCError,
    "POST",
    null,
    reqHeader,
    props.history
  );
}

// Actions For Fetching Medicine Data.

export const MedActions = {
  FetchMedStart: "Fetch_Med_Start",
  FetchMedSuccess: "Fetch_Med_Success",
  FetchMedFailure: "Fetch_Med_Failure",
  MedResultSuccess: "MedResultSuccess",
  MedResultFailure: "MedResultFailure",
  FetchMedStartUpdate: "FetchMedStartUpdate",
  FetchMedSuccessUpdate: "FetchMedSuccessUpdate",
  FetchMedFailureUpdate: "FetchMedFailureUpdate",
  RemoveMedStart: "RemoveMedStart",
  RemoveMedSuccess: "RemoveMedSuccess",
  RemoveMedFailure: "RemoveMedFailure",
};

export const FetchMedStartActionUpdate = (data) => ({
  type: MedActions.FetchMedStartUpdate,
  payload: data,
});

export const FetchMedSuccessActionUpdate = (data) => ({
  type: MedActions.FetchMedSuccessUpdate,
  payload: data,
});

export const FetchMedFailureActionUpdate = (error) => ({
  type: MedActions.FetchMedFailureUpdate,
  payload: error,
});

export const MedResultSuccessAction = (data) => ({
  type: MedActions.MedResultSuccess,
  payload: data,
});

export const MedResultFailureAction = () => ({
  type: MedActions.MedResultFailure,
});

// remove med
export const RemoveMedStartAction = (data) => ({
  type: MedActions.RemoveMedStart,
  payload: data,
});

export const RemoveMedSuccessAction = (data) => ({
  type: MedActions.RemoveMedSuccess,
  payload: data,
});

export const RemoveMedFailureAction = (error) => ({
  type: MedActions.RemoveMedFailure,
  payload: error,
});

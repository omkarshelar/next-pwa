import { CustomerService_URL } from "../../constants/Urls";
import { makeApiCall } from "../makeApiCall";

export const bannerAction = {
  FETCH_BANNER_FULFILLED: "FETCH_BANNER_FULFILLED",
  FETCH_BANNER_REJECTED: "FETCH_BANNER_REJECTED",
};

export function bannerSuccess(props) {
  return (dispatch) => {
    dispatch({
      type: bannerAction.FETCH_BANNER_FULFILLED,
      payload: props,
    });
  };
}

export function bannerFailure(error) {
  return {
    type: bannerAction.FETCH_BANNER_REJECTED,
    payload: error,
  };
}

export function getBanners(props) {
  const getUrl = `${CustomerService_URL}/getBannersForCustomer`;
  return makeApiCall(
    getUrl,
    bannerSuccess,
    bannerFailure,
    "GET",
    props.history
  );
}

export function getBannersThunk(props) {
  const getUrl = `${CustomerService_URL}/getBanners?type=website`;
  return makeApiCall(
    getUrl,
    bannerSuccess,
    bannerFailure,
    "GET",
    props.history
  );
}

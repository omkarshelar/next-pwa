// Test Separator Function
const separatorCartUTIL = (urlArray) => {
  let temparray = [];
  let one = urlArray.find((data) => {
    let lastIndexOfSlash = data.lastIndexOf("/");
    return data.substring(lastIndexOfSlash + 1).includes("_1");
  });

  let two = urlArray.find((data) => {
    let lastIndexOfSlash = data.lastIndexOf("/");
    return data.substring(lastIndexOfSlash + 1).includes("_2");
  });
  let three = urlArray.find((data) => {
    let lastIndexOfSlash = data.lastIndexOf("/");
    return data.substring(lastIndexOfSlash + 1).includes("_3");
  });
  let four = urlArray.find((data) => {
    let lastIndexOfSlash = data.lastIndexOf("/");
    return data.substring(lastIndexOfSlash + 1).includes("_4");
  });
  if (one) {
    temparray.push(one);
  }
  if (two) {
    temparray.push(two);
  }
  if (three) {
    temparray.push(three);
  }
  if (four) {
    temparray.push(four);
  }
  return temparray;
};

export const addItemToCart = (cartItems, cartItemToAdd) => {
  const existingCartItem = cartItems.find(
    (item) =>
      item._source.original_product_code ===
      cartItemToAdd._source.original_product_code
  );
  if (existingCartItem) {
    return cartItems.map((item) =>
      item._source.original_product_code ===
      cartItemToAdd._source.original_product_code
        ? { ...item, quantity: cartItemToAdd.newQty }
        : item
    );
  }

  return [
    ...cartItems,
    {
      ...cartItemToAdd,
      quantity: 1,
      // Update Test
      product_image_urls: cartItemToAdd._source.product_image_urls
        ? cartItemToAdd._source.product_image_urls.split(",").length > 0
          ? separatorCartUTIL(
              cartItemToAdd._source.product_image_urls.split(",")
            )
          : cartItemToAdd._source.product_image_urls
        : cartItemToAdd._source.product_image_urls,
    },
  ];
};

function calcSavingsPercent(subs_selling_price, original_mrp) {
  let a = 100 - (subs_selling_price / original_mrp) * 100;
  return `${a.toFixed(1)}%`;
}

export const addItemOfIncompleteOrder = (cartItems) => {
  let modifyArray = cartItems.map((item) => {
    return {
      _source: {
        original_available: item.orgAvailable,
        original_base_discount: item.orgDiscount,
        original_company_nm: item.orgCompanyName,
        original_sku_name: item.orgMedName,
        original_mrp: item.orgMrp / item.orgQuantity,
        original_supplied_bytm: true,
        subs_found: item.statusId === 62 ? false : true,
        subs_selling_price: item.subsSellingPrice / item.orgQuantity,
        original_product_code: item.orgProductCd,
        subs_product_code: item.subsProductCd,
        subs_mrp: item.subsQuantity
          ? item.subsMrp / item.subsQuantity
          : item.subsMrp / item.orgQuantity,
        subs_sku_name: item.subsMedName,
        sub_recommended_qty: item.subsQuantity / item.orgQuantity,
        subs_company_nm: item.subsCompanyName,
        savings_percentage: calcSavingsPercent(
          item.subsSellingPrice / item.orgQuantity,
          item.orgMrp / item.orgQuantity
        ),
      },
      quantity: item.orgQuantity,
    };
  });
  return modifyArray;
};

export const decreaseQuantity = (cartItems, cartItemToRemove) => {
  const existingCartItem = cartItems.find(
    (item) =>
      item._source.original_product_code ===
      cartItemToRemove._source.original_product_code
  );

  if (existingCartItem.quantity <= 1) {
    return cartItems.filter(
      (item) =>
        item._source.original_product_code !==
        cartItemToRemove._source.original_product_code
    );
  }

  return cartItems.map((item) =>
    item._source.original_product_code ===
    cartItemToRemove._source.original_product_code
      ? { ...item, quantity: item.quantity - 1 }
      : item
  );
};

export const qtyArr = [
  1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15, 16, 17, 18, 19, 20,
];

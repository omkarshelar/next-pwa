export const stepperAction = {
  TO_CART_SECTION: "TO_CART_SECTION",
  TO_UPLOAD_PRESCRIPTION_SECTION: "TO_UPLOAD_PRESCRIPTION_SECTION",
  TO_PATIENT_DETAILS_SECTION: "TO_PATIENT_DETAILS_SECTION",
  TO_ADDRESS_DETAILS_SECTION: "TO_ADDRESS_DETAILS_SECTION",
  TO_ORDER_SUMMARY_SECTION: "TO_ORDER_SUMMARY_SECTION",
  ONLY_ADDRESS_DONE: "ONLY_ADDRESS_DONE",
  CLEAR_SECTION: "CLEAR_SECTION",
  NOT_ON_ORDER_SUMMARY: "NOT_ON_ORDER_SUMMARY",
};

export const toCartSectionAction = () => ({
  type: stepperAction.TO_CART_SECTION,
});

export const toUploadPrescriptionAction = (data) => ({
  type: stepperAction.TO_UPLOAD_PRESCRIPTION_SECTION,
  payload: data,
});

export const toPatientDetailsAction = () => ({
  type: stepperAction.TO_PATIENT_DETAILS_SECTION,
});

export const toAddressDetailsAction = (data) => ({
  type: stepperAction.TO_ADDRESS_DETAILS_SECTION,
  payload: data,
});

export const toOrderSummaryAction = () => ({
  type: stepperAction.TO_ORDER_SUMMARY_SECTION,
});

export const AddressDone = () => ({
  type: stepperAction.ONLY_ADDRESS_DONE,
});

export const clearSectionAction = () => ({
  type: stepperAction.CLEAR_SECTION,
});

export const notOnOrderSummaryAction = () => ({
  type: stepperAction.NOT_ON_ORDER_SUMMARY,
});

import { CustomerService_URL } from "../../constants/Urls";
import { makeApiCall } from "../makeApiCall";
import { EndPoints } from "./EndPoints";

export const HelpActions = {
  HELP_SUCCESS: "HELP_SUCCESS",
  HELP_FAILURE: "HELP_FAILURE",
  HELP_CATEGORY_SUCCESS: "HELP_CATEGORY_SUCCESS",
  HELP_CATEGORY_FAILURE: "HELP_CATEGORY_FAILURE",
  HELP_DETAILS_SUCCESS: "HELP_DETAILS_SUCCESS",
  HELP_DETAILS_FAILURE: "HELP_DETAILS_FAILURE",
  RAISE_MED_ISSUE_SUCCESS: "RAISE_MED_ISSUE_SUCCESS",
  RAISE_MED_ISSUE_FAILURE: "RAISE_MED_ISSUE_FAILURE",
};

export function HelpSuccessAction(props) {
  return (dispatch) => {
    dispatch({
      type: HelpActions.HELP_SUCCESS,
      payload: props,
    });
  };
}

export function HelpFailureAction(error) {
  return {
    type: HelpActions.HELP_FAILURE,
    payload: error,
  };
}

// Fetch Help category name
export function helpThunk(props) {
  const reqHeader = {
    transactionId: "a",
    "Content-Type": "application/json",
  };
  const postUrl = `${CustomerService_URL}${EndPoints.Get_Order_Issues}`;
  return makeApiCall(
    postUrl,
    HelpSuccessAction,
    HelpFailureAction,
    "POST",
    null,
    reqHeader,
    props.history
  );
}

export function HelpCategorySuccessAction(props) {
  return (dispatch) => {
    dispatch({
      type: HelpActions.HELP_CATEGORY_SUCCESS,
      payload: props,
    });
  };
}

export function HelpCategoryFailureAction(error) {
  return {
    type: HelpActions.HELP_CATEGORY_FAILURE,
    payload: error,
  };
}

// Fetch Help Category
export function helpCategoryThunk(history) {
  const reqHeader = {
    transactionId: "a",
    "Content-Type": "application/json",
  };
  const postUrl = `${CustomerService_URL}${EndPoints.Get_Help_Category}`;
  return makeApiCall(
    postUrl,
    HelpCategorySuccessAction,
    HelpCategoryFailureAction,
    "POST",
    null,
    reqHeader,
    history
  );
}

export function HelpDetailsSuccessAction(props) {
  return (dispatch) => {
    dispatch({
      type: HelpActions.HELP_DETAILS_SUCCESS,
      payload: props,
    });
  };
}

export function HelpDetailsFailureAction(error) {
  return {
    type: HelpActions.HELP_DETAILS_FAILURE,
    payload: error,
  };
}

// Fetch Help Category Details
export function helpDetailsThunk(props) {
  const reqHeader = {
    transactionId: "a",
    "Content-Type": "application/json",
  };
  const postUrl = `${CustomerService_URL}${EndPoints.Get_Help_Category_Details}?id=${props.id}`;
  return makeApiCall(
    postUrl,
    HelpDetailsSuccessAction,
    HelpDetailsFailureAction,
    "POST",
    null,
    reqHeader,
    props.history
  );
}

export function RaiseMedIssueSuccessAction(props) {
  return (dispatch) => {
    dispatch({
      type: HelpActions.RAISE_MED_ISSUE_SUCCESS,
      payload: props,
    });
  };
}

export function RaiseMedIssueFailureAction(error) {
  return {
    type: HelpActions.RAISE_MED_ISSUE_FAILURE,
    payload: error,
  };
}

// Fetch Help Category Details
export function raiseMedIssueThunk(props) {
  const reqHeader = {
    transactionId: "a",
    "Content-Type": "application/json",
    Authorization: "Bearer " + props.access_token,
  };
  const postUrl = `${CustomerService_URL}/getAllIssueMedicines?orderId=${props.orderId}`;
  return makeApiCall(
    postUrl,
    RaiseMedIssueSuccessAction,
    RaiseMedIssueFailureAction,
    "POST",
    null,
    reqHeader,
    props.history
  );
}

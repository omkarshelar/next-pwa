export const EndPoints={
    Get_Order_Issues:'/getOrderIssues',
    Get_Help_Category:'/getHelpCategory',
    Get_Help_Category_Details:'/getHelpCategoryDetails'
}
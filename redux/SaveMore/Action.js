export const saveMoreAction = {
  SET_SAVE_MORE: "SET_SAVE_MORE",
  RESET_SAVE_MORE: "RESET_SAVE_MORE",
};

export const setSaveMoreAction = (data) => ({
  type: saveMoreAction.SET_SAVE_MORE,
  payload: "",
});
export const resetSaveMoreAction = (data) => ({
  type: saveMoreAction.RESET_SAVE_MORE,
  payload: "",
});

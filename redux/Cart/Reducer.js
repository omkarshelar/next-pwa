import {
  addItemOfIncompleteOrder,
  addItemToCart,
  decreaseQuantity,
} from "./Util/CartUtil";
import { cartActions } from "./Action";
import { ConfirmOrderActions } from "../ConfirmOrder/Action";
import { LoginActions } from "../Login/Action";

let Initial_State = {
  cartItems: [],
  couponDetails: [],
  appliedCoupon: {},
};

export const cartReducer = (state = Initial_State, action) => {
  switch (action.type) {
    case cartActions.ADD_ITEM_CART:
      return {
        ...state,
        cartItems: addItemToCart(state.cartItems, action.payload),
      };

    case cartActions.UPDATE_CART_ITEM:
      return {
        ...state,
        cartItems: action.payload,
      };

    case cartActions.ADD_INCOMPLETE_ORDER_MEDS_ADD:
      return {
        ...state,
        cartItems: addItemOfIncompleteOrder(action.payload),
      };

    case cartActions.REMOVE_QUANTITY:
      return {
        ...state,
        cartItems: decreaseQuantity(state.cartItems, action.payload),
      };

    case cartActions.REMOVE_CART_ITEM:
      return {
        ...state,
        cartItems: state.cartItems.filter(
          (item) =>
            item._source.original_product_code !==
            action.payload._source.original_product_code
        ),
      };

    case ConfirmOrderActions.CONFIRM_ORDER_SUCCESS:
      return {
        ...state,
        cartItems: [],
        appliedCoupon: {},
      };

    case ConfirmOrderActions.DISCARD_ORDER_SUCCESS:
      return {
        ...state,
        cartItems: [],
        appliedCoupon: {},
      };

    case cartActions.REMOVE_MED_SUCCESS:
      return { ...state, error: null };

    case cartActions.REMOVE_MED_FAILURE:
      return { ...state, error: action.payload };

    case cartActions.GET_ALL_COUPONS_SUCCESS:
      return { ...state, couponDetails: action.payload, error: null };

    case cartActions.GET_ALL_COUPONS_FAILURE:
      return { ...state, couponDetails: null, error: action.payload };

    case cartActions.ADD_COUPON:
      return {
        ...state,
        appliedCoupon: action.payload,
      };

    case cartActions.REMOVE_COUPON:
      return {
        ...state,
        appliedCoupon: {},
      };

    case LoginActions.USER_LOGOUT:
      return {
        ...Initial_State,
      };

    default:
      return state;
  }
};

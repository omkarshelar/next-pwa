// Cart Related Actions.

import {
  OrderManagementService_URL,
  CustomerService_URL,
} from "../../constants/Urls";
import { EndPoints } from "./Endpoints";
import { makeApiCall } from "../makeApiCall";

export const cartActions = {
  ADD_ITEM_CART: "ADD_ITEM_CART",
  REMOVE_QUANTITY: "REMOVE_QUANTITY",
  REMOVE_CART_ITEM: "REMOVE_CART_ITEM",
  ADD_INCOMPLETE_ORDER_MEDS_ADD: "ADD_INCOMPLETE_ORDER_MEDS_ADD",
  REMOVE_MED_SUCCESS: "REMOVE_MED_SUCCESS",
  REMOVE_MED_FAILURE: "REMOVE_MED_FAILURE",
  GET_ALL_COUPONS_SUCCESS: "GET_ALL_COUPONS_SUCCESS",
  GET_ALL_COUPONS_FAILURE: "GET_ALL_COUPONS_FAILURE",
  ADD_COUPON: "ADD_COUPON",
  REMOVE_COUPON: "REMOVE_COUPON",
  UPDATE_CART_ITEM: "UPDATE_CART_ITEM",
};

// export const addItemCartAction = (item) => ({
//   type: cartActions.ADD_ITEM_CART,
//   payload: item,
// });

export const addItemCartAction = (item) => (dispatch) => {
  dispatch({
    type: cartActions.ADD_ITEM_CART,
    payload: item,
  });
  return Promise.resolve();
};

export const updateCartItems = (props) => (dispatch) => {
  dispatch({
    type: cartActions.UPDATE_CART_ITEM,
    payload: props,
  });
};

export const removeQuantityAction = (item) => ({
  type: cartActions.REMOVE_QUANTITY,
  payload: item,
});

export const removeItemCartAction = (item) => ({
  type: cartActions.REMOVE_CART_ITEM,
  payload: item,
});

export const AddIncompleteOrderMedsAction = (item) => ({
  type: cartActions.ADD_INCOMPLETE_ORDER_MEDS_ADD,
  payload: item,
});

export function RemoveMedSuccessAction(props) {
  return (dispatch) => {
    dispatch({
      type: cartActions.REMOVE_MED_SUCCESS,
      payload: props,
    });
  };
}

export function RemoveMedFailureAction(error) {
  return {
    type: cartActions.REMOVE_MED_FAILURE,
    payload: error,
  };
}

// Remove medicine
export function removeMedThunk(props) {
  const reqHeader = {
    transactionId: "a",
    "Content-Type": "application/json",
    Authorization: "Bearer " + props.access_token,
  };
  const postUrl = `${OrderManagementService_URL}${EndPoints.Edit_Medicine}?orderId=${props.orderId}`;
  return makeApiCall(
    postUrl,
    RemoveMedSuccessAction,
    RemoveMedFailureAction,
    "POST",
    props.medData,
    reqHeader,
    props.history
  );
}

export function GetAllCouponsSuccessAction(props) {
  return (dispatch) => {
    dispatch({
      type: cartActions.GET_ALL_COUPONS_SUCCESS,
      payload: props,
    });
  };
}

export function GetAllCouponsFailureAction(error) {
  return {
    type: cartActions.GET_ALL_COUPONS_FAILURE,
    payload: error,
  };
}

// Get all coupons
export function getAllCouponsThunk(props) {
  const reqHeader = {
    // transactionId: "a",
    "Content-Type": "application/json",
    Authorization: "Bearer " + props.access_token,
  };
  const postUrl = `${CustomerService_URL}/offers/all?customerId=${props.customerId}`;
  return makeApiCall(
    postUrl,
    GetAllCouponsSuccessAction,
    GetAllCouponsFailureAction,
    "POST",
    null,
    reqHeader,
    props.history
  );
}

export const addCouponAction = (item) => (dispatch) => {
  dispatch({
    type: cartActions.ADD_COUPON,
    payload: item,
  });
  return Promise.resolve();
};

export const removeCouponAction = () => ({
  type: cartActions.REMOVE_COUPON,
});

import { Article_Service_URL } from "../../constants/Urls";
import Axios from "axios";

export const articleHLActions = {
  FETCH_ARTICLE_DETAILS_SUCCESS: "FETCH_ARTICLE_DETAILS_SUCCESS",
  FETCH_ARTICLE_DETAILS_ERROR: "FETCH_ARTICLE_DETAILS_ERROR",
  FETCH_ARTICLE_LIST_SUCCESS: "FETCH_ARTICLE_LIST_SUCCESS",
  FETCH_ARTICLE_LIST_FAILURE: "FETCH_ARTICLE_LIST_FAILURE",
  FETCH_ARTICLE_CUSTOM_LIST_SUCCESS: "FETCH_ARTICLE_CUSTOM_LIST_SUCCESS",
  FETCH_ARTICLE_CUSTOM_LIST_FAILURE: "FETCH_ARTICLE_CUSTOM_LIST_FAILURE",
  FETCH_BLOG_TOKEN_SUCCESS: "FETCH_BLOG_TOKEN_SUCCESS",
  FETCH_BLOG_TOKEN_FAILURE: "FETCH_BLOG_TOKEN_FAILURE",
  LOADING_START: "LOADING_START",
  LOADING_DONE: "LOADING_DONE",
  FETCH_CATEGORY_LIST_SUCCESS: "FETCH_CATEGORY_LIST_SUCCESS",
  FETCH_CATEGORY_LIST_FAILURE: "FETCH_CATEGORY_LIST_FAILURE",
};
export function fetchArticleDetails(slug, accessToken) {
  return (dispatch) => {
    const config = {
      headers: {
        Authorization: "Bearer " + accessToken,
      },
    };
    Axios.get(
      `${Article_Service_URL}/getWordpressArticle?urlParams=${encodeURIComponent(
        "posts?_embed&order=desc&slug=" + slug
      )}`,
      accessToken ? config : null
    )
      .then((response) => {
        let articleArray = response.data[0];
        let article = {};

        article.authorName = articleArray._embedded.author[0].name;
        article.categoryId = articleArray.categories[0];
        article.createdOn = articleArray.date;
        article.description = articleArray.content.rendered;
        article.id = articleArray.id;
        article.image = "";
        article.intrestedActicles = [];
        article.name = articleArray.title.rendered;
        article.type = "";
        article.url = articleArray.slug;
        article.category = [];
        article.tags = articleArray.tags;
        article.tagsList = articleArray._embedded["wp:term"]
          ? articleArray._embedded["wp:term"][1]
          : [];
        article.category = articleArray._embedded["wp:term"]
          ? articleArray._embedded["wp:term"][0]
          : [];
        article.metaDetails = articleArray.yoast_head_json
          ? articleArray.yoast_head_json
          : {};
        // articleArray._embedded["wp:term"][0].map((e) => {
        //   article.category.push(e.name);
        // });
        if (articleArray.featured_media) {
          article.image =
            articleArray._embedded["wp:featuredmedia"][0].source_url;
        }
        dispatch({
          type: articleHLActions.FETCH_ARTICLE_DETAILS_SUCCESS,
          payload: article,
        });
      })
      .catch((error) => {
        dispatch({
          type: articleHLActions.FETCH_ARTICLE_DETAILS_ERROR,
          payload: error,
        });
      });
  };
}

// Fetch all categories
export function fetchCategoryList(props) {
  return (dispatch) => {
    //per_page : number of results
    //parent : 0 will get only parent category & 1 will get only child Categories
    //allCat: will get all categories if sent true

    let accessToken = props.accessToken;
    let per_page = props.per_page ? props.per_page : 100;
    let parent = props.parent ? props.parent : 0;
    let allCat = props.allCat ? props.allCat : false;

    const config = {
      headers: {
        Authorization: "Bearer " + accessToken,
      },
    };

    let url = "categories?per_page=" + per_page + "&hide_empty=true";

    if (!allCat) {
      url += "&parent=" + parent;
    }
    return Axios.get(
      `${Article_Service_URL}/getWordpressArticle?urlParams=${encodeURIComponent(
        url
      )}`,
      accessToken ? config : null
    )
      .then((response) => {
        if (response.status && response.status === 200) {
          dispatch({
            type: articleHLActions.FETCH_CATEGORY_LIST_SUCCESS,
            payload: response.data,
          });
        } else {
          dispatch({
            type: articleHLActions.FETCH_CATEGORY_LIST_FAILURE,
            payload: "Network Error",
          });
        }
      })
      .catch((error) => {
        dispatch({
          type: articleHLActions.FETCH_CATEGORY_LIST_FAILURE,
          payload: error,
        });
      });
  };
}

export function fetchArticleListDetails(props) {
  return (dispatch) => {
    const config = {
      headers: {
        Authorization: "Bearer " + props.accessToken,
      },
    };

    let per_page = props.per_page ? props.per_page : 10;
    let order = props.order ? props.order : "desc";

    let url =
      "posts?status=publish&_embed&order=" + order + "&per_page=" + per_page;

    if (props.categories) {
      url += "&categories=" + props.categories;
    }

    if (props.page) {
      url += "&page=" + props.page;
    }

    if (props.searchString) {
      url += "&search=" + props.searchString;
    }
    return Axios.get(
      `${Article_Service_URL}/getWordpressArticle?urlParams=${encodeURIComponent(
        url
      )}&headers=true`,
      props.accessToken ? config : null
    )
      .then((response) => {
        if (response.status && response.status === 200) {
          let article = [];
          response.data.map((e) => {
            let tempArticle = {};
            tempArticle = e;
            tempArticle.categoryId = e.categories[0];
            tempArticle.categoryNameList = [];
            tempArticle.categorySlugList = [];
            let cnt = 0;
            e["_embedded"]["wp:term"][0].map((f) => {
              if (f.taxonomy === "category") {
                if (cnt === 0) {
                  tempArticle.categoryName = f.name;
                  cnt++;
                }
                tempArticle.categoryNameList.push(f.name);
                tempArticle.categorySlugList.push(f.slug);
              }
            });
            tempArticle.name = e.title.rendered;
            tempArticle.description = e.excerpt.rendered;
            article.push(tempArticle);
          });

          props.article = article;
          props.totalPosts = 0;
          props.totalPages = 0;
          if (response.headers["x-wp-totalpages"]) {
            props.totalPages = response.headers["x-wp-totalpages"];
          }
          if (response.headers["x-wp-total"]) {
            props.totalPosts = response.headers["x-wp-total"];
          }
          return dispatch({
            type: articleHLActions.FETCH_ARTICLE_LIST_SUCCESS,
            payload: props,
          });
        }
      })
      .catch((error) => {
        dispatch({
          type: articleHLActions.FETCH_ARTICLE_LIST_FAILURE,
          payload: error,
        });
      });
  };
}

// Old Fetch with respect to category Not using currently keep the logic in case there is a req change
export function fetchArticleListDetailsOld(accessToken, per_page = 10) {
  return (dispatch) => {
    let articles = [];
    let category = [];
    let users = [];

    const config = {
      headers: {
        Authorization: "Bearer " + accessToken,
      },
    };

    /* let art = new Promise((resolve, reject) => {
      Axios.get(
        `${Article_Service_URL}/getWordpressArticle?urlParams=${encodeURIComponent(
          "posts?status=publish&_embed&order=desc&per_page=" + per_page
        )}`,
        accessToken ? config : null
      )
        .then((response) => {
          resolve(response.data);
        })
        .catch((error) => {
          reject(error);
        });
    })
      .then((response) => {
        articles = response;
      })
      .catch((error) => {
        dispatch({
          type: articleHLActions.FETCH_ARTICLE_LIST_FAILURE,
          payload: error,
        });
      }); */

    let cat = new Promise((resolve, reject) => {
      Axios.get(
        `${Article_Service_URL}/getWordpressArticle?urlParams=${encodeURIComponent(
          "categories?per_page=" + per_page + "&parent=0&hide_empty=true"
        )}`,
        accessToken ? config : null
      )
        .then((response) => {
          resolve(response.data);
        })
        .catch((error) => {
          reject(error);
        });
    })
      .then((response) => {
        category = response;
      })
      .catch((error) => {
        dispatch({
          type: articleHLActions.FETCH_ARTICLE_LIST_FAILURE,
          payload: error,
        });
      });

    /*  let user = new Promise((resolve, reject) => {
      Axios.get(
        `${Article_Service_URL}/getWordpressArticle?urlParams=${encodeURIComponent(
          "users"
        )}`,
        accessToken ? config : null
      )
        .then((response) => {
          resolve(response.data);
        })
        .catch((error) => {
          reject(error);
        });
    })
      .then((response) => {
        users = response;
      })
      .catch((error) => {
        console.log(error);
      }); */

    return Promise.all([cat]).then(() => {
      let categoryList = [];
      let catList = [];
      category.map((e) => {
        categoryList.push({ name: e.name, id: e.id });
        catList.push(e.id);
      });

      let art = new Promise((resolve, reject) => {
        Axios.get(
          `${Article_Service_URL}/getWordpressArticle?urlParams=${encodeURIComponent(
            "posts?status=publish&_embed&order=desc&per_page=100&categories=" +
              catList.join(",")
          )}`,
          accessToken ? config : null
        )
          .then((response) => {
            resolve(response.data);
          })
          .catch((error) => {
            reject(error);
          });
      })
        .then((response) => {
          articles = response;
        })
        .catch((error) => {
          dispatch({
            type: articleHLActions.FETCH_ARTICLE_LIST_FAILURE,
            payload: error,
          });
        });

      return Promise.all([art]).then(() => {
        let article = [];
        articles.map((e) => {
          let tempArticle = {};
          tempArticle = e;
          tempArticle.categoryId = e.categories[0];
          tempArticle.categoryNameList = [];
          categoryList.map((f) => {
            e.categories.map((g) => {
              if (g == f.id) {
                tempArticle.categoryNameList.push(f.name);
              }
            });
            if (f.id == tempArticle.categoryId) {
              tempArticle.categoryName = f.name;
            }
          });
          tempArticle.name = e.title.rendered;
          tempArticle.description = e.excerpt.rendered;
          article.push(tempArticle);
        });

        let props = {};
        props.category = category;
        props.article = article;
        return dispatch({
          type: articleHLActions.FETCH_ARTICLE_LIST_SUCCESS,
          payload: props,
        });
      });
    });
  };
}

// Gets List for custom pages For e.g. Individual Tags and Category Pages
export function fetchCustomList(typeName, id, accessToken, page = 1) {
  return (dispatch) => {
    let typeId;
    let url = `${Article_Service_URL}/getWordpressArticle?urlParams=`;
    let error = {};
    let count = 0;
    const config = {
      headers: {
        Authorization: "Bearer " + accessToken,
      },
    };
    switch (typeName) {
      case "tag":
        url += `${encodeURIComponent("tags?slug=" + id)}`;
        break;
      case "category":
        url += `${encodeURIComponent("categories?slug=" + id)}`;
        break;
      default:
        error.message = "Sorry! No such category or tag present";
        dispatch({
          type: articleHLActions.FETCH_ARTICLE_CUSTOM_LIST_FAILURE,
          payload: error,
        });
        break;
    }
    return Axios.get(url, accessToken ? config : null)
      .then((response) => {
        if (response.data.length > 0) {
          typeId = response.data[0].id;
          count = response.data[0].count;
        } else {
          error.message = "Sorry! No data Available";
          dispatch({
            type: articleHLActions.FETCH_ARTICLE_CUSTOM_LIST_FAILURE,
            payload: error,
          });
        }

        let url1 = `${Article_Service_URL}/getWordpressArticle?urlParams=`;

        switch (typeName) {
          case "tag":
            url1 += `${encodeURIComponent(
              "posts?_embed&order=desc&tags=" + typeId + "&page=" + page
            )}`;
            break;
          case "category":
            url1 += `${encodeURIComponent(
              "posts?_embed&order=desc&categories=" + typeId + "&page=" + page
            )}`;
            break;
          default:
            error.message = "Sorry! No such category or tag present";
            dispatch({
              type: articleHLActions.FETCH_ARTICLE_CUSTOM_LIST_FAILURE,
              payload: error,
            });
            break;
        }

        if (!error.message) {
          return Axios.get(url1, accessToken ? config : null)
            .then((response1) => {
              let article = [];
              let articles = response1.data;
              articles.map((e) => {
                let tempArticle = {};
                tempArticle = e;
                tempArticle.categoryId = e.categories[0];
                tempArticle.categoryNameList = [];
                //0 always has category
                e._embedded["wp:term"][0].map((f) => {
                  if (f.id == tempArticle.categoryId) {
                    tempArticle.categoryName = f.name;
                  }
                  tempArticle.categoryNameList.push(f.name);
                });
                tempArticle.name = e.title.rendered;
                tempArticle.description = e.excerpt.rendered;
                article.push(tempArticle);
              });

              let props = {};
              props.article = article;
              props.count = count;
              dispatch({
                type: articleHLActions.FETCH_ARTICLE_CUSTOM_LIST_SUCCESS,
                payload: props,
              });
            })
            .catch((error1) => {
              dispatch({
                type: articleHLActions.FETCH_ARTICLE_CUSTOM_LIST_FAILURE,
                payload: error1,
              });
            });
        }
      })
      .catch((error) => {
        dispatch({
          type: articleHLActions.FETCH_ARTICLE_CUSTOM_LIST_FAILURE,
          payload: error,
        });
      });
  };
}

import { CustomerService_URL } from "../../constants/Urls";
import { makeApiCall } from "../makeApiCall";

export const legalThunkActions = {
  GET_TNC_SUCCESS: " GET_TNC_SUCCESS",
  GET_TNC_FAILURE: "GET_TNC_FAILURE",
  GET_PP_SUCCESS: " GET_PP_SUCCESS",
  GET_PP_FAILURE: "GET_PP_FAILURE",
};

export function legalThunkSuccess(props) {
  return (dispatch) => {
    dispatch({
      type: legalThunkActions.GET_TNC_SUCCESS,
      payload: props,
    });
  };
}

export function legalThunkError(error) {
  return {
    type: legalThunkActions.GET_TNC_FAILURE,
    payload: error,
  };
}

export function legalActionThunk(history, isTmWallet = false) {
  const reqHeader = {
    transactionId: "a",
    "Content-Type": "application/json",
  };
  let url = `${CustomerService_URL}/getTNC`;
  if (isTmWallet) {
    url += `?isPrimary=false`;
  }
  const getUrl = url;
  return makeApiCall(
    getUrl,
    legalThunkSuccess,
    legalThunkError,
    "POST",
    null,
    reqHeader,
    history
  );
}

// Privacy Policy

export function legalPPThunkSuccess(props) {
  return (dispatch) => {
    dispatch({
      type: legalThunkActions.GET_PP_SUCCESS,
      payload: props,
    });
  };
}

export function legalPPThunkError(error) {
  return {
    type: legalThunkActions.GET_PP_FAILURE,
    payload: error,
  };
}

export function legalPPActionThunk(history) {
  const reqHeader = {
    transactionId: "a",
    "Content-Type": "application/json",
  };
  const getUrl = `${CustomerService_URL}/getPP`;
  return makeApiCall(
    getUrl,
    legalPPThunkSuccess,
    legalPPThunkError,
    "POST",
    null,
    reqHeader,
    history
  );
}

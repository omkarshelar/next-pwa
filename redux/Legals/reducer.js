import { legalThunkActions } from "./action";

const initState = {
  tncSuccess: null,
  tncError: null,
  PPSuccess: null,
  PPError: null,
};

export const legalThunkReducer = (state = initState, { type, payload }) => {
  switch (type) {
    case legalThunkActions.GET_TNC_SUCCESS:
      return {
        ...state,
        tncSuccess: payload.Legals[0],
        tncError: null,
      };

    case legalThunkActions.GET_TNC_FAILURE:
      return {
        ...state,
        tncSuccess: null,
        tncError: payload,
      };
    case legalThunkActions.GET_PP_SUCCESS:
      return {
        ...state,
        PPSuccess: payload.Legals[0],
        PPError: null,
      };

    case legalThunkActions.GET_PP_FAILURE:
      return {
        ...state,
        PPSuccess: null,
        PPError: payload,
      };
    default:
      return state;
  }
};

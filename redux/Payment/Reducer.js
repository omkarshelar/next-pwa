import { paymentAction } from "./Action";
import { LoginActions } from "../Login/Action";
import { ConfirmOrderActions } from "../ConfirmOrder/Action";

let Initial_State = {
  isOnline: true,
  hasSelected: false,
};

export const paymentReducer = (state = Initial_State, action) => {
  switch (action.type) {
    case paymentAction.HAS_SELECTED:
      return {
        ...state,
        hasSelected: true,
      };
    case paymentAction.RESET_SELECTED:
      return {
        ...state,
        hasSelected: false,
      };
    case paymentAction.IS_ONLINE:
      return {
        ...state,
        isOnline: action.payload,
      };
    case LoginActions.USER_LOGOUT:
      return {
        ...Initial_State,
      };
    case ConfirmOrderActions.CONFIRM_ORDER_SUCCESS:
      return {
        ...Initial_State,
      };
    default:
      return state;
  }
};

export const paymentAction = {
  IS_ONLINE: "IS_ONLINE",
  HAS_SELECTED: "HAS_SELECTED",
  RESET_SELECTED: "RESET_SELECTED",
};

export const changePaymentAction = (isOnline) => ({
  type: paymentAction.IS_ONLINE,
  payload: isOnline,
});

export const selectPaymentThunk = () => ({
  type: paymentAction.HAS_SELECTED,
  payload: "",
});

export const resetPaymentThunk = () => ({
  type: paymentAction.RESET_SELECTED,
  payload: "",
});

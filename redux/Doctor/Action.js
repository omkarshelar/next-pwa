import { CustomerService_URL } from "../../constants/Urls";
import { makeApiCall } from "../makeApiCall";

export const doctorActions = {
  DOCTOR_DETAILS_SUCCESS: "DOCTOR_DETAILS_SUCCESS",
  DOCTOR_DETAILS_ERROR: "DOCTOR_DETAILS_ERROR",
};

export function DoctorSuccess(props) {
  return (dispatch) => {
    dispatch({
      type: doctorActions.DOCTOR_DETAILS_SUCCESS,
      payload: props,
    });
  };
}

export function DoctorError(error) {
  return {
    type: doctorActions.DOCTOR_DETAILS_ERROR,
    payload: error,
  };
}

export function doctorDetailThunk(history, loadertype, access_token) {
  const reqHeader = {
    transactionId: "a",
    "Content-Type": "application/json",
  };
  const reqHeaderWithToken = {
    transactionId: "a",
    "Content-Type": "application/json",
    Authorization: "Bearer " + access_token,
  };
  const getUrl = `${CustomerService_URL}/doctor/all`;
  return makeApiCall(
    getUrl,
    DoctorSuccess,
    DoctorError,
    "POST",
    null,
    access_token ? reqHeaderWithToken : reqHeader,
    history,
    loadertype
  );
}

import { doctorActions } from "./Action";

const initState = {
  doctorList: null,
  doctorError: null,
};

export const doctorReducer = (state = initState, { type, payload }) => {
  switch (type) {
    case doctorActions.DOCTOR_DETAILS_SUCCESS:
      return {
        ...state,
        doctorList: payload.payload,
        doctorError: null,
      };

    case doctorActions.DOCTOR_DETAILS_ERROR:
      return {
        ...state,
        doctorList: null,
        doctorError: payload,
      };
    default:
      return state;
  }
};

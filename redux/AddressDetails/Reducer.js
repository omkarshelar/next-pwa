import { LoginActions } from "../Login/Action";
import { AddressActions } from "./Action";
import { markSelectedAddress } from "./Util/AddressUtil";

let addressState = {
  fetchAddressSuccess: null,
  fetchAddressError: null,
  newAddressSuccess: null,
  newAddressError: null,
  checkoutAddressError: null,
  deleteAddressSuccess: null,
  deleteAddressError: null,
  pincodeSuccess: null,
  pincodeError: null,
};

export const AddressReducer = (initialState = addressState, action) => {
  switch (action.type) {
    case AddressActions.FETCH_ADDRESS_SUCCESS:
      return {
        ...initialState,
        fetchAddressSuccess: action.payload.AddressList,
        fetchAddressError: null,
      };
    case AddressActions.FETCH_ADDRESS_FAILURE:
      return {
        ...initialState,
        fetchAddressSuccess: null,
        fetchAddressError: action.payload,
      };
    case AddressActions.NEW_ADDRESS_SUCCESS:
      return {
        ...initialState,
        newAddressSuccess: action.payload,
        newAddressError: null,
      };
    case AddressActions.NEW_ADDRESS_FAILURE:
      return {
        ...initialState,
        newAddressSuccess: null,
        newAddressError: action.payload,
      };
    case AddressActions.RESET_ADDRESS:
      return {
        ...initialState,
        newAddressSuccess: null,
        newAddressError: null,
      };

    // Update
    case AddressActions.SAVE_USER_ADDRESS_FOR_CHECKOUT:
      return {
        ...initialState,
        fetchAddressSuccess: markSelectedAddress(
          initialState.fetchAddressSuccess,
          action.payload
        ),
      };
    case AddressActions.SAVE_ADDRESS_SUCCESS:
      return {
        ...initialState,
        fetchAddressSuccess: markSelectedAddress(
          initialState.fetchAddressSuccess,
          action.payload
        ),
        checkoutAddressError: null,
      };
    case AddressActions.SAVE_ADDRESS_FAILURE:
      return {
        ...initialState,
        checkoutAddressError: action.payload,
      };
    case AddressActions.DELETE_ADDRESS_SUCCESS:
      return {
        ...initialState,
        deleteAddressSuccess: action.payload,
        deleteAddressError: null,
      };
    case AddressActions.DELETE_ADDRESS_FAILURE:
      return {
        ...initialState,
        deleteAddressSuccess: null,
        deleteAddressError: action.payload,
      };
    case AddressActions.CHECK_PINCODE_SUCCESS:
      return {
        ...initialState,
        pincodeSuccess: action.payload,
        pincodeError: null,
      };
    case AddressActions.CHECK_PINCODE_FAILURE:
      return {
        ...initialState,
        pincodeSuccess: null,
        pincodeError: action.payload,
      };
    case LoginActions.USER_LOGOUT:
      return {
        ...addressState,
      };

    default:
      return initialState;
  }
};

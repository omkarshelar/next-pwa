import axios from "axios";
import {
  CustomerService_URL,
  ThirdPartySevice_URL,
} from "../../constants/Urls";
import { makeApiCall } from "../makeApiCall";
import HashMap from "hashmap";
let map = new HashMap();

export const AddressActions = {
  FETCH_ADDRESS_SUCCESS: "FETCH_ADDRESS_SUCCESS",
  FETCH_ADDRESS_FAILURE: "FETCH_ADDRESS_FAILURE",
  CHECK_PINCODE_SUCCESS: "CHECK_PINCODE_SUCCESS",
  CHECK_PINCODE_FAILURE: "CHECK_PINCODE_FAILURE",
  DELETE_ADDRESS_SUCCESS: "DELETE_ADDRESS_SUCCESS",
  DELETE_ADDRESS_FAILURE: "DELETE_ADDRESS_FAILURE",
  NEW_ADDRESS_SUCCESS: "NEW_ADDRESS_SUCCESS",
  NEW_ADDRESS_FAILURE: "NEW_ADDRESS_FAILURE",
  SAVE_ADDRESS_SUCCESS: "SAVE_ADDRESS_SUCCESS",
  SAVE_ADDRESS_FAILURE: "SAVE_ADDRESS_FAILURE",
  SAVE_USER_ADDRESS_FOR_CHECKOUT: "SAVE_USER_ADDRESS_FOR_CHECKOUT",
  RESET_ADDRESS: "RESET_ADDRESS",
};

// SAVE ADDRESS FOR CHECKOUT
// Update
export function selectAdresssForCheckoutAction(props) {
  return (dispatch) => {
    dispatch({
      type: AddressActions.SAVE_USER_ADDRESS_FOR_CHECKOUT,
      payload: props,
    });
  };
}

// Fetch Address Details

export function fetchAddressSuccess(props) {
  return (dispatch) => {
    dispatch({
      type: AddressActions.FETCH_ADDRESS_SUCCESS,
      payload: props,
    });
  };
}

export function fetchAddressError(error) {
  return {
    type: AddressActions.FETCH_ADDRESS_FAILURE,
    payload: error,
  };
}

export function resetAddress() {
  return (dispatch) => {
    dispatch({
      type: AddressActions.RESET_ADDRESS,
      payload: "",
    });
  };
}

export function fetchAddressThunk(props) {
  const reqHeader = {
    transactionId: "a",
    "Content-Type": "application/json",
    Authorization: "Bearer " + props.accessToken,
  };
  const getUrl = `${CustomerService_URL}/getAllAddress`;
  return makeApiCall(
    getUrl,
    fetchAddressSuccess,
    fetchAddressError,
    "POST",
    null,
    reqHeader,
    props.history
  );
}

// Add New Address

export function addAddressSuccess(props) {
  return (dispatch) => {
    dispatch({
      type: AddressActions.NEW_ADDRESS_SUCCESS,
      payload: props,
    });
  };
}

export function addAddressError(error) {
  return {
    type: AddressActions.NEW_ADDRESS_FAILURE,
    payload: error,
  };
}

export function addAddressThunk(props) {
  const reqHeader = {
    transactionId: "a",
    "Content-Type": "application/json",
    Authorization: "Bearer " + props.accessToken,
  };
  let getUrl = `${CustomerService_URL}/saveAddress?hitAlgoSync=${true}&isAppEdit=${false}`;
  if (props.orderId) {
    getUrl += `&orderId=${props.orderId}`;
  }
  return makeApiCall(
    getUrl,
    addAddressSuccess,
    addAddressError,
    "POST",
    props.addressData,
    reqHeader,
    props.history
  );
}

// Save address for checkout

export function saveAddressSuccess(props) {
  return (dispatch) => {
    dispatch({
      type: AddressActions.SAVE_ADDRESS_SUCCESS,
      payload: props,
    });
  };
}

export function saveAddressError(error) {
  return {
    type: AddressActions.SAVE_ADDRESS_FAILURE,
    payload: error,
  };
}

export function saveAddressThunk(props) {
  return async (dispatch) => {
    const getUrl = `${CustomerService_URL}/saveAddressForOrder?hitAlgoSync=${true}&addressId=${
      props.addressid
    }&orderId=${props.orderid}`;
    let reqHeader = {
      transactionId: "a",
      "Content-Type": "application/json",
      Authorization: "Bearer " + props.accessToken,
    };
    let axiosBody = {
      method: "POST",
      url: getUrl,
      data: null,
      headers: reqHeader,
    };
    return axios(axiosBody)
      .then((response) => {
        if (response.status === 200) {
          // Update
          dispatch(
            saveAddressSuccess({
              addressId: props.addressid,
            })
          );
        }
      })
      .catch((err) => {
        if (err.response) {
          dispatch(
            saveAddressError("An error occurred while saving the changes")
          );
        } else {
          dispatch(saveAddressError("Something went wrong"));
        }
      });
  };
}

// Delete Address

export function deleteAddressSuccess(props) {
  return (dispatch) => {
    dispatch({
      type: AddressActions.DELETE_ADDRESS_SUCCESS,
      payload: props,
    });
  };
}

export function deleteAddressError(error) {
  return {
    type: AddressActions.DELETE_ADDRESS_FAILURE,
    payload: error,
  };
}

export function deleteAddressThunk(props) {
  const reqHeader = {
    transactionId: "a",
    "Content-Type": "application/json",
    Authorization: "Bearer " + props.accessToken,
  };
  const getUrl = `${CustomerService_URL}/deleteAddress?addressId=${props.addressId}`;
  return makeApiCall(
    getUrl,
    deleteAddressSuccess,
    deleteAddressError,
    "POST",
    null,
    reqHeader,
    props.history
  );
}

// Check pincode

export function checkPincodeSuccess(props) {
  return (dispatch) => {
    dispatch({
      type: AddressActions.CHECK_PINCODE_SUCCESS,
      payload: props,
    });
  };
}

export function checkPincodeError(error) {
  return {
    type: AddressActions.CHECK_PINCODE_FAILURE,
    payload: error,
  };
}

function checkServiceabilityHandler(data) {
  let TempStorage = [];
  if (data) {
    for (let index = 0; index < data.length; index++) {
      const element = data[index];

      let { serviceable } = element;
      if (serviceable.COD && serviceable.PREPAID) {
        TempStorage.push(element);
      }
    }
    if (TempStorage.length > 0) {
      return true;
    } else {
      return false;
    }
  } else {
    return false;
  }
}

export function checkPincodeThunk(props) {
  return async (dispatch) => {
    const getUrl = `${ThirdPartySevice_URL}/checkPincodeServiceability?pincode=${props.pincode}`;
    let reqHeader = {
      transactionId: "a",
      "Content-Type": "application/json",
      // Authorization: "Bearer " + props.accessToken,
    };
    let axiosBody = {
      method: "GET",
      url: getUrl,
      data: null,
      headers: reqHeader,
    };
    return axios(axiosBody)
      .then((response) => {
        if (response.status === 200) {
          let filterResponse = checkServiceabilityHandler(
            response.data.clickpostPincodeData.result
          );
          dispatch(
            checkPincodeSuccess({
              isServiceable: filterResponse,
              pincodeData: response.data.pincodeData[0].city,
              warehouseId: response.data.pincodeData[0].warehouseId,
              pincode: response.data.pincodeData[0].pincode,
            })
          );
        }
      })
      .catch((err) => {
        if (err.response) {
          err = handleError(err);
          dispatch(checkPincodeError(err));
        } else {
          dispatch(checkPincodeError("Something went wrong"));
        }
      });
  };
}

export function handleError(err) {
  map = objToStrMap(JSON.parse(JSON.stringify(err.response.data)));
  map.forEach((ele) => (err = ele));
  return err;
}
export function objToStrMap(obj) {
  let strMap = new Map();
  for (let k of Object.keys(obj)) {
    strMap.set(k, obj[k]);
  }
  return strMap;
}

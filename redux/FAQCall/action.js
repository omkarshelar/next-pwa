import { AdminService_URL } from "../../constants/Urls";
import { makeApiCall } from "../makeApiCall";

export const faqCategoryActions = {
  FETCH_FAQ_CATEGORY_FULFILLED: "FETCH_FAQ_CATEGORY_FULFILLED",
  FETCH_FAQ_CATEGORY_REJECTED: "FETCH_FAQ_CATEGORY_REJECTED",
};

export function faqCategorySuccess(props) {
  return (dispatch) => {
    dispatch({
      type: faqCategoryActions.FETCH_FAQ_CATEGORY_FULFILLED,
      payload: props,
    });
  };
}

export function faqCategoryError(error) {
  return {
    type: faqCategoryActions.FETCH_FAQ_CATEGORY_REJECTED,
    payload: error,
  };
}

export function getFaqCategory(props) {
  const getUrl = `${AdminService_URL}/faq/category`;
  return makeApiCall(
    getUrl,
    faqCategorySuccess,
    faqCategoryError,
    "POST",
    props.history
  );
}

/////////// Get FAQ

export const faqActions = {
  FETCH_FAQ_FULFILLED: "FETCH_FAQ_FULFILLED",
  FETCH_FAQ_REJECTED: "FETCH_FAQ_REJECTED",
};

export function faqSuccess(props) {
  return (dispatch) => {
    dispatch({
      type: faqActions.FETCH_FAQ_FULFILLED,
      payload: props,
    });
  };
}

export function faqError(error) {
  return {
    type: faqActions.FETCH_FAQ_REJECTED,
    payload: error,
  };
}

export function getFaq(props) {
  const getUrl = `${AdminService_URL}/faq/id/category?categoryId=${props.categoryId}`;
  return makeApiCall(getUrl, faqSuccess, faqError, "POST", props.history);
}

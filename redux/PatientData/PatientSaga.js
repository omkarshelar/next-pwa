import { takeLatest, put, all, call } from "redux-saga/effects";
import Axios from "axios";
import {
  PatientDataActions,
  PatientDetSuccessAction,
  PatientDetFailureAction,
  UpdateProfileSuccessAction,
  UpdateProfileFailureAction,
  AddPatientSuccessAction,
  AddPatientFailureAction,
  UpdatePatientSuccessAction,
  UpdatePatientFailureAction,
  DeletePatientSuccessAction,
  DeletePatientFailureAction,
} from "./Action";
import { startLoading, stopLoading } from "../Loader/Action";
import { CustomerService_URL } from "../../Url/Urls";
import { push } from "react-router-redux";
import {
  Get_All_Patient,
  Update_Profile,
  Add_Patient,
  UpdatePatientId_In_SubOrder,
  Delete_Patient,
} from "./Endpoint";
import { toAddressDetailsAction } from "../Timeline/Action";

// Reusable function for fetching data after adding new patient.
function* PatientDetailsReusable(access_token) {
  try {
    const Uri = `${CustomerService_URL}${Get_All_Patient}?showMyself=true`;
    const config = {
      headers: {
        transactionId: "a",
        "Content-Type": "application/json",
        Authorization: "Bearer " + access_token,
      },
    };
    yield put(startLoading());
    let response = yield call(() => Axios.post(Uri, null, config));
    let modify = response.data.PatientList.map((data) => {
      if (data.relationId === 5) {
        return { ...data, selected: true };
      } else {
        return { ...data, selected: false };
      }
    });
    let success = yield put(PatientDetSuccessAction(modify));
    if (success) {
      yield put(stopLoading());
    }
  } catch (error) {
    yield put(stopLoading());
    yield put(PatientDetFailureAction(error));
  }
}

export function* PatientDetailsSaga({ payload: access_token }) {
  try {
    const Uri = `${CustomerService_URL}${Get_All_Patient}?showMyself=true`;
    const config = {
      headers: {
        transactionId: "a",
        "Content-Type": "application/json",
        Authorization: "Bearer " + access_token,
      },
    };
    yield put(startLoading());
    let response = yield call(() => Axios.post(Uri, null, config));
    let modify = response.data.PatientList.map((data) => {
      if (data.relationId === 5) {
        return { ...data, selected: true };
      } else {
        return { ...data, selected: false };
      }
    });
    let success = yield put(PatientDetSuccessAction(modify));
    if (success) {
      yield put(stopLoading());
    }
  } catch (error) {
    yield put(stopLoading());
    yield put(PatientDetFailureAction(error));
  }
}

export function* onPatientDetailsStart() {
  yield takeLatest(PatientDataActions.PatientDetStart, PatientDetailsSaga);
}

// Update Profile Saga.

export function* updateProfileSaga({ payload: { access_token, latestData } }) {
  try {
    const Uri = `${CustomerService_URL}${Update_Profile}`;
    const config = {
      headers: {
        transactionId: "a",
        "Content-Type": "application/json",
        Authorization: "Bearer " + access_token,
      },
    };
    yield put(startLoading());
    let response = yield call(() => Axios.post(Uri, latestData, config));
    let success = yield put(
      UpdateProfileSuccessAction({
        Response: response.data,
        updateData: latestData,
      })
    );
    if (success) {
      yield call(PatientDetailsReusable, access_token);
      yield put(stopLoading());
    }
  } catch (error) {
    yield put(stopLoading());
    yield put(UpdateProfileFailureAction(error));
  }
}

export function* onUpdateProfileStart() {
  yield takeLatest(PatientDataActions.UploadProfileStart, updateProfileSaga);
}

// Add Patient.

export function* addPatientSaga({ payload: { access_token, patientData } }) {
  try {
    const Uri = `${CustomerService_URL}${Add_Patient}`;
    const config = {
      headers: {
        transactionId: "a",
        "Content-Type": "application/json",
        Authorization: "Bearer " + access_token,
      },
    };
    yield put(startLoading());
    let response = yield call(() => Axios.post(Uri, patientData, config));
    let success = yield put(AddPatientSuccessAction(response.data));
    if (success) {
      yield call(PatientDetailsReusable, access_token);
      yield put(stopLoading());
    }
  } catch (error) {
    yield put(stopLoading());
    yield put(AddPatientFailureAction(error));
  }
}

export function* onAddPatientStart() {
  yield takeLatest(PatientDataActions.AddPatientStart, addPatientSaga);
}

// Update Patient Id Sub Order

export function* updatePatientSaga({
  payload: { access_token, PatientId, OrderId },
}) {
  try {
    const Uri = `${CustomerService_URL}${UpdatePatientId_In_SubOrder}?patientId=${PatientId}&orderId=${OrderId}`;
    const config = {
      headers: {
        transactionId: "a",
        "Content-Type": "application/json",
        Authorization: "Bearer " + access_token,
      },
    };
    yield put(startLoading());
    let response = yield call(() => Axios.get(Uri, config));
    let success = yield put(UpdatePatientSuccessAction(response.data));
    if (success) {
      yield put(toAddressDetailsAction());
      yield put(push(`/orderflow`));
      yield put(stopLoading());
    }
  } catch (error) {
    yield put(stopLoading());
    yield put(UpdatePatientFailureAction(error));
  }
}

export function* onUpdatePatientStart() {
  yield takeLatest(PatientDataActions.UpdatePatientStart, updatePatientSaga);
}

// delete Patient details

export function* deletePatientSaga({
  payload: { access_token, PatientId, customerId },
}) {
  try {
    const Uri = `${CustomerService_URL}${Delete_Patient}?patientId=${PatientId}&customerId=${customerId}`;
    const config = {
      headers: {
        transactionId: "a",
        "Content-Type": "application/json",
        Authorization: "Bearer " + access_token,
      },
    };
    yield put(startLoading());
    let response = yield call(() => Axios.post(Uri, null, config));
    let success = yield put(DeletePatientSuccessAction(response.data));
    if (success) {
      yield call(PatientDetailsReusable, access_token);
      yield put(stopLoading());
    }
  } catch (error) {
    yield put(stopLoading());
    yield put(DeletePatientFailureAction(error));
  }
}

export function* onDeletePatientStart() {
  yield takeLatest(PatientDataActions.DeletePatientStart, deletePatientSaga);
}

// Root Pipe.

export function* PatientDetailsSagas() {
  yield all([
    call(onPatientDetailsStart),
    call(onUpdateProfileStart),
    call(onAddPatientStart),
    call(onUpdatePatientStart),
    call(onDeletePatientStart),
  ]);
}

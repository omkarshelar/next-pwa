// Actions Related To Patient Deatails.

export const PatientDataActions = {
  PatientDetStart: "PatientDet_Start",
  PatientDetSuccess: "PatientDet_Success",
  PatientDetFailure: "PatientDet_Failure",
  UploadProfileStart: "UploadProfile_Start",
  UploadProfileSuccess: "UploadProfile_Success",
  UploadProfileFailure: "UploadProfile_Failure",
  AddPatientStart: "AddPatient_Start",
  AddPatientSuccess: "AddPatient_Success",
  AddPatientFailure: "AddPatient_Failure",
  UpdatePatientStart: "UpdatePatient_Start",
  UpdatePatientSuccess: "UpdatePatient_Success",
  UpdatePatientFailure: "UpdatePatient_Failure",
  DeletePatientStart: "DeletePatient_Start",
  DeletePatientSuccess: "DeletePatient_Success",
  DeletePatientFailure: "DeletePatient_Failure",
  selectPatient: "select_Patient",
};

// Fetch All patient data.
export const PatientDetStartAction = (data) => ({
  type: PatientDataActions.PatientDetStart,
  payload: data,
});

export const PatientDetSuccessAction = (data) => ({
  type: PatientDataActions.PatientDetSuccess,
  payload: data,
});

export const PatientDetFailureAction = (error) => ({
  type: PatientDataActions.PatientDetFailure,
  payload: error,
});

// Update User Profile.
export const UpdateProfileStartAction = (data) => ({
  type: PatientDataActions.UploadProfileStart,
  payload: data,
});

export const UpdateProfileSuccessAction = (data) => ({
  type: PatientDataActions.UploadProfileSuccess,
  payload: data,
});

export const UpdateProfileFailureAction = (error) => ({
  type: PatientDataActions.UploadProfileFailure,
  payload: error,
});

// Add Patient Saga.

export const AddPatientStartAction = (data) => ({
  type: PatientDataActions.AddPatientStart,
  payload: data,
});

export const AddPatientSuccessAction = (data) => ({
  type: PatientDataActions.AddPatientSuccess,
  payload: data,
});

export const AddPatientFailureAction = (error) => ({
  type: PatientDataActions.AddPatientFailure,
  payload: error,
});

// Update Patient Id Sub Order Actions.

export const UpdatePatientStartAction = (data) => ({
  type: PatientDataActions.UpdatePatientStart,
  payload: data,
});

export const UpdatePatientSuccessAction = (data) => ({
  type: PatientDataActions.UpdatePatientSuccess,
  payload: data,
});

export const UpdatePatientFailureAction = (error) => ({
  type: PatientDataActions.UpdatePatientFailure,
  payload: error,
});

// Delete patient Action
export const DeletePatientStartAction = (data) => ({
  type: PatientDataActions.DeletePatientStart,
  payload: data,
});

export const DeletePatientSuccessAction = (data) => ({
  type: PatientDataActions.DeletePatientSuccess,
  payload: data,
});

export const DeletePatientFailureAction = (error) => ({
  type: PatientDataActions.DeletePatientFailure,
  payload: error,
});

// select patient from list

export const selectPatientAction = (data) => ({
  type: PatientDataActions.selectPatient,
  payload: data,
});

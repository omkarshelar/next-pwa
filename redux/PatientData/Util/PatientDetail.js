export const selectPatientUtil = (patientList, patientListToAdd) => {
  const existingPatient = patientList.find(
    (item) => item.patientId === patientListToAdd.patientId
  );
  if (existingPatient) {
    return patientList.map((item) =>
      item.patientId === patientListToAdd.patientId
        ? { ...item, selected: true }
        : { ...item, selected: false }
    );
  }
  return [...patientList];
};

import { reorderActions } from "./Action";

const initState = {
  reorderData: null,
  reorderError: null,
};

export const reorderReducer = (state = initState, { type, payload }) => {
  switch (type) {
    case reorderActions.REORDER_SUCCESS:
      return {
        ...state,
        reorderData: payload,
        reorderError: null,
      };

    case reorderActions.REORDER_FAILURE:
      return {
        ...state,
        reorderData: null,
        reorderError: payload,
      };
    default:
      return state;
  }
};

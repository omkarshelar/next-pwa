import { OrderManagementService_URL } from "../../constants/Urls";
import { makeApiCall } from "../makeApiCall";

export const reorderActions = {
  REORDER_SUCCESS: "REORDER_SUCCESS",
  REORDER_FAILURE: "REORDER_FAILURE",
};
// Fetch My savings

export function reorderSuccess(props) {
  return (dispatch) => {
    dispatch({
      type: reorderActions.REORDER_SUCCESS,
      payload: props,
    });
  };
}

export function reorderError(error) {
  return {
    type: reorderActions.REORDER_FAILURE,
    payload: error,
  };
}

export function reorderThunk(props) {
  const reqHeader = {
    "Content-Type": "application/json",
    Authorization: "Bearer " + props.accessToken,
  };
  const getUrl = `${OrderManagementService_URL}/reOrder?customerId=${props.customerId}&orderId=${props.orderId}`;
  return makeApiCall(
    getUrl,
    reorderSuccess,
    reorderError,
    "POST",
    props.patientIdSet,
    reqHeader,
    props.history
  );
}

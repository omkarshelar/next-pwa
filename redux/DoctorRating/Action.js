import { CustomerService_URL } from "../../constants/Urls";
import { makeApiCall } from "../makeApiCall";

export const doctorRatingActions = {
  GET_DOCTOR_RATING_DETAILS_SUCCESS: "GET_DOCTOR_RATING_DETAILS_SUCCESS",
  GET_DOCTOR_RATING_DETAILS_FAILURE: "GET_DOCTOR_RATING_DETAILS_FAILURE",
  SET_DOCTOR_RATING_DETAILS_SUCCESS: "SET_DOCTOR_RATING_DETAILS_SUCCESS",
  SET_DOCTOR_RATING_DETAILS_FAILURE: "SET_DOCTOR_RATING_DETAILS_FAILURE",
};

///////////////////////////////////////
//? Fetch doctor ratings
///////////////////////////////////////

export function getDoctorRatingDetailsSuccess(props) {
  return (dispatch) => {
    dispatch({
      type: doctorRatingActions.GET_DOCTOR_RATING_DETAILS_SUCCESS,
      payload: props,
    });
  };
}

export function getDoctorRatingDetailsError(error) {
  return {
    type: doctorRatingActions.GET_DOCTOR_RATING_DETAILS_FAILURE,
    payload: error,
  };
}

export function getDoctorRatingDetailsThunk(props) {
  const reqHeader = {
    transactionId: "a",
    "Content-Type": "application/json",
    Authorization: "Bearer " + props.access_token,
  };
  const postUrl = `${CustomerService_URL}/doctor/getDoctorDetails?orderId=${props.orderId}`;
  return makeApiCall(
    postUrl,
    getDoctorRatingDetailsSuccess,
    getDoctorRatingDetailsError,
    "GET",
    null,
    reqHeader,
    props.history
  );
}

///////////////////////////////////////
//? Set doctor ratings
///////////////////////////////////////

export function saveDoctorRatingSuccess(props) {
  return (dispatch) => {
    dispatch({
      type: doctorRatingActions.SET_DOCTOR_RATING_DETAILS_SUCCESS,
      payload: props,
    });
  };
}

export function saveDoctorRatingError(error) {
  return {
    type: doctorRatingActions.SET_DOCTOR_RATING_DETAILS_FAILURE,
    payload: error,
  };
}

export function saveDoctorRatingThunk(props) {
  const reqHeader = {
    transactionId: "a",
    "Content-Type": "application/json",
    Authorization: "Bearer " + props.access_token,
  };
  const postUrl = `${CustomerService_URL}/rating/doctor`;
  return makeApiCall(
    postUrl,
    saveDoctorRatingSuccess,
    saveDoctorRatingError,
    "POST",
    props.data,
    reqHeader,
    props.history
  );
}

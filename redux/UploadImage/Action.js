import Axios from "axios";
import {
  CustomerService_URL,
  OrderManagementService_URL,
} from "../../constants/Urls";
import { makeApiCall } from "../makeApiCall";
import { EndPoints } from "./EndPoints";

export const UploadActions = {
  UPLOAD_IMAGE_SUCCESS: "UPLOAD_IMAGE_SUCCESS",
  UPLOAD_IMAGE_FAILURE: "UPLOAD_IMAGE_FAILURE",
  DELETE_UPLOAD_IMAGE_SUCCESS: "DELETE_UPLOAD_IMAGE_SUCCESS",
  DELETE_UPLOAD_IMAGE_FAILURE: "DELETE_UPLOAD_IMAGE_FAILURE",
  GET_PRESCRIPTION_BY_ID_SUCCESS: "GET_PRESCRIPTION_BY_ID_SUCCESS",
  GET_PRESCRIPTION_BY_ID_FAILURE: "GET_PRESCRIPTION_BY_ID_FAILURE",
  LAST_IMAGE_CLEAR_REDUCER: "LAST_IMAGE_CLEAR_REDUCER",
  IS_UPLOAD: "IS_UPLOAD",
};

export function UploadImageSuccessAction(props) {
  return (dispatch) => {
    dispatch({
      type: UploadActions.UPLOAD_IMAGE_SUCCESS,
      payload: props,
    });
  };
}

export function UploadImageFailureAction(error) {
  return {
    type: UploadActions.UPLOAD_IMAGE_FAILURE,
    payload: error,
  };
}

// Upload image
// export function uploadImageThunk(props) {
//   const reqHeader = {
//     orderId: props.imgData.orderId,
//     transactionId: "a",
//     "Content-Type": "application/json",
//     Authorization: "Bearer " + props.access_token,
//     "Access-Control-Allow-Origin": "*",
//     "Access-Control-Allow-Methods": "POST,GET,OPTIONS",
//   };
//   const postUrl = `${CustomerService_URL}${EndPoints.Upload_Image}?orderId=${props.imgData.orderId}`;
//   return makeApiCall(
//     postUrl,
//     UploadImageSuccessAction,
//     UploadImageFailureAction,
//     "POST",
//     props.imgData,
//     reqHeader,
//     props.history
//   );
// }

export function uploadImageThunk(props) {
  const reqHeader = {
    orderId: props.imgData.orderId,
    transactionId: "a",
    "Content-Type": "application/json",
    Authorization: "Bearer " + props.access_token,
    "Access-Control-Allow-Origin": "*",
    "Access-Control-Allow-Methods": "POST,GET,OPTIONS",
  };
  const postUrl = `${OrderManagementService_URL}${EndPoints.Upload_Image}?orderId=${props.imgData.orderId}&customerId=${props.customerId}&pincode=${props.pincode}`;
  return makeApiCall(
    postUrl,
    UploadImageSuccessAction,
    UploadImageFailureAction,
    "POST",
    props.imgData,
    reqHeader,
    props.history
  );
}

export function DeleteUploadImageSuccessAction(props) {
  return (dispatch) => {
    dispatch({
      type: UploadActions.DELETE_UPLOAD_IMAGE_SUCCESS,
      payload: props,
    });
  };
}

export function DeleteUploadImageFailureAction(error) {
  return {
    type: UploadActions.DELETE_UPLOAD_IMAGE_FAILURE,
    payload: error,
  };
}

export function deleteUploadImageThunk(props) {
  return (dispatch) => {
    const postUrl = `${CustomerService_URL}${EndPoints.Delete_Image}?orderId=${props.orderIds}`;
    const reqHeader = {
      orderId: props.orderIds,
      transactionId: "a",
      "Content-Type": "application/json",
      Authorization: "Bearer " + props.access_token,
    };
    let axiosBody = {
      method: "POST",
      url: postUrl,
      data: props.imgIdSet,
      headers: reqHeader,
    };
    return Axios(axiosBody)
      .then((response) => {
        if (response.status === 200) {
          dispatch(
            DeleteUploadImageSuccessAction({
              deleteMsg: response.data,
              imgId: props.imgIdSet,
            })
          );
        }
      })
      .catch((err) => {
        if (err.response) {
          dispatch(DeleteUploadImageFailureAction(err.response));
        } else {
          dispatch(DeleteUploadImageFailureAction("Something went wrong"));
        }
      });
  };
}

export function GetPrescriptionByIdSuccessAction(props) {
  return (dispatch) => {
    dispatch({
      type: UploadActions.GET_PRESCRIPTION_BY_ID_SUCCESS,
      payload: props,
    });
  };
}

export function GetPrescriptionByIdFailureAction(error) {
  return {
    type: UploadActions.GET_PRESCRIPTION_BY_ID_FAILURE,
    payload: error,
  };
}

// Get images by customer id
export function getPrescriptionByIdThunk(props) {
  const reqHeader = {
    transactionId: "a",
    "Content-Type": "application/json",
    Authorization: "Bearer " + props.access_token,
  };
  const getUrl = `${OrderManagementService_URL}${EndPoints.Get_Image_By_Id}?customerId=${props.customerId}`;
  return makeApiCall(
    getUrl,
    GetPrescriptionByIdSuccessAction,
    GetPrescriptionByIdFailureAction,
    "GET",
    null,
    reqHeader,
    props.history
  );
}

export const lastImageClearAction = () => (dispatch) => {
  dispatch({
    type: UploadActions.LAST_IMAGE_CLEAR_REDUCER,
  });
  return Promise.resolve();
};

export const isUploadAction = (isUpload) => ({
  type: UploadActions.IS_UPLOAD,
  payload: isUpload,
});

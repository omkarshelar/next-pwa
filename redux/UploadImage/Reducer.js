import { UploadActions } from "./Action";
import { comfirmMedicineActions } from "../ConfirmMedicine/Action";
import { incompleteOrderActions } from "../IncompleteOrder/Action";
import { ConfirmOrderActions } from "../ConfirmOrder/Action";
import { LoginActions } from "../Login/Action";

let uploadedImageState = {
  msg: null,
  error: null,
  orderId: null,
  PatientId: null,
  uploadHistory: [],
  prescriptionById: null,
};

let uploadedImageState2 = {
  isUpload: false,
};

const upload = (old, next) => {
  return [...old, { imgId: next.ImageId, src: next.ImageUrl }];
};

export const UploadImageReducer = (
  state = uploadedImageState,
  { type, payload }
) => {
  switch (type) {
    case incompleteOrderActions.INCOMPLETE_ORDER_SUCCESS:
      return {
        ...state,
        msg: {
          ...state.msg,
          OrderId: payload.orderId,
        },
        orderId: payload.orderId,
      };

    case comfirmMedicineActions.CONFIRM_MEDICINE_SUCCESS:
      return {
        ...state,
        msg: {
          ...state.msg,
          OrderId: payload.orderId,
        },
        orderId: payload.orderId,
      };

    case UploadActions.UPLOAD_IMAGE_SUCCESS:
      return {
        ...state,
        msg: payload,
        error: null,
        orderId: payload.OrderId,
        PatientId: payload.PatientId,
        uploadHistory: upload(state.uploadHistory, payload),
      };

    case UploadActions.UPLOAD_IMAGE_FAILURE:
      return { ...state, msg: null, error: payload };

    case UploadActions.DELETE_UPLOAD_IMAGE_SUCCESS:
      return {
        ...state,
        uploadHistory: state.uploadHistory.filter((data) => {
          return data.imgId !== payload.imgId[0];
        }),
      };

    case UploadActions.DELETE_UPLOAD_IMAGE_FAILURE:
      return {
        ...state,
        error: payload,
      };

    case UploadActions.GET_PRESCRIPTION_BY_ID_SUCCESS:
      return {
        ...state,
        prescriptionById: payload,
        error: null,
      };

    case UploadActions.GET_PRESCRIPTION_BY_ID_FAILURE:
      return {
        ...state,
        error: payload,
        prescriptionById: null,
      };

    case ConfirmOrderActions.CONFIRM_ORDER_SUCCESS:
      return {
        msg: null,
        error: null,
        orderId: null,
        PatientId: null,
        uploadHistory: [],
      };

    case ConfirmOrderActions.DISCARD_ORDER_SUCCESS:
      return {
        msg: null,
        error: null,
        orderId: null,
        PatientId: null,
        uploadHistory: [],
      };

    case LoginActions.USER_LOGOUT:
      return {
        msg: null,
        error: null,
        orderId: null,
        PatientId: null,
        uploadHistory: [],
      };

    case UploadActions.LAST_IMAGE_CLEAR_REDUCER:
      return {
        ...state,
        msg: null,
        error: null,
        orderId: null,
        PatientId: null,
        uploadHistory: [],
      };

    default:
      return state;
  }
};

export const UploadImageReducer2 = (
  state = uploadedImageState2,
  { type, payload }
) => {
  switch (type) {
    case UploadActions.IS_UPLOAD:
      return {
        ...state,
        isUpload: payload,
      };

    case ConfirmOrderActions.CONFIRM_ORDER_SUCCESS:
      return {
        isUpload: false,
      };

    case ConfirmOrderActions.DISCARD_ORDER_SUCCESS:
      return {
        isUpload: false,
      };

    case LoginActions.USER_LOGOUT:
      return {
        isUpload: false,
      };

    default:
      return state;
  }
};

import { OrderManagementService_URL } from "../../constants/Urls";
import { makeApiCall } from "../makeApiCall";

export const comfirmMedicineActions = {
  CONFIRM_MEDICINE_SUCCESS: "CONFIRM_MEDICINE_SUCCESS",
  CONFIRM_MEDICINE_FAILURE: "CONFIRM_MEDICINE_FAILURE",
  CLEAR_MEDICINE_DETAILS: "CLEAR_MEDICINE_DETAILS",
};
// Fetch Address Details

export function comfirmMedicineSuccess(props) {
  return (dispatch) => {
    dispatch({
      type: comfirmMedicineActions.CONFIRM_MEDICINE_SUCCESS,
      payload: props,
    });
  };
}

export function comfirmMedicineError(error) {
  return {
    type: comfirmMedicineActions.CONFIRM_MEDICINE_FAILURE,
    payload: error,
  };
}

export function comfirmMedicineThunk(props) {
  const reqHeader = {
    "Content-Type": "application/json",
    Authorization: "Bearer " + props.accessToken,
  };
  const getUrl = `${OrderManagementService_URL}/saveMedsAndCreateOrder?orderId=${props.orderIds}&customerId=${props.customerId}&offerId=${props.offerId}&pincode=${props.pincode}`;
  return makeApiCall(
    getUrl,
    comfirmMedicineSuccess,
    comfirmMedicineError,
    "POST",
    props.medSet,
    reqHeader,
    props.history
  );
}

export const clearMedDetailsAction = () => (dispatch) => {
  dispatch({
    type: comfirmMedicineActions.CLEAR_MEDICINE_DETAILS,
  });
  return Promise.resolve();
};

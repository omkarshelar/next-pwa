import { analytics, events_Analytics } from "../src/Firebase/AnalyticsEvents";
// import { singularSdk } from "singular-sdk";
import window from "global";
// import { hotjar } from "react-hotjar";

export let eventCompleteRegistration = (customerId) => {
  // analytics.logEvent(events_Analytics.registration_completion);
  // window.fbq("track", "CompleteRegistration");
  // window.Moengage.track_event(events_Analytics.registration_completion);
  // window.Moengage.add_unique_user_id(customerId);
  // singularSdk.login(localStorage.getItem("UserPhone"));
};

export let eventSearchMedicines = () => {
  // analytics.logEvent(events_Analytics.search_medicines);
  // window.fbq("trackCustom", events_Analytics.search_medicines);
  // window.Moengage.track_event(events_Analytics.search_medicines);
};

export let eventSearchPrescription = () => {
  // analytics.logEvent(events_Analytics.search_prescription);
  // window.fbq("trackCustom", events_Analytics.search_prescription);
  // window.Moengage.track_event(events_Analytics.search_prescription);
};

export let eventCartViewed = () => {
  // analytics.logEvent(events_Analytics.cart_viewed);
  // window.fbq("trackCustom", events_Analytics.cart_viewed);
  // window.Moengage.track_event(events_Analytics.cart_viewed);
};

export let eventPrescriptionUploaded = () => {
  // analytics.logEvent(events_Analytics.prescription_uploaded);
  // window.fbq("trackCustom", events_Analytics.prescription_uploaded);
  // window.Moengage.track_event(events_Analytics.prescription_uploaded);
};

export let eventSubstituteViewed = () => {
  // analytics.logEvent(events_Analytics.substitute_viewed);
  // window.fbq("trackCustom", events_Analytics.substitute_viewed);
  // window.Moengage.track_event(events_Analytics.substitute_viewed);
};

export let eventOrderSubmission = (amount) => {
  // analytics.logEvent(events_Analytics.order_submission, { revenue: amount });
  // window.fbq("track", "Purchase", { currency: "INR", value: amount });
  // window.fbq("trackCustom", events_Analytics.order_submission, {
  //   revenue: amount,
  // });
  // singularSdk.revenue("order_submission", "INR", amount ? amount : 0);
  // window.Moengage.track_event(events_Analytics.order_submission);
};

export let eventMedicineConfirmed = () => {
  // analytics.logEvent(events_Analytics.medicine_confirmed);
  // window.fbq("trackCustom", events_Analytics.medicine_confirmed);
  // window.Moengage.track_event(events_Analytics.medicine_confirmed);
};

export let eventOrderCancelled = () => {
  // analytics.logEvent(events_Analytics.order_cancelled);
  // window.fbq("trackCustom", events_Analytics.order_cancelled);
  // window.Moengage.track_event(events_Analytics.order_cancelled);
};

export let eventSummaryScreen = () => {
  // analytics.logEvent(events_Analytics.summary_screen);
  // window.fbq("trackCustom", events_Analytics.summary_screen);
  // window.Moengage.track_event(events_Analytics.summary_screen);
};

export let eventWaitingOrderViewed = () => {
  // analytics.logEvent(events_Analytics.waiting_order_viewed);
  // window.fbq("trackCustom", events_Analytics.waiting_order_viewed);
  // window.Moengage.track_event(events_Analytics.waiting_order_viewed);
};

export let eventPastPrescriptionUploaded = () => {
  // analytics.logEvent(events_Analytics.past_prescription_uploaded);
  // window.fbq("trackCustom", events_Analytics.past_prescription_uploaded);
  // window.Moengage.track_event(events_Analytics.past_prescription_uploaded);
};

export let eventPrescriptionViewed = () => {
  // analytics.logEvent(events_Analytics.prescription_viewed);
  // window.fbq("trackCustom", events_Analytics.prescription_viewed);
  // window.Moengage.track_event(events_Analytics.prescription_viewed);
};

export let eventPrivacyPolicyViewed = () => {
  // analytics.logEvent(events_Analytics.privacy_policy_viewed);
  // window.fbq("trackCustom", events_Analytics.privacy_policy_viewed);
  // window.Moengage.track_event(events_Analytics.privacy_policy_viewed);
};

export let eventTncViewed = () => {
  // analytics.logEvent(events_Analytics.tnc_viewed);
  // window.fbq("trackCustom", events_Analytics.tnc_viewed);
  // window.Moengage.track_event(events_Analytics.tnc_viewed);
};

export let eventArticleViewed = () => {
  // analytics.logEvent(events_Analytics.article_viewed);
  // window.fbq("trackCustom", events_Analytics.article_viewed);
  // window.Moengage.track_event(events_Analytics.article_viewed);
};

export let eventPatientSelected = () => {
  // analytics.logEvent(events_Analytics.patient_selected);
  // window.fbq("trackCustom", events_Analytics.patient_selected);
  // window.Moengage.track_event(events_Analytics.patient_selected);
};

export let eventPatientAdded = () => {
  // analytics.logEvent(events_Analytics.patient_added);
  // window.fbq("trackCustom", events_Analytics.patient_added);
  // window.Moengage.track_event(events_Analytics.patient_added);
};

export let eventPatientDeleted = () => {
  // analytics.logEvent(events_Analytics.patient_deleted);
  // window.fbq("trackCustom", events_Analytics.patient_deleted);
  // window.Moengage.track_event(events_Analytics.patient_deleted);
};

export let eventAddressSelected = () => {
  // analytics.logEvent(events_Analytics.address_selected);
  // window.fbq("trackCustom", events_Analytics.address_selected);
  // window.Moengage.track_event(events_Analytics.address_selected);
};
export let eventAddressAdded = () => {
  // analytics.logEvent(events_Analytics.address_added);
  // window.fbq("trackCustom", events_Analytics.address_added);
  // window.Moengage.track_event(events_Analytics.address_added);
};

export let eventAddressdeleted = () => {
  // analytics.logEvent(events_Analytics.address_deleted);
  // window.fbq("trackCustom", events_Analytics.address_deleted);
  // window.Moengage.track_event(events_Analytics.address_deleted);
};

export let eventHomepageCallIcon = () => {
  // analytics.logEvent(events_Analytics.homepage_call_icon);
  // window.fbq("trackCustom", events_Analytics.homepage_call_icon);
  // window.Moengage.track_event(events_Analytics.homepage_call_icon);
};

export let eventHomepageCallNavbar = () => {
  // analytics.logEvent(events_Analytics.homepage_call_navbar);
  // window.fbq("trackCustom", events_Analytics.homepage_call_navbar);
  // window.Moengage.track_event(events_Analytics.homepage_call_navbar);
};

export let eventOrderDiscarded = () => {
  // analytics.logEvent(events_Analytics.order_discarded);
  // window.fbq("trackCustom", events_Analytics.order_discarded);
  // window.Moengage.track_event(events_Analytics.order_discarded);
};

export let eventCrosssellingAdded = () => {
  // analytics.logEvent(events_Analytics.crossselling_added);
  // window.fbq("trackCustom", events_Analytics.crossselling_added);
  // window.Moengage.track_event(events_Analytics.crossselling_added);
};

export let eventCrosssellingRemoved = () => {
  // analytics.logEvent(events_Analytics.crossselling_removed);
  // window.fbq("trackCustom", events_Analytics.crossselling_removed);
  // window.Moengage.track_event(events_Analytics.crossselling_removed);
};

//Adding user attributes to Moengage
export let addUserAttributesMoe = (data) => {
  // window.Moengage.add_first_name(data.customerName.split(" ")[0]);
  // if (data.customerName.split(" ").length > 1) {
  //   // window.Moengage.add_last_name(data.customerName.split(" ")[1]);
  // }
  // data.gender == "9"
  // ? window.Moengage.add_gender("F")
  // : window.Moengage.add_gender("M");
  // window.Moengage.add_email(data.emailAddress);
  // window.Moengage.add_mobile(localStorage.getItem("UserPhone"));
};

//hotjar page change for SPA
// export let hjPageChange = (url) => {
//   hotjar.stateChange(url);
// };

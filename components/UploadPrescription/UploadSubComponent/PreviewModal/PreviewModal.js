import React, { Component } from "react";
import Dialog from "@material-ui/core/Dialog";
import DialogActions from "@material-ui/core/DialogActions";
import Button from "@material-ui/core/Button";
import "./PreviewModal.css";

export default class PreviewModal extends Component {
  render() {
    return (
      <Dialog
        className="previewModalContainer"
        open={this.props.open}
        onClose={this.props.onCloseFunc}
        aria-labelledby="responsive-dialog-title"
      >
        <img
          className="previewModalImage"
          src={this.props.imageData}
          alt="prescription"
        />
        <DialogActions>
          <Button onClick={this.props.onCloseFunc} color="primary">
            Close
          </Button>
        </DialogActions>
      </Dialog>
    );
  }
}

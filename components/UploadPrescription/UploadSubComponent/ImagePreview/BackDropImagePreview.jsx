import React from "react";
import "../../../../Components/Navigation/BackDropSideBar/BackDropSideBar.css";
const BackDropSideBar = (props) =>
  props.show ? (
    <div className="Backdrop" onClick={() => props.onCloseFunc()}></div>
  ) : null;

export default BackDropSideBar;

import React from "react";
import { ImgPreviewContainer } from "./ImagePreview.style";
function ImagePreview({ imageData, onCloseFunc }) {
  return (
    <ImgPreviewContainer>
      <i
        className="fas fa-times-circle close"
        onClick={onCloseFunc}
        style={{ alignSelf: "flex-end" }}
      ></i>
      <div className="previewSection">
        <img src={imageData} alt="preview"></img>
      </div>
    </ImgPreviewContainer>
  );
}

export default ImagePreview;

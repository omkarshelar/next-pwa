import React, { Component } from "react";
import Shimmer from "react-shimmer-effect";
import "./ProductDetailsLoader.css";

export default class ProductDetailsLoader extends Component {
  render() {
    return (
      <div>
        <div className="productDetailsLoaderMainContainer">
          <div className="productDetailsLoaderContainer">
            <Shimmer>
              <div className="productDetailsLoaderImageContainer" />
            </Shimmer>
            <div className="productDetailsLoaderDetailsContainer">
              <Shimmer>
                <div className="productDetailsLoaderTitleContainer" />
              </Shimmer>
              <div className="productDetailsLoaderOthersContainer">
                <div className="productDetailsLoaderInnerContainer">
                  <Shimmer>
                    <div />
                    <div />
                  </Shimmer>
                </div>
                <div className="productDetailsLoaderInnerContainer">
                  <Shimmer>
                    <div />
                    <div />
                  </Shimmer>
                </div>
              </div>
              <div className="productDetailsLoaderInnerContainer">
                <Shimmer>
                  <div />
                  <div />
                </Shimmer>
              </div>
              <div className="productDetailsLoaderPriceContainer">
                <Shimmer>
                  <div />
                  <div />
                  <div />
                  <div />
                </Shimmer>
              </div>
            </div>
          </div>
        </div>
        <div className="productDetailsLoaderIntroContainer">
          <Shimmer>
            <div />
            <div />
            <div />
            <div />
            <div />
            <div />
          </Shimmer>
        </div>
        <div className="productDetailsLoaderIntroContainer">
          <Shimmer>
            <div />
            <div />
            <div />
            <div />
            <div />
            <div />
          </Shimmer>
        </div>
      </div>
    );
  }
}

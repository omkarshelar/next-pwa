import React, { useState } from "react";
import {
  ImgContainer,
  ImgMedDetails,
  Medwrapper,
  Price,
  Quantity,
  FirstRow,
  MedWrapperOutter,
  ViewOriginal,
  ViewOriginalSavings,
} from "./MedicineCardNew.style";
import { qtyArr } from "../../../redux/Cart/Util/CartUtil";
import optimizeImage from "../../Helper/OptimizeImage";
import checkMedType from "../../Helper/CheckMedType";
import ControlButton from "../ControlButton/ControlButton";
import ConfirmModal from "../ConfirmModal/ConfirmModal";
import Select from "react-select";
import { viewOriginalCalulations } from "../../Helper/calculationHelper";
import viewOriginalPolygon from "../../../src/Assets/viewOriginalPolygon.svg";
import window from "global";

const styles = (theme) => ({
  menuPaper: {
    maxHeight: 120,
  },
});
const MedicineCardNew = (props) => {
  const [showDelModal, setDeleteToggle] = useState(false);

  const separateImageHandler = (links) => {
    if (links) {
      let result = links.split(",");
      let filter = result.find((data) => {
        let lastIndexOfSlash = data.lastIndexOf("/");
        return data.substring(lastIndexOfSlash + 1).includes("_1");
      });
      if (filter) {
        return filter;
      } else {
        return result[0];
      }
    } else {
      return null;
    }
  };
  const setmaxQty = (arr, maxQty) => {
    let newArr = [];
    for (let i in arr) {
      if (arr[i] <= maxQty) {
        newArr.push(Number(arr[i]));
      }
    }
    return newArr;
  };
  return (
    <>
      {props.isMyOrderDetails && props.viewOriginal && (
        <ViewOriginalSavings>
          <div className="savingTitle">
            {`You${
              props.orderStatus === 57 ? " could" : ""
            } have saved ${viewOriginalCalulations(
              props.content.orgMrp,
              props.content.subsSellingPrice
            )}% extra`}
          </div>
          <div className="savingSubText">
            By opting Truemeds Recommended Brand
          </div>
          <div className="savingPointer">
            <img src={viewOriginalPolygon} alt="pointer" />
          </div>
        </ViewOriginalSavings>
      )}
      <MedWrapperOutter>
        <Medwrapper onClick={(e) => props.goToDetails(e)}>
          {!props.isSubs &&
            (props?.serviceImage ? (
              <ImgContainer>
                <img
                  className="tm-med-image"
                  src={optimizeImage(
                    separateImageHandler(props?.serviceImage),
                    "80"
                  )}
                  alt="medicine_image"
                />
              </ImgContainer>
            ) : (
              <ImgContainer>
                <img
                  className="tm-stock-image"
                  src={checkMedType(props?.medType)}
                  alt="medicine_image"
                />
              </ImgContainer>
            ))}

          <FirstRow>
            <ImgMedDetails>
              <p>{props?.medName.toLowerCase()}</p>
              <span>{props?.companyName.toLowerCase()}</span>
            </ImgMedDetails>
            <div
              className={`price-add-container  ${
                props.viewDetails ? "" : "max70"
              }`}
            >
              <Price>
                <div className="main-prices-container">
                  <span className="selling-price">
                    {props?.discount > 0 ? "" : "MRP"}
                    {` ₹${props?.sp}`}
                  </span>
                </div>
                <div className="main-price-wrap">
                  {props?.discount > 0 ? (
                    <div className="main-prices-container">
                      <span className="mrp-price">
                        MRP <del>₹{props?.mrp}</del>
                      </span>
                    </div>
                  ) : null}
                  {props?.discount > 0 ? (
                    <div className="min-discount-container">
                      <span>{props?.discount}% OFF</span>
                    </div>
                  ) : null}
                  {props.viewDetails && props.isSubs && props?.quantity ? (
                    <div className="main-prices-container">
                      <span className="mrp-quantity">{`Quantity: ${props?.quantity}`}</span>
                    </div>
                  ) : null}
                  <div></div>
                </div>
              </Price>
            </div>
          </FirstRow>
        </Medwrapper>
        <div>
          {props.coldChainDisabled ? (
            <div
              className="medCardSecondContainer"
              style={{
                flexDirection: "row",
              }}
            >
              <span className="medCardErrorMsg">Unserviceable</span>
            </div>
          ) : props.disabled ? (
            <div
              className="medCardSecondContainer"
              style={{
                flexDirection: "row",
              }}
            >
              <span className="medCardErrorMsg">Not available</span>
            </div>
          ) : !props.viewDetails ? (
            window.innerWidth > 768 ? (
              <Quantity>
                <div id="addtocart" className="quantity-select">
                  <Select
                    id="addtocart"
                    placeholder="QTY"
                    value={{ label: props.quantity, value: props.quantity }}
                    onChange={(e) => {
                      if (e.value === 0) {
                        setDeleteToggle(true);
                      } else {
                        props.quantityHandleChange(e.value);
                      }
                    }}
                    options={setmaxQty(
                      qtyArr,
                      props.maxQty ? props.maxQty : 20
                    ).map((qty) => {
                      return {
                        value: qty,
                        label: qty,
                      };
                    })}
                    isSearchable={false}
                  />
                </div>
              </Quantity>
            ) : (
              <Quantity>
                <ControlButton
                  value={props.quantity}
                  minValue={0}
                  step={1}
                  maxValue={props.maxQty}
                  changeHandler={(e) => {
                    if (e === 0) {
                      setDeleteToggle(true);
                    } else {
                      props.quantityHandleChange(e);
                    }
                  }}
                />
              </Quantity>
            )
          ) : !props.isSubs ? (
            <Quantity>
              <div className="qtyFloat">{`Qty ${props.quantity}`}</div>
            </Quantity>
          ) : (
            ""
          )}
        </div>
        <ConfirmModal
          open={showDelModal}
          Header={
            props.cartContents && props.cartContents.length === 1
              ? props.isCart
                ? "Your cart will be empty once you remove this item"
                : props.imgUpload?.uploadHistory?.length > 0
                ? "Are you sure you want to remove the item from your order"
                : "Removing this item will take you back to home page"
              : props.isCart
              ? "Are you sure you want to remove the item from your cart"
              : "Are you sure you want to remove the item from your order"
          }
          handleClose={() => setDeleteToggle(false)}
          noText={
            props.cartContents && props.cartContents.length === 1
              ? "No, don’t remove the item"
              : "No"
          }
          yesText={
            props.cartContents && props.cartContents.length === 1
              ? "Remove Item"
              : "Yes"
          }
          noMethod={() => {
            setDeleteToggle(false);
            props.quantityHandleChange(props.quantity);
          }}
          yesMethod={() => {
            setDeleteToggle(false);
            props.onClickRemove();
          }}
          type={props.cartContents && props.cartContents.length === 1 ? 1 : 0}
        />
      </MedWrapperOutter>
      {props.viewOriginal && (
        <ViewOriginal hideLast={props.hideLast}>
          <span>
            {props.isMyOrderDetails
              ? "Truemeds recommended medicine"
              : "You opted for Truemeds recommended medicine"}
          </span>
          <span
            onClick={() => {
              props.viewOriginalClick();
            }}
          >
            view original
          </span>
        </ViewOriginal>
      )}
    </>
  );
};

export default MedicineCardNew;

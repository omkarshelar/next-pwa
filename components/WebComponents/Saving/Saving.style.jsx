import styled from "styled-components";

export const SavingWrapper = styled.div`
  margin-top: 3rem;
  margin-bottom: 2rem;
  @media screen and (max-width: 568px) {
    margin: 2rem 0;
  }
  @media screen and (max-width: 480px) {
    margin: 1rem 0;
  }
`;

export const SavingsInfo = styled.div`
  position: relative;
  width: 60%;
  display: flex;
  justify-content: center;
  align-items: center;
  margin: 0 auto;
  height: 297px;
  background: linear-gradient(260.21deg, #22b573 -10.73%, #1e76ba 76.09%);
  border-radius: 206.5px;
  > div[class="placeholder"] {
    position: absolute;
    top: 2rem;
    left: -8rem;
  }

  > div[class="saving-header"] {
    width: 50%;
    display: flex;
    justify-content: flex-end;
    > span {
      width: 50%;
      /* font-family: "Century Gothic", sans-serif; */
      font-style: normal;
      font-weight: bold;
      font-size: 40px;
      line-height: 49px;
      color: #ffffff;
      display: flex;
    }
  }
  > div[class="saving-stats"] {
    width: 50%;
    display: flex;
    flex-flow: column;
    justify-content: center;
    align-items: center;
    > span {
      /* font-family: "Century Gothic", sans-serif; */
      font-style: normal;
      font-weight: bold;
      font-size: 24px;
      line-height: 29px;
      color: #ffffff;
      width: 50%;
    }
    > span:last-child {
      font-size: 55px;
      line-height: 74px;
    }
    > span[class="save-more-alt"] {
      width: 50%;
      /* font-family: "Century Gothic", sans-serif; */
      font-style: normal;
      font-weight: bold;
      font-size: 30px;
      line-height: 49px;
      color: #ffffff;
      display: flex;
    }
  }

  @media screen and (max-width: 1300px) {
    width: 70%;
  }
  @media screen and (max-width: 968px) {
    > div[class="placeholder"] {
      display: none;
    }
    > div[class="saving-header"] {
      > span {
        text-align: center;
        width: 100%;
        font-size: 35px;
        justify-content: center;
      }
      width: 100%;
    }
    > div[class="saving-stats"] {
      margin-top: 1rem;
      > span {
        width: 100%;
        text-align: center;
      }
      > span:last-child {
        font-size: 40px;
        line-height: 74px;
      }

      > span[class="save-more-alt"] {
        text-align: center;
        width: 100%;
        font-size: 25px;
        justify-content: center;
        line-height: 49px;
      }
      width: 100%;
    }
    padding: 1rem 0;
    width: 100%;
    height: auto;
    flex-flow: column;
    border-radius: 0;
  }
  @media screen and (max-width: 480px) {
    > div[class="saving-header"] {
      > span {
        font-size: 21px;
        line-height: 40px;
      }
    }
    > div[class="saving-stats"] {
      margin-top: 0;
      > span[class="save-more-alt"] {
        font-size: 16px;
        line-height: 35px;
      }
    }
  }
`;

import React, { Component } from "react";
import { SavingWrapper, SavingsInfo } from "./Saving.style";

export class Saving extends Component {
  kFormatter(num) {
    return Math.abs(num) > 999
      ? Math.sign(num) * (Math.abs(num) / 1000).toFixed(1) + "k"
      : Math.sign(num) * Math.abs(num);
  }

  render() {
    return (
      <SavingWrapper>
        <SavingsInfo>
          <div className="placeholder">
            {/* <img src={savingsIcon} alt="savings" /> */}
          </div>
          <div className="saving-header">
            <span>Save more with Truemeds</span>
          </div>
          <div className="saving-stats">
            {this.props?.saving > 0 ? (
              <>
                {" "}
                <span>You have saved</span>
                <span>₹ {this.kFormatter(this.props.saving)} </span>
              </>
            ) : (
              <span className="save-more-alt">
                You can save upto 72% on every order
              </span>
            )}
          </div>
        </SavingsInfo>
      </SavingWrapper>
    );
  }
}

export default Saving;

// Translate property

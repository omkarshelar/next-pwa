export let OrderStatusLink = {
  doctorScreen: require("../UtilOrderStatus/anim_doctor.json"),
  pharmaScreen: require("../UtilOrderStatus/anim_pharma.json"),
  processScreen: require("../UtilOrderStatus/processing.json"),
};

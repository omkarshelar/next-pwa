import React, { useState, useRef } from "react";
import EstimatedDelivery from "./SubComponents/EstimatedDelivery";
import SavingsCard from "./SubComponents/SavingsCard";
import StatusMsg from "./SubComponents/StatusMsg";
// import TotalCalculations from "./SubComponents/TotalCalculations";
import "./OrderStatusNew.css";
import "./OrderStatusNew.module.css";
import { Alert, Button, message, Tabs } from "antd";
import OrderHeader from "./SubComponents/OrderHeader";
import AlternateMobile from "./SubComponents/AlternateMobile";
import DoctorConsultationCard from "./SubComponents/DoctorConsultationCard";
import { useEffect } from "react";
import { connect } from "react-redux";
import Router, { withRouter } from "next/router";
import { orderStatusThunk } from "../../../redux/OrderDetail/Action";
import { fetchPatientByIdThunk } from "../../../redux/PatientDetails/Action";
import { fetchOrderStatusThunk } from "../../../redux/MyOrder/Action";
import { compareOrderDetailsThunk } from "../../../redux/PatientDetails/Action";
import { getDoctorRatingDetailsThunk } from "../../../redux/DoctorRating/Action";
import { fetchMedicineWithImagesThunk } from "../../../redux/SearchMedicine/Action";
import { changePaymentModeThunk } from "../../../redux/MyOrder/Action";
import AddressSection from "./SubComponents/AddressSection";
import PaymentModeWs from "./SubComponents/PaymentModeWS";
import MedicineCardAccordian from "../../WebComponents/MedicineCardAccordian/MedicineCardAccordian";
import cartBag from "../../../src/Assets/bag.svg";
import AndroidSeperator from "../../WebComponents/AndroidSeperator/AndroidSeperator";
import PrescriptionSection from "./SubComponents/PrescriptionSection";
import Timeline from "./Timeline/Timeline";
import { AndroidSeperatorDiv } from "../../WebComponents/AndroidSeperator/AndroidSeperator.style";
import Dialog from "@material-ui/core/Dialog";
import { cancelOrderThunk } from "../../../redux/ConfirmOrder/Action";
import { getAllCouponsThunk } from "../../../redux/Cart/Action";
import { BillDetails } from "../../WebComponents/BillDetails/BillDetails";
import warnIcon from "../../../src/Assets/warnIcon.svg";
import OrderStatusLoader from "../../WebComponents/Shimmer/OrderStatusLoader";
import Footer from "../../WebComponents/Footer/Footer";
import OrderStatusTabsLoader from "../../WebComponents/Shimmer/OrderStatusTabsLoader";
import {
  confirmOrderThunk,
  getAllPatientsOrderDetails,
} from "../../../redux/ConfirmOrder/Action";
import { eventOrderCancelled } from "../../../Events/Events";
import { updateOrderDetails } from "../../../redux/OrderDetail/Action";
import window from "global";
import checked from "../../../src/Assets/checkbox.svg";
import unchecked from "../../../src/Assets/checkbox_unselected.svg";
const { TabPane } = Tabs;
let orderDetailsLoader = true;
let orderStatusLoader = true;
let options = {
  root: null,
  rootMargin: "0px",
};

let serviceCallVar = false;

const OrderStatusNew = ({ orderid, ...props }) => {
  const [isServicePayment, setIsServicePayment] = useState(false);
  const [isServiceCall, setIsServiceCall] = useState(false);
  const [isServiceCallCan, setIsServiceCallCan] = useState(false);
  const [cancelModal, setCancelModal] = useState(false);
  const [ReasonId, setReasonId] = useState(1);
  const [ReasonValue, setReasonValue] = useState("");
  const [orderCancelled, setOrderCancelled] = useState(false);
  const [orderDetails, setOrderDetails] = useState(null);
  const [isMedDetails, setIsMedDetails] = useState(false);
  const [locationKeys, setLocationKeys] = useState([]);
  const [baseDiscount, setBaseDiscount] = useState(
    props.masterDetails?.baseDiscount
      ? Number(props.masterDetails.baseDiscount)
      : 20
  );
  const [deliveryCharge, setDeliveryCharge] = useState(
    props.masterDetails?.deliveryCharge
      ? Number(props.masterDetails.deliveryCharge)
      : 0
  );
  const [deliveryOnAmount, setDeliveryOnAmount] = useState(
    props.masterDetails?.deliveryOnAmount
      ? Number(props.masterDetails.deliveryOnAmount)
      : 0
  );
  const [deliveryChargeSubs, setDeliveryChargeSubs] = useState(
    props.masterDetails?.deliveryChargeSubs
      ? Number(props.masterDetails.deliveryChargeSubs)
      : 0
  );
  const [deliveryOnAmountSubs, setDeliveryOnAmountSubs] = useState(
    props.masterDetails?.deliveryOnAmountSubs
      ? Number(props.masterDetails.deliveryOnAmountSubs)
      : 0
  );
  useEffect(() => {
    setDeliveryOnAmountSubs(
      props.masterDetails?.deliveryOnAmountSubs
        ? Number(props.masterDetails.deliveryOnAmountSubs)
        : 0
    );
    setDeliveryChargeSubs(
      props.masterDetails?.deliveryChargeSubs
        ? Number(props.masterDetails.deliveryChargeSubs)
        : 0
    );
    setDeliveryOnAmount(
      props.masterDetails?.deliveryOnAmount
        ? Number(props.masterDetails.deliveryOnAmount)
        : 0
    );
    setDeliveryCharge(
      props.masterDetails?.deliveryCharge
        ? Number(props.masterDetails.deliveryCharge)
        : 0
    );
    setBaseDiscount(
      props.masterDetails?.baseDiscount
        ? Number(props.masterDetails.baseDiscount)
        : 20
    );
  }, [props.masterDetails]);
  const [activeTabKey, setActive] = useState("1");
  const cancelOrderAllowed = [1, 2, 39, 58, 142];
  const [isIntersecting, setIsIntersecting] = useState(true);
  const [isCancelError, setCancelError] = useState(false);
  const saveRef = useRef();
  const [couponDiscount, setCouponDiscount] = useState(0);
  const [headerTabActive, setHeaderTabActive] = useState(true);
  const orderHeaderRef = useRef();
  const [isIntersectingOrderHeader, setIsIntersectingOrderHeaderg] =
    useState(true);
  const [loadingDone, setLoadingDone] = useState(0);
  // const history = useHistory();

  useEffect(() => {
    // Need to resolve
    /* return history.listen((location) => {
      if (history.action === "PUSH") {
        setLocationKeys([location.key]);
      }
      if (history.action === "POP") {
        if (locationKeys[1] === location.key) {
          setLocationKeys(([_, ...keys]) => keys);
        } else {
          setLocationKeys((keys) => [location.key, ...keys]);
          onBackButtonEvent();
        }
      }
    }); */
  }, [locationKeys]);

  const onBackButtonEvent = () => {
    let isBackButtonClicked = false;
    if (!isBackButtonClicked) {
      if (window && Router?.location?.state?.isMyOrder) {
        Router.push("/myorders");
        // Router.push({
        //   pathname: "/myorders",
        //   state: {
        //     isMyOrder: true,
        //     orderType: Router?.location?.state?.orderType,
        //     patientFilter: Router?.location?.state?.patientFilter,
        //   },
        // });
        isBackButtonClicked = true;
      } else {
        window.history.pushState(null, null, Router?.goBack());
        isBackButtonClicked = false;
      }
    }
  };

  useEffect(() => {
    window.addEventListener("scroll", handleScroll);
  }, []);

  const handleScroll = () => {
    Intersection();
    // IntersectionOrderHeader();
  };
  const callBackFunction = (e) => {
    const [entry] = e;
    setIsIntersecting((prevState) => {
      if (
        prevState !== entry.isIntersecting &&
        entry.isIntersecting !== undefined
      ) {
        return entry.isIntersecting;
      } else {
        return prevState;
      }
    });
    setHeaderTabActive((prevState) => {
      if (prevState !== entry.isIntersecting) {
        return entry.isIntersecting;
      } else {
        return prevState;
      }
    });
  };

  const Intersection = () => {
    const observer = new IntersectionObserver(callBackFunction, options);
    if (saveRef.current) {
      observer.observe(saveRef.current);
    }
    return () => {
      if (saveRef.current) {
        observer.unobserve(saveRef.current);
      }
    };
  };

  useEffect(() => {
    //have kept == purposely
    if (props.updateDetail?.orderId == orderid) {
      if (props.updateDetail?.orderStatus?.serialId === 57) {
        setOrderCancelled(true);
      } else {
        setOrderCancelled(false);
      }

      if (props.updateDetail?.productSubsMappingList?.length > 0) {
        if (
          props.updateDetail?.orderStatus?.serialId === 49 ||
          props.updateDetail?.orderStatus?.serialId === 39 ||
          props.updateDetail?.orderStatus?.serialId === 2
        ) {
          props.getAllCouponsThunk({
            access_token: props.accessToken,
            customerId: props.forCustId,
            history: Router,
          });
        }
        if (isMedDetails) {
          updateProductListWithElastic();
        } else {
          setIsMedDetails(true);
          addImagesForMeds(props.updateDetail.productSubsMappingList);
        }
      } else if (
        props.updateDetail?.ImageMasterDto?.length > 0 &&
        props.updateDetail?.productSubsMappingList?.length === 0
      ) {
        setOrderDetails(props.updateDetail);
      }
    }
  }, [props.updateDetail]);

  useEffect(() => {
    if (isServiceCall) {
      props.orderStatusThunk({
        access_token: props.accessToken,
        orderIds: orderid,
        customerId: props.forCustId,
        history: Router,
      });

      props.compareOrderDetailsThunk({
        access_token: props.accessToken,
        orderId: orderid,
        history: Router,
      });

      props.getDoctorRatingDetailsThunk({
        access_token: props.accessToken,
        orderId: orderid,
        history: Router,
      });

      setIsServiceCall(false);
    }
  }, [isServiceCall]);

  useEffect(() => {
    updateProductListWithElastic();
  }, [props.medicineList]);

  const updateProductListWithElastic = () => {
    if (
      props.medicineList?.length > 0 &&
      props.updateDetail?.productSubsMappingList?.length > 0
    ) {
      let medArr = props.updateDetail.productSubsMappingList;
      let medNewArr = props.medicineList;
      for (let i in medArr) {
        let med = medArr[i];
        for (let j in medNewArr) {
          let medNew = medNewArr[j];
          med.subsFound = medNew._source.subs_found;
          if (
            medNew._source.original_product_code ===
            (props.updateDetail?.orderStatus?.serialId === 39 ||
            props.updateDetail?.orderStatus?.serialId === 49 ||
            props.updateDetail?.orderStatus?.serialId === 2
              ? med.orgProductCd
              : med.subsProductCd)
          ) {
            med.productImages = medNew._source.product_image_urls;
            break;
          }
        }
      }
      let updatedOrderDetails = props.updateDetail;
      updatedOrderDetails.productSubsMappingList = medArr;
      props.updateOrderDetails(updatedOrderDetails);
      setOrderDetails(updatedOrderDetails);
    }
  };
  const getAllProductCodes = (arr) => {
    let productCodes = [];
    for (let i in arr) {
      let med = arr[i];
      productCodes.push(
        props.updateDetail?.orderStatus?.serialId === 39 ||
          props.updateDetail?.orderStatus?.serialId === 49 ||
          props.updateDetail?.orderStatus?.serialId === 2
          ? med.orgProductCd
          : med.subsProductCd
      );
    }
    return productCodes;
  };

  const addImagesForMeds = (medArr) => {
    let productCodes = getAllProductCodes(medArr);
    let data = {
      access_token: props.accessToken?.Response?.access_token,
      medicine: {
        query: {
          bool: {
            should: [
              {
                terms: {
                  "original_product_code.keyword": productCodes,
                },
              },
            ],
          },
        },
      },
      warehouseId: 3,
    };
    props.fetchMedicineWithImagesThunk(data);
  };

  const calcDiscPreProcessing = (billArr) => {
    let filterMeds = billArr.filter(
      (data) => !data.disabled && !data.coldChainDisabled
    );
    let finalPrice = filterMeds.reduce(
      (prev, next) =>
        prev +
        (next.orgMrp -
          next.orgMrp *
            ((100 -
              (next.orgProductCd === next.subsProductCd
                ? ((next.subsMrp - next.subsSellingPrice) * 100) / next.subsMrp
                : next.orgDiscount)) /
              100)),

      0
    );
    return finalPrice.toFixed(2);
  };

  const calcTotalPreProcessing = (billArr, mrp, charge, couponValue) => {
    let finalDisc = calcDiscPreProcessing(billArr);
    let calcTotal = 0;
    if (couponValue) {
      calcTotal = mrp - finalDisc + charge - couponValue;
    } else {
      calcTotal = mrp - finalDisc + charge;
    }
    return calcTotal.toFixed(2);
  };

  // checking meds for cold chain and med active
  const checkMedsForSummary = (arr) => {
    let newArr = [];
    if (arr?.length > 0) {
      for (let i in arr) {
        let med = arr[i];
        if (!med.coldChainDisabled && !med.disabled && med.medActive) {
          newArr.push(med);
        }
      }
    }
    return newArr;
  };

  const getTotalMrpForSummary = (arr) => {
    let totalMrp = 0;
    if (arr?.length > 0) {
      for (let i in arr) {
        let med = arr[i];
        totalMrp = totalMrp + med.orgMrp;
      }
    }
    return totalMrp.toFixed(2);
  };

  const getTotalDiscountForSummary = (arr) => {
    let totalDiscount = 0;
    if (arr?.length > 0) {
      for (let i in arr) {
        let med = arr[i];
        med.orgProductCd === med.subsProductCd
          ? (totalDiscount =
              totalDiscount + (med.orgMrp - med.subsSellingPrice))
          : (totalDiscount =
              totalDiscount +
              (med.orgMrp -
                (med.orgMrp - med.orgMrp * (med.orgDiscount / 100))));
      }
    }
    return totalDiscount.toFixed(2);
  };

  const getTotalSpForSummary = (arr) => {
    let totalSp = 0;
    if (arr?.length > 0) {
      for (let i in arr) {
        let med = arr[i];
        let discount =
          med.orgProductCd !== med.subsProductCd
            ? med.orgDiscount
            : ((med.orgMrp - med.subsSellingPrice) * 100) / med.orgMrp;
        let orgMedSp = med.orgMrp - med.orgMrp * (discount / 100);
        totalSp = totalSp + orgMedSp;
      }
    }
    return totalSp;
  };

  const checkForSubs = (arr) => {
    let isSubs = false;
    for (let i in arr) {
      let med = arr[i];
      if (med.statusId === 61 || med.subsFound) {
        isSubs = true;
        break;
      }
    }
    return isSubs;
  };

  //? This function is to update useEffect everytime child component calls function
  const serviceCall = (value) => {
    value && setIsServiceCall(true);
  };

  const selectReason = (e, reason) => {
    setReasonId(e.target.value);
    setReasonValue(reason);
    setCancelError(false);
  };

  let itemCount = props.updateDetail?.productSubsMappingList.filter(
    (e) => e.medActive && !e.disabled
  ).length;

  useEffect(() => {
    if (isServiceCallCan) {
      if (props.cancelOrder.cancelMsg) {
        message.success(
          props.cancelOrder?.cancelMsg["200"]
            ? props.cancelOrder?.cancelMsg["200"]
            : "Order Cancelled Successfully"
        );
      }

      if (props.cancelOrder.cancelError) {
        message.error(
          props.cancelOrder.cancelError
            ? props.cancelOrder.cancelError
            : "Something went wrong"
        );
      }
      setTimeout(() => {
        Router.push("/");
      }, 2000);

      setIsServiceCallCan(false);
    }
  }, [props.cancelOrder]);

  let warningMsg = null;

  warningMsg = (
    <div className={"OrderStatusNew_warningWrapper"}>
      <div className={"OrderStatusNew_title"}>
        There are some changes in your order{" "}
      </div>{" "}
      <Button
        type="link"
        className={"OrderStatusNew_linkBtn"}
        onClick={() => {
          window.openSideBar(true, 12, {
            orderData: props.compareOrderData,
            medData: props.medicineList,
          });
        }}
      >
        View Modification log
      </Button>
    </div>
  );

  const cancelOrder = () => {
    if (ReasonValue) {
      setIsServiceCallCan(true);
      props
        .cancelOrderThunk({
          access_token: props.accessToken,
          orderid: orderid,
          history: Router,
          reasonId: ReasonId,
          notes: ReasonValue,
        })
        .then(() => {
          // Event_Truemeds
          // new
          eventOrderCancelled();
          setCancelModal(false);
          setOrderCancelled(true);
        });
    } else {
      setCancelError(true);
      // message.error("Please select a reason");
    }
  };

  useEffect(() => {
    if (isServicePayment) {
      if (props.myOrder.changePaymentModeSuccess) {
        message.success(
          props.myOrder.changePaymentModeSuccess
            ? props.myOrder.changePaymentModeSuccess
            : "Payment Mode changed successfully"
        );
        props
          .orderStatusThunk({
            access_token: props.accessToken,
            orderIds: orderid,
            customerId: props.forCustId,
            history: Router,
          })
          .then(() => {
            orderDetailsLoader = false;
          });
        props
          .fetchOrderStatusThunk({
            accessToken: props.accessToken,
            orderId: orderid,
            history: Router,
          })
          .then(() => {
            orderStatusLoader = false;
          });
      } else {
        if (
          props.myOrder.changePaymentModeError ===
          "Offer is applied on this payment mode, Delete invoice & then change payment mode if required."
        ) {
          message.error("Offer is applied on this payment mode");
        } else {
          message.error(props.myOrder.changePaymentModeError);
        }
      }
      setIsServicePayment(false);
    }
  }, [
    props.myOrder.changePaymentModeError,
    props.myOrder.changePaymentModeSuccess,
    isServicePayment,
  ]);

  useEffect(() => {
    if (
      props.couponDetails?.payload?.data?.length > 0 &&
      props.updateDetail?.offerId
    ) {
      onClickCalculateSummaryCouponDiscount(
        getCouponDetailsByOfferId(
          props.couponDetails?.payload?.data,
          props.updateDetail?.offerId
        )
      );
    }
  }, [props.couponDetails]);

  const getCouponDetailsByOfferId = (arr, offerId) => {
    let couponDetails = {};
    if (arr?.length > 0) {
      for (let i in arr) {
        let coupon = arr[i];
        if (coupon.offerId === offerId) {
          couponDetails = coupon;
          break;
        }
      }
    }
    return couponDetails;
  };

  const filterMedList = (arr) => {
    let finalArr = [];
    if (arr?.length > 0) {
      for (let i in arr) {
        let med = arr[i];
        if (!med.coldChainDisabled && !med.disabled && med.medActive) {
          finalArr.push(med);
        }
      }
    }
    return finalArr;
  };

  const calculateCouponDiscountFromPercentageInSummary = (data) => {
    let finalValue = 0;
    let discount = calculateDiscountFromMedsArrInSummary(
      data.discountValue,
      filterMedList(props.updateDetail?.productSubsMappingList)
    );
    if (Math.round(discount) <= data.maxDiscount) {
      finalValue = discount;
    } else {
      finalValue = data.maxDiscount;
    }
    return finalValue;
  };

  const calculateDiscountFromMedsArrInSummary = (discount, arr) => {
    let val = 0;
    if (arr?.length > 0) {
      for (let i in arr) {
        let med = arr[i];
        let newDiscount =
          med.orgProductCd !== med.subsProductCd
            ? med.orgDiscount
            : ((med.orgMrp - med.subsSellingPrice) * 100) / med.orgMrp;
        if (Math.round(newDiscount) >= baseDiscount && newDiscount <= 25) {
          let discountValue = (med.orgMrp * discount) / 100;
          val = val + discountValue;
        }
      }
    }
    return val;
  };

  // function to calculate coupon discount
  const onClickCalculateSummaryCouponDiscount = (data) => {
    let couponDiscount = 0;
    // check for coupon id
    if (data?.discountType?.serialId === 76) {
      couponDiscount = data.discountValue;
      setCouponDiscount(couponDiscount);
    } else {
      couponDiscount = calculateCouponDiscountFromPercentageInSummary(data);
      setCouponDiscount(couponDiscount);
    }
  };
  const handlePaymentChange = () => {
    props
      .changePaymentModeThunk({
        accessToken: props.accessToken,
        orderId: orderid,
        paymentModeId: 17,
        history: Router,
      })
      .then(() => {
        setIsServicePayment(true);
      });
  };

  let statusMsg = "We have received your order";

  if (props?.orderStatus?.orderTracking?.statusId) {
    if ([142].includes(props?.orderStatus?.orderTracking?.statusId)) {
      statusMsg = "Order is on track";
    }

    if ([60, 59, 66].includes(props?.orderStatus?.orderTracking?.statusId)) {
      statusMsg = "Order has been dispatched";
    }

    if (
      props?.orderStatus?.orderTracking?.statusId === 60 &&
      props.orderStatus?.deliveryPartnerName
    ) {
      statusMsg = "Order is Out for Delivery";
    }

    if ([58].includes(props?.orderStatus?.orderTracking?.statusId)) {
      statusMsg = "Payment pending";
    }
  }

  if (orderCancelled) {
    statusMsg = "Order Cancelled";
  }

  return (
    <>
      <div className={"OrderStatusNew_orderStatusWrapper orderStatusWrapper"}>
        <div className={"OrderStatusNew_leftContainer"}>
          <StatusMsg
            text={statusMsg}
            orderCancelled={orderCancelled}
            isLoading={props.isLoading}
          />
          {props?.isMobile && (
            <SavingsCard
              updateDetail={props.updateDetail}
              medicineList={props.medicineList}
              isLoading={props.isLoading}
            />
          )}
          {!orderCancelled && (
            <>
              <div ref={saveRef}></div>
              <div className={"OrderStatusNew_estimatedWrapper"}>
                <EstimatedDelivery
                  deliveryDate={props.updateDetail?.deliveryDate}
                  isIntersecting={isIntersecting}
                  isLoading={props.isLoading}
                />
              </div>
            </>
          )}
          {!props.isLoading && props.compareOrderData?.isModified && (
            <>
              <Alert
                type="warning"
                className={"OrderStatusNew_warnAlert"}
                message={warningMsg}
                icon={<img src={warnIcon} alt="warnIcon" />}
                showIcon
              />
            </>
          )}
          <div className="tabWrapper">
            {orderCancelled ? (
              props.isLoading ? (
                <OrderStatusLoader />
              ) : (
                <>
                  <DetailsTab
                    setCancelModal={(e) => setCancelModal(e)}
                    orderid={orderid}
                    patientData={props.patientData}
                    orderDetails={
                      orderDetails ? orderDetails : props.updateDetail
                    }
                    history={Router}
                    imgUpload={props.imgUpload}
                    cancelOrderAllowed={cancelOrderAllowed}
                    orderStatus={props.orderStatus}
                    updateDetail={props.updateDetail}
                    isMobile={props.isMobile}
                  />
                  {props?.isMobile && (
                    <>
                      <AndroidSeperator showMob={true} />
                      <BillDetails
                        details={true}
                        isOrderStatus={true}
                        updateDetail={
                          orderDetails ? orderDetails : props.updateDetail
                        }
                        baseDiscount={baseDiscount}
                        deliveryCharge={deliveryCharge}
                        deliveryOnAmount={deliveryOnAmount}
                        deliveryChargeSubs={deliveryChargeSubs}
                        deliveryOnAmountSubs={deliveryOnAmountSubs}
                        calcDiscPreProcessing={calcDiscPreProcessing}
                        calcTotalPreProcessing={calcTotalPreProcessing}
                        getTotalSpForSummary={getTotalSpForSummary}
                        checkMedsForSummary={checkMedsForSummary}
                        getTotalMrpForSummary={getTotalMrpForSummary}
                        getTotalDiscountForSummary={getTotalDiscountForSummary}
                        checkForSubs={checkForSubs}
                        isLoading={props.isLoading}
                        promoCode={props.updateDetail?.Promocode}
                        promoCodeValue={
                          props.updateDetail?.orderStatus?.serialId === 49 ||
                          props.updateDetail?.orderStatus?.serialId === 39 ||
                          props.updateDetail?.orderStatus?.serialId === 2
                            ? couponDiscount
                            : 0
                        }
                        patientOrderDetails={props.patientOrderDetails}
                      />
                      {cancelOrderAllowed.includes(
                        props?.orderStatus?.orderTracking?.statusId
                      ) && (
                        <>
                          <AndroidSeperator
                            colorCustom={!props?.isMobile ? "#DCE4F1" : false}
                            showWeb={true}
                            showMob={true}
                          />
                          <div
                            onClick={() => setCancelModal(true)}
                            className={"OrderStatusNew_cancelButton"}
                          >
                            Cancel Order
                          </div>
                        </>
                      )}
                    </>
                  )}
                </>
              )
            ) : props.isLoading ? (
              <OrderStatusTabsLoader />
            ) : (
              <>
                <div ref={orderHeaderRef} id="orderHeaderRef"></div>
                <Tabs
                  defaultActiveKey={activeTabKey}
                  activeKey={activeTabKey}
                  tabBarStyle={{ color: "#8897A2" }}
                  onTabClick={(key, event) => {
                    setActive(key);
                  }}
                >
                  <TabPane tab="Order Status" key="1">
                    {props.isLoading ? (
                      <OrderStatusLoader />
                    ) : (
                      <>
                        <OrderHeader
                          orderId={orderid}
                          patientName={props.patientData?.patientName}
                        />

                        <div className={"OrderStatusNew_timelineWrap"}>
                          {props.orderStatus?.orderTracking?.orderStatus
                            .length > 0 ? (
                            <Timeline
                              orderStatus={props.orderStatus}
                              updateDetail={props.updateDetail}
                              orderId={orderid}
                              access_token={props.accessToken}
                              doctorData={
                                props.doctorRatingData?.getDoctorRatingSuccess
                              }
                              history={Router}
                              customerId={props.forCustId}
                              serviceCall={(value) => serviceCall(value)}
                              myOrder={props.myOrder}
                              handlePaymentChange={() => handlePaymentChange()}
                            />
                          ) : (
                            <div className={"OrderStatusNew_fetchErrorText"}>
                              {"Sorry! Unable to fetch order status history."}
                            </div>
                          )}

                          <AndroidSeperatorDiv
                            colorCustom={!props?.isMobile ? "#DCE4F1" : false}
                            showWeb={true}
                          />
                        </div>

                        <div className={"OrderStatusNew_mWebSepWrap"}>
                          <AndroidSeperatorDiv
                            showMob={true}
                            colorCustom={"#E9EEF6"}
                          />
                        </div>
                        <AlternateMobile
                          orderId={orderid}
                          currentMobNo={props.userDto?.mobileNo}
                          altMob={props.updateDetail?.alternateNumber}
                          serviceCall={(value) => serviceCall(value)}
                        />
                      </>
                    )}
                  </TabPane>
                  <TabPane tab="Order Details" key="2">
                    {props.isLoading ? (
                      <OrderStatusLoader />
                    ) : (
                      <>
                        <DetailsTab
                          setCancelModal={(e) => setCancelModal(e)}
                          orderid={orderid}
                          patientData={props.patientData}
                          orderDetails={
                            orderDetails ? orderDetails : props.updateDetail
                          }
                          history={Router}
                          imgUpload={props.imgUpload}
                          cancelOrderAllowed={cancelOrderAllowed}
                          orderStatus={props.orderStatus}
                          updateDetail={props.updateDetail}
                          isMobile={props.isMobile}
                        />
                        {props?.isMobile && (
                          <>
                            <AndroidSeperator showMob={true} />
                            <BillDetails
                              details={true}
                              isOrderStatus={true}
                              updateDetail={props.updateDetail}
                              baseDiscount={baseDiscount}
                              deliveryCharge={deliveryCharge}
                              deliveryOnAmount={deliveryOnAmount}
                              deliveryChargeSubs={deliveryChargeSubs}
                              deliveryOnAmountSubs={deliveryOnAmountSubs}
                              calcDiscPreProcessing={calcDiscPreProcessing}
                              calcTotalPreProcessing={calcTotalPreProcessing}
                              getTotalSpForSummary={getTotalSpForSummary}
                              checkMedsForSummary={checkMedsForSummary}
                              getTotalMrpForSummary={getTotalMrpForSummary}
                              getTotalDiscountForSummary={
                                getTotalDiscountForSummary
                              }
                              checkForSubs={checkForSubs}
                              isLoading={props.isLoading}
                              promoCode={props.updateDetail?.Promocode}
                              promoCodeValue={
                                props.updateDetail?.orderStatus?.serialId ===
                                  49 ||
                                props.updateDetail?.orderStatus?.serialId ===
                                  39 ||
                                props.updateDetail?.orderStatus?.serialId === 2
                                  ? couponDiscount
                                  : 0
                              }
                              patientOrderDetails={props.patientOrderDetails}
                            />
                            {cancelOrderAllowed.includes(
                              props?.orderStatus?.orderTracking?.statusId
                            ) && (
                              <>
                                <AndroidSeperator
                                  colorCustom={
                                    !props?.isMobile ? "#DCE4F1" : "#E9EEF6"
                                  }
                                  showWeb={true}
                                  showMob={true}
                                />
                                <div
                                  onClick={() => setCancelModal(true)}
                                  className={"OrderStatusNew_cancelButton"}
                                >
                                  Cancel Order
                                </div>
                              </>
                            )}
                          </>
                        )}
                      </>
                    )}
                  </TabPane>
                </Tabs>
              </>
            )}
          </div>
        </div>
        <div className={"OrderStatusNew_rightContainer"}>
          <div className={"OrderStatusNew_rightContainerInner"}>
            {!props?.isMobile && (
              <SavingsCard
                updateDetail={props.updateDetail}
                medicineList={props.medicineList}
                isLoading={props.isLoading}
              />
            )}
            {!props?.isMobile && (
              <BillDetails
                details={true}
                isOrderStatus={true}
                updateDetail={props.updateDetail}
                baseDiscount={baseDiscount}
                deliveryCharge={deliveryCharge}
                deliveryOnAmount={deliveryOnAmount}
                deliveryChargeSubs={deliveryChargeSubs}
                deliveryOnAmountSubs={deliveryOnAmountSubs}
                calcDiscPreProcessing={calcDiscPreProcessing}
                calcTotalPreProcessing={calcTotalPreProcessing}
                getTotalSpForSummary={getTotalSpForSummary}
                checkMedsForSummary={checkMedsForSummary}
                getTotalMrpForSummary={getTotalMrpForSummary}
                getTotalDiscountForSummary={getTotalDiscountForSummary}
                checkForSubs={checkForSubs}
                isLoading={props.isLoading}
                promoCode={props.updateDetail?.Promocode}
                promoCodeValue={
                  props.updateDetail?.orderStatus?.serialId === 49 ||
                  props.updateDetail?.orderStatus?.serialId === 39 ||
                  props.updateDetail?.orderStatus?.serialId === 2
                    ? couponDiscount
                    : 0
                }
                patientOrderDetails={props.patientOrderDetails}
              />
            )}

            {props.doctorRatingData?.getDoctorRatingSuccess &&
            !props?.isMobile &&
            Number(activeTabKey) === 1 ? (
              (props?.orderStatus?.orderTracking?.orderStatus
                ?.map((e) => e.statusId)
                .includes(142) ||
                props?.orderStatus?.orderTracking?.drCallTime) &&
              ![39, 2, 49].includes(
                props?.orderStatus?.orderTracking?.statusId
              ) ? (
                <DoctorConsultationCard
                  doctorData={props.doctorRatingData?.getDoctorRatingSuccess}
                  orderId={orderid}
                  customerId={props.forCustId}
                  serviceCall={(value) => serviceCall(value)}
                  isLoading={props.isLoading}
                />
              ) : null
            ) : null}
          </div>
        </div>
        <Dialog
          fullScreen={false}
          open={cancelModal}
          aria-labelledby="responsive-dialog-title"
          classes={{ paper: "cancel-rectangle" }}
          id="responsive-dialog-title"
          onClose={() => setCancelModal(false)}
        >
          <div className={"OrderStatusNew_cancelModalWrapper"}>
            <div className={"OrderStatusNew_cancelTitle"}>
              Why do you want to cancel the order
            </div>
            <div className={"OrderStatusNew_reasonListWrap"}>
              {props?.list?.map((data) => (
                <label class="containerWrapper">
                  {data.value}
                  <input
                    type="radio"
                    value={data.reasonId}
                    name="CANCEL_REASON"
                    onChange={(e) => selectReason(e, data.value)}
                    style={{ marginTop: "5px" }}
                    defaultChecked={data.reasonId == ReasonId}
                  />
                  <span
                    className="checkmark"
                    style={{
                      backgroundImage: `url(${
                        data.reasonId == ReasonId ? checked : unchecked
                      })`,
                    }}
                  ></span>
                </label>
              ))}
            </div>
            {isCancelError && (
              <div className={"OrderStatusNew_canError"}>
                Please select an option
              </div>
            )}

            <div className={"OrderStatusNew_buttonWrap"}>
              <div
                className={"OrderStatusNew_canBtn"}
                onClick={() => {
                  cancelOrder();
                }}
              >
                Cancel Order
              </div>
              <div
                onClick={() => {
                  setCancelModal(false);
                  setCancelError(false);
                }}
                className={"OrderStatusNew_dntCanBtn"}
              >
                Don't Cancel
              </div>
            </div>
          </div>
        </Dialog>
      </div>
      {!props?.isMobile && <Footer />}
      {props?.isMobile && !headerTabActive && (
        <div className={"OrderStatusNew_tabsFixedHeader"}>
          <div
            onClick={() => {
              setActive("1");
            }}
            className={`${"OrderStatusNew_tabsFixedHeaderItem"} ${
              activeTabKey === "1"
                ? "OrderStatusNew_tabsFixedHeaderItemActive"
                : ""
            }`}
          >
            Order Status
          </div>
          <div
            onClick={() => {
              setActive("2");
            }}
            className={`${"OrderStatusNew_tabsFixedHeaderItem"} ${
              activeTabKey === "2"
                ? "OrderStatusNew_tabsFixedHeaderItemActive"
                : ""
            }`}
          >
            Order Details
          </div>
        </div>
      )}
    </>
  );
};

let mapStateToProps = (state) => ({
  Stepper: state.stepper,
  accessToken: state.loginReducer?.verifyOtpSuccess?.Response?.access_token,
  userDto: state.loginReducer?.verifyOtpSuccess?.CustomerDto,
  loginDetails: state.loginReducer,
  medConfirm: state.confirmMedicineReducer,
  forCustId: state.loginReducer?.verifyOtpSuccess?.CustomerId,
  updateDetail: state.orderStatusUpdate?.OrderStatusData,
  patientData: state.patientData?.fetchPatientByIdSuccess,
  orderStatus: state.myOrder.orderStatusSuccess,
  doctorRatingData: state.doctorRatings,
  list: state.reasonList?.reasonList,
  cancelOrder: state.cancelOrder,
  tempCheck: state,
  medicineList: state.searchMedicine?.medDataWithImgSuccess?.hits,
  isLoading: state.loader?.isLoading,
  compareOrderData: state.patientData?.compareOrderDetailsSuccess,
  patientDataDetails: state.patientData,
  myOrder: state.myOrder,
  couponDetails: state.cartData?.couponDetails,
  patientOrderDetails: state.patientOrderDetails,
  masterDetails: state.masterDetails?.MasterDetails,
});

export default withRouter(
  connect(mapStateToProps, {
    orderStatusThunk,
    fetchPatientByIdThunk,
    fetchOrderStatusThunk,
    getDoctorRatingDetailsThunk,
    cancelOrderThunk,
    fetchMedicineWithImagesThunk,
    compareOrderDetailsThunk,
    changePaymentModeThunk,
    getAllCouponsThunk,
    getAllPatientsOrderDetails,
    updateOrderDetails,
  })(OrderStatusNew)
);

const DetailsTab = (props) => {
  let itemCount = props.orderDetails?.productSubsMappingList.filter(
    (e) => e.medActive && !e.disabled
  ).length;

  return (
    <>
      <OrderHeader
        orderId={props.orderid}
        patientName={props.patientData?.patientName}
        isOrderDetails={true}
        setParentIntersection={(e) => {
          props.setParentIntersection(e);
        }}
      />
      {props.updateDetail?.AddressDetails && (
        <AddressSection addressDetails={props.updateDetail.AddressDetails} />
      )}
      {props.updateDetail?.paymentMode && (
        <PaymentModeWs paymentMode={props.updateDetail.paymentMode} />
      )}
      {props.updateDetail?.productSubsMappingList?.length > 0 && (
        <div className="orderSummaryMedcinesAddedContainer">
          <p>
            <img src={cartBag} />
            {itemCount === 0
              ? "No items ordered"
              : itemCount === 1
              ? "1 Item ordered"
              : itemCount + " Items ordered"}
          </p>
        </div>
      )}
      <div className="medicineDetailsWrap">
        {props.orderDetails?.productSubsMappingList?.length > 0 &&
          props.orderDetails.productSubsMappingList.map((content, index) => (
            <MedicineCardAccordian
              updateDetails={props.orderDetails}
              details={true}
              imgUpload={props.imgUpload}
              summary={false}
              content={content}
              {...props}
              index={index}
              isOrderSummary={true}
              isViewDetails={true}
              viewOriginal={true}
              isOrderStatus={true}
            />
          ))}
      </div>
      {props.updateDetail?.ImageMasterDto &&
        props.updateDetail?.ImageMasterDto.length > 0 && (
          <>
            <AndroidSeperator showWeb={true} showMob={true} />
            <PrescriptionSection updateDetail={props.updateDetail} />
          </>
        )}
      {!props?.isMobile &&
        props.cancelOrderAllowed.includes(
          props?.orderStatus?.orderTracking?.statusId
        ) && (
          <>
            <AndroidSeperator
              colorCustom={!props?.isMobile ? "#DCE4F1" : false}
              showWeb={true}
              showMob={true}
            />
            <div
              onClick={() => {
                props.setCancelModal(true);
              }}
              className={"OrderStatusNew_cancelButton"}
            >
              Cancel Order
            </div>
          </>
        )}
    </>
  );
};

import React from "react";
import CustomButton from "../../Components/CustomButton/CustomButton";
import BackDropSideBar from "../UploadPrescription/UploadSubComponent/ImagePreview/BackDropImagePreview";
import { ListWrapper } from "./OrderStatusShow.style";

function CancelReasonList({
  reasons,
  selectReason,
  handleClose,
  deleteMethod,
}) {
  return (
    <>
      <div className="newCustomLoaderWrapper">
        <ListWrapper>
          <h5>Select Cancellation Reason</h5>
          <div style={{ display: "flex", flexFlow: "column" }}>
            {reasons.map((data) => (
              <label style={{ display: "flex" }}>
                <input
                  type="radio"
                  value={data.reasonId}
                  name="CANCEL_REASON"
                  onChange={(e) => selectReason(e, data.value)}
                  style={{ marginTop: "5px" }}
                  defaultChecked={data.reasonId === 4 ? true : false}
                />
                <p
                  style={{
                    margin: "0 0 5px 5px",
                  }}
                >
                  {data.value}
                </p>
              </label>
            ))}
          </div>
        </ListWrapper>
        <div style={{ display: "flex", justifyContent: "center" }}>
          <CustomButton cancelClose onClick={() => handleClose()}>
            Close
          </CustomButton>
          <CustomButton cancelSubmit onClick={() => deleteMethod()}>
            Cancel Order
          </CustomButton>
        </div>
      </div>
      <BackDropSideBar show={true} onCloseFunc={handleClose} />
    </>
  );
}

export default CancelReasonList;

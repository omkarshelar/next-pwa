import React from "react";
import "./ActiveTile.module.css";

const ActiveTile = (props) => {
  return (
    <>
      <div className={"ActiveTile_dummyActiveTileHeight"}></div>
      <div className={"ActiveTile_activeTileWrap"}>
        <div className={"ActiveTile_pointer"}></div>
        <div className={"ActiveTile_displayLogo"}>
          <img src={props.imgLogo} />
        </div>
        <div className={"ActiveTile_activeTileContent"}>
          {props.tileType === 2 ? (
            props.children
          ) : (
            <>
              <div className={"ActiveTile_activeTileTitle"}>
                <div className={"ActiveTile_activeTileTitleText"}>
                  {props.title}
                </div>
                {props.subText && (
                  <div className={"ActiveTile_activeTileSubTitleText"}>
                    {props.subText}
                  </div>
                )}
              </div>
              <div className={"ActiveTile_activeTileDesc"}>{props.desc}</div>
            </>
          )}
        </div>
      </div>
    </>
  );
};

export default ActiveTile;

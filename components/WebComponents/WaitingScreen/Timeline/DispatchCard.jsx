import React from "react";
import "./DispatchCard.module.css";
import whiteChevronRight from "../../../../src/Assets/chevronWhiteRight_Mweb.svg";

import whiteChevronRightWeb from "../../../../src/Assets/dWeb_Whitw_chevron.svg";

const DispatchCard = (props) => {
  return (
    <div className={"DispatchCard_disptachCardWrap"}>
      <div className={"DispatchCard_cardTitle"}>{props.title}</div>
      {props.isPendingInfo ? (
        <div className={"DispatchCard_detailsTitle"}>
          Information will be available soon...
        </div>
      ) : (
        <>
          {props.subTitleArray &&
            props.subTitleArray.length > 0 &&
            props.subTitleArray.map((e) => (
              <div className={"DispatchCard_detailsWrap"}>
                <div className={"DispatchCard_detailsTitle"}>{e.title}</div>
                <div className={"DispatchCard_detailsValue"}>{e.value}</div>
              </div>
            ))}
          <div className={"DispatchCard_orderTrackingCta"}>
            <div
              className={"DispatchCard_orderTrackingCtaText"}
              onClick={() => {
                if (props.clickPostTrackingUrl) {
                  window.open(props.clickPostTrackingUrl, "_blank");
                }
              }}
            >
              Track Your Order Here
            </div>
            <div>
              <img
                src={
                  window.innerWidth > 768
                    ? whiteChevronRightWeb
                    : whiteChevronRight
                }
              />
            </div>
          </div>
        </>
      )}
    </div>
  );
};

export default DispatchCard;

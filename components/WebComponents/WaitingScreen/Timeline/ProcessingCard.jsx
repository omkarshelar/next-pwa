import React from "react";
import "./ProcessingCard.module.css";
import baseTick from "../../../../src/Assets/baseTick.svg";
import moment from "moment";

const ProcessingCard = (props) => {
  let currentTime = moment();
  let modifiedOn = props.modifiedOn ? moment(props.modifiedOn) : moment();

  let timeDiffInMinutes = currentTime.diff(modifiedOn, "minutes");

  let statusText = "Packaging order";

  if (timeDiffInMinutes < 360) {
    statusText = "Assigned to warehouse";
  } else if (timeDiffInMinutes < 720) {
    statusText = "Warehouse team at work";
  } else if (timeDiffInMinutes < 1080) {
    statusText = "3 step quality check";
  } else {
    statusText = "Packaging order";
  }
  return (
    <div className={"ProcessingCard_processingWrap"}>
      <div className={"ProcessingCard_title"}>Order is Being Processed</div>
      <div className={"ProcessingCard_statusWrap"}>
        <div className={"ProcessingCard_icon"}>
          <img src={baseTick} />
        </div>
        <div className={"ProcessingCard_subTitle"}>{statusText}</div>
      </div>
    </div>
  );
};

export default ProcessingCard;

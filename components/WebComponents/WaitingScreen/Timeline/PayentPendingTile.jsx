import React, { Component, useEffect, useState } from "react";
import "./PaymentPendingTile.module.css";
import Prompt from "../../../../src/Assets/paymentPrompt.svg";
import Axios from "axios";
import {
  ThirdPartySevice_URL,
  PWA_static_URL,
} from "../../../../constants/Urls";
import { connect } from "react-redux";
import { withRouter } from "next/router";
import { changePaymentModeThunk } from "../../../../redux/MyOrder/Action";
import { message } from "antd";
import { orderStatusThunk } from "../../../../redux/OrderDetail/Action";
import { fetchOrderStatusThunk } from "../../../../redux/MyOrder/Action";

export class PaymentPendingTile extends Component {
  cashFreeService = (orderId) => {
    const Uri = `${ThirdPartySevice_URL}/createPaymentUrlInCashFree?orderId=${orderId}&returnUrl=${PWA_static_URL}`;
    const config = {
      headers: {
        "Content-Type": "application/json",
        Authorization: "Bearer " + this.props?.access_token,
        "Access-Control-Allow-Origin": "*",
      },
    };
    Axios.get(Uri, config)
      .then((response) => {
        if (response.status === 200) {
          // window.open(response.data.paymentLink);
          window.location.href = response.data.paymentLink;
        }
      })
      .catch((err) => {
        console.error("createPaymentUrlInCashFree", err.response);
      });
  };

  handleSuccess = () => {
    message.success(
      this.props.myOrder.changePaymentModeSuccess
        ? this.props.myOrder.changePaymentModeSuccess
        : "Payment Mode changed successfully"
    );
    this.props.orderStatusThunk({
      access_token: this.props.access_token,
      orderIds: this.props.orderId,
      customerId: this.props.customerId,
      history: this.props.history,
    });
    this.props.fetchOrderStatusThunk({
      accessToken: this.props.access_token,
      orderId: this.props.orderId,
      history: this.props.history,
    });
  };

  handleError = () => {
    if (
      this.props.myOrder.changePaymentModeError ===
      "Offer is applied on this payment mode, Delete invoice & then change payment mode if required."
    ) {
      message.error("Offer is applied on this payment mode");
    } else {
      message.error(this.props.myOrder.changePaymentModeError);
    }
  };

  render() {
    return (
      <div className={"PaymentPendingTile_paymentPendingWrap"}>
        <div className={"PaymentPendingTile_leftTile"}>
          <div className={"PaymentPendingTile_iconTile"}>
            <img src={Prompt} />
          </div>
          <div className={"PaymentPendingTile_Title"}>Payment Pending</div>
        </div>
        <div className={"PaymentPendingTile_rightTile"}>
          <div
            className={"PaymentPendingTile_paymentModeTitle"}
            onClick={() => this.props.handlePaymentChange()}
          >
            Change to COD
          </div>
          <div
            onClick={() => this.cashFreeService(this.props.orderId)}
            className={"PaymentPendingTile_payNowCta"}
          >
            Pay Now
          </div>
        </div>
      </div>
    );
  }
}

let mapStateToProps = (state) => ({
  accessToken: state.loginReducer?.verifyOtpSuccess,
  myOrder: state.myOrder,
});

export default withRouter(
  connect(mapStateToProps, {
    changePaymentModeThunk,
    orderStatusThunk,
    fetchOrderStatusThunk,
  })(PaymentPendingTile)
);

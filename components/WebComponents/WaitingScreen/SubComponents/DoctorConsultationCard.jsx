///? Default IMPORTS
import React from "react";

///? Service IMPORTS

///? Component & Styling IMPORTS
import "./DoctorConsultationCard.module.css";
import doctorShield from "../../../../src/Assets/doctorSheild.svg";
import greyStar from "../../../../src/Assets/greyStar.svg";
import goldenStar from "../../../../src/Assets/starGolden.svg";
import chevIcon from "../../../../src/Assets/chevron_rate_doctor.svg";
import DoctorRatingsLoader from "../../../WebComponents/Shimmer/DoctorRatingsLoader";
///? Constants

const DoctorConsultationCard = (props) => {
  if (props.isLoading) {
    return <DoctorRatingsLoader />;
  } else {
    return (
      <>
        <div
          className={
            props.isSidePanel
              ? "doctorCard_cardPanelWrap"
              : "doctorCard_cardWrap"
          }
        >
          <div
            className={
              props.isSidePanel
                ? "doctorCard_callWithTextPanel"
                : "doctorCard_callWithText"
            }
          >
            You had a call with
          </div>
          <div className={"doctorCard_doctorCard"}>
            <div className={"doctorCard_doctorName"}>
              {props.doctorData?.doctorName}
            </div>
            <div className={"doctorCard_doctorDesc"}>
              {props.doctorData?.specialist &&
                props.doctorData?.specialist?.map((i, index) => (
                  <span key={index}>
                    {index > 0 ? " | " : ""}
                    {i}
                  </span>
                ))}
            </div>
            <div className={"doctorCard_ratingWrap"}>
              <StarRating active={props.doctorData?.rating >= 1} />
              <StarRating active={props.doctorData?.rating >= 2} />
              <StarRating active={props.doctorData?.rating >= 3} />
              <StarRating active={props.doctorData?.rating >= 4} />
              <StarRating active={props.doctorData?.rating >= 5} />
            </div>
          </div>
          {props.doctorData?.customerCount ? (
            <div
              className={
                props.isSidePanel
                  ? "doctorCard_doctorFeedbackPanel"
                  : "doctorCard_doctorFeedback"
              }
            >
              <div className={"doctorCard_logoWrap"}>
                <img src={doctorShield} alt="doctorShield" />
              </div>
              <div className={"doctorCard_doctorFeedbackText"}>
                {props.doctorData?.doctorName} has consulted{" "}
                <b>{props.doctorData?.customerCount}</b> Truemeds customers
              </div>
            </div>
          ) : null}
        </div>
        {!props.isSidePanel && props.doctorData?.orderRating === 0 && (
          <div
            className={"doctorCard_rateCTA"}
            onClick={() => {
              window.openSideBar(true, 11, {
                doctorData: props.doctorData,
                orderId: props.orderId,
                customerId: props.customerId,
                ratings: 0,
                serviceCall: props.serviceCall,
              });
              /* switch (props.callType) {
                case 1:
                  eventOrderStatusRateYourDoctor();
                  break;
                case 2:
                  eventMyOrdersRate();
                  break;
                default:
                  eventOrderStatusRateYourDoctor();
                  break;
              } */
            }}
          >
            <div className={"doctorCard_rateCTAText"}>Rate your doctor</div>
            <div className={"doctorCard_chevIcon"}>
              <img src={chevIcon} alt="chevIcon" />
            </div>
          </div>
        )}
      </>
    );
  }
};

export default DoctorConsultationCard;

const StarRating = (props) => {
  if (props.active) {
    return (
      <div className={"doctorCard_activeStar"}>
        <img src={goldenStar} alt="goldenStart" />
      </div>
    );
  } else {
    return (
      <div className={"doctorCard_disabledStar"}>
        <img src={greyStar} alt="grayStart" />
      </div>
    );
  }
};

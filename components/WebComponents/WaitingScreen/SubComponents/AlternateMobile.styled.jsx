import styled from "styled-components";

export const AltMobileWrapper = styled.div`
  border: 1px solid #c4cfd7;
  border-radius: 5px;
  @media screen and (max-width: 768px) {
    border: none;
    border-radius: 0px;
    padding: 0px 15px;
  }
`;

export const RegisterMobWrapper = styled.div`
  display: flex;
  flex-flow: ${(props) => (props.altNum ? "column" : "row")};
  justify-content: space-between;
  padding: 1.25em 1em;
  border-top: ${(props) => (props.altNum ? "1px solid #c4cfd7" : "")};

  .registeredNumberTitleWrap {
    display: flex;
    gap: 5px;
  }

  .registeredMobHeader {
    display: flex;
    flex-flow: row nowrap;
    justify-content: space-between;
    gap: 0.5em;
  }

  .registeredNumberTitle {
    color: #8897a2;
    font-size: 14px;
    font-weight: 400;
  }
  .registeredNumberValue {
    color: #4f585e;
    font-size: 14px;
    font-weight: 700;
  }

  .registeredNumberMsg {
    display: block;
    width: 100%;
    flex-grow: 1;
    margin-top: 20px;
    color: #8897a2;
    font-size: 14px;
    margin-bottom: 7px;
  }

  @media screen and (max-width: 768px) {
    .registeredNumberMsg {
      font-size: 12px;
    }
    flex-flow: column;
    padding: ${(props) =>
      props.altNum ? "14px 10px 10px 10px" : "16px 0px 16px 0px"};
    border: ${(props) => (props.altNum ? "1px solid #c4cfd7" : "none")};
    border-radius: 6px;
    .registeredNumberTitleWrap {
      justify-content: space-between;
    }

    .registeredMobHeader {
      border-bottom: 1px solid #c4cfd7;
      padding-bottom: 10px;
      align-items: center;
      gap: 0px;
    }

    .registeredNumberTitleWrap {
      justify-content: space-between;
      width: 100%;
    }
    .registeredNumberTitle {
      font-size: 12px;
    }
  }
`;

export const AddMobWrapper = styled.div`
  border-top: 1px solid #c4cfd7;
  padding: 15px 10px;

  .addFormWrap {
    display: flex;
    gap: 10px;
    margin-bottom: 5px;
  }

  .firstContent,
  .firstContentError {
    width: 100%;
  }

  .addFormWrap button {
    background: #0071bc !important;
    font-weight: 600;
  }

  .firstContent input,
  .firstContentError input {
    border: 1px solid #0071bc;
    border-radius: 4px;
    height: 100%;
    width: 100%;
    caret-color: #0071bc;
  }

  .firstContentError input {
    border: 1px solid #ff8c8c;
    caret-color: #ff8c8c;
  }

  .firstContent input::placeholder,
  .firstContentError input::placeholder {
    color: #8897a2;
  }

  .secondContent {
    width: 40%;
  }

  .secondContent button {
    width: 100%;
    background: #0071bc;
    color: #fff;
    border: none;
    border-radius: 5px;
    height: 44px;
    font-weight: 700;
    font-size: 14px;
  }

  .registeredNumberForm {
    width: 100%;
    display: grid;
    grid-template-columns: repeat(2, auto);
    gap: 4px 1em;
  }

  .registeredNumberErrorMsg {
    display: block;
    width: 100%;
    color: #f15959;
    font-size: 12px;

    /* font-weight: 600; */
  }

  .ctaWrap {
    grid-column: 1/3;
    display: flex;
    flex-flow: row nowrap;
    gap: 1em;
    justify-content: space-between;
    align-items: center;

    p {
      flex-grow: 1;
      display: flex;
      justify-content: flex-end;
    }
  }
`;

export const TextLinks = styled.p`
  display: ${(props) => (props.hide ? "none" : "block")};
  text-align: end;
  margin: 0;
  color: #016eb9;
  font-size: 14px;
  font-weight: 700;
  cursor: pointer;
  padding-top: 4px;

  @media screen and (max-width: 768px) {
    padding-top: 4px;
    margin-left: 16px;
  }
`;

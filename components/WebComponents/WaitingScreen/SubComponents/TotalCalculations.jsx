import React from "react";
import "./TotalCalculations.module.css";

const TotalCalculations = (props) => {
  return (
    <div className={"totalCalculations_calculationWrap"}>
      <div className={"totalCalculations_title"}>Estimated Total</div>
      <div className={"totalCalculations_calculationValue"}>₹200</div>
    </div>
  );
};

export default TotalCalculations;

import React from "react";
import "./AddressSection.module.css";

const AddressSection = (props) => {
  return (
    <div className={"AddressSection_AddressSectionWrap"}>
      {props.title ? (
        <div className={"AddressSection_addressSectionTitleCustom"}>
          {props.title}
        </div>
      ) : (
        <div className={"AddressSection_addressSectionTitle"}>Deliver to</div>
      )}

      <div className={"AddressSection_addressValue"}>
        <b>{props.addressDetails.addressType + " "}</b>
        {props.addressDetails.addressline1 +
          ", " +
          props.addressDetails.addressline2 +
          ", " +
          props.addressDetails.addressline3 +
          " " +
          props.addressDetails.cityName +
          "(" +
          props.addressDetails.pincode +
          ")"}
      </div>
      {props.showAltContact && props.altContact ? (
        <div className={"AddressSection_alternateContactWrap"}>
          <div className={"AddressSection_altTitle"}>Alternate Mobile No:</div>
          <div className={"AddressSection_altValue"}>{props.altContact}</div>
        </div>
      ) : null}
    </div>
  );
};

export default AddressSection;

import React, { useState } from "react";
import ScrollContainer from "react-indiana-drag-scroll";
import "./PrescriptionSection.module.css";
import { Image_URL } from "../../../../constants/Urls";
import PreviewModal from "../../../UploadPrescription/UploadSubComponent/PreviewModal/PreviewModal";
import { eventPrescriptionViewed } from "../../../../Events/Events";

const PrescriptionSection = (props) => {
  const [showPreview, setShowPreview] = useState(false);
  const [previewSrc, setPreviewSrc] = useState("");

  const openPreview = (image) => {
    // Event_Truemeds
    //new
    eventPrescriptionViewed();
    setPreviewSrc(image);
    setShowPreview(true);
  };

  return (
    <div
      className={`${"prescriptionSection_prescriptionWrap"} ${
        props.isMyOrderDetails ? "prescriptionSection_prescriptionWrapOd" : null
      }`}
    >
      <div
        className={`${"prescriptionSection_headerSection"} ${
          props.isMyOrderDetails ? "prescriptionSection_customMarginTop" : null
        }`}
      >
        {props.isMyOrderDetails ? "Prescription" : "Prescription Uploaded"}
      </div>
      <div
        className={
          props.isMyOrderDetails ? "prescriptionSection_bodySection" : null
        }
      >
        <ScrollContainer
          className={"prescriptionSection_prescriptionContainer"}
          horizontal={true}
          hideScrollbars={true}
        >
          {props.updateDetail.ImageMasterDto.map((data, index) => (
            <div style={{ position: "relative" }} key={index}>
              <img
                src={`${Image_URL}${data.imagePath}`}
                alt="Prescription"
                className="uploadPrescriptionDisplayImage"
                onClick={() => openPreview(`${Image_URL}${data.imagePath}`)}
              />
            </div>
          ))}
        </ScrollContainer>
      </div>
      {showPreview && (
        <PreviewModal
          open={showPreview}
          imageData={previewSrc}
          onCloseFunc={() => setShowPreview(false)}
        />
      )}
    </div>
  );
};

export default PrescriptionSection;

import React from "react";
import "./PaymentModeWs.module.css";

const PaymentModeWs = (props) => {
  return (
    <div
      className={`${"PaymentSection_paymentWrap"} ${
        props.spaceBetween ? "PaymentSection_spaceBetween" : ""
      }`}
    >
      <div className={"PaymentSection_paymentTitle"}>Payment Mode</div>
      <div className={"PaymentSection_paymentValue"}>
        {props.paymentMode.value && props.paymentMode.value === "COD"
          ? "COD"
          : props.paymentMode.value.toLowerCase()}
      </div>
    </div>
  );
};

export default PaymentModeWs;

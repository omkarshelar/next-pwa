import React from "react";
import "./SavingsCard.module.css";
import totalSavings from "../../../../src/Assets/totalSavings.svg";
import noSubsSaving from "../../../../src/Assets/noSubsSaving.svg";
import SavingsCardLoader from "../../../WebComponents/Shimmer/SavingsCardLoader";
import { cancelledOrderDetailsSavings } from "../../../Helper/calculationHelper";

const SavingsCard = (props) => {
  // function to check if the medicines have any subs in the order
  const checkForSubs = () => {
    let isSubs = false;
    if (props.updateDetail?.productSubsMappingList?.length > 0) {
      let arr = props.updateDetail.productSubsMappingList;
      for (let i in arr) {
        let med = arr[i];
        if (med.orgProductCd !== med.subsProductCd) {
          isSubs = true;
          break;
        }
      }
    }
    return isSubs;
  };

  // function to calulate savings for subs
  const subsSavings = (arr) => {
    let savings = 0;
    for (let i in arr) {
      let med = arr[i];
      if (
        med.orgProductCd !== med.subsProductCd &&
        !med.coldChainDisabled &&
        !med.disabled &&
        med.medActive
      ) {
        savings =
          savings +
          (med.orgMrp -
            med.orgMrp * (med.orgDiscount / 100) -
            med.subsSellingPrice);
      }
    }
    return savings.toFixed(2);
  };

  // function to calculate savings before doctor confirmation
  const preSavings = (arr) => {
    let savings = 0;
    for (let i in arr) {
      let med = arr[i];
      if (!med.coldChainDisabled && !med.disabled && med.medActive) {
        if (med.orgProductCd === med.subsProductCd) {
          savings = savings + (med.subsMrp - med.subsSellingPrice);
        } else {
          savings =
            savings +
            (med.orgMrp - (med.orgMrp - med.orgMrp * (med.orgDiscount / 100)));
        }
      }
    }
    let couponValue =
      props.updateDetail?.Promocode && props.updateDetail?.offerDiscountApplied
        ? props.updateDetail.offerDiscountApplied
        : 0;
    savings = savings + couponValue;
    return savings.toFixed(2);
  };

  // function to calculate savings after doctor confirmation
  const postSavings = (arr) => {
    let savings = 0;
    for (let i in arr) {
      let med = arr[i];
      if (!med.coldChainDisabled && !med.disabled && med.medActive) {
        if (med.orgProductCd !== med.subsProductCd) {
          savings = savings + (med.orgMrp - med.subsSellingPrice);
        } else {
          savings = savings + (med.subsMrp - med.subsSellingPrice);
        }
      }
    }
    let couponValue =
      props.updateDetail?.Promocode && props.updateDetail?.offerDiscountApplied
        ? props.updateDetail.offerDiscountApplied
        : 0;
    savings = savings + couponValue;
    return savings.toFixed(2);
  };

  // function to add elastic subs data in the existing med data
  const getSubsForOrg = () => {
    let finalArr = [];
    if (
      props.updateDetail?.productSubsMappingList?.length > 0 &&
      props.medicineList?.length > 0
    ) {
      let arr = props.updateDetail.productSubsMappingList;
      let medList = props.medicineList;
      for (let i in arr) {
        let med = arr[i];
        for (let j in medList) {
          let eMed = medList[j];
          if (
            !med.coldChainDisabled &&
            !med.disabled &&
            med.medActive &&
            med.orgProductCd === eMed._source.original_product_code
          ) {
            med.eSubsProductCd = eMed._source.subs_product_code;
            med.eSubsMrp =
              eMed._source.subs_mrp *
              med.orgQuantity *
              eMed._source.sub_recommended_qty;
            med.eSubsSellingPrice =
              eMed._source.subs_selling_price * med.orgQuantity;
            finalArr.push(med);
            break;
          }
        }
      }
    }
    return finalArr;
  };

  // function to check if org med has any subs after doc confirmation
  const checkForSubsInOrg = () => {
    let isSubs = false;
    let arr = getSubsForOrg();
    if (arr?.length > 0) {
      for (let i in arr) {
        let med = arr[i];
        if (med.orgProductCd !== med.eSubsProductCd) {
          isSubs = true;
          break;
        }
      }
    }
    return isSubs;
  };

  // function to calulate savings for subs which were not taken
  const subsForOrgSavings = () => {
    let arr = getSubsForOrg();
    let savings = 0;
    for (let i in arr) {
      let med = arr[i];
      if (
        med.orgProductCd !== med.eSubsProductCd &&
        !med.coldChainDisabled &&
        !med.disabled &&
        med.medActive
      ) {
        savings =
          savings +
          (med.orgMrp -
            med.orgMrp * (med.orgDiscount / 100) -
            med.eSubsSellingPrice);
      }
    }
    return savings.toFixed(2);
  };

  if (props.isLoading) {
    return <SavingsCardLoader />;
  } else if (props.updateDetail?.productSubsMappingList?.length === 0) {
    return null;
  } else if (props.isCancelled) {
    if (preSavings(props.updateDetail?.productSubsMappingList) > 50) {
      return (
        <div
          className={`${"SavingsCard_savingsWrap"} ${
            props.isOrderDetails ? "SavingsCard_odSavingsWrap" : ""
          }`}
        >
          <div
            style={{ borderRadius: "8px" }}
            className={"SavingsCard_subsSaving"}
          >
            <div className={"SavingsCard_subsSavingImage"}>
              <img src={noSubsSaving} alt="savings icon" />
            </div>
            <div className={"SavingsCard_subsSavingText"}>
              You lost the opportunity to{" "}
              <b>
                save ₹
                {cancelledOrderDetailsSavings(
                  props.updateDetail?.productSubsMappingList,
                  props.updateDetail?.Promocode &&
                    props.updateDetail?.offerDiscountApplied
                    ? props.updateDetail.offerDiscountApplied
                    : 0
                )}
              </b>{" "}
              by cancelling your order
            </div>
          </div>
        </div>
      );
    } else {
      return null;
    }
  } else if (checkForSubs()) {
    if (
      props.updateDetail?.orderStatus?.serialId === 49 ||
      props.updateDetail?.orderStatus?.serialId === 39 ||
      props.updateDetail?.orderStatus?.serialId === 2
    ) {
      if (preSavings(props.updateDetail?.productSubsMappingList) > 50) {
        return (
          <div
            className={`${"SavingsCard_savingsWrap"} ${
              props.isOrderDetails ? "SavingsCard_odSavingsWrap" : ""
            }`}
          >
            {subsSavings(props.updateDetail?.productSubsMappingList) > 50 && (
              <div className={"SavingsCard_subsSaving"}>
                <div className={"SavingsCard_subsSavingImage"}>
                  <img src={totalSavings} alt="savings icon" />
                </div>
                <div className={"SavingsCard_subsSavingText"}>
                  <b>
                    Save ₹
                    {subsSavings(props.updateDetail?.productSubsMappingList)}
                  </b>{" "}
                  extra by opting for Truemeds recommendations
                </div>
              </div>
            )}
            <div
              className={
                subsSavings(props.updateDetail?.productSubsMappingList) > 50
                  ? "SavingsCard_totalSavings"
                  : "SavingsCard_totalSavingsOnly"
              }
            >
              <span>Total savings on this order:</span>
              <span className={"SavingsCard_totalSavingsCost"}>
                ₹{preSavings(props.updateDetail?.productSubsMappingList)}
              </span>
            </div>
          </div>
        );
      } else {
        return null;
      }
    } else {
      if (postSavings(props.updateDetail?.productSubsMappingList) > 50) {
        return (
          <div
            className={`${"SavingsCard_savingsWrap"} ${
              props.isOrderDetails ? "SavingsCard_odSavingsWrap" : ""
            }`}
          >
            <div
              className={
                subsSavings(props.updateDetail?.productSubsMappingList) > 50
                  ? "SavingsCard_totalSavingsUp"
                  : "SavingsCard_totalSavingsOnly"
              }
            >
              <span>Total savings on this order:</span>
              <span className={"SavingsCard_totalSavingsCost"}>
                ₹{postSavings(props.updateDetail?.productSubsMappingList)}
              </span>
            </div>
            {subsSavings(props.updateDetail?.productSubsMappingList) > 50 && (
              <div className={"SavingsCard_subsSavingDown"}>
                <div className={"SavingsCard_subsSavingImage"}>
                  <img src={totalSavings} alt="savings icon" />
                </div>
                <div className={"SavingsCard_subsSavingText"}>
                  You saved{" "}
                  <b>
                    ₹{subsSavings(props.updateDetail?.productSubsMappingList)}
                  </b>{" "}
                  extra by going with Truemeds recommendations
                </div>
              </div>
            )}
          </div>
        );
      } else {
        return null;
      }
    }
  } else if (
    checkForSubsInOrg() &&
    props.updateDetail?.orderStatus?.serialId !== 49 &&
    props.updateDetail?.orderStatus?.serialId !== 39 &&
    props.updateDetail?.orderStatus?.serialId !== 2
  ) {
    if (postSavings(props.updateDetail?.productSubsMappingList) > 50) {
      return (
        <div
          className={`${"SavingsCard_savingsWrap"} ${
            props.isOrderDetails ? "SavingsCard_odSavingsWrap" : ""
          }`}
        >
          <div
            className={
              subsForOrgSavings(props.updateDetail?.productSubsMappingList) > 50
                ? "SavingsCard_totalSavingsUp"
                : "SavingsCard_totalSavingsOnly"
            }
          >
            <span>Total savings on this order:</span>
            <span className={"SavingsCard_totalSavingsCost"}>
              ₹{postSavings(props.updateDetail?.productSubsMappingList)}
            </span>
          </div>
          {subsForOrgSavings(props.updateDetail?.productSubsMappingList) >
            50 && (
            <div className={"SavingsCard_subsSavingDown"}>
              <div className={"SavingsCard_subsSavingImage"}>
                <img src={noSubsSaving} alt="savings icon" />
              </div>
              <div className={"SavingsCard_subsSavingText"}>
                You could have saved{" "}
                <b>
                  ₹
                  {subsForOrgSavings(
                    props.updateDetail?.productSubsMappingList
                  )}
                </b>{" "}
                extra by opting for Truemeds recommendations
              </div>
            </div>
          )}
        </div>
      );
    } else {
      return null;
    }
  } else {
    if (postSavings(props.updateDetail?.productSubsMappingList) > 50) {
      return (
        <div
          className={`${"SavingsCard_savingsWrap"} ${
            props.isOrderDetails ? "SavingsCard_odSavingsWrap" : ""
          }`}
        >
          <div className={"SavingsCard_totalSavingsOnly"}>
            <span>Total savings on this order:</span>
            <span className={"SavingsCard_totalSavingsCost"}>
              ₹{postSavings(props.updateDetail?.productSubsMappingList)}
            </span>
          </div>
        </div>
      );
    } else {
      return null;
    }
  }
};

export default SavingsCard;

import React from "react";
import "./StatusMsg.module.css";
import greenStatus from "../../../../src/Assets/greenStatus.svg";
import orderCancelled from "../../../../src/Assets/orderCancelled.svg";
import HeaderLoader from "../../../WebComponents/Shimmer/HeaderLoader";
String.prototype.toSentenceCase = function () {
  return this.charAt(0).toUpperCase() + this.slice(1).toLowerCase();
};

const StatusMsg = (props) => {
  if (props.isLoading) {
    return <HeaderLoader />;
  } else {
    return (
      <div
        className={`${"StatusMsg_Wrapper"} ${
          props.orderCancelled && "StatusMsg_redBg"
        }`}
      >
        <div className={"StatusMsg_imgWrap"}>
          <img
            src={props.orderCancelled ? orderCancelled : greenStatus}
            alt="tick"
          />
        </div>
        <div className={"StatusMsg_textWrap"}>
          {props.text.toSentenceCase()}
        </div>
      </div>
    );
  }
};

export default StatusMsg;

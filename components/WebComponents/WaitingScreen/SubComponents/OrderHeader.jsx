import React from "react";
import "./OrderHeader.module.css";
import moment from "moment";
const OrderHeader = (props) => {
  return (
    <div
      style={{ borderBottom: props.isOrderDetails ? "none" : "" }}
      className={`${"headerWrap_orderHeader"} ${
        props.isMyOrderDetails ? "headerOdWrap_orderHeader" : ""
      }`}
    >
      <div className={"contentWrap_orderHeader"}>
        <div
          className={` ${"contentTitle_orderHeader"} ${
            props.isMyOrderDetails ? "weight400_orderHeader" : ""
          }`}
        >
          Patient Name
        </div>
        <div className={"contentValue_orderHeader"}>{props.patientName}</div>
      </div>
      <div className={"contentWrap_orderHeader"}>
        <div
          className={` ${"contentTitle_orderHeader"} ${
            props.isMyOrderDetails ? "weight400_orderHeader" : ""
          }`}
        >
          Order ID
        </div>
        <div className={"contentValue_orderHeader"}>{props.orderId}</div>
      </div>
      {props.isMyOrderDetails ? (
        <div className={"contentWrap_orderHeader"}>
          <div
            className={` ${"contentTitle_orderHeader"} ${
              props.isMyOrderDetails ? "weight400_orderHeader" : ""
            }`}
          >
            Order Placed
          </div>
          <div className={"contentValue_orderHeader"}>
            {props.orderPlaced
              ? moment(props.orderPlaced).format("D MMM YYYY")
              : ""}
          </div>
        </div>
      ) : null}
    </div>
  );
};

export default OrderHeader;

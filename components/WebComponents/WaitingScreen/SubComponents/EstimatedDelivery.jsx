import React from "react";
import getDeliveryDate from "../../../Helper/EstimatedDateFormat";
import HeaderLoader from "../../../WebComponents/Shimmer/HeaderLoader";
import "./EstimatedDelivery.module.css";
import window from "global";

const EstimatedDelivery = (props) => {
  if (props.isLoading) {
    return <HeaderLoader />;
  } else {
    return (
      <>
        {!props.isIntersecting && window.innerWidth <= 768 && (
          <div className={"EDD_wrapFixed"}>
            <div className={"EDD_title"}>Estimated Delivery By</div>
            <div className={"EDD_value"}>
              {getDeliveryDate(props.deliveryDate)}
            </div>
          </div>
        )}

        <div
          className={"EDD_wrap"}
          style={{
            visibility:
              !props.isIntersecting && window.innerWidth <= 768
                ? "hidden"
                : "unset",
          }}
        >
          <div className={"EDD_title"}>Estimated Delivery By</div>
          <div className={"EDD_value"}>
            {getDeliveryDate(props.deliveryDate)}
          </div>
        </div>
      </>
    );
  }
};

export default EstimatedDelivery;

import React, { useEffect, useState } from "react";
import { connect } from "react-redux";
import Router, { withRouter } from "next/router";
import { LogoutAction } from "../../../redux/Login/Action";
import { toggleSidebarAction } from "../../../redux/SliderService/Actions";
import LogoutModal from "./LogoutModal/LogoutModal";
import "./SideBar.css";
import order from "../../../src/Assets/myorder.svg";
import setting from "../../../src/Assets/setting.svg";
import Help from "../../../src/Assets/Help.svg";
import subs from "../../../src/Assets/mysubs.svg";
import Legals from "../../../src/Assets/Legals.svg";
import userAvatar from "./user.svg";
import Homee from "../../../src/Assets/Home.svg";
import healthart from "../../../src/Assets/healthart.svg";
import wallet from "../../../src/Assets/wallet.svg";
import { Toast } from "react-bootstrap";
import Login from "../../WebComponents/Login/Login";
import { allOrdersThunk } from "../../../redux/MyOrder/Action";
import Button from "../../WebComponents/Button/Button";
import { removeRecentSearchAction } from "../../../redux/RecentSearch/action";
import Cookies from "js-cookie";
// import { singularSdk } from "singular-sdk";

function Sidebar({
  dispatch,
  sidebarToggle,
  profile,
  timeout,
  orderHistory,
  loginStatus,
  ...props
}) {
  let [show, setShow] = useState(false);
  let [loginModal, setloginModal] = useState(false);
  let [settingsModal, setSettingsModal] = useState(false);
  useEffect(() => {
    if (props?.accessToken) {
      dispatch(
        allOrdersThunk({
          accessToken: props.accessToken,
          history: Router,
        })
      );
    }
  }, []);

  let clearDataAndLogout = () => {
    dispatch(LogoutAction());
    localStorage.removeItem("branch_session_first");
    Cookies.remove("token", { path: "" });
    Cookies.remove("customerId", { path: "" });
    setShow(false);
    Router.push("/");
  };

  let letsLogout = () => {
    dispatch(toggleSidebarAction());
    setShow(true);
    dispatch(removeRecentSearchAction());
    // singularSdk.logout();
  };

  let goTo = (route) => {
    dispatch(toggleSidebarAction());
    Router.push(`${route}`);
  };

  let checkForDelivered = (orders) => {
    let display = false;
    if (orders?.pastOrder?.length > 0) {
      for (let i in orders.pastOrder) {
        let order = orders.pastOrder[i];
        if (order.orderStatusId === 55) {
          display = true;
          break;
        }
      }
    }
    return display;
  };

  let routeToHelp = () => {
    if (
      !props.decide &&
      orderHistory &&
      (orderHistory?.currentOrder?.length > 0 ||
        orderHistory?.pastOrder?.length > 0)
    ) {
      Router.push("/help/order");
      dispatch(toggleSidebarAction());
    } else {
      Router.push("/help");
      dispatch(toggleSidebarAction());
    }
  };
  const loginModalHandler = () => {
    setloginModal(!loginModal);
  };
  return (
    <>
      {settingsModal && (
        <Toast
          className="coming-soon-toast"
          onClose={() => setSettingsModal(false)}
          show={settingsModal}
          delay={4000}
          autohide
        >
          Coming Soon!!!
        </Toast>
      )}
      {show && (
        <LogoutModal
          handleClose={() => setShow(!show)}
          handleshow={show}
          logoutMeth={() => clearDataAndLogout()}
          Msg="Are you sure you want to logout?"
        />
      )}
      {loginModal && (
        <Login loginModal={loginModal} loginModalHandler={loginModalHandler} />
      )}
      {
        <div className={sidebarToggle ? "mainSidebar open" : "mainSidebar"}>
          {loginStatus && (
            <div className="headerSidebar">
              <div
                className="user-avatar"
                style={{ border: "5px soild black !important" }}
              >
                {profile?.profileImageUrl ? (
                  <img src={profile.profileImageUrl} alt="avatar"></img>
                ) : (
                  <img src={userAvatar} alt="avatar"></img>
                )}
              </div>
              <span>{profile?.customerName}</span>
              <span>{profile?.mobileNo}</span>
              <span>{profile?.emailAddress}</span>
            </div>
          )}
          {!loginStatus && (
            <div className="headerSidebar">
              <Button
                btnSidebar
                onClick={() => {
                  setloginModal(!loginModal);
                  dispatch(toggleSidebarAction());
                }}
              >
                Login / Sign Up
              </Button>
            </div>
          )}
          <div className="sidebaroptions">
            <div onClick={() => goTo("/")} className="particular-option">
              <div>
                <img src={Homee} alt="Option"></img>
              </div>
              <span>Home</span>
            </div>
            <div onClick={() => routeToHelp()} className="particular-option">
              <div>
                <img src={Help} alt="Option"></img>
              </div>
              <span>Help</span>
            </div>
            {loginStatus && (
              <>
                <div
                  onClick={() => goTo("/myorders")}
                  className="particular-option"
                >
                  <div>
                    <img src={order} alt="Option"></img>
                  </div>
                  <span>My Orders</span>
                </div>
                <div
                  onClick={() => goTo("/option/mysubstitute")}
                  className="particular-option"
                  style={{
                    display: checkForDelivered(orderHistory) ? "" : "none",
                  }}
                >
                  <div>
                    <img src={subs} alt="Option"></img>
                  </div>
                  <span>My Substitutes</span>
                </div>
                <div
                  onClick={() => goTo("/wallet")}
                  className="particular-option"
                >
                  <div>
                    <img src={wallet} alt="Option"></img>
                  </div>
                  <span>TM Wallet</span>
                </div>
                <div
                  onClick={() => goTo("/myprofile")}
                  className="particular-option"
                >
                  <div>
                    <img src={setting} alt="Option"></img>
                  </div>
                  <span>Profile</span>
                </div>
              </>
            )}

            <div onClick={() => goTo("/blog")} className="particular-option">
              <div>
                <img src={healthart} alt="Option"></img>
              </div>
              <span>Health Articles</span>
            </div>

            {/* <div
              onClick={() => goTo("/option/doctors")}
              className="particular-option"
            >
              <div>
                <img src={doctor} alt="Option"></img>
              </div>
              <span>Truemeds Doctors</span>
            </div> */}

            <div
              onClick={() => goTo("/option/legals")}
              className="particular-option"
            >
              <div>
                <img src={Legals} alt="Option"></img>
              </div>
              <span>Legal</span>
            </div>
            {/* <div
              onClick={() => goTo("/option/aboutus")}
              className="particular-option"
            >
              <div>
                <img src={Help} alt="Option"></img>
              </div>
              <span>About Us</span>
            </div> */}
            {loginStatus && (
              <div onClick={() => letsLogout()} className="particular-option">
                <div>
                  <img src={setting} alt="Option"></img>
                </div>
                <span>Logout</span>
              </div>
            )}
          </div>
        </div>
      }
    </>
  );
}

const mapStateToProps = (state) => ({
  loginStatus: state.loginReducer?.isUserLogged,
  timeout: state.myOrder?.orderTrackerError,
  profile: state.loginReducer?.verifyOtpSuccess?.CustomerDto,
  sidebarToggle: state.sidebar?.toggleSidebar,
  accessToken: state.loginReducer?.verifyOtpSuccess?.Response?.access_token,
  decide: state.myOrder?.orderTrackerError,
  orderHistory: state.myOrder?.allOrdersSuccess,
});

export default withRouter(connect(mapStateToProps)(Sidebar));

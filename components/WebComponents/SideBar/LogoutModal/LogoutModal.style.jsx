import { Modal } from "react-bootstrap";
import styled from "styled-components";

export const ModalFooter = styled(Modal.Footer)`
  border-top: 0;
  > button {
    width: 100px;
  }
`;

export const ModalStyle = styled(Modal)`
  .modal-content {
    border-radius: 1rem;
    border: 2px white solid;
    display: flex;
    justify-content: center;
    align-items: center;
  }
  z-index: 20000;
`;

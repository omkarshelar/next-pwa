import React from "react";
import { Modal } from "react-bootstrap";
import { connect } from "react-redux";
import CustomButton from "../../CustomButton/CustomButton";

import { ModalFooter, ModalStyle } from "./LogoutModal.style";

function LogoutModal({ handleClose, handleshow, Msg, logoutMeth }) {
  return (
    <ModalStyle show={handleshow} onHide={handleClose} centered>
      <Modal.Body>{Msg}</Modal.Body>
      <ModalFooter>
        <CustomButton delNo onClick={handleClose}>
          No
        </CustomButton>
        <CustomButton delYes onClick={logoutMeth}>
          Yes
        </CustomButton>
      </ModalFooter>
    </ModalStyle>
  );
}

export default connect()(LogoutModal);

import React, { Component } from "react";
import Dialog from "@material-ui/core/Dialog";
import DialogTitle from "@material-ui/core/DialogTitle";
import DialogContent from "@material-ui/core/DialogContent";
import DialogActions from "@material-ui/core/DialogActions";
import Button from "@material-ui/core/Button";
import backButtonIcon from "../../../../../src/Assets/back-button.svg";

export default class PastPrescriptionModal extends Component {
  render() {
    return (
      <Dialog
        open={this.props.open}
        onClose={this.props.onCloseFunc}
        aria-labelledby="responsive-dialog-title"
        fullWidth={true}
      >
        {this.props.onBackButtonClick ? (
          <div style={{ display: "flex", alignItems: "center" }}>
            <img
              src={backButtonIcon}
              alt="back"
              style={{ width: "20px", cursor: "pointer" }}
              onClick={this.props.onBackButtonClick}
            />
            <DialogTitle id="responsive-dialog-title">
              {this.props.header}
            </DialogTitle>
          </div>
        ) : (
          <DialogTitle id="responsive-dialog-title">
            {this.props.header}
          </DialogTitle>
        )}
        <DialogContent>{this.props.children}</DialogContent>
        <DialogActions>
          <Button color="primary" onClick={this.props.onClickContinue}>
            {this.props.continueText}
          </Button>
        </DialogActions>
      </Dialog>
    );
  }
}

import React, { Component } from "react";
import Dialog from "@material-ui/core/Dialog";
import "./MedListModal.css";
import { IoCloseOutline } from "react-icons/io5";
export default class MedListModal extends Component {
  render() {
    return (
      <>
        <Dialog
          open={this.props.open}
          onClose={this.props.onCloseFunc}
          aria-labelledby="responsive-dialog-title"
          className="medListModalContainer"
        >
          <div className="DailogWrap">
            {window.innerWidth <= 468 && (
              <div className="closeButton" onClick={this.props.onCloseFunc}>
                <IoCloseOutline />
              </div>
            )}
            <div className="medListModalHeader">
              <span>
                {this.props.medList?.length} Items require a prescription
              </span>
              {window.innerWidth >= 468 && (
                <span onClick={this.props.onCloseFunc}>Close</span>
              )}
            </div>
            <div className="medListDetailsContainer">
              {this.props.medList?.length > 0 &&
                this.props.medList.map((med) => (
                  <div className="medListDetails">
                    <div />
                    <span>{med._source.original_sku_name}</span>
                  </div>
                ))}
            </div>
          </div>
        </Dialog>
        {/* {this.props.open && (
          <img
            className="mobmedListClose"
            src={closeIcon}
            alt="close"
            onClick={this.props.onCloseFunc}
          />
        )} */}
      </>
    );
  }
}

import React, { Component } from "react";
import { connect } from "react-redux";
import { withRouter } from "next/router";
import { orderTrackerThunk } from "../../../redux/MyOrder/Action";
import {
  uploadImageThunk,
  deleteUploadImageThunk,
  getPrescriptionByIdThunk,
  lastImageClearAction,
  isUploadAction,
} from "../../../redux/UploadImage/Action";
import {
  toCartSectionAction,
  toOrderSummaryAction,
} from "../../../redux/Timeline/Action";
import "./UploadPrescription.css";
import galleryIcon from "../../../src/Assets/add-rx.svg";
import cameraIcon from "../../../src/Assets/add_a_photo.svg";
import pastIcon from "../../../src/Assets/past-rx.svg";
import removeImgIcon from "../../../src/Assets/remove-rx.svg";
import PreviewModal from "./UploadSubComponent/PreviewModal/PreviewModal";
import ScrollContainer from "react-indiana-drag-scroll";
import Shimmer from "react-shimmer-effect";
import { message } from "antd";
import Login from "../Login/Login";
import AndroidSeperator from "../AndroidSeperator/AndroidSeperator";
import PharmaDocCard from "../PharmaDocCard/PharmaDocCard";
import ConfirmModal from "../ConfirmModal/ConfirmModal";
import MedListModal from "./UploadSubComponent/MedListModal/MedListModal";
import { customNotification } from "../../Helper/helperFunction";
import window from "global";
// import {
//   eventPrescriptionUploaded,
//   eventPrescriptionViewed,
// } from "../../../src/Events/Events";

export class UploadPrescription extends Component {
  state = {
    imgAdd: "",
    imgSet: [],
    deleteImageShow: false,
    showPreview: false,
    previewSrc: "",
    rxRequiredMed: [],
    loginModal: false,
    pastPrescriptionModal: false,
    prescriptionsModal: false,
    patientArr: [],
    selectedPatient: null,
    selectedPrescriptions: [],
    medListModal: false,
    medData: [],
  };

  componentDidMount = () => {
    window.scrollTo(0, 0);
    window.history.pushState(null, null, window.location.pathname);
    window.addEventListener("popstate", this.onBackButtonEvent);
    if (
      this.props.imgUpload?.length > 0 ||
      this.props.cartContent?.length === 0
    ) {
      this.props.isUploadAction(true);
    }
    let data = [];
    if (this.props.cartContent?.length > 0) {
      for (var i in this.props.cartContent) {
        let med = this.props.cartContent[i];
        if (med._source.original_rx_required) {
          data.push(med);
        }
      }
      this.setState({ rxRequiredMed: data });
    }
    if (
      this.props.AuthErr !== 401 &&
      this.props.accessToken &&
      this.props.loginDetails?.isUserLogged
    ) {
      this.props.getPrescriptionByIdThunk({
        access_token: this.props.accessToken.Response.access_token,
        history: this.props.router,
        customerId: String(this.props.custId),
      });
    }
    //page change for hotjar
    // hjPageChange(window.location.origin + "/orderflow#upload");
  };

  componentDidUpdate = (prevProps) => {
    if (prevProps.isProceed !== this.props.isProceed && this.props.isProceed) {
      this.comfirmUploadDetails();
    }
    if (
      prevProps.accessToken !== this.props.accessToken &&
      this.props.accessToken &&
      this.props.loginDetails?.isUserLogged
    ) {
      this.props.getPrescriptionByIdThunk({
        access_token: this.props.accessToken.Response.access_token,
        history: this.props.router,
        customerId: String(this.props.custId),
      });
    }
  };

  // Clear State after unmounting
  componentWillUnmount = () => {
    window.removeEventListener("popstate", this.onBackButtonEvent);
  };

  // On Back Button click Event.
  onBackButtonEvent = (e) => {
    e.preventDefault();
    if (!this.isBackButtonClicked) {
      if (window && this.props.router?.query?.isReorder) {
        this.isBackButtonClicked = true;
        this.props.router.push("/myorders");
      } else if (window && this.props.router?.query?.upload) {
        this.isBackButtonClicked = true;
        this.props.router.push("/");
      } else {
        this.props.toCartSectionAction();
        this.isBackButtonClicked = false;
      }
    }
  };

  checkLoggedIn = (e) => {
    e.preventDefault();
    if (
      this.props.AuthErr === 401 ||
      !this.props.accessToken ||
      !this.props.loginDetails.isUserLogged
    ) {
      this.setState({ loginModal: true });
    }
  };

  onImageChange = (e) => {
    if (
      this.props.AuthErr === 401 ||
      !this.props.accessToken ||
      !this.props.loginDetails.isUserLogged
    ) {
      this.setState({ loginModal: true });
    } else {
      let { orderIdTrue, accessToken, patientIdTrue } = this.props;
      let file = e.target.files[0];
      let reader = new FileReader();
      reader.readAsDataURL(file);
      reader.onloadend = () => {
        this.setState({ imgAdd: reader.result }, () => {
          if (orderIdTrue && accessToken && patientIdTrue) {
            let fileName = file.name;
            if (file.name.length > 12) {
              let firstPart = fileName.substring(0, 5);
              let lastPart = fileName.substring(
                file.name.length - 5,
                file.name.length
              );
              fileName = firstPart + lastPart;
            }
            this.props
              .uploadImageThunk({
                imgData: {
                  fileName: fileName,
                  orderId: orderIdTrue,
                  patientId: patientIdTrue,
                  image: this.state.imgAdd,
                },
                access_token: accessToken.Response.access_token,
                history: this.props.router,
                customerId: this.props.custId,
                pincode: this.props.pincodeDetails.pincode,
              })
              .then(async () => {
                // Event_Truemeds
                //new
                // eventPrescriptionUploaded();
                if (this.props.allPrescriptions.error == null) {
                  customNotification("Successfully uploaded", "customNotify");
                } else {
                  message.error("Something went wrong!");
                }
              });
          } else if (orderIdTrue && accessToken && !patientIdTrue) {
            let fileName = file.name;
            if (file.name.length > 12) {
              let firstPart = fileName.substring(0, 5);
              let lastPart = fileName.substring(
                file.name.length - 5,
                file.name.length
              );
              fileName = firstPart + lastPart;
            }
            this.props
              .uploadImageThunk({
                imgData: {
                  fileName: fileName,
                  orderId: orderIdTrue,
                  patientId: "",
                  image: this.state.imgAdd,
                },
                access_token: accessToken.Response.access_token,
                history: this.props.router,
                customerId: this.props.custId,
                pincode: this.props.pincodeDetails.pincode,
              })
              .then(() => {
                // Event_Truemeds
                //new
                // eventPrescriptionUploaded();
                if (this.props.allPrescriptions.error == null) {
                  customNotification("Successfully uploaded", "customNotify");
                } else {
                  message.error("Something went wrong!");
                }
              });
          } else if (
            this.props.medConfirm?.ConfirmMedData?.orderId &&
            accessToken
          ) {
            let fileName = file.name;
            if (file.name.length > 12) {
              let firstPart = fileName.substring(0, 5);
              let lastPart = fileName.substring(
                file.name.length - 5,
                file.name.length
              );
              fileName = firstPart + lastPart;
            }
            this.props
              .uploadImageThunk({
                imgData: {
                  fileName: fileName,
                  orderId: this.props.medConfirm.ConfirmMedData.orderId,
                  patientId: "",
                  image: this.state.imgAdd,
                },
                access_token: accessToken.Response.access_token,
                history: this.props.router,
                customerId: this.props.custId,
                pincode: this.props.pincodeDetails.pincode,
              })
              .then(() => {
                // Event_Truemeds
                //new
                // eventPrescriptionUploaded();
                if (this.props.allPrescriptions.error == null) {
                  customNotification("Successfully uploaded", "customNotify");
                } else {
                  message.error("Something went wrong!");
                }
              });
          } else if (
            !orderIdTrue &&
            !this.props.medConfirm?.ConfirmMedData?.orderId &&
            accessToken
          ) {
            let fileName = file.name;
            if (file.name.length > 12) {
              let firstPart = fileName.substring(0, 5);
              let lastPart = fileName.substring(
                file.name.length - 5,
                file.name.length
              );
              fileName = firstPart + lastPart;
            }
            this.props.uploadImageThunk(
              {
                imgData: {
                  fileName: fileName,
                  orderId: "",
                  patientId: "",
                  image: this.state.imgAdd,
                },
                access_token: accessToken.Response.access_token,
                history: this.props.router,
                customerId: this.props.custId,
                pincode: this.props.pincodeDetails.pincode,
              },
              () => {
                // Event_Truemeds
                //new
                eventPrescriptionUploaded();
                if (this.props.allPrescriptions.error == null) {
                  customNotification("Successfully uploaded", "customNotify");
                } else {
                  message.error("Something went wrong!");
                }
              }
            );
          }
        });
      };
    }
  };

  comfirmUploadDetails = () => {
    if (this.props.accessToken && this.props.loginDetails.isUserLogged) {
      if (
        (this.props.isUpload && this.props.imgUpload?.length > 0) ||
        (!this.props.isUpload && this.props.cartContent?.length > 0)
      ) {
        if (this.props.cartContent?.length > 0) {
          if (this.props.imgUpload?.length > 0) {
          } else {
          }
        } else {
        }
        if (this.props.Stepper.onOrderSummary) {
          this.props.toOrderSummaryAction();
        } else {
          this.props.orderTrackerThunk({
            accessToken: this.props.accessToken?.Response.access_token,
            history: this.props.router,
          });
          this.props.toOrderSummaryAction();
        }
      } else {
        customNotification("Please upload a prescription", "customNotify");
      }
    } else {
      // login
      this.setState({ loginModal: true });
      //page change for hotjar
      // hjPageChange(window.location.origin + "/orderflow#upload_login");
    }
  };

  loginModalHandler = () => {
    this.setState({ loginModal: !this.state.loginModal });
  };

  closePreview = () => {
    this.setState({ showPreview: !this.state.showPreview });
  };

  openPreview = (image) => {
    // Event_Truemeds
    //new
    // eventPrescriptionViewed();
    this.setState({ showPreview: !this.state.showPreview, previewSrc: image });
  };

  onClickChangeRadio = (isUpload) => {
    this.props.isUploadAction(isUpload);
  };

  onClickDeleteUploadImage = (data) => {
    if (this.props.imgUpload?.length === 1) {
      this.props
        .deleteUploadImageThunk({
          imgIdSet: [data.imgId],
          access_token: this.props.accessToken?.Response.access_token,
          orderIds: this.props.orderIdTrue,
          history: this.props.router,
        })
        .then(() => {});
      this.props.lastImageClearAction();
      this.setState({
        deleteImageShow: !this.state.deleteImageShow,
      });
    } else {
      this.props
        .deleteUploadImageThunk({
          imgIdSet: [data.imgId],
          access_token: this.props.accessToken?.Response.access_token,
          orderIds: this.props.orderIdTrue,
          history: this.props.router,
        })
        .then(() => {});
      this.setState({
        deleteImageShow: !this.state.deleteImageShow,
      });
    }
  };

  checkForPastPrescriptions = (arr) => {
    let isAvailable = false;
    for (let i in arr) {
      let cust = arr[i];
      if (cust?.ActiveRx?.length > 0) {
        isAvailable = true;
      }
    }
    return isAvailable;
  };

  getPastPrescriptions = () => {
    if (
      this.props.AuthErr === 401 ||
      !this.props.accessToken ||
      !this.props.loginDetails.isUserLogged
    ) {
      this.setState({ loginModal: true });
    } else {
      window.openSideBar(true, 1);
    }
  };

  render() {
    return (
      <div className="uploadPrescriptionMainContainer">
        <Login
          loginModal={this.state.loginModal}
          loginModalHandler={this.loginModalHandler}
        />
        <MedListModal
          open={this.state.medListModal}
          onCloseFunc={() => this.setState({ medListModal: false })}
          medList={this.state.rxRequiredMed}
        />
        {this.props.isLoading ? (
          <div className="uploadPrescriptionItemsMainLoader">
            <Shimmer>
              <div className="uploadPrescriptionItemsLoader" />
              <div className="uploadPrescriptionItemsLoader" />
            </Shimmer>
          </div>
        ) : (
          this.state.rxRequiredMed?.length > 0 && (
            <>
              <div className="uploadPrescriptionItemsContainer">
                <p>
                  {this.state.rxRequiredMed?.length === 1
                    ? "1 Item in your cart requires prescription"
                    : `${this.state.rxRequiredMed.length} Items in your cart requires prescription`}
                </p>
                {this.state.rxRequiredMed.map((med, index) =>
                  index < 2 ? (
                    <div className="uploadPrescriptionrequiredContainer">
                      <div />
                      <span>{med._source.original_sku_name}</span>
                    </div>
                  ) : null
                )}
                {this.state.rxRequiredMed?.length > 2 ? (
                  <span
                    className="moreMedsText"
                    onClick={() => this.setState({ medListModal: true })}
                  >
                    + {this.state.rxRequiredMed?.length - 2} more
                  </span>
                ) : null}
              </div>
              <div style={{ marginTop: "15px" }}>
                <AndroidSeperator showWeb={true} showMob={true} />
              </div>
            </>
          )
        )}
        {this.props.isLoading ? (
          <div className="uploadPrescriptionImagesMainLoader">
            <Shimmer>
              <div className="uploadPrescriptionImagesLineLoader" />
              <div className="uploadPrescriptionImagesLineLoader" />
              <div className="uploadPrescriptionImagesLineLoader" />
              <div className="uploadPrescriptionImagesLineLoader" />
              <div className="uploadPrescriptionImagesLineLoader" />
              <div className="uploadPrescriptionImagesLineLoader" />
            </Shimmer>
          </div>
        ) : (
          <div className="uploadPrescriptionAddMainContainer">
            <div className="uploadPrescriptionAddContainer">
              <p>Do you have a prescription for your medicine?</p>
              {this.props.cartContent?.length === 0 && (
                <span className="onlyUploadText">
                  Just upload it and we will arrange the medicines for you.
                </span>
              )}
            </div>
            <div
              style={{ display: "flex", flexDirection: "column", gap: "0px" }}
            >
              {this.props.cartContent?.length > 0 && (
                <label
                  onClick={() => this.onClickChangeRadio(true)}
                  className={
                    this.props.isUpload
                      ? "uploadPrescriptionSelectedRadioButtonContainer"
                      : "uploadPrescriptionUnSelectedRadioButtonContainer"
                  }
                >
                  <label
                    className={
                      this.props.isUpload
                        ? "uploadPrescriptionSelectedRadioButton"
                        : "uploadPrescriptionUnSelectedRadioButton"
                    }
                  >
                    {/* <img src={tickIcon} alt="check" /> */}
                    <div className="uploadPresSelectInnerButton" />
                  </label>
                  <span className="spanLabel">
                    Yes, I do have a prescription
                  </span>
                </label>
              )}
              {this.props.isUpload && (
                <>
                  {this.state.showPreview && (
                    <PreviewModal
                      open={this.state.showPreview}
                      imageData={this.state.previewSrc}
                      onCloseFunc={() => this.closePreview()}
                    />
                  )}
                  <div className="uploadPrescriptionAddInsideContainer">
                    <div className="uploadPrescriptionImageContainer">
                      {window.innerWidth <= 768 && (
                        <label
                          htmlFor="upload-photo-image"
                          className="uploadPrescriptionImageInsideContainer"
                        >
                          <img src={cameraIcon} alt="upload" />
                          <label htmlFor="upload-photo-image">
                            Use <br />
                            Camera
                          </label>
                          <input
                            type="file"
                            name="photo"
                            id="upload-photo-image"
                            onChange={this.onImageChange}
                            // onClick={() => eventPrescriptionCamera()}
                            multiple
                            accept="image/*"
                            capture="camera"
                            // capture="environment"
                          />
                        </label>
                      )}
                      <label
                        htmlFor="upload-photo-image1"
                        className="uploadPrescriptionImageInsideContainer"
                      >
                        <img src={galleryIcon} alt="upload" />
                        <label htmlFor="upload-photo-image1">
                          {window.innerWidth > 768 ? (
                            "Upload Prescription"
                          ) : (
                            <>
                              Use <br />
                              Gallery
                            </>
                          )}
                        </label>
                        <input
                          type="file"
                          name="photo"
                          id="upload-photo-image1"
                          onChange={this.onImageChange}
                          onClick={() => {}}
                          multiple
                          accept="image/*"
                        />
                      </label>
                      {(!this.props.accessToken &&
                        !this.props.loginDetails?.isUserLogged) ||
                      (this.props.allPrescriptions?.prescriptionById?.CustomerRx
                        ?.length > 0 &&
                        this.checkForPastPrescriptions(
                          this.props.allPrescriptions.prescriptionById
                            .CustomerRx
                        )) ? (
                        <label
                          className="uploadPrescriptionImageInsideContainer"
                          onClick={() => this.getPastPrescriptions()}
                        >
                          <img src={pastIcon} alt="past" />
                          <label>
                            {window.innerWidth > 768 ? (
                              "Past Prescription"
                            ) : (
                              <>
                                Past <br />
                                Prescription
                              </>
                            )}
                          </label>
                        </label>
                      ) : null}
                    </div>
                    {this.props.imgUpload?.length > 0 ? (
                      <>
                        <p className="uploadedPrescriptionText">
                          Uploaded prescription
                        </p>
                        <ScrollContainer
                          className="uploadPrescriptionImageScrollContainer"
                          horizontal={true}
                          hideScrollbars={true}
                        >
                          {this.props.imgUpload.map((data, index) => (
                            <div
                              style={{
                                position: "relative",
                                paddingTop: "15px",
                              }}
                              key={index}
                            >
                              <img
                                src={removeImgIcon}
                                alt="remove"
                                className="uploadPrescriptionRemoveImage"
                                onClick={() =>
                                  this.setState((prevState) => ({
                                    deleteImageShow: !prevState.deleteImageShow,
                                    imgId: data.imgId,
                                  }))
                                }
                              />
                              <ConfirmModal
                                open={
                                  this.state.deleteImageShow &&
                                  data.imgId === this.state.imgId
                                }
                                Header={
                                  "Are you sure you want to remove the prescription?"
                                }
                                handleClose={() =>
                                  this.setState({
                                    deleteImageShow:
                                      !this.state.deleteImageShow,
                                  })
                                }
                                noText={"No"}
                                yesText={"Yes"}
                                noMethod={() =>
                                  this.setState({
                                    deleteImageShow:
                                      !this.state.deleteImageShow,
                                  })
                                }
                                yesMethod={() =>
                                  this.onClickDeleteUploadImage(data)
                                }
                                type={0}
                              />
                              <img
                                src={data.src}
                                alt="Prescription"
                                className="uploadPrescriptionDisplayImage"
                                onClick={() => this.openPreview(data.src)}
                              />
                            </div>
                          ))}
                        </ScrollContainer>
                      </>
                    ) : null}
                    {this.props.cartContent?.length > 0 ? (
                      <hr
                        style={{
                          margin: window.innerWidth > 768 ? "0" : "0px 18px",
                          border: "0",
                          borderBottom: "1px solid #BDBDBD",
                        }}
                      />
                    ) : (
                      <AndroidSeperator showWeb={true} showMob={true} />
                    )}
                  </div>
                </>
              )}
              {!this.props.isUpload && (
                <hr
                  style={{
                    "border-color": "#BDBDBD",
                    "margin-left": window.innerWidth <= 768 ? "16px" : "0px",
                    "margin-right": window.innerWidth <= 768 ? "16px" : "0px",
                    "margin-top": window.innerWidth <= 768 ? "16px" : "0px",
                    "margin-bottom": window.innerWidth <= 768 ? "16px" : "0px",
                  }}
                />
              )}
              {this.props.cartContent?.length > 0 && (
                <label
                  onClick={() => this.onClickChangeRadio(false)}
                  className={
                    !this.props.isUpload
                      ? "uploadPrescriptionSelectedRadioButtonContainer"
                      : "uploadPrescriptionUnSelectedRadioButtonContainer"
                  }
                >
                  <label
                    className={
                      !this.props.isUpload
                        ? "uploadPrescriptionSelectedRadioButton"
                        : "uploadPrescriptionUnSelectedRadioButton"
                    }
                  >
                    <div className="uploadPresSelectInnerButton" />
                  </label>
                  <span className="spanLabel">
                    No, continue without a prescription
                  </span>
                </label>
              )}
            </div>
          </div>
        )}
        {this.props.isLoading ? (
          <Shimmer>
            <div className="uploadPrescriptionImagesLineLoader" />
          </Shimmer>
        ) : (
          <PharmaDocCard
            isDoc={this.props.cartContent?.length > 0 ? true : false}
            // onClick={() => this.onClickChangeRadio(false)}
          />
        )}
      </div>
    );
  }
}

let mapStateToProps = (state) => ({
  cartContent: state.cartData?.cartItems,
  orderIdTrue: state.uploadImage.orderId,
  patientIdTrue: state.uploadImage.PatientId,
  imgUpload: state.uploadImage?.uploadHistory,
  accessToken: state.loginReducer?.verifyOtpSuccess,
  medConfirm: state.confirmMedicineReducer,
  isLoading: state.loader.isLoading,
  AuthErr: state.trackOrder.error,
  Stepper: state.stepper,
  loginDetails: state.loginReducer,
  custId: state.loginReducer?.verifyOtpSuccess?.CustomerId,
  allPrescriptions: state.uploadImage,
  pincodeDetails: state.pincodeData?.pincodeData,
  isUpload: state.uploadImg?.isUpload,
});

export default withRouter(
  connect(mapStateToProps, {
    toCartSectionAction,
    toOrderSummaryAction,
    uploadImageThunk,
    deleteUploadImageThunk,
    lastImageClearAction,
    orderTrackerThunk,
    getPrescriptionByIdThunk,
    isUploadAction,
  })(UploadPrescription)
);

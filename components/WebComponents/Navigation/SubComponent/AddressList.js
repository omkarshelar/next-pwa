import React from "react";
import {
  AddressListWrapper,
  SpinnerWrapper,
  ScrollBarProps,
} from "./AddressList.style";
import tickIcon from "../../../../src/Assets/tick.png";

import { Spin } from "antd";
import { LoadingOutlined } from "@ant-design/icons";
const antIcon = <LoadingOutlined style={{ fontSize: 18 }} spin />;
export default class AddressList extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      selectedAddress: 0,
      pincode: this.props?.pincode,
    };

    this.handleSelect = this.handleSelect.bind(this);
  }
  handleSelect(data) {
    this.setState({
      selectedAddress: data.addressId,
    });
    this.props.getSelectedPincode(data);
  }

  updatePincode() {
    let selectedAddress = this.state.selectedAddress;
    let that = this;
    this.props.addressList.map(function (data) {
      if (data.pincode == that.props.pincode) {
        selectedAddress = data.addressId;
      }
    });
    this.setState({
      selectedAddress,
    });
  }
  componentWillUpdate(prevProps) {
    if (this.props.pincode != prevProps.pincode) {
      this.updatePincode();
    }
    if (this.props.isLoading != prevProps.isLoading) {
      this.setState({
        isLoading: this.props.isLoading,
      });
    }
  }

  componentDidMount() {
    this.updatePincode();
  }
  render() {
    let { selectedAddress } = this.state;
    let { isLoading } = this.props;
    let that = this;
    return (
      <AddressListWrapper>
        <div>
          {this.props.addressList.map(function (e) {
            let selected = e.addressId == selectedAddress;
            return (
              <div
                className={selected ? "outerWrapper selected" : "outerWrapper"}
                onClick={that.handleSelect.bind(this, e)}
              >
                <div className="addressWrapper">
                  <div>{e.addressType}</div>
                  <div className={"seperator"}>-</div>
                  <div>{e.cityName}</div>
                  <div className={"seperator"}>-</div>
                  <div>{e.pincode}</div>
                </div>
                {selected &&
                  (isLoading ? (
                    <SpinnerWrapper>
                      <Spin indicator={antIcon} />
                    </SpinnerWrapper>
                  ) : (
                    <>
                      {/* <div className="defaultSelect">{"Default Address"}</div> */}
                      <div className={"orderFlowSteperSelectedElement"}>
                        <img src={tickIcon} alt="tick" />
                      </div>
                    </>
                  ))}
              </div>
            );
          })}
        </div>
      </AddressListWrapper>
    );
  }
}

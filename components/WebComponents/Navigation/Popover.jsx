import React from "react";
import { makeStyles } from "@material-ui/core/styles";
import Popover from "@material-ui/core/Popover";
import Typography from "@material-ui/core/Typography";

import Button from "@material-ui/core/Button";
import drop from "./drop.svg";
import { LogoutAction } from "../../../redux/Login/Action";
import { connect } from "react-redux";
import Router, { withRouter } from "next/router";
import { Height } from "@material-ui/icons";
// import { singularSdk } from "singular-sdk";
import { HiUserCircle } from "react-icons/hi";
import "./Popover.css";
import Cookies from "js-cookie";

const useStyles = makeStyles((theme) => ({
  typography: {
    padding: "5px 20px",
    cursor: "pointer",
  },
}));

export function SimplePopover({
  dispatch,
  history,
  allOrders,
  imgSrc,
  cusName,
  cusEmail,
  removeRecentSearchAction,
}) {
  const classes = useStyles();
  const [anchorEl, setAnchorEl] = React.useState(null);

  const handleClick = (event) => {
    setAnchorEl(event.currentTarget);
  };

  const handleClose = () => {
    setAnchorEl(null);
  };

  let clearDataAndLogout = () => {
    removeRecentSearchAction();
    dispatch(LogoutAction());
    localStorage.removeItem("branch_session_first");
    Cookies.remove("token", { path: "" });
    Cookies.remove("customerId", { path: "" });
    Router.push("/");
    // singularSdk.logout();
  };

  let checkForDelivered = (orders) => {
    let display = false;
    if (orders?.pastOrder?.length > 0) {
      for (let i in orders.pastOrder) {
        let order = orders.pastOrder[i];
        if (order.orderStatusId === 55) {
          display = true;
          break;
        }
      }
    }
    return display;
  };

  const open = Boolean(anchorEl);
  const id = open ? "simple-popover" : undefined;

  return (
    <div>
      <Button
        aria-describedby={id}
        onClick={handleClick}
        className="profileMenuBtn"
      >
        {imgSrc === "avatar" ? (
          <HiUserCircle className="avatarIcon" />
        ) : (
          <img src={imgSrc} alt="avatar"></img>
        )}
        {/* <img src={drop} alt="drop"></img> */}
      </Button>
      <Popover
        id={id}
        open={open}
        anchorEl={anchorEl}
        onClose={handleClose}
        anchorOrigin={{
          vertical: "bottom",
          horizontal: "right",
        }}
        style={{
          padding: "2em",
          border: "1px solid #ccc",
          boxShadow:
            "0 0 0 1px rgba(64,87,109,.07),0 2px 12px rgba(53,71,90,.2)",
        }}
        transformOrigin={{
          vertical: "top",
          horizontal: "right",
        }}
      >
        <div className="profileInfo">
          {/*  {imgSrc === "avatar" ? (
            <HiUserCircle className="avatarIcon" />
          ) : (
            <img src={imgSrc} alt="avatar"></img>
          )} */}

          <div className="profileData">
            <h5>{cusName}</h5>
            <p>{cusEmail}</p>
          </div>
        </div>
        <Typography
          onClick={() => {
            Router.push("/myprofile");
            handleClose();
          }}
          style={{ marginTop: ".5em" }}
          className={classes.typography}
        >
          Profile
        </Typography>
        <Typography
          onClick={() => {
            Router.push("/option/mysubstitute");
            handleClose();
          }}
          className={classes.typography}
          style={{
            display: checkForDelivered(allOrders) ? "" : "none",
          }}
        >
          My Substitutes
        </Typography>
        <Typography
          onClick={() => {
            clearDataAndLogout();
          }}
          style={{ marginBottom: "1em" }}
          className={classes.typography}
        >
          Logout
        </Typography>
      </Popover>
    </div>
  );
}

export default withRouter(connect()(SimplePopover));

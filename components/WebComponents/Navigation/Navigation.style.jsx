import Badge from "@material-ui/core/Badge";
import styled from "styled-components";

// Parent wrapper.
export const NavContainer = styled.div`
  background-color: #ffffff;
  color: #333333;
  position: sticky;
  z-index: 1100;
  top: 0;
  box-shadow: 0px 12px 20px 1px rgba(8, 65, 89, 0.23);
  > .MuiLinearProgress-colorPrimary {
    background-color: #fff;
  }
`;

export const LogoWrapper = styled.div`
  display: flex;

  @media screen and (max-width: 468px) {
    width: 100%;
    justify-content: space-between;
    margin-bottom: 5px;
    height: 35px;
  }
`;

// Content wrapper.
export const NavContent = styled.div`
  display: flex;
  gap: 1em;
  justify-content: space-between;
  align-items: center;
  width: 70vw;
  margin: 0 auto;
  min-height: 60px;
  padding: 16px 0;

  @media screen and (max-width: 1400px) {
    width: 80vw;
  }
  @media screen and (max-width: 1210px) {
    width: 95vw;
  }
  @media screen and (max-width: 968px) {
    width: 98vw;
    justify-content: flex-start;
  }
  @media screen and (max-width: 468px) {
    flex-flow: column;
    justify-content: space-between;
    padding: 8px 0 0 0;
    width: 100vw;
    gap: 0em;
  }
`;

// Search Medicine Input

export const SearchParent = styled.div`
  display: flex;
  flex-grow: 1;
  box-sizing: border-box;
  max-width: 700px;
  position: relative;
  @media screen and (max-width: 1068px) {
    flex-grow: 1;
  }
  @media screen and (max-width: 468px) {
    padding: 10px 5px;
    width: 100%;
    background-color: #003055;
    position: relative;
    margin-top: 15px;
  }

  .newSearchWrapper {
    position: absolute;
    display: flex;
    width: calc(100% - 110px);
    top: 18px;
    z-index: 2;
    right: 0px;
  }
  @media screen and (max-width: 468px) {
    .newSearchWrapper {
      position: absolute;
      display: flex;
      width: 100%;
      top: 0px;
      z-index: 2;
      left: 0;
    }
  }

  .coachmark {
    display: none;
    position: absolute;
    top: 41px;
    background: #22b573;
    border-radius: 6px;
    padding: 10px 13px;
    color: #fff;
    font-size: 14px;
    font-weight: 500;
    z-index: 2;
    @media screen and (max-width: 768px) {
      margin: auto;
      left: 0;
      right: 0;
      width: fit-content;
      top: 51px;
    }
  }

  .pointer {
    background: #22b573;
    width: 18px;
    height: 30px;
    position: absolute;
    transform: rotate(45deg);
    border-radius: 4px;
    z-index: -1;
    top: -8px;
    left: 25px;
    @media screen and (max-width: 768px) {
      left: 20%;
    }
  }

  .text {
    z-index: 2;
  }

  .searchDropDown {
    background: #ffffff;
    width: 100%;
    top: 36px;
    box-shadow: 0px 3px 11px -2px #00000029;
    border-bottom-right-radius: 12px;
    border-bottom-left-radius: 12px;
    padding: 18px 15px 0px 15px;
    @media screen and (max-width: 468px) {
      position: absolute;
      background: #ffffff;
      width: 100%;
      top: 57px;
      box-shadow: 0px 3px 11px -2px #00000029;
      border-bottom-right-radius: 12px;
      border-bottom-left-radius: 12px;
      padding: 0px 15px;
      left: 0;
      height: 100vh;
    }
  }
  .optionList {
    font-size: 14px;
    text-transform: lowercase;
    padding: 12px 3px;
    color: #2f5572;
    font-weight: 500;
    cursor: pointer;
    display: flex;
    justify-content: space-between;
    @media screen and (max-width: 768px) {
      font-size: 13px;
      text-transform: lowercase;
      padding: 8px 3px;
      color: #2f5572;
      font-weight: 600;
      cursor: pointer;
      display: flex;
      justify-content: space-between;
    }
  }

  .optionList .icon {
    display: none;
    @media screen and (max-width: 768px) {
      font-size: 13px;
      color: #0071bc;
      display: block;
    }
  }
  .optionList .iconProd {
    display: block;
    font-size: 18px;
    color: #0071bc;
    display: block;
    @media screen and (max-width: 768px) {
      font-size: 18px;
      color: #0071bc;
      display: block;
    }
  }

  .optionList:hover {
    background: #e5f1f8;
    margin: 0px -15px;
    padding: 12px 18px;
    @media screen and (max-width: 768px) {
      background: #e5f1f8;
      margin: 0px -15px;
      padding: 8px 18px;
    }
  }

  .optionList:first-letter {
    text-transform: capitalize;
  }
  span.searchResult {
    color: #728a9c;
    font-weight: 300;
  }
  .title {
    color: #869eab;
    font-size: 13px;
    line-height: 24px;
    padding: 5px 0px 10px 0px;
    font-weight: 500;
    letter-spacing: 0.02em;
    @media screen and (max-width: 768px) {
      color: #869eab;
      font-size: 12px;
      line-height: 24px;
      padding: 5px 0px 10px 0px;
      font-weight: 500;
      letter-spacing: 0.02em;
      text-transform: lowercase;
    }
  }
  .title:first-letter {
    text-transform: uppercase;
  }

  .titleWrapSearch {
    display: flex;
    justify-content: space-between;
  }

  .clearBtn {
    color: #0071bc;
    font-size: 13px;
    line-height: 24px;
    font-weight: 500;
    cursor: pointer;
    text-decoration: underline;
    padding: 5px 0px;
    letter-spacing: 0.02em;
  }

  .recentlySearchedContent {
    /* display: flex;
    gap: 10px; */
    overflow: hidden;
    max-height: 75px;
  }

  .recentItem {
    background: #e5f1f8;
    border: 1px solid #0071bc;
    color: #0071bc;
    border-radius: 6px;
    box-sizing: border-box;
    padding: 4px 18px;
    white-space: nowrap;
    font-size: 14px;
    cursor: pointer;
    display: inline-flex;
    margin: 0px 10px 10px 0px;
    @media screen and (max-width: 768px) {
      background: #e5f1f8;
      border: 1px solid #0071bc;
      color: #0071bc;
      border-radius: 4px;
      box-sizing: border-box;
      padding: 4px 18px;
      white-space: nowrap;
      font-weight: 500;
      font-size: 13px;
      cursor: pointer;
      display: inline-flex;
      margin: 0px 10px 10px 0px;
    }
  }

  .recentItem span {
    white-space: nowrap;
    overflow: hidden;
    text-overflow: ellipsis;
    max-width: 55px;
  }

  .recentlySearchedWrap {
    padding-bottom: 15px;
    overflow-x: hidden;
  }

  .hasData {
    border-bottom: 1px solid #aac5d3;
    margin: 0px -15px;
    padding: 0px 15px 15px 15px;
    @media screen and (max-width: 768px) {
      border-bottom: 0px solid #aac5d3;
    }
  }

  .optionListTitle {
    color: #40464d;
    font-weight: bold;
    font-size: 14px;
    line-height: 24px;
    letter-spacing: 0.02em;
    padding: 12px 3px;
    @media screen and (max-width: 768px) {
      font-size: 13px;
      padding: 8px 3px;
      color: #2f5572;
      font-weight: bold;
      line-height: 24px;
      letter-spacing: 0.02em;
    }
  }

  .spinLoader {
    display: flex;
    justify-content: center;
    flex-direction: column;
    margin-top: 24px;
  }
  .spinLoaderText {
    font-style: normal;
    font-weight: 600;
    font-size: 14px;
    line-height: 24px;
    display: flex;
    align-items: center;
    letter-spacing: 0.02em;
    color: #728a9c;
    margin: auto;
    margin-bottom: -14px;
  }
`;
export const SearchWrapper = styled.div`
  flex-grow: 1;
  height: 37px;
  background-color: #e5f1f8;
  border-radius: 154px;
  /* border: 1px solid #0071bc; */
  display: flex;
  border-top-left-radius: 0px;
  border-bottom-left-radius: 0px;
  border-left: 0px;
  position: relative;
  z-index: 4;
  .cross-image {
    cursor: pointer;
    display: flex;
    padding-right: 15px;
  }
  .cross-image img {
    width: 30px;
  }

  .spinLoader {
    display: flex;
    justify-content: center;
    padding: 20px;
  }

  input::placeholder {
    color: #728a9c;
  }
  .tm-search-modal {
    position: absolute;
    top: 37px;
    background-color: #fff;
    box-shadow: 0px 3px 11px -2px rgba(0, 0, 0, 0.16);
    border-radius: 0px 0px 12px 12px;
    width: 98%;
  }

  .recently-searched-container {
    padding: 20px 16px;
    border-bottom: 1px solid #aac5d3;
  }

  .recently-inside-container {
    display: flex;
    align-items: center;
    justify-content: space-between;
    margin-bottom: 16px;
  }

  .recently-inside-container span {
    font-weight: 500;
    font-size: 14px;
  }

  .recently-inside-container span:nth-child(1) {
    color: #869eab;
  }

  .recently-inside-container span:nth-child(2) {
    color: #0071bc;
    text-decoration: underline;
    cursor: pointer;
  }

  .recently-meds-container {
    display: flex;
    align-items: center;
    gap: 12px;
  }

  .recently-meds-container div {
    background-color: #e5f1f8;
    border: 1px solid #0071bc;
    padding: 4px 18px;
    border-radius: 6px;
    cursor: pointer;
  }

  .search-result-container {
    display: flex;
    flex-direction: column;
  }

  .search-result-container p {
    margin: 0;
    padding: 12px 18px;
    font-size: 14px;
    font-weight: 400;
    background-color: #fff;
    cursor: pointer;
  }

  .search-result-container p:last-child {
    border-radius: 0px 0px 12px 12px;
  }

  .search-result-container p:hover {
    background-color: #e5f1f8;
  }

  > div[class="search-image"] {
    box-sizing: border-box;
    width: 17px;
    margin: 5px 5px 5px 10px;
    display: flex;
    align-items: center;
    cursor: pointer;
    > img {
      width: 100%;
    }
  }
  /* @media screen and (max-width: 968px) {
    margin: 0 10px 0px 0px;
  } */
  @media screen and (max-width: 768px) {
    position: static;
  }
  @media screen and (max-width: 468px) {
    width: 100%;
    margin: 0 10px 0px 0px;
    border-left: 0px;
    .tm-search-modal {
      position: fixed;
      width: 100%;
      left: 0;
      top: 115px;
    }
    .search-result-container p {
      font-size: 12px;
      padding: 8px 14px;
    }
    .recently-inside-container span {
      font-size: 12px;
    }
  }

  .coachmark {
    display: none;
    position: absolute;
    top: 41px;
    background: #22b573;
    border-radius: 6px;
    padding: 10px 13px;
    color: #fff;
    font-size: 14px;
    font-weight: 500;
    z-index: 2;
    @media screen and (max-width: 768px) {
      margin: auto;
      left: 0;
      right: 0;
      width: fit-content;
      top: 51px;
    }
  }

  .pointer {
    background: #22b573;
    width: 18px;
    height: 30px;
    position: absolute;
    transform: rotate(45deg);
    border-radius: 4px;
    z-index: -1;
    top: -8px;
    left: 25px;
    @media screen and (max-width: 768px) {
      left: 20%;
    }
  }

  .text {
    z-index: 2;
  }

  .searchDropDown {
    position: absolute;
    background: #ffffff;
    width: 100%;
    top: 36px;
    box-shadow: 0px 3px 11px -2px #00000029;
    border-bottom-right-radius: 12px;
    border-bottom-left-radius: 12px;
    padding: 0px 15px;
    @media screen and (max-width: 768px) {
      position: absolute;
      background: #ffffff;
      width: 100%;
      top: 57px;
      box-shadow: 0px 3px 11px -2px #00000029;
      border-bottom-right-radius: 12px;
      border-bottom-left-radius: 12px;
      padding: 0px 15px;
      left: 0;
      height: 100vh;
    }
  }
  .optionList {
    font-size: 14px;
    text-transform: lowercase;
    padding: 12px 3px;
    color: #2f5572;
    font-weight: 500;
    cursor: pointer;
    display: flex;
    justify-content: space-between;
    @media screen and (max-width: 768px) {
      font-size: 13px;
      text-transform: lowercase;
      padding: 8px 3px;
      color: #2f5572;
      font-weight: 600;
      cursor: pointer;
      display: flex;
      justify-content: space-between;
    }
  }

  .optionList .icon {
    display: none;
    @media screen and (max-width: 768px) {
      font-size: 13px;
      color: #0071bc;
      display: block;
    }
  }
  .optionList .iconProd {
    display: block;
    @media screen and (max-width: 768px) {
      font-size: 18px;
      color: #0071bc;
      display: block;
    }
  }

  .optionList:hover {
    background: #e5f1f8;
    margin: 0px -15px;
    padding: 12px 18px;
    @media screen and (max-width: 768px) {
      background: #e5f1f8;
      margin: 0px -15px;
      padding: 8px 18px;
    }
  }

  .optionList:first-letter {
    text-transform: capitalize;
  }
  span.searchResult {
    color: #728a9c;
    font-weight: 300;
  }
  .title {
    color: #869eab;
    font-size: 13px;
    line-height: 24px;
    padding: 5px 0px 10px 0px;
    font-weight: 500;
    letter-spacing: 0.02em;
    @media screen and (max-width: 768px) {
      color: #869eab;
      font-size: 12px;
      line-height: 24px;
      padding: 5px 0px 10px 0px;
      font-weight: 500;
      letter-spacing: 0.02em;
      text-transform: lowercase;
    }
  }
  .title:first-letter {
    text-transform: uppercase;
  }

  .titleWrapSearch {
    display: flex;
    justify-content: space-between;
  }

  .clearBtn {
    color: #0071bc;
    font-size: 13px;
    line-height: 24px;
    font-weight: 500;
    cursor: pointer;
    text-decoration: underline;
    padding: 5px 0px;
    letter-spacing: 0.02em;
  }

  .recentlySearchedContent {
    /* display: flex;
    gap: 10px; */
    overflow: hidden;
    max-height: 75px;
  }

  .recentItem {
    background: #e5f1f8;
    border: 1px solid #0071bc;
    color: #0071bc;
    border-radius: 6px;
    box-sizing: border-box;
    padding: 4px 18px;
    white-space: nowrap;
    font-size: 14px;
    cursor: pointer;
    display: inline-flex;
    margin: 0px 10px 10px 0px;
    @media screen and (max-width: 768px) {
      background: #e5f1f8;
      border: 1px solid #0071bc;
      color: #0071bc;
      border-radius: 4px;
      box-sizing: border-box;
      padding: 4px 18px;
      white-space: nowrap;
      font-weight: 500;
      font-size: 13px;
      cursor: pointer;
      display: inline-flex;
      margin: 0px 10px 10px 0px;
    }
  }

  .recentlySearchedWrap {
    padding-bottom: 15px;
    overflow-x: hidden;
  }

  .hasData {
    border-bottom: 1px solid #aac5d3;
    margin: 0px -15px;
    padding: 0px 15px 15px 15px;
    @media screen and (max-width: 768px) {
      border-bottom: 0px solid #aac5d3;
    }
  }

  .optionListTitle {
    color: #40464d;
    font-weight: bold;
    font-size: 14px;
    line-height: 24px;
    letter-spacing: 0.02em;
    padding: 12px 3px;
    @media screen and (max-width: 768px) {
      font-size: 13px;
      padding: 8px 3px;
      color: #2f5572;
      font-weight: bold;
      line-height: 24px;
      letter-spacing: 0.02em;
    }
  }
`;

export const Search = styled.input`
  width: 100%;
  height: inherit;
  background: transparent;
  border: 1px solid transparent;
  border-radius: 154px;
  text-indent: 5px;
  /* font-family: "Century Gothic", sans-serif; */
  font-size: 14px;
  font-style: normal;
  letter-spacing: 0.02em;
  color: #728a9c;
  caret-color: #728a9c;
  font-weight: 400;
  @media screen and (max-width: 368px) {
    font-size: 12px;
  }
`;

export const Hamburger = styled.div`
  width: 30px;
  margin: 0 5px 0 10px;

  > img {
    width: 70%;
  }
  display: none;
  @media screen and (max-width: 968px) {
    display: flex;
    align-items: center;
  }
  @media screen and (max-width: 468px) {
    width: 30px;
    display: flex;
    align-items: center;
  }
`;

// Truemeds logo
export const LogoHeader = styled.div`
  width: 130px;
  margin: 0 5px;
  > a > img {
    width: 100%;
    cursor: pointer;
  }
  @media screen and (max-width: 968px) {
    width: 110px;
    display: flex;
    align-items: center;
  }
`;

// Mobile view cart logo

export const CartLogo = styled.div`
  width: 20px;
  display: none;
  align-items: center;
  margin-right: 13px;
  box-sizing: border-box;
  > img {
    width: 100%;
  }
  @media screen and (max-width: 468px) {
    width: 25px;
    display: flex;
    align-items: center;
    margin-right: 13px;
  }
`;

// Upload Prescription button
export const Upload = styled.div`
  display: block;
  margin-left: 5px;
  > button > img {
    width: 15px;
  }
  @media screen and (max-width: 1068px) {
    display: none;
  }
`;

export const SpanUpload = styled.span`
  font-size: 12px;
  font-style: normal;
  font-weight: 600;
  line-height: 14px;
  letter-spacing: 0.02em;
  text-align: center;
  color: #0071bc;
  margin-left: 5px;
  width: 50px;
  overflow: hidden;
  text-overflow: ellipse;
`;

export const LoginText = styled.div`
  font-size: 14px;
  font-style: normal;
  font-weight: 600;
  line-height: 14px;
  letter-spacing: 0.02em;
  text-align: center;
  color: #0071bc;
  > span {
    cursor: pointer;
  }
  margin-left: 5px;
  @media screen and (max-width: 968px) {
    display: none;
  }
`;
export const ProfileData = styled.div`
  display: flex;
  margin-left: 5px;
  align-items: center;
  > img[alt="drop"] {
    width: 20px;
  }
  > img {
    width: 40px;
    height: 40px;
    border-radius: 50%;
  }
  > span {
    /* font-family: "Raleway", sans-serif; */
    font-style: normal;
    font-weight: 600;
    font-size: 14px;
    text-align: right;
    color: #333333;
    margin-right: 5px;
  }
  @media screen and (max-width: 968px) {
    display: none;
  }
`;

export const BadgeStyle = styled(Badge)`
  .MuiBadge-anchorOriginTopRightRectangle {
    top: 1px !important;
    right: 2.5px;
  }
`;

export const CartBtn = styled.div`
  cursor: pointer;
  margin-top: 2px;
  .tm-cart-text {
    color: #0071bc;
    font-weight: 600;
    font-size: 14px;
    margin-left: 7px;
  }
  @media screen and (max-width: 768px) {
    .tm-cart-text {
      display: none;
    }
  }
  @media screen and (max-width: 468px) {
    display: none;
  }
`;

export const PincodeWrapper = styled.div`
  background-color: #e5f1f8;
  border-radius: 154px;
  /* border: 1px solid #0071bc; */
  width: 110px;
  border-top-right-radius: 0px;
  border-bottom-right-radius: 0px;
  display: flex;
  position: relative;
  cursor: pointer;
  z-index: 4;
  > img {
    display: flex;
    width: 16%;
    margin: 0px 11px 0px 5px;
    @media screen and (max-width: 468px) {
      display: flex;
      width: 18%;
      margin: 0px 10px 0px 5px;
    }
  }

  @media screen and (max-width: 468px) {
    border-right: 0px;
    margin: 0px 0px 0px 10px;
    width: 130px;
  }
`;

export const PincodeContent = styled.div`
  width: 100%;
  margin: auto;
  text-align: left;
  font-size: 11px;
  color: #003055;
  /* font-family: "Raleway"; */
  margin-left: 15px;
  display: block;
`;
export const PincodeLabel = styled.div``;
export const PincodeValue = styled.div`
  font-weight: bold;
  white-space: nowrap;
  text-overflow: ellipsis;
  overflow: hidden;
  width: 58px;
  @media screen and (max-width: 468px) {
    width: 50px;
  }
`;
export const PincodeBlabel = styled.div`
  /* display: flex; */
  margin: auto;
  color: #003055;
  /* font-family: "Raleway"; */
  font-size: 11px;
  font-weight: 500;
  cursor: pointer;
`;

export const DialogWrapper = styled.div`
  padding-bottom: 30px;
  padding-top: 10px;
  > div[class="choose"] {
    /* font-family: "Raleway"; */
    font-size: 15px;
    color: #003055;
    font-weight: 600;
    margin-top: 20px;
    margin-bottom: 5px;
    @media screen and (max-width: 468px) {
      margin-top: 50px;
      font-size: 16px;
    }
  }
  @media screen and (max-width: 468px) {
    padding-bottom: 50px;
    padding-top: 10px;
  }
`;

export const SelectPincodeWrapper = styled.div`
  display: flex;
  > div[class="welcomeText"] {
    /* font-family: "Raleway"; */
    font-size: 20px;
    color: #003055;
    font-weight: 600;
    padding-right: 15px;
  }
  > img {
    width: 120px;
  }
`;

export const ButtonWrapper = styled.div`
  display: flex;
  flex-direction: row;
  > div {
    order: 1;
    @media screen and (max-width: 468px) {
      order: 3;
    }
  }
  > div {
    position: relative;
    order: 1;
    @media screen and (max-width: 468px) {
      width: 100% !important;
      order: 3;
    }
  }
  > div > div[class="warningIcon"] {
    position: absolute;
    top: 10px;
    right: 15px;
  }
  > div > div[class="warningIcon"] > span[class="redIcon"] {
    font-size: 15px;
    color: #e74c3c;
  }
  > div > div[class="warningIcon"] > span[class="greenIcon"] {
    font-size: 15px;
    color: green;
  }
  > div > input {
    width: 240px !important;
    height: 44px !important;
    display: flex !important;
    justify-content: center;
    align-items: center;
    align-self: center;
    border-radius: 6px !important;
    /* font-family: Raleway; */
    font-size: 15px;
    @media screen and (max-width: 468px) {
      width: 100% !important;
      order: 3;
    }
  }
  > img {
    padding: 0px 10px;
    height: 44px;
    width: 65px;
    order: 2;
    @media screen and (max-width: 468px) {
      width: 100%;
      margin: 10px 0px;
    }
  }
  > button {
    order: 3;
    background-color: #0071bc;
    color: #fff;
    display: flex;
    width: 240px;
    margin: auto;
    height: 44px;
    align-items: center;
    justify-content: center;
    > img {
      display: flex;
      width: 13%;
      vertical-align: middle;
      align-items: center;
      height: 100%;
      padding: 0px 5px;
    }
    > div {
      display: flex;
      vertical-align: middle;
      align-items: center;
      height: 100%;
      font-size: 15px;
      /* font-family: "Raleway"; */
      color: #fff !important;
    }

    @media screen and (max-width: 468px) {
      order: 1;
      width: 100%;
      > img {
        width: 30px;
      }
    }
  }

  .loginTextWrap {
    display: flex;
    color: #0071bc;
    font-size: 12px;
    font-weight: 700;
    justify-content: flex-start;
    padding: 5px 0px;
    gap: 5px;
    align-items: center;
    vertical-align: middle;
    line-height: 12px;
    cursor: pointer;
  }

  .loginTextWrap .iconWrap {
    font-size: 15px;
  }

  @media screen and (max-width: 468px) {
    flex-direction: column;
  }
`;

export const CloseBtn = styled.div`
  > img {
    position: absolute;
    top: 10px;
    right: 10px;
    cursor: pointer;
    width: 17px;
    @media screen and (max-width: 468px) {
      position: absolute;
      top: 30px;
      right: 25px;
      cursor: pointer;
      width: 22px;
    }
  }
`;

export const Pointer = styled.div`
  position: fixed;
  background: #ffffff;
  width: 15px;
  height: 15px;
  top: 9%;
  -webkit-transform: rotate(45deg);
  -ms-transform: rotate(45deg);
  transform: rotate(45deg);
  left: 27%;
`;

export const ErrorModalWrapper = styled.div`
  display: grid;
  grid-template-columns: repeat(3, 1fr);
  gap: 10px;
  > div[class="imgWrap"] {
    grid-column: 1 / span 1;
  }
  > div[class="textWrap"] {
    grid-column: 2 / span 2;
    > div[class="title1"] {
      color: #003055;
      /* font-family: "Raleway"; */
      font-weight: 600;
      font-size: 15px;
      line-height: 20px;
      margin-bottom: 20px;
    }
    > div[class="title"] {
      color: #003055;
      /* font-family: "Raleway"; */
      font-weight: 600;
      font-size: 15px;
      line-height: 20px;
    }
    > div[class="value"] {
      color: #003055;
      /* font-family: "Raleway"; */
      font-weight: 400;
      font-size: 13px;
      line-height: 16px;
      margin-top: 5px;
    }
  }
`;

export const ReEnterWrap = styled.div`
  > button {
    color: #0071bc;
    border: 1px solid #0071bc;
    border-radius: 20px;
    font-size: 13px;
    /* font-family: "Raleway"; */
    font-weight: 500;
    margin: auto;
    display: block;
    margin-top: 15px;
    > span {
      display: inline-flex;
      font-size: 11px;
      padding-right: 4px;
    }
  }
`;

export const PincodeSeperator = styled.div`
  background-color: #003055;
  width: 0.5px;
  height: 20px;
  position: absolute;
  right: 1px;
  border-radius: 80px;
  top: 0;
  bottom: 0;
  margin: auto;

  @media screen and (max-width: 768px) {
    right: 0px;
  }
`;

export const DeliverSpinner = styled.div`
  display: flex;
  align-content: center;
  justify-content: center;
  margin: auto;
  width: 100%;
`;

export const StepsWrapper = styled.div`
  > div[class="stepTitle"] {
    /* font-family: "Raleway"; */
    padding-bottom: 5px;
    color: #555;
    font-weight: 500;
  }
  > img {
    max-width: 250px;
    width: 100%;
    padding-bottom: 10px;
  }
  @media screen and (max-width: 468px) {
    > div[class="stepTitle"] {
      /* font-family: "Raleway"; */
      padding-bottom: 5px;
      color: #555;
      font-weight: 500;
      font-size: 14px;
    }
    > img {
      max-width: 80%;
      width: 100%;
      padding-bottom: 10px;
    }
  }
`;

export const StepsWarning = styled.div`
  /* font-family: "Raleway"; */
  font-weight: 600;
  font-size: 14px;
  padding-bottom: 20px;
  color: #003055;
`;

export const PPTCWrap = styled.div``;
export const PPTitle = styled.div`
  font-size: 20px;
  text-align: center;
  margin: 5px 10px;
  font-weight: 600;
`;
export const PPWrap = styled.div`
  .policyWrap {
    max-height: 300px;
    overflow-y: scroll;
  }

  .textWrap {
    text-align: center;
    font-size: 14px;
    color: #333;
    margin-top: 15px;
  }
  .btnWrap {
    text-align: center;
    margin-top: 10px;
  }
  .btnWrap button:first-child {
    background-color: #e5f1f8;
    color: #0071bc;
    border: 1px solid #0071bc;
    margin: 10px;
    border-radius: 2px;
    width: 150px;
    border-radius: 154px;
    white-space: nowrap;
    @media screen and (max-width: 468px) {
      width: 120px;
    }
  }
  .btnWrap button:last-child {
    background-color: #e5f1f8;
    color: #0071bc;
    border: 1px solid #0071bc;
    margin: 10px;
    border-radius: 2px;
    width: 150px;
    border-radius: 154px;
    white-space: nowrap;
    @media screen and (max-width: 468px) {
      width: 120px;
    }
  }
`;

export const NewMobHeader = styled.div`
  display: none;
  @media screen and (max-width: 768px) {
    display: flex;
    align-items: center;
    justify-content: space-between;
    padding: 18px;
    background-color: #fff;
    z-index: 1100;
    filter: drop-shadow(0px 1px 8px rgba(0, 0, 0, 0.25));
    position: sticky;
    top: 0;
    .newMobInnerHeader {
      display: flex;
      align-items: center;
    }
    .newMobInnerHeader img {
      margin-right: 16px;
    }
    .newMobInnerHeader span {
      font-weight: 600;
      font-size: 16px;
      color: #333;
    }
    img {
      width: 20px;
    }
  }
`;

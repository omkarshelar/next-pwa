import React from "react";
import { connect } from "react-redux";
import Router, { withRouter } from "next/router";
// import LinearProgress from "@material-ui/core/LinearProgress";
import Link from "next/link";

//action imports
import {
  addRecentSearchAction,
  removeRecentSearchAction,
} from "../../../redux/RecentSearch/action";
import {
  toggleSidebarAction,
  getMasterDetailsThunkNew,
} from "../../../redux/SliderService/Actions";
import {
  toCartSectionAction,
  toUploadPrescriptionAction,
  toOrderSummaryAction,
} from "../../../redux/Timeline/Action";
import {
  checkPincodeThunk,
  fetchAddressThunk,
  saveAddressThunk,
} from "../../../redux/AddressDetails/Action";
import {
  pincodeSectionAction,
  fetchMedicineThunkPostSelect,
  saveSelectedAddress,
} from "../../../redux/Pincode/Actions";
import {
  checkIfCustomerAcceptedPPAndTNCThunk,
  acceptPPOrTNCThunk,
} from "../../../redux/Login/Action";
import { fetchMedicineThunkOnSearch } from "../../../redux/SearchMedicine/Action";

//css imports
import "../NewModal/Modal.css";
import "./Navigation.css";
import {
  Search,
  LoginText,
  NavContent,
  LogoHeader,
  NavContainer,
  Hamburger,
  SearchWrapper,
  SearchParent,
  LogoWrapper,
  CartLogo,
  BadgeStyle,
  ProfileData,
  CartBtn,
  PincodeWrapper,
  PincodeContent,
  PincodeLabel,
  PincodeValue,
  PincodeBlabel,
  SelectPincodeWrapper,
  DialogWrapper,
  ButtonWrapper,
  CloseBtn,
  Pointer,
  ErrorModalWrapper,
  ReEnterWrap,
  PincodeSeperator,
  DeliverSpinner,
  StepsWrapper,
  StepsWarning,
  PPTCWrap,
  PPWrap,
  PPTitle,
  NewMobHeader,
} from "./Navigation.style";

//component or helper imports
import Button from "../Button/Button";
import convertProductTitle from "../../Helper/convertProductTitle";
// import SimplePopover from "./Popover";
// import Slide1 from "./Slide";
// import Login from "../Login/Login";
import {
  eventHomepageLogin,
  eventHomepageSearchTyping,
  eventPdPageViewed,
} from "../../../src/Events/Events";
// import SidePanel from "../../WebComponents/SidePanel/SidePanel";

//plugin imports
// import axios from "axios";
import window from "global";
import { ImArrowUpLeft2 } from "react-icons/im";
import { BiChevronRight } from "react-icons/bi";
import ShoppingCartIcon from "@material-ui/icons/ShoppingCart";
import Dialog from "@material-ui/core/Dialog";
import Slide from "@material-ui/core/Slide";
import { LoadingOutlined } from "@ant-design/icons";
import { Input, message, Spin } from "antd";
import { IoIosArrowDroprightCircle } from "react-icons/io";
import {
  ExclamationCircleFilled,
  CheckCircleFilled,
  LeftOutlined,
} from "@ant-design/icons";
// import Lottie from "react-lottie";

//imageImports
import Magnify from "./Search.svg";
import cross from "./plus.svg";
import locationPin from "./location-pin.svg";
import locationPinWhite from "./location-pin-white.svg";
import orLogo from "./orLogo.svg";
import Ham from "./Hamburger.png";
import crossCloseMob from "./mobX.svg";
import TrueLogoTemp from "../../../src/Assets/truemedslogosvg.svg";
import closeCross from "./closecross.svg";
import mobOr from "./mobOr.svg";
import pincodeError from "./pincodeError.png";
import web1 from "./permission-assets/web1.png";
import web2 from "./permission-assets/web2.png";
import mob1 from "./permission-assets/mob1.jpg";
import mob2 from "./permission-assets/mob2.jpg";
import mob3 from "./permission-assets/mob3.jpg";
import searchIconNew from "../../../src/Assets/search.svg";
import AddressList from "./SubComponent/AddressList";
import animationData from "../../../src/Assets/searchLoader.json";
import backIcon from "../../../src/Assets/sidepanel-back.svg";

//Dynamic Imports
import dynamic from "next/dynamic";

const Login = dynamic(() => import("../Login/Login"));
const SimplePopover = dynamic(() => import("./Popover"));
const Slide1 = dynamic(() => import("./Slide"));
const Lottie = dynamic(() => import("react-lottie"));
const SidePanel = dynamic(() =>
  import("../../WebComponents/SidePanel/SidePanel")
);
const axios = dynamic(() => import("axios"));

const defaultOptions = {
  loop: true,
  autoplay: true,
  animationData: animationData,
  rendererSettings: {
    preserveAspectRatio: "xMidYMid slice",
  },
};

const TransitionMob = React.forwardRef(function Transition(props, ref) {
  return <Slide direction="up" ref={ref} {...props} />;
});

const TransitionWeb = React.forwardRef(function Transition(props, ref) {
  return <Slide direction="down" ref={ref} {...props} />;
});
const antIcon = <LoadingOutlined style={{ fontSize: 24 }} spin />;

message.config({
  maxCount: 1,
});
let isLocalUpdate = false;

class Navigation extends React.Component {
  constructor(props) {
    super(props);
  }
  state = {
    loginModal: false,
    progress: 0,
    open: false,
    pincode: this.props.pincodeDetails?.pincode,
    deliveryPincode: this.props.pincodeDetails?.pincode
      ? this.props.pincodeDetails.pincode
      : "",
    city: this.props.pincodeDetails?.city ? this.props.pincodeDetails.city : "",
    pinError: false,
    errorDialog: false,
    isLoading: false,
    selectedAddress: false,
    detectDialog: false,
    isLocalUpdate: false,
    searchFocus: false,
    inputMedicine: "",
    isRecentOrSearched: false,
    showDropDown: false,
    searchInput: "",
    searchValues: undefined,
    showLoader: false,
    openSideBar: false,
    activeComponent: 0,
    selectedData: null,
  };

  componentDidMount() {
    console.log(
      "Looks like you love looking under the hood, Interested in joining our team? send us your resume on hr@truemeds.in"
    );
    //  privacy policy code here
    // if (this.props.accessToken && this.props.profile?.CustomerDto?.customerId) {
    //   this.checkCustomerAcceptDetails();
    // }

    window.openSideBar = function (isOpen, value, selectedData) {
      that.setState({
        openSideBar: isOpen,
        activeComponent: value,
        selectedData: selectedData ? selectedData : null,
      });
    };

    window.openPincodeModal = function (value) {
      that.setState({
        open: value,
      });
    };

    if (!this.props.addressList) {
      this.fetchAddressData();
    }

    if (this.props.pincodeDetails) {
      if (
        !this.props.pincodeDetails.warehouseId &&
        this.props.pincodeDetails.pincode
      ) {
        this.handlePincodeValue(this.props.pincodeDetails.pincode, 1);
      }
    }

    //Open after 1 sec
    /* setTimeout(
      () =>
        this.state.deliveryPincode == ""
          ? this.setState({ open: true, loginModal: false }, () => {
              document.getElementById("pincodeWrapper").style.zIndex = "10001";
              if (window.innerWidth > 468) {
                document.getElementById("navContainer").style.position =
                  "unset";
              }
            })
          : "",
      1000
    ); */

    if (this.props.accessToken) {
      if (this.state.deliveryPincode && this.props.addressList) {
        this.props.addressList.map((e) => {
          if (e.pincode == this.state.deliveryPincode) {
            this.props.saveSelectedAddress(e);
          }
        });
      }
    }

    // to open and close the div
    let that = this;
    let idArray = [
      "searchDropDown",
      "searchInput",
      "clearBtn",
      "recentItem",
      "scroller",
      "titleWrap",
      "addMedBtn",
      "optionList",
    ];

    document.onclick = function (e) {
      if (!idArray.includes(e.target.id)) {
        that.setState({
          showDropDown: false,
          searchValues: undefined,
        });
      }
      if (document.getElementById("coachmark") && e.target.id != "addMedBtn") {
        document.getElementById("coachmark").style.display = "none";
      }

      if (!idArray.includes(e.target.id)) {
        that.setState({ searchInput: "" });
      }

      if (e.target.id === "addMedBtn") {
        that.activateSearchFocus();
      }
    };

    this.activateSearchFocus();

    window.onpopstate = function () {
      window.openSideBar(false);
    };
  }

  handleCheck = () => {
    clearTimeout(this.timer);
    this.timer = setTimeout(() => {
      this.triggerCheck();
    }, 300);
  };

  searchResultHandler = (value, searchInput) => {
    if (value.indexOf(searchInput) > -1) {
      return (
        <React.Fragment>
          <span id="optionList">
            {value.substr(0, value.indexOf(searchInput))}
          </span>
          <span id="optionList" className="searchResult">
            {searchInput}
          </span>
          <span id="optionList">
            {value.substr(
              value.indexOf(searchInput) + searchInput.length,
              value.length
            )}
          </span>
        </React.Fragment>
      );
    } else {
      return (
        <React.Fragment>
          <span>{value}</span>
        </React.Fragment>
      );
    }
  };

  triggerCheck = () => {
    this.setState({
      showLoader: true,
    });
    let cnt = 0;

    let searchValues = [];
    let searchInput = this.state.searchInput.toLowerCase();

    let data = {};
    data.access_token = this.props.accessToken;
    data.searchString = this.state.searchInput;
    data.isMultiSearch = true;
    data.elasticSearchType = "SKU_BRAND_SEARCH";
    data.warehouseId = this.props.pincodeDetails?.warehouseId
      ? this.props.pincodeDetails?.warehouseId
      : 3;
    if (this.state.searchInput) {
      this.props.fetchMedicineThunkOnSearch(data).then(() => {
        if (this.props.medicineList?.motherBrandHits.length > 1) {
          this.props.medicineList.motherBrandHits.map((e) => {
            if (cnt < 10) {
              cnt++;
              let value = e._source.original_mother_brand.toLowerCase();
              searchValues.push(
                <div
                  id="optionList"
                  className="optionList"
                  onClick={() =>
                    this.redirectToSearch(e._source.original_mother_brand, 1)
                  }
                >
                  <div id="optionList">
                    {this.searchResultHandler(value, searchInput)}
                  </div>
                  <div id="optionList">
                    <ImArrowUpLeft2 className="icon" />
                  </div>
                </div>
              );
            }
          });
        }
        if (this.props.medicineList?.motherBrandHits.length <= 1) {
          cnt = 0;
        }
        if (this.props.medicineList?.hits.length > 0) {
          this.props.medicineList.hits.map((e) => {
            if (cnt < 10) {
              cnt++;
              let value = e._source.original_sku_name.toLowerCase();
              searchValues.push(
                <div
                  id="optionList"
                  className="optionList"
                  onClick={() => {
                    eventPdPageViewed();
                    this.redirectToSearch(
                      convertProductTitle(
                        e._source.original_sku_name,
                        e._source.original_product_code
                      ),
                      2
                    );
                  }}
                >
                  <div id="optionList">
                    {this.searchResultHandler(value, searchInput)}
                  </div>
                  <div id="optionList">
                    <BiChevronRight className="iconProd" />
                  </div>
                </div>
              );
            }
          });
        }

        if (
          this.props.medicineList?.motherBrandHits.length <= 1 &&
          this.props.medicineList?.hits.length === 0
        ) {
          cnt = 0;
          if (this.props.medicineList.suggestion) {
            this.props.medicineList.suggestion.map((e) => {
              e.options.map((f) => {
                if (cnt < 10) {
                  cnt++;
                  let value = f.text.toLowerCase();
                  searchValues.push(
                    <div
                      id="optionList"
                      className="optionList"
                      onClick={() => this.redirectToSearch(f.text, 1)}
                    >
                      <div id="optionList">
                        {this.searchResultHandler(value, searchInput)}
                      </div>
                      <div id="optionList">
                        <ImArrowUpLeft2 className="icon" />
                      </div>
                    </div>
                  );
                }
              });
            });
            if (searchValues.length > 0) {
              searchValues.unshift(
                <div className="optionListTitle">{"Did you mean:"}</div>
              );
            }
          }
        }

        this.setState({
          searchValues: searchValues,
          showLoader: false,
        });
      });
    } else {
      this.setState({
        showLoader: false,
      });
    }
  };

  fetchAddressData = () => {
    let that = this;
    if (this.props.accessToken) {
      let data = {
        accessToken: this.props.accessToken,
        history: Router,
      };
      this.props.fetchAddressThunk(data).then(() => {
        if (this.props.addressData.fetchAddressError) {
          message.warning("something went wrong");
        } else if (this.props.addressList?.length > 0) {
          let pincode = that.state.pincode;
          that.props.addressList.map(function (data) {
            if (data.pincode == that.state.pincode) {
              pincode = "";
            }
            if (that.state.deliveryPincode && that.props.accessToken) {
              if (data.pincode == that.state.deliveryPincode) {
                that.props.saveSelectedAddress(data);
              }
            }
          });

          that.setState({ pincode });
        }
      });
    }
  };

  componentDidUpdate(prevProps, prevState) {
    if (
      this.state.searchFocus &&
      this.props.medicineList?.hits?.length > 0 &&
      prevProps.medicineList !== this.props.medicineList
    ) {
      this.setState({ isRecentOrSearched: true });
    }

    if (prevState.inputMedicine !== this.state.inputMedicine) {
      this.handleCheck();
    }
    if (
      this.props.accessToken != prevProps.accessToken &&
      !this.props.addressList
    ) {
      this.fetchAddressData();

      //  privacy policy code here
      // if (
      //   this.props.accessToken &&
      //   this.props.profile?.CustomerDto?.customerId
      // ) {
      //   this.checkCustomerAcceptDetails();
      // }
    }

    if (this.props.addressList != prevProps.addressList) {
      this.updatePincode();
    }

    if (
      this.props.pincodeDetails?.pincode != prevProps.pincodeDetails?.pincode &&
      prevProps.pincodeDetails?.pincode
    ) {
      if (isLocalUpdate) {
        isLocalUpdate = false;
        this.setState({
          isLocalUpdate: false,
        });
      } else {
        this.setState({
          isLoading: true,
        });
        this.handlePincodeValue(this.props.pincodeDetails?.pincode, 1);
      }
    }
    if (this.state.searchInput != prevState.searchInput) {
      this.handleCheck();
    }

    if (this.props.router?.query?.search != prevProps.router?.query?.search) {
      this.activateSearchFocus();
    }
  }

  activateSearchFocus = () => {
    const isSearch = this.props.router?.query?.search;

    if (isSearch) {
      this.setState(
        {
          showDropDown: true,
          showLoader: false,
        },
        () => {
          document.getElementById("searchInput").focus();
          if (document.getElementById("coachmark")) {
            document.getElementById("coachmark").style.display = "block";
          }
        }
      );
    }
  };

  async updatePincode() {
    if (this.state.deliveryPincode == "") {
      let pincode = "";
      let cnt = 0;
      if (this.props.addressList) {
        this.props.addressList.map((data) => {
          if (data.addressType === "Home" && cnt === 0) {
            pincode = data.pincode;
            cnt++;
            this.props.saveSelectedAddress(data);
          }
        });

        if (this.props.addressList.length > 0 && cnt === 0) {
          pincode =
            this.props.addressList[this.props.addressList.length - 1].pincode;
          this.props.saveSelectedAddress(
            this.props.addressList[this.props.addressList.length - 1]
          );
        }

        if (this.props.addressList.length == 0) {
          this.setState({
            isLoading: true,
          });
          const res = await axios.get("https://geolocation-db.com/json/");
          let ipv4 = res.data.IPv4;
          await axios
            .get(`https://ipapi.co/${ipv4}/json/`)
            .then((res) => {
              if (res.data.postal) {
                pincode = res.data.postal;
              } else {
                pincode = "400079";
              }
            })
            .catch((error) => {
              pincode = "400079";
            });
        }
      }

      if (pincode) {
        this.handlePincodeValue(pincode, 1);
      }
    }
  }

  setProgress = () => {
    if (this.state.progress < 40) {
      this.setState({ progress: this.state.progress + 10 });
    } else if (this.state.progress < 65) {
      this.setState({ progress: this.state.progress + 5 });
    } else if (this.state.progress < 90) {
      this.setState({ progress: this.state.progress + 1 });
    }
  };

  setInitialProgress = () => {
    this.setState({ progress: 0 });
  };

  finalProgressCall = () => {
    if (!this.props.isLoading && this.state.progress > 0) {
      this.setInitialProgress();
    } else if (this.props.isLoading) {
      setTimeout(() => this.setProgress(), 100);
    }
  };

  handlePincode(e) {
    let pincode = e.target.value;
    isLocalUpdate = true;
    this.setState(
      {
        isLoading: true,
        isLocalUpdate: true,
      },
      () => {
        this.handlePincodeValue(pincode);
      }
    );
  }

  handlePincodeValue(pincode, type = 0) {
    if (pincode && pincode.replace(/[^\d{1,6}]/gi, "").length <= 6) {
      if (pincode.replace(/[^\d{1,6}]/gi, "").length === 6) {
        let data = {
          pincode: pincode,
          accessToken: "fb83371f-288f-4b5d-8d4a-d61328ad9e81",
          history: Router,
        };
        this.props.checkPincodeThunk(data).then((response) => {
          if (
            !this.props.pincodeError &&
            this.props.isServiceable?.isServiceable
          ) {
            this.setState({
              open: false,
              deliveryPincode: pincode,
              city: this.props.isServiceable.pincodeData,
              warehouseId: this.props.isServiceable.warehouseId,
              pinError: false,
              isLoading: false,
              pincode: "",
            });
            let data = {
              pincode: pincode,
              city: this.state.city,
              warehouseId: this.props.isServiceable.warehouseId,
            };

            if (isLocalUpdate && this.props.cartContent.length > 0) {
              let medList = [];
              this.props.cartContent.map((e) => {
                medList.push(e._source.original_product_code);
              });

              let data1 = {
                query: {
                  bool: {
                    should: [
                      { terms: { "original_product_code.keyword": medList } },
                    ],
                  },
                },
              };
              let params = {};
              params.medicine = data1;
              params.warehouseId = this.props.isServiceable.warehouseId;
              params.cartContent = this.props.cartContent;
              if (this.props.accessToken) {
                params.access_token = this.props.accessToken;
              }
              this.props.fetchMedicineThunkPostSelect(params);
            }

            this.props.pincodeSectionAction(data);
            if (document.getElementById("pincodeWrapper")) {
              document.getElementById("pincodeWrapper").style.zIndex = "1";
            }
          } else {
            if (this.props.currentPage === "orderFlow") {
              this.setState({
                pinError: false,
                open: false,
                errorDialog: false,
                isLoading: false,
                detectDialog: false,
              });
            } else {
              this.setState({
                pinError: true,
                open: false,
                errorDialog: true,
                isLoading: false,
                detectDialog: false,
              });
            }
          }
        });
        /* this.setState({
          pincode: pincode.replace(/[^\d{1,6}]/gi, ""),
        }); */
        if (type === 1) {
          this.setState({
            pincode: "",
          });
        } else {
          this.setState({
            pincode: pincode.replace(/[^\d{1,6}]/gi, ""),
          });
        }
      } else {
        this.setState({
          pincode: pincode.replace(/[^\d{1,6}]/gi, ""),
          isLoading: false,
        });
      }
    } else {
      this.setState({
        isLoading: false,
      });
      return;
    }
  }

  handleSticky() {
    if (!this.props.isMobile) {
      document.getElementById("navContainer").style.position = "unset";
    }
  }

  addScroller = () => {
    let scroller = document.getElementsByClassName("recentlySearchedContent");
    let existingScroller = document.getElementById("scroller");
    if (scroller && scroller.length > 0 && !existingScroller) {
      scroller[0].setAttribute("id", "scroller");
    }
  };

  handleSearchKeychange = (e) => {
    if (this.state.searchInput && e.key === "Enter") {
      this.setState({
        showDropDown: false,
      });
      this.redirectToSearch(this.state.searchInput.replaceAll("%", ""), 1);
    }
  };

  handleSearchChange = (e) => {
    if (e.target.value?.length === 1) {
      eventHomepageSearchTyping();
    }
    let { showLoader } = this.state;
    if (e.target.value === "") {
      showLoader = false;
    }
    this.setState({
      searchInput: e.target.value,
      showDropDown: true,
      showLoader: showLoader,
    });
  };

  redirectToSearch(value, type) {
    if (this.state.searchInput) {
      this.props.addRecentSearchAction(this.state.searchInput);
    }
    switch (type) {
      case 1:
        Router.push("/search/" + encodeURIComponent(value), undefined, {
          shallow: true,
        });
        break;
      case 2:
        Router.push("/medicine/" + encodeURIComponent(value), undefined, {
          shallow: true,
        });
        break;
      default:
        Router.push("/search/" + encodeURIComponent(value), undefined, {
          shallow: true,
        });
        break;
    }
    this.setState({ showDropDown: false });
  }

  handleSearchIcon = () => {
    if (this.state.searchInput) {
      this.setState({
        showDropDown: false,
      });
      this.redirectToSearch(this.state.searchInput.replaceAll("%", ""), 1);
    }
  };

  addScroller = () => {
    let scroller = document.getElementsByClassName("recentlySearchedContent");
    let existingScroller = document.getElementById("scroller");
    if (scroller && scroller.length > 0 && !existingScroller) {
      scroller[0].setAttribute("id", "scroller");
    }
  };

  handleSearchDropDownClose = () => {
    if (!this.props.isMobile) {
      this.setState({
        searchInput: "",
      });
    } else {
      this.setState(
        {
          searchInput: "",
          searchFocus: false,
          showDropDown: false,
        },
        () => {
          if (
            this.props.currentPage === "MedSearch" &&
            this.props.medicineList?.hits.length > 0
          ) {
            Router.push(this.props.router?.asPath);
          } else {
            Router.push("/");
          }
        }
      );
    }
  };

  loginModalHandler = () => {
    eventHomepageLogin();
    this.setState({ loginModal: !this.state.loginModal });
  };

  handleAgree = (type) => {
    message.loading("Action in progress", 0);
    let data = {};
    data.access_token = this.props.accessToken;
    data.type = 1;
    data.customerId = this.props.profile.CustomerId;

    this.props.acceptPPOrTNCThunk(data).then(() => {
      if (this.props.AllDetails.acceptStatus) {
        let data = {};
        data.access_token = this.props.accessToken;
        data.type = 2;
        data.customerId = this.props.profile.CustomerId;

        this.props.acceptPPOrTNCThunk(data).then(() => {
          if (this.props.AllDetails.acceptStatus) {
            message.destroy();
            message.success(
              "Thank you for accepting our policy and terms & conditions"
            );
            // this.handlePrivacyClose();
            this.props
              .checkIfCustomerAcceptedPPAndTNCThunk({
                access_token: this.props.accessToken,
                customerId: this.props.profile.CustomerDto.customerId,
                history: Router,
              })
              .then(() => {
                let value = 0;
                let tstamp = new Date(new Date().getTime() + 86400000);

                if (
                  this.props.AllDetails.checkStatus.accpetedTermsConditions &&
                  this.props.AllDetails.checkStatus.accpetedPrivacyPolicy
                ) {
                  value = 3;
                }
                //setting in Localstorage for checking values 1: if policy only 2: if tnc only 3: if both
                localStorage.setItem(
                  "custAcceptedPPTC",
                  JSON.stringify({
                    value: value,
                    timestamp: tstamp,
                  })
                );
              });
          }
        });
      } else if (this.props.AllDetails.acceptStatusError) {
        message.destroy();
        message.error(this.props.AllDetails.acceptStatusError);
      } else {
        message.destroy();
        message.success("Something went wrong");
      }
    });
  };

  async setForcePincode() {
    if (!this.state.deliveryPincode) {
      this.setState({
        isLoading: true,
        isLocalUpdate: true,
      });
      isLocalUpdate = true;
      const res = await axios.get("https://geolocation-db.com/json/");
      let ipv4 = res.data.IPv4;
      await axios
        .get(`https://ipapi.co/${ipv4}/json/`)
        .then((res) => {
          if (res.data.postal) {
            this.handlePincodeValue(res.data.postal);
          } else {
            this.handlePincodeValue("400079");
          }
        })
        .catch((error) => {
          this.handlePincodeValue("400079");
        });
    }
    if (this.props.isMobile) {
      document.getElementById("navContainer").style.position = "sticky";
    }
  }

  getSelectedPincode = (data) => {
    this.setState({
      isLoading: true,
      isLocalUpdate: true,
    });
    isLocalUpdate = true;
    this.handlePincodeValue(data.pincode, 1);
    this.props.saveSelectedAddress(data);
  };

  backClick = () => {
    if (this.props.router?.query?.detail) {
      Router.back();
    } else if (this.props.Stepper.toUploadPrescription === true) {
      if (this.props.cartContent.length > 0) {
        this.props.toCartSectionAction();
      } else {
        Router.back();
      }
    } else if (this.props.Stepper.toOrderSummary === true) {
      if (this.props.imgUpload?.uploadHistory.length > 0) {
        this.props.toUploadPrescriptionAction();
      } else {
        this.props.toCartSectionAction();
      }
    } else {
      if (this.props.router?.asPath === "/empty-cart") {
        Router.push("/");
      } else {
        Router.back();
      }
    }
  };

  pageTitle = () => {
    let title = "";
    if (this.props.router?.asPath.split("/")[1].indexOf("orderstatus") > -1) {
      title = "Order Status";
    } else if (
      this.props.router?.asPath.split("/")[1].indexOf("myorders") > -1
    ) {
      title = "My Orders";
    } else if (
      this.props.router?.asPath.split("/")[1].indexOf("myorderdetails") > -1
    ) {
      title = "Order Details";
    } else if (this.props.Stepper.toCartSection === true) {
      title = "Cart";
    } else if (this.props.Stepper.toUploadPrescription === true) {
      title = "Upload Prescription";
    } else if (this.props.Stepper.toOrderSummary === true) {
      title = "Order Summary";
    }
    return title;
  };

  render() {
    this.finalProgressCall();
    let { location, history, timeout, loginStatus, profile, otherProfile } =
      this.props;
    let { loginModal, searchValues } = this.state;
    let recentlySearched = [];
    if (this.props.recentSearch && this.props.recentSearch.length > 0) {
      this.props.recentSearch.map((e) => {
        recentlySearched.push(e);
      });
      recentlySearched.reverse();
    }
    let cartHideArray = ["orderflow"];
    if (this.props.isMobile && this.props.newHeader) {
      return (
        <>
          <SidePanel
            visible={this.state.openSideBar}
            activeComponent={this.state.activeComponent}
            selectedData={this.state.selectedData}
            onCloseFunc={() => this.setState({ openSideBar: false })}
          />
          <NewMobHeader>
            <div className="newMobInnerHeader">
              <img src={backIcon} alt="back" onClick={this.backClick} />
              <span>{this.pageTitle()}</span>
            </div>
            {this.props.showSearch && (
              <img
                id={"addMedBtn"}
                src={searchIconNew}
                alt="search"
                onClick={() => Router.push("/?search=true")}
              />
            )}
          </NewMobHeader>
        </>
      );
    } else {
      return (
        <>
          {loginModal && (
            <Login
              loginModal={loginModal}
              loginModalHandler={this.loginModalHandler}
            />
          )}
          <SidePanel
            visible={this.state.openSideBar}
            activeComponent={this.state.activeComponent}
            selectedData={this.state.selectedData}
            onCloseFunc={() => this.setState({ openSideBar: false })}
          />
          <NavContainer id="navContainer">
            {/* NavContent is responsive not NavContainer. */}
            <NavContent>
              <LogoWrapper>
                <Hamburger>
                  <img
                    src={Ham}
                    alt="Logo"
                    title="Truemeds"
                    onClick={() => this.props.toggleSidebarAction()}
                  ></img>
                </Hamburger>

                <LogoHeader>
                  <Link href="/">
                    <a>
                      <img
                        src={TrueLogoTemp}
                        alt="truemeds-logo"
                        title="Truemeds"
                      />
                    </a>
                  </Link>
                </LogoHeader>
                {!cartHideArray.includes(this.props.currentPage) ||
                (this.props.currentPage === "orderFlow" &&
                  this.props.Stepper.toCartSection === true) ? (
                  <CartLogo
                    onClick={() => {
                      this.props.toCartSectionAction();
                      Router.push("/orderflow");
                      // Event_Truemeds
                      // eventCartViewed();
                    }}
                  >
                    <BadgeStyle
                      overlap="rectangular"
                      badgeContent={this.props.cart}
                      color="primary"
                    >
                      <ShoppingCartIcon
                        style={{ color: "#0071bc", width: "20px" }}
                      />
                    </BadgeStyle>
                  </CartLogo>
                ) : (
                  this.props.isMobile && <div className="dummyCartIcon"></div>
                )}
              </LogoWrapper>
              <SearchParent>
                <PincodeWrapper
                  id="pincodeWrapper"
                  onClick={() => {
                    if (
                      this.props.currentPage == "orderFlow" &&
                      this.props.Stepper.toOrderSummary
                    ) {
                      message.warning(
                        "Pincode change not available on Order Summary"
                      );
                    } else {
                      this.setState({
                        open: true,
                        detectDialog: false,
                        errorDialog: false,
                      });
                    }
                    document.getElementById("pincodeWrapper").style.zIndex =
                      "10001";
                    this.handleSticky();
                  }}
                >
                  {this.state.deliveryPincode == "" ? (
                    this.state.isLoading ? (
                      <DeliverSpinner>
                        <Spin indicator={antIcon} />
                      </DeliverSpinner>
                    ) : (
                      <PincodeBlabel>Deliver to</PincodeBlabel>
                    )
                  ) : this.state.isLoading ? (
                    <DeliverSpinner>
                      <Spin indicator={antIcon} />
                    </DeliverSpinner>
                  ) : (
                    <PincodeContent>
                      <PincodeLabel>{this.state.deliveryPincode}</PincodeLabel>
                      <PincodeValue>{this.state.city}</PincodeValue>
                    </PincodeContent>
                  )}
                  <PincodeSeperator id="seperator" />
                  <img src={locationPin} alt="location" />
                </PincodeWrapper>
                {/* {location.pathname === "/search" ? ( */}
                <SearchWrapper>
                  <div
                    onClick={() => {
                      this.handleSearchIcon();
                    }}
                    className="search-image"
                  >
                    <img src={Magnify} alt="search" />
                  </div>

                  <Search
                    autoComplete="off"
                    id="searchInput"
                    placeholder={
                      this.state.searchFocus
                        ? ""
                        : "Search for Medicines & Health care products"
                    }
                    value={this.state.searchInput}
                    // onChange={this.props.setInputSearch}
                    onChange={(e) => {
                      this.handleSearchChange(e);
                    }}
                    onFocus={() => this.setState({ searchFocus: true })}
                    onBlur={() => this.setState({ searchFocus: false })}
                    onClick={() => {
                      this.setState({ showDropDown: true }, () => {
                        this.addScroller();
                      });
                    }}
                    onKeyPress={(e) => this.handleSearchKeychange(e)}
                  />
                  {this.state.showDropDown ? (
                    <div
                      className="cross-image"
                      onClick={() => {
                        this.handleSearchDropDownClose();
                      }}
                    >
                      <img src={cross} alt="clear" />
                    </div>
                  ) : (
                    ""
                  )}
                  {/* {this.state.isRecentOrSearched ? (
                      <div className="tm-search-modal">
                        {this.props.recentSearch?.length > 0 && (
                          <div className="recently-searched-container">
                            <div className="recently-inside-container">
                              <span>recently searched</span>
                              <span
                                onClick={() =>
                                  this.props.removeRecentSearchAction()
                                }
                              >
                                clear
                              </span>
                            </div>
                            <div className="recently-meds-container">
                              {this.props.recentSearch.map((med) => (
                                <div
                                  onClick={() => {
                                    this.props.history.push(`/search/${med}`);
                                    this.setState({
                                      isRecentOrSearched: false,
                                      inputMedicine: "",
                                    });
                                  }}
                                >
                                  <span>{med}</span>
                                </div>
                              ))}
                            </div>
                          </div>
                        )}
                        {this.props.medicineList?.hits?.length > 0 && (
                          <div className="search-result-container">
                            <p onClick={() => this.onClickMed()}>cremaffin plus</p>
                          </div>
                        )}
                      </div>
                    ) : null} */}
                  {this.state.showDropDown &&
                  !searchValues &&
                  this.props.recentSearch?.length === 0 &&
                  !this.props.isMobile ? (
                    <div id="coachmark" className="coachmark">
                      <div className="pointer"></div>
                      <div className="text">
                        Search for the medicines you want
                      </div>
                    </div>
                  ) : (
                    ""
                  )}

                  {/* <div className="tm-search-modal">
                      <span>Search Results</span>
                    </div> */}
                </SearchWrapper>
                <div className="newSearchWrapper">
                  {this.state.showDropDown &&
                  (this.props.recentSearch?.length > 0 || searchValues) ? (
                    <div id="searchDropDown" className="searchDropDown">
                      {this.props.recentSearch?.length > 0 ? (
                        <div
                          id="searchDropDown"
                          className={
                            searchValues && searchValues.length > 0
                              ? "recentlySearchedWrap hasData"
                              : "recentlySearchedWrap"
                          }
                        >
                          <div id="titleWrap" className="titleWrapSearch">
                            <div id="titleWrap" className="title">
                              recently searched
                            </div>
                            <div
                              id="clearBtn"
                              className="clearBtn"
                              onClick={() => {
                                this.setState({
                                  showDropDown: false,
                                  searchInput: "",
                                });
                                if (this.props.currentPage === "orderFlow") {
                                  Router.push("/orderflow?search=true");
                                } else {
                                  Router.push("/?search=true");
                                }
                                this.props.removeRecentSearchAction();
                              }}
                            >
                              clear
                            </div>
                          </div>
                          <div
                            id="scroller"
                            className="recentlySearchedContent"
                          >
                            {recentlySearched.map((e) => {
                              return (
                                <div
                                  id="recentItem"
                                  className="recentItem"
                                  onClick={() => {
                                    this.setState({
                                      showDropDown: false,
                                      searchInput: e,
                                    });
                                    this.redirectToSearch(
                                      e.replaceAll("%", "")
                                    );
                                  }}
                                >
                                  <span id="recentItem">{e}</span>
                                </div>
                              );
                            })}
                          </div>
                        </div>
                      ) : (
                        ""
                      )}
                      {this.state.showLoader ? (
                        <div className="spinLoader">
                          <div className="spinLoaderText">Searching...</div>
                          <Lottie
                            options={defaultOptions}
                            height={100}
                            width={100}
                          />
                        </div>
                      ) : (
                        searchValues &&
                        searchValues.map((e) => {
                          return e;
                        })
                      )}
                    </div>
                  ) : (
                    ""
                  )}
                </div>

                {/* ) : (
                    <SearchWrapper>
                      <div className="search-image">
                        <img src={Magnify} alt="search"></img>
                      </div>
    
                      <Search
                        placeholder="Search for Medicines & Health care products "
                        onClick={() => {
                          this.goToSearchPageHandler();
                        }}
                      />
                    </SearchWrapper>
                  )} */}
              </SearchParent>
              {/* <Upload
                  className="prescBtn"
                  onClick={() => {
                    this.props.toUploadPrescriptionAction();
                    this.props.history.push("/orderflow");
                    // Event_Truemeds
                    eventSearchPrescription();
                  }}
                >
                  <Button
                    uploadPres
                    className="uplPrescriptionBtn"
                    title="Upload Prescription"
                  >
                    <img src={paperclip} alt="upload"></img>
                    <SpanUpload>Upload Prescription</SpanUpload>
                  </Button>
                </Upload> */}
              {/* {(timeout === 401 || !loginStatus) && (
                  <LoginText onClick={() => this.loginModalHandler()}>
                    <span style={{ whiteSpace: "nowrap" }}>Login / Signup</span>
                  </LoginText>
                )} */}
              <div
                style={{ display: "flex", alignItems: "center", gap: "15px" }}
              >
                {loginStatus && this.props.accessToken ? (
                  <>
                    <ProfileData>
                      <SimplePopover
                        allOrders={this.props.orderHistory?.allOrdersSuccess}
                        imgSrc={
                          otherProfile?.CustomerDetails?.profileImageUrl
                            ? otherProfile?.CustomerDetails?.profileImageUrl
                            : profile?.CustomerDto?.profileImageUrl
                            ? profile?.CustomerDto?.profileImageUrl
                            : "avatar"
                        }
                        cusEmail={
                          otherProfile?.CustomerDetails?.emailAddress
                            ? otherProfile?.CustomerDetails?.emailAddress
                            : profile?.CustomerDto?.emailAddress
                            ? profile?.CustomerDto?.emailAddress
                            : ""
                        }
                        cusName={
                          otherProfile?.CustomerDetails?.customerName
                            ? otherProfile?.CustomerDetails?.customerName
                            : profile?.CustomerDto?.customerName
                            ? profile?.CustomerDto?.customerName
                            : ""
                        }
                        removeRecentSearchAction={() => {
                          this.props.removeRecentSearchAction();
                        }}
                      />
                    </ProfileData>
                  </>
                ) : (
                  <LoginText onClick={() => this.loginModalHandler()}>
                    <span style={{ whiteSpace: "nowrap" }}>Login / Signup</span>
                  </LoginText>
                )}
                {!cartHideArray.includes(this.props.currentPage) ||
                (this.props.currentPage === "orderFlow" &&
                  (this.props.Stepper.toCartSection === true ||
                    this.props.Stepper.toUploadPrescription === true)) ? (
                  <CartBtn
                    onClick={() => {
                      if (this.props.cartContent?.length > 0) {
                        this.props.toCartSectionAction();
                        Router.push("/orderflow");
                      } else {
                        Router.push("/empty-cart");
                      }
                    }}
                    style={{ cursor: "pointer" }}
                  >
                    <BadgeStyle
                      overlap="rectangular"
                      color="primary"
                      badgeContent={this.props.cart}
                    >
                      <ShoppingCartIcon style={{ color: "#0071bc" }} />
                    </BadgeStyle>
                    <span className="tm-cart-text">Cart</span>
                  </CartBtn>
                ) : (
                  <div className="dummyCartIcon"></div>
                )}
              </div>
            </NavContent>
          </NavContainer>
          <Slide1
            timeout={timeout}
            loginStatus={loginStatus}
            isMobile={this.props.isMobile}
          ></Slide1>
          {/* {this.props.isLoading && (
            <LinearProgress variant="determinate" value={this.state.progress} />
          )} */}
          {/* {!this.props.isLoading && (
            )} */}

          <Dialog
            fullScreen={false}
            open={this.state.open}
            onClose={() => {
              // this.shakeModal();
              this.setState({
                open: false,
              });
            }}
            aria-labelledby="responsive-dialog-title"
            classes={{
              paper: "location-rectangle animate__animated",
            }}
            id="responsive-dialog-title"
            TransitionComponent={
              this.props.isMobile ? TransitionMob : TransitionWeb
            }
          >
            <DialogWrapper>
              <SelectPincodeWrapper>
                <div className="welcomeText">Welcome to</div>
                <img src={TrueLogoTemp} alt="logo"></img>
              </SelectPincodeWrapper>

              {this.props.addressList && this.props.accessToken && (
                <>
                  {this.props.addressList.length > 0 ? (
                    <div className="choose">
                      Select your location to see availability.
                    </div>
                  ) : (
                    ""
                  )}
                  <AddressList
                    addressList={this.props.addressList}
                    getSelectedPincode={(e) => this.getSelectedPincode(e)}
                    pincode={this.state.deliveryPincode}
                    isLoading={this.state.isLoading}
                  />
                </>
              )}

              <div className="choose">Choose your location</div>
              <ButtonWrapper>
                <div>
                  <Input
                    placeholder={"Enter PIN Code"}
                    onChange={this.handlePincode.bind(this)}
                    value={this.state.pincode}
                  />
                  <div className="warningIcon">
                    {this.state.isLoading && this.state.pincode != "" ? (
                      <Spin indicator={antIcon} />
                    ) : this.state.pinError ? (
                      <ExclamationCircleFilled
                        className="redIcon"
                        style={{ color: "#ffc37d" }}
                      />
                    ) : (
                      this.state.pincode &&
                      this.state.pincode.length === 6 && (
                        <CheckCircleFilled
                          className="greenIcon"
                          style={{ color: "#30b94f" }}
                        />
                      )
                    )}
                  </div>
                  {!this.props.accessToken ? (
                    <div
                      className="loginTextWrap"
                      onClick={() => {
                        this.loginModalHandler();
                        this.setState({ open: false });
                        isLoginUpdate = true;
                      }}
                    >
                      <div>Login to view saved address</div>
                      <div className="iconWrap">
                        <IoIosArrowDroprightCircle />
                      </div>
                    </div>
                  ) : (
                    ""
                  )}
                </div>
                <img
                  style={{ display: "none" }}
                  src={!this.props.isMobile ? orLogo : mobOr}
                  alt="or"
                />
                <Button
                  style={{ display: "none" }}
                  onClick={() => {
                    this.detectLocation();
                  }}
                >
                  <img src={locationPinWhite} alt="pin" />
                  <div>Detect my location</div>
                </Button>
              </ButtonWrapper>
              <CloseBtn
                onClick={() => {
                  this.setState({ open: false }, () => {
                    this.setForcePincode();
                    document.getElementById("pincodeWrapper").style.zIndex =
                      "1";
                  });
                }}
              >
                <img
                  src={!this.props.isMobile ? closeCross : crossCloseMob}
                  alt="close"
                />
              </CloseBtn>
            </DialogWrapper>
            {!this.props.isMobile && <Pointer></Pointer>}
          </Dialog>

          <Dialog
            fullScreen={false}
            open={this.state.errorDialog}
            aria-labelledby="responsive-dialog-title"
            classes={{ paper: "location-rectangle" }}
            id="responsive-dialog-title"
            onClose={() => {
              this.setState({
                errorDialog: false,
                deliveryPincode: "400079",
                pincode: "400079",
              });
              this.handlePincodeValue("400079");
            }}
          >
            <DialogWrapper>
              <ErrorModalWrapper>
                <div className="imgWrap">
                  <img src={pincodeError} alt="error logo" />
                </div>
                <div className="textWrap">
                  <div className="title1">
                    We don’t have any services available in your area right now.
                  </div>
                  <div className="title" style={{ display: "none" }}>
                    We are currently having our services in following areas:
                  </div>
                  <div className="value" style={{ display: "none" }}>
                    Kolhapur, Dharwad, Hubli, Panjim, Margao, Miraj, Gokak,
                    Sangli, Karad, Ratnagiri, Satara.
                  </div>
                </div>
              </ErrorModalWrapper>
              <ReEnterWrap>
                <Button
                  onClick={() => {
                    this.setState({ open: true, errorDialog: false });
                  }}
                >
                  <LeftOutlined className="icon" /> Re-enter Pincode
                </Button>
              </ReEnterWrap>
            </DialogWrapper>
            {!this.props.isMobile && <Pointer></Pointer>}
          </Dialog>

          {/* {this.state.open && (
              <SelectPincodeWrapper>
                <div className="overlay"></div>
                <LocationRectangle class></LocationRectangle>
              </SelectPincodeWrapper>
            )} */}
          <Dialog
            fullScreen={false}
            open={this.state.detectDialog}
            aria-labelledby="responsive-dialog-title"
            classes={{ paper: "location-rectangle" }}
            id="responsive-dialog-title"
            onClose={() => {
              this.setState({ detectDialog: false, open: true });
              document.getElementById("pincodeWrapper").style.zIndex = "1";
            }}
          >
            <DialogWrapper>
              <StepsWarning>
                Your location access is blocked for truemeds.in. Follow below
                given steps to unblock the access.
              </StepsWarning>
              {!this.props.isMobile ? (
                <StepsWrapper>
                  <div className="stepTitle">
                    {"Step 1 - Click on the lock"}
                  </div>
                  <img src={web1} alt="step1" />
                  <div className="stepTitle">
                    {
                      "Step 2 - Select Allow option instead of Block for location."
                    }
                  </div>
                  <img src={web2} alt="step2" />
                </StepsWrapper>
              ) : (
                <StepsWrapper>
                  <div className="stepTitle">
                    {"Step 1 - Click on the lock"}
                  </div>
                  <img src={mob1} alt="step1" />
                  <div className="stepTitle">
                    {"Step 2 - Select permissions."}
                  </div>
                  <img src={mob2} alt="step2" />
                  <div className="stepTitle">
                    {"Step 3 - Select toggle Location option to allow access."}
                  </div>
                  <img src={mob3} alt="step3" />
                </StepsWrapper>
              )}
            </DialogWrapper>
            {!this.props.isMobile && <Pointer></Pointer>}
          </Dialog>
          <PPTCWrap>
            <Dialog
              Title
              fullScreen={false}
              // open={this.state.privacyPolicy}
              open={false}
              // style={{ zIndex: 1400 }}
              /* onClose={() => {
                  this.handlePrivacyClose();
                }} */
            >
              <DialogWrapper>
                <PPTitle>Privacy Policy & Terms and Conditions</PPTitle>
                <PPWrap>
                  <div className="policyWrap">
                    {/*   <PrivacyPolicy isHeader={true} />
  
                    <Tnc isHeader={true} /> */}
                  </div>

                  <div className="btnWrap">
                    <div className="textWrap">
                      {"Please accept the terms and conditions to proceed"}
                    </div>
                    <Button
                      onClick={() => {
                        this.handleAgree(1);
                      }}
                      className="agree"
                    >
                      Accept
                    </Button>
                    {/*  <Button
                        onClick={() => {
                          this.handlePrivacyClose();
                        }}
                        className="cancel"
                      >
                        Cancel
                      </Button> */}
                  </div>
                </PPWrap>
              </DialogWrapper>
            </Dialog>
            {/* <Dialog
                Title
                fullScreen={false}
                open={this.state.tncPolicy}
                onClose={() => {
                  this.handleTncClose();
                }}
              >
                <DialogWrapper>
                  <DialogTitle>Terms And Conditions</DialogTitle>
                  <PPWrap>
                    <div className="policyWrap">
                      <Tnc />
                    </div>
                    <div className="btnWrap">
                      <Button
                        onClick={() => {
                          this.handleAgree(2);
                        }}
                        className="agree"
                      >
                        Agree
                      </Button>
                      <Button
                        onClick={() => {
                          this.handleTncClose();
                        }}
                        className="cancel"
                      >
                        Cancel
                      </Button>
                    </div>
                  </PPWrap>
                </DialogWrapper>
              </Dialog> */}
          </PPTCWrap>
        </>
      );
    }
  }
}

const mapStateToProps = (state) => ({
  Stepper: state.stepper,
  timeout: state.myOrder?.orderTrackerError,
  loginStatus: state.loginReducer?.isUserLogged,
  isLoading: state.loader?.isLoading,
  profile: state.loginReducer?.verifyOtpSuccess,
  cart: state.cartData?.cartItems?.length,
  otherProfile: state.loginReducer?.customerDetails,
  orderHistory: state.myOrder,
  isServiceable: state.addressData?.pincodeSuccess,
  pincodeError: state.addressData?.pincodeError,
  pincodeDetails: state.pincodeData?.pincodeData,
  pincodeSelectedAddress: state.pincodeSelctedAddress?.pincodeSelectedAddress,
  accessToken: state.loginReducer?.verifyOtpSuccess?.Response?.access_token,
  addressList: state.addressData?.fetchAddressSuccess
    ? state.addressData?.fetchAddressSuccess
    : state.loginReducer?.verifyOtpSuccess?.AddressList.AddressList,
  addressData: state.addressData,
  cartContent: state.cartData?.cartItems,
  updateDetail: state.orderStatusUpdate?.OrderStatusData,
  currentPage: state.pincodeData?.currentPage?.currentPage,
  AllDetails: state.loginReducer,
  // medicineList: state.searchMedicine?.medDataSuccess,
  medicineList: state.searchMedicine?.medDataSearchSuccess,
  recentSearch: state.recentSearch?.searchHistory,
  medDataSearchFailure: state.searchMedicine?.medDataSearchFailure,
  imgUpload: state.uploadImage,
});

const mapDispatchToProps = {
  getMasterDetailsThunkNew,
  addRecentSearchAction,
  toggleSidebarAction,
  removeRecentSearchAction,
  toCartSectionAction,
  fetchMedicineThunkPostSelect,
  checkPincodeThunk,
  pincodeSectionAction,
  acceptPPOrTNCThunk,
  checkIfCustomerAcceptedPPAndTNCThunk,
  fetchMedicineThunkOnSearch,
  fetchAddressThunk,
  saveSelectedAddress,
};
export default withRouter(
  connect(mapStateToProps, mapDispatchToProps)(Navigation)
);

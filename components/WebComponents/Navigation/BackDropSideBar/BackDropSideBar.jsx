import React from "react";
import { connect } from "react-redux";
import { toggleSidebarAction } from "../../../../redux/SliderService/Actions";
import "./BackDropSideBar.css";
const BackDropSideBar = (props) =>
  props.show ? (
    <div
      className="Backdrop"
      onClick={() => {
        props.dispatch(toggleSidebarAction());
      }}
    ></div>
  ) : null;

const mapStateToProps = (state) => ({
  show: state.sidebar?.backdrop,
});

export default connect(mapStateToProps)(BackDropSideBar);

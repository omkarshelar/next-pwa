import Badge from "@material-ui/core/Badge";
import styled from "styled-components";

// Parent wrapper.
export const SlideContainer = styled.div`
  background-color: #003055;
  z-index: 1000;
`;

// Content wrapper.
export const SlideContent = styled.div`
  display: flex;
  justify-content: space-between;
  align-items: center;
  width: 70vw;
  margin: 0 auto;
  padding: 10px 0;
  color: #ffffff;

  @media screen and (max-width: 1400px) {
    width: 80vw;
  }
  @media screen and (max-width: 1210px) {
    width: 95vw;
  }

  @media screen and (max-width: 768px) {
    display: none;
  }
`;

export const Contact = styled.div`
  display: flex;
  align-items: center;
  width: 10rem;
  /* font-family: "Century Gothic", sans-serif; */
  > a {
    padding-left: 5px;
    color: white;
    text-decoration: none;
    /* font-family: "Century Gothic", sans-serif; */
  }
`;

export const Option = styled.div`
  width: 600px;
  display: flex;
  align-items: center;
  gap: 2rem;
  justify-content: flex-end;
  > span {
    cursor: pointer;
  }
`;

export const BadgeStyle = styled(Badge)`
  .MuiBadge-anchorOriginTopRightRectangle {
    top: 3px !important;
  }
`;

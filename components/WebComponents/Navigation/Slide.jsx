import React from "react";
import PropTypes from "prop-types";
import CssBaseline from "@material-ui/core/CssBaseline";
import useScrollTrigger from "@material-ui/core/useScrollTrigger";
import Slide from "@material-ui/core/Slide";
import { SlideContainer, SlideContent, Contact, Option } from "./Slide.style";
import Router, { withRouter } from "next/router";
import { connect } from "react-redux";
import { toCartSectionAction } from "../../../redux/Timeline/Action";
import { clearOrderStatusDetailsDataAction } from "../../../redux/storeData/Action";
import PhoneIcon from "@material-ui/icons/Phone";
import {
  eventHomepageCallHeader,
  eventHomepageHealthArticles,
  eventHomepageMyOrders,
  eventHomepageNeedHelp,
  eventHomepageTmWallet,
} from "../../../src/Events/Events";

function HideOnScroll(props) {
  const { children, window } = props;
  const trigger = useScrollTrigger({ target: window ? window() : undefined });

  return (
    <Slide appear={false} direction="down" in={!trigger}>
      {children}
    </Slide>
  );
}

HideOnScroll.propTypes = {
  children: PropTypes.element.isRequired,
  window: PropTypes.func,
};

export function HideAppBar(props) {
  const navigateHelp = () => {
    if (
      !props.decide &&
      props.orderHistory &&
      (props.orderHistory?.currentOrder?.length > 0 ||
        props.orderHistory?.pastOrder?.length > 0)
    ) {
      Router.push("/help/order", undefined, { shallow: true });
    } else {
      Router.push("/help", undefined, { shallow: true });
    }
  };

  const triggerNavbarCallEvent = () => {
    // Event_Truemeds
    eventHomepageCallHeader();
  };
  return (
    <React.Fragment>
      <CssBaseline />
      <HideOnScroll {...props}>
        <SlideContainer>
          <SlideContent>
            <Contact onClick={() => triggerNavbarCallEvent()}>
              <PhoneIcon style={{ color: "white" }}></PhoneIcon>
              <a href="tel:022-48977965"> 022-48977965</a>
            </Contact>

            <Option>
              {props.loginStatus && props.timeout !== 401 && (
                <>
                  {/* {props.orderHistory &&
                    (props.orderHistory?.currentOrder?.length > 0 ||
                      props.orderHistory?.pastOrder?.length > 0) && (
                      <a style={{ color: "#fff" }} href="/myorders">
                        <span
                          onClick={() => {
                            eventHomepageMyOrders();
                          }}
                        >
                          My Orders
                        </span>
                      </a>
                    )} */}
                  <span
                    onClick={() => {
                      eventHomepageMyOrders();
                      props.clearOrderStatusDetailsDataAction().then(() => {
                        Router.push("/myorders", undefined, {
                          shallow: true,
                        });
                      });
                      // Router.push("/myorders", undefined, { shallow: true });
                    }}
                  >
                    My Orders
                  </span>
                  <span
                    onClick={() => {
                      eventHomepageTmWallet();
                      Router.push("/wallet", undefined, { shallow: true });
                    }}
                  >
                    TM Wallet
                  </span>
                </>
              )}

              <span
                onClick={() => {
                  eventHomepageHealthArticles();
                  Router.push("/blog", undefined, { shallow: true });
                }}
              >
                Health Articles
              </span>
              <span
                onClick={() => {
                  eventHomepageNeedHelp();
                  navigateHelp();
                }}
              >
                Need Help
              </span>
              {/* <span onClick={() =>Router.push("/option/aboutus")}>
                About Us
              </span> */}
              {/* {props.loginStatus && props.timeout !== 401 && ( */}
              {/* //? Shifted Cart to top Navbar */}
              {/* <span
                onClick={() => {
                  props.toCartSectionAction();
                 Router.push("/orderflow");
                }}
              >
                Cart{" "}
                <BadgeStyle badgeContent={props.cart} color="primary">
                  <ShoppingCartIcon style={{ color: "white" }} />
                </BadgeStyle>
              </span> */}
              {/* )} */}
            </Option>
          </SlideContent>
        </SlideContainer>
      </HideOnScroll>
    </React.Fragment>
  );
}
const mapStateToProps = (state) => ({
  cart: state.cartData?.cartItems?.length,
  decide: state.myOrder?.orderTrackerError,
  orderHistory: state.myOrder?.allOrdersSuccess,
});

export default withRouter(
  connect(mapStateToProps, {
    toCartSectionAction,
    clearOrderStatusDetailsDataAction,
  })(HideAppBar)
);

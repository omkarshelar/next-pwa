import React, { Component } from "react";

//? Styled Imports
import "./FAQSidebar.styled";
import Accordion from "@material-ui/core/Accordion";
import AccordionSummary from "@material-ui/core/AccordionSummary";
import AccordionDetails from "@material-ui/core/AccordionDetails";
import ExpandMoreIcon from "@material-ui/icons/ExpandMore";
import { FAQDiv, List, SectionTitle } from "./FAQSidebar.styled";
import { Button, Divider } from "antd";
import { MdOutlineChevronRight } from "react-icons/md";
import { Dialog, IconButton } from "@material-ui/core";
import DialogContent from "@material-ui/core/DialogContent";
import DialogTitle from "@material-ui/core/DialogTitle";
import { IoClose, IoArrowBack } from "react-icons/io5";
import Slide from "@material-ui/core/Slide";
import FAQLoaderCard from "./FAQItemLoader/FAQLoaderCard";
import window from "global";
//? Images Imports
import TermsShadow from "../../../src/Assets/TermsShadow.svg";
import styled from "styled-components";
import Router, { withRouter } from "next/router";
//? Service Imports
//? Constants
const TransitionMob = React.forwardRef(function Transition(props, ref) {
  return <Slide direction="up" ref={ref} {...props} />;
  // console.log("🚀 ~ file: FAQSidebar.jsx ~ line 24 ~ TransitionMob ~ props", props);
});

export const LineButton = styled(Button)`
  &&& {
    span {
      color: ${(props) => (props.tncBtn ? "#868889" : "#0071bc")};
      font-weight: ${(props) => (props.tncBtn ? "400" : "unset")};
      text-decoration: ${(props) => (props.tncBtn ? "underline" : "none")};
    }

    svg {
      fill: ${(props) => (props.faqClose ? "#40464d" : "#0071bc")};
      width: ${(props) => props.faqClose && "24px"};
      height: ${(props) => props.faqClose && "24px"};
    }
  }
`;

const TransitionWeb = React.forwardRef(function Transition(props, ref) {
  return <Slide direction="left" ref={ref} {...props} />;
});

export class FAQSidebar extends Component {
  state = {
    faqModal: false,
  };
  render() {
    return (
      <FAQDiv>
        <SectionTitle>Frequently asked questions</SectionTitle>

        <List>
          <div className="FAQModalBody" style={{ padding: 0 }}>
            {this.props.loading ? (
              <FAQLoaderCard />
            ) : this.props.faqData && window.innerWidth > 768 ? (
              this.props.faqData.map((item, pos) => (
                <Accordion square key={pos}>
                  <AccordionSummary
                    expandIcon={<ExpandMoreIcon />}
                    aria-controls="panel1a-content"
                    id="panel1a-header"
                  >
                    <span className="faq-list-text">{item.question}</span>
                  </AccordionSummary>
                  <AccordionDetails style={{ paddingLeft: "2.5em" }}>
                    {item.answer}
                  </AccordionDetails>
                </Accordion>
              ))
            ) : this.props.faqData && window.innerWidth <= 768 ? (
              this.props.faqData.slice(0, 3).map((item, pos) => (
                <Accordion square key={pos}>
                  <AccordionSummary
                    expandIcon={<ExpandMoreIcon />}
                    aria-controls="panel1a-content"
                    id="panel1a-header"
                  >
                    <span className="faq-list-text">{item.question}</span>
                  </AccordionSummary>
                  <AccordionDetails style={{ paddingLeft: "2.5em" }}>
                    {item.answer}
                  </AccordionDetails>
                </Accordion>
              ))
            ) : (
              ""
            )}
          </div>
          {window.innerWidth <= 768 && (
            <LineButton
              type="link"
              seeMore
              onClick={() => this.setState({ faqModal: true })}
            >
              View More <MdOutlineChevronRight />
            </LineButton>
          )}

          {window.innerWidth <= 768 && (
            <img
              src={TermsShadow}
              alt="shadow-divider"
              className="termsShadow"
            />
          )}

          {/* <img src={TermsShadow} alt="shadow-divider" className="termsShadow" /> */}

          <LineButton
            type="link"
            seeMore
            className="termsBtn"
            tncBtn
            onClick={() =>
              Router.push({
                pathname: "/option/legals/tnc",
                query: {
                  isTMWALLET: this.props.tmwalletCondition ? true : false,
                },
              })
            }
          >
            Terms & Conditions
          </LineButton>

          <Dialog
            fullWidth
            fullHeight
            maxWidth={"xs"}
            open={this.state.faqModal}
            className="FAQModal"
            // onClose={() => this.setState({ faqModal: false })}
            // TransitionComponent={TransitionWeb}
            TransitionComponent={
              window.innerWidth > 767 ? TransitionWeb : TransitionMob
            }
          >
            <div className="FAQModalBody">
              <div className="titleWrapper">
                {/* <IconButton
                  edge="start"
                  color="inherit"
                  onClick={() => this.setState({ faqModal: false })}
                  aria-label="close"
                >
                  <IoClose />
                </IconButton> */}
                <SectionTitle>Frequently asked questions</SectionTitle>

                <LineButton
                  type="text"
                  faqClose
                  onClick={() => this.setState({ faqModal: false })}
                >
                  {window.innerWidth > 767 ? <IoClose /> : <IoArrowBack />}
                </LineButton>
              </div>

              <div className="faqTitleDivider"></div>
              <DialogContent>
                {this.props.faqData
                  ? this.props.faqData.map((item, pos) => (
                      <Accordion square key={pos}>
                        <AccordionSummary
                          expandIcon={<ExpandMoreIcon />}
                          aria-controls="panel1a-content"
                          id="panel1a-header"
                        >
                          <span className="faq-list-text">{item.question}</span>
                        </AccordionSummary>
                        <AccordionDetails style={{ paddingLeft: "2.5em" }}>
                          {item.answer}
                        </AccordionDetails>
                      </Accordion>
                    ))
                  : ""}
              </DialogContent>
            </div>
          </Dialog>
        </List>
      </FAQDiv>
    );
  }
}

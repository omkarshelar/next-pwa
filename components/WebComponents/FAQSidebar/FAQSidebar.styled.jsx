import styled from "styled-components";

export const FAQDiv = styled.div`
  height: max-content;
  position: sticky;
  top: calc(0% + 100px);
`;

export const SectionTitle = styled.h4`
  font-weight: 700;
  margin: 1em 0;
  z-index: 5;
  position: relative;
  color: #40464d;
  font-size: 16.67px;
  line-height: 27.78px;

  @media only screen and (max-width: 767px) {
    font-size: 16px;
    line-height: 29px;
    margin-bottom: 1em;
  }
`;

export const List = styled.div`
  width: 100%;
  display: flex;
  flex-direction: column;
  justify-content: space-between;
  @media screen and (max-width: 568px) {
    /* width: 95%; */
    margin: auto;
  }

  .termsShadow {
    width: calc(100% + 30px);
    margin-left: -15px;
  }

  > button {
    /* align-self: left; */
    padding-left: 0;
    padding-right: 0;
    margin-top: 1em;
    display: flex;
    justify-content: flex-start;
    align-items: center;
    gap: 0.3em;
    font-size: small;
    font-weight: 700;
    svg {
      font-size: medium;
    }
  }

  .MuiPaper-elevation1 {
    box-shadow: none;
  }

  .MuiAccordion-root {
    border-bottom: 1px solid var(--border-clr) !important;

    &::before {
      display: none;
    }

    .faq-list-text {
      font-weight: 600;
      font-size: 16px;
      line-height: 19.45px;
      display: flex;
      color: #40464d;

      @media screen and (max-width: 767px) {
        font-size: 14px;
        line-height: 21px;
        font-weight: 500;
      }

      &::before {
        content: "Q.";
        margin-right: 1em;
        /* font-weight: 700; */
      }
    }
  }

  .MuiAccordionSummary-content {
    margin: 19px 0 !important;

    .faq-list-text {
      font-weight: 600;
      font-size: 14px;
      line-height: 19.45px;
      color: #40464d;
    }
  }

  .MuiAccordionSummary-content.Mui-expanded {
    margin: 19px 0 !important;
  }

  .MuiIconButton-root {
    padding: 18px 12px;
  }

  .MuiAccordionSummary-root {
    align-items: flex-start !important;
  }

  .MuiAccordionSummary-root.Mui-expanded {
    min-height: unset;
  }

  .MuiAccordion-root.Mui-expanded {
    margin: unset;
  }

  .MuiAccordionDetails-root {
    font-size: 14px;
    line-height: 20.38px;
    font-weight: 400;
    color: #40464d;
  }

  @media screen and (max-width: 767px) {
    .MuiAccordionDetails-root {
      font-size: 14px;
      line-height: 22px;
      font-weight: 400;
      color: #40464d;
    }
  }
`;

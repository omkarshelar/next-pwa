import React, { Component } from "react";
import recommendation from "../Upload/recommendation.svg";
import Alternate from "../Upload/Alternative.svg";
import effective from "../Upload/effective.svg";
import ingredient from "../Upload/ingredient.svg";
import save from "../Upload/save.svg";
import testing from "../Upload/testing.svg";
import { Alternative, FeatureList, Feature } from "./Recommendation.style";

export class Recommend extends Component {
  render() {
    return (
      <>
        <Alternative>
          <h3> Why pick Truemeds recommended alternatives </h3>
          <FeatureList>
            <Feature>
              <div className="feature-img">
                <img src={recommendation} alt="feature"></img>
              </div>
              <span>Recommended by Truemeds doctors</span>
            </Feature>
            <Feature>
              <div className="feature-img">
                <img src={testing} alt="feature"></img>
              </div>
              <span>Same testing standards as branded medicines</span>
            </Feature>
            <Feature>
              <div className="feature-img">
                <img src={save} alt="feature"></img>
              </div>
              <span>Help you save up to 72% on every order</span>
            </Feature>
            <Feature>
              <div className="feature-img">
                <img src={ingredient} alt="feature"></img>
              </div>
              <span>
                Contain the same active ingredients as branded medicines
              </span>
            </Feature>
            <Feature>
              <div className="feature-img">
                <img src={effective} alt="feature"></img>
              </div>
              <span>Are as effective as branded medicines</span>
            </Feature>

            <Feature>
              <div className="feature-img">
                <img src={Alternate} alt="feature"></img>
              </div>
              <span>
                Alternatives sold are produced by only the top 30 medicine
                makers{" "}
              </span>
            </Feature>
          </FeatureList>
        </Alternative>
      </>
    );
  }
}

export default Recommend;

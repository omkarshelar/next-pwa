import React, { Component } from "react";
import Button from "@material-ui/core/Button";
import Dialog from "@material-ui/core/Dialog";
import DialogActions from "@material-ui/core/DialogActions";
import DialogContent from "@material-ui/core/DialogContent";
import DialogContentText from "@material-ui/core/DialogContentText";
import DialogTitle from "@material-ui/core/DialogTitle";
import "./Modal.css";

export class Modal extends Component {
  render() {
    return (
      <div className="newModalMainContainer">
        <Dialog
          open={this.props.open}
          onClose={this.props.handleClose}
          aria-labelledby="responsive-dialog-title"
          fullWidth={true}
          disableBackdropClick={this.props.disableBackdropClick ? true : false}
        >
          <DialogTitle id="responsive-dialog-title">
            {this.props.Header}
          </DialogTitle>
          {this.props.Subheader ? (
            <DialogContent>
              <DialogContentText>{this.props.Subheader}</DialogContentText>
            </DialogContent>
          ) : (
            <DialogContent>{this.props.children}</DialogContent>
          )}
          <DialogActions>
            {this.props.noMethod && (
              <Button
                onClick={this.props.noMethod}
                color="primary"
                style={{
                  borderRight: "1px solid #c0e2f7",
                  borderBottomRightRadius: "0",
                  borderTop: "1px solid #c0e2f7",
                  borderTopLeftRadius: "0",
                  borderTopRightRadius: "0",
                }}
              >
                {this.props.noText}
              </Button>
            )}
            <Button
              onClick={this.props.yesMethod}
              color="primary"
              style={{
                marginLeft: "0",
                borderTop: "1px solid #c0e2f7",
                borderTopLeftRadius: "0",
                borderTopRightRadius: "0",
                width: this.props.noText ? "" : "100%",
              }}
            >
              {this.props.yesText}
            </Button>
          </DialogActions>
        </Dialog>
      </div>
    );
  }
}

export default Modal;

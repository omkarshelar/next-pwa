import React, { Component } from "react";
import { connect } from "react-redux";
import { withRouter } from "next/router";
import {
  toUploadPrescriptionAction,
  toCartSectionAction,
} from "../../../redux/Timeline/Action";
import "./OrderSummary.css";
import Modal from "../NewModal/Modal";
import Accordion from "@material-ui/core/Accordion";
import AccordionSummary from "@material-ui/core/AccordionSummary";
import MedCardLoader from "../Shimmer/MedCardLoader";
import Shimmer from "react-shimmer-effect";
import { fetchProductDetailsThunk } from "../../../redux/ProductDetails/Action";
import { isUploadAction } from "../../../redux/UploadImage/Action";
import MedicineCardAccordian from "../MedicineCardAccordian/MedicineCardAccordian";
import cartBag from "../../../src/Assets/bag.svg";
import AddMedicine from "../../../pageComponents/cart/AddMedicine";
import AddressPatientSection from "./SubComponents/AddressPatientSection";
import PaymentModeSection from "./SubComponents/PaymentModeSection";
import DoctorCard from "./SubComponents/DoctorCards";
import ConfirmModal from "../ConfirmModal/ConfirmModal";
import PrescriptionContent from "./SubComponents/PrescriptionContent";
import subsDropdown from "../../../src/Assets/paymentMethodChevron.svg";
import AndroidSeperator from "../AndroidSeperator/AndroidSeperator";
import { fetchMedicineWithImagesThunk } from "../../../redux/SearchMedicine/Action";
import { updateOrderDetails } from "../../../redux/OrderDetail/Action";
import window from "global";

// import { hjPageChange } from "../../Events/Events";
export class OrderSummary extends Component {
  state = {
    medData: [],
    uploadPres: false,
    deleteImageShow: false,
    openMed: false,
    openPres: false,
    openRec: false,
    rMedData: [],
    imageId: null,
    deleteMedShow: false,
    selectedInfo: "",
    validateMedModal: false,
    noPatientAddress: false,
    unservicableMed: false,
    updatedOrderDetails: this.props.updateDetails,
  };

  componentDidMount() {
    window.scrollTo(0, 0);
    window.addEventListener("popstate", this.onBackButtonEvent);
    this.props.getAllOrderDetails();
    this.props.getPatientDetails();
  }

  // Clear State after unmounting
  componentWillUnmount = () => {
    window.removeEventListener("popstate", this.onBackButtonEvent);
  };

  // On Back Button click Event.
  onBackButtonEvent = (e) => {
    e.preventDefault();
    if (!this.isBackButtonClicked) {
      if (this.props.imgUpload?.uploadHistory?.length > 0) {
        this.props.toUploadPrescriptionAction();
      } else {
        this.props.toCartSectionAction();
      }
      this.isBackButtonClicked = false;
    }
  };

  componentDidUpdate(prevProp) {
    if (prevProp.isProceed !== this.props.isProceed && this.props.isProceed) {
      if (
        !this.props.patientData?.patientName ||
        !this.props.updateDetails?.AddressDetails?.addressId
      ) {
        window.scrollTo(0, 0);
        this.setState({ noPatientAddress: true });
        setTimeout(() => {
          this.setState({
            noPatientAddress: false,
          });
        }, 5000);
      } else {
        this.setState({ noPatientAddress: false }, () => {
          if (
            this.props.isServiceable?.isServiceable &&
            this.props.updateDetails?.AddressDetails?.pincode ===
              this.props.isServiceable?.pincode
          )
            this.props.orderConfirm();
        });
      }
    }

    if (
      this.props.updateDetails?.productSubsMappingList?.length > 0 &&
      prevProp.updateDetails?.productSubsMappingList !==
        this.props.updateDetails?.productSubsMappingList
    ) {
      this.validateMedicines(this.props.updateDetails.productSubsMappingList);
      this.addImagesForMeds(this.props.updateDetails.productSubsMappingList);
    }

    //page change for hotjar
    // hjPageChange(window.location.origin + "/orderflow#order_summary");
  }

  validateMedicines = (medList) => {
    if (this.checkMeds(medList)) {
      this.setState({ validateMedModal: true });
    }
  };

  checkMeds = (medList) => {
    let openModal = true;
    for (let i in medList) {
      let med = medList[i];
      if (!med.coldChainDisabled && !med.disabled) {
        openModal = false;
      }
      if (med.coldChainDisabled) {
        this.setState({ unservicableMed: true });
      }
    }
    return openModal;
  };

  removeAllMeds = (medList) => {
    for (let i in medList) {
      let med = medList[i];
      let data = [
        {
          medicineName: med.orgMedName,
          medicineQty: "0",
          medicineId: med.orgProductCd,
        },
      ];
      this.setState(
        {
          rMedData: data,
        },
        () => {
          this.props.removeMedOnClickFinal(this.state.rMedData);
          this.props.removeItemCart(
            this.props.removeMedContent(med.orgProductCd)
          );
        }
      );
    }
  };

  handleChange = (e) => {
    let { value, name } = e.target;
    this.setState({ [name]: value });
  };

  orderTrack = () => {
    if (this.props.trackOrder?.clickPostTrackingUrl) {
      // window.open(this.props.trackOrder.clickPostTrackingUrl);
      window.location.href = this.props.trackOrder.clickPostTrackingUrl;
    }
  };

  countAvailMed = (meds) => {
    let medCount = 0;
    if (meds) {
      for (const item of meds.productSubsMappingList) {
        if (!item.disabled) {
          medCount++;
        }
      }
    }
    return medCount;
  };

  toggleAccordian = () => {
    this.setState({
      openRec: !this.state.openRec,
    });
  };

  calcDiscPreProcessing = (billArr) => {
    let filterMeds = billArr.filter(
      (data) => !data.disabled && !data.coldChainDisabled
    );

    let finalPrice = filterMeds.reduce(
      (prev, next) =>
        prev +
        (next.orgMrp -
          next.orgMrp *
            ((100 -
              (next.orgProductCd === next.subsProductCd
                ? Math.round(
                    ((next.subsMrp - next.subsSellingPrice) * 100) /
                      next.subsMrp
                  )
                : next.orgDiscount)) /
              100)),

      0
    );
    return finalPrice.toFixed(2);
  };

  calcTotalPreProcessing = (billArr, mrp, charge) => {
    let finalDisc = this.calcDiscPreProcessing(billArr);
    let calcTotal = mrp - finalDisc + charge;
    return calcTotal.toFixed(2);
  };

  formatEDD = (val) => {
    const monthNames = [
      "Jan",
      "Feb",
      "Mar",
      "Apr",
      "May",
      "Jun",
      "Jul",
      "Aug",
      "Sep",
      "Oct",
      "Nov",
      "Dec",
    ];
    let presentDate = new Date();
    let eddTemp = new Date(val);
    let edd = new Date(val);
    var diff = Math.floor(edd.getTime() - presentDate.getTime());
    var day = 1000 * 60 * 60 * 24;
    var days = Math.floor(diff / day);
    if (days >= 2 && days <= 3) {
      edd.setDate(edd.getDate() - 1);
    } else if (days >= 4 && days <= 7) {
      edd.setDate(edd.getDate() - 2);
    } else if (days >= 8) {
      edd.setDate(edd.getDate() - 3);
    }
    let monthTemp = monthNames[eddTemp.getMonth()];
    let month = monthNames[edd.getMonth()];
    let finalString = `${edd.getDate()} ${month} - ${eddTemp.getDate()} ${monthTemp}`;
    return finalString;
  };

  dateToDays = (date) => {
    let date1 = new Date();
    let date2 = new Date(date);
    let diff = date2.getTime() - date1.getTime();
    let days = Number((diff / (1000 * 3600 * 24)).toFixed(0));
    let day1 = days - 1;
    let day2 = days + 1;
    if (
      !date ||
      !day1 ||
      !day2 ||
      day1 > 10 ||
      day1 < 0 ||
      day2 > 10 ||
      day2 < 0
    ) {
      return "";
    } else {
      return `Your product will be delivered within ${day1} - ${day2} days`;
    }
  };

  getAllProductCodes = (arr) => {
    let productCodes = [];
    for (let i in arr) {
      let med = arr[i];
      productCodes.push(
        this.props.updateDetails?.orderStatus?.serialId === 39 ||
          this.props.updateDetails?.orderStatus?.serialId === 49 ||
          this.props.updateDetails?.orderStatus?.serialId === 2
          ? med.orgProductCd
          : med.subsProductCd
      );
    }
    return productCodes;
  };

  addImagesForMeds = (medArr) => {
    let productCodes = this.getAllProductCodes(medArr);
    let data = {
      access_token: this.props.accessToken?.Response?.access_token,
      medicine: {
        query: {
          bool: {
            should: [
              {
                terms: {
                  "original_product_code.keyword": productCodes,
                },
              },
            ],
          },
        },
      },
      warehouseId: 3,
    };
    this.props.fetchMedicineWithImagesThunk(data).then(() => {
      if (
        this.props.medicineList?.length > 0 &&
        this.props.updateDetails?.productSubsMappingList?.length > 0
      ) {
        let medArr = this.props.updateDetails.productSubsMappingList;
        let medNewArr = this.props.medicineList;
        for (let i in medArr) {
          let med = medArr[i];
          for (let j in medNewArr) {
            let medNew = medNewArr[j];
            med.subsFound = medNew._source.subs_found;
            if (
              medNew._source.original_product_code ===
              (this.props.updateDetails?.orderStatus?.serialId === 39 ||
              this.props.updateDetails?.orderStatus?.serialId === 49 ||
              this.props.updateDetails?.orderStatus?.serialId === 2
                ? med.orgProductCd
                : med.subsProductCd)
            ) {
              med.productImages = medNew._source.product_image_urls;
              break;
            }
          }
        }
        let updatedOrderDetails = this.props.updateDetails;
        updatedOrderDetails.productSubsMappingList = medArr;
        this.props.updateOrderDetails(updatedOrderDetails);
        this.setState({ updatedOrderDetails: updatedOrderDetails });
      }
    });
  };

  render() {
    let { updateDetails } = this.props;
    // let updateDetails = this.state.updatedOrderDetails;
    let itemCount = updateDetails?.productSubsMappingList.filter(
      (e) => e.medActive && !e.disabled
    ).length;

    return (
      <div>
        <ConfirmModal
          open={this.state.validateMedModal}
          Header={
            this.state.unservicableMed
              ? "This item is unserviceable, we are removing this item from the cart"
              : "This item is out of stock, we are removing this item from the cart "
          }
          noMethod={() => {
            this.removeAllMeds(this.props.updateDetails.productSubsMappingList);
            this.setState({ validateMedModal: false });
          }}
          noText="Ok, got it"
        />
        <Modal
          open={this.state.deleteMedShow}
          Header="Are you sure, want to remove this medicine ?"
          handleClose={() =>
            this.setState({
              deleteMedShow: !this.state.deleteMedShow,
            })
          }
          noText="No"
          yesText="Yes"
          noMethod={() =>
            this.setState({
              deleteMedShow: !this.state.deleteMedShow,
            })
          }
          yesMethod={() => {
            this.setState({
              deleteMedShow: !this.state.deleteMedShow,
            });
            this.props.removeMedOnClickFinal(this.state.rMedData);
            this.props.removeItemCart(
              this.props.removeMedContent(this.state.rMedData[0].medicineId)
            );
          }}
        />

        {this.props.isLoading ? (
          <>
            <Shimmer>
              <div className="orderSummarymedicineAddedLoader" />
            </Shimmer>
          </>
        ) : (
          <AddressPatientSection
            isServiceable={this.props.isServiceable}
            patientData={this.props.patientData}
            updateDetails={updateDetails}
            noPatientAddress={this.state.noPatientAddress}
            viewDetails={false}
            patientsList={this.props.patientsList}
          />
        )}
        {this.props.isLoading ? (
          <>
            <Shimmer>
              <div className="orderSummarymedicineAddedLoader" />
            </Shimmer>
          </>
        ) : (
          <PaymentModeSection
            onClickSelectPaymentType={this.props.onClickSelectPaymentType}
            payment={this.props.payment}
            viewDetails={false}
            appliedCoupon={this.props.appliedCoupon}
            updateDetails={this.props.updateDetails}
          />
        )}
        {this.props.isLoading ? (
          <>
            <Shimmer>
              <div className="orderSummarymedicineAddedLoader" />
            </Shimmer>
            <MedCardLoader />
          </>
        ) : (
          updateDetails?.productSubsMappingList && (
            <>
              {updateDetails.productSubsMappingList.length > 0 && (
                <div className="orderSummaryMedcinesAddedContainer">
                  <p>
                    <img src={cartBag} style={{ "margin-top": "-5px" }} />
                    {itemCount === 0
                      ? "No items in cart"
                      : itemCount === 1
                      ? "1 Item in cart"
                      : itemCount + " Items in cart"}
                  </p>
                </div>
              )}
              <div className="medicineDetailsWrap">
                {updateDetails?.productSubsMappingList.length > 0 &&
                  updateDetails.productSubsMappingList.map((content, index) => (
                    <MedicineCardAccordian
                      imgUpload={this.props.imgUpload}
                      summary={true}
                      content={content}
                      {...this.props}
                      getAddress={() => {
                        this.props
                          .fetchProductDetailsThunk({
                            productCd: content?._source?.original_product_code,
                            history: this.props.router,
                          })
                          .then(() => {
                            this.setState({
                              selectedInfo:
                                content?._source?.original_product_code,
                            });
                          });
                      }}
                      selectedInfo={this.state.selectedInfo}
                      closeToolTip={() => this.setState({ selectedInfo: "" })}
                      quantityOnHandleChange={(e, content) => {
                        this.props.quantityOnHandleChange(e, content);
                      }}
                      onClickRemove={() => {
                        let data = [
                          {
                            medicineName: content.orgMedName,
                            medicineQty: "0",
                            medicineId: content.orgProductCd,
                          },
                        ];
                        this.setState(
                          {
                            rMedData: data,
                          },
                          () => {
                            this.props.removeMedOnClickFinal(
                              this.state.rMedData
                            );
                            this.props.removeItemCart(
                              this.props.removeMedContent(content.orgProductCd)
                            );
                          }
                        );
                      }}
                      index={index}
                      isOrderSummary={true}
                    />
                  ))}
              </div>
            </>
          )
        )}

        {this.props.isLoading ? (
          <div className="orderSummarymedicineAddedLoader" />
        ) : (
          <>
            <AddMedicine
              redirectToHandler={() => {
                window.innerWidth < 468
                  ? this.props.router.push("/?search=true")
                  : this.props.router.push("/orderflow?search=true");
              }}
              cartContents={this.props.cartContents}
            />
            {/* <AndroidSeperator showMob={true} showWeb={false} /> */}
          </>
        )}

        {this.props.isLoading ? (
          <Shimmer>
            <div className="orderSummarymedicineAddedLoader" />
            <div className="orderSummaryPrescriptionCardLoader" />
          </Shimmer>
        ) : (
          this.props.updateDetails?.ImageMasterDto &&
          (window.innerWidth >= 468 ? (
            <>
              <div
                className="orderSummaryMedcinesAddedContainer"
                style={{ borderBottom: "none", marginBottom: "0" }}
              >
                <p>Prescription Uploaded</p>
              </div>
              <PrescriptionContent
                isUpload={this.props.isUpload}
                deleteImageShow={this.state.deleteImageShow}
                onClickUploadPrescription={() => {
                  this.props.isUploadAction(true);
                  this.props.toUploadPrescriptionAction();
                }}
                {...this.props}
                removeImage={(id) => {
                  this.setState((prevState) => ({
                    deleteImageShow: !prevState.deleteImageShow,
                    imageId: id,
                  }));
                }}
                imageId={this.state.imageId}
                handleClose={() => {
                  this.setState({
                    deleteImageShow: !this.state.deleteImageShow,
                  });
                }}
                noMethod={() => {
                  this.setState({
                    deleteImageShow: !this.state.deleteImageShow,
                  });
                }}
                yesMethod={(data) => {
                  this.setState({
                    deleteImageShow: !this.state.deleteImageShow,
                  });
                  this.props.onClickDeleteUploadImage(data);
                }}
                viewDetails={false}
              />
            </>
          ) : (
            <>
              <div className="mobPrescriptionWrap">
                <Accordion
                  square
                  defaultExpanded={
                    this.props.updateDetails?.ImageMasterDto?.length > 0 ||
                    this.props.imgUpload?.uploadHistory?.length > 0
                      ? true
                      : false
                  }
                  style={{ margin: "0px" }}
                >
                  <AccordionSummary
                    expandIcon={
                      <img
                        src={subsDropdown}
                        style={{
                          width: "20px",
                          transform: "rotate( 180deg)",
                        }}
                        alt=""
                      />
                    }
                    aria-controls="panel1a-content"
                    id="panel1a-header"
                  >
                    <div
                      className="orderSummaryMedcinesAddedContainer"
                      style={{ borderBottom: "none", marginBottom: "0" }}
                    >
                      <p>
                        {this.props.updateDetails.ImageMasterDto &&
                        this.props.updateDetails.ImageMasterDto.length > 0
                          ? "Prescription Uploaded"
                          : "Upload Prescription"}
                      </p>
                    </div>
                  </AccordionSummary>
                  <PrescriptionContent
                    isUpload={this.props.isUpload}
                    deleteImageShow={this.state.deleteImageShow}
                    onClickUploadPrescription={() => {
                      this.props.isUploadAction(true);
                      this.props.toUploadPrescriptionAction();
                    }}
                    {...this.props}
                    removeImage={(id) => {
                      this.setState((prevState) => ({
                        deleteImageShow: !prevState.deleteImageShow,
                        imageId: id,
                      }));
                    }}
                    imageId={this.state.imageId}
                    handleClose={() => {
                      this.setState({
                        deleteImageShow: !this.state.deleteImageShow,
                      });
                    }}
                    noMethod={() => {
                      this.setState({
                        deleteImageShow: !this.state.deleteImageShow,
                      });
                    }}
                    yesMethod={(data) => {
                      this.setState({
                        deleteImageShow: !this.state.deleteImageShow,
                      });
                      this.props.onClickDeleteUploadImage(data);
                    }}
                    viewDetails={false}
                  />
                </Accordion>
              </div>
              <AndroidSeperator showMob={true} />
            </>
          ))
        )}
        {this.props.isLoading ? (
          <Shimmer>
            <div className="orderSummarymedicineAddedLoader" />
            <div className="orderSummaryPrescriptionCardLoader" />
          </Shimmer>
        ) : (
          window.innerWidth > 468 && <DoctorCard />
        )}
      </div>
    );
  }
}

const mapStateToProps = (state) => ({
  productDetails: state.productDetails,
  isServiceable: state.addressData.pincodeSuccess,
  appliedCoupon: state.cartData?.appliedCoupon,
  patientsList: state.patientData.fetchPatientSuccess,
  accessToken: state.loginReducer?.verifyOtpSuccess,
  medicineList: state.searchMedicine?.medDataWithImgSuccess?.hits,
  isUpload: state.uploadImg?.isUpload,
});

export default withRouter(
  connect(mapStateToProps, {
    toUploadPrescriptionAction,
    toCartSectionAction,
    fetchProductDetailsThunk,
    fetchMedicineWithImagesThunk,
    isUploadAction,
    updateOrderDetails,
  })(OrderSummary)
);

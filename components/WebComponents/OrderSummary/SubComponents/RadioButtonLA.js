import React from "react";
import "./RadioButtonLA.css";

const RadioButtonLA = (props) => {
  return (
    <div
      className={`${"rdb_radiobutton__wrap"} ${
        props.selected && "rdb_selection"
      }`}
      onClick={() => props.clickHandler()}
    >
      <div className={`${"rdb_radioSelect"}`}>
        <div className={`${"rdb_radioInnerselect"} `}></div>
      </div>
      <div className={`${"rdb_labelText"}`}>{props.label}</div>
    </div>
  );
};

export default RadioButtonLA;

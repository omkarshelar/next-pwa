import React from "react";
import moment from "moment";
import "./EstimatedDeliveryDate.css";
import getDeliveryDate from "../../../Helper/EstimatedDateFormat";
import window from "global";

const EstimatedDeliveryDate = (props) => {
  const showDeliveryDate = () => {
    return `${moment(props.deliveryDate).format("D MMMM")}`;
  };
  const deliveredOnArray = [55, 201, 199, 124];

  return (
    <div
      style={{
        bottom: window.innerWidth <= 480 && props.details ? "5px" : "",
      }}
      className={`${
        props.viewDetails
          ? "estimatedDeliveryWrapViewDetails"
          : "estimatedDeliveryWrap"
      } `}
    >
      {props.orderStatus && deliveredOnArray.includes(props.orderStatus) ? (
        <>
          {" "}
          <div className={"estimatedDeliveryText"}>Delivered on</div>
          <div className={"estimateDeliveryValue"}>{showDeliveryDate()}</div>
        </>
      ) : (
        <>
          {" "}
          <div className={"estimatedDeliveryText"}>Estimated Delivery by</div>
          <div className={"estimateDeliveryValue"}>
            {getDeliveryDate(props.deliveryDate)}
          </div>
        </>
      )}
    </div>
  );
};

export default EstimatedDeliveryDate;

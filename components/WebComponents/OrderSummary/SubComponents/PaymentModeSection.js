import React, { Fragment, useEffect } from "react";
import "./PaymentModeSection.css";
import wallet from "../../../../src/Assets/wallet.svg";
import paymentCreditCard from "../../../../src/Assets/paymentCreditCard.svg";
import RadioButtonLA from "./RadioButtonLA";
import Accordion from "@material-ui/core/Accordion";
import AccordionSummary from "@material-ui/core/AccordionSummary";
import subsDropdown from "../../../../src/Assets/paymentMethodChevron.svg";
import window from "global";

const PayMentModeContent = (props) => {
  return (
    <Fragment>
      <div className={"payment_paymentMode__details"}>
        <div className={"payment_selection__wrap"}>
          <RadioButtonLA
            label={"Online Payment"}
            selected={props.payment.isOnline}
            clickHandler={() => {
              props.onClickSelectPaymentType(true);
            }}
          />
          <RadioButtonLA
            label={"Cash on Delivery"}
            selected={!props.payment.isOnline}
            clickHandler={() => {
              props.onClickSelectPaymentType(false);
            }}
          />
        </div>
        <div className={"payment_details__Note"}>
          Payment to be made only once the order has been processed at our end
        </div>
      </div>
      <div className={"payment_paymentMent__footer"}>
        <span>
          <img src={wallet} />
        </span>
        <span className={"payment_footer__Note"}>
          TM Reward & Credit will be auto applied once the order has been
          processed
        </span>
      </div>
    </Fragment>
  );
};

const PaymentModeSection = (props) => {
  const [value, setValue] = React.useState(true);

  useEffect(() => {
    if (props.viewDetails) {
      setValue(false);
    }
  }, []);
  if (window.innerWidth <= 468) {
    return (
      <Fragment>
        <div className={"payment_paymentMode_wrap"}>
          <Accordion
            square
            defaultExpanded={props.viewDetails ? false : true}
            style={{ margin: "0px" }}
            onChange={(e, expanded) => {
              if (!props.viewDetails) {
                setValue(expanded);
              }
            }}
          >
            <AccordionSummary
              expandIcon={
                props.viewDetails ? (
                  ""
                ) : (
                  <img
                    src={subsDropdown}
                    style={{
                      width: "20px",
                      transform: "rotate( 180deg)",
                    }}
                  />
                )
              }
              aria-controls="panel1a-content"
              id="panel1a-header"
            >
              <div className={"payment_paymentMode__Header"}>
                <div className={"payment_paymentMode__Header_inner"}>
                  <span>
                    <img src={paymentCreditCard} />
                  </span>
                  <span className={"payment_header__title"}>Payment Mode</span>
                </div>
                <div className={"payment_paymentMode_paymentType"}>
                  <span>
                    {!value && props.viewDetails
                      ? props.updateDetails?.paymentMode &&
                        props.updateDetails?.paymentMode.value + " Payment"
                      : !value
                      ? props.payment.isOnline
                        ? "Online Payment"
                        : "COD Payment"
                      : ""}
                  </span>
                </div>
              </div>
            </AccordionSummary>
            {!props.viewDetails && (
              <PayMentModeContent
                {...props}
                onClickSelectPaymentType={props.onClickSelectPaymentType}
              />
            )}
          </Accordion>
        </div>
        {props.updateDetails?.productSubsMappingList?.length > 0 && (
          <div className={"payment_seperator"}></div>
        )}
      </Fragment>
    );
  } else {
    return (
      <div className={"payment_paymentMode_wrap"}>
        <div className={"payment_paymentMode__Header"}>
          <span>
            <img src={paymentCreditCard} />
          </span>
          <span className={"payment_header__title"}>Payment Mode</span>
          {props.viewDetails && (
            <span className={"payment_header__Subtitle"}>
              {props.updateDetails?.paymentMode &&
                props.updateDetails?.paymentMode.value + " Payment"}
            </span>
          )}
        </div>
        {!props.viewDetails && (
          <PayMentModeContent
            {...props}
            onClickSelectPaymentType={props.onClickSelectPaymentType}
          />
        )}
      </div>
    );
  }
};

export default PaymentModeSection;

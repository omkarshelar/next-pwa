import React from "react";
import ScrollContainer from "react-indiana-drag-scroll";
import addPrescription from "../../../../src/Assets/addPrescription.svg";
import removeImgIcon from "../../../../src/Assets/remove-rx.svg";
import ConfirmModal from "../../ConfirmModal/ConfirmModal";
import { Image_URL } from "../../../../constants/Urls";

const PrescriptionContent = (props) => {
  if (
    props.updateDetails.ImageMasterDto &&
    props.updateDetails.ImageMasterDto.length > 0 &&
    (props.isUpload || props.orderFlowHistory?.location?.state?.detail)
  ) {
    return (
      <ScrollContainer
        className="orderSummaryImagesContainer"
        horizontal={true}
        hideScrollbars={true}
      >
        {!props.viewDetails && (
          <div
            onClick={props.onClickUploadPrescription}
            className="addMorePrescriptionWrap"
          >
            <div className="addMorePrescriptionCard">
              <img src={addPrescription} />
            </div>
            <div className="addMorePrescriptionText">Add More Prescription</div>
          </div>
        )}

        {props.updateDetails.ImageMasterDto.map((data, index) => (
          <div style={{ position: "relative" }} key={index}>
            {!props.orderFlowHistory?.location?.state?.detail && (
              <img
                src={removeImgIcon}
                alt="remove"
                className="orderSummaryPrescriptionRemoveImage"
                onClick={() => props.removeImage(data.imageId)}
              />
            )}
            {props.imageId === data.imageId && (
              <ConfirmModal
                open={props.deleteImageShow}
                Header={
                  props.updateDetails.ImageMasterDto.length === 1 &&
                  props.cartContents.length === 0
                    ? "Removing this prescription will take you back to home page"
                    : "Are you sure, want to remove this prescription ?"
                }
                handleClose={() => props.handlleClose()}
                noText={
                  props.updateDetails.ImageMasterDto.length === 1 &&
                  props.cartContents.length === 0
                    ? "No, don’t remove"
                    : "No"
                }
                yesText={
                  props.updateDetails.ImageMasterDto.length === 1 &&
                  props.cartContents.length === 0
                    ? "Remove Item"
                    : "Yes"
                }
                noMethod={() => props.noMethod()}
                yesMethod={() => {
                  props.yesMethod(data);
                }}
                type={
                  props.updateDetails.ImageMasterDto.length === 1 &&
                  props.cartContents.length === 0
                    ? 1
                    : 0
                }
                // isPrescription={true}
              />
            )}
            <img
              src={`${Image_URL}${data.imagePath}`}
              alt="Prescription"
              className="uploadPrescriptionDisplayImage"
            />
          </div>
        ))}
      </ScrollContainer>
    );
  } else {
    if (!props.viewDetails) {
      return (
        <ScrollContainer
          className="orderSummaryImagesContainer"
          horizontal={true}
          hideScrollbars={true}
        >
          <div
            onClick={props.onClickUploadPrescription}
            className="addMorePrescriptionWrap"
          >
            <div className="addMorePrescriptionCard">
              <img src={addPrescription} />
            </div>
            <div className="addMorePrescriptionText">Add Prescription</div>
          </div>
        </ScrollContainer>
      );
    } else {
      return <div></div>;
    }
  }
};

export default PrescriptionContent;

import React, { useEffect, useState } from "react";
import { ControlButtonWrap } from "./ControlButton.style";
import { HiPlus, HiMinus } from "react-icons/hi";
import { customNotification } from "../../Helper/helperFunction";

const ControlButton = (props) => {
  const [value, setValue] = useState(props.value ? props.value : 0);
  const [step, setStep] = useState(props.step ? props.step : 1);
  const [minValue, setMinValue] = useState(props.minValue ? props.minValue : 0);
  const [maxValue, setMaxValue] = useState(
    props.maxValue ? props.maxValue : 20
  );

  const addHandler = () => {
    setValue((prevValue) => {
      if (prevValue + step >= maxValue) {
        customNotification(`You can only add max ${maxValue} items`);
        return maxValue;
      } else {
        return prevValue + step;
      }
    });
  };

  useEffect(() => {
    props.changeHandler(value);
  }, [value]);

  const minusHandler = () => {
    setValue((prevValue) => {
      if (prevValue - step <= minValue) {
        return minValue;
      } else {
        return prevValue - step;
      }
    });
  };

  useEffect(() => {
    setValue(props.value);
  }, [props]);

  return (
    <ControlButtonWrap>
      <button className="leftBtn" onClick={minusHandler}>
        <HiMinus />
      </button>
      <input type="number" value={value} />
      <button className="rightBtn" onClick={addHandler}>
        <HiPlus />
      </button>
    </ControlButtonWrap>
  );
};

export default ControlButton;

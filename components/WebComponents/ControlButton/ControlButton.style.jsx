import styled from "styled-components";

export const ControlButtonWrap = styled.div`
  display: grid;
  grid-template-columns: 1fr 1.3fr 1fr;
  width: 100%;
  height: 34px;
  border-radius: 6px;
  color: #fff;

  .leftBtn {
    border-top-left-radius: 4px;
    border-bottom-left-radius: 4px;
  }

  .rightBtn {
    border-top-right-radius: 4px;
    border-bottom-right-radius: 4px;
  }

  button {
    background: #0071bd;
    font-size: 19px;
    border: none;
    padding: 0px 5px;
    width: 100%;
  }

  input[type="number"] {
    border: none;
    color: #4f585e;
    font-size: 14px;
    font-weight: 700;
    text-align: center;
    padding: 0px;
    width: 100%;
    background: #fff;
    border: 1px solid #0071bc;
    border-radius: 0px;
  }
`;

import styled from "styled-components";

export const BlankContainer = styled.div`
  min-width: 220px !important;
  height: auto;
  @media screen and (max-width: 968px) {
    display: none;
  }
`;

// <OrderCard
// uniquieId = {id for styling the card}
// orderId = {order id}
// orderPlaced = {order placed date}
// total = {order total amount}
// medArr = {medicine array}
// />

import React, { Component } from "react";
import { connect } from "react-redux";
import Router, { withRouter } from "next/router";
import Axios from "axios";
import medIcon from "../../../src/Assets/med.svg";
import addIcon from "../../../src/Assets/add-new.svg";
import doctorCallingPendingIcon from "../../../src/Assets/Doctor_Pending_call_copy.svg";
import proccessingOrderIcon from "../../../src/Assets/Processing-order.svg";
import shipOrderIcon from "../../../src/Assets/Ready-to-ship-order.svg";
import orderShippedIcon from "../../../src/Assets/Order-shipped.svg";
import orderCancelledIcon from "../../../src/Assets/Order-cancelled.svg";
import orderDeliveredIcon from "../../../src/Assets/Order-delivered.svg";
import pharmacistPendingIcon from "../../../src/Assets/Pharmacist_Pending_call_copy.svg";
import orderMissingIcon from "../../../src/Assets/Order-missing-details.svg";
import refundPendingIcon from "../../../src/Assets/Refund-pending.svg";
import returnGeneratedIcon from "../../../src/Assets/Return-generated.svg";
import paymentPendingIcon from "../../../src/Assets/Payment-pending.svg";
import orderOnHoldIcon from "../../../src/Assets/Order-on-hold.svg";
import returnInTransitIcon from "../../../src/Assets/Return-In-transit.svg";
import returnReceivedIcon from "../../../src/Assets/Return-received.svg";
import discardedOrderIcon from "../../../src/Assets/Discarded-order.svg";
import returnRequestPendingIcon from "../../../src/Assets/Return-request-pending-approval.svg";
import returnRequestDeclinedIcon from "../../../src/Assets/Return-request-declined.svg";
import incompleteOrderIcon from "../../../src/Assets/incomplete-processed.svg";
import "./OrderCard.css";
import { PWA_static_URL, ThirdPartySevice_URL } from "../../../constants/Urls";
import { reorderThunk } from "../../../redux/Reorder/Action";
import { AddIncompleteOrderMedsAction } from "../../../redux/Cart/Action";
import { toCartSectionAction } from "../../../redux/Timeline/Action";
import { orderStatusThunk } from "../../../redux/OrderDetail/Action";
// import NotProcessModal from "../../Webpage/Help/HelpSubComponent/TicketGenerate/NotProcessModal/NotProcessModal";
import window from "global";
import { message } from "antd";
import {
  eventHomepageReorder,
  eventHomepageViewDetails,
} from "../../../src/Events/Events";
import { getOrderStatusDetailsDataAction } from "../../../redux/storeData/Action";

export class OrderCard extends Component {
  state = {
    shipmentModal: false,
    shipmentMsg: "",
  };

  convertEpoch = (epoch) => {
    var date = new Date(epoch);
    return date.toUTCString().slice(4, 16);
  };

  goToOrderStatus = (orderId) => {
    eventHomepageViewDetails();
    Router.push(`/orderstatus/${orderId}`);
  };

  navigateToOrderSummary = (id, status, statusText) => {
    eventHomepageViewDetails();
    this.props
      .getOrderStatusDetailsDataAction({
        id: id,
        orderStatus: status,
        showEdd: statusText,
      })
      .then(() => {
        Router.push(`/myorderdetails/${id}`);
      });
    // Router.push(
    //   {
    //     pathname: "/myorderdetails",
    //     query: {
    //       id: id,
    //       orderStatus: status,
    //       showEdd: statusText,
    //     },
    //   },
    //   "/myorderdetails"
    // );
  };

  windowToReturnOrder = () => {
    let someDate = new Date(this.props.orderDetails?.deliveredDate);
    someDate.toUTCString();
    let presentDate = new Date();
    let numberOfDaysToAdd = 15;
    someDate.setDate(someDate.getDate() + numberOfDaysToAdd);
    if (presentDate > someDate) {
      return false;
    } else {
      return true;
    }
  };

  returnOrderNew = (orderId) => {
    Router.push({
      pathname: "/raiseissue",
      state: {
        orderId: orderId,
        formmyOrder: true,
      },
    });
  };

  returnOrder = async (orderId) => {
    const apiUrl = `https://internal.clickpost.in/api/v1/fetch_return_info/`;

    let orderData = {
      domain: "truemeds",
      reference_number: orderId,
      phone_number: this.props.token?.CustomerDto?.mobileNo,
    };

    Axios.post(apiUrl, orderData, {
      "Content-Type": "application/json",
      "Access-Control-Allow-Origin": "*",
    })
      .then((response) => {
        if (response.data.meta.status === 200) {
          // window.open(
          //   `https://truemeds.clickpost.in/returns?&getAwb=${response.data.result.awb}getShipment_uuid=${response.data.result.shipment_uuid}`
          // );
          window.location.href = `https://truemeds.clickpost.in/returns?&getAwb=${response.data.result.awb}getShipment_uuid=${response.data.result.shipment_uuid}`;
        }
        if (response.data.meta.status === 400) {
          this.setState({
            shipmentModal: true,
            shipmentMsg: response.data.meta.message,
          });
          return true;
        }
      })
      .catch((err) => {
        console.error(err.response);
      });
  };

  reorderHandler = (orderId) => {
    this.props
      .reorderThunk({
        customerId: this.props.custId,
        orderId: orderId,
        accessToken: this.props.token?.Response?.access_token,
        patientIdSet: [this.props.orderDetails?.patientId],
        history: Router,
      })
      .then(() => {
        if (this.props.reorder.reorderError) {
          message.success(this.props.reorder.reorderError);
        } else {
          eventHomepageReorder();
          this.props
            .orderStatusThunk({
              access_token: this.props.token?.Response?.access_token,
              orderIds: orderId,
              customerId: this.props.custId,
              history: Router,
            })
            .then(() => {
              if (this.props.orderStatus.error) {
                message.success(this.props.orderStatus.error);
              } else if (
                this.props.orderStatus?.OrderStatusData?.productSubsMappingList
                  ?.length > 0
              ) {
                this.props.AddIncompleteOrderMedsAction(
                  this.props.orderStatus?.OrderStatusData
                    ?.productSubsMappingList
                );
                this.props.toCartSectionAction();
                Router.push("/orderflow");
              }
            });
        }
      });
  };

  NavigateToRaisePage = (orderId) => {
    this.setState({ shipmentModal: false });
    Router.push({
      pathname: "/raiseissue",
      state: {
        orderId: orderId,
        formmyOrder: true,
      },
    });
  };

  navigateToOrderSummaryWOStatusText = (id, status) => {
    Router.push({
      pathname: "/orderflow",
      state: {
        detail: id,
        orderStatus: status,
      },
    });
  };

  cashFreeService = (orderId) => {
    const Uri = `${ThirdPartySevice_URL}/createPaymentUrlInCashFree?orderId=${orderId}&returnUrl=${PWA_static_URL}`;
    const config = {
      headers: {
        "Content-Type": "application/json",
        Authorization: "Bearer " + this.props.token?.Response?.access_token,
        "Access-Control-Allow-Origin": "*",
      },
    };
    Axios.get(Uri, config)
      .then((response) => {
        if (response.status === 200) {
          // window.open(response.data.paymentLink);
          window.location.href = response.data.paymentLink;
        }
      })
      .catch((err) => {
        console.error("createPaymentUrlInCashFree", err.response);
      });
  };

  getOrderStatus = (id, img) => {
    let order = "";
    let orderStatus = "";
    let orderStatusImg = "";
    if (id == 1 || id == 2) {
      orderStatus = "Pharmacist call pending";
      orderStatusImg = pharmacistPendingIcon;
    } else if (id == 3 || id == 4) {
      orderStatus = "Order missing details";
      orderStatusImg = orderMissingIcon;
    } else if (id == 39) {
      orderStatus = "Doctor call pending";
      orderStatusImg = doctorCallingPendingIcon;
    } else if (id == 49) {
      orderStatus = "Incomplete order";
      orderStatusImg = incompleteOrderIcon;
    } else if (id == 55) {
      orderStatus = "Order delivered";
      orderStatusImg = orderDeliveredIcon;
    } else if (id == 56 || id == 200) {
      orderStatus = "Return received. Refund pending";
      orderStatusImg = refundPendingIcon;
    } else if (id == 57) {
      orderStatus = "Order cancelled";
      orderStatusImg = orderCancelledIcon;
    } else if (id == 58) {
      orderStatus = "Payment pending";
      orderStatusImg = paymentPendingIcon;
    } else if (id == 59 || id == 66) {
      orderStatus = "Ready to ship order";
      orderStatusImg = shipOrderIcon;
    } else if (id == 60) {
      orderStatus = "Order shipped";
      orderStatusImg = orderShippedIcon;
    } else if (id == 81) {
      orderStatus = "Order on hold";
      orderStatusImg = orderOnHoldIcon;
    } else if (id == 121) {
      orderStatus = "Return In-transit";
      orderStatusImg = returnInTransitIcon;
    } else if (id == 124) {
      orderStatus = "Return received";
      orderStatusImg = returnReceivedIcon;
    } else if (id == 142) {
      orderStatus = "Processing order";
      orderStatusImg = proccessingOrderIcon;
    } else if (id == 174) {
      orderStatus = "Discarded order";
      orderStatusImg = discardedOrderIcon;
    } else if (id == 190) {
      orderStatus = "Return request pending approval ";
      orderStatusImg = returnRequestPendingIcon;
    } else if (id == 191) {
      orderStatus = "Return generated";
      orderStatusImg = returnGeneratedIcon;
    } else if (id == 192) {
      orderStatus = "Return request declined";
      orderStatusImg = returnRequestDeclinedIcon;
    } else if (id == 199) {
      orderStatus = "Refund processed";
      orderStatusImg = refundPendingIcon;
    } else if (id == 201) {
      orderStatus = "Partial refund processed";
      orderStatusImg = refundPendingIcon;
    }
    if (img) {
      order = orderStatusImg;
    } else {
      order = orderStatus;
    }
    return order;
  };

  render() {
    return (
      <div id={this.props.uniquieId} className="orderCardMainContainer">
        <div>
          <div className="orderCardHeaderContainer">
            <div className="orderCardHeaderInnerContainer">
              <span>Order Id</span>
              <span>{this.props.orderDetails?.orderId}</span>
            </div>
            <div className="orderCardSecondHeaderContainer">
              <div className="orderCardHeaderInnerContainer">
                <span>Order Placed</span>
                <span>
                  {this.convertEpoch(this.props.orderDetails?.orderDate)}
                </span>
              </div>
              {/* Added to hide amt mis-matching - OMKAR  */}
              {this.props.orderDetails.orderStatusId != 1 &&
                this.props.orderDetails.orderStatusId != 2 &&
                this.props.orderDetails.orderStatusId != 39 && (
                  <div className="orderCardHeaderInnerContainer">
                    <span>Total</span>
                    <span>
                      ₹
                      {parseFloat(this.props.orderDetails?.orderamount).toFixed(
                        2
                      )}
                    </span>
                  </div>
                )}
            </div>
          </div>
          <div className="orderCardDetailsContainer">
            <img
              src={this.getOrderStatus(
                this.props.orderDetails?.orderStatusId,
                true
              )}
              alt="status"
              className="orderCardDetailsContainerImg"
            />
            <div>
              <span>
                {this.getOrderStatus(
                  this.props.orderDetails?.orderStatusId,
                  false
                )}
              </span>
              <span>Estimated Arrival - 2 Feb 2021</span>
            </div>
          </div>
          <div style={{ paddingBottom: "20px" }}>
            <div className="orderCardMedContainer">
              {this.props.orderDetails?.myMedidicnes?.length > 0 && (
                <img className="orderCardMedImg" src={medIcon} alt="med" />
              )}
              {this.props.orderDetails?.myMedidicnes?.length > 4 ? (
                <div>
                  {this.props.orderDetails?.myMedidicnes?.map(
                    (med, index) =>
                      index < 3 && (
                        <span>
                          {index + 1}. {med.productName}
                        </span>
                      )
                  )}
                  <div className="orderCardBlueFontContainer">
                    <img src={addIcon} alt="add" />
                    <span
                      className="orderCardBlueFont"
                      style={{ cursor: "auto" }}
                    >
                      {this.props.orderDetails?.myMedidicnes?.length - 3} More
                    </span>
                  </div>
                </div>
              ) : (
                <div>
                  {this.props.orderDetails?.myMedidicnes?.map((med, index) => (
                    <span>
                      {index + 1}. {med.productName}
                    </span>
                  ))}
                </div>
              )}
            </div>
          </div>
        </div>
        <div className="orderCardButtonContainer">
          {/* {this.state.shipmentModal && (
            <NotProcessModal
              Msg={this.state.shipmentMsg}
              handleClose={() =>
                this.NavigateToRaisePage(this.props.orderDetails?.orderId)
              }
              handleshow={this.state.shipmentModal}
            />
          )} */}

          {/* Payment Pending */}
          {this.props.orderDetails?.orderStatusId === 58 && (
            <>
              <span
                className="orderCardBlueFont"
                // onClick={() =>
                //   this.goToOrderStatus(this.props.orderDetails?.orderId)
                // }
                onClick={() =>
                  this.goToOrderStatus(this.props.orderDetails?.orderId)
                }
              >
                View details
              </span>
              <span
                className="orderCardBlueFont"
                onClick={() =>
                  this.cashFreeService(this.props.orderDetails?.orderId)
                }
              >
                Pay Now
              </span>
            </>
          )}

          {/* Pre processing  */}
          {[1, 2, 39, 142, 66, 59].includes(
            this.props.orderDetails?.orderStatusId
          ) && (
            <span
              className="orderCardBlueFont"
              onClick={() =>
                this.goToOrderStatus(this.props.orderDetails?.orderId)
              }
            >
              View Details
            </span>
          )}

          {/* Before Delivered  */}
          {/*  {(this.props.orderDetails?.orderStatusId === 66 ||
            this.props.orderDetails?.orderStatusId === 59) && (
            <span
              className="orderCardBlueFont"
              onClick={() =>
                this.navigateToOrderSummaryWOStatusText(
                  this.props.orderDetails?.orderId,
                  this.props.orderDetails?.orderStatus
                )
              }
            >
              View Details
            </span>
          )} */}

          {this.props.orderDetails?.orderStatusId === 60 && (
            <>
              <span
                className="orderCardBlueFont"
                onClick={() =>
                  this.goToOrderStatus(this.props.orderDetails?.orderId)
                }
              >
                View Details
              </span>
              <span
                className="orderCardBlueFont"
                onClick={() =>
                  this.reorderHandler(this.props.orderDetails?.orderId)
                }
              >
                Reorder
              </span>
            </>
          )}

          {/* Delivered  */}
          {this.props.orderDetails?.orderStatusId === 55 && (
            <>
              {this.windowToReturnOrder() ? (
                <>
                  <span
                    className="orderCardBlueFont"
                    onClick={() =>
                      this.returnOrderNew(this.props.orderDetails?.orderId)
                    }
                  >
                    Return
                  </span>
                  <span
                    className="orderCardBlueFont"
                    onClick={() =>
                      this.navigateToOrderSummary(
                        this.props.orderDetails?.orderId,
                        this.props.orderDetails?.orderStatus,
                        "Delivered"
                      )
                    }
                  >
                    View Details
                  </span>
                </>
              ) : (
                <span
                  className="orderCardBlueFont"
                  onClick={() =>
                    this.navigateToOrderSummary(
                      this.props.orderDetails?.orderId,
                      this.props.orderDetails?.orderStatus,
                      "Delivered"
                    )
                  }
                >
                  View Details
                </span>
              )}
              <span
                className="orderCardBlueFont"
                onClick={() =>
                  this.reorderHandler(this.props.orderDetails?.orderId)
                }
              >
                Reorder
              </span>
            </>
          )}

          {/* Statuses after delivered  */}
          {(this.props.orderDetails?.orderStatusId === 56 ||
            this.props.orderDetails?.orderStatusId === 191 ||
            this.props.orderDetails?.orderStatusId === 199 ||
            this.props.orderDetails?.orderStatusId === 200 ||
            this.props.orderDetails?.orderStatusId === 201) && (
            <>
              <span
                className="orderCardBlueFont"
                onClick={() =>
                  this.navigateToOrderSummary(
                    this.props.orderDetails?.orderId,
                    this.props.orderDetails?.orderStatus,
                    "Delivered"
                  )
                }
              >
                View Details
              </span>
              <span
                className="orderCardBlueFont"
                onClick={() =>
                  this.reorderHandler(this.props.orderDetails?.orderId)
                }
              >
                Reorder
              </span>
            </>
          )}

          {/* Cancelled  */}
          {this.props.orderDetails?.orderStatusId === 57 && (
            <>
              <span
                className="orderCardBlueFont"
                onClick={() =>
                  this.navigateToOrderSummary(
                    this.props.orderDetails?.orderId,
                    this.props.orderDetails?.orderStatus,
                    "Cancelled"
                  )
                }
              >
                View Details
              </span>
              <span
                className="orderCardBlueFont"
                onClick={() =>
                  this.reorderHandler(this.props.orderDetails?.orderId)
                }
              >
                Reorder
              </span>
            </>
          )}
        </div>
      </div>
    );
  }
}

let mapStateToProps = (state) => ({
  token: state.loginReducer?.verifyOtpSuccess,
  custId: state.loginReducer?.verifyOtpSuccess?.CustomerDto?.customerId,
  reorder: state.reorderReducer,
  orderStatus: state.orderStatusUpdate,
});

export default withRouter(
  connect(mapStateToProps, {
    reorderThunk,
    AddIncompleteOrderMedsAction,
    toCartSectionAction,
    orderStatusThunk,
    getOrderStatusDetailsDataAction,
  })(OrderCard)
);

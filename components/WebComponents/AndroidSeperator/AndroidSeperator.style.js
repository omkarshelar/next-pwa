import styled from "styled-components";

export const AndroidSeperatorDiv = styled.div`
  display: ${(props) => (props.showWeb ? "block" : "none")};
  height: 8px;
  margin: 0px;
  background-color: ${(props) =>
    props.colorCustom ? props.colorCustom : "#E3E6EC"};
  /* box-shadow: 0px 1px 4px 0px #ccc inset; */

  @media (max-width: 767px) {
    display: ${(props) => (props.showMob ? "block" : "none")};
  }
`;

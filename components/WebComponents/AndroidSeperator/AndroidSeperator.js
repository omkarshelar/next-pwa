import React from "react";
import { AndroidSeperatorDiv } from "./AndroidSeperator.style";

const AndroidSeperator = (props) => {
  return (
    <AndroidSeperatorDiv
      showWeb={props.showWeb}
      showMob={props.showMob}
      colorCustom={props.colorCustom}
    />
  );
};

export default AndroidSeperator;

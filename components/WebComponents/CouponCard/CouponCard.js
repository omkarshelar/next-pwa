import React, { Component } from "react";
import "./CouponCard.css";
import ticketIcon from "../../../src/Assets/coupon-ticket.svg";
import appliedIcon from "../../../src/Assets/coupon-applied.svg";
import applyCoupon from "../../../src/Assets/applyCouponChevron.svg";
import AndroidSeperator from "../AndroidSeperator/AndroidSeperator";
export class CouponCard extends Component {
  render() {
    if (
      !this.props.details ||
      (this.props.details && this.props.promoCode && this.props.promoCodeValue)
    ) {
      return (
        <React.Fragment>
          <div className="reusableCouponCardContainer">
            {this.props.promoCode && this.props.promoCodeValue ? (
              <>
                <div className="reusableCouponCardApplied">
                  <div className="ticketLeftPart" />
                  <div className="ticketRightPart" />
                  <div>
                    <img src={appliedIcon} alt="applied" />
                    <span>{this.props.promoCode} Applied</span>
                  </div>
                  {this.props.details ? null : (
                    <span
                      className="reusableCouponCardRemove"
                      onClick={this.props.onClickRemove}
                    >
                      Remove
                    </span>
                  )}
                </div>
                {!this.props.details && (
                  <p onClick={this.props.onClickTnc}>Terms and Conditions</p>
                )}
              </>
            ) : (
              <div
                className="reusableCouponCard"
                onClick={this.props.onClickApply}
              >
                <div className="ticketLeftPart" />
                <div className="ticketRightPart" />
                <div>
                  <img src={ticketIcon} alt="coupon" />
                  <span>Apply Coupon</span>
                </div>
                <img src={applyCoupon} />
              </div>
            )}
          </div>
          <AndroidSeperator showMob={true} />
        </React.Fragment>
      );
    } else {
      return null;
    }
  }
}

export default CouponCard;

import React, { Component } from "react";
import {
  CarouselItem,
  CarouselStyle,
  Carouselwrapper,
  BlankBannerBox,
} from "./Banner.style";
import next from "../Upload/next.svg";
import prev from "../Upload/prev.svg";
import Login from "../../WebComponents/Login/Login";
import Shimmer from "react-shimmer-effect";
import {
  eventHomepageBanner1,
  eventHomepageBanner2,
  eventHomepageBanner3,
} from "../../../src/Events/Events";
import Router from "next/router";
import window from "global";
import Image from "next/image";

export class Banner extends Component {
  constructor(props) {
    super(props);
    this.state = {
      innerWid: 1500,
      loginModal: false,
    };
  }

  handleResize = () => {
    this.setState({ innerWid: window.innerWidth });
  };
  componentDidMount() {
    this.setState({ innerWid: window.innerWidth });
    window.addEventListener("resize", this.handleResize);

    //hiding on mobile
    if (this.refs.vidBannerRef) {
      this.refs.vidBannerRef.play();
    }

    //hiding on mobile
    if (this.refs.vidBanner1Ref) {
      this.refs.vidBanner1Ref.play();
    }
  }
  componentWillUnmount() {
    window.addEventListener("resize", this.handleResize);
  }
  loginModalHandler = () => {
    this.setState({ loginModal: !this.state.loginModal });
  };
  handleRouter = (content, index) => {
    if (index === 0) {
      eventHomepageBanner1();
    } else if (index === 1) {
      eventHomepageBanner2();
    } else if (index === 2) {
      eventHomepageBanner3();
    }
    if (content.drugType) {
      Router.push({
        pathname: `/search/${content.drugType}`,
        search: `?type=1`,
      });
    } else {
      let redirect = `/${content.redirectTo}`;
      if (content.redirectTo === "search") {
        redirect = "/?search=true";
      }
      if (content.redirectTo === "wallet") {
        if (this.props.accessToken) {
          Router.push({
            pathname: "/wallet",
            state: {
              id: 2,
            },
          });
        } else {
          // login
          this.setState({ loginModal: true });
        }
      } else {
        Router.push(redirect);
      }
    }
  };
  render() {
    return (
      <>
        <Login
          loginModal={this.state.loginModal}
          loginModalHandler={this.loginModalHandler}
        />
        {this.props.bannerLoading ? (
          <BlankBannerBox>
            <Shimmer>
              <div className="loadingBannerBox"></div>
            </Shimmer>
          </BlankBannerBox>
        ) : (
          <Carouselwrapper>
            <CarouselStyle indicators={false} next={next} prev={prev}>
              {this.state.innerWid > 668
                ? this.props.bannerImages &&
                  this.props.bannerImages.map((content, index) => (
                    <CarouselItem key={content.id}>
                      <div
                        className="banner-wrapper"
                        onClick={() => this.handleRouter(content, index)}
                      >
                        {content.webUrl.includes(".mp4") ? (
                          <video
                            ref="vidBannerRef"
                            id="addMedBtn"
                            autoPlay
                            loop
                            muted
                            playsInline
                          >
                            <source src={content.webUrl} type="video/mp4" />
                            Your browser does not support the video
                          </video>
                        ) : (
                          <Image
                            id="addMedBtn"
                            alt="banner"
                            src={content.webUrl}
                            layout="fill"
                            priority={index === 0 ? true : false}
                            objectFill="contain"
                          />
                        )}
                      </div>
                    </CarouselItem>
                  ))
                : this.props.bannerImages &&
                  this.props.bannerImages.map((content, index) => (
                    <CarouselItem key={content.id}>
                      <div
                        className="banner-wrapper"
                        onClick={() => this.handleRouter(content, index)}
                      >
                        {content.image.includes(".mp4") ? (
                          <video
                            ref="vidBanner1Ref"
                            id="addMedBtn"
                            autoPlay
                            loop
                            muted
                            playsInline
                          >
                            <source src={content.image} type="video/mp4" />
                            Your browser does not support the video
                          </video>
                        ) : (
                          <Image
                            id="addMedBtn"
                            alt="banner"
                            src={content.image}
                            priority={index === 0 ? true : false}
                            layout="fill"
                            objectFill="contain"
                          />
                        )}
                      </div>
                    </CarouselItem>
                  ))}
            </CarouselStyle>
          </Carouselwrapper>
        )}
      </>
    );
  }
}

export default Banner;

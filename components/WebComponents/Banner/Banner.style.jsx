import { Carousel } from "react-bootstrap";
import styled from "styled-components";

export const BlankBannerBox = styled.div`
  width: 100%;
  height: 250px;
  display: flex;
  justify-content: center;
  align-items: center;

  .loadingBannerBox {
    width: 90%;
    height: 200px;
    border-radius: 5px;
    /* margin-top: 1rem; */
  }

  @media screen and (max-width: 768px) {
    height: 200px;

    .loadingBannerBox {
      height: 150px;
    }
  }
`;

export const Carouselwrapper = styled.div`
  width: 100%;
  position: relative;
  margin: 0 auto;
`;

export const CarouselItem = styled(Carousel.Item)`
  > div[class="banner-wrapper"] {
    width: 100%;
    position: relative;
    display: flex !important;
    z-index: 0;
    height: 270px;
    img[alt="banner"],
    video {
      width: 100%;
      cursor: pointer;
    }
  }
  @media screen and (max-width:1024px)
  { > div[class="banner-wrapper"] {
    height: 240px;
  }
  @media screen and (max-width:993px)
  { > div[class="banner-wrapper"] {
    height: 200px;
  }
  @media screen and (max-width:426px)
  { > div[class="banner-wrapper"] {
    height: 200px;
  }
  @media screen and (max-width:321px)
  { > div[class="banner-wrapper"] {
    height: 150px;
  }
    
  }

  > div[class="caption"] {
    position: absolute;
    width: 340px;
    top: 15%;
    left: 10%;
    > span {
      /* font-family: "Century Gothic", sans-serif; */
      font-size: 40px;
      font-style: normal;
      font-weight: 700;
      line-height: 61px;
      letter-spacing: 0px;
      text-align: left;
      color: #003055;
      @media screen and (max-width: 1068px) {
        font-size: 40px;
      }
      @media screen and (max-width: 968px) {
        font-size: 30px;
      }
      @media screen and (max-width: 768px) {
        font-size: 20px;
      }
      @media screen and (max-width: 568px) {
        display: none;
      }
    }
    > p {
      font-size: 16px;
      font-style: normal;
      font-weight: 400;
      line-height: 21px;
      letter-spacing: 0em;
      text-align: left;
      color: #003055;
      @media screen and (max-width: 1068px) {
        font-size: 14px;
      }
      @media screen and (max-width: 968px) {
        font-size: 12px;
      }
      @media screen and (max-width: 568px) {
        display: none;
      }
    }

    @media screen and (max-width: 568px) {
      bottom: 1rem;
      top: auto;
      left: 50%;
      display: flex;
      align-items: center;
      transform: translate(-50%, -50%);
      justify-content: center;
    }
  }

  @media screen and (max-width: 668px) {
    > div[class="banner-wrapper"] {
      img[alt="banner"] {
        width: 100%;
        cursor: pointer;
        z-index: 0;
      }
    }
  }
`;

export const CarouselStyle = styled(Carousel)`
  .carousel-control-prev-icon {
    width: 50px;
    height: 50px;
    background-image: ${(props) => `url(${props.prev})`};
  }

  .carousel-control-next-icon {
    background-image: ${(props) => `url(${props.next})`};
    width: 50px;
    height: 50px;
  }
  .carousel-control-prev {
    transform: translateX(-50px);
    opacity: 1;
  }
  .carousel-control-next {
    transform: translateX(50px);
    opacity: 1;
  }
  @media screen and (max-width: 1068px) {
    .carousel-control-prev {
      transform: translateX(-30px);
      opacity: 1;
    }
    .carousel-control-next {
      transform: translateX(30px);
      opacity: 1;
    }
  }

  @media screen and (max-width: 668px) {
    .carousel-control-prev-icon {
      width: 30px;
      height: 30px;
    }

    .carousel-control-next-icon {
      width: 30px;
      height: 30px;
    }
    .carousel-control-prev {
      transform: translateX(-5px);
      opacity: 1;
    }
    .carousel-control-next {
      transform: translateX(5px);
      opacity: 1;
    }
  }
`;

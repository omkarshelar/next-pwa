import React from "react";
import "./DeliveryError.css";
import prompt from "../../../src/Assets/Prompt.svg";
const DeliveryError = (props) => {
  return (
    <div className={"deliveryError_wrapper"}>
      <div className={"errorPrompt_wrap"}>
        <div className={"errorPrompt_img"}>
          <img src={prompt} alt="prompt" />
        </div>
        <div className={"errorPrompt_text"}>
          This pincode is no longer serviceable. Please change your pincode
        </div>
      </div>
      <div className={"deliveryError_textwrap"}>
        <div className={"deliveryError_innerWrap"}>
          <div className={"deliveryError_textTitle"}>CAN'T BE DELIVERED TO</div>
          <div className={"deliveryError_pincode_text"}>
            {props.isServiceable?.pincode}
          </div>
        </div>

        <div
          className={"deliveryError_text_cta"}
          onClick={props.onClickHandler}
        >
          CHANGE
        </div>
      </div>
    </div>
  );
};

export default DeliveryError;

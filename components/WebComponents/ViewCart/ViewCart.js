import React, { Component } from "react";
import "./ViewCart.css";
import Button from "../Button/Button";
import { FaChevronCircleRight } from "react-icons/fa";
export class ViewCart extends Component {
  render() {
    return (
      <div className="view-cart-container">
        <p>
          {this.props.items +
            (this.props.items === 1 ? " item " : " items ") +
            "in the cart"}
        </p>
        <Button CartView onClick={this.props.onClickView}>
          View Cart
          <FaChevronCircleRight style={{ marginLeft: "9px" }} />
        </Button>
      </div>
    );
  }
}

export default ViewCart;

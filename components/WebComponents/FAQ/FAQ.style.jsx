import styled from "styled-components";

export const FaqWrapper = styled.div`
  width: 70%;
  display: flex;
  box-sizing: border-box;
  flex-direction: column;
  margin: 0 auto;
  padding-top: 1rem;
  padding-bottom: 3rem;
  @media screen and (max-width: 1200px) {
    width: 90%;
  }
  @media screen and (max-width: 968px) {
    display: none;
  }
`;
export const Header = styled.div`
  > h2 {
    /* font-family: "Century Gothic", sans-serif; */
    font-size: 30px;
    font-style: normal;
    font-weight: 700;
    letter-spacing: 0px;
    text-align: center;
    color: #003055;
    margin: 0;
  }
  @media screen and (max-width: 568px) {
    > h2 {
      text-align: left;
      padding: 0 10px;
      font-size: 25px;
    }
  }
`;
export const SubHeader = styled.div`
  width: 70%;
  align-self: center;
  > p {
    font-style: normal;
    font-weight: normal;
    font-size: 16px;
    line-height: 130.2%;
    text-align: center;
    color: #333333;
    margin-top: 10px;
  }
  @media screen and (max-width: 568px) {
    display: none;
  }
`;
export const FaqList = styled.div`
  display: flex;
  @media screen and (max-width: 568px) {
    flex-flow: column;
  }
`;
export const PlaceHolder = styled.div`
  box-sizing: border-box;
  /* padding: 20px; */
  width: 40%;
  > div {
    height: 100%;
    background: #ffffff;
    border: 1px solid #6ec1fd;
    box-sizing: border-box;
  }
  > img {
    width: 100%;
  }
  @media screen and (max-width: 568px) {
    /* width: 100%;
    > div {
      width: 90%;
      height: 300px;
      margin: auto;
    } */
    display: none;
  }
`;
export const List = styled.div`
  width: 60%;
  display: flex;
  flex-direction: column;
  justify-content: space-between;
  @media screen and (max-width: 568px) {
    width: 95%;
    margin: auto;
  }
  > button {
    align-self: center;
  }
`;

import React, { Component } from "react";
import "./FAQ.css";
import {
  FaqWrapper,
  Header,
  SubHeader,
  FaqList,
  PlaceHolder,
  List,
} from "./FAQ.style";
import Accordion from "@material-ui/core/Accordion";
import AccordionSummary from "@material-ui/core/AccordionSummary";
import AccordionDetails from "@material-ui/core/AccordionDetails";
import ExpandMoreIcon from "@material-ui/icons/ExpandMore";
import Button from "../../WebComponents/Button/Button";
import questionsIcon from "../../../src/Assets/questions.webp";
import {
  eventHomepageFaq,
  eventHomepageViewMoreFaq,
} from "../../../src/Events/Events";

export class FAQ extends Component {
  render() {
    return (
      <FaqWrapper className="faqMainContainer">
        <Header>
          <h2>FAQ</h2>
        </Header>
        <SubHeader>
          <p>
            When it comes to your health, be 100% sure. And we’re happy to help
            you at every step of the way. Before reaching out to us, we request
            you to check the list of frequently asked questions.
          </p>
        </SubHeader>
        <FaqList>
          <PlaceHolder>
            {/* <div /> */}
            <img src={questionsIcon} alt="questions" />
          </PlaceHolder>
          <List>
            <Accordion square>
              <AccordionSummary
                expandIcon={<ExpandMoreIcon />}
                aria-controls="panel1a-content"
                id="panel1a-header"
                onClick={() => eventHomepageFaq()}
              >
                <span className="faq-list-text">
                  Is opting for alternatives safe?
                </span>
              </AccordionSummary>
              <AccordionDetails>
                Yes. Opting for alternatives is safe. Alternatives contain the
                same drug salts, go through the same testing standards and are
                as effective as your branded medicines. They just cost a lot
                lesser and thus help you save a lot every time you buy
                medicines.
              </AccordionDetails>
            </Accordion>
            <Accordion square>
              <AccordionSummary
                expandIcon={<ExpandMoreIcon />}
                aria-controls="panel1a-content"
                id="panel1a-header"
                onClick={() => eventHomepageFaq()}
              >
                <span className="faq-list-text">
                  How do I know if I am choosing the right alternative?
                </span>
              </AccordionSummary>
              <AccordionDetails>
                Choosing the right alternative can be tricky. Before paying for
                your medicine order, you can talk to our doctors for free. They
                will guide you and help you pick the best alternative to your
                branded medicine.
              </AccordionDetails>
            </Accordion>
            <Accordion square>
              <AccordionSummary
                expandIcon={<ExpandMoreIcon />}
                aria-controls="panel1a-content"
                id="panel1a-header"
                onClick={() => eventHomepageFaq()}
              >
                <span className="faq-list-text">
                  Is there a guarantee on the quality of alternatives?
                </span>
              </AccordionSummary>
              <AccordionDetails>
                At Truemeds, we believe that your health comes first. Always. We
                would never compromise on the quality of the medicines we
                recommend. We only sell medicines produced by the top 30
                medicine makers of India.
              </AccordionDetails>
            </Accordion>
            <Accordion square>
              <AccordionSummary
                expandIcon={<ExpandMoreIcon />}
                aria-controls="panel1a-content"
                id="panel1a-header"
                onClick={() => eventHomepageFaq()}
              >
                <span className="faq-list-text">
                  How can I save on medicines?
                </span>
              </AccordionSummary>
              <AccordionDetails>
                That’s simple. Search for the medicines you’re looking to buy
                and confirm your order. Our doctors will get in touch with you
                to help you replace your branded medicines with quality
                alternatives. Opt for Options by Truemeds and save up to 72% on
                every order.
              </AccordionDetails>
            </Accordion>
            <Accordion square>
              <AccordionSummary
                expandIcon={<ExpandMoreIcon />}
                aria-controls="panel1a-content"
                id="panel1a-header"
                onClick={() => eventHomepageFaq()}
              >
                <span className="faq-list-text">
                  How are alternatives different from branded medicines?
                </span>
              </AccordionSummary>
              <AccordionDetails>
                Alternatives contain the same drug salts, go through the same
                testing standards and are as effective as branded medicines.
                They may differ in shape, size, colour and packaging and also
                definitely cost a lot lesser than branded medicines.
              </AccordionDetails>
            </Accordion>
            <Accordion square>
              <AccordionSummary
                expandIcon={<ExpandMoreIcon />}
                aria-controls="panel1a-content"
                id="panel1a-header"
                onClick={() => eventHomepageFaq()}
              >
                <span className="faq-list-text">
                  How can I avail free delivery?
                </span>
              </AccordionSummary>
              <AccordionDetails>
                Ensure your order value is above ₹500 to avail free delivery OR
                opt for Truemeds Recommendations and get FREE delivery.
              </AccordionDetails>
            </Accordion>

            <Button
              style={{ color: "#0071BC" }}
              seeMore
              onClick={() => {
                eventHomepageViewMoreFaq();
                Router.push("/help");
              }}
            >
              View More FAQ’s
            </Button>
          </List>
        </FaqList>
      </FaqWrapper>
    );
  }
}

export default FAQ;

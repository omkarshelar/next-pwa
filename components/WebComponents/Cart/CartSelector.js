import { createSelector } from "reselect";

const selectCart = (state) => state.cartData;

export const selectCartItems = createSelector(
  [selectCart],
  (cart) => cart.cartItems
);

export const selectTotalAmount = createSelector(
  [selectCartItems],
  (cartItems) =>
    cartItems.reduce(
      (prev, next) =>
        prev +
        next.quantity *
          (next._source.original_product_code === next._source.subs_product_code
            ? next._source.subs_selling_price
            : next._source.original_mrp -
              next._source.original_mrp *
                (next._source.original_base_discount / 100)),
      0
    )
);

// export const selectTotalDiscount = createSelector(
//   [selectCartItems],
//   (cartItems) =>
//     cartItems.reduce((prev, next) => prev + next._source.original_mrp, 0)
// );

export const selectTotalDiscount = createSelector(
  [selectCartItems],
  (cartItems) =>
    cartItems.reduce(
      (prev, next) => prev + next.quantity * next._source.original_mrp,
      0
    )
);

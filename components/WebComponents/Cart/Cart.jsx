import React, { Component } from "react";
import { connect } from "react-redux";
import { withRouter } from "next/router";
import "./Cart.css";
import { comfirmMedicineThunk } from "../../../redux/ConfirmMedicine/Action";
import { incompleteOrderThunk } from "../../../redux/IncompleteOrder/Action";
import {
  toUploadPrescriptionAction,
  notOnOrderSummaryAction,
} from "../../../redux/Timeline/Action";
import { setPaymentModeThunk } from "../../../redux/MyOrder/Action";
import { fetchProductDetailsThunk } from "../../../redux/ProductDetails/Action";
import { message } from "antd";
import MedCardLoader from "../Shimmer/MedCardLoader";
import Login from "../Login/Login";
import { clearMedDetailsAction } from "../../../redux/ConfirmMedicine/Action";
import { lastImageClearAction } from "../../../redux/UploadImage/Action";
import MedicineCardAccordian from "../MedicineCardAccordian/MedicineCardAccordian";
import cartBag from "../../../src/Assets/bag.svg";
import AddMedicine from "../../../pageComponents/cart/AddMedicine";
import FeaturedMedicineWOBG from "../../../pageComponents/cart/FeaturedMedicineWOBG/FeaturedMedicineWOBG";
import Dialog from "@material-ui/core/Dialog";
import subsSavingsPercent from "../../../src/Assets/subsSavingsPercent.svg";
import { IoClose } from "react-icons/io5";
import { setSaveMoreAction } from "../../../redux/SaveMore/Action";
// import { eventMedicineConfirmed } from "../../../src/Events/Events";
import Skeleton from "@mui/material/Skeleton";
import window from "global";

export class Cart extends Component {
  state = {
    medData: [],
    uploadPres: false,
    lessTotal: false,
    openRec: false,
    rMedData: [],
    rContent: {},
    loading: false,
    loginModal: false,
    doctorsImgLoaded: false,
    selectedInfo: "",
    showSavingsModal: false,
    isVisible: false,
    redirectDone: false,
  };

  componentDidMount = () => {
    window.scrollTo(0, 0);
    window.history.pushState(null, null, window.location.pathname);
    window.addEventListener("popstate", this.onBackButtonEvent);
    //page change for hotjar
    // hjPageChange(window.location.origin + "/orderflow#cart");
    let savings =
      this.props.cartContents?.length > 0
        ? Number(
            this.props.calcDiscPreProcessingForCart(this.props.cartContents)
          )
        : 0;

    if (savings >= 50 && !this.props.saveMore) {
      this.setState(
        {
          showSavingsModal: true,
        },
        () => {
          this.props.setSaveMoreAction();
        }
      );
    }
    setTimeout(() => {
      this.setState({
        showSavingsModal: false,
      });
    }, 2000);
  };

  componentDidUpdate = (prevProps) => {
    if (this.props.cartContents?.length === 0 && !this.state.redirectDone) {
      this.setState({ redirectDone: true });
      this.props.router.replace("/empty-cart");
    }

    if (prevProps.isProceed !== this.props.isProceed && this.props.isProceed) {
      this.confirmMedRender(this.props.totalAmountCheckOut);
    }

    if (this.props.duplicateOrderId) {
      this.props.setDuplicateOrderId(false);
      this.confirmMedRender(this.props.totalAmountCheckOut);
    }
  };

  // Clear State after unmounting
  componentWillUnmount = () => {
    window.removeEventListener("popstate", this.onBackButtonEvent);
  };

  // On Back Button click Event.
  onBackButtonEvent = (e) => {
    e.preventDefault();
    if (!this.isBackButtonClicked) {
      if (
        window &&
        this.props.storeData?.orderStatusDetailsData?.isReorder &&
        this.props.storeData?.orderStatusDetailsData?.isMyOrder
      ) {
        this.isBackButtonClicked = true;
        this.props.router.push("/myorders");
        // this.props.router.push(
        //   {
        //     pathname: "/myorders",
        //     query: {
        //       isMyOrder: true,
        //       orderType: this.props.storeData?.orderStatusDetailsData?.orderType,
        //       patientFilter: this.props.storeData?.orderStatusDetailsData?.patientFilter,
        //     },
        //   },
        //   "/myorders"
        // );
      } else {
        this.props.router.back();
        this.isBackButtonClicked = false;
      }
    }
  };

  createOrder = () => {
    this.setState(
      {
        uploadPres: false,
        lessTotal: false,
        medData: this.props.cartContents.map((data) => {
          return {
            productCode: data._source.original_product_code,
            quantity: data.quantity,
          };
        }),
      },
      async () => {
        if (this.props.medConfirm?.ConfirmMedData?.orderId) {
          this.setState({ loading: true });
          if (this.props.appliedCoupon?.offerId) {
            await this.props.setPaymentModeThunk({
              orderId: this.props.medConfirm.ConfirmMedData.orderId,
              access_token: this.props.accessToken?.Response.access_token,
              paymentId: this.props.getPaymentIdFromType(
                this.props.appliedCoupon.applicableOn
              ),
            });
          }
          this.props
            .incompleteOrderThunk({
              orderIds: this.props.medConfirm.ConfirmMedData.orderId,
              accessToken: this.props.accessToken?.Response.access_token,
              medSet: this.state.medData,
              offerId: this.props.appliedCoupon?.offerId
                ? this.props.appliedCoupon.offerId
                : 0,
              history: this.props.router,
              pincode: this.props.pincodeDetails.pincode,
            })
            .then(() => {
              this.setState({ loading: false });
              if (!this.props.medIncomplete.incompleteOrderError) {
                this.props.toUploadPrescriptionAction();
              } else {
                if (
                  this.props.medIncomplete.incompleteOrderError ===
                  "Sorry order cannot be confirmed. Please contact customer support."
                ) {
                  this.props.clearMedDetailsAction().then(() => {
                    this.props.lastImageClearAction().then(() => {
                      this.props.notOnOrderSummaryAction();
                      this.createOrder();
                    });
                  });
                } else {
                  message.error(this.props.medIncomplete.incompleteOrderError);
                }
              }
            });
        } else {
          this.setState({ loading: true });
          this.props
            .comfirmMedicineThunk({
              orderIds: 0,
              accessToken: this.props.accessToken?.Response.access_token,
              medSet: this.state.medData,
              customerId: this.props.custId,
              offerId: this.props.appliedCoupon?.offerId
                ? this.props.appliedCoupon.offerId
                : 0,
              pincode: this.props.pincodeDetails?.pincode,
              history: this.props.router,
            })
            .then(async () => {
              if (this.props.appliedCoupon?.offerId) {
                if (this.props.medConfirm?.ConfirmMedData?.orderId) {
                  await this.props.setPaymentModeThunk({
                    orderId: this.props.medConfirm.ConfirmMedData.orderId,
                    access_token: this.props.accessToken?.Response.access_token,
                    paymentId: this.props.getPaymentIdFromType(
                      this.props.appliedCoupon.applicableOn
                    ),
                  });
                } else if (this.props.imgUpload?.orderId) {
                  await this.props.setPaymentModeThunk({
                    orderId: this.props.imgUpload?.orderId,
                    access_token: this.props.accessToken?.Response.access_token,
                    paymentId: this.props.getPaymentIdFromType(
                      this.props.appliedCoupon.applicableOn
                    ),
                  });
                }
              }
              this.setState({ loading: false });
              if (!this.props.medConfirm.ConfirmMedError) {
                // Event_Truemeds
                //new
                // eventMedicineConfirmed();
                this.props.toUploadPrescriptionAction();
              } else {
                message.error(this.props.medConfirm.ConfirmMedError);
              }
            });
        }
      }
    );
  };

  confirmMedRender = (total) => {
    if (this.props.accessToken && this.props.loginDetails.isUserLogged) {
      if (
        this.props.Stepper.onOrderSummary &&
        (this.props.medConfirm?.ConfirmMedData?.orderId ||
          this.props.imgUpload?.orderId)
      ) {
        this.props.createOrderWithSummary();
      } else {
        this.createOrder();
      }
    } else {
      // login
      this.setState({ loginModal: true });
      //page change for hotjar
      // hjPageChange("https://stage-pwa.truemeds.in/orderflow#login");
    }
  };

  calcPercent = () => {
    let result = this.props.cartContents.map((content) =>
      content._source.original_product_code !==
      content._source.subs_product_code
        ? content._source.original_base_discount
        : Math.round(
            ((content._source.original_mrp -
              content._source.subs_selling_price) *
              100) /
              content._source.original_mrp
          )
    );
    return result;
  };

  calcAvgPercent = () => {
    let data = this.calcPercent();
    let addPercent = data.reduce((prev, next) => prev + next, 0);
    let avg = addPercent / data.length;
    return Math.round(avg);
  };

  loginModalHandler = () => {
    this.setState({ loginModal: !this.state.loginModal });
  };

  render() {
    return (
      <div>
        <Login
          loginModal={this.state.loginModal}
          loginModalHandler={this.loginModalHandler}
        />
        <div className="cartMainContainer">
          {this.props.isLoading ? (
            <div className="cartItemsAddedLoader">
              <Skeleton animation="wave" height={40} />
            </div>
          ) : (
            this.props.cartContents?.length > 0 && (
              <p>
                <img src={cartBag} style={{ "margin-top": "-6px" }} />
                {this.props.cartContents?.length === 1
                  ? "1 Item in cart"
                  : `${this.props.cartContents.length} Items in cart`}
              </p>
            )
          )}
          <div className="cartDetailsMainContainer">
            {this.props.isLoading ? (
              <MedCardLoader />
            ) : (
              this.props.cartContents.length > 0 &&
              this.props.cartContents.map((content, index) => (
                <MedicineCardAccordian
                  isCart={true}
                  content={content}
                  {...this.props}
                  getAddress={() => {
                    this.props
                      .fetchProductDetailsThunk({
                        productCd: content?._source?.original_product_code,
                        history: this.props.router,
                      })
                      .then(() => {
                        this.setState({
                          selectedInfo: content?._source?.original_product_code,
                        });
                      });
                  }}
                  selectedInfo={this.state.selectedInfo}
                  closeToolTip={() => this.setState({ selectedInfo: "" })}
                  quantityOnHandleChange={(e, content) => {
                    this.props.quantityOnHandleChange(e, content);
                  }}
                  onClickRemove={() => {
                    let data = [
                      {
                        medicineName: content._source.original_sku_name,
                        medicineQty: "0",
                        medicineId: content._source.original_product_code,
                      },
                    ];
                    this.setState(
                      {
                        rMedData: data,
                        rContent: content,
                      },
                      () => {
                        this.props.removeMedOnClick(this.state.rMedData);
                        this.props.removeItemCart(this.state.rContent);
                      }
                    );
                  }}
                  index={index}
                />
              ))
            )}
          </div>
          {this.props.isLoading ? (
            <div className="cartItemsAddedLoader">
              <Skeleton animation="wave" height={40} />
            </div>
          ) : (
            <AddMedicine
              redirectToHandler={() => {
                window.innerWidth < 468
                  ? this.props.router.push("/?search=true")
                  : this.props.router.push("/orderflow?search=true");
              }}
              cartContents={this.props.cartContents}
            />
          )}
          {window.innerWidth > 768 && (
            <div className="featureMedSection">
              <FeaturedMedicineWOBG />
            </div>
          )}
          <Dialog
            open={this.state.showSavingsModal}
            onClose={() => {
              this.setState({
                showSavingsModal: false,
              });
            }}
            aria-labelledby="responsive-dialog-title"
            fullWidth={false}
            classes={{ paper: "savings-rectangle" }}
          >
            <div className="savingModalWrap">
              <div className="logoWrap">
                <img src={subsSavingsPercent} />
              </div>
              <div className="savingModalWrapTitle">Yay! You have saved</div>
              <div className="savingModalWrapPrice">
                ₹
                {this.props.cartContents?.length > 0
                  ? Number(
                      this.props.calcDiscPreProcessingForCart(
                        this.props.cartContents
                      )
                    ).toFixed(2)
                  : 0}
              </div>
              <div className="savingModalWrapSubTitle">on this order</div>
              <div
                className="closeBtn"
                onClick={() => this.setState({ showSavingsModal: false })}
              >
                <IoClose />
              </div>
            </div>
          </Dialog>
        </div>
      </div>
    );
  }
}

const mapStateToProps = (state) => ({
  productDetails: state.productDetails,
  pincodeDetails: state.pincodeData?.pincodeData,
  masterDetails: state.masterDetails?.MasterDetails,
  saveMore: state.saveMore?.saveMore,
  storeData: state.storeData,
});

export default withRouter(
  connect(mapStateToProps, {
    comfirmMedicineThunk,
    incompleteOrderThunk,
    toUploadPrescriptionAction,
    setPaymentModeThunk,
    fetchProductDetailsThunk,
    clearMedDetailsAction,
    lastImageClearAction,
    notOnOrderSummaryAction,
    setSaveMoreAction,
  })(Cart)
);

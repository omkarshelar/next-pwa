import React from "react";
import { connect } from "react-redux";
import "./PaymetModal.Style.css";
import circleClose from "../../../src/Assets/CircleClose.png";

function PaymentModal({
  bill,
  cashFreeService,
  changePayment,
  billNav,
  handleClose,
}) {
  return (
    <div className="paymentModalWrapper">
      <div className="paymentModalBody">
        <span className="top-right" onClick={() => handleClose()}>
          <img
            src={circleClose}
            alt="cancel"
            style={{ width: "22px", height: "22px" }}
          ></img>
        </span>
        <div className="paymentHeader">
          <span className="paymentSpan">
            <span id="pay1">Pending</span>
            <span id="pay2"> payment</span>
          </span>
        </div>
        <div>
          {bill ? (
            bill.paymentValue ? (
              <span id="rupeeBill">&#8377;{bill.paymentValue}</span>
            ) : null
          ) : null}
        </div>
        <button onClick={cashFreeService} className="pay-btn">
          Pay Now
        </button>
        <div className="button-div">
          <button onClick={changePayment}>Change Payment Mode</button>
          <button onClick={billNav}>View Order Details</button>
        </div>
      </div>
    </div>
  );
}

export default connect(null)(PaymentModal);

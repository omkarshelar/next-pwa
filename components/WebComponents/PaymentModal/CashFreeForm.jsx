import React, { useRef, useEffect } from "react";

export default function CashFreeForm(props) {
  const submitInput = useRef();

  useEffect(() => {
    submitInput.current.submit();
  }, []);
  return (
    <div>
      <form
        ref={submitInput}
        id="redirectForm"
        method="post"
        action="https://test.cashfree.com/billpay/checkout/post/submit"
      >
        <input hidden name="appId" value={props.props.appId} />
        <input hidden name="orderId" value={props.props.orderId} />
        <input hidden name="orderAmount" value={props.props.orderAmount} />
        <input hidden name="orderCurrency" value={props.props.orderCurrency} />
        <input hidden name="customerName" value={props.props.customerName} />
        <input hidden name="customerEmail" value={props.props.customerEmail} />
        <input hidden name="customerPhone" value={props.props.customerPhone} />
        <input hidden name="returnUrl" value={props.props.returnUrl} />
        <input hidden name="notifyUrl" value={props.props.notifyUrl} />
        <input hidden name="signature" value={props.props.signature} />
        <input hidden type="submit" value="Pay"></input>
      </form>
    </div>
  );
}

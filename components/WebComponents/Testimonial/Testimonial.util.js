import alternativesIcon from "../../../src/Assets/alternatives.svg";
import savingsIcon from "../../../src/Assets/savings.svg";
import pincodesIcon from "../../../src/Assets/pincodes.svg";

export const stats_data = [
  {
    id: 1,
    img: alternativesIcon,
    header: "1.2 Lakhs +",
    subHeader: "Generic and Branded Medicines Available",
  },
  {
    id: 2,
    img: savingsIcon,
    header: "₹7 Cr. + Saved",
    subHeader: "We've helped India save upto 72% on every order",
  },
  {
    id: 3,
    img: pincodesIcon,
    header: "22,000 +",
    subHeader: "We service over 22k pin codes across the country",
  },
];

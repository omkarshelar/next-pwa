import React, { Component } from "react";
import {
  TestimonialWrapper,
  StatsWrapper,
  Stat,
  StatInfo,
  Container,
  Heading,
  Testimonials,
  Controller,
  List,
  TestimonialCard,
  Name,
  Experience,
  Wrapper,
  TestimonialContainer,
} from "./Testimonial.style";
import next from "../Upload/next.svg";
import prev from "../Upload/prev.svg";
import { createRef } from "react";
import quote from "./quote.svg";
import Button from "../../WebComponents/Button/Button";
import { stats_data } from "./Testimonial.util";
import BlankCard from "../BlankCard/BlankCard";
import { TestimonialContent } from "./TestimonialData";
import Image from "next/image";

export class Testimonial extends Component {
  constructor(props) {
    super(props);
    this.ListRef = createRef();
  }
  scrollRight = () => {
    if (this.ListRef.current) {
      this.ListRef.current.scrollLeft = this.ListRef.current.scrollLeft + 500;
    }
  };
  scrollLeft = () => {
    if (this.ListRef.current) {
      this.ListRef.current.scrollLeft = this.ListRef.current.scrollLeft - 500;
    }
  };
  render() {
    return (
      <TestimonialContainer>
        <TestimonialWrapper>
          <StatsWrapper>
            {stats_data.map((data) => (
              <Stat>
                <Image
                  src={data.img}
                  alt="alternatives"
                  width={100}
                  height={100}
                />
                <StatInfo>
                  <h3>{data.header}</h3>
                  <p>{data.subHeader}</p>
                </StatInfo>
              </Stat>
            ))}
          </StatsWrapper>
        </TestimonialWrapper>

        <Container>
          <Wrapper>
            <Heading>
              <h2>Don’t just believe us. Listen to our customers</h2>
              <Controller>
                <Button>
                  <img src={prev} onClick={this.scrollLeft} alt="prev"></img>
                </Button>
                <Button>
                  <img src={next} onClick={this.scrollRight} alt="next"></img>
                </Button>
              </Controller>
            </Heading>
          </Wrapper>
          <Testimonials>
            <List ref={this.ListRef}>
              <BlankCard />
              {TestimonialContent.map((data) => (
                <TestimonialCard>
                  <Name>
                    <h5>{data.author}</h5>
                    <img src={quote} alt="quote"></img>
                  </Name>
                  <Experience>
                    <p>{data.data}</p>
                  </Experience>
                </TestimonialCard>
              ))}
            </List>
          </Testimonials>
        </Container>
      </TestimonialContainer>
    );
  }
}

export default Testimonial;

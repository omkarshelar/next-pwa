import styled from "styled-components";

export const TestimonialWrapper = styled.div``;
export const StatsWrapper = styled.div`
  box-sizing: border-box;
  padding: 4rem 0;
  width: 80%;
  margin: 0 auto;
  display: flex;
  justify-content: space-evenly;
  flex-wrap: wrap;
  gap: 2rem;
  @media screen and (max-width: 568px) {
    flex-flow: column;
    justify-content: center;
    align-items: center;
    width: 100%;
  }
  @media screen and (max-width: 968px) {
    display: none;
  }
`;
export const Stat = styled.div`
  display: flex;
  gap: 5px;
  /* position: relative; */
  /* width: 250px;
  height: 240px; */
  @media screen and (max-width: 568px) {
    width: 300px;
    height: 210px;
  }
`;
export const StatInfo = styled.div`
  width: 215px;
  display: flex;
  flex-flow: column;
  /* position: absolute; */
  /* bottom: 0; */
  /* right: 0; */
  /* left: 140px; */
  > h3 {
    /* font-family: "Century Gothic", sans-serif; */
    font-size: 30px;
    font-style: normal;

    font-weight: bolder;

    letter-spacing: 0px;
    background: linear-gradient(180deg, #0071bc 0%, #6ec1fd 100%);
    background-clip: text;
    -webkit-background-clip: text;
    -webkit-text-fill-color: transparent;
    margin: 0;
  }
  > p {
    font-style: normal;
    font-weight: normal;
    font-size: 19px;
    color: #999;
  }
  @media screen and (max-width: 568px) {
    > p {
      width: 150px;
      font-size: 14px;
    }
    > h3 {
      width: 150px;
      font-size: 25px;
    }
    right: 00px;
    bottom: 1rem;
  }
`;

// Testimonial List

export const TestimonialContainer = styled.div`
  position: relative;
  background: linear-gradient(360deg, #c9e8fe 0%, rgba(180, 193, 253, 0) 50%);
  padding-bottom: 14rem;
  @media screen and (max-width: 968px) {
    padding-bottom: 0rem;
  }
`;
export const Container = styled.div`
  width: 100%;
  display: flex;
  flex-flow: column;
`;

export const Wrapper = styled.div`
  width: 85%;
  align-self: flex-end;
  @media screen and (max-width: 968px) {
    width: 100%;
  }
`;
export const Heading = styled.div`
  display: flex;
  justify-content: space-between;
  box-sizing: border-box;
  > h2 {
    /* font-family: "Century Gothic", sans-serif; */
    font-size: 30px;
    font-style: normal;
    font-weight: 700;
    letter-spacing: 0px;
    color: #003055;
  }
  @media screen and (max-width: 968px) {
    flex-flow: column;
    text-align: center;
    align-items: center;
    > h2 {
      box-sizing: border-box;
      margin-top: 1rem;
      font-size: 25px;
      padding: 5px;
    }
  }
  @media screen and (max-width: 568px) {
    > h2 {
      font-size: 21px;
    }
  }
  @media screen and (max-width: 480px) {
    > h2 {
      font-size: 18px;
      line-height: 35px;
    }
  }
`;
export const Testimonials = styled.div`
  display: flex;
  padding: 1rem 0 5rem 0;
  @media screen and (max-width: 568px) {
    flex-flow: column;
    padding: 0rem 0 3rem 0;
  }
`;
export const Controller = styled.div`
  display: flex;
  @media screen and (max-width: 568px) {
    justify-content: center;
  }
`;
export const List = styled.div`
  display: flex;

  overflow-x: auto;
  scroll-behavior: smooth;
  transition: scroll-behavior 1s ease;
  -ms-overflow-style: none; /* IE and Edge */
  scrollbar-width: none; /* Firefox */
  ::-webkit-scrollbar:horizontal {
    height: 0;
    width: 0;
    display: none;
  }

  ::-webkit-scrollbar-thumb:horizontal {
    display: none;
  }
`;
export const TestimonialCard = styled.div`
  box-sizing: border-box;
  min-width: 413px;
  padding: 3rem 1.5rem;
  margin: 0 13px;
  background: #ffffff;
  /* #D3F0E3 */
  border: 1px solid #d3f0e3;
  border-radius: 10px;
  @media screen and (max-width: 568px) {
    box-sizing: border-box;
    min-width: 300px;
    padding: 1rem 1rem;
  }
  @media screen and (max-width: 480px) {
    min-width: 175px;
    padding: 0.5rem;
    margin: 0 8px;
  }
`;
export const Name = styled.div`
  display: flex;
  justify-content: space-between;
  align-items: center;
  > h5 {
    /* width: 154px; */
    /* height: 25px; */
    /* font-family: Century Gothic; */
    font-style: normal;
    font-weight: bolder;
    font-size: 16px;
    line-height: 25px;

    color: #333333;
  }
  > img {
    width: 52px;
    height: 42px;
  }
  @media screen and (max-width: 568px) {
    > img {
      width: 42px;
      height: 32px;
    }
  }
  @media screen and (max-width: 480px) {
    > h5 {
      /* width: auto; */
      /* height: auto; */
      font-size: 12px;
    }
    > img {
      width: 20px;
    }
  }
`;
export const Experience = styled.div`
  > p {
    font-style: normal;
    font-weight: normal;
    font-size: 16px;
    margin-bottom: 0;
    color: #333333;
  }
  @media screen and (max-width: 568px) {
    > p {
      font-size: 16px;
    }
  }
  @media screen and (max-width: 480px) {
    > p {
      font-size: 12px;
    }
  }
`;

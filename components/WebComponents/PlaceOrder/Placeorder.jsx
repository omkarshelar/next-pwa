import React from "react";
import {
  PlaceorderWrapper,
  StepsWrapper,
  Step,
  StepImage,
  StepInfo,
} from "./Placeorder.style";
import medicineIcon from "../../../src/Assets/medicine.svg";
import consultationIcon from "../../../src/Assets/consultation.svg";
import prescriptionIcon from "../../../src/Assets/prescription.svg";

const mobOffset = 20;
const webOffset = 100;

function Placeorder() {
  return (
    <PlaceorderWrapper>
      <span>How to place an order with Truemeds</span>
      <StepsWrapper>
        <Step>
          <StepImage>
            <img src={prescriptionIcon} alt="prescription" />
          </StepImage>
          <StepInfo>
            <h3>Step 1</h3>
            <span>Upload your prescription</span>
            <p>Or search for your medicines and add them to your cart</p>
          </StepInfo>
        </Step>
        <Step>
          <StepImage>
            <img src={consultationIcon} alt="consultation" />
          </StepImage>
          <StepInfo>
            <h3>Step 2</h3>
            <span>Avail free doctor e-consultation</span>
            <p>
              Once you confirm your order, our doctors will get in touch with
              you
            </p>
          </StepInfo>
        </Step>
        <Step>
          <StepImage>
            <img src={medicineIcon} alt="medicine" />
          </StepImage>
          <StepInfo>
            <h3>Step 3</h3>
            <span>Opt for quality alternatives</span>
            <p>
              Replace your branded medicines with doctor recommended
              alternatives and save up to 72%.
            </p>
          </StepInfo>
        </Step>
      </StepsWrapper>
    </PlaceorderWrapper>
  );
}

export default Placeorder;

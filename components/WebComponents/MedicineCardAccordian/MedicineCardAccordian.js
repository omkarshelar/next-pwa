import React from "react";
import "./MedicineCardAccordian.css";
import MedicineCardNew from "../MedicineCardNew/MedicineCardNew";
import convertProductTitle from "../../Helper/convertProductTitle";
import subsSavingsPercent from "../../../src/Assets/subsSavingsPercent.svg";
import subsDropdown from "../../../src/Assets/subsDropdown.svg";
import Accordion from "@material-ui/core/Accordion";
import AccordionSummary from "@material-ui/core/AccordionSummary";
import { customNumber } from "../../Helper/helperFunction";
// import { eventSubstituteViewed } from "../../../src/Events/Events";
import window from "global";

const MedicineCardAccordian = (props) => {
  const content = props.content;

  const checkIsProcessingStatus = (arr) => {
    let isProcessing = false;
    if (arr?.length > 0) {
      for (let i in arr) {
        let status = arr[i];
        if (status.statusId === 142) {
          isProcessing = true;
        }
      }
    }
    return isProcessing;
  };

  if (props.isOrderSummary) {
    return (
      <>
        {props.updateDetails.orderStatus.serialId === 39 ||
        props.updateDetails.orderStatus.serialId === 49 ||
        props.updateDetails.orderStatus.serialId === 2 ||
        (props.updateDetails.orderStatus.serialId === 57 &&
          !checkIsProcessingStatus(
            props.orderStatus?.orderTracking?.orderStatus
          )) ? (
          <div className={"medItem"}>
            <React.Fragment key={content.finalSubsId}>
              <MedicineCardNew
                isMyOrderDetails={props.isMyOrderDetails}
                content={content}
                isCart={props.isCart}
                goToDetails={() => {
                  let medId = (
                    "/medicine/" +
                    convertProductTitle(
                      content.orgMedName,
                      content.orgProductCd
                    )
                  ).slice(10);

                  window.openSideBar(true, 10, {
                    medId: medId,
                    medQty: content.orgQuantity,
                  });
                }}
                productCode={content.orgProductCd}
                getAddress={() => {
                  props.getAddress();
                }}
                isInfo={props.selectedInfo}
                closeToolTip={() => props.closeToolTip()}
                companyAddress={
                  props.productDetails?.allProductDetails?.MedicineDetails
                    ?.companyAddress
                }
                viewDetails={
                  !props.isViewDetails
                    ? props.orderFlowHistory?.location?.state?.detail
                    : true
                }
                maxQty={content.orgMaxCappedQty}
                coldChainDisabled={content.coldChainDisabled}
                disabled={content.disabled}
                medName={content.orgMedName}
                medType={content.medType ? content.medType : "DROPS"}
                packSize={content.orgPackSize}
                packUnits={content.orgUnit}
                companyName={content.orgCompanyName}
                serviceImage={content.productImages}
                mrp={content.orgMrp.toFixed(2)}
                sp={
                  content.orgProductCd === content.subsProductCd
                    ? content.subsSellingPrice.toFixed(2)
                    : (
                        content.orgMrp -
                        content.orgMrp * (content.orgDiscount / 100)
                      ).toFixed(2)
                }
                discount={
                  content.orgProductCd !== content.subsProductCd
                    ? content.orgDiscount
                    : Math.round(
                        ((content.orgMrp - content.subsSellingPrice) * 100) /
                          content.orgMrp
                      )
                }
                quantity={content.orgQuantity}
                rxRequired={content.orgRxRequired}
                quantityHandleChange={(e) =>
                  props.quantityOnHandleChange(e, content)
                }
                onClickRemove={() => props.onClickRemove()}
                cartContents={props.cartContents}
                imgUpload={props.imgUpload}
                hideLast={props.hideLast}
                orderStatus={props.updateDetails?.orderStatus?.serialId}
              />
              {content.orgProductCd !== content.subsProductCd && (
                <div className={"cartSubsMainContainer"}>
                  <div className={"cartSubsRecommendation"}>
                    <div className={"cartSubsRecommendation__wrap"}>
                      <div className={"cartSubsImgWrap"}>
                        <img src={subsSavingsPercent} />
                      </div>
                      <div>
                        <div className={"savingsSubs"}>
                          {"Get " +
                            (
                              ((content.orgMrp - content.subsSellingPrice) *
                                100) /
                              content.orgMrp
                            ).toFixed(0) +
                            "% Savings"}
                        </div>
                        <div className={"savingsSubsTitle"}>
                          Buy Truemeds Recommended Brands
                        </div>
                      </div>
                    </div>
                  </div>
                  <Accordion
                    square
                    defaultExpanded={true}
                    style={{ background: "#edf6f3", margin: "0px" }}
                  >
                    <AccordionSummary
                      className={"accordianHeaderHeight"}
                      expandIcon={
                        <img
                          src={subsDropdown}
                          style={{
                            width: "20px",
                            transform: "rotate( 180deg)",
                          }}
                        />
                      }
                      aria-controls="panel1a-content"
                      id="panel1a-header"
                    ></AccordionSummary>
                    <div className="cartSubMedCardWrap">
                      <MedicineCardNew
                        isMyOrderDetails={props.isMyOrderDetails}
                        content={content}
                        isCart={props.isCart}
                        goToDetails={() => {
                          let medId = (
                            "/medicine/" +
                            convertProductTitle(
                              content.subsMedName,
                              content.subsProductCd
                            )
                          ).slice(10);

                          window.openSideBar(true, 10, {
                            medId: medId,
                            subsDetails: {
                              isSubs: true,
                              subsMrp: content.subsMrp.toFixed(2),
                              subsSp: content.subsSellingPrice.toFixed(2),
                              subsDiscount: (
                                ((content.subsMrp - content.subsSellingPrice) *
                                  100) /
                                content.subsMrp
                              ).toFixed(2),
                            },
                          });
                        }}
                        productCode={content.subsProductCd}
                        getAddress={() => {
                          props.getAddress();
                        }}
                        isInfo={props.selectedInfo}
                        closeToolTip={() => props.closeToolTip()}
                        companyAddress={
                          props.productDetails?.allProductDetails
                            ?.MedicineDetails?.companyAddress
                        }
                        viewDetails={true}
                        quantity={content.subsQuantity}
                        addToCart={false}
                        medName={content.subsMedName}
                        packSize={content.subsPackSize}
                        packUnits={content.subsUnit}
                        companyName={content.subsCompanyName}
                        mrp={content.subsMrp}
                        sp={content.subsSellingPrice.toFixed(2)}
                        discount={(
                          ((content.subsMrp - content.subsSellingPrice) * 100) /
                          content.subsMrp
                        ).toFixed(2)}
                        cartContents={props.cartContents}
                        imgUpload={props.imgUpload}
                        rxRequired={content.subsRxRequired}
                        isSubs={true}
                        coldChainDisabled={content.coldChainDisabled}
                        disabled={content.disabled}
                        orderStatus={props.updateDetails?.orderStatus?.serialId}
                      />
                    </div>
                  </Accordion>
                </div>
              )}
              {(props.updateDetails?.productSubsMappingList &&
                props.updateDetails?.productSubsMappingList.length - 1 ===
                  props.index) ||
              props.hideLast ? null : (
                <hr className={"medCard_hr"} />
              )}
            </React.Fragment>
          </div>
        ) : (
          <div className={"medItem"}>
            <React.Fragment key={content.finalSubsId}>
              <MedicineCardNew
                isMyOrderDetails={props.isMyOrderDetails}
                isCart={props.isCart}
                goToDetails={() => {
                  let medId = (
                    "/medicine/" +
                    convertProductTitle(
                      content.statusId === 61 ||
                        content.orgProductCd === content.subsProductCd
                        ? content.subsMedName
                        : content.orgMedName,
                      content.statusId === 61 ||
                        content.orgProductCd === content.subsProductCd
                        ? content.subsProductCd
                        : content.orgProductCd
                    )
                  ).slice(10);
                  window.openSideBar(true, 10, {
                    medId: medId,
                    medQty:
                      content.statusId === 61 ||
                      content.orgProductCd === content.subsProductCd
                        ? content.subsQuantity
                        : content.orgQuantity,
                    subsDetails:
                      props.details &&
                      (content.statusId === 61 ||
                        content.orgProductCd === content.subsProductCd)
                        ? {
                            isSubs: true,
                            subsMrp: content.subsMrp.toFixed(2),
                            subsSp: content.subsSellingPrice.toFixed(2),
                            subsDiscount: (
                              ((content.subsMrp - content.subsSellingPrice) *
                                100) /
                              content.subsMrp
                            ).toFixed(2),
                          }
                        : null,
                  });
                }}
                productCode={
                  content.statusId === 61 ||
                  content.orgProductCd === content.subsProductCd
                    ? content.subsProductCd
                    : content.orgProductCd
                }
                getAddress={() => props.getAddress()}
                isInfo={props.selectedInfo}
                closeToolTip={() => props.closeToolTip()}
                companyAddress={
                  props.productDetails?.allProductDetails?.MedicineDetails
                    ?.companyAddress
                }
                viewDetails={true}
                quantity={
                  content.statusId === 61 ||
                  content.orgProductCd === content.subsProductCd
                    ? content.subsQuantity
                    : content.orgQuantity
                }
                addToCart={false}
                medName={
                  content.statusId === 61 ||
                  content.orgProductCd === content.subsProductCd
                    ? content.subsMedName
                    : content.orgMedName
                }
                packSize={
                  content.statusId === 61 ||
                  content.orgProductCd === content.subsProductCd
                    ? content.subsPackSize
                    : content.orgPackSize
                }
                packUnits={
                  content.statusId === 61 ||
                  content.orgProductCd === content.subsProductCd
                    ? content.subsUnit
                    : content.orgUnit
                }
                companyName={
                  content.statusId === 61 ||
                  content.orgProductCd === content.subsProductCd
                    ? content.subsCompanyName
                    : content.orgCompanyName
                }
                serviceImage={content.productImages}
                mrp={
                  content.statusId === 61 ||
                  content.orgProductCd === content.subsProductCd
                    ? content.subsMrp
                    : content.orgMrp.toFixed(2)
                }
                sp={
                  content.statusId === 61 ||
                  content.orgProductCd === content.subsProductCd
                    ? content.subsSellingPrice.toFixed(2)
                    : (
                        content.orgMrp -
                        content.orgMrp * (content.orgDiscount / 100)
                      ).toFixed(2)
                }
                discount={
                  content.statusId === 61 &&
                  content.orgProductCd !== content.subsProductCd
                    ? (
                        ((content.subsMrp - content.subsSellingPrice) * 100) /
                        content.subsMrp
                      ).toFixed(2)
                    : content.orgProductCd === content.subsProductCd
                    ? (
                        ((content.orgMrp - content.subsSellingPrice) * 100) /
                        content.orgMrp
                      ).toFixed(2)
                    : content.orgDiscount
                }
                rxRequired={
                  content.statusId === 61 ||
                  content.orgProductCd === content.subsProductCd
                    ? content.subsRxRequired
                    : content.orgRxRequired
                }
                cartContents={props.cartContents}
                imgUpload={props.imgUpload}
                coldChainDisabled={content.coldChainDisabled}
                disabled={content.disabled}
                viewOriginal={
                  props.viewOriginal &&
                  content.orgProductCd !== content.subsProductCd
                }
                content={content}
                viewOriginalClick={() => {
                  let medId = (
                    "/medicine/" +
                    convertProductTitle(
                      content.orgMedName,
                      content.orgProductCd
                    )
                  ).slice(10);
                  window.openSideBar(true, 10, {
                    medId: medId,
                    medQty: content.orgQuantity,
                    viewOriginal: true,
                    medSavings: (
                      (100 * (content.orgMrp - content.subsSellingPrice)) /
                      content.orgMrp
                    ).toFixed(2),
                    orderStatus: props.updateDetail?.orderStatus?.serialId,
                  });
                }}
                hideLast={props.hideLast}
                orderStatus={props.updateDetails?.orderStatus?.serialId}
              />
              {props.updateDetails?.productSubsMappingList.length - 1 ===
                props.index || props.hideLast ? null : (
                <hr className={"medCard_hr"} />
              )}
            </React.Fragment>
          </div>
        )}
      </>
    );
  } else {
    return (
      <div key={content._id} className={"medItem"}>
        <MedicineCardNew
          isCart={props.isCart}
          medType={content?._source?.original_drug_type}
          goToDetails={() => {
            let medId = (
              "/medicine/" +
              convertProductTitle(
                content?._source?.original_sku_name,
                content?._source?.original_product_code
              )
            ).slice(10);
            window.openSideBar(true, 10, {
              medId: medId,
              medQty: content.quantity,
            });
          }}
          productCode={content?._source?.original_product_code}
          getAddress={() => {
            props.getAddress();
          }}
          isInfo={props.selectedInfo}
          closeToolTip={() => props.closeToolTip()}
          companyAddress={
            props.productDetails?.allProductDetails?.MedicineDetails
              ?.companyAddress
          }
          countryOrigin={content?._source?.original_country_nm}
          serviceImage={content?._source?.product_image_urls}
          maxQty={content?._source.max_capped_qty}
          medName={content._source.original_sku_name}
          packSize={content._source.original_pack}
          packUnits={content._source.original_unit}
          companyName={content._source.original_company_nm}
          mrp={(content._source.original_mrp * content.quantity).toFixed(2)}
          sp={
            content._source.original_product_code ===
            content._source.subs_product_code
              ? (content._source.subs_selling_price * content.quantity).toFixed(
                  2
                )
              : (
                  (content._source.original_mrp -
                    content._source.original_mrp *
                      (content._source.original_base_discount / 100)) *
                  content.quantity
                ).toFixed(2)
          }
          discount={
            content._source.original_product_code !==
            content._source.subs_product_code
              ? content._source.original_base_discount
              : Math.round(
                  ((content._source.original_mrp -
                    content._source.subs_selling_price) *
                    100) /
                    content._source.original_mrp
                )
          }
          quantity={content.quantity}
          rxRequired={content._source.original_rx_required}
          quantityHandleChange={(e) => props.quantityOnHandleChange(e, content)}
          onClickRemove={() => props.onClickRemove()}
          cartContents={props.cartContents}
          imgUpload={props.imgUpload}
          coldChainDisabled={content.coldChainDisabled}
          disabled={content.disabled}
          orderStatus={props.updateDetails?.orderStatus?.serialId}
        />
        {content._source.subs_found &&
          content._source.original_product_code !==
            content._source.subs_product_code && (
            <div className={"cartSubsMainContainer"}>
              <div className={"cartSubsRecommendation"}>
                <div className={"cartSubsRecommendation__wrap"}>
                  <div className={"cartSubsImgWrap"}>
                    <img src={subsSavingsPercent} />
                  </div>
                  <div>
                    <div className={"savingsSubs"}>
                      {"Get " +
                        customNumber(
                          content._source.savings_percentage.replace("%", "")
                        ).toFixed(0) +
                        "% Savings"}
                    </div>
                    <div className={"savingsSubsTitle"}>
                      Buy Truemeds Recommended Brands
                    </div>
                  </div>
                </div>
              </div>
              <Accordion
                square
                defaultExpanded={true}
                style={{ background: "#edf6f3", margin: "0px" }}
              >
                <AccordionSummary
                  className={"accordianHeaderHeight"}
                  expandIcon={
                    <img
                      src={subsDropdown}
                      style={{
                        width: "20px",
                        transform: "rotate( 180deg)",
                      }}
                    />
                  }
                  aria-controls="panel1a-content"
                  id="panel1a-header"
                  onClick={() => {
                    // Event_Truemeds
                    // new
                    // eventSubstituteViewed();
                  }}
                ></AccordionSummary>
                <div className="cartSubMedCardWrap">
                  <MedicineCardNew
                    isCart={props.isCart}
                    goToDetails={() => {
                      let medId = (
                        "/medicine/" +
                        convertProductTitle(
                          content?._source?.subs_sku_name,
                          content?._source?.subs_product_code
                        )
                      ).slice(10);

                      window.openSideBar(true, 10, {
                        medId: medId,
                        subsDetails: {
                          isSubs: true,
                          subsMrp: (
                            content._source.subs_mrp *
                            content?._source?.sub_recommended_qty
                          ).toFixed(2),
                          subsSp: content._source.subs_selling_price.toFixed(2),
                          subsDiscount: (
                            ((content._source.subs_mrp *
                              content?._source?.sub_recommended_qty -
                              content._source.subs_selling_price) *
                              100) /
                            (content._source.subs_mrp *
                              content?._source?.sub_recommended_qty)
                          ).toFixed(2),
                        },
                      });
                    }}
                    cartContents={props.cartContents}
                    imgUpload={props.imgUpload}
                    productCode={content?._source?.subs_product_code}
                    getAddress={() => {
                      props.getAddress();
                    }}
                    isInfo={props.selectedInfo}
                    closeToolTip={() => props.closeToolTip()}
                    companyAddress={
                      props.productDetails?.allProductDetails?.MedicineDetails
                        ?.companyAddress
                    }
                    viewDetails={true}
                    quantity={
                      content?._source?.sub_recommended_qty * content.quantity
                    }
                    companyOrigin={content?._source?.subs_country_nm}
                    medName={content._source.subs_sku_name}
                    packSize={content._source.subs_pack}
                    packUnits={content._source.subs_unit}
                    companyName={content._source.subs_company_nm}
                    mrp={
                      content._source?.sub_recommended_qty
                        ? (
                            content._source?.subs_mrp *
                            content._source?.sub_recommended_qty *
                            content.quantity
                          ).toFixed(2)
                        : content._source?.subs_mrp * content.quantity
                    }
                    sp={(
                      content._source.subs_selling_price * content.quantity
                    ).toFixed(2)}
                    discount={(
                      ((content._source.subs_mrp *
                        content._source.sub_recommended_qty -
                        content._source.subs_selling_price) *
                        100) /
                      (content._source.subs_mrp *
                        content._source.sub_recommended_qty)
                    ).toFixed(2)}
                    rxRequired={content._source.subs_rx_required}
                    isSubs={true}
                    coldChainDisabled={content.coldChainDisabled}
                    disabled={content.disabled}
                    orderStatus={props.updateDetails?.orderStatus?.serialId}
                  />
                </div>
              </Accordion>
            </div>
          )}
        {props.cartContents &&
        props.cartContents.length - 1 === props.index ? null : (
          <hr className={"medCard_hr"} />
        )}
      </div>
    );
  }
};

export default MedicineCardAccordian;

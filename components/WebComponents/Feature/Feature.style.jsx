import styled from "styled-components";

export const FeatureContainer = styled.div`
  margin-top: 10rem;
  @media screen and (max-width: 968px) {
    margin-top: 0rem;
    margin-bottom: 2rem;
  }
`;

export const FeatureWrapper = styled.div`
  position: relative;
  background: linear-gradient(180deg, #c9e8fe 0%, rgba(110, 193, 253, 0) 100%);
  border-radius: 10px;

  box-sizing: border-box;
  width: 70%;
  margin: 0 auto;
  padding: 2rem;
  @media screen and (max-width: 1068px) {
    width: 85%;
  }
  @media screen and (max-width: 968px) {
    background: none;
    width: 100%;
    padding: 0rem;
  }
`;

export const FeatureList = styled.div`
  > div[class="list-wrapper"] {
    padding: 10px 20px;
    border-radius: 10px;
    border-top: 1px solid #c9e8fe;
    border-left: 1px solid #c9e8fe;
    border-right: 1px solid #c9e8fe;
    border-image: linear-gradient(180deg, #0071bc, #c9e8fe) 1 stretch;

    > div[class="list-item"] {
      width: calc(100% - 330px);
      margin: 10px 0;
      > h5 {
        /* font-family: "Raleway", sans-serif; */
        font-style: normal;
        font-weight: 600;
        font-size: 19px;
        display: flex;
        flex-direction: column;

        color: #333333;
      }
      > p {
        /* font-family: "Raleway", sans-serif; */
        font-style: normal;
        font-weight: normal;
        font-size: 16px;

        color: #333333;
      }
    }
    > div[class="list-item"]:nth-child(4) {
      width: 90%;
    }
    > div[class="list-item"]:nth-child(5) {
      width: 90%;
    }
    @media screen and (max-width: 1200px) {
      > div[class="list-item"]:nth-child(3) {
        width: 90%;
      }
    }

    > h3 {
      /* font-family: "Raleway", sans-serif; */
      font-style: normal;
      font-weight: bold;
      font-size: 25px;
      line-height: 37px;
      margin-top: 1rem;
      color: #003055;
    }
    @media screen and (max-width: 968px) {
      > h3 {
        display: none;
      }
      width: 100%;
      padding: 0;
      background: #ffffff;
    }
  }
  @media screen and (max-width: 968px) {
    display: none;
  }
`;

export const Contact = styled.div`
  position: absolute;
  top: -8rem;
  right: 3rem;
  box-sizing: border-box;
  width: 290px;
  height: 360px;
  background: #ffffff;
  box-shadow: 0px 20px 50px rgba(87, 100, 173, 0.22);
  border-radius: 10px;
  padding: 1rem;
  margin: 1rem 0;
  display: flex;
  flex-flow: column;
  justify-content: center;
  @media screen and (max-width: 968px) {
    position: relative;
    top: 0rem;
    right: 0rem;
    margin: 2rem auto;
  }
  @media screen and (max-width: 480px) {
    width: 80vw;
    height: auto;
    padding: 2rem 1rem;
  }
  > div[class="feature-heading"] {
    > header {
      /* font-family: "Century Gothic", sans-serif; */
      font-style: normal;
      font-weight: bold;
      font-size: 22px;
      color: #003055;
      @media screen and (max-width: 480px) {
        font-size: 16px;
      }
    }
    > p {
      margin: 10px 0;
      font-style: normal;
      font-weight: normal;
      font-size: 16px;
      color: #333333;
      @media screen and (max-width: 480px) {
        font-size: 12px;
      }
    }
  }
  > div[class="feature-info"] {
    display: flex;
    flex-flow: column;
    .feature-item {
      margin: 10px 0;
      @media screen and (max-width: 480px) {
        margin: 5px 0;
      }
      > img {
        width: 30px;
        height: 30px;
        margin-right: 10px;
        @media screen and (max-width: 480px) {
          width: 15px;
          height: 15px;
          margin-right: 5px;
        }
      }
      a {
        /* font-family: "Century Gothic", sans-serif; */
        font-style: normal;
        font-weight: bold;
        font-size: 16px;
        text-align: center;
        color: #0071bc;
        @media screen and (max-width: 480px) {
          font-size: 12px;
        }
      }
      span {
        /* font-family: "Century Gothic", sans-serif; */
        font-style: normal;
        font-weight: bold;
        font-size: 16px;
        text-align: center;
        color: #0071bc;
        @media screen and (max-width: 480px) {
          font-size: 12px;
        }
      }
    }
  }
`;

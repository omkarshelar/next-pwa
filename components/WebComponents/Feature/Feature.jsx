import React, { Component } from "react";
import call from "./call.svg";
import mail from "./mail.svg";
import {
  FeatureWrapper,
  FeatureList,
  Contact,
  FeatureContainer,
} from "./Feature.style";
import {
  eventHomepageEmail,
  eventHomepageCallDetails,
} from "../../../src/Events/Events";

export class Feature extends Component {
  render() {
    return (
      <FeatureContainer>
        <FeatureWrapper>
          <FeatureList>
            <div className="list-wrapper">
              <h3>Why Truemeds?</h3>
              <div className="list-item">
                <h5>We’ve got your health and your budget covered!</h5>
                <p>
                  That’s right. With every medicine order, you can talk to our
                  doctors for free. They’ll help you opt for the right quality
                  alternative to bring down your medicine bill by up to 72%.
                </p>
              </div>
              <div className="list-item">
                <h5>Only the best for you. </h5>
                <p>
                  When you order from Truemeds, you choose to buy medicines only
                  from the top 30 medicine makers of India. We have curated only
                  the best for you.
                </p>
              </div>
              <div className="list-item">
                <h5>Medicines at your doorstep in just a tap</h5>
                <p>
                  With increasingly busy schedules, you may often forget to take
                  out the time to buy your medicines. We’ve got you covered.
                  Just tell us the medicines you need and we’ll have them
                  delivered to your doorstep for free.* (*Free delivery on
                  orders above ₹500)
                </p>
              </div>
              <div className="list-item">
                <h5>Helping you choose the right medicine</h5>
                <p>
                  To ensure you’re choosing the best medicine for your health,
                  our doctors will stay connected with you. Learn about the
                  different quality alternatives available to help you fight
                  your ailment without burning a whole in your pocket.
                </p>
              </div>
            </div>
          </FeatureList>
          <Contact>
            <div className="feature-heading">
              <header>Get in touch with us</header>
              <p>
                Our customer representative team is available 7 days a week from
                9 am - 9 pm. You can reach us on
              </p>
            </div>
            <div className="feature-info">
              {/* <span className="feature-item">
                <img src={message} />
                <span>Live Chat</span>
              </span> */}
              <span className="feature-item">
                <img src={mail} alt="mail" />
                <a
                  href="https://mail.google.com/mail/?view=cm&fs=1&to=support@truemeds.in"
                  target="_blank"
                  onClick={() => eventHomepageEmail()}
                >
                  support@truemeds.in
                </a>
              </span>
              <span className="feature-item">
                <img src={call} alt="call" />
                <a
                  href="tel:022-48977965"
                  onClick={() => eventHomepageCallDetails()}
                >
                  <span>022-48977965</span>
                </a>
              </span>
            </div>
          </Contact>
        </FeatureWrapper>
      </FeatureContainer>
    );
  }
}

export default Feature;

import styled, { css } from "styled-components";

const onHoverStyle = css`
  :hover {
    color: white;
  }
`;

const OTPUserRegisterStyle = css`
  background-color: #0071bc;
  color: white;
  margin: 1% 0 2% 0;
  border-radius: 10px;
  ${onHoverStyle};
  @media screen and (max-width: 468px) {
    height: 30;
    font-size: smaller;
  }
`;

const OTPUserVerifyStyle = css`
  background-color: #0071bc;
  color: white;
  margin: 1% 0 1% 0;
  width: 100px;
  ${onHoverStyle}
  @media screen and (max-width: 468px) {
    margin: 2% 0 2% 0;
  }
`;

const AddToCartStyle = css`
  background-color: #0071bc;
  color: white;
  border-radius: 20px;
  align-self: flex-end;
  width: 170px;
  ${onHoverStyle}
  @media screen and (max-width: 468px) {
    width: 140px;
    font-size: smaller;
  }
`;

const ProceedStyle = css`
  background-color: #0071bc;
  color: white;
  border-radius: 5px;
  width: 100%;
  ${onHoverStyle}
  @media screen and (max-width: 468px) {
    font-size: small;
    height: 2.4rem;
    font-weight: 600;
  }
`;

const confirmMedStyle = css`
  background-color: #0071bc;
  color: white;
  border-radius: 10px;
  ${onHoverStyle}
`;

const addressContStyle = css`
  background-color: #0071bc;
  color: white;
  border-radius: 5px;
  font-weight: 600;
  ${onHoverStyle}
  width:50%;
  @media screen and (max-width: 468px) {
    width: 100%;
  }
`;

const paymentStyle = css`
  color: white;
  border: 2px #0071bc solid;
  background-color: #0071bc;
  border-radius: 10px;
  width: 170px;
  margin: 5px;
  @media screen and (max-width: 468px) {
    width: 150px;
  }
  :hover {
    background-color: #0071bc;
    color: white;
  }
`;

const tncStyle = css`
  color: white;
  width: 170px;
  border-radius: 10px;
  margin: 5px;
  background-color: #0071bc;
  ${onHoverStyle}
`;

const imgUpload = css`
  color: white;
  width: 130px;
  border-radius: 7px;
  margin: 3px;
  background-color: #0071bc;
  ${onHoverStyle}
`;

const getStatusStyle = css`
  color: white;
  border-radius: 7px;
  margin: 5px;
  background-color: #0071bc;
  ${onHoverStyle}
`;

const vieworderStyle = css`
  background-color: #0071bc;
  color: white;
  border-radius: 5px;
  margin: 5px;
  @media screen and (max-width: 468px) {
    width: 48%;
    font-size: small;
    margin: 0px;
  }
  ${onHoverStyle};
`;

const discardStyle = css`
  color: #0071bc;
  border: 2px #0071bc solid;
  border-radius: 5px;
  width: 170px;
  margin: 8px;
  @media screen and (max-width: 468px) {
    width: 48%;
    font-size: small;
    margin: 0px;
  }
  :hover {
    background-color: #0071bc;
    color: white;
  }
  ${onHoverStyle}
`;

const paynowmyorderStyle = css`
  border: 1px #1cb976 solid;
  background-color: #1cb976;
  margin: 10px 5px 5px 5px;
  border-radius: 8px;
  color: white;
  padding: 0 20px;
  @media screen and (max-width: 468px) {
    font-size: smaller;
    padding: 0 15px;
  }
`;
const viewmyorderStyle = css`
  background-color: #0071bc;
  border: 1px #0071bc solid;
  margin: 10px 5px 5px 5px;
  border-radius: 8px;
  color: white;

  @media screen and (max-width: 468px) {
    font-size: smaller;
  }
`;

const viewDetailStyle = css`
  color: white;
  background-color: #1cb976;
  border: 1px #1cb976 solid;
  border-radius: 8px;
  width: 170px;
  margin: 10px 5px 5px 5px;
  @media screen and (max-width: 468px) {
    width: 130px;
    margin: 5px 2px 2px 2px;
    font-size: smaller;
  }
`;

const viewDetailForMyOrderStyle = css`
  color: white;
  background-color: #0071bc;
  border: 1px #0071bc solid;
  border-radius: 8px;
  width: 170px;
  margin: 10px 5px 5px 5px;
  @media screen and (max-width: 468px) {
    width: 130px;
    margin: 5px 2px 2px 2px;
    font-size: smaller;
  }
`;

const delYesStyle = css`
  color: #0071bc;
  border-radius: 7px;
  border: 1px #0071bc solid;
  width: 100px;
  margin: 5px;
  :hover {
    background-color: #0071bc;
    color: white;
  }
`;

const delNoStyle = css`
  color: #18b574;
  border-radius: 7px;
  border: 1px #18b574 solid;
  width: 100px;
  margin: 5px;
  :hover {
    background-color: #18b574;
    color: white;
  }
`;
const HelpStyle = css`
  color: #18b574;
  border-radius: 7px;
  border: 1px #18b574 solid;
  margin: 5px;
  :hover {
    background-color: #18b574;
    color: white;
  }
`;

const withPresStyle = css`
  border: 1px #0071bc solid;
  font-weight: 500;
  border-radius: 10px;
  margin: 8px;
  @media screen and (max-width: 468px) {
    font-size: smaller;
  }
`;
const woPresStyle = css`
  border: 1px #0071bc solid;
  background-color: #0071bc;
  font-weight: 500;
  color: white;
  margin: 8px;
  border-radius: 10px;
  @media screen and (max-width: 468px) {
    font-size: smaller;
  }
`;

const amyesStyle = css`
  border: 1px #0071bc solid;
  width: 150px;
  font-weight: 500;
  margin: 10px;
  border-radius: 10px;
`;

const amnoStyle = css`
  border: 1px #0071bc solid;
  background-color: #0071bc;
  font-weight: 500;
  color: white;
  width: 150px;
  margin: 10px;
  border-radius: 10px;
`;

const cancelSubmitStyle = css`
  border: 1px #0071bc solid;
  background-color: #0071bc;
  font-weight: 500;
  color: white;
  width: 150px;
  margin: 7px;
  border-radius: 10px;
`;

const cancelCloseStyle = css`
  border: 1px #1cb976 solid;
  background-color: #1cb976;
  font-weight: 500;
  color: white;
  width: 150px;
  margin: 7px;
  border-radius: 10px;
`;

const downNowStyle = css`
  border: 1px #1cb976 solid;
  background-color: #1cb976;
  font-weight: 500;
  color: white;
  margin: 5px;
  @media screen and (max-width: 468px) {
    font-size: smaller;
  }
`;
const paynowStyle = css`
  border: 1px #1cb976 solid;
  background-color: #1cb976;
  color: white;
`;

const healthStyle = css`
  border: 1px #1cb976 solid;
  margin: 10px;
  :hover {
    border: 1px #1cb976 solid;
    background-color: #1cb976;
    color: white;
  }
`;
const redeemStyle = css`
  border: 2px #1cb976 solid;
  color: #1cb976;
  font-weight: 700;
  font-size: large;
  border-radius: 10px;
  margin-top: 1rem;
  :hover {
    background-color: #1cb976;
    color: white;
  }
  width: 200px;
  @media screen and (max-width: 468px) {
    width: 100%;
  }
`;

const withBorderstyle = css`
  border: 1px #0071bc solid;
  color: #0071bc;
  border-radius: 10px;
  margin: 5px;
  @media screen and (max-width: 468px) {
    font-size: small;
    height: 2.5rem;
    width: 150px;
    font-weight: 600;
  }
`;
const woBorderstyle = css`
  border: 1px #0071bc solid;
  background-color: #0071bc;
  color: white;
  border-radius: 10px;
  margin: 5px;
  @media screen and (max-width: 468px) {
    font-size: small;
    width: 150px;
    height: 2.5rem;
    font-weight: 600;
  }
`;

const patientDetBtnstyle = css`
  margin: 5px;
  font-weight: 600;
`;
const SubmitIssueStyle = css`
  background-color: #1a73e8;
  color: white;
  text-align: center;
  width: 100%;
  :hover {
    color: white;
  }
`;

const GetButtonStyles = (props) => {
  if (props.withBorder) {
    return withBorderstyle;
  }
  if (props.cancelClose) {
    return cancelCloseStyle;
  }
  if (props.cancelSubmit) {
    return cancelSubmitStyle;
  }
  if (props.patientDetBtn) {
    return patientDetBtnstyle;
  }
  if (props.woBorder) {
    return woBorderstyle;
  }
  if (props.userRegi) {
    return OTPUserRegisterStyle;
  }
  if (props.userVeri) {
    return OTPUserVerifyStyle;
  }
  if (props.addToCart) {
    return AddToCartStyle;
  }
  if (props.proceedProp) {
    return ProceedStyle;
  }
  if (props.confirmMed) {
    return confirmMedStyle;
  }
  if (props.addressCont) {
    return addressContStyle;
  }

  if (props.payment) {
    return paymentStyle;
  }
  if (props.tnc) {
    return tncStyle;
  }
  if (props.discard) {
    return discardStyle;
  }
  if (props.getStatus) {
    return getStatusStyle;
  }
  if (props.imgUpload) {
    return imgUpload;
  }
  if (props.delYes) {
    return delYesStyle;
  }
  if (props.delNo) {
    return delNoStyle;
  }
  if (props.viewDetail) {
    return viewDetailStyle;
  }
  if (props.help) {
    return HelpStyle;
  }
  if (props.wp) {
    return withPresStyle;
  }
  if (props.wo) {
    return woPresStyle;
  }
  if (props.downNow) {
    return downNowStyle;
  }
  if (props.amyes) {
    return amyesStyle;
  }
  if (props.amno) {
    return amnoStyle;
  }
  if (props.vieworder) {
    return vieworderStyle;
  }
  if (props.paynow) {
    return paynowStyle;
  }
  if (props.paynowmyorder) {
    return paynowmyorderStyle;
  }
  if (props.viewmyorder) {
    return viewmyorderStyle;
  }
  if (props.redeem) {
    return redeemStyle;
  }
  if (props.health) {
    return healthStyle;
  }
  if (props.SubmitIssue) {
    return SubmitIssueStyle;
  }
  if (props.viewDetailForMyOrder) {
    return viewDetailForMyOrderStyle;
  }
};

export const CustomButtonContainer = styled.button`
  ${GetButtonStyles};
`;

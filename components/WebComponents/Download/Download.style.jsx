import styled from "styled-components";

// Download App Badge

export const Container = styled.div`
  transform: translateY(-9rem);
  @media screen and (max-width: 968px) {
    display: none;
  }
`;
export const DownloadWrapper = styled.div`
  position: relative;
  width: 70%;
  display: flex;
  justify-content: flex-end;
  align-items: center;
  margin: 0 auto;
  height: 297px;
  background: linear-gradient(260.21deg, #22b573 -10.73%, #1e76ba 76.09%);
  border-radius: 206.5px;
  @media screen and (max-width: 1268px) {
    width: 90%;
  }
  @media screen and (max-width: 968px) {
    transition: border-radius 1s ease;
    border-radius: 0;
    width: 100%;
  }
  /* Mobile View */
  @media screen and (max-width: 768px) {
    flex-flow: column;
    justify-content: center;
    height: auto;
  }
`;

export const BadgeImage = styled.div`
  width: 280px;
  position: absolute;
  bottom: 0;
  left: 9.1rem;
  > img {
    width: 100%;
    height: 408px;
  }
  @media screen and (max-width: 868px) {
    left: 4rem;
  }

  /* Mobile view */

  @media screen and (max-width: 768px) {
    left: 0;
    right: 0;
    bottom: 50%;
    margin-left: auto;
    margin-right: auto;
  }
`;
export const Link = styled.div`
  width: 50%;
  box-sizing: border-box;
  display: flex;
  flex-flow: column;
  padding: 30px;
  > a > img {
    width: 200px;
  }
  > h2 {
    /* font-family: "Century Gothic", sans-serif; */
    font-size: 40px;
    font-style: normal;
    font-weight: 700;
    letter-spacing: 1px;
    text-align: left;
    color: #ffffff;
  }
  > p {
    /* font-family: "Raleway", sans-serif; */
    font-size: 16px;
    font-style: normal;
    font-weight: 400;
    line-height: 21px;
    letter-spacing: 0em;
    text-align: left;
    color: #ffffff;
  }
  /* Mobile view */

  @media screen and (max-width: 768px) {
    margin-top: 15rem;
    width: 100%;
    > h2,
    p {
      text-align: center;
    }
    > img {
      align-self: center;
    }
    > a {
      text-align: center;
    }
  }
  @media screen and (max-width: 468px) {
    margin-top: 19rem;
  }
  @media screen and (max-width: 368px) {
    margin-top: 20rem;
  }
`;

import styled, { css } from "styled-components";

// Sidepanel container

export const SidePanelContainer = css`
  position: fixed;
  bottom: 0;
  padding: 16px;
`;

// Default

const Default = css`
  border-radius: 154px;
  font-weight: 600;
  font-style: normal;
  text-align: center;
  box-sizing: border-box;
  :hover {
    color: #0071bc;
  }
`;

// Blue Background
const Primary = css`
  border: 1px solid #0071bc;
  background-color: #e5f1f8;
  :hover {
    color: #0071bc;
  }
  ${Default}
`;

// White Background
const Secondary = css`
  border: 1px solid #0071bc;
  background-color: #ffffff;
  :hover {
    color: #0071bc;
  }
`;

const ButtonNav = css`
  font-size: 12px;
  line-height: 14px;
  letter-spacing: 0.02em;
  text-transform: capitalize;
  height: 37px;
  :hover {
    color: #0071bc;
  }
  ${Primary}
`;

const ButtonContinue = css`
  font-size: 12px;
  line-height: 14px;
  letter-spacing: 0.02em;
  text-transform: capitalize;
  width: 100%;
  padding: 12px 0;
  margin: 10px 0;
  color: #0071bc;
  display: flex;
  align-items: center;
  justify-content: center;
  :hover {
    color: #0071bc;
  }
  @media screen and (max-width: 968px) {
    width: 100%;
  }
  ${Primary};
`;

const ButtonAdd = css`
  font-size: 12px;
  line-height: 14px;
  letter-spacing: 0.02em;
  text-transform: capitalize;
  width: 100%;
  padding: 12px 0;
  margin: 10px 5px 10px 0;
  color: #0071bc;
  ${Default};
  ${Secondary};
  :hover {
    color: #0071bc;
  }
`;

const ButtonHistory = css`
  min-width: 150px;
  overflow: hidden;
  white-space: nowrap;
  display: block;
  text-overflow: ellipsis;
  margin: 0 5px;
  border: 1px solid rgba(0, 113, 188, 0.1);
  box-sizing: border-box;
  border-radius: 4px;
  color: #0071bc;
  /* font-family: "Century Gothic", sans-serif; */
  font-size: 14px;
  font-style: normal;
  font-weight: 600;
  letter-spacing: 0.02em;
  :hover {
    color: #0071bc;
  }
`;

const ButtonCartAdd = css`
  font-weight: 600;
  font-size: 16px;
  background-color: #0071bc;
  border-radius: 6px;
  color: #fff;
  width: 154px;
  height: 44px;
  display: flex;
  align-items: center;
  justify-content: center;
  :hover {
    color: #fff;
  }

  .plusIcon {
    font-size: 25px;
  }
  @media screen and (max-width: 468px) {
    font-size: 14px;
    width: 120px;
    /* height: 34px; */
  }
`;

const ButtonProceed = css`
  width: 100%;
  font-weight: 600;
  font-size: 16px;
  background-color: #0071bc;
  border-radius: 6px;
  color: #fff;
  padding: 11px 15px;
  display: flex;
  align-items: center;
  justify-content: center;
  :hover {
    color: #fff;
  }
`;

const ButtonCartView = css`
  display: flex;
  align-items: center;
  justify-content: center;
  font-weight: 600;
  font-size: 16px;
  background-color: #0071bc;
  border-radius: 6px;
  color: #fff;
  width: 100%;
  :hover {
    color: #fff;
  }
`;

const ButtonUpload = css`
  display: flex;
  align-items: center;
  justify-content: center;
  font-weight: 600;
  font-size: 16px;
  background-color: #22b573;
  border-radius: 6px;
  color: #fff;
  :hover {
    color: #fff;
  }
`;

// const ButtonCartAdd = css`
//   border: 1px solid #0071bc;
//   box-sizing: border-box;
//   border-radius: 154px;
//   font-family: "Raleway", sans-serif;
//   font-size: 14px;
//   font-style: normal;
//   font-weight: 600;
//   letter-spacing: 0.02em;
//   text-align: center;
//   height: 35px;
//   color: #0071bc;
//   :hover {
//     color: #0071bc;
//   }
//   > i {
//     padding-right: 10px;
//   }
//   @media screen and (max-width: 468px) {
//     font-size: small;
//   }
// `;

const ButtonProductAdd = css`
  border: 1px solid #0071bc;
  box-sizing: border-box;
  border-radius: 154px;
  /* font-family: "Raleway", sans-serif; */
  font-size: 14px;
  font-style: normal;
  font-weight: 600;
  letter-spacing: 0.02em;
  text-align: center;
  height: 35px;
  color: #0071bc;
  width: 100%;
  :hover {
    color: #0071bc;
  }
  display: flex;
  align-items: center;
  justify-content: center;
  > i {
    padding-right: 10px;
  }
  @media screen and (max-width: 468px) {
    font-size: small;
  }
`;

const featureBtn = css`
  font-size: 19px;
  font-style: normal;
  font-weight: 600;
  line-height: 25px;
  letter-spacing: 0em;
  text-align: center;

  color: #0071bc;
  :hover {
    color: #0071bc;
  }
  @media screen and (max-width: 468px) {
    font-size: 16px;
  }
`;

const ButtonBanner = css`
  font-size: 12px;
  line-height: 14px;
  letter-spacing: 0.02em;
  text-transform: capitalize;
  width: 100%;
  padding: 12px 0;
  ${Primary};
  color: #0071bc;
  :hover {
    color: #0071bc;
  }
  @media screen and (max-width: 768px) {
    width: 96%;
  }
  @media screen and (max-width: 568px) {
    width: 80%;
  }
  @media screen and (max-width: 468px) {
    width: 50%;
    font-size: x-small;
    padding: 10px;
    background-color: transparent;
  }
`;

const seeMoreBtn = css`
  background: #e5f1f8;
  border: 1px solid #0071bc;
  box-sizing: border-box;
  border-radius: 154px;
  width: 241px;
  height: 45px;
  color: #0071bc;
  font-size: 14px;
  font-style: normal;
  font-weight: 600;
  line-height: 16px;
  letter-spacing: 0.02em;
  text-align: center;
  margin: 10px 10px;
  :hover {
    color: #0071bc;
  }
  @media screen and (max-width: 568px) {
    width: 200px;
    font-size: 12px;
    height: 40px;
  }
`;

const resendStyle = css`
  /* font-family: "Century Gothic", sans-serif; */
  font-style: normal;
  font-weight: 600;
  font-size: 16px;
  color: #003055;
  :hover {
    color: #003055;
  }
`;

const onpayment = css`
  background-color: #003055;
  color: white;
  background-color: #e5f1f8;
  color: #0071bc;
  border: 1px solid #0071bc;
  margin: 10px;
  border-radius: 2px;
  width: 150px;
  /* box-shadow: rgba(0, 0, 0, 0.12) 0px 1px 3px, rgba(0, 0, 0, 0.24) 0px 1px 2px; */
  @media screen and (max-width: 468px) {
    font-size: smaller;
  }
  border-radius: 154px;
  :hover {
    color: #0071bc;
  }
`;
const uploadPres = css`
  border: 1px solid #0071bc;
  font-size: 12px;
  line-height: 14px;
  letter-spacing: 0.02em;
  text-transform: capitalize;
  height: 37px;
  border-radius: 154px;
  font-weight: 600;
  font-style: normal;
  text-align: center;
  box-sizing: border-box;
  :hover {
    color: #0071bc;
  }
`;

const viewDetails = css`
  background: #ffffff;
  border: 1px solid #0071bd;
  border-radius: 4px;
  display: flex;
  align-items: center;
  justify-content: center;
  cursor: pointer;
  width: 210px;
  height: 48px;
  font-weight: 600;
  font-size: 16px;
  color: #0071bd;
  :hover {
    color: #0071bd;
  }
  @media screen and (max-width: 768px) {
    display: none;
  }
`;

const reorder = css`
  width: 174px;
  height: 48px;
  background: #0071bd;
  border-radius: 4px;
  display: flex;
  align-items: center;
  justify-content: center;
  cursor: pointer;
  font-weight: 600;
  font-size: 16px;
  color: #fff;
  margin-left: 24px;
  :hover {
    color: #fff;
  }
  @media screen and (max-width: 768px) {
    width: auto;
    height: auto;
    padding: 8px 18px;
    font-size: 14px;
  }
`;

const ButtonLogin = css`
  font-size: 12px;
  line-height: 14px;
  letter-spacing: 0.02em;
  text-transform: capitalize;
  width: 120px;
  padding: 12px 0;
  margin: 10px 5px;
  color: #0071bc;
  display: flex;
  align-items: center;
  justify-content: center;
  :hover {
    color: #0071bc;
  }

  ${Primary};
`;

const ButtonBack = css`
  font-size: 12px;
  line-height: 14px;
  letter-spacing: 0.02em;
  text-transform: capitalize;
  width: 120px;
  padding: 12px 0;
  margin: 10px 5px;
  color: #0071bc;
  display: flex;
  align-items: center;
  justify-content: center;
  :hover {
    color: #0071bc;
  }
  border-radius: 154px;
  border: 1px solid #0071bc;
  background-color: white;
`;

const btnSidebarStyle = css`
  line-height: 14px;
  letter-spacing: 0.02em;
  text-transform: capitalize;
  background-image: linear-gradient(
    to right top,
    #2578b4,
    #3583be,
    #438fc7,
    #509ad1,
    #5da6db
  );
  font-size: 12px;
  padding: 10px 12px;
  margin: 10px;
  display: flex;
  align-items: center;
  justify-content: center;
  color: #fff;
  :hover {
    color: #fff;
  }
`;

const reOrderDetails = css`
  background: #0071bd;
  border: 1px solid #0071bd;
  box-sizing: border-box;
  border-radius: 4px;
  display: flex;
  flex-direction: row;
  justify-content: center;
  align-items: center;
  padding: 12px;
  font-style: normal;
  font-weight: 600;
  font-size: 16px;
  line-height: 24px;
  color: #ffffff;
  margin-top: 24px;
  :hover {
    color: #fff;
  }

  @media screen and (max-width: 768px) {
    width: 100%;
    font-size: 14px;
    padding: 8px;
    margin: 0;
  }
`;

const GetButtonStyles = (props) => {
  if (props.uploadPres) {
    return uploadPres;
  }
  if (props.btnback) {
    return ButtonBack;
  }

  if (props.btnSidebar) {
    return btnSidebarStyle;
  }
  if (props.Buttonlogin) {
    return ButtonLogin;
  }
  if (props.BtnNav) {
    return ButtonNav;
  }
  if (props.BtnContinue) {
    return ButtonContinue;
  }
  if (props.BtnHistory) {
    return ButtonHistory;
  }
  if (props.BtnAdd) {
    return ButtonAdd;
  }
  if (props.CartAdd) {
    return ButtonCartAdd;
  }
  if (props.proceedBtn) {
    return ButtonProceed;
  }
  if (props.CartView) {
    return ButtonCartView;
  }
  if (props.upload) {
    return ButtonUpload;
  }
  if (props.ProductAdd) {
    return ButtonProductAdd;
  }
  if (props.bannerBtn) {
    return ButtonBanner;
  }
  if (props.feature) {
    return featureBtn;
  }
  if (props.seeMore) {
    return seeMoreBtn;
  }
  if (props.resend) {
    return resendStyle;
  }
  if (props.makepayment) {
    return onpayment;
  }
  if (props.viewDetails) {
    return viewDetails;
  }
  if (props.reorder) {
    return reorder;
  }
  if (props.reOrderDetails) {
    return reOrderDetails;
  }
};

export const CustomButtonContainer = styled.button`
  ${GetButtonStyles} white-space: nowrap;

  > .loaderImg {
    width: 22px;
    margin-right: 5px;
  }
`;

import React, { Component } from "react";
import Shimmer from "react-shimmer-effect";
import "./CardArticleLoader.css";

export class CardArticleLoader extends Component {
  render() {
    return (
      <div className="blank-med-loader-card-container">
        <div className="blank-med-loader-card-from-article-wrapper">
          <Shimmer>
            <div className="blank-med-loader-card-from-article-image"></div>
          </Shimmer>
          <div className="blank-med-loader-card-from-article-line-wrapper">
            <Shimmer>
              <div className="blank-med-loader-card-from-article-line"></div>
              <div className="blank-med-loader-card-from-article-line"></div>
            </Shimmer>
          </div>
          <div className="blank-med-loader-card-from-article-author">
            <Shimmer>
              <div className="blank-med-loader-card-from-article-line"></div>
            </Shimmer>
          </div>
        </div>
        <div className="blank-med-loader-card-from-article-wrapper">
          <Shimmer>
            <div className="blank-med-loader-card-from-article-image"></div>
          </Shimmer>
          <div className="blank-med-loader-card-from-article-line-wrapper">
            <Shimmer>
              <div className="blank-med-loader-card-from-article-line"></div>
              <div className="blank-med-loader-card-from-article-line"></div>
            </Shimmer>
          </div>
          <div className="blank-med-loader-card-from-article-author">
            <Shimmer>
              <div className="blank-med-loader-card-from-article-line"></div>
            </Shimmer>
          </div>
        </div>
        <div className="blank-med-loader-card-from-article-wrapper">
          <Shimmer>
            <div className="blank-med-loader-card-from-article-image"></div>
          </Shimmer>
          <div className="blank-med-loader-card-from-article-line-wrapper">
            <Shimmer>
              <div className="blank-med-loader-card-from-article-line"></div>
              <div className="blank-med-loader-card-from-article-line"></div>
            </Shimmer>
          </div>
          <div className="blank-med-loader-card-from-article-author">
            <Shimmer>
              <div className="blank-med-loader-card-from-article-line"></div>
            </Shimmer>
          </div>
        </div>
      </div>
    );
  }
}

export default CardArticleLoader;

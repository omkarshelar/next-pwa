import React, { Component } from "react";
import Button from "../../WebComponents/Button/Button";
import ArticleCard from "./ArticleCard";
import { createRef } from "react";
import BlankCard from "../BlankCard/BlankCard";
import next from "../Upload/next.svg";
import prev from "../Upload/prev.svg";
import {
  ArticleWrapper,
  Heading,
  ArticleList,
  Container,
} from "./Articles.style";
import Router from "next/router";
import CardArticleLoader from "./CardArticleLoader";
import { eventHomepageReadArticles } from "../../../src/Events/Events";

export default class Articles extends Component {
  constructor(props) {
    super(props);
    this.ListRef = createRef();
  }
  scrollRight = () => {
    if (this.ListRef.current) {
      this.ListRef.current.scrollLeft = this.ListRef.current.scrollLeft + 500;
    }
  };
  scrollLeft = () => {
    if (this.ListRef.current) {
      this.ListRef.current.scrollLeft = this.ListRef.current.scrollLeft - 500;
    }
  };

  specificArticle = (data) => {
    Router.push(`/blog/${data.slug}`);
    // Event_Truemeds
    eventHomepageReadArticles();
  };
  render() {
    let { article } = this.props;
    return (
      <ArticleWrapper>
        <Container>
          <Heading>
            <h2>A Healthy Read</h2>
            <div className="vaWrap">
              <div
                className="vaText"
                onClick={() => {
                  Router.push("/blog");
                }}
              >
                <span>View All</span>
              </div>
              <div>
                <Button>
                  <img src={prev} onClick={this.scrollLeft} alt="prev"></img>
                </Button>
                <Button>
                  <img src={next} onClick={this.scrollRight} alt="next"></img>
                </Button>
              </div>
            </div>
          </Heading>
        </Container>
        <ArticleList ref={this.ListRef}>
          <BlankCard />
          {this.props.isLoading2 ? (
            <CardArticleLoader />
          ) : article ? (
            article.map((data) => (
              <ArticleCard
                data={data}
                specificArticle={this.specificArticle}
                isMobile={this.props.isMobile}
              />
            ))
          ) : (
            <CardArticleLoader />
          )}
        </ArticleList>
      </ArticleWrapper>
    );
  }
}

import React, { Component } from "react";
import { CardWrapper, Info } from "./Articles.style";
import optimizeImage from "../../Helper/OptimizeImage";
import moment from "moment";
import window from "global";
import Image from "next/image";

export class ArticleCard extends Component {
  render() {
    let { data, specificArticle } = this.props;
    return (
      <CardWrapper onClick={() => specificArticle(data)}>
        <div className="img-placeHolder">
          {data._embedded["wp:featuredmedia"]?.length > 0 ? (
            <div className="img-articleContainer">
              <Image
                src={data._embedded["wp:featuredmedia"][0].source_url}
                alt="article-cover"
                width={this.props.isMobile ? 150 : 250}
                height={this.props.isMobile ? 97 : 170}
              />
            </div>
          ) : (
            ""
          )}
        </div>
        <Info>
          <div className="first-row">
            <header>{data.name}</header>
            <p>{data._embedded.author[0].name}</p>
          </div>
          <div className="second-row">
            <p>{moment(data.date).format("Do MMM YYYY")}</p>
          </div>
          <div className="hover-text">
            <p>Read More &gt; &gt;</p>
          </div>
        </Info>
      </CardWrapper>
    );
  }
}

export default ArticleCard;

import React, { Component } from "react";
import "./CardFeatureLoader.css";
import Skeleton from "@mui/material/Skeleton";

export class CardFeatureLoader extends Component {
  render() {
    return (
      <div className="blank-med-loader-card-feature-container">
        <div className="blank-med-loader-card-feature-wrapper">
          <div className="blank-med-loader-card-feature-image">
            <Skeleton animation="wave" height={200} />
          </div>
          <div className="blank-med-loader-card-feature-info">
            <p className="blank-med-loader-card-feature-lineOne">
              <Skeleton animation="wave" height={30} />
            </p>
            <p className="blank-med-loader-card-feature-lineOne">
              <Skeleton animation="wave" height={30} />
            </p>
            <p className="blank-med-loader-card-feature-lineTwo">
              <Skeleton animation="wave" height={30} />
            </p>
            <p className="blank-med-loader-card-feature-lineTwo">
              <Skeleton animation="wave" height={30} />
            </p>
          </div>
        </div>
        <div className="blank-med-loader-card-feature-wrapper">
          <div className="blank-med-loader-card-feature-image">
            <Skeleton animation="wave" height={200} />
          </div>
          <div className="blank-med-loader-card-feature-info">
            <p className="blank-med-loader-card-feature-lineOne">
              <Skeleton animation="wave" height={30} />
            </p>
            <p className="blank-med-loader-card-feature-lineOne">
              <Skeleton animation="wave" height={30} />
            </p>
            <p className="blank-med-loader-card-feature-lineTwo">
              <Skeleton animation="wave" height={30} />
            </p>
            <p className="blank-med-loader-card-feature-lineTwo">
              <Skeleton animation="wave" height={30} />
            </p>
          </div>
        </div>
        <div className="blank-med-loader-card-feature-wrapper">
          <div className="blank-med-loader-card-feature-image">
            <Skeleton animation="wave" height={200} />
          </div>
          <div className="blank-med-loader-card-feature-info">
            <p className="blank-med-loader-card-feature-lineOne">
              <Skeleton animation="wave" height={30} />
            </p>
            <p className="blank-med-loader-card-feature-lineOne">
              <Skeleton animation="wave" height={30} />
            </p>
            <p className="blank-med-loader-card-feature-lineTwo">
              <Skeleton animation="wave" height={30} />
            </p>
            <p className="blank-med-loader-card-feature-lineTwo">
              <Skeleton animation="wave" height={30} />
            </p>
          </div>
        </div>
      </div>
    );
  }
}

CardFeatureLoader.defaultProps = {
  fromArticle: false,
};

export default CardFeatureLoader;

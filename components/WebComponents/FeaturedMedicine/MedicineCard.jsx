import React, { Component } from "react";
import { connect } from "react-redux";
import {
  addItemCartAction,
  removeItemCartAction,
  removeMedThunk,
} from "../../../redux/Cart/Action";
import {
  CardContainer,
  CardInfo,
  CarouselItemStyle,
  CarouseStyle,
  ItemAdded,
} from "./MedicineCard.style";
import next from "../../../src/Assets/next.svg";
import prev from "../../../src/Assets/prev.svg";
import removeIcon from "../../../src/Assets/remove.png";
import { withStyles } from "@material-ui/core/styles";
// import { Select } from "antd";
// import Select from "react-select";
/* import {
  eventHomepageCrossellingAdded,
  eventHomepageCrossellingRemoved,
} from "../../../Events/Events"; */
// import optimizeImage from "../../../WebComponent/Helpers/OptimizeImage";
// import { LazyLoadImage } from "react-lazy-load-image-component";
import { HiPlusSm } from "react-icons/hi";
import optimizeImage from "../../Helper/imageHelper";
// import window from "global";
import { qtyArr } from "../../../redux/util/CartUtil";
import Button from "../Button/Button";
import convertProductTitle from "../../Helper/convertProductTitle";
import {
  eventHomepageCrossellingAdded,
  eventHomepageCrossellingRemoved,
} from "../../../src/Events/Events";
import Image from "next/image";
import dynamic from "next/dynamic";

const Select = dynamic(() => import("react-select"), { ssr: false });
const { Option } = Select;

const styles = () => ({
  menuPaper: {
    maxHeight: 120,
  },
});

export class MedicineCard extends Component {
  state = {
    medData: [],
    rMedData: [],
    rContent: {},
    removeQuantity: false,
  };
  componentDidMount = () => {
    // window.closeSelect = function () {};
  };
  addNewQuantity = (obj, qty) => {
    obj.newQty = Number(qty);
    return obj;
  };
  removeMedOnClick = (data) => {
    if (this.props.medConfirm?.orderId) {
      this.props.removeMedThunk({
        orderId: this.props.medConfirm.orderId,
        access_token: this.props.accessToken,
        medData: data,
        history: this.props.history,
      });
    } else if (this.props.uploadImage?.orderId) {
      this.props.removeMedThunk({
        orderId: this.props.uploadImage.orderId,
        access_token: this.props.accessToken,
        medData: data,
        history: this.props.history,
      });
    }
  };
  quantityOnHandleChange = (values, content) => {
    let value = values.value;
    if (this.props.cartContent?.length > 0) {
      this.setState(
        {
          medData: this.props.cartContent.map((data) => {
            return {
              productCode: data.orgProductCd,
              quantity: data.orgQuantity,
            };
          }),
        },
        () => {
          this.props.addItemCartAction(this.addNewQuantity(content, value));
        }
      );
    }
  };

  setmaxQty = (arr, maxQty) => {
    let newArr = [];
    for (let i in arr) {
      if (arr[i] <= maxQty) {
        newArr.push(Number(arr[i]));
      }
    }
    return newArr;
  };

  render() {
    let { medicineName, companyName, mrp, images, content, productCode, key } =
      this.props;
    return (
      <div key={key}>
        <CardContainer>
          {images && (
            <div className="card-imag-featured">
              <CarouseStyle
                indicators={false}
                interval={null}
                next={next}
                prev={prev}
              >
                {images.map((content, i) => (
                  <CarouselItemStyle key={i}>
                    {/*  <LazyLoad
                    height={window.innerWidth > 467 ? 150 : 50}
                    offset={100}
                    scroll={true}
                    overflow
                    once
                  > */}
                    {/* <LazyLoadImage
                      src={optimizeImage(content, "150")}
                      alt="med"
                    /> */}
                    <Image
                      src={optimizeImage(
                        content,
                        this.props.isMobile ? "50" : "150"
                      )}
                      width={this.props.isMobile ? 50 : 150}
                      height={this.props.isMobile ? 50 : 150}
                      alt="med"
                      objectPosition={"50% 50%"}
                      objectFit={"contain"}
                    />
                    {/* </LazyLoad> */}
                  </CarouselItemStyle>
                ))}
              </CarouseStyle>
            </div>
          )}

          <CardInfo>
            <a
              href={
                "/medicine/" + convertProductTitle(medicineName, productCode)
              }
            >
              <div className="h6">{medicineName}</div>
            </a>
            <p>{companyName}</p>
            <span>
              {this.props.discount ? <del>₹{mrp}</del> : ""} ₹
              {(mrp - mrp * (this.props.discount / 100)).toFixed(2)}
            </span>
            {content._source?.original_base_discount ? (
              <span>(Min {content._source?.original_base_discount}% off)</span>
            ) : null}
          </CardInfo>
          {content.quantity ? (
            <ItemAdded>
              <div
                className="remove"
                onClick={() => {
                  let data = [
                    {
                      medicineName: content._source.original_sku_name,
                      medicineQty: "0",
                      medicineId: content._source.original_product_code,
                    },
                  ];
                  this.setState(
                    {
                      rMedData: data,
                      rContent: content,
                      removeQuantity: false,
                    },
                    () => {
                      this.removeMedOnClick(this.state.rMedData);
                      this.props.removeItemCartAction(this.state.rContent);
                      this.props.refreshData();
                      // Event_Truemeds
                      eventHomepageCrossellingRemoved();
                    }
                  );
                }}
                style={{ cursor: "pointer" }}
              >
                <img src={removeIcon} alt="remove" />
                <span>Remove</span>
              </div>
              <div className="quntity-select">
                {/*  <Select
                defaultValue="QTY"
                value={content.quantity}
                onChange={(e) => this.quantityOnHandleChange(e, content)}
              >
                {this.setmaxQty(qtyArr, content._source.max_capped_qty).map(
                  (qty) => (
                    <Option value={qty}>{qty}</Option>
                  )
                )}
              </Select> */}

                <Select
                  id="addtocart"
                  placeholder="QTY"
                  menuPlacement="auto"
                  menuPosition="fixed"
                  value={{ label: content.quantity, value: content.quantity }}
                  onChange={(e) => {
                    this.quantityOnHandleChange(e, content);
                  }}
                  closeMenuOnScroll={(e) => {
                    if (e.target.className) {
                      return false;
                    } else {
                      return true;
                    }
                  }}
                  isSearchable={false}
                  options={this.setmaxQty(
                    qtyArr,
                    content._source.max_capped_qty
                  ).map((qty) => {
                    return {
                      value: qty,
                      label: qty,
                    };
                  })}
                />
              </div>
            </ItemAdded>
          ) : (
            <>
              {!content._source.original_available &&
              !content._source.subs_found ? (
                <span className="not-in-stock">Not In Stock</span>
              ) : content._source.original_supplied_bytm ? (
                <Button
                  CartAdd
                  onClick={() => {
                    this.props.addItemCartAction(
                      this.addNewQuantity(content, 1)
                    );
                    // Event_Truemeds;
                    eventHomepageCrossellingAdded();
                  }}
                  id={"addtocart"}
                >
                  <HiPlusSm className="plusIcon" /> Add to cart
                </Button>
              ) : (
                <span
                  style={{ alignSelf: "flex-end" }}
                  className="not-available"
                >
                  Not sold by Truemeds
                </span>
              )}
            </>
          )}
        </CardContainer>
      </div>
    );
  }
}
MedicineCard.defaultProps = {
  images: null,
};

const mapStateToProps = (state) => ({
  cartContent: state.cartData?.cartItems,
  accessToken: state.loginReducer.verifyOtpSuccess?.Response?.access_token,
  medConfirm: state.confirmMedicineReducer?.ConfirmMedData,
  uploadImage: state.uploadImage,
});

export default connect(mapStateToProps, {
  addItemCartAction,
  removeItemCartAction,
  removeMedThunk,
})(withStyles(styles, { withTheme: true })(MedicineCard));

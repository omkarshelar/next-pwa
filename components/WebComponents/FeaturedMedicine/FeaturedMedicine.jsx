import React, { Component, createRef } from "react";
import Button from "../../WebComponents/Button/Button";
import {
  FeaturedWrapper,
  Header,
  MedicineList,
  Slider,
  DummyHeight,
} from "./FeaturedMedicine.style";
import MedicineCard from "./MedicineCard";
import next from "../../../src/Assets/next.svg";
import prev from "../../../src/Assets/prev.svg";
import BlankCard from "../BlankCard/BlankCard";
import {
  featuredMedicineData,
  featuredMedicineOnly,
} from "./FeaturedMedicineUtil";
import { connect } from "react-redux";
import noDataIcon from "../../../src/Assets/no-data.svg";
import CardFeatureLoader from "./CardFeatureLoader";
import { FeaturedMedThunk } from "../../../redux/FeaturedMedicine/Action";

export class FeaturedMedicine extends Component {
  state = {
    FeaturedType: 1,
  };
  constructor(props) {
    super(props);
    this.ListRef = createRef();
  }
  scrollRight = () => {
    if (this.ListRef.current) {
      this.ListRef.current.scrollLeft = this.ListRef.current.scrollLeft + 500;
    }
  };
  scrollLeft = () => {
    if (this.ListRef.current) {
      this.ListRef.current.scrollLeft = this.ListRef.current.scrollLeft - 500;
    }
  };

  renderCartandMedItems = (apiData, cartData) => {
    let updatedApiData = [];
    for (const apiItem in apiData) {
      for (const cardItem in cartData) {
        if (
          cartData[cardItem]._source.original_product_code ===
          apiData[apiItem]._source.original_product_code
        ) {
          updatedApiData.push({
            dataIndex: apiItem,
            ...cartData[cardItem],
          });
        }
      }
    }
    for (const updatedItem of updatedApiData) {
      apiData.splice(Number(updatedItem.dataIndex), 1, updatedItem);
    }
    return apiData;
  };
  callPastMedicineData = () => {
    /*  this.props.PastPurchaseThunk({
      history: this.props.history,
      accessToken: this.props?.accessToken,
      customerId: this.props?.customerId,
    }); */
  };

  render() {
    let { SuggestMedicine, PastMedicine } = this.props;
    let { FeaturedType } = this.state;
    let SuggestMedicineData = this.renderCartandMedItems(
      SuggestMedicine,
      this.props.cart
    );
    let PastMedicineData = this.renderCartandMedItems(
      PastMedicine,
      this.props.cart
    );

    return (
      <>
        <FeaturedWrapper>
          <Header>
            <div className="feature-top-header">
              <div className="heading">
                <h2>Medicines popularly bought on Truemeds</h2>
              </div>
              <div className="feature-filter-wrapper">
                <div className="myOrderTypesContainer">
                  {PastMedicineData?.length > 0
                    ? featuredMedicineData.map((data) => (
                        <div
                          key={data.id}
                          className={
                            FeaturedType == data.id
                              ? "myOrderSelectedType"
                              : "myOrderUnselectedType"
                          }
                          onClick={() => {
                            this.setState({ FeaturedType: data.id }, () => {});
                          }}
                        >
                          <span
                            className={
                              FeaturedType == data.id
                                ? "myOrderSelectedTypeText"
                                : "myOrderUnselectedTypeText"
                            }
                          >
                            {data.status}
                          </span>
                        </div>
                      ))
                    : featuredMedicineOnly.map((data) => (
                        <div
                          key={data.id}
                          className={
                            FeaturedType == data.id
                              ? "myOrderSelectedType"
                              : "myOrderUnselectedType"
                          }
                          onClick={() => {
                            this.setState({ FeaturedType: data.id }, () => {});
                          }}
                        >
                          <span
                            className={
                              FeaturedType == data.id
                                ? "myOrderSelectedTypeText"
                                : "myOrderUnselectedTypeText"
                            }
                          >
                            {data.status}
                          </span>
                        </div>
                      ))}
                </div>

                <div style={{ display: "flex" }}>
                  <Button>
                    <img src={prev} onClick={this.scrollLeft} alt="left"></img>
                  </Button>
                  <Button>
                    <img
                      src={next}
                      onClick={this.scrollRight}
                      alt="right"
                    ></img>
                  </Button>
                </div>
              </div>
            </div>
          </Header>
          <Slider>
            <MedicineList ref={this.ListRef}>
              <BlankCard />
              {this.props.isLoading || !SuggestMedicineData ? (
                <CardFeatureLoader />
              ) : FeaturedType === 1 ? (
                SuggestMedicineData?.map((data) => (
                  <MedicineCard
                    isMobile={this.props.isMobile}
                    key={data._source.original_product_code}
                    medicineName={data._source.original_sku_name}
                    companyName={data._source.original_company_nm}
                    mrp={data._source.original_mrp}
                    images={data.product_image_urls}
                    content={data}
                    refreshData={() => {
                      this.props.FeaturedMedThunk({
                        warehouseId: this.props.pincodeDetails?.warehouseId
                          ? this.props.pincodeDetails.warehouseId
                          : 3,
                        access_token: this.props.accessToken
                          ? this.props.accessToken
                          : null,
                      });
                    }}
                    discount={data._source.original_base_discount}
                    productCode={data?._source?.original_product_code}
                  />
                ))
              ) : PastMedicineData?.length > 0 ? (
                PastMedicineData.map((data) => (
                  <MedicineCard
                    key={data._source.original_product_code}
                    medicineName={data._source.original_sku_name}
                    companyName={data._source.original_company_nm}
                    mrp={data._source.original_mrp}
                    discount={data._source.original_base_discount}
                    content={data}
                    refreshData={this.callPastMedicineData}
                    productCode={data?._source?.original_product_code}
                  />
                ))
              ) : SuggestMedicineData?.length > 0 ? (
                SuggestMedicineData?.map((data) => (
                  <MedicineCard
                    key={data._source.original_product_code}
                    medicineName={data._source.original_sku_name}
                    companyName={data._source.original_company_nm}
                    mrp={data._source.original_mrp}
                    images={data.product_image_urls}
                    content={data}
                    refreshData={() => {
                      this.props.FeaturedMedThunk({
                        warehouseId: this.props.pincodeDetails?.warehouseId
                          ? this.props.pincodeDetails.warehouseId
                          : 3,
                        access_token: this.props.accessToken
                          ? this.props.accessToken
                          : null,
                      });
                    }}
                    discount={data._source.original_base_discount}
                    productCode={data?._source?.original_product_code}
                  />
                ))
              ) : (
                <div className="myOrdersNoDataContainer">
                  <img src={noDataIcon} alt="no-data" />
                  <span>No Data Found</span>
                </div>
              )}
            </MedicineList>
          </Slider>

          {this.props.router?.pathname === "/medicine/[id]" ? (
            <DummyHeight></DummyHeight>
          ) : (
            ""
          )}
        </FeaturedWrapper>
      </>
    );
  }
}

let mapStateToProps = (state) => ({
  SuggestMedicine: state.suggestMed.featuredMedicineList?.hits,
  PastMedicine: state.suggestMed.PastMedicineList,
  cart: state.cartData.cartItems,
  accessToken: state.loginReducer.verifyOtpSuccess?.Response?.access_token,
  customerId: state.loginReducer.verifyOtpSuccess?.CustomerId,
  isLoading: state.loader?.isLoading,
  pincodeDetails: state.pincodeData?.pincodeData,
});

export default connect(mapStateToProps, {
  FeaturedMedThunk,
  //   PastPurchaseThunk,
})(FeaturedMedicine);

import styled from "styled-components";

export const DummyHeight = styled.div`
  @media screen and (max-width: 768px) {
    height: 80px;
  }
`;
export const FeaturedWrapper = styled.div`
  width: 100%;
  display: flex;
  flex-flow: column;
  margin-bottom: 1.5rem;
  background: linear-gradient(180deg, #c9e8fe 0%, rgba(110, 193, 253, 0) 100%);
`;

export const MedicineList = styled.div`
  display: flex;
  overflow-x: auto;
  scroll-behavior: smooth;
  transition: scroll-behavior 1s ease;
  min-width: 100%;
  -ms-overflow-style: none; /* IE and Edge */
  scrollbar-width: none; /* Firefox */
  ::-webkit-scrollbar:horizontal {
    height: 0;
    width: 0;
    display: none;
  }

  ::-webkit-scrollbar-thumb:horizontal {
    display: none;
  }
  .myOrdersNoDataContainer {
    padding: 10px;
    display: flex;
    justify-content: center;
    align-items: center;
    margin: 1rem 0 1rem 1rem;
  }

  .myOrdersNoDataContainer img {
    width: 30px;
    margin-right: 10px;
  }

  .myOrdersNoDataContainer span {
    font-size: 18px;
    font-weight: 600;
    color: #333;
  }
  @media screen and (max-width: 568px) {
    .myOrdersNoDataContainer {
      flex-grow: 1;
    }
  }
`;
export const Slider = styled.div`
  display: flex;
  justify-content: flex-end;
  > button > img {
    width: 30px;
  }
`;

export const Header = styled.div`
  display: flex;
  width: 100%;
  justify-content: flex-end;
  > div[class="feature-top-header"] {
    box-sizing: border-box;
    width: 85%;
    display: flex;
    align-items: center;
    justify-content: space-between;
    > div[class="feature-filter-wrapper"] {
      display: flex;
      align-items: center;
      > div[class="myOrderTypesContainer"] {
        display: flex;
        flex-direction: row;
        align-items: center;
        justify-content: center;
        margin-bottom: 0 !important;
        .myOrderSelectedType,
        .myOrderUnselectedType {
          display: flex;
          flex-direction: row;
          justify-content: center;
          cursor: pointer;
          padding-bottom: 5px;
          width: auto;
          padding: 5px 10px;
          transition: 0.2s;
        }
        .myOrderSelectedType {
          border-bottom: 4px solid #0071bc;
        }

        .myOrderUnselectedType {
          border-bottom: 1px solid #0071bc;
        }
        .myOrderSelectedType > span {
          text-align: center;
        }
        .myOrderUnselectedType > span {
          text-align: center;
        }
      }
    }
    > div[class="heading"] {
      padding: 5px 10px;
      > h2 {
        width: 70%;
        /* font-family: "Century Gothic", sans-serif; */
        font-size: 25px;
        font-style: normal;
        font-weight: 700;
        line-height: 40px;
        letter-spacing: 0px;
        text-align: left;
        color: #003055;
        margin-top: 1rem;
      }
    }
  }
  @media screen and (max-width: 1068px) {
    > div[class="feature-top-header"] {
      width: 98%;
      > div[class="feature-filter-wrapper"] {
        > div[class="myOrderTypesContainer"] {
          min-height: 40px;
        }
      }
    }
  }
  @media screen and (max-width: 868px) {
    > div[class="feature-top-header"] {
      flex-flow: column;
      width: 100%;
      > div[class="heading"] {
        > h2 {
          width: 100%;
          text-align: center;

          margin: 1rem 0 0 0;
          font-size: 21px;
        }
      }
      > div[class="feature-filter-wrapper"] {
        flex-flow: column;
        width: 100%;
      }
    }
  }
  @media screen and (max-width: 468px) {
    > div[class="feature-top-header"] {
      > div[class="heading"] {
        > h2 {
          margin: 0.5rem 0 0 0;
          font-size: 18px;
          line-height: 35px;
        }
      }
      > div[class="feature-filter-wrapper"] {
        > div[class="myOrderTypesContainer"] {
          .myOrderSelectedType,
          .myOrderUnselectedType {
            .myOrderSelectedTypeText,
            .myOrderUnselectedTypeText {
              font-size: 12px;
            }
          }
        }
      }
    }
  }
`;

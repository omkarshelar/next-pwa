import React, { Component } from "react";
import "./SaveCard.css";
import savingsIcon from "../../../src/Assets/savings-percent.svg";
import maskImg from "../../../src/Assets/rectMask.svg";

export class SaveCard extends Component {
  render() {
    return (
      <div
        className={
          this.props.isCart
            ? "reusableSaveCardCartContainer"
            : "reusableSaveCardContainer"
        }
        style={{
          paddingBottom: this.props.savings ? "" : "0",
          display: !this.props.showSaveCard && this.props.isCart ? "none" : "",
        }}
      >
        <div
          className="reusableSaveCard"
          style={{ display: this.props.savings ? "" : "none" }}
        >
          <div className="maskWrap">
            {!this.props.isCart && <img src={maskImg} />}
          </div>
          <img src={savingsIcon} />
          <div>
            <span>You have already </span>
            <span>saved ₹{this.props.savings} </span>
            <span> on this order.</span>
          </div>
        </div>
      </div>
    );
  }
}

export default SaveCard;

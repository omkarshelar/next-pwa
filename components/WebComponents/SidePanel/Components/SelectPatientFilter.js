import React, { Component, createRef } from "react";
import "./SelectPatientFilter.css";
import { withRouter } from "next/router";
import { connect } from "react-redux";
import { Checkbox } from "@material-ui/core";
import Button from "../../Button/Button";
import {
  applyPatientFilterAction,
  clearPatientFilterAction,
} from "../../../../redux/PatientDetails/Action";
import window from "global";

export class SelectPatientFilter extends Component {
  state = {
    patientList: this.props.patientList,
    checkError: false,
  };

  errorRef = createRef();

  componentDidMount() {
    if (
      this.props.patientFilter?.length > 0 &&
      this.state.patientList?.length > 0
    ) {
      this.addChecked(this.state.patientList, this.props.patientFilter);
    }
  }

  addChecked = (arr, ids) => {
    let finalArr = [];
    for (let i in arr) {
      let patient = arr[i];
      for (let j in ids) {
        let id = ids[j];
        if (patient.patientId === id) {
          patient.checked = true;
          break;
        }
      }
      finalArr.push(patient);
    }
    this.setState({ patientList: finalArr });
  };

  selectPatient = (e) => {
    let newList = this.state.patientList;
    for (let i in newList) {
      let patient = newList[i];
      if (patient.patientId === Number(e.target.value)) {
        patient.checked = e.target.checked;
        break;
      }
    }
    this.setState({ patientList: newList });
  };

  validateFilter = (arr) => {
    let isChecked = false;
    for (let i in arr) {
      let patient = arr[i];
      if (patient.checked) {
        isChecked = true;
        break;
      }
    }
    return isChecked;
  };

  getPatientIds = (arr) => {
    let ids = [];
    for (let i in arr) {
      let patient = arr[i];
      if (patient.checked) {
        ids.push(patient.patientId);
      }
    }
    return ids;
  };

  applyFilter = () => {
    if (this.validateFilter(this.state.patientList)) {
      this.setState({ checkError: false });
      this.props.applyPatientFilterAction(
        this.getPatientIds(this.state.patientList)
      );
      window.openSideBar(false);
    } else {
      this.setState({ checkError: true }, () => {
        this.errorRef.scrollIntoView({ behavior: "smooth" });
      });
    }
  };

  clearFilter = () => {
    this.setState({ checkError: false });
    let newList = this.state.patientList;
    for (let i in newList) {
      let patient = newList[i];
      patient.checked = false;
    }
    this.setState({ patientList: newList });
    this.props.clearPatientFilterAction();
    // window.openSideBar(false);
  };

  render() {
    return (
      <div>
        <div className="selectPatientFilterContainer">
          <div>
            {this.state.patientList.map((data) => (
              <div className="selectPatientFilterCheckboxContainer">
                <Checkbox
                  className="selectPatientFilterCheckbox"
                  checked={data.checked}
                  name={data.patientId}
                  value={data.patientId}
                  onChange={this.selectPatient}
                  disableRipple
                />
                <span>{data.patientName}</span>
              </div>
            ))}
          </div>
          {this.state.checkError ? (
            <p
              ref={(el) => {
                this.errorRef = el;
              }}
            >
              Please select a patient
            </p>
          ) : null}
        </div>
        {this.props.visible ? (
          <div className="selectPatientFooter">
            <p onClick={this.clearFilter}>Clear</p>
            <Button proceedBtn onClick={this.applyFilter}>
              Proceed
            </Button>
          </div>
        ) : null}
      </div>
    );
  }
}

let mapStateToProps = (state) => ({});

export default withRouter(
  connect(mapStateToProps, {
    applyPatientFilterAction,
    clearPatientFilterAction,
  })(SelectPatientFilter)
);

import React from "react";
import "./PatientDetailsCustom.css";
import "./PatientDetailsSidePanel.css";
import { withRouter } from "next/router";
import { connect } from "react-redux";
import { Input, Radio, message } from "antd";
import Button from "../../Button/Button";
import {
  updateProfileThunk,
  addPatientThunk,
} from "../../../../redux/PatientDetails/Action";
import DummyHeight from "./SubComponents/DummyHeight";
import dropdownIcon from "../../../../src/Assets/patientDetailsDropdown.svg";
// import {
//   addUserAttributesMoe,
//   eventPatientAdded,
// } from "../../../../src/Events/Events";
import window from "global";
import dynamic from "next/dynamic";
const Select = dynamic(() => import("antd/lib/select"), { ssr: false });
const { Option } = Select;
const errorObject = {
  patientName: "Please enter valid name",
  age: "Please enter valid age",
  relationId: "Please select who this order is for",
  gender: "Please select a gender",
};
class PatientDetailsSidePanel extends React.Component {
  state = {
    details: this.props.data
      ? this.props.data
      : {
          relationId: 5,
          patientName: "",
          age: "",
          gender: 0,
        },
    orderedByDetails: {},
    detailsError: {},
    orderedByDetailsError: {},
    isOrderedByActive: false,
    hasSelf: false,
  };

  componentDidMount = () => {
    let customerDetails = {};
    if (this.props.patientsList) {
      customerDetails = this.props.patientsList.find((e) => e.relationId === 5);
    }
    let { details } = this.state;
    if (this.props.isAdd) {
      details.relationId = customerDetails ? null : 5;
    }
    this.setState({
      orderedByDetails: customerDetails || {},
      details: details,
      hasSelf: customerDetails ? true : false,
    });
  };

  componentDidUpdate = (prevProps, prevState) => {
    if (this.props.isBack) {
      window.openSideBar(true, 3);
    }
  };

  handleRelationChange = (e, name) => {
    let { details, detailsError } = this.state;
    details[name] = e;

    if (!e) {
      detailsError[name] = errorObject[name];
    } else {
      if (detailsError[name]) {
        delete detailsError[name];
      }
    }
    this.setState({
      details,
      detailsError,
    });
  };

  handleChange = (e, name) => {
    let { details, detailsError } = this.state;
    details[name] = e.target.value;

    let nameRegex = /^[a-zA-z]+([\s][a-zA-Z]+)*$/;
    if (!e.target.value) {
      detailsError[name] = errorObject[name];
    } else if (name === "patientName" && !nameRegex.test(e.target.value)) {
      detailsError[name] = errorObject[name];
    } else if (
      name === "age" &&
      (Number(e.target.value) < 1 || Number(e.target.value) > 125)
    ) {
      detailsError[name] = errorObject[name];
    } else {
      if (detailsError[name]) {
        delete detailsError[name];
      }
    }
    this.setState({
      details,
      detailsError,
    });
  };

  handleChangeOrderBy = (e, name) => {
    let { orderedByDetails, orderedByDetailsError } = this.state;
    orderedByDetails[name] = e.target.value;
    let nameRegex = /^[a-zA-z]+([\s][a-zA-Z]+)*$/;
    if (!e.target.value) {
      orderedByDetailsError[name] = errorObject[name];
    } else if (name === "patientName" && !nameRegex.test(e.target.value)) {
      orderedByDetailsError[name] = errorObject[name];
    } else if (
      name === "age" &&
      (Number(e.target.value) < 1 || Number(e.target.value) > 125)
    ) {
      orderedByDetailsError[name] = errorObject[name];
    } else {
      if (orderedByDetailsError[name]) {
        delete orderedByDetailsError[name];
      }
    }

    this.setState({
      orderedByDetails,
      isOrderedByActive: true,
      orderedByDetailsError,
    });
  };

  editPatientHandler = (value) => {
    let data = {
      accessToken: this.props.accessToken,
      patientData: {
        patientName: value.patientName,
        age: value.age,
        gender: value.gender,
        relationId: value.relationId,
        patientId: value.patientId,
      },
      history: this.props.router,
    };
    this.props.addPatientThunk(data).then(() => {
      if (this.props.patientContent?.newPatientError) {
        message.error("something went wrong");
      } else {
        window.openSideBar(true, 3);
        if (!value.patientId) {
          // new
          // eventPatientAdded();
        }
      }
    });
  };

  updateProfileHandler = (value, redirect = false) => {
    let data = {
      accessToken: this.props.accessToken,
      latestData: {
        customerName: value.patientName,
        emailAddress: value.emaiemailAddressl ? value.emailAddress : "",
        age: value.age,
        gender: value.gender,
      },
      history: this.props.router,
    };
    this.props.updateProfileThunk(data).then(() => {
      if (this.props.patientContent?.updateProfileError) {
        message.error("something went wrong");
      } else {
        // new
        // addUserAttributesMoe(data.latestData);
        // Event_Truemeds
        // new
        // eventPatientAdded();
        if (redirect) {
          window.openSideBar(true, 3);
        }
      }
    });
  };

  validate = () => {
    let { details, orderedByDetails, detailsError, orderedByDetailsError } =
      this.state;

    if (!orderedByDetails.patientName) {
      orderedByDetailsError.patientName = errorObject.patientName;
    }
    if (!orderedByDetails.gender) {
      orderedByDetailsError.gender = errorObject.gender;
    }
    if (!orderedByDetails.age) {
      orderedByDetailsError.age = errorObject.age;
    }

    if (!details.patientName) {
      detailsError.patientName = errorObject.patientName;
    }
    if (!details.gender) {
      detailsError.gender = errorObject.gender;
    }
    if (!details.age) {
      detailsError.age = errorObject.age;
    }

    if (!details.relationId) {
      detailsError.relationId = errorObject.relationId;
    }

    this.setState({
      detailsError,
      orderedByDetailsError,
    });

    if (details.relationId === 5) {
      if (Object.keys(detailsError).length > 0) {
        return false;
      } else {
        return true;
      }
    } else {
      if (
        Object.keys(detailsError).length > 0 ||
        Object.keys(orderedByDetailsError).length > 0
      ) {
        return false;
      } else {
        return true;
      }
    }
  };
  handlePatientSave = () => {
    let { isOrderedByActive, orderedByDetails, details } = this.state;
    if (this.validate()) {
      if (isOrderedByActive) {
        this.updateProfileHandler(orderedByDetails, false);
        this.editPatientHandler(details);
      } else {
        if (details.relationId === 5) {
          this.updateProfileHandler(details, true);
        } else {
          this.editPatientHandler(details);
        }
      }
    }
  };

  render() {
    return (
      <div className="patientDetailsSidePanelWrapper">
        <div className={"patientDetailsSidePanel_formWrap"}>
          <div className={"patientDetailsSidePanel_InputWrapper"}>
            <div className={"patientDetailsSidePanel_input_label"}>
              Your relation with the patient
            </div>
            <Select
              value={this.state.details.relationId}
              onChange={(e) => this.handleRelationChange(e, "relationId")}
              placeholder={
                <span className={"patientDetailsSidePanel_antPlaceholder"}>
                  {"Select"}
                </span>
              }
              className={`${"patientDetailsSidePanel_inputForm"}`}
              style={{ width: "100%" }}
              dropdownClassName={"patientDetailsDropDown"}
              disabled={!this.props.isAdd && this.props.data?.relationId === 5}
              suffixIcon={<img src={dropdownIcon} />}
            >
              {!this.state.hasSelf && <Option value={5}>Self</Option>}
              {this.props.data?.relationId === 5 && (
                <Option value={5}>Self</Option>
              )}
              <Option value={1}>My Grandparent</Option>
              <Option value={2}>My Parent</Option>
              <Option value={3}>My Sibling</Option>
              <Option value={4}>My Spouse</Option>

              <Option value={6}>My Child</Option>
              <Option value={7}>My Grandchild</Option>
              <Option value={8}>Someone else</Option>
            </Select>
            {this.state.detailsError?.relationId && (
              <div className={"patientDetailsSidePanel_errorText"}>
                {this.state.detailsError?.relationId}
              </div>
            )}
          </div>
          <div className={"patientDetailsSidePanel_InputWrapper"}>
            <div className={"patientDetailsSidePanel_input_label"}>
              Patient’s Name
            </div>
            <Input
              name="patientName"
              className={`${"patientDetailsSidePanel_inputForm"}`}
              value={this.state.details.patientName}
              onChange={(e) => this.handleChange(e, "patientName")}
              disabled={
                this.props.data?.patientName &&
                this.props.data?.relationId !== 5
                  ? true
                  : false
              }
            />
            {this.state.detailsError?.patientName && (
              <div className={"patientDetailsSidePanel_errorText"}>
                {this.state.detailsError?.patientName}
              </div>
            )}
          </div>
          <div className={"patientDetailsSidePanel_InputWrapper"}>
            <div className={"patientDetailsSidePanel_input_label"}>Age</div>
            <Input
              name="age"
              className={`${"patientDetailsSidePanel_inputForm"}`}
              value={this.state.details.age}
              onChange={(e) => this.handleChange(e, "age")}
            />
            {this.state.detailsError?.age && (
              <div className={"patientDetailsSidePanel_errorText"}>
                {this.state.detailsError?.age}
              </div>
            )}
          </div>
          <div className={"patientDetailsSidePanel_InputWrapper"}>
            <div className={"patientDetailsSidePanel_input_label"}>Gender</div>
            <div>
              <Radio.Group
                onChange={(e) => this.handleChange(e, "gender")}
                value={this.state.details.gender}
              >
                <Radio value={8}>Male</Radio>
                <Radio value={9}>Female</Radio>
                <Radio value={10}>Other</Radio>
              </Radio.Group>
            </div>
            {this.state.detailsError?.gender && (
              <div className={"patientDetailsSidePanel_errorText"}>
                {this.state.detailsError?.gender}
              </div>
            )}
          </div>
          {this.state.details?.relationId != 5 ? (
            <>
              <div className={"patientDetailsSidePanel_InputWrapper"}>
                <div className={"patientDetailsSidePanel_input_label"}>
                  Ordered By
                </div>
                <Input
                  name="patientName"
                  className={`${"patientDetailsSidePanel_inputForm"}`}
                  value={this.state.orderedByDetails?.patientName}
                  onChange={(e) => this.handleChangeOrderBy(e, "patientName")}
                />
                {this.state.orderedByDetailsError?.patientName && (
                  <div className={"patientDetailsSidePanel_errorText"}>
                    {this.state.orderedByDetailsError?.patientName}
                  </div>
                )}
              </div>
              <div className={"patientDetailsSidePanel_InputWrapper"}>
                <div className={"patientDetailsSidePanel_input_label"}>Age</div>
                <Input
                  name="age"
                  className={`${"patientDetailsSidePanel_inputForm"}`}
                  value={this.state.orderedByDetails?.age}
                  onChange={(e) => this.handleChangeOrderBy(e, "age")}
                />
                {this.state.orderedByDetailsError?.age && (
                  <div className={"patientDetailsSidePanel_errorText"}>
                    {this.state.orderedByDetailsError?.age}
                  </div>
                )}
              </div>
              <div className={"patientDetailsSidePanel_InputWrapper"}>
                <div className={"patientDetailsSidePanel_input_label"}>
                  Gender
                </div>
                <div>
                  <Radio.Group
                    onChange={(e) => this.handleChangeOrderBy(e, "gender")}
                    value={this.state.orderedByDetails?.gender}
                  >
                    <Radio value={8}>Male</Radio>
                    <Radio value={9}>Female</Radio>
                    <Radio value={10}>Other</Radio>
                  </Radio.Group>
                </div>
                {this.state.orderedByDetailsError?.gender && (
                  <div className={"patientDetailsSidePanel_errorText"}>
                    {this.state.orderedByDetailsError?.gender}
                  </div>
                )}
              </div>
              <div
                className={"patientDetailsSidePanel_bottomSpaceAdjustment"}
              ></div>
            </>
          ) : (
            ""
          )}
        </div>
        <DummyHeight />
        {this.props.visible && (
          <Button
            CartAdd
            isSidePanel={true}
            onClick={() => {
              this.handlePatientSave();
            }}
          >
            Save Patient
          </Button>
        )}
      </div>
    );
  }
}

let mapStateToProps = (state) => ({
  accessToken: state.loginReducer.verifyOtpSuccess?.Response?.access_token,
  patientsList: state.patientData.fetchPatientSuccess,
  isLoading: state.loader.isLoading,
  patientContent: state.patientData,
});

export default withRouter(
  connect(mapStateToProps, {
    addPatientThunk,
    updateProfileThunk,
  })(PatientDetailsSidePanel)
);

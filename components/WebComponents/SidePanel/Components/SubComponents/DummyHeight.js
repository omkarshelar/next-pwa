import React from "react";
import "./DummyHeight.css";
const DummyHeight = (props) => {
  return <div style={{ height: props.height ? props.height : "76px" }}></div>;
};

export default DummyHeight;

import React from "react";
import "./PatientCard.css";
import tick from "../../../../../src/Assets/TickNew.svg";
const PatientCard = (props) => {
  return (
    <div
      className={`${"patientCard_wrap"} ${
        props.selected && "patientCard_selected"
      }`}
      onClick={() => {
        props.selectionHandler();
      }}
    >
      <div className={"patientCard_patientName"}>
        {props.details.patientName}
      </div>
      <div className={"patientCard_relationName"}>
        {props.details.relationName?.toLowerCase()}
      </div>
      <div className={"patientCard_patientCta_wrap"}>
        <div
          className={"patientCard_patientCta"}
          onClick={(e) => {
            e.stopPropagation();
            window.openSideBar(true, 5, { details: props.details });
          }}
        >
          Edit
        </div>
        <div className={"patientCard_patientCta_select"}>
          {props.selected ? (
            <div>
              <img src={tick} />
            </div>
          ) : (
            <>
              Select <div className={"patientCard_patientCta_selectRing"}></div>
            </>
          )}
        </div>
      </div>
    </div>
  );
};

export default PatientCard;

import React from "react";
import "./AddressCard.css";
import tick from "../../../../../src/Assets/TickNew.svg";

const AddressCard = (props) => {
  return (
    <div
      className={` ${"addressCard_AddressCard_wrap"} ${
        props.selected && "addressCard_selected"
      }`}
      onClick={() => {
        props.selectionHandler();
      }}
    >
      <div className={"addressCard_addressType"}>
        {props.details.addressType}
      </div>
      <div className={"addressCard_addressSection"}>
        {props.details.addressline1 +
          ", " +
          props.details.addressline2 +
          ", " +
          props.details.landmark +
          ", " +
          props.details.cityName +
          ", " +
          props.details.pincode}
      </div>
      <div className={"addressCard_addressCta_wrap"}>
        <div
          onClick={(e) => {
            e.stopPropagation();
            window.openSideBar(true, 4, props.details);
          }}
          className={"addressCard_addressCta"}
        >
          Edit
        </div>
        <div className={"addressCard_addressCta_select"}>
          {props.unServiciableArray.includes(props.details.addressId) ? (
            <div className={"addressCard_unServicible"}>Unserviceable</div>
          ) : props.selected ? (
            <div>
              <img src={tick} />
            </div>
          ) : (
            <>
              Select <div className={"addressCard_addressCta_selectRing"}></div>
            </>
          )}
        </div>
      </div>
    </div>
  );
};

export default AddressCard;

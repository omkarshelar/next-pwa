export const allComponents = [
  {
    //   Select Patient
    id: 1,
    title: "Past Prescriptions",
    close: true,
  },
  {
    //   Select & Upload Prescription
    id: 2,
    title: "Past Prescriptions",
    back: true,
  },
  {
    //   Select and Save Patient and Address
    id: 3,
    title: "Patient & Address",
    close: true,
  },
  {
    id: 4,
    title: "Address Details",
    back: true,
  },

  {
    id: 5,
    title: "Patient Details",
    back: true,
  },

  {
    //   Display Summary Details
    id: 10,
    title: "Medicine Details",
    close: true,
  },
  {
    id: 11,
    title: "Doctor Details",
    close: true,
  },
  {
    id: 12,
    title: "Modification log",
    close: true,
  },
  {
    id: 13,
    title: "Select Patient",
    close: true,
  },
];

import React, { useEffect, useState } from "react";
import "./PastPresSelectPrescription.css";
import { connect } from "react-redux";
import { withRouter } from "next/router";
import { Image_URL } from "../../../../../constants/Urls";
import { Checkbox } from "@material-ui/core";
import PreviewModal from "../../../UploadPrescription/UploadSubComponent/PreviewModal/PreviewModal";
import Button from "../../../Button/Button";
import { uploadImageThunk } from "../../../../../redux/UploadImage/Action";
import { customNotification } from "../../../../Helper/helperFunction";
import moment from "moment";
// import {
//   eventPastPrescriptionUploaded,
//   eventPrescriptionViewed,
// } from "../../../../../src/Events/Events";
import window from "global";

export function PastPresSelectPrescription(props) {
  const [selectedPatient, setSelectedPatient] = useState([]);
  const [prescriptionArr, setprescriptionArr] = useState([]);
  const [selectedPrescriptions, setSelectedPrescriptions] = useState([]);
  const [showPreview, setShowPreview] = useState(false);
  const [previewSrc, setPreviewSrc] = useState("");
  const [waitForOrderId, setWaitOrderId] = useState(false);

  let activeRxStamp = [];

  const filterPrescriptionByDates = (arr) => {
    let finalArr = [];
    let uniqueDates = [];
    arr.sort((a, b) => {
      let uploadedDateA = new Date(a.createdOn);
      uploadedDateA.setHours(0, 0, 0, 0);
      let uploadedDateB = new Date(b.createdOn);
      uploadedDateB.setHours(0, 0, 0, 0);
      return new Date(uploadedDateB) - new Date(uploadedDateA);
    });
    for (let i in arr) {
      let img = arr[i];
      let uploadedDate = new Date(img.createdOn);
      if (!uniqueDates.includes(uploadedDate.toDateString())) {
        uniqueDates.push(uploadedDate.toDateString());
      }
    }
    for (let i in uniqueDates) {
      let date = uniqueDates[i];
      let arrByDate = {};
      arrByDate.date = date.slice(4);
      arrByDate.imgDetails = [];
      for (let j in arr) {
        let img = arr[j];
        let uploadedDate = new Date(img.createdOn);
        if (uploadedDate.toDateString() === date) {
          arrByDate.imgDetails.push(img);
        }
      }
      finalArr.push(arrByDate);
    }
    return finalArr;
  };

  const getPrevOrderId = (arr, imgId) => {
    let prevId = null;
    for (let i in arr) {
      let img = arr[i];
      if (Number(imgId) === Number(img.imageId)) {
        prevId = img.orderId;
      }
    }
    return prevId;
  };
  const onChangePrescriptionByDiv = (id, value) => {
    let cnt = 0;
    props.imgUpload.map((f) => {
      if (f.imgId == id) {
        cnt++;
      }
    });
    let selectedPrescriptionsArr = selectedPrescriptions;
    if (value) {
      selectedPrescriptionsArr.push({
        imageId: Number(id),
        prevOrderId: getPrevOrderId(selectedPatient.ActiveRx, id),
      });
    } else {
      selectedPrescriptionsArr = selectedPrescriptionsArr.filter(
        (item) => item.imageId !== Number(id)
      );
    }
    setSelectedPrescriptions([...selectedPrescriptionsArr]);
  };

  const checkImgForUpload = (id) => {
    let isUploaded = false;
    for (let i in props.imgUpload) {
      let img = props.imgUpload[i];
      if (img.imgId === id) {
        isUploaded = true;
        break;
      }
    }
    return isUploaded;
  };

  const uploadPastPrescription = async () => {
    if (!waitForOrderId) {
      if (selectedPrescriptions?.length > 3) {
        customNotification(
          "Select max 3 prescriptions",
          "customSidePanelNotify"
        );
      } else if (selectedPrescriptions?.length === 0) {
        customNotification(
          "Select a prescription to proceed",
          "customSidePanelNotify"
        );
      } else {
        let arr = selectedPrescriptions;
        if (
          props.orderIdTrue
            ? props.orderIdTrue
            : props.medConfirm?.ConfirmMedData?.orderId ||
              selectedPrescriptions.length === 1
        ) {
          window.openSideBar(false, 0);
          for (let i in arr) {
            let imageDetails = arr[i];
            await props.uploadImageThunk({
              imgData: {
                orderId: props.orderIdTrue
                  ? props.orderIdTrue
                  : props.medConfirm?.ConfirmMedData?.orderId,
                patientId: props.patientIdTrue,
                imageId: imageDetails.imageId,
                prevOrderId: imageDetails.prevOrderId,
              },
              access_token: props.accessToken?.Response?.access_token,
              history: props.router,
              customerId: props.custId,
              pincode: props.pincodeDetails.pincode,
            });
          }
        } else if (selectedPrescriptions.length > 1) {
          let imageDetails = arr[0];
          setWaitOrderId(true);
          await props.uploadImageThunk({
            imgData: {
              orderId: props.orderIdTrue
                ? props.orderIdTrue
                : props.medConfirm?.ConfirmMedData?.orderId,
              patientId: props.patientIdTrue,
              imageId: imageDetails.imageId,
              prevOrderId: imageDetails.prevOrderId,
            },
            access_token: props.accessToken?.Response?.access_token,
            history: props.router,
            customerId: props.custId,
            pincode: props.pincodeDetails.pincode,
          });
        }

        // Event_Truemeds
        // new
        // eventPastPrescriptionUploaded();
        if (props.allPrescriptions.error) {
          customNotification("Something went wrong!", "customNotify");
        } else {
          customNotification("Successfully uploaded", "customNotify");
        }
      }
    }
  };

  useEffect(() => {
    async function getPendingUpload() {
      if (waitForOrderId) {
        let arr = selectedPrescriptions;
        for (let i in arr) {
          if (i > 0) {
            let imageDetails = arr[i];
            await props.uploadImageThunk({
              imgData: {
                orderId: props.orderIdTrue
                  ? props.orderIdTrue
                  : props.medConfirm?.ConfirmMedData?.orderId,
                patientId: props.patientIdTrue,
                imageId: imageDetails.imageId,
                prevOrderId: imageDetails.prevOrderId,
              },
              access_token: props.accessToken?.Response?.access_token,
              history: props.router,
              customerId: props.custId,
              pincode: props.pincodeDetails.pincode,
            });
          }
        }
        setWaitOrderId(false);
        window.openSideBar(false, 0);
      }
    }
    getPendingUpload();
  }, [props.orderIdTrue]);

  useEffect(() => {
    async function getPendingUpload() {
      if (waitForOrderId) {
        let arr = selectedPrescriptions;
        for (let i in arr) {
          if (i > 0) {
            let imageDetails = arr[i];
            await props.uploadImageThunk({
              imgData: {
                orderId: props.orderIdTrue
                  ? props.orderIdTrue
                  : props.medConfirm?.ConfirmMedData?.orderId,
                patientId: props.patientIdTrue,
                imageId: imageDetails.imageId,
                prevOrderId: imageDetails.prevOrderId,
              },
              access_token: props.accessToken?.Response?.access_token,
              history: props.router,
              customerId: props.custId,
              pincode: props.pincodeDetails.pincode,
            });
          }
        }
        setWaitOrderId(false);
        window.openSideBar(false, 0);
      }
    }
    getPendingUpload();
  }, [props.medConfirm]);

  useEffect(() => {
    setSelectedPatient(props.pastPresSelectedPatient);
    setprescriptionArr(
      filterPrescriptionByDates(props.pastPresSelectedPatient?.ActiveRx)
    );
  }, []);

  useEffect(() => {
    if (props.isBack) {
      window.openSideBar(true, 1);
    }
  }, [props.isBack]);

  return (
    <div>
      <PreviewModal
        open={showPreview}
        imageData={previewSrc}
        onCloseFunc={() => setShowPreview(false)}
      />
      <div className="selectPrescriptionContainer">
        <p>
          {selectedPatient?.patientName
            ? selectedPatient?.patientName === "All Prescriptions"
              ? "All"
              : selectedPatient?.patientName + "'s"
            : "Patient"}{" "}
          Prescriptions
        </p>
        {prescriptionArr?.length > 0 &&
          prescriptionArr.map((data) => (
            <div className="selectPrescriptionCard">
              <div className="tmUploadedContainer">
                <span>
                  Uploaded On {moment(data.date).format("DD MMM YYYY")}
                </span>
                <div />
              </div>
              <div className="selectPrescriptionGridCard">
                {data.imgDetails?.length > 0 &&
                  data.imgDetails.map((img) => {
                    if (activeRxStamp.includes(img.imageId) == false) {
                      activeRxStamp.push(img.imageId);
                      let checked = false;
                      selectedPrescriptions.map((e) => {
                        if (img.imageId === e.imageId) {
                          checked = true;
                        }
                      });
                      return (
                        <div className="selectPrescriptionInsideCard">
                          <img
                            src={`${Image_URL}${img.imagePath}`}
                            alt="prescription"
                            onClick={() => {
                              //new
                              // eventPrescriptionViewed();
                              setPreviewSrc(`${Image_URL}${img.imagePath}`);
                              setShowPreview(true);
                            }}
                          />
                          {checkImgForUpload(img.imageId) ? (
                            <div>
                              <span className="presSelectedText">Uploaded</span>
                            </div>
                          ) : (
                            <div
                              onClick={() => {
                                onChangePrescriptionByDiv(
                                  img.imageId,
                                  !checked
                                );
                              }}
                            >
                              <Checkbox
                                checked={checked}
                                name={img.imageId}
                                value={img.imageId}
                                // onChange={onChangePrescription}
                                color="primary"
                                inputProps={{
                                  "aria-label": "primary checkbox",
                                }}
                              />
                              {checked ? (
                                <span className="presSelectedText">
                                  Selected
                                </span>
                              ) : (
                                <span className="presSelectText">Select</span>
                              )}
                            </div>
                          )}
                          {window.innerWidth <= 768 && (
                            <div
                              className="thirtyWrap"
                              onClick={() => {
                                onChangePrescriptionByDiv(
                                  img.imageId,
                                  !checked
                                );
                              }}
                            ></div>
                          )}
                        </div>
                      );
                    }
                  })}
              </div>
            </div>
          ))}
      </div>
      {props.visible && (
        <Button CartAdd isSidePanel={true} onClick={uploadPastPrescription}>
          Upload
        </Button>
      )}
    </div>
  );
}

const mapStateToProps = (state) => ({
  accessToken: state.loginReducer?.verifyOtpSuccess,
  imgUpload: state.uploadImage?.uploadHistory,
  orderIdTrue: state.uploadImage.orderId,
  patientIdTrue: state.uploadImage.PatientId,
  medConfirm: state.confirmMedicineReducer,
  custId: state.loginReducer?.verifyOtpSuccess?.CustomerId,
  pincodeDetails: state.pincodeData?.pincodeData,
  allPrescriptions: state.uploadImage,
});

export default withRouter(
  connect(mapStateToProps, { uploadImageThunk })(PastPresSelectPrescription)
);

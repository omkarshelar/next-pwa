import { Input, message, Spin } from "antd";
import React, { useEffect, useState, useReducer } from "react";
import { connect } from "react-redux";
import { withRouter } from "next/router";
import "./AddressDetailsSidePanel.css";
import * as Yup from "yup";
import { Formik } from "formik";
import Button from "../../Button/Button";
import {
  addAddressThunk,
  checkPincodeThunk,
  resetAddress,
} from "../../../../redux/AddressDetails/Action";
import DummyHeight from "./SubComponents/DummyHeight";
// import { eventAddressAdded } from "../../../../src/Events/Events";
import window from "global";

const pincodeReducer = (state, action) => {
  if (action.type === "SET_PINCODE_LOADER") {
    return {
      pincode: state.pincode,
      isPincodeLoader: action.value.isPincodeLoader,
      isPincodeError: state.isPincodeError,
    };
  }

  if (action.type === "SET_PINCODE") {
    return {
      pincode: action.value.pincode,
      isPincodeLoader: state.isPincodeLoader,
      isPincodeError: state.isPincodeError,
    };
  }

  if (action.type === "SET_PINCODE_POSTSERVICE") {
    return {
      pincode: action.value.pincode,
      isPincodeLoader: action.value.isPincodeLoader,
      isPincodeError: action.value.isPincodeError,
    };
  }

  if (action.type === "EFFECT_UPDATE") {
    return {
      pincode: state.pincode,
      isPincodeLoader: state.isPincodeLoader,
      isPincodeError: action.value.isPincodeError,
    };
  }
  return { pincode: "", isPincodeLoader: false, isPincodeError: false };
};
const AddressDetailsSidePanel = (props) => {
  const [pincodeState, dispatchPincode] = useReducer(pincodeReducer, {
    pincode: "",
    isPincodeLoader: false,
    isPincodeError: false,
  });
  const [addressType, setAddressType] = useState();

  const handleSubmit = (values, data) => {
    let { orderTypeOneOrderId, orderTypeTwoOrderId } = props;

    let payload = {
      accessToken: props.accessToken,
      addressData: {
        addressType: values.addressType,
        addressline1: values.addressline1,
        addressline2: values.addressline2,
        landmark: values.landmark,
        pincode: pincodeState.pincode,
        addressId: data ? data.addressId : 0,
      },
      isAppEdit: false,
      history: props.router,
    };

    if (orderTypeOneOrderId || orderTypeTwoOrderId) {
      payload.orderId = orderTypeOneOrderId
        ? orderTypeOneOrderId
        : orderTypeTwoOrderId;
    }
    if (!props.data) {
      // new
      // eventAddressAdded();
    }
    props.addAddressThunk(payload).then(() => {});
  };

  const postAddressSave = () => {
    if (props.addressData.newAddressError) {
      message.warning(props.addressData?.newAddressError);
      props.resetAddress();
    } else if (props.addressData.newAddressSuccess) {
      props.resetAddress();
      window.openSideBar(true, 3);
    }
  };
  useEffect(() => {
    postAddressSave();
  }, [props.addressData.newAddressError]);

  useEffect(() => {
    postAddressSave();
  }, [props.addressData.newAddressSuccess]);

  const handlePincode = (event) => {
    let pincode = event.target.value;
    dispatchPincode({
      type: "SET_PINCODE_LOADER",
      value: { isPincodeLoader: true },
    });
    if (pincode.length <= 6) {
      dispatchPincode({ type: "SET_PINCODE", value: { pincode: pincode } });
      if (pincode.length === 6) {
        let data = {
          pincode: pincode,
          accessToken: props.accessToken,
          history: props.router,
        };
        props.checkPincodeThunk(data).then(() => {
          if (props.isServiceable && props.isServiceable?.isServiceable) {
            dispatchPincode({
              type: "SET_PINCODE_POSTSERVICE",
              value: {
                isPincodeLoader: false,
                pincode: pincode,
                isPincodeError: false,
              },
            });
          } else {
            dispatchPincode({
              type: "SET_PINCODE_POSTSERVICE",
              value: {
                isPincodeLoader: false,
                pincode: pincode,
                isPincodeError: true,
              },
            });
          }
        });
      } else {
        dispatchPincode({
          type: "SET_PINCODE_POSTSERVICE",
          value: {
            isPincodeLoader: false,
            pincode: pincode,
            isPincodeError: true,
          },
        });
      }
    }
  };

  const checkPincodeErrorLocal = (pincode) => {
    if (pincode.length === 6) {
      return false;
    } else {
      return true;
    }
  };

  useEffect(() => {
    if (props.data) {
      dispatchPincode({
        type: "SET_PINCODE",
        value: {
          pincode: props.data.pincode,
          pincodeError: checkPincodeErrorLocal(props.data.pincode),
        },
      });
      // setPincode(props.data.pincode);
      setAddressType(props.data.addressType);
    }
  }, []);

  useEffect(() => {
    if (props.isBack) {
      window.openSideBar(true, 3);
    }
  }, [props.isBack]);

  useEffect(() => {
    if (props.isServiceable?.isServiceable) {
      dispatchPincode({
        type: "EFFECT_UPDATE",
        value: { isPincodeError: false },
      });
    }
  }, [props.isServiceable]);
  return (
    <div>
      <Formik
        initialValues={
          props.data
            ? props.data
            : {
                pincode: "",
                addressline1: "",
                addressline2: "",
                landmark: "",
                addressType: "",
              }
        }
        validationSchema={Yup.object().shape({
          pincode: Yup.string()
            .required("Please enter a pincode")
            .test("pincode", "", (value) => !pincodeState.isPincodeError),
          addressline1: Yup.string()
            .max(255, "Only 255 characters allowed")
            .required("Please add your address"),
          landmark: Yup.string()
            .max(255, "Only 255 characters allowed")
            .required("Please add your landmark"),
          addressline2: Yup.string()
            .max(255, "Only 255 characters allowed")
            .required("Please add your locality"),
          addressType: Yup.string()
            .required("Please select an option")
            .test(
              "addressType",
              "Enter a valid option",
              (value) =>
                value === "Home" || value === "Office" || value !== "Others"
            ),
        })}
        onSubmit={(values) => {
          handleSubmit(values, props.data);
        }}
      >
        {({
          errors,
          handleSubmit,
          touched,
          values,
          handleChange,
          setValues,
        }) => (
          <form onSubmit={handleSubmit}>
            <div className={"addressDetailsSidepanel_addressEditWrap"}>
              <div className={"addressDetailsSidepanel_InputWrapper"}>
                <div className={"addressDetailsSidepanel_input_label"}>
                  Enter Pincode
                  {pincodeState.isPincodeLoader && (
                    <div className={"addressDetailsSidepanel_pincodeLoader"}>
                      <Spin />
                    </div>
                  )}
                </div>
                <Input
                  name="pincode"
                  className={`${"addressDetailsSidepanel_inputForm"} ${
                    props.data && props.data.addressId
                      ? "addressDetailsSidepanel_inputFormDisabled"
                      : ""
                  } ${
                    (pincodeState.isPincodeError ||
                      (touched.pincode && errors.pincode)) &&
                    "addressDetailsSidepanel_errorInput"
                  }`}
                  value={pincodeState.pincode}
                  onChange={(e) => {
                    handleChange(e);
                    handlePincode(e);
                  }}
                  disabled={props.data && props.data.addressId}
                />
                {!Boolean(touched.pincode && errors.pincode) &&
                  pincodeState.isPincodeError && (
                    <div className={"addressDetailsSidepanel_errorText"}>
                      {pincodeState.pincode && pincodeState.pincode.length === 6
                        ? "Sorry! We currently do not deliver to this location"
                        : "Enter a valid pincode"}
                    </div>
                  )}
                {Boolean(touched.pincode && errors.pincode) && (
                  <div className={"addressDetailsSidepanel_errorText"}>
                    {touched.pincode && errors.pincode}
                  </div>
                )}
              </div>
              <div className={"addressDetailsSidepanel_InputWrapper"}>
                <div className={"addressDetailsSidepanel_input_label"}>
                  Flat number & Building name
                </div>
                <Input
                  name="addressline1"
                  className={`${"addressDetailsSidepanel_inputForm"} ${
                    Boolean(touched.addressline1 && errors.addressline1) &&
                    "addressDetailsSidepanel_errorInput"
                  }`}
                  value={values.addressline1}
                  onChange={handleChange}
                />
                {Boolean(touched.addressline1 && errors.addressline1) && (
                  <div className={"addressDetailsSidepanel_errorText"}>
                    {touched.addressline1 && errors.addressline1}
                  </div>
                )}
              </div>
              <div className={"addressDetailsSidepanel_InputWrapper"}>
                <div className={"addressDetailsSidepanel_input_label"}>
                  Locality, Area
                </div>
                <Input
                  name="addressline2"
                  className={`${"addressDetailsSidepanel_inputForm"} ${
                    Boolean(touched.addressline2 && errors.addressline2) &&
                    "addressDetailsSidepanel_errorInput"
                  }`}
                  value={values.addressline2}
                  onChange={handleChange}
                />
                {Boolean(touched.addressline2 && errors.addressline2) && (
                  <div className={"addressDetailsSidepanel_errorText"}>
                    {touched.addressline2 && errors.addressline2}
                  </div>
                )}
              </div>
              <div className={"addressDetailsSidepanel_InputWrapper"}>
                <div className={"addressDetailsSidepanel_input_label"}>
                  Landmark
                </div>
                <Input
                  name="landmark"
                  className={` ${"addressDetailsSidepanel_inputForm"} ${
                    Boolean(touched.landmark && errors.landmark) &&
                    "addressDetailsSidepanel_errorInput"
                  }`}
                  value={values.landmark}
                  onChange={handleChange}
                />
                {Boolean(touched.landmark && errors.landmark) && (
                  <div className={"addressDetailsSidepanel_errorText"}>
                    {touched.landmark && errors.landmark}
                  </div>
                )}
              </div>
              <div className={"addressDetailsSidepanel_InputWrapper"}>
                <div className={"addressDetailsSidepanel_input_label"}>
                  Save as
                </div>
                <div className={"addressDetailsSidepanel_typeWrap"}>
                  <div
                    className={`${"addressDetailsSidepanel_typeSelection"} ${
                      addressType === "Home" &&
                      "addressDetailsSidepanel_selectedAddress"
                    }`}
                    onClick={() => {
                      setAddressType("Home");
                      values.addressType = "Home";
                      setValues(values);
                    }}
                  >
                    Home
                  </div>
                  <div
                    className={`${"addressDetailsSidepanel_typeSelection"} ${
                      addressType === "Office" &&
                      "addressDetailsSidepanel_selectedAddress"
                    }`}
                    onClick={() => {
                      setAddressType("Office");
                      values.addressType = "Office";
                      setValues(values);
                    }}
                  >
                    Office
                  </div>
                  <div
                    className={`${"addressDetailsSidepanel_typeSelection"} ${
                      addressType &&
                      addressType !== "Home" &&
                      addressType !== "Office" &&
                      "addressDetailsSidepanel_selectedAddress"
                    }`}
                    name={"addressType"}
                    onClick={() => {
                      setAddressType("Others");
                      values.addressType = "Others";
                      setValues(values);
                    }}
                  >
                    Others
                  </div>
                </div>
                <div className={"addressDetailsSidepanel_input_label"}></div>
                {addressType &&
                  addressType !== "Home" &&
                  addressType !== "Office" && (
                    <>
                      <Input
                        name="addressType"
                        className={` ${"addressDetailsSidepanel_inputForm"} ${
                          Boolean(touched.addressType && errors.addressType) &&
                          "addressDetailsSidepanel_errorInput"
                        }`}
                        defaultValue={""}
                        value={
                          values.addressType === "Others"
                            ? ""
                            : values.addressType
                        }
                        onChange={handleChange}
                        placeholder="Enter a name"
                      />
                    </>
                  )}
                {Boolean(touched.addressType && errors.addressType) && (
                  <div className={"addressDetailsSidepanel_errorText"}>
                    {touched.addressType && errors.addressType}
                  </div>
                )}
              </div>
            </div>
            <DummyHeight />
            {props.visible && (
              <Button CartAdd isSidePanel={true}>
                Save Address
              </Button>
            )}
          </form>
        )}
      </Formik>
    </div>
  );
};

let mapStateToProps = (state) => ({
  accessToken: state.loginReducer.verifyOtpSuccess?.Response?.access_token,
  orderTypeOneOrderId: state.uploadImage?.orderId,
  orderTypeTwoOrderId: state.confirmMedicineReducer.ConfirmMedData?.orderId,
  addressData: state.addressData,
  isServiceable: state.addressData.pincodeSuccess,
  pincodeError: state.addressData.pincodeError,
  pincodeDetails: state.pincodeData?.pincodeData,
  pincodeSelectedAddress: state.pincodeSelctedAddress?.pincodeSelectedAddress,
});

export default withRouter(
  connect(mapStateToProps, {
    addAddressThunk,
    checkPincodeThunk,
    resetAddress,
  })(AddressDetailsSidePanel)
);

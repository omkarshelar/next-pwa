///? Default IMPORTS
import React, { useEffect, useState } from "react";
import { withRouter } from "next/router";
import { connect } from "react-redux";

///? Service IMPORTS
import { saveDoctorRatingThunk } from "../../../../redux/DoctorRating/Action";

///? Component & Styling IMPORTS
import { Button, message, Rate } from "antd";
import DoctorConsultationCard from "../../../WebComponents/WaitingScreen/SubComponents/DoctorConsultationCard";
import "./DoctorDetails.module.css";

///? Constants
const DoctorDetails = (props) => {
  const [inputRating, setInputRating] = useState(0);
  const [inputRatingHover, setInputRatingHover] = useState(0);
  const [isErrorShow, setIsErrorShow] = useState(false);
  const [alreadyRated, setAlreadyRated] = useState(false);
  const [isSubmitted, setIsSubmitted] = useState(false);

  //* ComponentDidMount
  useEffect(() => {
    if (props.doctorData?.orderRating > 0) {
      setInputRating(props.doctorData?.orderRating);
      setAlreadyRated(true);
    }
  }, []);

  useEffect(() => {
    if (props.inputRating === 0 && !props.doctorData?.orderRating) {
      setInputRating(0);
    }
  }, [props.inputRating]);

  //* DoctorRating Error Msg
  useEffect(() => {
    message.destroy();
    if (props.doctorRatingData?.setDoctorRatingError) {
      message.error(props.doctorRatingData?.setDoctorRatingError);
    }
  }, [props.doctorRatingData?.setDoctorRatingError]);

  //* DoctorRating Success Msg
  useEffect(() => {
    if (
      props.doctorRatingData?.setDoctorRatingSuccess?.status === "success" &&
      isSubmitted
    ) {
      message.destroy();
      setIsSubmitted(false);
      setAlreadyRated(true);
      props.serviceCall(true);
    }
  }, [props.doctorRatingData?.setDoctorRatingSuccess]);

  //* Submit DoctorRating Func
  const submitRating = () => {
    if (inputRating === 0) {
      setIsErrorShow(true);
      props.serviceCall(false);
    } else {
      setIsErrorShow(false);
      message.loading("Ratings are submitting...", 0);
      let serviceData = {
        access_token: props.accessToken,
        data: {
          customerId: props.customerId,
          orderId: props.orderId,
          doctorId: props.doctorData?.doctorId,
          rating: inputRating,
          comments: "",
        },
      };
      setIsSubmitted(true);
      props.saveDoctorRatingThunk(serviceData);
    }
  };

  return (
    <div className={"DoctorDetails_doctorDetailsWrap"}>
      <DoctorConsultationCard
        isSidePanel={true}
        doctorData={props.doctorData}
        orderId={props.orderid}
        customerId={props.forCustId}
      />
      <div className={"DoctorDetails_doctorDetailsReview"}>
        {!alreadyRated && (
          <div className={"DoctorDetails_reviewTitle"}>
            How would you rate your experience with the doctor?
          </div>
        )}

        <div
          style={{ pointerEvents: alreadyRated ? "none" : "" }}
          className={"DoctorDetails_starWrap"}
        >
          <Rate
            count={5}
            className="doctorStarRating"
            onChange={(e) => {
              setInputRating(e);
              // props.updateRating(e);
            }}
            onMouse={(e) => setInputRatingHover(e)}
            disabled={alreadyRated ? true : false}
            value={inputRating}
          />
        </div>
        {isErrorShow && (
          <p className={"DoctorDetails_errorMsg"}>Please select a rating </p>
        )}
        <div>
          {alreadyRated ? (
            <div className={"DoctorDetails_reviewTitle"}>
              Thanks for the feedback
            </div>
          ) : (
            <Button
              className={"DoctorDetails_submitBtn"}
              onClick={() => submitRating()}
            >
              Submit
            </Button>
          )}
        </div>
      </div>
    </div>
  );
};

const StarInputField = (props) => {
  return (
    <div className={"DoctorDetails_startInputWrap"}>
      {props.rating <= props.inputRating ||
      props.rating <= props.inputRatingHover ? (
        <div
          className={"DoctorDetails_activeStar"}
          onClick={() => (props.disable ? "" : props.onClick(props.rating))}
          onMouseOver={() => (props.disable ? "" : props.onMouse(props.rating))}
          onMouseLeave={() => (props.disable ? "" : props.onMouse(0))}
        ></div>
      ) : (
        <div
          className={"DoctorDetails_deactiveStar"}
          onClick={() => {
            return props.disable ? "" : props.onClick(props.rating);
          }}
          onMouseOver={() => (props.disable ? "" : props.onMouse(props.rating))}
        ></div>
      )}
    </div>
  );
};

const mapStateToProps = (state) => ({
  accessToken: state.loginReducer?.verifyOtpSuccess?.Response?.access_token,
  userDto: state.loginReducer?.verifyOtpSuccess?.CustomerDto,
  doctorRatingData: state.doctorRatings,
});

export default withRouter(
  connect(mapStateToProps, { saveDoctorRatingThunk })(DoctorDetails)
);

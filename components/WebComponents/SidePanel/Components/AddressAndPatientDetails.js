import React, { createRef } from "react";
import "./AddressAndPatientDetails.css";
import { connect } from "react-redux";
import { withRouter } from "next/router";
import {
  fetchpatientThunk,
  selectPatinetForOrderAction,
  savePatientCheckoutThunk,
} from "../../../../redux/PatientDetails/Action";
import {
  fetchAddressThunk,
  checkPincodeThunk,
  selectAdresssForCheckoutAction,
  saveAddressThunk,
} from "../../../../redux/AddressDetails/Action";
import {
  pincodeSectionAction,
  saveSelectedAddress,
  fetchMedicineThunkPostSelect,
} from "../../../../redux/Pincode/Actions";
import { fetchPatientByIdThunk } from "../../../../redux/PatientDetails/Action";
import { orderStatusThunk } from "../../../../redux/OrderDetail/Action";
import Button from "../../Button/Button";
import { message } from "antd";
import AndroidSeperator from "../../AndroidSeperator/AndroidSeperator";
import HeaderWithCTA from "./SubComponents/HeaderWithCTA";
import PatientCard from "./SubComponents/PatientCard";
import AddressCard from "./SubComponents/AddressCard";
import AddNewDetailsBtn from "./SubComponents/AddNewDetailsBtn";
import DummyHeight from "./SubComponents/DummyHeight";
import { customNotification } from "../../../Helper/helperFunction";
import directionIcon from "../../../../src/Assets/paymentMethodChevron.svg";
import disabledDirectionArrow from "../../../../src/Assets/chevron_grey.svg";
// import {
//   eventAddressSelected,
//   eventPatientSelected,
//   eventSummaryScreen,
// } from "../../../../src/Events/Events";
import window from "global";
let options = {
  root: null,
  rootMargin: "0px",
};
class AddressAndPatientDetails extends React.Component {
  state = {
    selectedAddressId: 0,
    selectedPatientId: 0,
    addressList: this.props.addressList || [],
    unServiciableArray: [],
    isVisible: false,
    isVisibleSticky: true,
    isLeftDisabled: true,
  };
  ListRef = createRef();
  ListRefWrap = createRef();
  stickyRef = createRef();
  scrollRight = () => {
    this.useIntersection();
    if (this.ListRef.current) {
      let currentValue = this.ListRef.current.scrollLeft + 180;
      this.ListRef.current.scrollLeft = currentValue;
      if (currentValue > 0) {
        this.setState({
          isLeftDisabled: false,
        });
      } else {
        this.setState({
          isLeftDisabled: true,
        });
      }
    }
  };
  scrollLeft = () => {
    this.useIntersection();
    if (this.ListRef.current) {
      let currentValue = this.ListRef.current.scrollLeft - 180;
      this.ListRef.current.scrollLeft = currentValue;
      if (currentValue <= 1) {
        this.setState({
          isLeftDisabled: true,
        });
      } else {
        this.setState({
          isLeftDisabled: false,
        });
      }
    }
  };
  callBackFunction = (e) => {
    const [entry] = e;
    let { isVisible } = this.state;
    if (isVisible !== entry.intersecting) {
      this.setState({ isVisible: entry.isIntersecting });
    }
  };

  useIntersection = () => {
    const observer = new IntersectionObserver(this.callBackFunction, options);
    if (this.ListRefWrap.current) {
      observer.observe(this.ListRefWrap.current);
    }
    return () => {
      if (this.ListRefWrap.current) {
        observer.unobserve(this.ListRefWrap.current);
      }
    };
  };

  callBackFunctionSticky = (e) => {
    const [entry] = e;
    this.setState({ isVisibleSticky: entry.isIntersecting });
  };
  useIntersectionSticky = () => {
    const observerSticky = new IntersectionObserver(
      this.callBackFunctionSticky,
      options
    );
    if (this.stickyRef.current) {
      observerSticky.observe(this.stickyRef.current);
    }
    return () => {
      if (this.stickyRef.current) {
        observerSticky.unobserve(this.stickyRef.current);
      }
    };
  };
  componentDidMount = () => {
    this.fetchPatientData();
    this.fetchAddressData();
    if (this.props.updateDetail?.AddressDetails) {
      this.setSelectedPincode();
    }
    if (this.props.updateDetail?.patientId) {
      this.setState({
        selectedPatientId: this.props.updateDetail.patientId,
      });
    }
    this.useIntersection();
  };

  setSelectedPincode = () => {
    this.props
      .checkPincodeThunk({
        pincode: this.props.updateDetail.AddressDetails.pincode,
        history: this.props.router,
      })
      .then(() => {
        if (
          !this.props.pincodeError &&
          this.props.isServiceable?.isServiceable
        ) {
          this.setState({
            selectedAddressId: this.props.updateDetail.AddressDetails.addressId,
          });
        } else {
          let { unServiciableArray } = this.state;
          if (
            !unServiciableArray.includes(
              this.props.updateDetail.AddressDetails.addressId
            )
          ) {
            unServiciableArray.push(
              this.props.updateDetail.AddressDetails.addressId
            );
          }
          this.setState({
            selectedAddressId: 0,
            unServiciableArray,
          });
        }
      });
  };

  componentDidUpdate = (prevProps) => {
    if (this.props.visible != prevProps.visible && this.props.visible) {
      if (this.props.updateDetail?.AddressDetails) {
        this.setSelectedPincode();
      }
      if (this.props.updateDetail?.patientId) {
        this.setState({
          selectedPatientId: this.props.updateDetail.patientId,
        });
      }
    }
    if (this.props.addressList != prevProps.addressList) {
      this.setState({
        addressList: this.props.addressList,
      });
    }
  };

  fetchAddressData = () => {
    if (this.props.accessToken) {
      let data = {
        accessToken: this.props.accessToken,
        history: this.props.router,
      };
      this.props.fetchAddressThunk(data).then(() => {
        if (this.props.addressData.fetchAddressError) {
          message.warning("something went wrong");
        } else {
          if (this.props.pincodeSelectedAddress) {
            this.saveAddressForCheckout(
              this.props.pincodeSelectedAddress.addressId
            );
          }
        }
      });
    }
  };

  saveAddressForCheckout = (addressId) => {
    if (this.props.addressList) {
      let idFound = this.props.addressList.find(
        (data) => data.addressId === addressId
      );

      if (idFound) {
        this.props.selectAdresssForCheckoutAction(idFound);
      }
    }
  };

  fetchPatientData = () => {
    if (this.props.accessToken) {
      let data = {
        accessToken: this.props.accessToken,
        history: this.props.router,
      };
      this.props.fetchpatientThunk(data).then(() => {
        if (this.props.patientContent?.fetchPatientError) {
          message.error("something went wrong");
        } else {
        }
      });
    }
  };

  savePincode = (data) => {
    this.props.pincodeSectionAction(data);
  };

  selectPatientHandler = (patientId) => {
    let data = this.props.patientsList?.find(
      (data) => data.patientId === patientId
    );
    // Event_Truemeds
    // new
    // eventPatientSelected();
    this.props.selectPatinetForOrderAction(data);
  };

  selectAddressHandler = (addressId) => {
    let idFound = this.props.addressList.find(
      (data) => data.addressId === addressId
    );

    this.props.saveSelectedAddress(idFound);

    this.props.selectAdresssForCheckoutAction(idFound);

    // this.savePincode({ pincode: idFound.pincode, city: idFound.cityName });
  };
  handleAddressSelect = (details) => {
    let { unServiciableArray, selectedAddressId } = this.state;

    let data = {
      pincode: details.pincode,
      history: this.props.router,
    };

    if (selectedAddressId != details.addressId) {
      this.props.checkPincodeThunk(data).then(() => {
        if (
          !this.props.pincodeError &&
          this.props.isServiceable?.isServiceable
        ) {
          if (unServiciableArray.includes(details.addressId)) {
            unServiciableArray.splice(
              unServiciableArray.indexOf(details.addressId),
              1
            );
          }
          this.setState(
            {
              selectedAddressId: details.addressId,
              unServiciableArray: unServiciableArray,
            },
            () => {
              this.selectAddressHandler(details.addressId);
            }
          );
        } else {
          if (!unServiciableArray.includes(details.addressId)) {
            unServiciableArray.push(details.addressId);
          }
          this.setState(
            {
              unServiciableArray: unServiciableArray,
            },
            () => {
              customNotification(
                "Pincode Unserviciable",
                "customSidePanelNotify"
              );
            }
          );
        }
      });
    }
  };
  handleSave = () => {
    let {
      accessToken,
      orderTypeOneOrderId,
      orderTypeTwoOrderId,
      patientsList,
      addressList,
    } = this.props;

    const promisePateint = new Promise((resolve, reject) => {
      let idFound = patientsList.find(
        (data) => data.patientId === this.state.selectedPatientId
      );
      if (idFound?.patientId) {
        if (accessToken && (orderTypeOneOrderId || orderTypeTwoOrderId)) {
          let data = {
            accessToken: accessToken,
            patientId: idFound.patientId,
            orderId: orderTypeOneOrderId || orderTypeTwoOrderId,
            history: this.props.router,
          };
          this.props.savePatientCheckoutThunk(data).then(() => {
            resolve();
          });
        }
      }
    });

    const promiseAddress = new Promise((resolve, reject) => {
      let idFound1 = addressList.find(
        (data) => data.addressId === this.state.selectedAddressId
      );
      if (idFound1?.addressId) {
        if (accessToken && (orderTypeOneOrderId || orderTypeTwoOrderId)) {
          let payload = {
            accessToken: this.props.accessToken,
            addressid: idFound1.addressId,
            history: this.props.router,
            orderid:
              this.props.orderTypeOneOrderId || this.props.orderTypeTwoOrderId,
          };
          this.props.saveAddressThunk(payload).then(() => {
            if (this.props.addressData?.checkoutAddressError) {
              message.error(this.props.addressData?.checkoutAddressError);
            } else {
              // Event_Truemeds
              // new
              // eventAddressSelected();
              // Event_Truemeds
              // new
              // eventSummaryScreen();
              this.savePincode(idFound1);
              resolve();
            }
          });
        }
      }
    });

    if (
      this.props.patientsList.length > 0 &&
      this.props.addressList.length > 0
    ) {
      Promise.all([promisePateint, promiseAddress]).then(() => {
        this.saveOrder();
      });
    } else if (this.props.patientsList.length > 0) {
      Promise.all([promisePateint]).then(() => {
        this.saveOrder();
      });
    } else {
      Promise.all([promiseAddress]).then(() => {
        this.saveOrder();
      });
    }
  };

  saveOrder = () => {
    this.props
      .orderStatusThunk({
        access_token: this.props.accessToken,
        orderIds:
          (this.props.medConfirm.ConfirmMedData &&
            this.props.medConfirm.ConfirmMedData.orderId) ||
          this.props.updateDetail.orderId,
        customerId: this.props.forCustId,
        history: this.props.router,
      })
      .then(() => {
        this.props.fetchPatientByIdThunk({
          accessToken: this.props.accessToken,
          orderId:
            (this.props.medConfirm.ConfirmMedData &&
              this.props.medConfirm.ConfirmMedData.orderId) ||
            this.props.updateDetail.orderId,
          patientId: 0,
          history: this.props.router,
        });
        window.openSideBar(false, 0);
      });
  };
  render() {
    return (
      <div
        className={"addressPatientDetails_outerWrap"}
        onMouseMove={() => {
          this.useIntersection();
        }}
      >
        <div className={"addressPatientDetails_contentSectionWrap"}>
          <div>
            <HeaderWithCTA
              title={"Your Patients"}
              ctaText={"+ Add New Patient"}
              onCta={() => {
                window.openSideBar(true, 5, { isAdd: true });
              }}
              showCta={
                this.props.patientsList && this.props.patientsList.length > 0
              }
            />
            <div
              className={"addressPatientDetails_patientCardWrap"}
              ref={this.ListRef}
            >
              {this.props.patientsList && this.props.patientsList.length > 0 ? (
                this.props.patientsList?.map((e) => {
                  return (
                    <PatientCard
                      details={e}
                      selected={e.patientId === this.state.selectedPatientId}
                      selectionHandler={() => {
                        this.setState(
                          {
                            selectedPatientId: e.patientId,
                          },
                          () => {
                            this.selectPatientHandler(e.patientId);
                          }
                        );
                      }}
                    />
                  );
                })
              ) : (
                <div style={{ width: "100%" }}>
                  <div className={"addressPatientDetails_addSubText"}>
                    Please add patient details to continue
                  </div>
                  <AddNewDetailsBtn
                    addText={"Add Patient"}
                    clickHandler={() =>
                      window.openSideBar(true, 5, { isAdd: true })
                    }
                  />
                </div>
              )}
              {this.props.patientsList &&
                this.props.patientsList.length > 0 && (
                  <div style={{ marginLeft: "-15px" }} ref={this.ListRefWrap} />
                )}
            </div>
            {this.props.patientsList &&
            this.props.patientsList.length > 1 &&
            window.innerWidth > 768 ? (
              <div className={"addressPatientDetails_buttonWraps"}>
                <div onClick={() => this.scrollLeft()}>
                  <img
                    className={"addressPatientDetails_leftArrow"}
                    src={
                      this.state.isLeftDisabled
                        ? disabledDirectionArrow
                        : directionIcon
                    }
                    alt="left"
                  />
                </div>
                <div onClick={() => this.scrollRight()}>
                  <img
                    className={"addressPatientDetails_rightArrow"}
                    src={
                      this.state.isVisible
                        ? disabledDirectionArrow
                        : directionIcon
                    }
                    alt="right"
                  />
                </div>
              </div>
            ) : (
              ""
            )}
          </div>
          <div id="refText" ref={this.stickyRef1}></div>
        </div>
        <AndroidSeperator showWeb={true} showMob={true} />
        <div className={"addressPatientDetails_contentSectionWrap"}>
          <div>
            {!this.state.isVisibleSticky && <DummyHeight height={"60px"} />}
            <HeaderWithCTA
              title={"Saved Addresses"}
              ctaText={"+ Add New Address"}
              onCta={() => window.openSideBar(true, 4)}
              showCta={
                this.state.addressList && this.state.addressList.length > 0
              }
              id={"addressHeader"}
              // isVisibleSticky={this.state.isVisibleSticky}
              shouldSticky={true}
            />
            {this.state.addressList && this.state.addressList.length > 0 ? (
              this.state.addressList?.map((e) => {
                return (
                  <AddressCard
                    details={e}
                    selected={e.addressId === this.state.selectedAddressId}
                    selectionHandler={() => {
                      this.handleAddressSelect(e);
                    }}
                    unServiciableArray={this.state.unServiciableArray}
                  />
                );
              })
            ) : (
              <>
                <div className={"addressPatientDetails_addSubText"}>
                  Please add the add the delivery address
                </div>
                <AddNewDetailsBtn
                  addText={"Add Address"}
                  clickHandler={() => window.openSideBar(true, 4)}
                />
              </>
            )}
          </div>
        </div>
        <DummyHeight />
        {this.props.visible && (
          <Button
            CartAdd
            isSidePanel={true}
            onClick={() => {
              this.handleSave();
            }}
          >
            Save
          </Button>
        )}
      </div>
    );
  }
}

let mapStateToProps = (state) => ({
  accessToken: state.loginReducer.verifyOtpSuccess?.Response?.access_token,
  patientsList: state.patientData.fetchPatientSuccess,
  user: state.loginReducer.verifyOtpSuccess?.CustomerDto,
  imgUpload: state.uploadImage?.uploadHistory,
  orderTypeOneOrderId: state.uploadImage?.orderId,
  orderTypeTwoOrderId: state.confirmMedicineReducer.ConfirmMedData?.orderId,
  Stepper: state.stepper,
  isLoading: state.loader.isLoading,
  patientContent: state.patientData,
  allPrescriptions: state.uploadImage,
  custId: state.loginReducer.customerDetails?.CustomerDetails?.customerId,
  isServiceable: state.addressData.pincodeSuccess,
  addressList: state.addressData.fetchAddressSuccess,
  addressData: state.addressData,
  cartContent: state.cartData?.cartItems,
  pincodeDetails: state.pincodeData?.pincodeData,
  pincodeSelectedAddress: state.pincodeSelctedAddress?.pincodeSelectedAddress,
  updateDetail: state.orderStatusUpdate?.OrderStatusData,
  medConfirm: state.confirmMedicineReducer,
  forCustId: state.loginReducer?.verifyOtpSuccess?.CustomerId,
  pincodeError: state.addressData.pincodeError,
});

export default withRouter(
  connect(mapStateToProps, {
    fetchpatientThunk,
    selectPatinetForOrderAction,
    fetchAddressThunk,
    checkPincodeThunk,
    selectAdresssForCheckoutAction,
    fetchMedicineThunkPostSelect,
    pincodeSectionAction,
    saveAddressThunk,
    saveSelectedAddress,
    savePatientCheckoutThunk,
    orderStatusThunk,
    fetchPatientByIdThunk,
  })(AddressAndPatientDetails)
);

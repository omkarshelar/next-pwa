import React, { useEffect, useState } from "react";
import window from "global";
import "./SidePanel.css";
import { Drawer } from "antd";
import { IoCloseOutline } from "react-icons/io5";
import backIcon from "../../../src/Assets/sidepanel-back.svg";
import { allComponents } from "./Components/SidePanelComponents";
import ProductDetails from "../../../pages/medicine/[id]";
import PastPresSelectPatient from "./Components/PastPresSelectPatient/PastPresSelectPatient";
import PastPresSelectPrescription from "./Components/PastPresSelectPrescription/PastPresSelectPrescription";
import AddressAndPatientDetails from "./Components/AddressAndPatientDetails";
import AddressDetailsSidePanel from "./Components/AddressDetailsSidePanel";
import PatientDetailsSidePanel from "./Components/PatientDetailsSidePanel";
import DoctorDetails from "./Components/DoctorDetails";
import CompareOrder from "./Components/CompareOrder/CompareOrder";
import SelectPatientFilter from "./Components/SelectPatientFilter";

export default function SidePanel(props) {
  const [back, setBack] = useState(false);
  const [inputRating, setInputRating] = useState(props.selectedData?.ratings);

  const getTitleById = (arr, id) => {
    let title = "";
    for (let i in arr) {
      let active = arr[i];
      if (active.id === id) {
        title = active.title;
        break;
      }
    }
    return title;
  };

  const getCloseById = (arr, id) => {
    let isClose = false;
    for (let i in arr) {
      let active = arr[i];
      if (active.id === id) {
        isClose = active.close ? true : false;
        break;
      }
    }
    return isClose;
  };

  const getBackById = (arr, id) => {
    let isBack = false;
    for (let i in arr) {
      let active = arr[i];
      if (active.id === id) {
        isBack = active.back ? true : false;
        break;
      }
    }
    return isBack;
  };

  useEffect(() => {
    if (back) {
      setBack(false);
    }
  }, [back]);

  useEffect(() => {
    setInputRating(props.selectedData?.ratings);
  }, [props.selectedData]);
  return (
    <>
      <Drawer
        className={`reusable-side-panel 
        ${
          props.activeComponent === 11
            ? "resuable-side-panel-doctorFilter"
            : props.activeComponent === 13
            ? "resuable-side-panel-patientFilter"
            : ""
        }`}
        visible={props.visible}
        title={
          <>
            {getBackById(allComponents, props.activeComponent) && (
              <img
                src={backIcon}
                style={{ marginRight: "14px", cursor: "pointer" }}
                onClick={() => setBack(true)}
                alt=""
              />
            )}
            <span className="sidePanelTitle">
              {getTitleById(allComponents, props.activeComponent)}
            </span>
          </>
        }
        placement={window.innerWidth > 480 ? "right" : "bottom"}
        closable={
          window.innerWidth > 480
            ? getCloseById(allComponents, props.activeComponent)
            : false
        }
        onClose={props.onCloseFunc}
      >
        {props.visible && window.innerWidth <= 480 && (
          <div className="newClose" onClick={props.onCloseFunc}>
            <IoCloseOutline />
          </div>
        )}
        {props.activeComponent === 1 ? (
          <PastPresSelectPatient />
        ) : props.activeComponent === 2 ? (
          <PastPresSelectPrescription
            isBack={back}
            pastPresSelectedPatient={props.selectedData}
            visible={props.visible}
          />
        ) : props.activeComponent === 3 ? (
          <AddressAndPatientDetails visible={props.visible} />
        ) : props.activeComponent === 4 ? (
          <AddressDetailsSidePanel
            isBack={back}
            data={props.selectedData}
            visible={props.visible}
          />
        ) : props.activeComponent === 5 ? (
          <PatientDetailsSidePanel
            isBack={back}
            data={props.selectedData?.details}
            visible={props.visible}
            isAdd={props.selectedData?.isAdd}
          />
        ) : props.activeComponent === 10 ? (
          <ProductDetails
            medQty={props.selectedData.medQty}
            medId={props.selectedData.medId}
            subsDetails={props.selectedData.subsDetails}
            viewOriginal={props.selectedData.viewOriginal}
            medSavings={props.selectedData.medSavings}
            orderStatus={props?.selectedData?.orderStatus}
          />
        ) : props.activeComponent === 11 ? (
          <DoctorDetails
            doctorData={props.selectedData.doctorData}
            orderId={props.selectedData.orderId}
            customerId={props.selectedData.customerId}
            ratings={props.selectedData.ratings}
            serviceCall={props.selectedData.serviceCall}
          />
        ) : props.activeComponent === 12 ? (
          <CompareOrder
            orderData={props.selectedData.orderData}
            medData={props.selectedData.medData}
          />
        ) : props.activeComponent === 13 ? (
          <SelectPatientFilter
            patientList={props.selectedData.patientList}
            patientFilter={props.selectedData.patientFilter}
            visible={props.visible}
          />
        ) : null}
      </Drawer>
    </>
  );
}

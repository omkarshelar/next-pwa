const getDateInFormat = (date) => {
  var mm = date.getMonth() + 1;
  var dd = date.getDate();
  return [
    date.getFullYear(),
    (mm > 9 ? "" : "0") + mm,
    (dd > 9 ? "" : "0") + dd,
  ].join("/");
};

const getTimeInFormat = (date) => {
  var currentOffset = date.getTimezoneOffset();
  var ISTOffset = 330; // IST offset UTC +5:30
  var ISTTime = new Date(date.getTime() + (ISTOffset + currentOffset) * 60000);
  var hh = ISTTime.getHours();
  var mm = ISTTime.getMinutes();
  var ss = ISTTime.getSeconds();
  return [hh, (mm > 9 ? "" : "0") + mm, (ss > 9 ? "" : "0") + ss].join(":");
};

const createDefaultToken = () => {
  let date = getDateInFormat(new Date());
  let time = getTimeInFormat(new Date());
  let str = "TM_WEBSITE_V_2.0.9-" + date + " " + time;
  let token = Buffer.from(str).toString("base64");
  return token;
};

export default createDefaultToken;

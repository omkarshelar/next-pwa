import React, { Component } from "react";
import { Spinner } from "react-bootstrap";
import truemedsLogo from "../../../src/Assets/Truemedslogo.svg";
import { sliderInfoData } from "../SliderInfo/SliderInfoData";
import * as Yup from "yup";
import { Formik } from "formik";
import Checkbox from "@material-ui/core/Checkbox";
import Typography from "@material-ui/core/Typography";
import Button from "../../WebComponents/Button/Button";
import { connect } from "react-redux";
import {
  fetchSessionTokenThunk,
  fetchDefaultTokenThunk,
  generateOtpThunk,
  verifyOtpThunk,
} from "../../../redux/Login/Action";
import {
  CarouselWrapper,
  ModalStyle,
  Carouselitem,
  Loginwrapper,
  Textfield,
  OtpWrapper,
  CarouseStyle,
  FormWrapper,
  Formcontrollabel,
} from "./Login.style";
import { message } from "antd";
import OtpInput from "react-otp-input";
import { orderTrackerThunk } from "../../../redux/MyOrder/Action";
import ResendOTP from "./ResendOTP/ResendOTP";
import {
  eventCompleteLogin,
  eventCompleteRegistration,
  eventOtpLogin,
  eventOtpRegistration,
} from "../../../src/Events/Events";
import next from "./chevron-next.png";
import prev from "./chevron-prev.png";
import firebase from "../../../src/Firebase/Firebase";
import createDefaultToken from "./CreateDefaultToken";
import { customNotification } from "../../Helper/helperFunction";
import { REACT_VAPID_KEY } from "../../../constants/Urls.js";
import Router, { withRouter } from "next/router";
import window from "global";
import Cookies from "js-cookie";
import { getMasterDetailsThunkNew } from "../../../redux/SliderService/Actions";
const publicKey = REACT_VAPID_KEY;
const messaging = null;
if (typeof window !== "undefined" && firebase.messaging.isSupported()) {
  firebase.messaging();
}

export class Login extends Component {
  state = {
    initalState: {
      phone: null,
      tnc: true,
    },
    toggleverifySection: false,
    OTP: "",
    disableButton: false,
    disableButtonVerifyOTP: false,
    currentToken: null,
    phoneNumber: "",
    errorMessage: "",
    successMessage: "",
  };

  componentDidMount() {
    if (this.props.loginModal) {
      if (messaging && Notification.permission !== "granted") {
        message.info({
          content: "Please allow notifications for better experience",
          style: { marginTop: "85vh" },
        });
      }
      this.getFcmToken();
    }
  }

  componentDidUpdate() {
    if (
      messaging &&
      !this.state.currentToken &&
      Notification.permission === "granted"
    ) {
      this.getFcmToken();
    }
  }

  getFcmToken = () => {
    // get fcm token
    if (messaging) {
      messaging
        .getToken({ vapidKey: publicKey })
        .then((currentToken) => {
          this.setState({ currentToken: currentToken });
        })
        .catch((err) => {
          console.log("An error occurred while retrieving token.", err);
        });
    }
  };

  handleSubmitHandler = async (values, phoneNumber) => {
    this.setState({
      disableButton: true,
      errorMessage: "",
      successMessage: "",
    });
    localStorage.setItem("UserPhone", phoneNumber ? phoneNumber : values.phone);
    this.setState({ phoneNumber: phoneNumber ? phoneNumber : values.phone });
    if (this.state.currentToken) {
      // send fcm token to get session id
      this.props
        .fetchSessionTokenThunk({
          sessionTokenReq: {
            token: this.state.currentToken,
            client: "TM_WEBSITE_V_2.0.9",
          },
          history: Router,
        })
        .then(() => {
          if (this.props.login?.fetchSessionTokenSuccess) {
            this.props
              .generateOtpThunk({
                mobileOtpRequest: {
                  mobileNum: phoneNumber ? phoneNumber : values.phone,
                  sessionToken: this.props.login.fetchSessionTokenSuccess,
                },
                history: Router,
              })
              .then(() => {
                this.setState({ disableButton: false });
                if (this.props.login.generateOtpError) {
                  this.setState({
                    errorMessage: this.props.login.generateOtpError,
                  });
                } else {
                  if (this.props.login?.generateOtpSuccess?.isNewCustomer) {
                    eventOtpRegistration();
                  } else {
                    eventOtpLogin();
                  }
                  this.setState({
                    successMessage: this.props.login?.generateOtpSuccess[201],
                    toggleverifySection: true,
                  });
                }
              })
              .catch(() => {
                console.log("Error");
              });
          } else {
            message.error("Invalid request. Please try again later");
            this.setState({ disableButton: false });
          }
        });
    } else {
      let defaultToken = createDefaultToken();
      // send the default token to get fcm token
      this.props
        .fetchDefaultTokenThunk({
          defaultTokenRequest: defaultToken,
          history: Router,
        })
        .then(() => {
          if (this.props.login?.fetchDefaultTokenSuccess) {
            this.props
              .fetchSessionTokenThunk({
                sessionTokenReq: {
                  token: this.props.login.fetchDefaultTokenSuccess,
                  client: "TM_WEBSITE_V_2.0.9",
                },
                history: Router,
              })
              .then(() => {
                if (this.props.login?.fetchSessionTokenSuccess) {
                  this.props
                    .generateOtpThunk({
                      mobileOtpRequest: {
                        mobileNum: phoneNumber ? phoneNumber : values.phone,
                        sessionToken: this.props.login.fetchSessionTokenSuccess,
                      },
                      history: Router,
                    })
                    .then(() => {
                      if (this.props.login.generateOtpError) {
                        this.setState({
                          errorMessage: this.props.login.generateOtpError,
                        });
                        this.setState({ disableButton: false });
                      } else {
                        this.setState({
                          successMessage:
                            this.props.login?.generateOtpSuccess[201],
                          toggleverifySection: true,
                          disableButton: false,
                        });
                      }
                    });
                } else {
                  message.error("Invalid request. Please try again later");
                  this.setState({ disableButton: false });
                }
              });
          } else {
            message.error("Invalid request. Please try again later");
            this.setState({ disableButton: false });
          }
        });
    }
  };
  handleSubmitOtpHandler = (otp) => {
    this.setState({ disableButtonVerifyOTP: true, errorMessage: "" });
    this.props
      .verifyOtpThunk({
        mobileNo: localStorage.getItem("UserPhone"),
        otp,
        history: Router,
      })
      .then(() => {
        if (this.props.login.verifyOtpError) {
          this.setState({
            errorMessage: this.props.login.verifyOtpError,
          });
          this.setState({ disableButtonVerifyOTP: false });
        } else {
          if (this.props.login?.verifyOtpSuccess?.Response?.access_token) {
            Cookies.set(
              "token",
              this.props.login?.verifyOtpSuccess?.Response?.access_token,
              { path: "" }
            );
            Cookies.set(
              "customerId",
              this.props.login?.verifyOtpSuccess?.CustomerId,
              { path: "" }
            );
          }
          if (this.props.login?.verifyOtpSuccess?.isNewCustomer) {
            eventCompleteRegistration(
              this.props.login.verifyOtpSuccess.CustomerDto.customerId
            );
          } else {
            eventCompleteLogin(
              this.props.login.verifyOtpSuccess.CustomerDto.customerId
            );
          }
          this.props.loginModalHandler();
          this.props.orderTrackerThunk({
            accessToken: this.props.accessToken,
            disableButtonVerifyOTP: false,
          });
          let params = {};
          params.history = this.props.history;
          if (this.props.accessToken) {
            params.accessToken = this.props.accessToken;
            params.customerId = this.props.login?.verifyOtpSuccess?.CustomerId;
          }
          this.props.getMasterDetailsThunkNew(params);
          customNotification("Hey! You are succesfully logged in");
        }
      });
  };

  loginTimeOut = () => {
    let phoneNumber = Object.assign({}, this.state.initalState);
    phoneNumber.phone = localStorage.getItem("UserPhone");
    this.setState({
      toggleverifySection: false,
      initalState: phoneNumber,
      OTP: "",
    });
  };

  onChangeHandler = (value) => {
    this.setState({ OTP: value }, () => {
      if (this.state.OTP?.length === 4) {
        this.handleSubmitOtpHandler(this.state.OTP);
      }
    });
  };

  render() {
    let { loginModal, loginModalHandler } = this.props;

    return (
      <ModalStyle show={loginModal} onHide={loginModalHandler} centered>
        <CarouselWrapper>
          <CarouseStyle
            indicators={false}
            interval={null}
            next={next}
            prev={prev}
          >
            {sliderInfoData.map((content) => (
              <Carouselitem key={content.id}>
                <div className="carousel-item-wrapper">
                  <img alt="slide" src={content.imgSrc} />
                  <span style={{ marginTop: "15px" }}>{content.header}</span>
                  <p>{content.Info}</p>
                </div>
              </Carouselitem>
            ))}
          </CarouseStyle>
        </CarouselWrapper>
        <Loginwrapper>
          <img src={truemedsLogo} alt="slide"></img>
          {this.state.toggleverifySection ? (
            <FormWrapper>
              <h4 className="header-credential">Verify OTP</h4>
              <div className="header-otp-details">
                <span>Enter the OTP sent on</span>
                <span>{this.state.phoneNumber}</span>
                <span onClick={this.loginTimeOut}>Edit</span>
              </div>
              <OtpWrapper>
                <OtpInput
                  value={this.state.OTP}
                  onChange={this.onChangeHandler}
                  numInputs={4}
                  name="OTP"
                  isInputNum={true}
                  shouldAutoFocus={true}
                  containerStyle={
                    this.state.errorMessage ? "otp-input-error" : ""
                  }
                />
              </OtpWrapper>
              <ResendOTP
                seconds={59}
                onClickResend={() =>
                  this.handleSubmitHandler(null, this.state.phoneNumber)
                }
                errorMessage={this.state.errorMessage}
                successMessage={this.state.successMessage}
              />
            </FormWrapper>
          ) : (
            <Formik
              initialValues={this.state.initalState}
              validationSchema={Yup.object().shape({
                phone: Yup.string()
                  .matches(/^[6-9]\d{9}$/, "Please enter a valid mobile number")
                  .required("Please enter a valid mobile number"),
                tnc: Yup.boolean().oneOf(
                  [true],
                  "Please accept Terms and Conditions to proceed."
                ),
              })}
              onSubmit={(values) => {
                this.handleSubmitHandler(values);
              }}
            >
              {({ errors, handleSubmit, touched, values, handleChange }) => (
                <>
                  <form noValidate onSubmit={handleSubmit}>
                    <FormWrapper>
                      <h4 className="header-credential">
                        Enter your phone number
                      </h4>
                      <Textfield
                        name="phone"
                        label="Enter Mobile Number"
                        type={"number"}
                        value={values.phone}
                        onChange={handleChange}
                        error={Boolean(touched.phone && errors.phone)}
                        onInput={(e) => {
                          e.target.value = Math.max(0, parseInt(e.target.value))
                            .toString()
                            .slice(0, 10);
                        }}
                        style={{ marginBottom: "5px" }}
                      />
                      {touched.phone && errors.phone && (
                        <div style={{ marginTop: "10px" }}>
                          <span style={{ color: "#e74c3c", fontWeight: "500" }}>
                            Please enter valid mobile number
                          </span>
                        </div>
                      )}
                      <Formcontrollabel
                        control={
                          <Checkbox
                            checked={values.tnc}
                            onChange={handleChange}
                            name="tnc"
                            color="primary"
                          />
                        }
                        label={
                          <Typography
                            style={{
                              display: "block",
                            }}
                          >
                            By logging in, I agree with{" "}
                            <span
                              style={{
                                color: "#1890ff",
                                textDecoration: "none",
                              }}
                              onClick={() => {
                                this.props.loginModalHandler();
                                Router.push("/option/legals/tnc");
                              }}
                            >
                              Terms and Conditions
                            </span>
                          </Typography>
                        }
                      />
                      {errors.tnc && touched.tnc && (
                        <div>
                          <span style={{ color: "#e74c3c", fontWeight: "500" }}>
                            {errors.tnc}
                          </span>
                        </div>
                      )}

                      <Button
                        Buttonlogin
                        type="submit"
                        disabled={this.state.disableButton}
                      >
                        {this.state.disableButton ? (
                          <Spinner
                            as="span"
                            animation="border"
                            size="sm"
                            role="status"
                            aria-hidden="true"
                          />
                        ) : (
                          <>Generate OTP</>
                        )}
                      </Button>
                    </FormWrapper>
                  </form>
                </>
              )}
            </Formik>
          )}
        </Loginwrapper>
      </ModalStyle>
    );
  }
}

let mapStateToProps = (state) => ({
  login: state.loginReducer,
  accessToken: state.loginReducer.verifyOtpSuccess?.Response?.access_token,
});

export default withRouter(
  connect(mapStateToProps, {
    fetchSessionTokenThunk,
    fetchDefaultTokenThunk,
    generateOtpThunk,
    verifyOtpThunk,
    orderTrackerThunk,
    getMasterDetailsThunkNew,
  })(Login)
);

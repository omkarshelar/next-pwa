import FormControlLabel from "@material-ui/core/FormControlLabel";
import TextField from "@material-ui/core/TextField";
import { Modal, CarouselItem, Carousel } from "react-bootstrap";
import styled from "styled-components";
// Card container
export const Loginwrapper = styled.div`
  box-sizing: border-box;
  max-width: 50%;
  width: 50%;
  padding: 50px 30px;
  display: flex;
  justify-content: space-evenly;
  flex-flow: column;

  > img {
    width: 170px;
    align-self: center;
  }
  .header-credential {
    /* font-family: "Century Gothic", sans-serif; */
    font-size: 16px;
    font-style: normal;
    font-weight: 700;
    line-height: 18px;
    letter-spacing: 0em;
    margin: 1rem 0;
    @media screen and (max-width: 468px) {
      margin-top: 1em;
      font-weight: 600;
    }
  }
  .header-otp-details {
    display: flex;
    align-items: center;
    margin-bottom: 12px;
  }
  .header-otp-details span {
    /* font-family: "Century Gothic", sans-serif; */
    font-size: 14px;
    @media screen and (max-width: 468px) {
      font-size: 12px;
    }
  }
  .header-otp-details span:nth-child(1) {
    color: #333;
    margin-right: 5px;
    font-weight: 500;
  }
  .header-otp-details span:nth-child(2) {
    color: #333;
    margin-right: 10px;
    font-weight: 600;
  }
  .header-otp-details span:nth-child(3) {
    color: #0071bc;
    font-weight: 600;
    cursor: pointer;
    text-decoration: underline;
  }
  .otp-input-error div {
    border: 1px solid #e74c3c;
  }
  .otp-success-message {
    margin-top: 5px;
    display: flex;
    align-items: center;
  }
  .otp-success-message div {
    border: none;
    background-color: #23b574;
    width: 18px;
    height: 18px;
    border-radius: 18px;
    display: flex;
    align-items: center;
    justify-content: center;
    margin-right: 5px;
    @media screen and (max-width: 468px) {
      width: 15px;
      height: 15px;
      border-radius: 15px;
    }
  }
  .otp-success-message div img {
    width: 10px;
    @media screen and (max-width: 468px) {
      width: 8px;
    }
  }
  .otp-success-message span {
    color: #333;
    font-weight: 500;
    font-size: 14px;
    @media screen and (max-width: 468px) {
      font-size: 12px;
    }
  }
  .MuiTypography-body1 {
    font-size: 12px !important;
    font-weight: 500;
    color: #333;
    /* font-family: "Century Gothic", sans-serif; */
  }
  @media screen and (max-width: 768px) {
    max-width: 100%;
    width: 100%;
    padding: 0px;
  }
  @media screen and (max-width: 368px) {
    max-width: 100%;
    width: 100%;
    height: 50vh;
    padding: 0px;
  }
`;

export const ModalStyle = styled(Modal)`
  .modal-content {
    box-sizing: border-box;
    width: 100%;
    background-color: #ffffff;
    border-radius: 10px;
    border: 2px white solid;
    display: flex;
    flex-direction: row !important;
    padding: 20px;
    @media screen and (max-width: 768px) {
      justify-content: center;
      align-items: center;
      padding: 20px 10px;
      min-height: 70vh;
    }
  }
  .modal-dialog {
    width: 70% !important;
    max-width: 70% !important;
    @media screen and (max-width: 1068px) {
      width: 80% !important;
      max-width: 80% !important;
    }
    @media screen and (max-width: 968px) {
      width: 90% !important;
      max-width: 90% !important;
      margin: auto;
    }
  }
  z-index: 20000;
`;

export const CarouselWrapper = styled.div`
  box-sizing: border-box;
  max-width: 50%;
  width: 50%;
  padding: 10px;
  background: #e5f1f8;
  border-radius: 10px;
  @media screen and (max-width: 800px) {
    display: none;
  }
`;
export const CarouseStyle = styled(Carousel)`
  .carousel-control-prev-icon {
    background-image: ${(props) => `url(${props.prev})`};
  }

  .carousel-control-next-icon {
    background-image: ${(props) => `url(${props.next})`};
  }
`;

export const Carouselitem = styled(CarouselItem)`
  > div[class="carousel-item-wrapper"] {
    box-sizing: border-box;
    display: flex;
    flex-flow: column;
    justify-content: center;
    align-items: center;
    height: 400px;
    > img {
      width: 50%;
    }
    > span {
      font-size: 14px;
      font-style: normal;
      font-weight: 700;
      line-height: 18px;
      letter-spacing: 0em;
      text-align: left;
    }
    > p {
      text-align: center;
    }
  }
`;

export const Textfield = styled(TextField)`
  width: 320px;
  @media screen and (max-width: 768px) {
    .MuiFormLabel-root {
      font-size: 14px !important;
    }
  }
  @media screen and (max-width: 400px) {
    width: 300px;
  }
  @media screen and (max-width: 350px) {
    width: 250px;
  }
`;

export const Formcontrollabel = styled(FormControlLabel)`
  width: 320px;
  margin: 0;
  margin-right: 0 !important;
  margin-left: -20px !important;

  @media screen and (max-width: 400px) {
    width: 300px;
  }
  @media screen and (max-width: 350px) {
    width: 250px;
  }
`;

export const OtpWrapper = styled.div`
  > div > div {
    display: flex;
    -webkit-box-align: center;
    align-items: center;
    justify-content: center;
    overflow: hidden;
    font-size: 16px;
    font-weight: 500;
    border: 1px solid #0071bc;
    border-radius: 8px;
    height: 48px;
    width: 48px;
    padding: 0px;
    box-sizing: border-box;
    margin-right: 20px;
    margin-bottom: 4px;
  }
  > div > div:last-child {
    margin-right: 0px;
  }
  > div > div > input {
    outline: none;
    border: none;
    padding: 0px;
  }
`;

export const ResendText = styled.span`
  /* font-family: "Century Gothic", sans-serif; */
  font-weight: 400;
  font-size: 14px;
  color: #999;
  margin: 12px 0;
  @media screen and (max-width: 468px) {
    font-size: 12px;
  }
`;

export const FormWrapper = styled.div`
  display: flex;
  align-items: center;
  flex-flow: column;
`;

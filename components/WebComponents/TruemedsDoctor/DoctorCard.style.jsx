import styled from "styled-components";

export const DoctorCardWrapper = styled.div`
  box-sizing: border-box;
  min-width: 250px;
  width: 250px;
  padding: 10px;
  display: flex;
  flex-direction: column;
  justify-content: center;
  align-items: center;
  img {
    width: 100px;
    height: 100px;
    border-radius: 50%;
  }
  span[class="name"] {
    margin-top: 1rem;
    font-size: 13px;
  }
  span[class="education"] {
    width: 50%;
    text-align: center;
    margin-top: 1rem;
    font-size: 13px;
  }
  span[class="mantra"] {
    flex-grow: 1;
    margin-top: 1rem;
    text-align: center;
    font-style: italic;
    font-size: 13px;
  }
  border-right: 1px solid #beddf2;
  @media screen and (max-width: 480px) {
    min-width: 175px;
    width: 175px;
    img {
      width: 50px;
      height: 50px;
    }
    span[class="name"] {
      margin-top: 0.5rem;
      font-size: 12px;
    }
    span[class="education"] {
      margin-top: 0.5rem;
      font-size: 12px;
    }
    span[class="mantra"] {
      margin-top: 0.5rem;
      font-size: 12px;
    }
  }
`;

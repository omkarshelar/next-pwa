import styled from "styled-components";

export const DoctorWrapper = styled.div`
  width: 100%;
  padding-left: 250px;
  display: flex;
  margin: 5rem 0;

  @media screen and (max-width: 968px) {
    padding-left: 0rem;
    flex-flow: column;
    margin: 1rem 0;
  }
`;
export const DoctorList = styled.div`
  display: flex;
  overflow-x: auto;
  scroll-behavior: smooth;
  transition: scroll-behavior 1s ease;
  -ms-overflow-style: none; /* IE and Edge */
  scrollbar-width: none; /* Firefox */
  ::-webkit-scrollbar:horizontal {
    height: 0;
    width: 0;
    display: none;
  }

  ::-webkit-scrollbar-thumb:horizontal {
    display: none;
  }
`;

export const Heading = styled.div`
  min-width: 275px;
  width: 275px;
  padding: 10px;
  > h2 {
    font-size: 30px;
    font-style: normal;
    font-weight: 700;
    line-height: 49px;
    letter-spacing: 0px;
    text-align: left;
    color: #003055;
  }
  > p {
    font-style: normal;
    font-weight: normal;
    font-size: 16px;
    line-height: 130.2%;
    color: #333333;
  }
  > div[class="scroll-btn"] {
    display: flex;
  }

  @media screen and (max-width: 968px) {
    width: 100%;
    > h2,
    p {
      text-align: center;
    }
    > p {
      font-size: 12px;
    }
    display: flex;
    flex-flow: column;

    > div[class="scroll-btn"] {
      display: flex;
      justify-content: center;
    }
  }
  @media screen and (max-width: 568px) {
    > h2 {
      font-size: 21px;
    }
  }
  @media screen and (max-width: 480px) {
    > h2 {
      font-size: 18px;
      line-height: 35px;
    }
  }
`;

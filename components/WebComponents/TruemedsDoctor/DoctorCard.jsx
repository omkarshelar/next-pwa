import React from "react";
import { DoctorCardWrapper } from "./DoctorCard.style";
import optimizeImage from "../../Helper/OptimizeImage";
import window from "global";
import Image from "next/image";

function DoctorCard({ data }) {
  return (
    <>
      <DoctorCardWrapper>
        <Image
          src={optimizeImage(
            data.image,
            "100"
            // window.innerWidth > 467 ? "100" : "50"
          )}
          alt="Doctor"
          width={data.isMobile ? 50 : 100}
          height={data.isMobile ? 50 : 100}
        />
        <span className="name">{data.doctorName}</span>
        <span className="education">
          {" "}
          {data.qualification &&
            data.qualification.map(function (item, index) {
              return (
                <span key={`demo_snap_${index}`}>
                  {(index ? ", " : "") + item}
                </span>
              );
            })}
        </span>
        {data.healthMantra?.length > 0 ? (
          <span className="mantra"> "{data.healthMantra}"</span>
        ) : (
          <span className="mantra"></span>
        )}
      </DoctorCardWrapper>
    </>
  );
}

export default DoctorCard;

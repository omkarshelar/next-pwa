import React, { Component, createRef } from "react";
import Button from "../../WebComponents/Button/Button";
import {
  FeaturedWrapper,
  Header,
  MedicineList,
  Slider,
} from "./RecentOrders.style";
import next from "../Upload/next.svg";
import prev from "../Upload/prev.svg";
import OrderCard from "../../WebComponents/OrderCard/OrderCard";
import BlankCard from "../BlankCard/BlankCard";
import { connect } from "react-redux";
import Router, { withRouter } from "next/router";
import Axios from "axios";
import { PWA_static_URL, ThirdPartySevice_URL } from "../../../constants/Urls";
import { allOrdersThunk } from "../../../redux/MyOrder/Action";
import noDataIcon from "../../../src/Assets/no-data.svg";
import RecentOrderLoaderCard from "./RecentOrderLoaderCard";
import window from "global";
export class RecentOrders extends Component {
  constructor(props) {
    super(props);
    this.ListRef = createRef();
  }
  state = {
    paymentPendingOrder: null,
    filteredList: [],
    type: "",
    orderType: "All",
  };
  scrollRight = () => {
    if (this.ListRef.current) {
      this.ListRef.current.scrollLeft = this.ListRef.current.scrollLeft + 500;
    }
  };
  scrollLeft = () => {
    if (this.ListRef.current) {
      this.ListRef.current.scrollLeft = this.ListRef.current.scrollLeft - 500;
    }
  };

  componentDidMount() {
    this.setPaymentPendingOrder();
  }
  cashFreeLinkImpl = (orderId) => {
    const Uri = `${ThirdPartySevice_URL}/createPaymentUrlInCashFree?orderId=${orderId}&returnUrl=${PWA_static_URL}`;
    const config = {
      headers: {
        "Content-Type": "application/json",
        Authorization:
          "Bearer " + this.props.accessToken?.Response?.access_token,
        "Access-Control-Allow-Origin": "*",
      },
    };
    Axios.get(Uri, config)
      .then((response) => {
        if (response.status === 200) {
          // window.open(response.data.paymentLink);
          window.location.href = response.data.paymentLink;
          this.setState({
            paymentPendingOrder: null,
          });
        }
      })
      .catch((err) => {
        console.error("createPaymentUrlInCashFree", err.response);
      });
  };
  goToBuyMed = (route) => {
    Router.push(`${route}`);
  };
  setPaymentPendingOrder = () => {
    if (this.props.orderTracker?.length > 0) {
      const paymentPending = this.props.orderTracker.find(
        (item) => item.statusId === 58
      );
      this.setState({ paymentPendingOrder: paymentPending });
    }
  };

  FilterOrders = (data) => {
    if (data === 55) {
      this.filterDeliveredOrder();
      this.setState({ type: "deliver" });
    }
    if (data === 57) {
      this.filterSelectedData(57);
      this.setState({ type: "cancel" });
    }
    if (data === "All") {
      this.setState({ filteredList: [] });
      this.setState({ type: "all" });
    }
    if (data === "Active") {
      this.filterActiveOrder();
      this.setState({ type: "active" });
    }
  };

  filterSelectedData = (orderStatusId) => {
    let filtered = [];
    if (this.props?.allOrders?.length > 0) {
      filtered = this.props?.allOrders.filter(
        (data) => data.orderStatusId === orderStatusId
      );
      this.setState({ filteredList: filtered });
    }
    return true;
  };

  filterActiveOrder = () => {
    let filtered = [];
    if (this.props?.allOrders?.length > 0) {
      filtered = this.props?.allOrders.filter(
        (data) =>
          data.orderStatusId !== 55 &&
          data.orderStatusId !== 57 &&
          data.orderStatusId !== 201 &&
          data.orderStatusId !== 199 &&
          data.orderStatusId !== 124
      );
      this.setState({ filteredList: filtered });
    }
    return true;
  };

  filterDeliveredOrder = () => {
    let filtered = [];
    if (this.props.allOrders?.length > 0) {
      filtered = this.props.allOrders.filter(
        (data) =>
          data.orderStatusId === 55 ||
          data.orderStatusId === 201 ||
          data.orderStatusId === 199 ||
          data.orderStatusId === 124
      );
      this.setState({ filteredList: filtered });
    }
    return true;
  };

  render() {
    let { type, filteredList } = this.state;
    let { allOrders } = this.props;
    return (
      allOrders?.length > 0 && (
        <FeaturedWrapper>
          <Header>
            <div className="feature-top-header">
              <div className="heading">
                <h2>Recent Orders</h2>
              </div>
              <div style={{ display: "flex" }} className="recent-order-filter">
                <div>
                  <Button>
                    <img src={prev} onClick={this.scrollLeft} alt="left"></img>
                  </Button>
                  <Button>
                    <img
                      src={next}
                      onClick={this.scrollRight}
                      alt="right"
                    ></img>
                  </Button>
                </div>
              </div>
            </div>
          </Header>
          <Slider>
            <MedicineList ref={this.ListRef}>
              <BlankCard />
              {type === "deliver" || type === "cancel" || type === "active" ? (
                <>
                  {filteredList?.length > 0 ? (
                    filteredList.map((order) => (
                      <OrderCard orderDetails={order} history={Router} />
                    ))
                  ) : (
                    <div className="myOrdersNoDataContainer">
                      <img src={noDataIcon} alt="no-data" />
                      <span>No Data Found</span>
                    </div>
                  )}
                </>
              ) : (
                <>
                  {this.props.isLoading ? (
                    <RecentOrderLoaderCard />
                  ) : (
                    allOrders?.map((order) => (
                      <OrderCard orderDetails={order} history={Router} />
                    ))
                  )}
                </>
              )}
            </MedicineList>
          </Slider>
        </FeaturedWrapper>
      )
    );
  }
}
let mapStateToProps = (state) => ({
  accessToken: state.loginReducer?.verifyOtpSuccess,
  isLoading: state.loader?.isLoading,
  orderHistory: state.myOrder,
  orderTracker: state.trackOrder.TrackOrderData,
});

export default withRouter(
  connect(mapStateToProps, {
    allOrdersThunk,
  })(RecentOrders)
);

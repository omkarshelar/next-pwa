import styled from "styled-components";

export const FeaturedWrapper = styled.div`
  width: 100%;
  display: flex;
  background: linear-gradient(180deg, #c9e8fe 0%, rgba(110, 193, 253, 0) 100%);

  flex-flow: column;
`;
export const MedicineList = styled.div`
  display: flex;
  overflow-y: hidden;
  overflow-x: scroll;
  scroll-behavior: smooth;
  transition: scroll-behavior 1s ease;
  -ms-overflow-style: none; /* IE and Edge */
  scrollbar-width: none; /* Firefox */

  ::-webkit-scrollbar:horizontal {
    height: 0;
    width: 0;
    display: none;
  }

  ::-webkit-scrollbar-thumb:horizontal {
    display: none;
  }
  > div {
    min-width: 400px;
    margin: 2rem 20px;
  }
  > div div:nth-child(3) span {
    max-width: 18vw;
  }
  @media screen and (max-width: 768px) {
    > div div:nth-child(3) span {
      max-width: 30vw;
    }
  }
  @media screen and (max-width: 480px) {
    > div {
      min-width: 250px;
      width: 250px;
      margin: 2rem 8px;
      height: 230px;
    }
    > div div:nth-child(3) span {
      min-width: 200px;
      width: 200px;
    }
    > div > div > div:nth-child(1) > div:nth-child(2) {
      gap: 0.5rem;
    }
    > div > div > div:nth-child(2) img {
      width: 25px;
    }
    > div > div > div:nth-child(2) span {
      font-size: 12px;
    }
    > div > div > div:nth-child(3) > div > img {
      display: none;
    }
  }
`;
export const Slider = styled.div`
  display: flex;
  /* justify-content: flex-end; */
  > button > img {
    width: 30px;
  }
`;

export const Header = styled.div`
  display: flex;
  width: 100%;
  margin-top: 1rem;
  justify-content: flex-end;
  > div[class="feature-top-header"] {
    width: 85%;
    display: flex;
    align-items: center;
    justify-content: space-between;
    > div[class="recent-order-filter"] {
      display: flex;
      align-items: center;
      > div[class="myOrderTypesContainer"] {
        display: flex;
        flex-direction: row;
        align-items: center;
        justify-content: center;
        margin-bottom: 0 !important;
        .myOrderSelectedType,
        .myOrderUnselectedType {
          display: flex;
          flex-direction: row;
          justify-content: center;
          cursor: pointer;
          padding-bottom: 5px;
          width: 115px;
          transition: 0.2s;
        }
        .myOrderSelectedType {
          border-bottom: 4px solid #0071bc;
        }

        .myOrderUnselectedType {
          border-bottom: 1px solid #0071bc;
        }
      }
    }

    > div[class="heading"] {
      > h2 {
        /* font-family: "Century Gothic", sans-serif; */
        font-size: 25px;
        font-style: normal;
        font-weight: 600;
        line-height: 40px;
        letter-spacing: 0px;
        text-align: left;
        color: #003055;
        margin-left: 2rem;
        margin-top: 1rem;
      }
    }
  }

  @media screen and (max-width: 1068px) {
    > div[class="feature-top-header"] {
      width: 98%;
      > div[class="recent-order-filter"] {
        > div[class="myOrderTypesContainer"] {
          min-height: 40px;
          .myOrderSelectedType,
          .myOrderUnselectedType {
            width: 100px;
          }
        }
      }
    }
  }

  @media screen and (max-width: 868px) {
    > div[class="feature-top-header"] {
      flex-flow: column;
      width: 100%;
      > div[class="heading"] {
        > h2 {
          width: 100%;
          text-align: center;
          margin: 0rem;
          font-size: 21px;
        }
      }
      > div[class="recent-order-filter"] {
        flex-flow: column;
      }
    }
  }
  @media screen and (max-width: 468px) {
    > div[class="feature-top-header"] {
      > div[class="heading"] {
        > h2 {
          font-size: 18px;
          line-height: 35px;
        }
      }
      > div[class="recent-order-filter"] {
        > div[class="myOrderTypesContainer"] {
          .myOrderSelectedType,
          .myOrderUnselectedType {
            width: 75px;
          }
        }
      }
    }
  }
`;

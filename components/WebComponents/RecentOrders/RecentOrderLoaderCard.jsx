import React, { Component } from "react";
import Shimmer from "react-shimmer-effect";
import "./RecentOrderLoaderCard.css";
export class RecentOrderLoaderCard extends Component {
  render() {
    return (
      <div className="blank-med-loader-recent-order-card-container">
        <div className="blank-med-loader-recent-order-card-wrapper">
          <Shimmer>
            <div className="blank-med-loader-recent-order-rectangle"></div>
          </Shimmer>
          <Shimmer>
            <div className="blank-med-loader-recent-order-order-status"></div>
          </Shimmer>
          <div className="blank-med-loader-recent-order-med-item-wrapper">
            <Shimmer>
              <div className="blank-med-loader-recent-order-med-item"></div>
              <div className="blank-med-loader-recent-order-med-item"></div>
            </Shimmer>
          </div>
          <div className="blank-med-loader-recent-order-med-item-wrapper">
            <Shimmer>
              <div className="blank-med-loader-recent-order-med-button"></div>
            </Shimmer>
          </div>
        </div>
        <div className="blank-med-loader-recent-order-card-wrapper">
          <Shimmer>
            <div className="blank-med-loader-recent-order-rectangle"></div>
          </Shimmer>
          <Shimmer>
            <div className="blank-med-loader-recent-order-order-status"></div>
          </Shimmer>
          <div className="blank-med-loader-recent-order-med-item-wrapper">
            <Shimmer>
              <div className="blank-med-loader-recent-order-med-item"></div>
              <div className="blank-med-loader-recent-order-med-item"></div>
            </Shimmer>
          </div>
          <div className="blank-med-loader-recent-order-med-item-wrapper">
            <Shimmer>
              <div className="blank-med-loader-recent-order-med-button"></div>
            </Shimmer>
          </div>
        </div>
        <div className="blank-med-loader-recent-order-card-wrapper">
          <Shimmer>
            <div className="blank-med-loader-recent-order-rectangle"></div>
          </Shimmer>
          <Shimmer>
            <div className="blank-med-loader-recent-order-order-status"></div>
          </Shimmer>
          <div className="blank-med-loader-recent-order-med-item-wrapper">
            <Shimmer>
              <div className="blank-med-loader-recent-order-med-item"></div>
              <div className="blank-med-loader-recent-order-med-item"></div>
            </Shimmer>
          </div>
          <div className="blank-med-loader-recent-order-med-item-wrapper">
            <Shimmer>
              <div className="blank-med-loader-recent-order-med-button"></div>
            </Shimmer>
          </div>
        </div>
      </div>
    );
  }
}

export default RecentOrderLoaderCard;

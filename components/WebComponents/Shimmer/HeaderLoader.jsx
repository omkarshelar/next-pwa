import React, { Component } from "react";
import Shimmer from "react-shimmer-effect";
import "./HeaderLoader.css";

export default class HeaderLoader extends Component {
  render() {
    return (
      <Shimmer>
        <div className="headerShimmer" />
      </Shimmer>
    );
  }
}

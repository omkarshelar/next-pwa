import React, { Component } from "react";
import Shimmer from "react-shimmer-effect";
import "./ModificationLogLoader.css";

export default class ModificationLogLoader extends Component {
  render() {
    return (
      <div className="modificationLogShimmerContainer">
        <Shimmer>
          <div className="modificationLogTextShimmer" />
        </Shimmer>
        <Shimmer>
          <div className="modificationLogMedShimmer" />
        </Shimmer>
        <Shimmer>
          <div className="modificationLogTextShimmer" />
        </Shimmer>
        <Shimmer>
          <div className="modificationLogMedShimmer" />
        </Shimmer>
      </div>
    );
  }
}

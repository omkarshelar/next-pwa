import React, { Component } from "react";
import Shimmer from "react-shimmer-effect";
import "./SaveMoreLoader.css";

export default class SaveMoreLoader extends Component {
  render() {
    return (
      <div className="orderFlowSaveMoreLoaderMainContainer">
        <Shimmer>
          <div className="orderFlowSaveMoreCircleLoader" />
        </Shimmer>
        <div className="orderFlowSaveMoreLineMainLoader">
          <Shimmer>
            <div className="orderFlowSaveMoreLineLoader" />
            <div className="orderFlowSaveMoreLineLoader" />
            <div className="orderFlowSaveMoreLineLoader" />
          </Shimmer>
        </div>
      </div>
    );
  }
}

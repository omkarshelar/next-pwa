import React, { Component } from "react";
import Shimmer from "react-shimmer-effect";
import "./PaymentLoader.css";

export default class PaymentLoader extends Component {
  render() {
    return (
      <div className="orderFlowPaymentLoaderMainContainer">
        <Shimmer>
          <div className="orderFlowPaymentLineLoader" />
          <div className="orderFlowPaymentDivLoader" />
          <div className="orderFlowPaymentDivLoader" />
        </Shimmer>
      </div>
    );
  }
}

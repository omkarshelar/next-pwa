import React, { Component } from "react";
import Shimmer from "react-shimmer-effect";
import "./DoctorRatingsLoader.css";

export default class DoctorRatingsLoader extends Component {
  render() {
    return (
      <div className="doctorRatingsMainContainer">
        <Shimmer>
          <div className="doctorRatingsLineLoader" />
          <div className="doctorRatingsLineLoader" />
          <div className="doctorRatingsLineLoader" />
        </Shimmer>
      </div>
    );
  }
}

import React, { Component } from "react";
import Shimmer from "react-shimmer-effect";
import "./SavingsLoader.css";

export default class SavingsLoader extends Component {
  render() {
    return (
      <div className="orderFlowSavingsLoaderMainContainer">
        <Shimmer>
          <div className="orderFlowSavingsLineLoaderContainer" />
          <div className="orderFlowSavingsLineLoaderContainer" />
          <div className="orderFlowSavingsLineLoaderContainer" />
          <div className="orderFlowSavingsLineLoaderContainer" />
        </Shimmer>
      </div>
    );
  }
}

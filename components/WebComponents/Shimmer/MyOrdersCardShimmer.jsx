import React, { Component } from "react";
import Shimmer from "react-shimmer-effect";
import "./MyOrdersCardShimmer.css";

export default class MyOrdersCardShimmer extends Component {
  render() {
    return (
      <div className="MyOrdersCardContainerShimmer">
        <div className="MyOrdersCardFilterContainerShimmer">
          <Shimmer>
            <div className="MyOrdersCardFilterTitleShimmer" />
          </Shimmer>
          <div className="MyOrdersCardFilterShimmer">
            <Shimmer>
              <div className="MyOrdersCardFilterTypeShimmer" />
              <div className="MyOrdersCardFilterTypeShimmer" />
            </Shimmer>
          </div>
        </div>
        <Shimmer>
          <div className="MyOrdersCardFilterCardShimmer" />
          <div className="MyOrdersCardFilterCardShimmer" />
          <div className="MyOrdersCardFilterCardShimmer" />
        </Shimmer>
      </div>
    );
  }
}

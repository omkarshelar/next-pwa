import React, { Component } from "react";
import Shimmer from "react-shimmer-effect";
import "./OrderStatusLoader.css";

export default class OrderStatusLoader extends Component {
  render() {
    return (
      <div className="orderStatusShimmerContainer">
        <div className="orderStatusShimmerDetails">
          <Shimmer>
            <div className="orderStatusShimmerInnerDetails" />
          </Shimmer>
          <Shimmer>
            <div className="orderStatusShimmerInnerDetails" />
          </Shimmer>
        </div>
        <div className="orderStatusShimmerDetails">
          <Shimmer>
            <div className="orderStatusShimmerInnerDetails" />
          </Shimmer>
          <Shimmer>
            <div className="orderStatusShimmerInnerDetails" />
          </Shimmer>
        </div>
        <div className="orderStatusShimmerTimeline">
          <div className="orderStatusShimmerDetails">
            <Shimmer>
              <div className="orderStatusShimmerInnerDetails" />
            </Shimmer>
            <Shimmer>
              <div className="orderStatusShimmerInnerDetails" />
            </Shimmer>
          </div>
          <div className="orderStatusShimmerInnerTimeline">
            <Shimmer>
              <div />
            </Shimmer>
            <Shimmer>
              <div />
            </Shimmer>
          </div>
        </div>
      </div>
    );
  }
}

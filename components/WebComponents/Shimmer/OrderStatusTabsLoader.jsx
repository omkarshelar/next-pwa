import React from "react";
import Shimmer from "react-shimmer-effect";
import "./OrderStatusTabsLoader.css";

const OrderStatusTabsLoader = (props) => {
  return (
    <div>
      <div className="OrderStatusTabsLoaderHeader">
        <Shimmer>
          <div></div>
        </Shimmer>
        <Shimmer>
          <div></div>
        </Shimmer>
      </div>
      <div className="ostl_patientDetailsWrap">
        <Shimmer>
          <div></div>
        </Shimmer>
        <Shimmer>
          <div></div>
        </Shimmer>
      </div>
      <div className="ostl_patientDetailsWrap">
        <Shimmer>
          <div></div>
        </Shimmer>
        <Shimmer>
          <div></div>
        </Shimmer>
      </div>
      <div className="ostl_TimelineWrap">
        <div className="ostl_Timeline">
          <Shimmer>
            <div></div>
          </Shimmer>
          <Shimmer>
            <div></div>
          </Shimmer>
        </div>
        <div className="ostl_Timeline">
          <Shimmer>
            <div></div>
          </Shimmer>
          <Shimmer>
            <div></div>
          </Shimmer>
        </div>
        <div className="ostl_Timeline">
          <Shimmer>
            <div></div>
          </Shimmer>
          <Shimmer>
            <div></div>
          </Shimmer>
        </div>
        <div className="ostl_Timeline">
          <Shimmer>
            <div></div>
          </Shimmer>
          <Shimmer>
            <div></div>
          </Shimmer>
        </div>
        <div className="ostl_TimelineActive">
          <Shimmer>
            <div></div>
          </Shimmer>
        </div>
      </div>

      <div className="ostl_AlternateWrapper">
        <Shimmer>
          <div></div>
        </Shimmer>
      </div>
    </div>
  );
};

export default OrderStatusTabsLoader;

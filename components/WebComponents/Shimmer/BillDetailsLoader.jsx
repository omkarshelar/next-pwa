import React, { Component } from "react";
import "./BillDetailsLoader.css";
import Skeleton from "@mui/material/Skeleton";

export default class BillDetailsLoader extends Component {
  render() {
    return (
      <div className="orderFlowBillLoaderMainContainer">
        <div className="orderFlowBillLineLoader">
          <Skeleton animation="wave" height={30} />
        </div>
        <div className="orderFlowBillLineLoader">
          <Skeleton animation="wave" height={30} />
        </div>
        <div className="orderFlowBillLineLoader">
          <Skeleton animation="wave" height={30} />
        </div>
        <div className="orderFlowBillLineLoader">
          <Skeleton animation="wave" height={30} />
        </div>
        <div className="orderFlowBillLineLoader">
          <Skeleton animation="wave" height={80} />
        </div>
      </div>
    );
  }
}

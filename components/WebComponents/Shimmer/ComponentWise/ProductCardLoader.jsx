import React from "react";
import "./ProductCardLoader.module.css";
import Shimmer from "react-shimmer-effect";

const ProductCardLoader = (props) => {
  return (
    <div className={"productCardLoader_productList"}>
      <div className={"productCardLoader_productItem"}>
        <Shimmer>
          <div className={"productCardLoader_productImageWrap"}></div>
        </Shimmer>

        <div className={"productCardLoader_productDetailsWrapper"}>
          <div>
            <div>
              <Shimmer>
                <div className={"productCardLoader_productName"}></div>
              </Shimmer>
            </div>

            <Shimmer>
              <div className={"productCardLoader_productCompanyName"}></div>
            </Shimmer>
          </div>
          <div className={"productCardLoader_priceContainer"}>
            <Shimmer>
              <div className={"productCardLoader_sellingPrice"}></div>
            </Shimmer>

            <div className={"productCardLoader_pricingWrap"}>
              <Shimmer>
                <div className={"productCardLoader_mrp"}></div>
              </Shimmer>
              <Shimmer>
                <div className={"productCardLoader_discount"}></div>
              </Shimmer>
            </div>
          </div>
        </div>
        <Shimmer>
          <div className={"productCardLoader_qty"}></div>
        </Shimmer>
      </div>
      <div className={"productCardLoader_productItem"}>
        <Shimmer>
          <div className={"productCardLoader_productImageWrap"}></div>
        </Shimmer>

        <div className={"productCardLoader_productDetailsWrapper"}>
          <div>
            <div>
              <Shimmer>
                <div className={"productCardLoader_productName"}></div>
              </Shimmer>
            </div>

            <Shimmer>
              <div className={"productCardLoader_productCompanyName"}></div>
            </Shimmer>
          </div>
          <div className={"productCardLoader_priceContainer"}>
            <Shimmer>
              <div className={"productCardLoader_sellingPrice"}></div>
            </Shimmer>

            <div className={"productCardLoader_pricingWrap"}>
              <Shimmer>
                <div className={"productCardLoader_mrp"}></div>
              </Shimmer>
              <Shimmer>
                <div className={"productCardLoader_discount"}></div>
              </Shimmer>
            </div>
          </div>
        </div>
        <Shimmer>
          <div className={"productCardLoader_qty"}></div>
        </Shimmer>
      </div>
    </div>
  );
};

export default ProductCardLoader;

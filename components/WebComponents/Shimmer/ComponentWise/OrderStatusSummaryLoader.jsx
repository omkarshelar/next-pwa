import React from "react";
import Shimmer from "react-shimmer-effect";
import AndroidSeperator from "../../AndroidSeperator/AndroidSeperator";

import "./OrderStatusSummaryLoader.module.css";

const OrderStatusSummaryLoader = (props) => {
  return (
    <div className={"orderSummaryLoader_OrderStatusSummaryWrapper"}>
      <div>
        <Shimmer>
          <div className={"orderSummaryLoader_OrderStatusSummaryHeader"}></div>
        </Shimmer>
      </div>
      <div className={"orderSummaryLoader_detailsSection"}>
        <div className={"orderSummaryLoader_detailsRow"}>
          <div>
            <Shimmer>
              <div className={"orderSummaryLoader_title"}></div>
            </Shimmer>
          </div>
          <div>
            <Shimmer>
              <div className={"orderSummaryLoader_date"}></div>
            </Shimmer>
          </div>
        </div>
        <div className={"orderSummaryLoader_detailsRow"}>
          <div>
            <Shimmer>
              <div className={"orderSummaryLoader_title"}></div>
            </Shimmer>
          </div>
          <div>
            <Shimmer>
              <div className={"orderSummaryLoader_date"}></div>
            </Shimmer>
          </div>
        </div>
        <div className={"orderSummaryLoader_detailsRow"}>
          <div>
            <Shimmer>
              <div className={"orderSummaryLoader_title"}></div>
            </Shimmer>
          </div>
          <div>
            <Shimmer>
              <div className={"orderSummaryLoader_date"}></div>
            </Shimmer>
          </div>
        </div>
        <div className={"orderSummaryLoader_detailsRow"}>
          <div>
            <Shimmer>
              <div className={"orderSummaryLoader_title"}></div>
            </Shimmer>
          </div>
          <div>
            <Shimmer>
              <div className={"orderSummaryLoader_date"}></div>
            </Shimmer>
          </div>
        </div>
        <div className={"orderSummaryLoader_detailsRow"}>
          <div>
            <Shimmer>
              <div className={"orderSummaryLoader_title"}></div>
            </Shimmer>
          </div>
          <div>
            <Shimmer>
              <div className={"orderSummaryLoader_date"}></div>
            </Shimmer>
          </div>
        </div>
      </div>
      <div className={"orderSummaryLoader_AndroidSeperatorWrap"}>
        <AndroidSeperator showWeb={true} />
      </div>
    </div>
  );
};

export default OrderStatusSummaryLoader;

import styled from "styled-components";

export const PageLoaderWrapper = styled.div`
  width: 70%;
  min-width: 700px;
  margin: 10px auto;
`;
export const MediumRectangle = styled.div`
  width: 100%;
  height: 100px;
  border-radius: 4px;
  margin-bottom: 15px;
`;

export const SmallRectangle = styled.div`
  width: 100%;
  height: 30px;
  border-radius: 4px;
  margin-bottom: 15px;
`;

export const BigRectangle = styled.div`
  width: 100%;
  height: 200px;
  border-radius: 4px;
  margin-bottom: 15px;
`;

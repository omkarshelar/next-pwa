import React, { Component } from "react";
import "./BillDetails.css";
import BillDetailsLoader from "../Shimmer/BillDetailsLoader";
import SaveCard from "../SaveCard/SaveCard";

export class BillDetails extends Component {
  state = {
    promoCodeValue: 0,
  };

  componentDidMount = () => {
    if (this.props.promoCode && this.props.promoCodeValue) {
      this.setState({
        promoCodeValue:
          this.props.promoCode && this.props.promoCodeValue
            ? Number(this.props.promoCodeValue)
            : 0,
      });
    }
  };

  componentDidUpdate(prevProps) {
    if (this.props.promoCodeValue !== prevProps.promoCodeValue) {
      this.setState({
        promoCodeValue:
          this.props.promoCode && this.props.promoCodeValue
            ? Number(this.props.promoCodeValue)
            : 0,
      });
    }
  }

  render() {
    if (this.props.isLoading) {
      // Bill Shimmer Loader
      return <BillDetailsLoader />;
    } else if (this.props.isSummary || this.props.details) {
      // Summary Bill
      return (
        <>
          {this.props.updateDetail?.orderStatus?.serialId === 49 ||
          this.props.updateDetail?.orderStatus?.serialId === 39 ||
          this.props.updateDetail?.orderStatus?.serialId === 2 ? (
            this.props.details ? (
              <>
                <div className="reusableBillMainContainer">
                  <p
                    style={{
                      borderTop: this.props.isOrderStatus ? "none" : "",
                    }}
                  >
                    Bill Details
                  </p>
                  {this.props.updateDetail.finalCalcAmt?.orderValue ? (
                    <>
                      <div>
                        <span>MRP</span>
                        <span>
                          ₹
                          {this.props.updateDetail?.finalCalcAmt?.orderValue
                            ? this.props.updateDetail.finalCalcAmt.orderValue.toFixed(
                                2
                              )
                            : 0}
                        </span>
                      </div>
                      {(this.props.updateDetail?.productSubsMappingList
                        ?.length > 0
                        ? this.props.calcDiscPreProcessing(
                            this.props.updateDetail.productSubsMappingList
                          )
                        : 0) > 0 ? (
                        <div>
                          <span>Discount</span>
                          <span>
                            -₹
                            {this.props.updateDetail?.productSubsMappingList
                              ?.length > 0
                              ? this.props.calcDiscPreProcessing(
                                  this.props.updateDetail.productSubsMappingList
                                )
                              : 0}
                          </span>
                        </div>
                      ) : null}
                      {this.props.updateDetail?.Promocode &&
                      this.state.promoCodeValue ? (
                        <div>
                          <span>Coupon code</span>
                          <span>-₹{this.state.promoCodeValue.toFixed(2)}</span>
                        </div>
                      ) : null}
                      <div>
                        <span>Delivery charge</span>
                        {this.props.updateDetail?.finalCalcAmt
                          ?.deliveryCharge ? (
                          <span>
                            ₹
                            {
                              this.props.updateDetail?.finalCalcAmt
                                ?.deliveryCharge
                            }
                          </span>
                        ) : (
                          <span style={{ color: "#41a45c", fontWeight: "500" }}>
                            FREE
                          </span>
                        )}
                      </div>
                      <div className="reusableBillMainTotalContainer">
                        <div className="reusableBillEstimatedTotalContainer">
                          <span>Total</span>
                          <span>
                            ₹
                            {this.props.updateDetail?.productSubsMappingList
                              ?.length > 0 &&
                            this.props.updateDetail.finalCalcAmt?.orderValue
                              ? this.props.calcTotalPreProcessing(
                                  this.props.updateDetail
                                    .productSubsMappingList,
                                  this.props.updateDetail.finalCalcAmt
                                    .orderValue,
                                  this.props.updateDetail.finalCalcAmt
                                    .deliveryCharge,
                                  this.state.promoCodeValue
                                )
                              : 0}
                          </span>
                        </div>
                        <p>Inclusive of all taxes*</p>
                      </div>
                    </>
                  ) : (
                    <div
                      style={{
                        backgroundColor: "#fff",
                        border: "none",
                        margin: "0",
                        paddingTop: "0",
                      }}
                    >
                      <span className="postProcessingText">
                        Total amount will be calculated post processing
                      </span>
                    </div>
                  )}
                </div>
                {(!this.props.details ||
                  (this.props.details &&
                    this.props.updateDetail?.orderStatus?.serialId &&
                    ![57].includes(
                      this.props.updateDetail?.orderStatus?.serialId
                    ))) &&
                this.props.updateDetail?.productSubsMappingList?.length > 0 &&
                Number(
                  this.props.calcDiscPreProcessing(
                    this.props.updateDetail.productSubsMappingList
                  )
                ) +
                  (this.props.updateDetail?.Promocode &&
                  this.state.promoCodeValue
                    ? this.state.promoCodeValue.toFixed(2)
                    : 0) >
                  50 &&
                !this.props.isOrderStatus ? (
                  <SaveCard
                    savings={
                      this.props.updateDetail?.productSubsMappingList?.length >
                      0
                        ? (
                            Number(
                              this.props.calcDiscPreProcessing(
                                this.props.updateDetail.productSubsMappingList
                              )
                            ) +
                            (this.props.updateDetail?.Promocode &&
                            this.state.promoCodeValue
                              ? this.state.promoCodeValue.toFixed(2)
                              : 0)
                          ).toFixed(2)
                        : 0
                    }
                  />
                ) : null}
              </>
            ) : (
              // Summary Bill before order confirm
              <>
                <div className="reusableBillMainContainer">
                  <p
                    style={{
                      borderTop: this.props.isOrderStatus ? "none" : "",
                    }}
                  >
                    Bill Details
                  </p>
                  {this.props.getTotalSpForSummary(
                    this.props.checkMedsForSummary(
                      this.props.updateDetail.productSubsMappingList
                    )
                  ) ? (
                    <>
                      <div>
                        <span>MRP</span>
                        <span>
                          ₹
                          {this.props.getTotalMrpForSummary(
                            this.props.checkMedsForSummary(
                              this.props.updateDetail.productSubsMappingList
                            )
                          )}
                        </span>
                      </div>
                      {(this.props.updateDetail?.productSubsMappingList
                        ?.length > 0
                        ? this.props.getTotalDiscountForSummary(
                            this.props.checkMedsForSummary(
                              this.props.updateDetail.productSubsMappingList
                            )
                          )
                        : 0) > 0 ? (
                        <div>
                          <span>Discount</span>
                          <span>
                            -₹
                            {this.props.updateDetail?.productSubsMappingList
                              ?.length > 0
                              ? this.props.getTotalDiscountForSummary(
                                  this.props.checkMedsForSummary(
                                    this.props.updateDetail
                                      .productSubsMappingList
                                  )
                                )
                              : 0}
                          </span>
                        </div>
                      ) : null}
                      {this.state.promoCodeValue ? (
                        <div>
                          <span>Coupon code</span>
                          <span>-₹{this.state.promoCodeValue.toFixed(2)}</span>
                        </div>
                      ) : null}
                      <div>
                        <span>Delivery charge</span>
                        {Number(
                          this.props.getTotalSpForSummary(
                            this.props.checkMedsForSummary(
                              this.props.updateDetail.productSubsMappingList
                            )
                          )
                        ) >=
                        (this.props.checkForSubs(
                          this.props.updateDetail?.productSubsMappingList
                        )
                          ? this.props.deliveryOnAmountSubs
                          : this.props.deliveryOnAmount) ? (
                          <span style={{ color: "#41a45c", fontWeight: "500" }}>
                            FREE
                          </span>
                        ) : (
                          <span>
                            ₹
                            {this.props.checkForSubs(
                              this.props.updateDetail?.productSubsMappingList
                            )
                              ? this.props.deliveryChargeSubs
                              : this.props.deliveryCharge}
                          </span>
                        )}
                      </div>
                      <div className="reusableBillMainTotalContainer">
                        <div className="reusableBillEstimatedTotalContainer">
                          <span>Total</span>
                          <span>
                            ₹
                            {Number(
                              this.props.getTotalSpForSummary(
                                this.props.checkMedsForSummary(
                                  this.props.updateDetail.productSubsMappingList
                                )
                              )
                            ) >=
                            (this.props.checkForSubs(
                              this.props.updateDetail?.productSubsMappingList
                            )
                              ? this.props.deliveryOnAmountSubs
                              : this.props.deliveryOnAmount)
                              ? (
                                  this.props.getTotalSpForSummary(
                                    this.props.checkMedsForSummary(
                                      this.props.updateDetail
                                        .productSubsMappingList
                                    )
                                  ) - this.state.promoCodeValue
                                ).toFixed(2)
                              : (
                                  this.props.getTotalSpForSummary(
                                    this.props.checkMedsForSummary(
                                      this.props.updateDetail
                                        .productSubsMappingList
                                    )
                                  ) +
                                  (this.props.checkForSubs(
                                    this.props.updateDetail
                                      ?.productSubsMappingList
                                  )
                                    ? this.props.deliveryChargeSubs
                                    : this.props.deliveryCharge) -
                                  this.state.promoCodeValue
                                ).toFixed(2)}
                          </span>
                        </div>
                        <p>Inclusive of all taxes*</p>
                      </div>
                    </>
                  ) : (
                    <div
                      style={{
                        backgroundColor: "#fff",
                        border: "none",
                        margin: "0",
                        paddingTop: "0",
                      }}
                    >
                      <span className="postProcessingText">
                        Total amount will be calculated post processing
                      </span>
                    </div>
                  )}
                </div>
                {(!this.props.details ||
                  (this.props.details &&
                    this.props.updateDetail?.orderStatus?.serialId &&
                    ![57].includes(
                      this.props.updateDetail?.orderStatus?.serialId
                    ))) &&
                this.props.updateDetail?.productSubsMappingList?.length > 0 &&
                Number(
                  this.props.getTotalDiscountForSummary(
                    this.props.checkMedsForSummary(
                      this.props.updateDetail.productSubsMappingList
                    )
                  )
                ) +
                  this.state.promoCodeValue >
                  50 &&
                !this.props.isOrderStatus ? (
                  <SaveCard
                    savings={
                      this.props.updateDetail?.productSubsMappingList?.length >
                      0
                        ? (
                            Number(
                              this.props.getTotalDiscountForSummary(
                                this.props.checkMedsForSummary(
                                  this.props.updateDetail.productSubsMappingList
                                )
                              )
                            ) + this.state.promoCodeValue
                          ).toFixed(2)
                        : 0
                    }
                  />
                ) : null}
              </>
            )
          ) : (
            <>
              <div className="reusableBillMainContainer">
                <p
                  style={{ borderTop: this.props.isOrderStatus ? "none" : "" }}
                >
                  Bill Details
                </p>
                {this.props.updateDetail?.finalCalcAmt?.totalAmount ? (
                  <>
                    <div>
                      <span>MRP</span>
                      <span>
                        ₹
                        {this.props.updateDetail?.finalCalcAmt?.subsMrp
                          ? this.props.updateDetail.finalCalcAmt.subsMrp.toFixed(
                              2
                            )
                          : 0}
                      </span>
                    </div>
                    {(this.props.updateDetail?.finalCalcAmt?.discount
                      ? this.props.updateDetail.finalCalcAmt.discount.toFixed(2)
                      : 0) > 0 ? (
                      <div>
                        <span>Discount</span>
                        <span>
                          -₹
                          {this.props.updateDetail?.finalCalcAmt?.discount
                            ? this.props.updateDetail.finalCalcAmt.discount.toFixed(
                                2
                              )
                            : 0}
                        </span>
                      </div>
                    ) : null}
                    {this.props.updateDetail?.Promocode &&
                    this.props.updateDetail?.offerDiscountApplied ? (
                      <div>
                        <span>Coupon code</span>
                        <span>
                          -₹{this.props.updateDetail?.offerDiscountApplied}
                        </span>
                      </div>
                    ) : null}
                    {this.props.details &&
                    this.props.updateDetail?.finalCalcAmt &&
                    this.props.updateDetail.finalCalcAmt.tmCash ? (
                      <div>
                        <span>TM Rewards</span>
                        <span>
                          -₹
                          {this.props.updateDetail.finalCalcAmt.tmCash.toFixed(
                            2
                          )}
                        </span>
                      </div>
                    ) : null}
                    <div>
                      <span>Delivery charge</span>
                      {this.props.updateDetail?.finalCalcAmt?.deliveryCharge ? (
                        <span>
                          ₹{this.props.updateDetail.finalCalcAmt.deliveryCharge}
                        </span>
                      ) : (
                        <span style={{ color: "#41a45c", fontWeight: "500" }}>
                          FREE
                        </span>
                      )}
                    </div>
                    <div className="reusableBillMainTotalContainer">
                      <div
                        style={{ border: "0" }}
                        className="reusableBillEstimatedTotalContainer"
                      >
                        <span>Total</span>
                        <span>
                          ₹
                          {this.props.updateDetail?.finalCalcAmt?.totalAmount
                            ? this.props.updateDetail.finalCalcAmt.totalAmount.toFixed(
                                2
                              )
                            : this.props.updateDetail?.finalCalcAmt?.finalAmount
                            ? this.props.updateDetail.finalCalcAmt.finalAmount.toFixed(
                                2
                              )
                            : 0}
                        </span>
                      </div>
                      {this.props.details &&
                      this.props.updateDetail?.finalCalcAmt?.tmCredit &&
                      this.props.updateDetail?.finalCalcAmt?.finalAmount >
                        0.1 ? null : (
                        <p>Inclusive of all taxes*</p>
                      )}
                    </div>
                    {this.props.details &&
                    this.props.updateDetail?.finalCalcAmt?.tmCredit ? (
                      <div>
                        <span>TM Credit</span>
                        <span>
                          -₹
                          {this.props.updateDetail.finalCalcAmt.tmCredit.toFixed(
                            2
                          )}
                        </span>
                      </div>
                    ) : null}
                    {this.props.details &&
                    this.props.updateDetail?.finalCalcAmt?.tmCredit ? (
                      <div className="reusableBillMainTotalContainer">
                        <div
                          style={{ border: "none" }}
                          className="reusableBillEstimatedTotalContainer"
                        >
                          <span>Total Payable</span>
                          <span>
                            ₹
                            {this.props.updateDetail?.finalCalcAmt
                              ?.finalAmount > 0.1
                              ? this.props.updateDetail.finalCalcAmt.finalAmount.toFixed(
                                  2
                                )
                              : 0}
                          </span>
                        </div>
                        {this.props.updateDetail?.finalCalcAmt?.finalAmount >
                        0.1 ? (
                          <p>Inclusive of all taxes*</p>
                        ) : null}
                      </div>
                    ) : null}
                  </>
                ) : (
                  <div
                    style={{
                      backgroundColor: "#fff",
                      border: "none",
                      margin: "0",
                      paddingTop: "0",
                    }}
                  >
                    <span className="postProcessingText">
                      Total amount will be calculated post processing
                    </span>
                  </div>
                )}
              </div>
              {(!this.props.details ||
                (this.props.details &&
                  this.props.updateDetail?.orderStatus?.serialId &&
                  ![57].includes(
                    this.props.updateDetail?.orderStatus?.serialId
                  ))) &&
              this.props.updateDetail?.finalCalcAmt?.discount &&
              Number(this.props.updateDetail.finalCalcAmt.discount) +
                (this.props.updateDetail?.Promocode &&
                this.props.updateDetail?.offerDiscountApplied
                  ? Number(this.props.updateDetail?.offerDiscountApplied)
                  : 0) >
                50 &&
              !this.props.isOrderStatus ? (
                <SaveCard
                  savings={
                    this.props.updateDetail?.finalCalcAmt?.discount
                      ? (
                          Number(
                            this.props.updateDetail.finalCalcAmt.discount
                          ) +
                          (this.props.updateDetail?.Promocode &&
                          this.props.updateDetail?.offerDiscountApplied
                            ? Number(
                                this.props.updateDetail?.offerDiscountApplied
                              )
                            : 0)
                        ).toFixed(2)
                      : 0
                  }
                />
              ) : null}
            </>
          )}
        </>
      );
    } else {
      // Cart Bill
      return (
        <>
          <div className="reusableBillMainContainer">
            <p>Bill Details</p>
            <div>
              <span>MRP</span>
              <span>₹{this.props.totalDiscountMed.toFixed(2)}</span>
            </div>
            {(this.props.cartContent?.length > 0
              ? this.props.calcDiscPreProcessingForCart(this.props.cartContent)
              : 0) > 0 ? (
              <div>
                <span>Discount</span>
                <span>
                  -₹
                  {this.props.cartContent?.length > 0
                    ? this.props.calcDiscPreProcessingForCart(
                        this.props.cartContent
                      )
                    : 0}
                </span>
              </div>
            ) : null}
            {this.state.promoCodeValue ? (
              <div>
                <span>Coupon code</span>
                <span>-₹{this.state.promoCodeValue.toFixed(2)}</span>
              </div>
            ) : null}
            <div>
              <span>Delivery charge</span>
              {this.props.totalAmountCheckOut >=
              (this.props.checkForSubsCart(this.props.cartContent)
                ? this.props.deliveryOnAmountSubs
                : this.props.deliveryOnAmount) ? (
                <span style={{ color: "#41a45c", fontWeight: "500" }}>
                  FREE
                </span>
              ) : (
                <span>
                  ₹
                  {this.props.checkForSubsCart(this.props.cartContent)
                    ? this.props.deliveryChargeSubs
                    : this.props.deliveryCharge}
                </span>
              )}
            </div>
            <div className="reusableBillMainTotalContainer">
              <div className="reusableBillEstimatedTotalContainer">
                <span>Estimated Total</span>
                <span>
                  ₹
                  {this.props.totalAmountCheckOut >=
                  (this.props.checkForSubsCart(this.props.cartContent)
                    ? this.props.deliveryOnAmountSubs
                    : this.props.deliveryOnAmount)
                    ? (
                        this.props.totalAmountCheckOut -
                        this.state.promoCodeValue
                      ).toFixed(2)
                    : (
                        this.props.totalAmountCheckOut +
                        (this.props.checkForSubsCart(this.props.cartContent)
                          ? this.props.deliveryChargeSubs
                          : this.props.deliveryCharge) -
                        this.state.promoCodeValue
                      ).toFixed(2)}
                </span>
              </div>
              <p>Inclusive of all taxes*</p>
            </div>
          </div>
          <div ref={this.props.innerRef}></div>

          {(!this.props.details ||
            (this.props.details &&
              this.props.updateDetail?.orderStatus?.serialId &&
              ![57].includes(
                this.props.updateDetail?.orderStatus?.serialId
              ))) &&
          this.props.cartContent?.length > 0 &&
          Number(
            this.props.calcDiscPreProcessingForCart(this.props.cartContent)
          ) +
            this.state.promoCodeValue >
            50 ? (
            <SaveCard
              savings={
                this.props.cartContent?.length > 0
                  ? (
                      Number(
                        this.props.calcDiscPreProcessingForCart(
                          this.props.cartContent
                        )
                      ) + this.state.promoCodeValue
                    ).toFixed(2)
                  : 0
              }
            />
          ) : null}
        </>
      );
    }
  }
}

export default React.forwardRef((props, ref) => (
  <BillDetails innerRef={ref} {...props} />
));

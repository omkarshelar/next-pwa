import React, { Component } from "react";
import "./UploadCard.css";

export class UploadCard extends Component {
  render() {
    return (
      <div className="upload-card-container">
        <div>
          <span>Upload prescription and get medicines</span>
          <span>
            Quick Buy! Place an order in a single click by uploading a valid
            prescription.
          </span>
        </div>
        <p onClick={this.props.onClickUpload}>Upload Prescription</p>
      </div>
    );
  }
}

export default UploadCard;

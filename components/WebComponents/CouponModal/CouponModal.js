import React, { Component } from "react";
import Dialog from "@material-ui/core/Dialog";
import TextField from "@material-ui/core/TextField";
import "./CouponModal.css";
import truemedsLogo from "../../../src/Assets/truemedslogosvg.svg";
import close from "../../../src/Assets/CircleClose.png";
import { Collapse } from "antd";
const { Panel } = Collapse;
export default class CouponModal extends Component {
  state = {
    showTnc: false,
    couponArr: this.props.couponArr,
  };

  componentDidUpdate(prevProps, prevState) {
    if (this.props.couponArr != prevProps.couponArr) {
      this.setState({
        couponArr: this.props.couponArr,
      });
    }
  }

  getDaysFromDate = (timestamp) => {
    let date1 = new Date(timestamp);
    let date2 = new Date();
    let diffTime = Math.abs(date1 - date2);
    let diffDays = Math.ceil(diffTime / (1000 * 60 * 60 * 24));
    return diffDays;
  };

  checkForExpiry = (timestamp) => {
    let isValid = false;
    var date1 = new Date(timestamp);
    var date2 = new Date();
    if (date1 >= date2) {
      isValid = true;
    }
    return isValid;
  };

  checkForCoupons = (arr) => {
    let notAvailable = true;
    for (let i in arr) {
      let coupon = arr[i];
      if (coupon.offersVisible && this.checkForExpiry(coupon.validity)) {
        notAvailable = false;
        break;
      }
    }
    return notAvailable;
  };
  handleShowTnc = (index) => {
    let couponArr = this.state.couponArr;
    couponArr[index]["showTnc"] = true;
    this.setState({ couponArr: couponArr });
  };

  render() {
    return (
      <Dialog
        open={this.props.open}
        onClose={this.props.handleClose}
        aria-labelledby="responsive-dialog-title"
        fullWidth={true}
        className="couponModalMainContainer"
      >
        <img
          src={close}
          alt="close"
          className="couponModalCloseIcon"
          onClick={this.props.handleClose}
        />
        <p className="applyCouponTitle">Apply Coupon</p>
        <div className="couponInputContainer">
          <div style={{ position: "relative" }}>
            <TextField
              id="outlined-basic"
              label="Enter Coupon Code"
              variant="outlined"
              className={
                this.props.errorMsg ? "couponErrorInput" : "couponInput"
              }
              style={{ width: "100%" }}
              value={this.props.couponValue}
              onChange={this.props.onChange}
              error={this.props.errorMsg ? true : false}
              inputProps={{ style: { textTransform: "uppercase" } }}
            />
            <div
              className="applyButtonContainer"
              onClick={this.props.onClickCouponInputApply}
            >
              <span>Apply</span>
            </div>
          </div>
          {this.props.errorMsg && <p>{this.props.errorMsg}</p>}
        </div>
        {this.state.couponArr?.length > 0 &&
          this.state.couponArr.map((data, index) => (
            <div
              className="couponContainer"
              style={{
                display:
                  data.offersVisible && this.checkForExpiry(data.validity)
                    ? ""
                    : "none",
                borderBottom:
                  index + 1 === this.state.couponArr.length
                    ? "none"
                    : "1px solid rgba(0, 0, 0, 0.1)",
              }}
            >
              <div className="couponFirstMainContainer">
                <div className="couponFirstContainer">
                  <div className="couponFirstInsideContainer">
                    <div>
                      <img src={truemedsLogo} alt="truemeds" />
                    </div>
                    <div>
                      <span>{data.promoCode}</span>
                    </div>
                  </div>
                </div>
                <div className="couponSecondContainer">
                  <span>{data.title}</span>
                  <span>{data.description}</span>
                  <span>
                    Expires in {this.getDaysFromDate(data.validity)} days
                  </span>
                  {data.termsAndConditions &&
                    data.termsAndConditions.length > 0 && (
                      <Collapse
                        className="collapse-Header"
                        expandIconPosition="right"
                        ghost
                      >
                        <Panel
                          showArrow={true}
                          className="collapse-Panel"
                          header="Terms & Conditions"
                          key="1"
                        >
                          {data?.termsAndConditions?.map((f) => {
                            return (
                              <div
                                className="textWrapTnc"
                                dangerouslySetInnerHTML={{
                                  __html: f?.description?.replaceAll(
                                    "\n",
                                    "<br />"
                                  ),
                                }}
                              ></div>
                            );
                          })}
                        </Panel>
                      </Collapse>
                    )}
                </div>
              </div>
              <span
                className="couponFirstContainerApplyButton"
                onClick={() => this.props.onClickCouponApply(data)}
              >
                Apply
              </span>
            </div>
          ))}
        {(this.state.couponArr?.length === 0 ||
          this.checkForCoupons(this.state.couponArr)) && (
          <span className="couponNotAvailable">
            No offers available right now
          </span>
        )}
      </Dialog>
    );
  }
}

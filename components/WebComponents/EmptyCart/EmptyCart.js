import React, { Component } from "react";
import emptyCartIcon from "../../../src/Assets/empty-cart.svg";
import "./EmptyCart.css";

export class EmptyCart extends Component {
  render() {
    return (
      <div className="empty-cart-container">
        <img src={emptyCartIcon} alt="empty-cart" />
        <span>Your cart is empty</span>
      </div>
    );
  }
}

export default EmptyCart;

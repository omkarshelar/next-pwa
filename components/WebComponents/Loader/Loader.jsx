import React from "react";
import { connect } from "react-redux";
import "./Loader.Style.css";
import loader from "../../../src/Assets/loader.svg";
import BackDrop from "../../UploadPrescription/UploadSubComponent/DiscardModal/BackDrop";
// import BackDrop from "../../Page/UploadPrescription/UploadSubComponent/DiscardModal/BackDrop";

function Loader() {
  return (
    <>
      <div className="newCustomLoaderWrapper">
        <div className="newLoaderBody">
          <img className="loaderImage" src={loader} alt="loader" />
          <h6>Loading</h6>
        </div>
      </div>
      <BackDrop show={true} />
    </>
  );
}

export default connect(null)(Loader);

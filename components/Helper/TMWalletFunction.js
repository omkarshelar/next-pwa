// function to check is order id is unique
const checkOrderId = (orderId, arr) => {
  let isNew = true;
  if (arr && arr.length > 0) {
    for (let i in arr) {
      if (arr[i] === orderId) {
        isNew = false;
        break;
      }
    }
  }
  return isNew;
};

// function to get single transaction to be displayed for transactions with same order id
const filterTransactionWithSameOrderId = (arr) => {
  let selectedTransaction = {};
  let sortedArr = arr.sort(
    (a, b) => new Date(b.transDate) - new Date(a.transDate)
  );

  selectedTransaction = sortedArr[0];
  selectedTransaction.transaction = sortedArr[sortedArr.length - 1].transaction;
  return selectedTransaction;
};

// function to get filtered transactiions based on type tm cash or tm credit
const filterTransactions = (arrWithAllTypes, type) => {
  let arr = [];
  let finalArr = [];
  let orderIdsArr = [];
  let finalArrWithStatus0 = [];
  let finalArrWithAllStatus = [];
  // filtering array based on type tm cash or tm credit
  arr = arrWithAllTypes.filter((data) => data.type === type);

  // filtering array based on status 0 & 1
  let arrWithStatus0 = arr.filter((data) => data.status === 0);
  let arrWithStatus1 = arr.filter((data) => data.status === 1);

  for (let i in arrWithStatus0) {
    let tempArr = [];
    let transaction = arrWithStatus0[i];
    if (checkOrderId(transaction.orderId, orderIdsArr)) {
      orderIdsArr.push(transaction.orderId);
      for (let j in arrWithStatus0) {
        let transaction1 = arrWithStatus0[j];
        if (transaction1.orderId === transaction.orderId) {
          tempArr.push(transaction1);
        }
      }
      if (tempArr && tempArr.length === 1) {
        finalArrWithStatus0.push(tempArr[0]);
      } else if (tempArr && tempArr.length > 0) {
        let tempObj = {};
        tempObj = filterTransactionWithSameOrderId(tempArr);
        if (tempObj) {
          finalArrWithStatus0.push(tempObj);
        }
      }
    }
  }
  // adding all filtered arrays of status 0 & 1 in final array and sorting based on timestamp
  finalArrWithAllStatus = [...finalArrWithStatus0, ...arrWithStatus1];
  finalArr = finalArrWithAllStatus.sort(
    (a, b) => new Date(b.transDate) - new Date(a.transDate)
  );
  return finalArr;
};

export default filterTransactions;

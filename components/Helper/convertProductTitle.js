const convertProductTitle = (name, id) => {
  return (
    name
      .replace(/[/.]/g, "-")
      .replace(/[&\/\\#,+()$~%'":*?<>{}]/g, "")
      .toLowerCase()
      .trimEnd()
      .split(" ")
      .join("-") +
    "-" +
    id
  );
};
export default convertProductTitle;

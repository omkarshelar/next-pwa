const optimizeImage = (img, width) => {
    return width ? `${img}?tr=cm-pad_resize,bg-FFFFFF,lo-true,w-${width}` : `${img}?tr=cm-pad_resize,bg-FFFFFF,lo-true`;
}

export default optimizeImage;
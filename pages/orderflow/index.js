import React, { Component, createRef } from "react";
import { connect } from "react-redux";
import { withRouter } from "next/router";
import dynamic from "next/dynamic";

//plugins
// import { message } from "antd";
// import Axios from "axios";
import Dialog from "@material-ui/core/Dialog";
import { IoClose } from "react-icons/io5";
import Shimmer from "react-shimmer-effect";
import Skeleton from "@mui/material/Skeleton";
import window from "global";

//css
import "../../pageComponents/orderflow/Orderflow.css";

//Components
// import Cart from "../../components/WebComponents/Cart/Cart";
// import UploadPrescription from "../../components/WebComponents/UploadPrescription/UploadPrescription";
// import OrderSummary from "../../components/WebComponents/OrderSummary/OrderSummary";
import {
  selectTotalAmount,
  selectTotalDiscount,
} from "../../components/WebComponents/Cart/CartSelector";
// import CouponModal from "../../components/WebComponents/CouponModal/CouponModal";
// import Login from "../../components/WebComponents/Login/Login";
// import Modal from "../../components/WebComponents/NewModal/Modal";
import BillDetails from "../../components/WebComponents/BillDetails/BillDetails";
// import CouponCard from "../../components/WebComponents/CouponCard/CouponCard";
import Button from "../../components/WebComponents/Button/Button";
// import EstimatedDeliveryDate from "../../components/WebComponents/OrderSummary/SubComponents/EstimatedDeliveryDate";
// import DeliveryError from "../../components/WebComponents/DeliveryError/DeliveryError";
// import SaveCard from "../../components/WebComponents/SaveCard/SaveCard";
// import DoctorCard from "../../components/WebComponents/OrderSummary/SubComponents/DoctorCards";
// import FeaturedMedicineWOBG from "../../pageComponents/cart/FeaturedMedicineWOBG/FeaturedMedicineWOBG";
import {
  customNotification,
  customNumber,
} from "../../components/Helper/helperFunction";
// import DummyHeight from "../../components/WebComponents/SidePanel/Components/SubComponents/DummyHeight";

//actions
import {
  addItemCartAction,
  removeItemCartAction,
  removeMedThunk,
  getAllCouponsThunk,
  addCouponAction,
  removeCouponAction,
} from "../../redux/Cart/Action";
import { orderStatusThunk } from "../../redux/OrderDetail/Action";
import { incompleteOrderThunk } from "../../redux/IncompleteOrder/Action";
import {
  deleteUploadImageThunk,
  lastImageClearAction,
} from "../../redux/UploadImage/Action";
import { confirmOrderThunk } from "../../redux/ConfirmOrder/Action";
import {
  fetchOrderStatusThunk,
  changePaymentModeThunk,
  orderTrackerThunk,
  setPaymentModeThunk,
} from "../../redux/MyOrder/Action";
import {
  changePaymentAction,
  selectPaymentThunk,
  resetPaymentThunk,
} from "../../redux/Payment/Action";
import { fetchPatientByIdThunk } from "../../redux/PatientDetails/Action";
import {
  toCartSectionAction,
  toUploadPrescriptionAction,
  toPatientDetailsAction,
  toAddressDetailsAction,
  toOrderSummaryAction,
  notOnOrderSummaryAction,
  clearSectionAction,
} from "../../redux/Timeline/Action";
import { discardOrderThunk } from "../../redux/ConfirmOrder/Action";
import { setCurrentPage } from "../../redux/Pincode/Actions";
import { clearMedDetailsAction } from "../../redux/ConfirmMedicine/Action";
import { checkPincodeThunk } from "../../redux/AddressDetails/Action";
import { resetSaveMoreAction } from "../../redux/SaveMore/Action";

//images
import fabIcon from "../../src/Assets/fabIcon.svg";
import rxIcon from "../../src/Assets/upload-rx.svg";
import orderPlaced from "../../src/Assets/orderPlaced.svg";
import orderProcessing from "../../src/Assets/orderProcessing.svg";

//Helper
import { PWA_static_URL, ThirdPartySevice_URL } from "../../constants/Urls";
import { eventOrderSubmission } from "../../Events/Events";
import { getCookie } from "../../components/Helper/parseCookies";
import { wrapper } from "../../redux/store";

//dynamic
// import CouponModal from "../../components/WebComponents/CouponModal/CouponModal";
// import Login from "../../components/WebComponents/Login/Login";
// import Modal from "../../components/WebComponents/NewModal/Modal";

const CouponModal = dynamic(() =>
  import("../../components/WebComponents/CouponModal/CouponModal")
);
const Login = dynamic(() =>
  import("../../components/WebComponents/Login/Login")
);
const Modal = dynamic(() =>
  import("../../components/WebComponents/NewModal/Modal")
);
const Axios = dynamic(() => import("axios"));
const message = dynamic(() => import("antd/lib/message"), { ssr: false });

const Cart = dynamic(() => import("../../components/WebComponents/Cart/Cart"));
const UploadPrescription = dynamic(() =>
  import("../../components/WebComponents/UploadPrescription/UploadPrescription")
);
const OrderSummary = dynamic(() =>
  import("../../components/WebComponents/OrderSummary/OrderSummary")
);

const CouponCard = dynamic(() =>
  import("../../components/WebComponents/CouponCard/CouponCard")
);
const DeliveryError = dynamic(() =>
  import("../../components/WebComponents/DeliveryError/DeliveryError")
);

const FeaturedMedicineWOBG = dynamic(() =>
  import("../../pageComponents/cart/FeaturedMedicineWOBG/FeaturedMedicineWOBG")
);

const DoctorCard = dynamic(() =>
  import(
    "../../components/WebComponents/OrderSummary/SubComponents/DoctorCards"
  )
);

const SaveCard = dynamic(() =>
  import("../../components/WebComponents/SaveCard/SaveCard")
);

const DummyHeight = dynamic(() =>
  import(
    "../../components/WebComponents/SidePanel/Components/SubComponents/DummyHeight"
  )
);

const EstimatedDeliveryDate = dynamic(() =>
  import(
    "../../components/WebComponents/OrderSummary/SubComponents/EstimatedDeliveryDate"
  )
);

let stepperCookies = getCookie("stepperData");
let options = {
  root: null,
  rootMargin: "0px",
};
export class OrderFlow extends Component {
  state = {
    medData: [],
    loading: false,
    loginModal: false,
    couponModal: false,
    summaryCouponModal: false,
    couponData: [],
    couponValue: "",
    couponErr: "",
    couponRemovedModal: false,
    appliedCoupon: {},
    duplicateOrderId: false,
    baseDiscount: this.props.masterDetails?.baseDiscount
      ? Number(this.props.masterDetails.baseDiscount)
      : 20,
    deliveryCharge: this.props.masterDetails?.deliveryCharge
      ? Number(this.props.masterDetails.deliveryCharge)
      : 0,
    deliveryOnAmount: this.props.masterDetails?.deliveryOnAmount
      ? Number(this.props.masterDetails.deliveryOnAmount)
      : 0,
    deliveryChargeSubs: this.props.masterDetails?.deliveryChargeSubs
      ? Number(this.props.masterDetails.deliveryChargeSubs)
      : 0,
    deliveryOnAmountSubs: this.props.masterDetails?.deliveryOnAmountSubs
      ? Number(this.props.masterDetails.deliveryOnAmountSubs)
      : 0,
    tncModal: false,
    showOrderProcessModal: false,
    isOrderProcessing: false,
    isProceed: false,
    showSaveCard: false,
    isVisible: false,
    isVisibleTop: false,
    changePayment: false,
    stateLoading: false,
    redirectDone: false,
  };

  billRef = createRef();
  saveRef = createRef();

  componentDidMount() {
    this.setState({ stateLoading: true });
    this.props.setCurrentPage({ currentPage: "orderFlow" });
    this.setState({ appliedCoupon: this.props.appliedCoupon }, () => {
      this.calculateCouponDiscount(this.state.appliedCoupon);
    });
    if (this.props.pincodeDetails && this.props.Stepper.toCartSection) {
      let data = {
        pincode: this.props.pincodeDetails.pincode,
        history: this.props.router,
      };
      this.props.checkPincodeThunk(data);
    }
    if (this.props.Stepper.toCartSection && this.props.isMobile) {
      window.addEventListener("scroll", this.handleScroll);
    }

    if (this.props.isMobile) {
      window.addEventListener("scroll", () => {
        if (window.scrollY >= 0 && window.scrollY <= 500) {
          this.setState({
            isVisibleTop: false,
          });
        } else {
          this.setState({
            isVisibleTop: true,
          });
        }
      });
    }
  }

  callBackFunction = (e) => {
    const [entry] = e;
    this.setState({ isVisible: entry.isIntersecting });
  };

  useIntersection = () => {
    const observer = new IntersectionObserver(this.callBackFunction, options);
    if (this.saveRef.current) {
      observer.observe(this.saveRef.current);
    }
    return () => {
      if (this.saveRef.current) {
        observer.unobserve(this.saveRef.current);
      }
    };
  };

  handleScroll = () => {
    this.useIntersection();
    /* if(this.state.isVisible)
    {
this.setState({
  showSaveCard:false
})
    }
    else{

    } */
    if (window.pageYOffset > 50 && !this.state.isVisible) {
      this.setState({
        showSaveCard: true,
      });
    } else {
      this.setState({
        showSaveCard: false,
      });
    }
  };

  componentDidUpdate(prevProp, prevState) {
    if (prevState.isProceed !== this.state.isProceed && this.state.isProceed) {
      this.setState({ isProceed: false });
    }

    if (
      prevState.changePayment !== this.state.changePayment &&
      this.state.changePayment
    ) {
      this.setState({ changePayment: false });
    }

    if (
      !this.props.Stepper.toOrderSummary &&
      (prevProp.cartContent !== this.props.cartContent ||
        prevProp.updateDetail !== this.props.updateDetail)
    ) {
      this.calculateCouponDiscount(this.props.appliedCoupon);
    }

    if (
      this.props.Stepper.onOrderSummary &&
      !this.props.Stepper.toOrderSummary &&
      prevProp.Stepper.toOrderSummary !== this.props.Stepper.toOrderSummary
    ) {
      this.setState({ appliedCoupon: this.props.appliedCoupon }, () => {
        this.calculateCouponDiscount(this.state.appliedCoupon);
      });
    }

    if (
      this.props.Stepper.toOrderSummary === true &&
      (prevProp.cartContent !== this.props.cartContent ||
        prevProp.updateDetail !== this.props.updateDetail)
    ) {
      this.calculateCouponDiscountInSummary(this.props.appliedCoupon);
    }

    if (
      prevProp.pincodeDetails?.pincode !== this.props.pincodeDetails?.pincode
    ) {
      if (this.props.medConfirm?.ConfirmMedData?.orderId) {
        this.props
          .orderStatusThunk({
            access_token: this.props.accessToken.Response.access_token,
            orderIds: this.props.medConfirm?.ConfirmMedData?.orderId,
            customerId: this.props.forCustId,
            history: this.props.router,
          })
          .then(() => {
            this.checkPincode();
          });
      }
    }

    if (
      prevProp.updateDetail?.patientId !== this.props.updateDetail?.patientId
    ) {
      this.getPatientDetails();
    }

    if (
      this.props.appliedCoupon != prevProp.appliedCoupon &&
      this.props.appliedCoupon
    ) {
      this.getPaymentIdFromType(this.props.appliedCoupon.applicableOn);
    }

    if (
      prevProp.updateDetail?.paymentMode?.serialId !=
        this.props.updateDetail?.paymentMode?.serialId &&
      !this.props.payment.hasSelected
    ) {
      this.setPaymentMode();
    }
  }

  checkPincode() {
    let data = {
      pincode: this.props.updateDetail?.AddressDetails?.pincode,
      history: this.props.router,
    };
    if (data.pincode) {
      this.props.checkPincodeThunk(data);
    }
  }

  setPaymentMode = () => {
    if (this.props.updateDetail?.paymentMode) {
      let value = false;
      if (this.props.updateDetail.paymentMode.serialId === 16) {
        value = true;
      }
      this.onClickSelectPaymentType(value);
    }
  };

  calculateCouponDiscount = (coupon) => {
    if (coupon?.promoCode && coupon?.discountValue) {
      this.onClickCalculateCouponDiscount(coupon, true);
    }
  };

  calculateCouponDiscountInSummary = (coupon) => {
    if (coupon?.promoCode && coupon?.discountValue) {
      this.onClickCalculateSummaryCouponDiscount(coupon, true);
    }
  };

  loginModalHandler = () => {
    this.setState({ loginModal: !this.state.loginModal });
  };

  getTrackDetails = async () => {
    if (this.props.accessToken && this.props.imgUpload?.orderId) {
      await this.props.fetchOrderStatusThunk({
        accessToken: this.props.accessToken.Response.access_token,
        orderId: this.props.imgUpload.orderId,
        history: this.props.router,
      });
    } else if (
      this.props.accessToken &&
      this.props.medConfirm?.ConfirmMedData?.orderId
    ) {
      await this.props.fetchOrderStatusThunk({
        accessToken: this.props.accessToken.Response.access_token,
        orderId: this.props.medConfirm.ConfirmMedData.orderId,
        history: this.props.router,
      });
    }
  };

  calcDiscPreProcessing = (billArr) => {
    let filterMeds = billArr.filter(
      (data) => !data.disabled && !data.coldChainDisabled
    );
    let finalPrice = filterMeds.reduce(
      (prev, next) =>
        prev +
        (next.orgMrp -
          next.orgMrp *
            ((100 -
              (next.orgProductCd === next.subsProductCd
                ? ((next.subsMrp - next.subsSellingPrice) * 100) / next.subsMrp
                : next.orgDiscount)) /
              100)),

      0
    );
    return finalPrice.toFixed(2);
  };

  calcDiscPreProcessingForCart = (billArr) => {
    let finalPrice = billArr.reduce(
      (prev, next) =>
        prev +
        next.quantity *
          (next._source.original_mrp -
            next._source.original_mrp *
              ((100 -
                (next._source.original_product_code ===
                next._source.subs_product_code
                  ? ((next._source.subs_mrp - next._source.subs_selling_price) *
                      100) /
                    next._source.subs_mrp
                  : next._source.original_base_discount)) /
                100)),

      0
    );
    return finalPrice.toFixed(2);
  };

  // applyDeliveryChargeCart = (arr) => {
  //   let freeDelivery = false;
  //   for (let i in arr) {
  //     let med = arr[i];
  //     if (med._source.subs_found) {
  //       freeDelivery = true;
  //       break;
  //     }
  //   }
  //   return freeDelivery;
  // };

  checkForSubsCart = (arr) => {
    let isSubs = false;
    for (let i in arr) {
      let med = arr[i];
      if (med._source.subs_found) {
        isSubs = true;
        break;
      }
    }
    return isSubs;
  };

  // applyDeliveryChargeSummary = (arr) => {
  //   let freeDelivery = false;
  //   for (let i in arr) {
  //     let med = arr[i];
  //     if (med.statusId === 61) {
  //       freeDelivery = true;
  //       break;
  //     }
  //   }
  //   return freeDelivery;
  // };

  checkForSubs = (arr) => {
    let isSubs = false;
    for (let i in arr) {
      let med = arr[i];
      if (med.statusId === 61) {
        isSubs = true;
        break;
      }
    }
    return isSubs;
  };

  calcTotalPreProcessing = (billArr, mrp, charge, couponValue) => {
    let finalDisc = this.calcDiscPreProcessing(billArr);
    let calcTotal = 0;
    if (couponValue) {
      calcTotal = mrp - finalDisc + charge - couponValue;
    } else {
      calcTotal = mrp - finalDisc + charge;
    }
    return calcTotal.toFixed(2);
  };

  onClickSelectPaymentType = (isOnline) => {
    if (!this.props.appliedCoupon) {
      this.props.changePaymentAction(isOnline);
    } else {
      if (
        this.props.appliedCoupon.applicableOn === "Online Payment" &&
        !isOnline
      ) {
        customNotification(
          "Only Online payment allowed with selected Coupon",
          "customNotify"
        );
      } else if (
        this.props.appliedCoupon.applicableOn === "Cod Payment" &&
        isOnline
      ) {
        customNotification(
          "Only COD payment allowed with selected Coupon",
          "customNotify"
        );
      } else {
        this.props.changePaymentAction(isOnline);
      }
    }
  };

  changeQtyInArr = (arr, code, qty) => {
    for (var i in arr) {
      let med = arr[i];
      if (med.productCode === code) {
        med.quantity = Number(qty);
        break;
      }
    }
    return arr;
  };

  getPaymentIdFromType = (type) => {
    // setting payment id based on the paymnet type we get from coupon details
    let paymentId = this.props.payment.isOnline ? 16 : 17;
    if (type === "Cod Payment") {
      paymentId = 17;
      this.onClickSelectPaymentType(false);
    } else if (type === "Online Payment") {
      paymentId = 16;
      this.onClickSelectPaymentType(true);
    }
    return paymentId;
  };

  onClickincompleteOrderWithSummary = async (orgProductCd, value) => {
    if (this.props.medConfirm?.ConfirmMedData?.orderId) {
      // if coupon is applied, set payment mode
      if (this.props.appliedCoupon?.offerId) {
        await this.props.setPaymentModeThunk({
          orderId: this.props.medConfirm.ConfirmMedData.orderId,
          access_token: this.props.accessToken?.Response.access_token,
          paymentId: this.getPaymentIdFromType(
            this.props.appliedCoupon.applicableOn
          ),
        });
      }
      await this.props.incompleteOrderThunk({
        orderIds: this.props.medConfirm.ConfirmMedData.orderId,
        accessToken: this.props.accessToken?.Response.access_token,
        medSet: this.changeQtyInArr(this.state.medData, orgProductCd, value),
        offerId: this.props.appliedCoupon?.offerId
          ? this.props.appliedCoupon.offerId
          : 0,
        history: this.props.router,
        pincode: this.props.pincodeDetails.pincode,
      });
      if (
        this.props.medIncomplete.incompleteOrderError ===
        "Sorry order cannot be confirmed. Please contact customer support."
      ) {
        this.props.clearMedDetailsAction().then(() => {
          this.props.lastImageClearAction().then(() => {
            message.error("Oops! Please try to place the order again.");
            this.props.router.push("/");
            this.props.clearSectionAction();
          });
        });
      }
    } else if (this.props.imgUpload?.orderId) {
      // if coupon is applied, set payment mode
      if (this.props.appliedCoupon?.offerId) {
        await this.props.setPaymentModeThunk({
          orderId: this.props.imgUpload.orderId,
          access_token: this.props.accessToken?.Response.access_token,
          paymentId: this.getPaymentIdFromType(
            this.props.appliedCoupon.applicableOn
          ),
        });
      }
      await this.props.incompleteOrderThunk({
        orderIds: this.props.imgUpload.orderId,
        accessToken: this.props.accessToken?.Response.access_token,
        medSet: this.changeQtyInArr(this.state.medData, orgProductCd, value),
        offerId: this.props.appliedCoupon?.offerId
          ? this.props.appliedCoupon.offerId
          : 0,
        history: this.props.router,
        pincode: this.props.pincodeDetails.pincode,
      });
      if (
        this.props.medIncomplete.incompleteOrderError ===
        "Sorry order cannot be confirmed. Please contact customer support."
      ) {
        this.props.clearMedDetailsAction().then(() => {
          this.props.lastImageClearAction().then(() => {
            message.error("Oops! Please try to place the order again.");
            this.props.router.push("/");
            this.props.clearSectionAction();
          });
        });
      }
    }
  };

  setDuplicateOrderId = (status) => {
    this.setState({ duplicateOrderId: status });
  };

  createOrderWithSummary = () => {
    this.setState(
      {
        duplicateOrderId: false,
        medData: this.props.cartContent.map((data) => {
          return {
            productCode: data._source.original_product_code,
            quantity: data.quantity,
          };
        }),
      },
      async () => {
        if (this.props.medConfirm?.ConfirmMedData?.orderId) {
          // if coupon is applied, set payment mode
          if (this.props.appliedCoupon?.offerId) {
            await this.props.setPaymentModeThunk({
              orderId: this.props.medConfirm.ConfirmMedData.orderId,
              access_token: this.props.accessToken?.Response.access_token,
              paymentId: this.getPaymentIdFromType(
                this.props.appliedCoupon.applicableOn
              ),
            });
          }
          this.props
            .incompleteOrderThunk({
              orderIds: this.props.medConfirm.ConfirmMedData.orderId,
              accessToken: this.props.accessToken.Response.access_token,
              medSet: this.state.medData,
              offerId: this.props.appliedCoupon?.offerId
                ? this.props.appliedCoupon.offerId
                : 0,
              history: this.props.router,
              pincode: this.props.pincodeDetails.pincode,
            })
            .then(() => {
              if (this.props.medIncomplete.incompleteOrderError) {
                if (
                  this.props.medIncomplete.incompleteOrderError ===
                  "Sorry order cannot be confirmed. Please contact customer support."
                ) {
                  this.props.clearMedDetailsAction().then(() => {
                    this.props.lastImageClearAction().then(() => {
                      this.props.notOnOrderSummaryAction();
                      this.setState({ duplicateOrderId: true });
                    });
                  });
                } else {
                  message.error(this.props.medIncomplete.incompleteOrderError);
                }
              } else {
                this.props.toOrderSummaryAction();
              }
            });
        } else if (this.props.imgUpload?.orderId) {
          // if coupon is applied, set payment mode
          if (this.props.appliedCoupon?.offerId) {
            await this.props.setPaymentModeThunk({
              orderId: this.props.imgUpload.orderId,
              access_token: this.props.accessToken?.Response.access_token,
              paymentId: this.getPaymentIdFromType(
                this.props.appliedCoupon.applicableOn
              ),
            });
          }
          this.props
            .incompleteOrderThunk({
              orderIds: this.props.imgUpload.orderId,
              accessToken: this.props.accessToken.Response.access_token,
              medSet: this.state.medData,
              offerId: this.props.appliedCoupon?.offerId
                ? this.props.appliedCoupon.offerId
                : 0,
              history: this.props.router,
              pincode: this.props.pincodeDetails.pincode,
            })
            .then(() => {
              if (this.props.medIncomplete.incompleteOrderError) {
                if (
                  this.props.medIncomplete.incompleteOrderError ===
                  "Sorry order cannot be confirmed. Please contact customer support."
                ) {
                  this.props.clearMedDetailsAction().then(() => {
                    this.props.lastImageClearAction().then(() => {
                      this.props.notOnOrderSummaryAction();
                      this.setState({ duplicateOrderId: true });
                    });
                  });
                } else {
                  message.error(this.props.medIncomplete.incompleteOrderError);
                }
              } else {
                this.props.toOrderSummaryAction();
              }
            });
        }
      }
    );
  };

  updateOrderForCouponInSummary = () => {
    this.setState(
      {
        medData: this.props.cartContent.map((data) => {
          return {
            productCode: data._source.original_product_code,
            quantity: data.quantity,
          };
        }),
      },
      async () => {
        if (this.props.medConfirm?.ConfirmMedData?.orderId) {
          // if coupon is applied, set payment mode
          if (this.props.appliedCoupon?.offerId) {
            await this.props.setPaymentModeThunk({
              orderId: this.props.medConfirm.ConfirmMedData.orderId,
              access_token: this.props.accessToken?.Response.access_token,
              paymentId: this.getPaymentIdFromType(
                this.props.appliedCoupon.applicableOn
              ),
            });
          }
          this.props
            .incompleteOrderThunk({
              orderIds: this.props.medConfirm.ConfirmMedData.orderId,
              accessToken: this.props.accessToken.Response.access_token,
              medSet: this.state.medData,
              offerId: this.props.appliedCoupon?.offerId
                ? this.props.appliedCoupon.offerId
                : 0,
              history: this.props.router,
              pincode: this.props.pincodeDetails.pincode,
            })
            .then(() => {
              this.props
                .orderStatusThunk({
                  access_token: this.props.accessToken.Response.access_token,
                  orderIds: this.props.medConfirm.ConfirmMedData.orderId,
                  customerId: this.props.forCustId,
                  history: this.props.router,
                })
                .then(() => {
                  this.checkPincode();
                });
            });
        } else if (this.props.imgUpload?.orderId) {
          // if coupon is applied, set payment mode
          if (this.props.appliedCoupon?.offerId) {
            await this.props.setPaymentModeThunk({
              orderId: this.props.imgUpload.orderId,
              access_token: this.props.accessToken?.Response.access_token,
              paymentId: this.getPaymentIdFromType(
                this.props.appliedCoupon.applicableOn
              ),
            });
          }
          this.props
            .incompleteOrderThunk({
              orderIds: this.props.imgUpload.orderId,
              accessToken: this.props.accessToken.Response.access_token,
              medSet: this.state.medData,
              offerId: this.props.appliedCoupon?.offerId
                ? this.props.appliedCoupon.offerId
                : 0,
              history: this.props.router,
              pincode: this.props.pincodeDetails.pincode,
            })
            .then(() => {
              this.props
                .orderStatusThunk({
                  access_token: this.props.accessToken.Response.access_token,
                  orderIds: this.props.imgUpload.orderId,
                  customerId: this.props.forCustId,
                  history: this.props.router,
                })
                .then(() => {
                  this.checkPincode();
                });
            });
        }
      }
    );
  };

  getPatientDetails = () => {
    if (this.props.accessToken && this.props.imgUpload?.orderId) {
      this.props.fetchPatientByIdThunk({
        accessToken: this.props.accessToken.Response.access_token,
        orderId: this.props.imgUpload.orderId,
        patientId: 0,
        history: this.props.router,
      });
    } else if (
      this.props.accessToken &&
      this.props.medConfirm?.ConfirmMedData?.orderId
    ) {
      this.props.fetchPatientByIdThunk({
        accessToken: this.props.accessToken.Response.access_token,
        orderId: this.props.medConfirm.ConfirmMedData.orderId,
        patientId: 0,
        history: this.props.router,
      });
    }
  };

  getAllOrderDetails = async () => {
    // Updated APi
    if (
      this.props.accessToken &&
      this.props.imgUpload?.orderId &&
      this.props.forCustId
    ) {
      await this.props
        .orderStatusThunk({
          access_token: this.props.accessToken.Response.access_token,
          orderIds: this.props.imgUpload.orderId,
          customerId: this.props.forCustId,
          history: this.props.router,
        })
        .then(() => {
          this.checkPincode();
          if (this.props.orderStatusUpdate?.error) {
            this.discardOrder();
            this.props.router.push("/");
          }
        });
      if (
        this.props.updateDetail?.ImageMasterDto?.length === 0 &&
        this.props.updateDetail?.productSubsMappingList?.length === 0
      ) {
        this.discardOrder();
      }
    } else if (
      this.props.accessToken &&
      this.props.medConfirm?.ConfirmMedData?.orderId &&
      this.props.forCustId
    ) {
      await this.props
        .orderStatusThunk({
          access_token: this.props.accessToken.Response.access_token,
          orderIds: this.props.medConfirm.ConfirmMedData.orderId,
          customerId: this.props.forCustId,
          history: this.props.router,
        })
        .then(() => {
          this.checkPincode();
        });
      if (
        this.props.updateDetail?.ImageMasterDto?.length === 0 &&
        this.props.updateDetail?.productSubsMappingList?.length === 0
      ) {
        this.discardOrder();
      }
    }
  };

  removeMedOnClick = async (data) => {
    if (this.props.medConfirm?.ConfirmMedData?.orderId) {
      await this.props.removeMedThunk({
        orderId: this.props.medConfirm.ConfirmMedData.orderId,
        access_token: this.props.accessToken.Response.access_token,
        medData: data,
        history: this.props.router,
      });

      if (
        this.props.Stepper.toCartSection === true &&
        this.props.cartContent?.length === 0 &&
        this.props.imgUpload?.uploadHistory?.length === 0
      ) {
        this.discardOrder(true);
      }
    } else if (this.props.imgUpload?.orderId) {
      await this.props.removeMedThunk({
        orderId: this.props.imgUpload.orderId,
        access_token: this.props.accessToken.Response.access_token,
        medData: data,
        history: this.props.router,
      });
      if (
        this.props.Stepper.toCartSection === true &&
        this.props.cartContent?.length === 0 &&
        this.props.imgUpload?.uploadHistory?.length === 0
      ) {
        this.discardOrder(true);
      }
    }
  };

  removeMedOnClickFinal = async (data) => {
    await this.removeMedOnClick(data);
    this.getAllOrderDetails();
  };

  quantityOnHandleChange = (value, content) => {
    if (this.props.updateDetail?.productSubsMappingList?.length > 0) {
      this.setState(
        {
          medData: this.props.updateDetail.productSubsMappingList.map(
            (data) => {
              return {
                productCode: data.orgProductCd,
                quantity: data.orgQuantity,
              };
            }
          ),
        },
        async () => {
          await this.onClickincompleteOrderWithSummary(
            content.orgProductCd,
            value
          );
          this.props.addItemCartAction(
            this.addNewQuantity(
              {
                _source: {
                  original_product_code: content.orgProductCd,
                },
              },
              value
            )
          );
          this.getAllOrderDetails();
        }
      );
    }
  };

  addNewQuantity = (obj, qty) => {
    obj.newQty = Number(qty);
    return obj;
  };

  quantityOnHandleChangeForCart = (value, content) => {
    if (this.props.cartContent?.length > 0) {
      this.setState(
        {
          medData: this.props.cartContent.map((data) => {
            return {
              productCode: data.orgProductCd,
              quantity: data.orgQuantity,
            };
          }),
        },
        () => {
          this.props.addItemCartAction(this.addNewQuantity(content, value));
        }
      );
    }
  };

  removeMedContent = (code) => {
    let content = {};
    let arr = this.props.cartContent;
    for (let i in arr) {
      let med = arr[i];
      if (med._source.original_product_code === code) {
        content = med;
        break;
      }
    }
    return content;
  };

  onClickDeleteUploadImage = (data) => {
    if (this.props.imgUpload?.orderId) {
      this.props
        .deleteUploadImageThunk({
          imgIdSet: [data.imageId],
          access_token: this.props.accessToken?.Response.access_token,
          orderIds: this.props.imgUpload.orderId,
          history: this.props.router,
        })
        .then(() => {
          this.getAllOrderDetails();
          if (this.props.imgUpload?.uploadHistory?.length === 0) {
            this.props.lastImageClearAction();
          }
        });
    }
  };

  orderConfirm = async () => {
    this.setState({
      showOrderProcessModal: true,
      isOrderProcessing: true,
    });
    let id = 0;
    this.props.resetSaveMoreAction();
    if (
      this.props.imgUpload?.uploadHistory?.length > 0 &&
      !this.props.isUpload
    ) {
      let arr = this.props.imgUpload.uploadHistory;
      for (let i in arr) {
        let img = arr[i];
        let imgData = {
          imgIdSet: [img.imgId],
          access_token: this.props.accessToken?.Response.access_token,
          orderIds: this.props.imgUpload.orderId,
          history: this.props.router,
        };
        await this.props.deleteUploadImageThunk(imgData);
      }
    }
    if (this.props.imgUpload?.orderId) {
      id = this.props.imgUpload.orderId;
      // this.setState({ loading: true });
      this.props
        .confirmOrderThunk({
          access_token: this.props.accessToken.Response.access_token,
          paymentid: this.props.payment.isOnline ? 16 : 17,
          orderid: this.props.imgUpload.orderId,
          history: this.props.router,
          offerId: this.props.appliedCoupon?.offerId
            ? this.props.appliedCoupon.offerId
            : 0,
          pincode: this.props.pincodeDetails.pincode,
        })
        .then(() => {
          this.setState({ loading: false });
          this.props.resetPaymentThunk();
          if (this.props.confirmOrder.error) {
            this.setState({
              showOrderProcessModal: false,
              isOrderProcessing: false,
            });
            if (
              this.props.confirmOrder.error ===
              "Sorry order cannot be confirmed. Please contact customer support."
            ) {
              this.props.clearMedDetailsAction().then(() => {
                this.props.lastImageClearAction().then(() => {
                  message.error("Oops! Please try to place the order again.");
                  this.props.router.push("/");
                  this.props.clearSectionAction();
                });
              });
            } else {
              message.error(this.props.confirmOrder.error);
            }
          } else {
            // Event_Truemeds
            // new
            eventOrderSubmission(
              this.props.updateDetail?.finalCalcAmt?.orderValue
            );
            this.setState({
              showOrderProcessModal: true,
              isOrderProcessing: false,
            });
            setTimeout(() => {
              this.props.router.push(`/orderstatus/${id}`);
            }, 2000);

            // message.success("Order placed successfully!");
          }
        });
    } else if (this.props.medConfirm?.ConfirmMedData?.orderId) {
      id = this.props.medConfirm.ConfirmMedData.orderId;
      // this.setState({ loading: true });
      this.props
        .confirmOrderThunk({
          access_token: this.props.accessToken.Response.access_token,
          paymentid: this.props.payment.isOnline ? 16 : 17,
          orderid: this.props.medConfirm.ConfirmMedData.orderId,
          history: this.props.router,
          offerId: this.props.appliedCoupon?.offerId
            ? this.props.appliedCoupon.offerId
            : 0,
          pincode: this.props.pincodeDetails.pincode,
        })
        .then(() => {
          this.setState({ loading: false });
          this.props.resetPaymentThunk();
          if (this.props.confirmOrder.error) {
            this.setState({
              showOrderProcessModal: false,
              isOrderProcessing: false,
            });
            if (
              this.props.confirmOrder.error ===
              "Sorry order cannot be confirmed. Please contact customer support."
            ) {
              this.props.clearMedDetailsAction().then(() => {
                this.props.lastImageClearAction().then(() => {
                  message.error("Oops! Please try to place the order again.");
                  this.props.router.push("/");
                  this.props.clearSectionAction();
                });
              });
            } else {
              message.error(this.props.confirmOrder.error);
            }
          } else {
            // Event_Truemeds
            // new
            eventOrderSubmission(
              this.props.updateDetail?.finalCalcAmt?.orderValue
            );
            this.setState({
              showOrderProcessModal: true,
              isOrderProcessing: false,
            });
            setTimeout(() => {
              this.props.router.push(`/orderstatus/${id}`);
            }, 2000);
            // message.success("Order placed successfully!");
          }
        });
    }
    /*  this.setState({
      showOrderProcessModal: false,
      isOrderProcessing: false,
    }); */
  };

  cashFreeLinkImpl = (orderId) => {
    const reqParamOrderId = orderId;
    const Uri = `${ThirdPartySevice_URL}/createPaymentUrlInCashFree?orderId=${reqParamOrderId}&returnUrl=${PWA_static_URL}`;
    const config = {
      headers: {
        "Content-Type": "application/json",
        Authorization:
          "Bearer " + this.props.accessToken?.Response?.access_token,
        "Access-Control-Allow-Origin": "*",
      },
    };
    Axios.get(Uri, config)
      .then((response) => {
        if (response.status === 200) {
          // window.open(response.data.paymentLink);
          window.location.href = response.data.paymentLink;
        }
      })
      .catch((err) => {
        console.error("createPaymentUrlInCashFree", err.response);
      });
  };

  changePaymentServiceCall = (orderId) => {
    if (orderId) {
      this.props
        .changePaymentModeThunk({
          accessToken: this.props.accessToken?.Response?.access_token,
          orderId: orderId,
          paymentModeId: 17,
          history: this.props.router,
        })
        .then(() => {
          if (this.props.orderTracker.changePaymentModeError) {
            message.error("Something went wrong");
          } else {
            this.props.orderTrackerThunk({
              accessToken: this.props.accessToken?.Response?.access_token,
            });
            this.getAllOrderDetails();
          }
        });
    }
  };

  onChangeCouponValue = (e) => {
    this.setState({ couponValue: e.target.value.toUpperCase() });
  };

  checkForCouponCode = (couponCode, couponArr) => {
    let isValid = false;
    for (let i in couponArr) {
      let coupon = couponArr[i];
      if (couponCode === coupon.promoCode) {
        isValid = true;
      }
    }
    return isValid;
  };

  checkForExpiry = (timestamp) => {
    let isValid = false;
    var date1 = new Date(timestamp);
    var date2 = new Date();
    if (date1 >= date2) {
      isValid = true;
    }
    return isValid;
  };

  checkForCouponValidity = (couponCode, couponArr) => {
    let isValid = false;
    for (let i in couponArr) {
      let coupon = couponArr[i];
      if (
        couponCode === coupon.promoCode &&
        this.checkForExpiry(coupon.validity)
      ) {
        isValid = true;
      }
    }
    return isValid;
  };

  getCouponDataFromCode = (couponCode, couponArr) => {
    let obj = {};
    for (let i in couponArr) {
      let coupon = couponArr[i];
      if (couponCode === coupon.promoCode) {
        obj = coupon;
      }
    }
    return obj;
  };

  onClickCouponInputApply = () => {
    if (
      !this.state.couponValue ||
      !this.checkForCouponCode(this.state.couponValue, this.state.couponData)
    ) {
      this.setState({
        couponErr: "This coupon is invalid. Please try another coupon",
      });
    } else if (
      !this.checkForCouponValidity(
        this.state.couponValue,
        this.state.couponData
      )
    ) {
      this.setState({
        couponErr: "This coupon is expired. Please try another coupon",
      });
    } else {
      this.setState({ couponErr: "" }, () => {
        if (this.props.Stepper.toOrderSummary === true) {
          this.onClickCalculateSummaryCouponDiscount(
            this.getCouponDataFromCode(
              this.state.couponValue,
              this.state.couponData
            )
          );
        } else {
          this.onClickCalculateCouponDiscount(
            this.getCouponDataFromCode(
              this.state.couponValue,
              this.state.couponData
            )
          );
        }
      });
    }
  };

  onClickCouponApply = (data) => {
    if (this.props.Stepper.toOrderSummary === true) {
      this.onClickCalculateSummaryCouponDiscount(data);
    } else {
      this.onClickCalculateCouponDiscount(data);
    }
  };

  checkMedsDiscount = (arr) => {
    let isInvalid = true;
    for (let i in arr) {
      let med = arr[i];
      let discount =
        med._source.original_product_code !== med._source.subs_product_code
          ? med._source.original_base_discount
          : ((med._source.original_mrp - med._source.subs_selling_price) *
              100) /
            med._source.original_mrp;
      if (Math.round(discount) >= this.state.baseDiscount && discount <= 25) {
        isInvalid = false;
        break;
      }
    }
    return isInvalid;
  };

  checkMedsDiscountInSummary = (arr) => {
    let isInvalid = true;
    if (arr?.length > 0) {
      for (let i in arr) {
        let med = arr[i];
        let discount =
          med.orgProductCd !== med.subsProductCd
            ? med.orgDiscount
            : ((med.orgMrp - med.subsSellingPrice) * 100) / med.orgMrp;
        if (Math.round(discount) >= this.state.baseDiscount && discount <= 25) {
          isInvalid = false;
          break;
        }
      }
    }
    return isInvalid;
  };

  onClickCalculateCouponDiscount = async (data, recalculate) => {
    let couponDiscount = 0;
    // check for minimum cart value
    if (this.props.totalAmountCheckOut < data.minCartValue) {
      if (recalculate) {
        this.setState({
          couponRemovedModal: true,
          appliedPromoCode: this.props.appliedCoupon.promoCode,
        });
        await this.props.removeCouponAction();
        this.setState({
          appliedCoupon: {},
        });
      } else {
        message.error(
          "Oops! The minimum cart value should be ₹" + data.minCartValue
        );
      }
    }
    // check for medicines discount
    else if (this.checkMedsDiscount(this.props.cartContent)) {
      if (recalculate) {
        this.setState({
          couponRemovedModal: true,
          appliedPromoCode: this.props.appliedCoupon.promoCode,
        });
        await this.props.removeCouponAction();
        this.setState({
          appliedCoupon: {},
        });
      } else {
        message.error("Best discount already applied!");
      }
    }
    // check for coupon id
    else if (data.discountType.serialId === 76) {
      couponDiscount = data.discountValue;
      this.onClickApplyCoupon(data, couponDiscount, recalculate);
    } else {
      couponDiscount = this.calculateCouponDiscountFromPercentage(data);
      this.onClickApplyCoupon(data, couponDiscount, recalculate);
    }
  };

  filterMedList = (arr) => {
    let finalArr = [];
    if (arr?.length > 0) {
      for (let i in arr) {
        let med = arr[i];
        if (!med.coldChainDisabled && !med.disabled && med.medActive) {
          finalArr.push(med);
        }
      }
    }
    return finalArr;
  };

  // function to calculate coupon discount in summary
  onClickCalculateSummaryCouponDiscount = async (data, recalculate) => {
    let couponDiscount = 0;
    // check for minimum cart value
    if (
      Number(
        this.getTotalSpForSummary(
          this.checkMedsForSummary(
            this.props.updateDetail?.productSubsMappingList
          )
        )
      ) < data.minCartValue
    ) {
      if (recalculate) {
        this.setState({
          couponRemovedModal: true,
          appliedPromoCode: this.props.appliedCoupon.promoCode,
        });
        await this.props.removeCouponAction();
        this.setState({
          appliedCoupon: {},
        });
      } else {
        message.error(
          "Oops! The minimum cart value should be ₹" + data.minCartValue
        );
      }
    }
    // check for medicines discount
    else if (
      this.checkMedsDiscountInSummary(
        this.filterMedList(this.props.updateDetail?.productSubsMappingList)
      )
    ) {
      if (recalculate) {
        this.setState({
          couponRemovedModal: true,
          appliedPromoCode: this.props.appliedCoupon.promoCode,
        });
        await this.props.removeCouponAction();
        this.setState({
          appliedCoupon: {},
        });
      } else {
        message.error("Best discount already applied!");
      }
    }
    // check for coupon id
    else if (data.discountType.serialId === 76) {
      couponDiscount = data.discountValue;
      this.onClickApplyCoupon(data, couponDiscount, recalculate);
    } else {
      couponDiscount =
        this.calculateCouponDiscountFromPercentageInSummary(data);
      this.onClickApplyCoupon(data, couponDiscount, recalculate);
    }
  };

  calculateCouponDiscountFromPercentage = (data) => {
    let finalValue = 0;
    let discount = this.calculateDiscountFromMedsArr(
      data.discountValue,
      this.props.cartContent
    );
    if (Math.round(discount) <= data.maxDiscount) {
      finalValue = discount;
    } else {
      finalValue = data.maxDiscount;
    }
    return finalValue;
  };

  calculateDiscountFromMedsArr = (discount, arr) => {
    let val = 0;
    for (let i in arr) {
      let med = arr[i];
      let newDiscount =
        med._source.original_product_code !== med._source.subs_product_code
          ? med._source.original_base_discount
          : ((med._source.original_mrp - med._source.subs_selling_price) *
              100) /
            med._source.original_mrp;
      if (
        Math.round(newDiscount) >= this.state.baseDiscount &&
        newDiscount <= 25
      ) {
        let discountValue =
          (med._source.original_mrp * med.quantity * discount) / 100;
        val = val + discountValue;
      }
    }
    return val;
  };

  calculateCouponDiscountFromPercentageInSummary = (data) => {
    let finalValue = 0;
    let discount = this.calculateDiscountFromMedsArrInSummary(
      data.discountValue,
      this.filterMedList(this.props.updateDetail?.productSubsMappingList)
    );
    if (Math.round(discount) <= data.maxDiscount) {
      finalValue = discount;
    } else {
      finalValue = data.maxDiscount;
    }
    return finalValue;
  };

  calculateDiscountFromMedsArrInSummary = (discount, arr) => {
    let val = 0;
    if (arr?.length > 0) {
      for (let i in arr) {
        let med = arr[i];
        let newDiscount =
          med.orgProductCd !== med.subsProductCd
            ? med.orgDiscount
            : ((med.orgMrp - med.subsSellingPrice) * 100) / med.orgMrp;
        if (
          Math.round(newDiscount) >= this.state.baseDiscount &&
          newDiscount <= 25
        ) {
          let discountValue = (med.orgMrp * discount) / 100;
          val = val + discountValue;
        }
      }
    }
    return val;
  };

  onClickApplyCoupon = async (data, discount, recalculate) => {
    data.couponDiscount = discount;
    // save applied coupon data
    this.props.addCouponAction(data).then(() => {
      this.setState({ appliedCoupon: this.props.appliedCoupon });
      if (!recalculate) {
        if (this.props.Stepper.toOrderSummary === true) {
          this.updateOrderForCouponInSummary();
        }
        if (this.props.Stepper.toOrderSummary === true) {
          this.setState({ summaryCouponModal: false }, () => {
            message.success("Hurray! Coupon code applied!");
          });
        } else {
          this.setState({ couponModal: false }, () => {
            message.success("Hurray! Coupon code applied!");
          });
        }
      }
    });
  };

  onClickApply = () => {
    if (this.props.Stepper.toOrderSummary === true) {
    } else {
    }
    // get coupon details if loggedin
    if (this.props.accessToken && this.props.loginDetails.isUserLogged) {
      this.props
        .getAllCouponsThunk({
          access_token: this.props.accessToken.Response.access_token,
          customerId: this.props.forCustId,
          history: this.props.router,
        })
        .then(() => {
          if (this.props.cartErrorMessage) {
            message.error(this.props.cartErrorMessage);
          } else {
            this.setState(
              {
                couponData: this.props.couponData,
                couponValue: "",
                couponErr: "",
              },
              () => {
                if (this.props.Stepper.toOrderSummary === true) {
                  this.setState({ summaryCouponModal: true });
                } else {
                  this.setState({
                    couponModal: true,
                  });
                }
              }
            );
          }
        });
    } else {
      // login
      this.setState({ loginModal: true });
    }
  };

  onClickRemove = async () => {
    if (this.props.Stepper.toOrderSummary === true) {
      this.updateOrderForCouponInSummary();
    }
    await this.props.removeCouponAction();
    this.setState({ appliedCoupon: {} });
    message.success("Coupon code removed");
  };

  // checking meds for cold chain and med active
  checkMedsForSummary = (arr) => {
    let newArr = [];
    if (arr?.length > 0) {
      for (let i in arr) {
        let med = arr[i];
        if (!med.coldChainDisabled && !med.disabled && med.medActive) {
          newArr.push(med);
        }
      }
    }
    return newArr;
  };

  getTotalMrpForSummary = (arr) => {
    let totalMrp = 0;
    if (arr?.length > 0) {
      for (let i in arr) {
        let med = arr[i];
        totalMrp = totalMrp + med.orgMrp;
      }
    }
    return totalMrp.toFixed(2);
  };

  getTotalDiscountForSummary = (arr) => {
    let totalDiscount = 0;
    if (arr?.length > 0) {
      for (let i in arr) {
        let med = arr[i];
        med.orgProductCd === med.subsProductCd
          ? (totalDiscount =
              totalDiscount + (med.orgMrp - med.subsSellingPrice))
          : (totalDiscount =
              totalDiscount +
              (med.orgMrp -
                (med.orgMrp - med.orgMrp * (med.orgDiscount / 100))));
      }
    }
    return totalDiscount.toFixed(2);
  };

  getTotalSpForSummary = (arr) => {
    let totalSp = 0;
    if (arr?.length > 0) {
      for (let i in arr) {
        let med = arr[i];
        let discount =
          med.orgProductCd !== med.subsProductCd
            ? med.orgDiscount
            : ((med.orgMrp - med.subsSellingPrice) * 100) / med.orgMrp;
        let orgMedSp = med.orgMrp - med.orgMrp * (discount / 100);
        totalSp = totalSp + orgMedSp;
      }
    }
    return totalSp;
  };

  onClickTnc = () => {
    this.setState({
      tncModal: true,
    });
  };

  orderTrack = () => {
    if (this.props.trackOrder?.clickPostTrackingUrl) {
      // window.open(this.props.trackOrder.clickPostTrackingUrl);
      window.location.href = this.props.trackOrder.clickPostTrackingUrl;
    }
  };

  discardOrder = (emptyCart) => {
    if (this.props.updateDetail?.orderId) {
      this.props.discardOrderThunk({
        access_token: this.props.accessToken?.Response?.access_token,
        orderid: this.props.updateDetail?.orderId,
        history: this.props.router,
      });
      this.props.clearMedDetailsAction().then(() => {
        this.props.lastImageClearAction().then(() => {
          if (!emptyCart) {
            // this.props.router.push("/");
          }
          this.props.clearSectionAction();
        });
      });
    } else if (this.props.medConfirm?.ConfirmMedData?.orderId) {
      this.props.discardOrderThunk({
        access_token: this.props.accessToken?.Response?.access_token,
        orderid: this.props.medConfirm.ConfirmMedData.orderId,
        history: this.props.router,
      });
      this.props.clearMedDetailsAction().then(() => {
        this.props.lastImageClearAction().then(() => {
          if (!emptyCart) {
            // this.props.router.push("/");
          }
          this.props.clearSectionAction();
        });
      });
    } else if (this.props.imgUpload?.orderId) {
      this.props.discardOrderThunk({
        access_token: this.props.accessToken?.Response?.access_token,
        orderid: this.props.imgUpload.orderId,
        history: this.props.router,
      });
      this.props.clearMedDetailsAction().then(() => {
        this.props.lastImageClearAction().then(() => {
          if (!emptyCart) {
            // this.props.router.push("/");
          }
          this.props.clearSectionAction();
        });
      });
    }
  };

  render() {
    if (stepperCookies) {
      if (
        !stepperCookies.toCartSection &&
        !stepperCookies.toUploadPrescription
      ) {
        if (!this.state.showOrderProcessModal && !this.state.redirectDone) {
          this.setState({ redirectDone: true });
          this.props.router.replace("/empty-cart");
        }
      }
    }
    let stepperArr = [];
    if (this.props.Stepper) {
      stepperArr = [
        {
          name: "View Cart",
          value: this.props.Stepper.toCartSection,
          hide: this.props.cartContent?.length > 0 ? false : true,
        },
        {
          name: "Upload Prescription",
          value: this.props.Stepper.toUploadPrescription,
        },
        { name: "Patient Details", value: this.props.Stepper.toPatientDetails },
        { name: "Address Details", value: this.props.Stepper.toAddressDetails },
        {
          name: "Order Summary",
          value: this.props.Stepper.toOrderSummary,
          noHr: true,
        },
      ];
    }

    let savingsCart =
      this.props.cartContent?.length > 0
        ? (
            Number(this.calcDiscPreProcessingForCart(this.props.cartContent)) +
            Number(
              this.state.appliedCoupon?.couponDiscount
                ? this.state.appliedCoupon?.couponDiscount
                : 0
            )
          ).toFixed(2)
        : 0;
    return (
      <div className="orderFlowMainContainer">
        <Login
          loginModal={this.state.loginModal}
          loginModalHandler={this.loginModalHandler}
        />
        <CouponModal
          open={this.state.couponModal}
          handleClose={() => this.setState({ couponModal: false })}
          couponArr={this.state.couponData}
          couponValue={this.state.couponValue}
          onChange={this.onChangeCouponValue}
          onClickCouponInputApply={this.onClickCouponInputApply}
          errorMsg={this.state.couponErr}
          onClickCouponApply={this.onClickCouponApply}
        />
        <CouponModal
          open={this.state.summaryCouponModal}
          handleClose={() => this.setState({ summaryCouponModal: false })}
          couponArr={this.state.couponData}
          couponValue={this.state.couponValue}
          onChange={this.onChangeCouponValue}
          onClickCouponInputApply={this.onClickCouponInputApply}
          errorMsg={this.state.couponErr}
          onClickCouponApply={this.onClickCouponApply}
        />
        <Modal
          open={this.state.couponRemovedModal}
          Header={
            this.state.appliedPromoCode
              ? `Coupon Code "${this.state.appliedPromoCode}" is no longer applicable for the order`
              : "Coupon code is not applicable to your order"
          }
          yesText="Continue"
          handleClose={() =>
            this.setState({ couponRemovedModal: false, appliedPromoCode: null })
          }
          yesMethod={() =>
            this.setState({ couponRemovedModal: false, appliedPromoCode: null })
          }
        />
        {/* Need only on cart hence checking witn true */}
        {this.props.Stepper.toCartSection === true &&
          !this.props.isLoading &&
          this.state.stateLoading &&
          savingsCart > 50 && (
            <SaveCard
              savings={
                this.props.cartContent?.length > 0
                  ? (
                      Number(
                        this.calcDiscPreProcessingForCart(
                          this.props.cartContent
                        )
                      ) +
                      Number(
                        this.state.appliedCoupon?.couponDiscount
                          ? this.state.appliedCoupon?.couponDiscount
                          : 0
                      )
                    ).toFixed(2)
                  : 0
              }
              isCart={true}
              showSaveCard={this.state.showSaveCard}
            />
          )}

        <div className="orderFlowFirstContainer">
          {this.props.Stepper.toCartSection === true && (
            <>
              <Cart
                isProceed={this.state.isProceed}
                loginDetails={this.props.loginDetails}
                timeout={this.props.timeout}
                cartContents={this.props.cartContent}
                createOrderWithSummary={this.createOrderWithSummary}
                quantityOnHandleChange={this.quantityOnHandleChangeForCart}
                removeMedOnClick={this.removeMedOnClick}
                removeItemCart={this.props.removeItemCartAction}
                accessToken={this.props.accessToken}
                totalAmountCheckOut={this.props.totalAmountCheckOut}
                totalDiscountMed={this.props.totalDiscountMed}
                medConfirm={this.props.medConfirm}
                custId={this.props.forCustId}
                medIncomplete={this.props.medIncomplete}
                payment={this.props.payment}
                Stepper={this.props.Stepper}
                isLoading={this.props.isLoading || !this.state.stateLoading}
                imgUpload={this.props.imgUpload}
                appliedCoupon={this.props.appliedCoupon}
                getPaymentIdFromType={this.getPaymentIdFromType}
                duplicateOrderId={this.state.duplicateOrderId}
                setDuplicateOrderId={this.setDuplicateOrderId}
                deliveryCharge={
                  this.checkForSubsCart(this.props.cartContent)
                    ? this.state.deliveryChargeSubs
                    : this.state.deliveryCharge
                }
                deliveryOnAmount={
                  this.checkForSubsCart(this.props.cartContent)
                    ? this.state.deliveryOnAmountSubs
                    : this.state.deliveryOnAmount
                }
                calcDiscPreProcessingForCart={this.calcDiscPreProcessingForCart}
                discardOrder={this.discardOrder}
              />
            </>
          )}
          {this.props.Stepper.toUploadPrescription === true && (
            <UploadPrescription
              isProceed={this.state.isProceed}
              appliedCoupon={this.props.appliedCoupon}
            />
          )}
          {this.props.Stepper.toOrderSummary === true && (
            <OrderSummary
              isProceed={this.state.isProceed}
              changePayment={this.state.changePayment}
              quantityOnHandleChange={this.quantityOnHandleChange}
              removeMedOnClickFinal={this.removeMedOnClickFinal}
              updateDetails={this.props.updateDetail}
              removeItemCart={this.props.removeItemCartAction}
              removeMedContent={this.removeMedContent}
              orderConfirm={this.orderConfirm}
              cartContents={this.props.cartContent}
              imgUpload={this.props.imgUpload}
              accessToken={this.props.accessToken}
              trackOrder={this.props.trackOrder}
              getAllOrderDetails={this.getAllOrderDetails}
              getPatientDetails={this.getPatientDetails}
              getTrackDetails={this.getTrackDetails}
              onClickDeleteUploadImage={this.onClickDeleteUploadImage}
              patientData={this.props.patientData}
              isLoading={this.props.isLoading || !this.state.stateLoading}
              loading={this.state.loading}
              cashFreeLinkImpl={this.cashFreeLinkImpl}
              changePaymentServiceCall={this.changePaymentServiceCall}
              onClickSelectPaymentType={(e) => {
                this.onClickSelectPaymentType(e);
                this.props.selectPaymentThunk();
              }}
              payment={this.props.payment}
              promoCodeValue={
                this.state.appliedCoupon?.couponDiscount
                  ? this.state.appliedCoupon?.couponDiscount
                  : 0
              }
              calcDiscPreProcessingForCart={this.calcDiscPreProcessingForCart}
              calcDiscPreProcessing={this.calcDiscPreProcessing}
            />
          )}
        </div>
        <div className="orderFlowSecondContainer">
          <div className="orderFlowSecondInsideContainer">
            {this.props.Stepper.toUploadPrescription === true ? (
              <img className="upload-rx-icon" src={rxIcon} alt="rx" />
            ) : (
              <>
                {this.props.Stepper.toCartSection === true &&
                  this.props.pincodeDetails &&
                  this.props.isServiceable?.isServiceable === false && (
                    <DeliveryError
                      onClickHandler={() => {
                        window.openPincodeModal(true);
                      }}
                      pincodeDetails={this.props.pincodeDetails}
                      isServiceable={this.props.isServiceable}
                    />
                  )}

                {this.props.isLoading || !this.state.stateLoading ? (
                  <div className="rightSectionLoader">
                    <Skeleton animation="wave" height={80} />
                  </div>
                ) : (
                  <>
                    {(this.props.Stepper.toOrderSummary === true ||
                      this.props.Stepper.toCartSection === true ||
                      this.props.Stepper.toUploadPrescription === true) && (
                      <CouponCard
                        promoCode={this.state.appliedCoupon?.promoCode}
                        promoCodeValue={
                          this.state.appliedCoupon?.couponDiscount
                        }
                        onClickApply={this.onClickApply}
                        onClickRemove={this.onClickRemove}
                        onClickTnc={this.onClickTnc}
                      />
                    )}
                  </>
                )}

                {this.props.isLoading || !this.state.stateLoading ? (
                  <Shimmer>
                    <div className="orderSummarymedicineAddedLoader" />
                    <div className="orderSummaryPrescriptionCardLoader" />
                  </Shimmer>
                ) : (
                  this.props.isMobile &&
                  this.props.Stepper.toOrderSummary && (
                    <>
                      <DoctorCard />
                    </>
                  )
                )}

                {this.props.isMobile &&
                  !this.props.Stepper.toOrderSummary &&
                  this.props.Stepper.toCartSection && (
                    <div className="featureMedSection">
                      <FeaturedMedicineWOBG />
                    </div>
                  )}

                {(this.props.Stepper.toOrderSummary === true ||
                  this.props.Stepper.toCartSection === true ||
                  this.props.Stepper.toUploadPrescription === true) && (
                  <BillDetails
                    ref={this.saveRef}
                    cartContent={this.props.cartContent}
                    totalDiscountMed={Number(this.props.totalDiscountMed)}
                    totalAmountCheckOut={Number(this.props.totalAmountCheckOut)}
                    baseDiscount={this.state.baseDiscount}
                    deliveryCharge={this.state.deliveryCharge}
                    deliveryOnAmount={this.state.deliveryOnAmount}
                    deliveryChargeSubs={this.state.deliveryChargeSubs}
                    deliveryOnAmountSubs={this.state.deliveryOnAmountSubs}
                    calcDiscPreProcessingForCart={
                      this.calcDiscPreProcessingForCart
                    }
                    checkForSubsCart={this.checkForSubsCart}
                    isSummary={this.props.Stepper.toOrderSummary}
                    updateDetail={this.props.updateDetail}
                    calcDiscPreProcessing={this.calcDiscPreProcessing}
                    calcTotalPreProcessing={this.calcTotalPreProcessing}
                    getTotalSpForSummary={this.getTotalSpForSummary}
                    checkMedsForSummary={this.checkMedsForSummary}
                    getTotalMrpForSummary={this.getTotalMrpForSummary}
                    getTotalDiscountForSummary={this.getTotalDiscountForSummary}
                    checkForSubs={this.checkForSubs}
                    appliedCoupon={this.state.appliedCoupon}
                    promoCode={this.state.appliedCoupon?.promoCode}
                    promoCodeValue={this.state.appliedCoupon?.couponDiscount}
                    isLoading={this.props.isLoading || !this.state.stateLoading}
                    savingsCart={savingsCart}
                  />
                )}
              </>
            )}

            {this.props.updateDetail?.deliveryDate &&
              this.props.Stepper.toOrderSummary &&
              (this.props.isLoading || !this.state.stateLoading ? (
                <div className="rightSectionLoader">
                  <Skeleton animation="wave" height={80} />
                </div>
              ) : (
                <EstimatedDeliveryDate
                  deliveryDate={this.props.updateDetail?.deliveryDate}
                  orderStatus={this.props.updateDetail?.orderStatus?.serialId}
                />
              ))}
            {this.props.isMobile && <DummyHeight height="106px" />}

            <div
              className={
                this.props.cartContent?.length > 0 &&
                this.props.isMobile &&
                this.props.Stepper.toUploadPrescription !== true
                  ? "tmProceedBtnMobContainer"
                  : "tmProceedBtnContainer"
              }
            >
              {this.props.cartContent?.length > 0 &&
              this.props.isMobile &&
              this.props.Stepper.toUploadPrescription !== true ? (
                <div>
                  {this.props.isLoading || !this.state.stateLoading ? (
                    <div className="rightSectionLoader">
                      <Skeleton animation="wave" height={80} />
                    </div>
                  ) : this.props.Stepper.toOrderSummary ? (
                    this.props.updateDetail?.orderStatus?.serialId === 49 ||
                    this.props.updateDetail?.orderStatus?.serialId === 39 ||
                    this.props.updateDetail?.orderStatus?.serialId === 2 ? (
                      // Summary Bill before order confirm
                      <>
                        {this.getTotalSpForSummary(
                          this.checkMedsForSummary(
                            this.props.updateDetail.productSubsMappingList
                          )
                        ) ? (
                          <>
                            <span>
                              ₹
                              {Number(
                                this.getTotalSpForSummary(
                                  this.checkMedsForSummary(
                                    this.props.updateDetail
                                      .productSubsMappingList
                                  )
                                )
                              ) >=
                              (this.checkForSubs(
                                this.props.updateDetail?.productSubsMappingList
                              )
                                ? this.state.deliveryOnAmountSubs
                                : this.state.deliveryOnAmount)
                                ? (
                                    this.getTotalSpForSummary(
                                      this.checkMedsForSummary(
                                        this.props.updateDetail
                                          .productSubsMappingList
                                      )
                                    ) -
                                    customNumber(
                                      this.state.appliedCoupon?.couponDiscount
                                    )
                                  ).toFixed(2)
                                : (
                                    this.getTotalSpForSummary(
                                      this.checkMedsForSummary(
                                        this.props.updateDetail
                                          .productSubsMappingList
                                      )
                                    ) +
                                    (this.checkForSubs(
                                      this.props.updateDetail
                                        ?.productSubsMappingList
                                    )
                                      ? this.state.deliveryChargeSubs
                                      : this.state.deliveryCharge) -
                                    customNumber(
                                      this.state.appliedCoupon?.couponDiscount
                                    )
                                  ).toFixed(2)}
                            </span>
                          </>
                        ) : (
                          <span>₹ {0.0}</span>
                        )}
                      </>
                    ) : (
                      <>
                        {this.props.updateDetail?.finalCalcAmt?.totalAmount ? (
                          <>
                            <span>
                              ₹
                              {this.props.updateDetail?.finalCalcAmt
                                ?.totalAmount
                                ? this.props.updateDetail.finalCalcAmt
                                    .totalAmount
                                : this.props.updateDetail?.finalCalcAmt
                                    ?.finalAmount
                                ? this.props.updateDetail.finalCalcAmt
                                    .finalAmount
                                : 0}
                            </span>
                          </>
                        ) : (
                          <span>₹ {0.0}</span>
                        )}
                      </>
                    )
                  ) : (
                    <span>
                      ₹
                      {customNumber(this.props.totalAmountCheckOut) >=
                      (this.checkForSubsCart(this.props.cartContent)
                        ? this.state.deliveryOnAmountSubs
                        : this.state.deliveryOnAmount)
                        ? (
                            customNumber(this.props.totalAmountCheckOut) -
                            customNumber(
                              this.state.appliedCoupon?.couponDiscount
                            )
                          ).toFixed(2)
                        : (
                            customNumber(this.props.totalAmountCheckOut) +
                            (this.checkForSubsCart(this.props.cartContent)
                              ? this.state.deliveryChargeSubs
                              : this.state.deliveryCharge) -
                            customNumber(
                              this.state.appliedCoupon?.couponDiscount
                            )
                          ).toFixed(2)}
                    </span>
                  )}
                  {!this.props.isLoading && this.state.stateLoading && (
                    <span
                      onClick={() => {
                        var ua = navigator.userAgent.toLowerCase();
                        if (ua.indexOf("safari") != -1) {
                          if (ua.indexOf("chrome") > -1) {
                            if (this.billRef.current) {
                              this.billRef.current.scrollIntoView({
                                behavior: "smooth",
                                block: "nearest",
                              });
                            }
                          } else {
                            window.scrollTo(0, 999999999);
                          }
                        }
                      }}
                    >
                      View Details
                    </span>
                  )}
                </div>
              ) : null}
              {this.props.isLoading || !this.state.stateLoading ? (
                <div className="rightSectionLoader">
                  <Skeleton animation="wave" height={80} />
                </div>
              ) : (
                (this.props.Stepper.toOrderSummary === true ||
                  this.props.Stepper.toCartSection === true ||
                  this.props.Stepper.toUploadPrescription === true) && (
                  <Button
                    id="tmProceedBtn"
                    CartAdd
                    onClick={() => this.setState({ isProceed: true })}
                  >
                    {this.props.Stepper.toOrderSummary
                      ? "Place Order"
                      : "Proceed"}
                  </Button>
                )
              )}
            </div>
          </div>
        </div>
        {this.props.isMobile &&
          this.state.isVisibleTop &&
          this.props.Stepper.toCartSection === true && (
            <div className="scrollTopBtn" onClick={() => window.scrollTo(0, 0)}>
              <img src={fabIcon} />
            </div>
          )}

        <Dialog
          fullScreen={false}
          open={this.state.tncModal}
          aria-labelledby="responsive-dialog-title"
          classes={{ paper: "terms-rectangle" }}
          id="responsive-dialog-title"
          onClose={() => {
            this.setState({
              tncModal: false,
            });
          }}
        >
          <div className="terms-rectangleWrap">
            <div className="titleWrap">Terms & Conditions</div>
            <div
              onClick={() => {
                this.setState({
                  tncModal: false,
                });
              }}
              className="closeWrap"
            >
              <IoClose />
            </div>
            {this.state.appliedCoupon.termsAndConditions &&
              this.state.appliedCoupon.termsAndConditions.map((e) => {
                return (
                  <div
                    className="textWrap"
                    dangerouslySetInnerHTML={{
                      __html: e.description.replaceAll("\n", "<br />"),
                    }}
                  ></div>
                );
              })}
          </div>
        </Dialog>

        <Dialog
          fullScreen={false}
          open={this.state.showOrderProcessModal}
          aria-labelledby="responsive-dialog-title"
          classes={{ paper: "order-processing-modal" }}
          id="responsive-dialog-title"
          onClose={() => {
            /* this.setState({
              showOrderProcessModal: false,
            }); */
          }}
        >
          <div className="orderPlacing_wrap">
            {this.state.isOrderProcessing ? (
              <div className="orderProcessing">
                <div className="orderProcessing_img">
                  <img src={orderProcessing} />
                </div>
                <div className="orderProcessing_text">
                  Your Order is being placed
                </div>
              </div>
            ) : (
              <div className="orderProcessing">
                <div className="orderPlaced_img">
                  <img src={orderPlaced} />
                </div>
                <div className="orderProcessing_text">Order Placed</div>
              </div>
            )}
          </div>
        </Dialog>
        <div ref={this.billRef} />
      </div>
    );
  }
}

OrderFlow.getInitialProps = wrapper.getInitialPageProps(
  (store) => async (props) => {
    let prop = {};
    return { ...prop };
  }
);

let mapStateToProps = (state) => ({
  Stepper: state.stepper,
  cartContent: state.cartData?.cartItems,
  couponData: state.cartData?.couponDetails?.payload?.data,
  appliedCoupon: state.cartData?.appliedCoupon,
  cartErrorMessage: state.cartData?.error,
  accessToken: state.loginReducer?.verifyOtpSuccess,
  loginDetails: state.loginReducer,
  totalAmountCheckOut: selectTotalAmount(state),
  totalDiscountMed: selectTotalDiscount(state),
  medConfirm: state.confirmMedicineReducer,
  imgUpload: state.uploadImage,
  forCustId: state.loginReducer?.verifyOtpSuccess?.CustomerId,
  trackOrder: state.myOrder?.orderStatusSuccess,
  updateDetail: state.orderStatusUpdate?.OrderStatusData,
  medIncomplete: state.incompleteOrderReducer,
  confirmOrder: state.confirmOrder,
  payment: state.paymentReducer,
  patientData: state.patientData?.fetchPatientByIdSuccess,
  isLoading: state.loader?.isLoading,
  timeout: state.myOrder?.orderTrackerError,
  orderTracker: state.myOrder,
  pincodeDetails: state.pincodeData?.pincodeData,
  currentPage: state.pincodeData?.currentPage?.currentPage,
  masterDetails: state.masterDetails?.MasterDetails,
  isServiceable: state.addressData.pincodeSuccess,
  orderStatusUpdate: state.orderStatusUpdate,
  isUpload: state.uploadImg?.isUpload,
});

export default withRouter(
  connect(mapStateToProps, {
    checkPincodeThunk,
    setCurrentPage,
    addItemCartAction,
    removeItemCartAction,
    removeMedThunk,
    getAllCouponsThunk,
    addCouponAction,
    removeCouponAction,
    orderStatusThunk,
    incompleteOrderThunk,
    deleteUploadImageThunk,
    lastImageClearAction,
    confirmOrderThunk,
    fetchOrderStatusThunk,
    changePaymentAction,
    fetchPatientByIdThunk,
    toCartSectionAction,
    toUploadPrescriptionAction,
    toPatientDetailsAction,
    toAddressDetailsAction,
    toOrderSummaryAction,
    changePaymentModeThunk,
    orderTrackerThunk,
    setPaymentModeThunk,
    clearMedDetailsAction,
    notOnOrderSummaryAction,
    clearSectionAction,
    selectPaymentThunk,
    resetPaymentThunk,
    discardOrderThunk,
    resetSaveMoreAction,
  })(OrderFlow)
);

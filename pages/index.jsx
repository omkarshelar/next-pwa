import React, { Component } from "react";
import { connect } from "react-redux";
import Router, { withRouter } from "next/router";
import window from "global";
import dynamic from "next/dynamic";
import Image from "next/image";
// import Axios from "axios";
// import { message } from "antd";

//helper
import {
  PWA_static_URL,
  ThirdPartySevice_URL,
  Image_URL,
} from "../constants/Urls";
import { eventHomepageCallIcon } from "../src/Events/Events";
import { wrapper } from "../redux/store";
import { parseCookies } from "../components/Helper/parseCookies";

//images
import paperclip from "../src/Assets/paperclip.svg";
import call from "../components/WebComponents/Upload/call.svg";

//actions
import { getBannersThunk } from "../redux/BannerImages/Action";
import { FeaturedMedThunk } from "../redux/FeaturedMedicine/Action";
import { mySavingsThunk } from "../redux/MySaving/Action";
import { toUploadPrescriptionAction } from "../redux/Timeline/Action";
import { doctorDetailThunk } from "../redux/Doctor/Action";
import { fetchMedicineThunk } from "../redux/SearchMedicine/Action";
import { fetchArticleListDetails } from "../redux/HeadlessHealth/Action";
import { setCurrentPage } from "../redux/Pincode/Actions";
import {
  allOrdersThunk,
  changePaymentModeThunk,
  orderTrackerThunk,
} from "../redux/MyOrder/Action";

//Components
import Articles from "../components/WebComponents/Articles/Articles";
import { Banner } from "../components/WebComponents/Banner/Banner";
import Button from "../components/WebComponents/Button/Button";
import Download from "../components/WebComponents/Download/Download";
import FAQ from "../components/WebComponents/FAQ/FAQ";
import Feature from "../components/WebComponents/Feature/Feature";
import FeaturedMedicine from "../components/WebComponents/FeaturedMedicine/FeaturedMedicine";
// import Modal from "../components/WebComponents/NewModal/Modal";
import Placeorder from "../components/WebComponents/PlaceOrder/Placeorder";
import Recommend from "../components/WebComponents/Recommendation/Recommendation";
// import RecentOrders from "../components/WebComponents/RecentOrders/RecentOrders";
// import Saving from "../components/WebComponents/Saving/Saving";
import Testimonial from "../components/WebComponents/Testimonial/Testimonial";
import TruemedsDoctor from "../components/WebComponents/TruemedsDoctor/TruemedsDoctor";

//css
import {
  HomeWrapper,
  ButtonUpload,
  PaymentContent,
  FooterAnimation,
} from "../pageComponents/Home/Home.style";

//dynamic Imports
const Axios = dynamic(() => import("axios"));
const Modal = dynamic(() =>
  import("../components/WebComponents/NewModal/Modal")
);
const message = dynamic(() => import("antd/lib/message"), { ssr: false });

const RecentOrders = dynamic(() =>
  import("../components/WebComponents/RecentOrders/RecentOrders")
);
const Saving = dynamic(() =>
  import("../components/WebComponents/Saving/Saving")
);

export class Home extends Component {
  state = {
    modalShow: false,
    search: false,
    paymentFlag: false,
    changePayment: false,
    paymentPendingOrderId: undefined,
    paymentValue: null,
    allOrders: [],
    callMyorderService: false,
    bannerImgData: null,
  };

  componentDidMount() {
    this.props.setCurrentPage({ currentPage: "home" });

    if (this.refs.vidRef) {
      this.refs.vidRef.play();
    }

    this.setState({
      bannerImgData: this.props.bannerData?.newBanners.sort(
        (a, b) => a.ranking - b.ranking
      ),
    });

    const paymentPending = this.props.orderHistory?.orderTrackerSuccess?.find(
      (item) => item.statusId === 58
    );
    if (paymentPending) {
      this.setState({
        paymentFlag: true,
        paymentValue: paymentPending.paymentValue,
        paymentPendingOrderId: paymentPending.orderId,
      });
    }

    if (this.props.orderHistory?.allOrdersError) {
      message.error(this.props.orderHistory.allOrdersError);
    } else if (
      this.props.orderHistory?.allOrdersSuccess?.currentOrder &&
      this.props.orderHistory?.allOrdersSuccess?.pastOrder
    ) {
      this.setState({
        allOrders: [
          ...this.props.orderHistory.allOrdersSuccess.currentOrder,
          ...this.props.orderHistory.allOrdersSuccess.pastOrder,
        ],
      });
    }

    let data = {};
    data.warehouseId = this.props.pincodeDetails?.warehouseId
      ? this.props.pincodeDetails.warehouseId
      : 3;
  }

  getAllOrderHandler = () => {
    this.props
      .allOrdersThunk({
        accessToken: this.props?.accessToken,
        history: Router,
      })
      .then(() => {
        if (this.props.orderHistory.allOrdersError) {
          message.error(this.props.orderHistory.allOrdersError);
        } else {
          if (
            this.props.orderHistory?.allOrdersSuccess?.currentOrder &&
            this.props.orderHistory?.allOrdersSuccess?.pastOrder
          ) {
            this.setState({
              allOrders: [
                ...this.props.orderHistory.allOrdersSuccess.currentOrder,
                ...this.props.orderHistory.allOrdersSuccess.pastOrder,
              ],
            });
          }
        }
      });
  };

  componentDidUpdate(prevProp, prevState) {
    if (prevProp.accessToken !== this.props.accessToken) {
      let data = {};
      data.warehouseId = this.props.pincodeDetails?.warehouseId
        ? this.props.pincodeDetails.warehouseId
        : 3;
      if (this.props.accessToken) {
        data.access_token = this.props.accessToken;
      }
      FeaturedMedThunk(data);
      this.props
        .orderTrackerThunk({ accessToken: this.props?.accessToken })
        .then(() => {
          if (this.props.orderHistory?.orderTrackerSuccess) {
            const paymentPending =
              this.props.orderHistory?.orderTrackerSuccess.find(
                (item) => item.statusId === 58
              );
            if (paymentPending) {
              this.setState({
                paymentFlag: true,
                paymentValue: paymentPending.paymentValue,
                paymentPendingOrderId: paymentPending.orderId,
              });
            }
          }
        });
      this.getAllOrderHandler();
    }
    if (prevState.callMyorderService !== this.state.callMyorderService) {
      this.getAllOrderHandler();
    }
    if (
      this.props.pincodeDetails &&
      prevProp.pincodeDetails?.warehouseId !=
        this.props.pincodeDetails.warehouseId
    ) {
      let data = {};
      data.warehouseId = this.props.pincodeDetails.warehouseId;
      if (this.props.accessToken) {
        data.access_token = this.props.accessToken;
      }
      this.props.FeaturedMedThunk(data);
    }
  }

  changePaymentActivate = () => {
    this.setState({
      changePayment: true,
      paymentFlag: false,
    });
  };

  changePaymentServiceCall = () => {
    const orderId = this.state.paymentPendingOrderId;
    if (orderId) {
      this.props
        .changePaymentModeThunk({
          accessToken: this.props?.accessToken,
          orderId: orderId,
          paymentModeId: 17,
          history: Router,
        })
        .then(() => {
          if (this.props.orderHistory.changePaymentModeError) {
            message.error("something went wrong");
          } else {
            this.props
              .orderTrackerThunk({ accessToken: this.props?.accessToken })
              .then(() => {
                if (this.props.orderHistory?.orderTrackerSuccess) {
                  this.setState({
                    paymentFlag: false,
                    changePayment: false,
                    paymentPendingOrderId: undefined,
                    callMyorderService: true,
                  });
                }
              });
          }
        });
    }
  };

  cashFreeLinkImpl = (orderId) => {
    const reqParamOrderId = orderId
      ? orderId
      : this.state.paymentPendingOrderId;
    const Uri = `${ThirdPartySevice_URL}/createPaymentUrlInCashFree?orderId=${reqParamOrderId}&returnUrl=${PWA_static_URL}`;
    const config = {
      headers: {
        "Content-Type": "application/json",
        Authorization: "Bearer " + this.props?.accessToken,
        "Access-Control-Allow-Origin": "*",
      },
    };
    Axios.get(Uri, config)
      .then((response) => {
        if (response.status === 200) {
          window.location.href = response.data.paymentLink;
          this.setState({
            paymentFlag: false,
            changePayment: false,
            paymentValue: undefined,
          });
        }
      })
      .catch((err) => {
        console.error("createPaymentUrlInCashFree", err.response);
      });
  };

  triggerCallEvent = () => {
    // Event_Truemeds
    eventHomepageCallIcon();
  };

  render() {
    return (
      <HomeWrapper>
        <Banner
          history={Router}
          accessToken={this.props.accessToken}
          bannerImages={this.props.bannerData?.newBanners}
          bannerLoading={this.props.bannerLoading}
        />

        <div className="call" onClick={() => this.triggerCallEvent()}>
          <a href="tel:022-48977965">
            <img src={call} alt="call"></img>
          </a>
        </div>

        <ButtonUpload>
          <Button
            uploadPres
            onClick={() => {
              this.props.toUploadPrescriptionAction();
              Router.push("/orderflow");
            }}
          >
            <Image width={16} height={17} src={paperclip} alt="upload" />
            <span>Upload prescription</span>
          </Button>
        </ButtonUpload>

        {this.props.accessToken && this.props.timeout !== 401 && (
          <>
            <RecentOrders allOrders={this.state.allOrders} />
            <Saving
              saving={this.props.saving?.CustomerDetails?.totalSavingsTillDate}
            />
          </>
        )}
        {this.props.saving?.CustomerDetails?.totalSavingsTillDate > 0 ? null : (
          <Placeorder />
        )}

        <FeaturedMedicine
          SuggestMedicine={this.props.SuggestMedicine}
          isMobile={this.props.isMobile}
        />
        <Recommend />
        <TruemedsDoctor
          doctor={this.props.doctor}
          isLoading1={this.props.isLoading1}
          isMobile={this.props.isMobile}
        />
        <Articles
          article={this.props.article}
          isLoading2={this.props.isLoading2}
          isMobile={this.props.isMobile}
        />
        <Testimonial />
        <Download />
        <FAQ history={Router} />
        <Feature />
        <FooterAnimation>
          <video ref="vidRef" autoPlay loop muted playsInline>
            <source
              src={`${Image_URL}Images/Assets/footer_animation.mp4`}
              type="video/mp4"
            />
            Your browser does not support the video
          </video>
        </FooterAnimation>
        {this.state.paymentFlag && this.state.paymentValue ? (
          <Modal
            open={this.state.paymentFlag}
            Header="Payment Pending"
            handleClose={() => this.setState({ paymentFlag: false })}
            yesMethod={() => {
              Router.push(`/orderstatus/${this.state.paymentPendingOrderId}`);
            }}
            yesText="view order details"
            noMethod={this.changePaymentActivate}
            noText="Change Payment Mode"
          >
            <PaymentContent>
              <span>
                ₹{this.state.paymentValue ? this.state.paymentValue : 0}
              </span>
              <Button
                makepayment
                onClick={() =>
                  this.cashFreeLinkImpl(this.state.paymentPendingOrderId)
                }
              >
                Pay Now
              </Button>
            </PaymentContent>
          </Modal>
        ) : null}
        {this.state.changePayment ? (
          <>
            <Modal
              open={this.state.changePayment}
              Header="Please Confirm"
              Subheader="Are you sure you want to change payment mode to COD for this order ?"
              handleClose={() => this.setState({ changePayment: false })}
              noMethod={() => {
                this.changePaymentServiceCall();
              }}
              noText="Change TO COD"
              yesMethod={() =>
                this.cashFreeLinkImpl(this.state.paymentPendingOrderId)
              }
              yesText="Make online payment"
            />
          </>
        ) : null}
      </HomeWrapper>
    );
  }
}

//server calls
Home.getInitialProps = wrapper.getInitialPageProps((store) => async (props) => {
  const cookies = parseCookies(props.req);

  const p1 = new Promise((resolve, reject) => {
    store.dispatch(getBannersThunk(Router)).then(() => resolve());
  });

  const p2 = new Promise((resolve, reject) => {
    if (cookies?.token) {
      store
        .dispatch(
          orderTrackerThunk({
            accessToken: cookies?.token,
          })
        )
        .then(async () => {
          if (store.getState().myOrder?.orderTrackerSuccess) {
            const s1 = new Promise((resolve, reject) => {
              store
                .dispatch(
                  mySavingsThunk({
                    history: Router,
                    accessToken: cookies?.token,
                    customerId: cookies?.customerId,
                  })
                )
                .then(() => {
                  resolve();
                });
            });

            const s2 = new Promise((resolve, reject) => {
              store
                .dispatch(
                  allOrdersThunk({
                    accessToken: cookies?.token,
                    history: Router,
                  })
                )
                .then(() => {
                  resolve();
                });
            });
            await Promise.all([s1, s2]).then(() => {
              resolve();
            });
          } else {
            resolve();
          }
        });
    } else {
      resolve();
    }
  });

  const p3 = new Promise((resolve, reject) => {
    let token = cookies?.token;
    if (cookies?.token) {
      token = cookies?.token;
    }
    store.dispatch(doctorDetailThunk(Router, 1, token)).then(() => {
      resolve();
    });
  });

  const p4 = new Promise((resolve, reject) => {
    store
      .dispatch(
        fetchArticleListDetails({
          accessToken: cookies?.token,
          per_page: 10,
        })
      )
      .then(() => {
        resolve();
      });
  });

  let data = {};
  data.warehouseId = store.getState().pincodeData?.pincodeData?.warehouseId
    ? store.getState().pincodeData.pincodeData.warehouseId
    : 3;
  if (cookies?.token) {
    data.access_token = cookies?.token;
  }

  const p5 = new Promise((resolve, reject) => {
    store.dispatch(FeaturedMedThunk(data)).then(() => {
      resolve();
    });
  });

  let prop = {};
  prop.accessToken = cookies?.token;

  await Promise.all([p1, p2, p3, p4, p5]).then(() => {
    prop.bannerData = store.getState().bannerImages?.BannerData;
    if (prop.bannerData?.newBanners) {
      prop.bannerData.newBanners = prop.bannerData.newBanners.sort(
        (a, b) => a.ranking - b.ranking
      );
    }
    prop.bannerLoading = store.getState().bannerImages?.isLoading;
    prop.saving = store.getState().savingsReducer.savingsData;
    prop.orderHistory = store.getState().myOrder;
    prop.timeout = store.getState().myOrder.orderTrackerError;
    prop.customerId = cookies?.customerId;
    prop.isLoading = store.getState().loader.isLoading;
    prop.pincodeDetails = store.getState().pincodeData?.pincodeData;
    prop.currentPage = store.getState().pincodeData?.currentPage?.currentPage;
    prop.doctor = store.getState().doctorReducer?.doctorList;
    prop.isLoading1 = store.getState().loader?.isLoading1;
    prop.SuggestMedicine =
      store.getState().suggestMed?.featuredMedicineList?.hits;
    prop.PastMedicineList = store.getState().suggestMed?.PastMedicineList;
    (prop.article = store.getState().articleHlDetails?.articleList),
      (prop.isLoading2 = store.getState().loader?.isLoading2);
  });

  return { ...prop };
});

let mapStateToProps = (state) => ({
  accessToken: state.loginReducer.verifyOtpSuccess?.Response?.access_token,
  orderTracker: state.myOrder,
  orderHistory: state.myOrder,
  timeout: state.myOrder?.orderTrackerError,
  loginStatus: state.loginReducer.isUserLogged,
  customerId: state.loginReducer.verifyOtpSuccess?.CustomerId,
  saving: state.savingsReducer.savingsData,
  isLoading: state.loader?.isLoading,
  loginDetails: state.loginReducer,
  pincodeDetails: state.pincodeData?.pincodeData,
  currentPage: state.pincodeData?.currentPage?.currentPage,
  bannerData: state.bannerImages?.BannerData,
  bannerLoading: state.bannerImages?.isLoading,
  SuggestMedicine: state.suggestMed?.featuredMedicineList?.hits,
  PastMedicine: state.suggestMed?.PastMedicineList,
});

export default withRouter(
  connect(mapStateToProps, {
    orderTrackerThunk,
    setCurrentPage,
    FeaturedMedThunk,
    changePaymentModeThunk,
    mySavingsThunk,
    allOrdersThunk,
    toUploadPrescriptionAction,
    getBannersThunk,
    doctorDetailThunk,
    fetchMedicineThunk,
    fetchArticleListDetails,
  })(Home)
);

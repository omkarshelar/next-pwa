import React, { Component } from "react";
import Router, { withRouter } from "next/router";
import { connect } from "react-redux";
// import OrderStatusShow from "./OrderStatusShow";
// import OrderStatusNew from "./OrderStatusNew";
// import window from "global";
import {
  fetchReasonThunk,
  getAllPatientsOrderDetails,
} from "../../redux/ConfirmOrder/Action";
import { wrapper } from "../../redux/store";
import { parseCookies } from "../../components/Helper/parseCookies";
import { orderStatusThunk } from "../../redux/OrderDetail/Action";
import {
  compareOrderDetailsThunk,
  fetchPatientByIdThunk,
} from "../../redux/PatientDetails/Action";
import { fetchOrderStatusThunk } from "../../redux/MyOrder/Action";
import { getDoctorRatingDetailsThunk } from "../../redux/DoctorRating/Action";
import OrderStatusNew from "../../components/WebComponents/WaitingScreen/OrderStatusNew";

export class WaitingScreen extends Component {
  componentDidMount() {
    // window.history.pushState(null, null, window.location.pathname);
    // window.addEventListener("popstate", this.onBackButtonEvent);
    if (this.props.accessToken && this.props.custId) {
      /*  this.props.orderStatusThunk({
        access_token: this.props.accessToken.Response.access_token,
        orderIds: this.props.match.params.id,
        customerId: this.props.custId,
        history: this.props.history,
      }); */
      this.props.fetchReasonThunk({
        access_token: this.props.accessToken,
        history: Router,
      });
    }
  }

  // Clear State after unmounting
  // componentWillUnmount = () => {
  //   window.removeEventListener("popstate", this.onBackButtonEvent);
  // };

  // On Back Button click Event.
  // onBackButtonEvent = (e) => {
  //   e.preventDefault();
  //   if (!this.isBackButtonClicked) {
  //     if (window && this.props.history?.location?.state?.detail) {
  //       this.isBackButtonClicked = true;
  //       Router.push("/myorders");
  //     } else if (window) {
  //       this.isBackButtonClicked = true;
  //       Router.push("/");
  //     } else {
  //       window.history.pushState(null, null, window.location.pathname);
  //       this.isBackButtonClicked = false;
  //     }
  //   }
  // };

  render() {
    return (
      <>
        <OrderStatusNew
          orderid={this.props.orderid}
          isLoading={this.props.isLoading}
          {...this.props}
        />
      </>
    );
  }
}

//server calls
WaitingScreen.getInitialProps = wrapper.getInitialPageProps(
  (store) => async (props) => {
    let orderid = props.query?.orderId;

    const cookies = parseCookies(props.req);

    const p1 = new Promise((resolve, reject) => {
      store
        .dispatch(
          fetchReasonThunk({ access_token: cookies?.token, history: Router })
        )
        .then(() => {
          resolve();
        });
    });

    const p2 = new Promise((resolve, reject) => {
      store
        .dispatch(
          orderStatusThunk({
            access_token: cookies?.token,
            orderIds: orderid,
            customerId: cookies?.customerId,
            history: Router,
          })
        )
        .then(() => {
          resolve();
        });
    });

    const p3 = new Promise((resolve, reject) => {
      store
        .dispatch(
          getAllPatientsOrderDetails({
            access_token: cookies?.token,
            orderid: orderid,
          })
        )
        .then(() => {
          resolve();
        });
    });

    const p4 = new Promise((resolve, reject) => {
      store
        .dispatch(
          fetchPatientByIdThunk({
            accessToken: cookies?.token,
            orderId: orderid,
            patientId: 0,
            history: Router,
          })
        )
        .then(() => {
          resolve();
        });
    });

    const p5 = new Promise((resolve, reject) => {
      store
        .dispatch(
          fetchOrderStatusThunk({
            accessToken: cookies?.token,
            orderId: orderid,
            history: Router,
          })
        )
        .then(() => {
          resolve();
        });
    });

    const p6 = new Promise((resolve, reject) => {
      store
        .dispatch(
          compareOrderDetailsThunk({
            access_token: cookies?.token,
            orderId: orderid,
            history: Router,
          })
        )
        .then(() => {
          resolve();
        });
    });

    const p7 = new Promise((resolve, reject) => {
      store
        .dispatch(
          getDoctorRatingDetailsThunk({
            access_token: cookies?.token,
            orderId: orderid,
            history: Router,
          })
        )
        .then(() => {
          resolve();
        });
    });

    let prop = {};
    await Promise.all([p1, p2, p3, p4, p5, p6, p7]).then(() => {
      prop.updateDetail = store.getState().orderStatusUpdate?.OrderStatusData;
      prop.list = store.getState().reasonList?.reasonList;
      prop.doctorRatingData = store.getState().doctorRatings;
      prop.compareOrderData =
        store.getState().patientData?.compareOrderDetailsSuccess;
      prop.orderStatus = store.getState().myOrder.orderStatusSuccess;
      prop.patientData = store.getState().patientData?.fetchPatientByIdSuccess;
      prop.patientOrderDetails = store.getState().patientOrderDetails;
      prop.orderid = orderid;
      prop.isLoading = false;
    });
    return { ...prop };
  }
);

let mapStateToProps = (state) => ({
  accessToken: state.loginReducer?.verifyOtpSuccess?.Response.access_token,
  isLoading: state.loader?.isLoading,
  custId: state.loginReducer?.verifyOtpSuccess?.CustomerDto?.customerId,
  orderStatusUpdate: state.orderStatusUpdate?.OrderStatusData,
  updateDetail: state.orderStatusUpdate?.OrderStatusData,
  list: state.reasonList?.reasonList,
  doctorRatingData: state.doctorRatings,
  compareOrderData: state.patientData?.compareOrderDetailsSuccess,
  orderStatus: state.myOrder.orderStatusSuccess,
  patientData: state.patientData?.fetchPatientByIdSuccess,
  patientOrderDetails: state.patientOrderDetails,
});

export default withRouter(
  connect(mapStateToProps, {
    fetchReasonThunk,
  })(WaitingScreen)
);

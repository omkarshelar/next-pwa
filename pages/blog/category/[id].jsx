import React from "react";
import { parseCookies } from "../../../components/Helper/parseCookies";
import CustomBlogsList from "../../../pageComponents/blog/CustomBlogsList";
import {
  fetchCategoryList,
  fetchCustomList,
} from "../../../redux/HeadlessHealth/Action";
import { wrapper } from "../../../redux/store";

class CategoryListPage extends React.Component {
  render() {
    return (
      <div>
        <CustomBlogsList
          article={this.props.article}
          count={this.props.count}
          queryId={this.props.queryId}
          type={"category"}
          isMobile={this.props.isMobile}
          category={this.props.category}
        />
      </div>
    );
  }
}

CategoryListPage.getInitialProps = wrapper.getInitialPageProps(
  (store) => async (props) => {
    const cookies = parseCookies(props.req);
    let accessToken = cookies.token;
    await store.dispatch(
      fetchCustomList("category", props.query.id, accessToken, 1)
    );
    let params = {};
    params.accessToken = cookies.token;
    await store.dispatch(fetchCategoryList(params));
    let prop = {};
    prop.queryId = props.query?.id;
    prop.article = store.getState().articleHlDetails?.articleCustomList;
    prop.count = store.getState().articleHlDetails?.articleCustomListCount;
    prop.articleError = store.getState().articleHlDetails?.articleError;
    prop.articleCustomListError =
      store.getState().articleHlDetails?.articleCustomListError;
    prop.category = store.getState().articleHlDetails?.categoryList;
    return { ...prop };
  }
);

export default CategoryListPage;

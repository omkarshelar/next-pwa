import React from "react";
import { connect } from "react-redux";
import Router, { withRouter } from "next/router";
import {
  HealthWrapper,
  Header,
  ArticleList,
  ArticleWrapper,
} from "../../../pageComponents/blog/HeadlessHealthBlogs.style";
import { PaginationWrap } from "../../../pageComponents/blog/Pagination.style";

import _ from "underscore";
import ArticleCard from "../../../pageComponents/blog/SubComponent/ArticleCard";
import ArticleCardShimmer from "../../../pageComponents/blog/SubComponent/ArticleCardShimmer";
import { setCurrentPage } from "../../../redux/Pincode/Actions";
import ArticleSideBarComponent from "../../../pageComponents/blog/SubComponent/ArticleSideBarComponent";
import { message } from "antd";
import ReactPaginate from "react-paginate";
import BlogError from "../../../pageComponents/blog/SubComponent/BlogError";
import { eventArticleViewed } from "../../../Events/Events";
import { wrapper } from "../../../redux/store";
import {
  fetchArticleListDetails,
  fetchCategoryList,
} from "../../../redux/HeadlessHealth/Action";
import { parseCookies } from "../../../components/Helper/parseCookies";
import window from "global";

export class SearchBlog extends React.Component {
  state = {
    option: 1,
    filterList: this.props.article,
    activeSlug: "",
    totalPostCount: this.props.article.length,
    searchString: this.props.searchString,
    isLoading: false,
  };
  fetchArticle = (props) => {
    this.props.fetchArticleListDetails(props).then(() => {
      if (this.props.articleListError) {
        if (
          this.props.articleListError &&
          this.props.articleListError.response &&
          this.props.articleListError.response.status === 504
        ) {
          message.error(
            "The page number requested is larger than the number of pages available."
          );
        } else {
          message.error("Error! Please try again in sometime");
        }
      } else {
        this.setState({
          filterList: this.props.article,
          isLoading: false,
        });
        if (this.props.article?.length < 10) {
          this.setState({
            totalPostCount: this.props.article.length,
          });
        }
      }
    });
  };
  componentDidMount = () => {
    let html = document.getElementsByTagName("html");
    if (html?.length > 0) {
      html[0].style.scrollBehavior = "smooth";
    }

    this.props.setCurrentPage({ currentPage: "search-article" });
    /* 
    this.fetchArticle({
      accessToken: this.props.accessToken,
      searchString: this.state.searchString,
    }); */
  };

  componentDidUpdate(prevProps, prevState) {}

  specificArticle = (data) => {
    Router.push(`/blog/${data.slug}`);
    // Event_Truemeds
    //new
    eventArticleViewed();
  };

  handlePageClick(e) {
    let props = {
      accessToken: this.props.accessToken,
      page: e.selected + 1,
    };
    if (this.state.searchString) {
      props.searchString = this.state.searchString;
    }
    window.scrollTo(0, 0);
    this.fetchArticle(props);
  }
  render() {
    let { article } = this.props;
    let { filterList, totalPostCount } = this.state;
    let catTemp = [];
    if (article?.length > 0) {
      article.map((e) => {
        catTemp.push({ id: e.categoryId, name: e.categoryName });
      });
      catTemp = _.uniq(catTemp, "id");
    }
    return (
      <>
        <HealthWrapper>
          <Header>
            <h5>Search</h5>
          </Header>
        </HealthWrapper>
        <ArticleWrapper>
          <ArticleList>
            {this.props.isMobile ? (
              <ArticleSideBarComponent
                searchString={this.state.searchString}
                handleSearchString={(e) => {
                  this.setState({ searchString: e });
                }}
                handleSearchEnter={() => {
                  this.setState({ isLoading: true });
                  this.fetchArticle({
                    accessToken: this.props.accessToken,
                    searchString: this.state.searchString,
                  });
                }}
                getTotalCount={(e) => this.setState({ totalPostCount: e })}
                category={this.props.category}
              />
            ) : (
              ""
            )}
            {this.props.isLoading || this.state.isLoading ? (
              <ArticleCardShimmer />
            ) : filterList.length > 0 ? (
              filterList.map((data, index) => (
                <ArticleCard
                  data={data}
                  specificArticle={this.specificArticle}
                  lastIndex={filterList.length !== index + 1}
                />
              ))
            ) : (
              <BlogError type={1} />
            )}
            {totalPostCount > 0 ? (
              <PaginationWrap>
                <ReactPaginate
                  breakLabel="..."
                  nextLabel=">"
                  onPageChange={(e) => {
                    this.handlePageClick(e);
                  }}
                  pageRangeDisplayed={3}
                  pageCount={
                    this.props.totalPages
                      ? this.props.totalPages
                      : Math.ceil(totalPostCount / 10)
                  }
                  previousLabel="<"
                  renderOnZeroPageCount={null}
                  containerClassName={"containerWrap"}
                  pageClassName="page"
                  pageLinkClassName="pageLink"
                  activeClassName="active"
                  activeLinkClassName="activeLink"
                  previousClassName="previous"
                  nextClassName="next"
                  previousLinkClassName="previousLink"
                  nextLinkClassName="nextLinkClassName"
                  disabledClassName="disabled"
                  disabledLinkClassName="disabledLink"
                />
              </PaginationWrap>
            ) : (
              ""
            )}
          </ArticleList>
          {!this.props.isMobile ? (
            <ArticleSideBarComponent
              searchString={this.state.searchString}
              handleSearchString={(e) => {
                this.setState({ searchString: e });
              }}
              handleSearchEnter={() => {
                this.setState({ isLoading: true });
                this.fetchArticle({
                  accessToken: this.props.accessToken,
                  searchString: this.state.searchString,
                });
              }}
              getTotalCount={(e) => this.setState({ totalPostCount: e })}
              category={this.props.category}
            />
          ) : (
            ""
          )}
        </ArticleWrapper>
      </>
    );
  }
}

SearchBlog.getInitialProps = wrapper.getInitialPageProps(
  (store) => async (props) => {
    const cookies = parseCookies(props.req);
    let accessToken = cookies.token;
    let params = {};
    params.accessToken = cookies.token;
    await store.dispatch(fetchCategoryList(params));
    params.searchString = props.query?.searchString;

    await store.dispatch(fetchArticleListDetails(params));

    let prop = {};
    prop.queryId = props.query?.searchString;
    prop.article = store.getState().articleHlDetails?.articleList;
    prop.articleListError = store.getState().articleHlDetails?.articleListError;
    prop.category = store.getState().articleHlDetails?.categoryList;
    prop.totalPages = store.getState().articleHlDetails?.totalPages;
    prop.totalPosts = store.getState().articleHlDetails?.totalPosts;
    return { ...prop };
  }
);

let mapStateToProps = (state) => ({
  accessToken: state.loginReducer.verifyOtpSuccess?.Response?.access_token,
  article: state.articleHlDetails?.articleList,
  category: state.articleHlDetails?.categoryList,
  articleListError: state.articleHlDetails?.articleListError,
  isLoading: state.articleHlDetails?.isLoading,
  totalPages: state.articleHlDetails?.totalPages,
  totalPosts: state.articleHlDetails?.totalPosts,
});

export default withRouter(
  connect(mapStateToProps, { fetchArticleListDetails, setCurrentPage })(
    SearchBlog
  )
);

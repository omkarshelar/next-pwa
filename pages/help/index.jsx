import React, { Component } from "react";
import { connect } from "react-redux";
import Router, { withRouter } from "next/router";
import {
  HelpContainer,
  EnquiryWrapper,
  TopicWrapper,
  HeaderHelp,
} from "../../pageComponents/help/Help.style";
import RightArrow from "../../src/Assets/RightArrow.png";
// import Footer from "../../Components/Footer/Footer";
import { helpCategoryThunk } from "../../redux/Help/action";
import { setCurrentPage } from "../../redux/Pincode/Actions";
export class Help extends Component {
  componentDidMount() {
    this.props.setCurrentPage({ currentPage: "help" });
    this.props.helpCategoryThunk(Router);
  }

  convertTitle = (name, id) => {
    return (
      name
        .replace(/[&\/\\#,+()$~%.'":*?<>{}]/g, "")
        .toLowerCase()
        .trimEnd()
        .split(" ")
        .join("-")
        .replace("--", "-") +
      "--" +
      id
    );
  };

  goToIssue = (id, name) => {
    Router.push(`/help/${this.convertTitle(name, id)}`);
  };

  goToMyOrder = () => {
    Router.push(`/myorders`);
  };

  render() {
    return (
      <>
        <HeaderHelp> Select a Topic</HeaderHelp>
        <HelpContainer>
          <TopicWrapper>
            {this.props.helpCategories &&
              this.props.helpCategories.Category.map((data) => (
                <div
                  key={data.categoryId}
                  onClick={() =>
                    this.goToIssue(data.categoryId, data.categoryName)
                  }
                >
                  <span>{data.categoryName} </span>
                  <div>
                    <img src={RightArrow} alt="Go"></img>
                  </div>
                </div>
              ))}
          </TopicWrapper>

          <EnquiryWrapper>
            <span>For Enquiries Call</span>
            <div className="info-enquiry">
              <span>
                <a
                  style={{
                    color: "#179b62",
                    textDecoration: "none",
                    fontWeight: 600,
                    // fontFamily: "Century Gothic",
                  }}
                  href="tel:022-48977965"
                >
                  022-48977965
                </a>
              </span>
              <span>(Between 9am-9pm)</span>
            </div>
          </EnquiryWrapper>
        </HelpContainer>
      </>
    );
  }
}
let mapStateToProps = (state) => ({
  helpCategories: state.help.HelpCategoryData,
  pastOrderAvailable: state.myOrder.msg,
  accessToken: state.loginReducer?.verifyOtpSuccess,
});

export default withRouter(
  connect(mapStateToProps, { helpCategoryThunk, setCurrentPage })(Help)
);

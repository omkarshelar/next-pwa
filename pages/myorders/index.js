import React, { Component } from "react";
import "../../pageComponents/myorders/MyOrders.css";
import { connect } from "react-redux";
import { withRouter } from "next/router";
import OrderCard from "../../pageComponents/myorders/Components/OrderCard";
import { allOrdersThunk } from "../../redux/MyOrder/Action";
import { message } from "antd";
// import Lottie from "react-lottie";
import animationData from "../../src/Assets/loaderSearchPage.json";
import Select from "react-select";
import UploadCard from "../../components/WebComponents/UploadCard/UploadCard";
import {
  toCartSectionAction,
  toUploadPrescriptionAction,
} from "../../redux/Timeline/Action";
import { fetchpatientThunk } from "../../redux/PatientDetails/Action";
import { reorderThunk } from "../../redux/Reorder/Action";
import { AddIncompleteOrderMedsAction } from "../../redux/Cart/Action";
import { orderStatusThunk } from "../../redux/OrderDetail/Action";
import selectPatient from "../../src/Assets/select-patient.svg";
import selectPatientMob from "../../src/Assets/select-patient-mob.svg";
import noOrders from "../../src/Assets/no-orders.svg";
import Button from "../../components/WebComponents/Button/Button";
import AndroidSeperator from "../../components/WebComponents/AndroidSeperator/AndroidSeperator";
import MyOrdersCardShimmer from "../../components/WebComponents/Shimmer/MyOrdersCardShimmer";
import window from "global";
import { wrapper } from "../../redux/store";
import { parseCookies } from "../../components/Helper/parseCookies";
import { getOrderStatusDetailsDataAction } from "../../redux/storeData/Action";
import dynamic from "next/dynamic";

//dynamic imports
const Lottie = dynamic(() => import("react-lottie"));
const defaultOptions = {
  loop: true,
  autoPlay: true,
  animationData: animationData,
  rendererSettings: {
    preserveAspectRatio: "xMidYMid slice",
  },
};

const orderFilters = [
  { label: "Order Delivered", value: 3 },
  { label: "Order Active", value: 2 },
  { label: "Order Cancelled", value: 4 },
  { label: "All Orders", value: 1 },
];

export class MyOrders extends Component {
  state = {
    filteredList: [],
    type: "",
    allOrders: [],
    allPatients: [],
    orderType: { label: "All Orders", value: 1 },
    myOrdersLoader: false,
    isLoading: false,
  };

  storeUpdateTask = () => {
    if (this.props.storeData?.orderStatusDetailsData?.isMyOrder) {
      this.setState({
        orderType: this.props.storeData?.orderStatusDetailsData?.orderType,
      });
    }
    if (!this.props.storeData?.orderStatusDetailsData) {
      this.setState({
        orderType: { label: "All Orders", value: 1 },
      });
    }
    this.setState({ myOrdersLoader: true });
    this.props
      .allOrdersThunk({
        accessToken: this.props.accessToken,
        history: this.props.router,
      })
      .then(() => {
        this.setState({ myOrdersLoader: false });
        if (this.props.orderHistory.allOrdersError) {
          message.error(this.props.orderHistory.allOrdersError);
        } else {
          if (
            this.props.orderHistory?.allOrdersSuccess?.currentOrder &&
            this.props.orderHistory?.allOrdersSuccess?.pastOrder
          ) {
            if (this.props.storeData?.orderStatusDetailsData?.isMyOrder) {
              this.setState(
                {
                  allOrders: this.filterOrders(
                    this.props.orderHistory.allOrdersSuccess,
                    this.props.storeData?.orderStatusDetailsData?.orderType
                      ?.value
                  ),
                  allPatients: this.addPatientDetailsWithFilterFromOrder(
                    [
                      ...this.props.orderHistory.allOrdersSuccess.currentOrder,
                      ...this.props.orderHistory.allOrdersSuccess.pastOrder,
                    ],
                    this.props.storeData?.orderStatusDetailsData?.patientFilter
                  ),
                },
                () => {
                  if (
                    this.props.storeData?.orderStatusDetailsData?.patientFilter
                      ?.length > 0
                  ) {
                    this.setState({
                      allOrders: this.filterByPatient(
                        this.state.allOrders,
                        this.props.storeData?.orderStatusDetailsData
                          ?.patientFilter
                      ),
                    });
                  }
                }
              );
            } else {
              this.setState({
                allOrders: [
                  ...this.props.orderHistory.allOrdersSuccess.currentOrder,
                  ...this.props.orderHistory.allOrdersSuccess.pastOrder,
                ],
                allPatients: this.addPatientDetailsFromOrder([
                  ...this.props.orderHistory.allOrdersSuccess.currentOrder,
                  ...this.props.orderHistory.allOrdersSuccess.pastOrder,
                ]),
              });
            }
          }
        }
      });
  };

  componentDidMount() {
    this.storeUpdateTask();

    // window.history.pushState(null, null, window.location.pathname);
    // window.addEventListener("popstate", this.onBackButtonEvent);
  }

  // Clear State after unmounting
  // componentWillUnmount = () => {
  //   window.removeEventListener("popstate", this.onBackButtonEvent);
  // };

  // On Back Button click Event.
  // onBackButtonEvent = (e) => {
  //   e.preventDefault();
  //   if (!this.isBackButtonClicked) {
  //     this.props.router.push("/");
  //     //   window.history.pushState(null, null, Router.push("/"));
  //     this.isBackButtonClicked = false;
  //   }
  // };

  componentDidUpdate(prevProps) {
    if (
      this.props.patientData?.patientFilter !==
      prevProps.patientData?.patientFilter
    ) {
      this.setState({ myOrdersLoader: true }, () => {
        this.applyFilter();
      });
    }

    if (this.props.storeData !== prevProps.storeData) {
      this.storeUpdateTask();
    }
  }

  addPatientDetailsFromOrder = (arr) => {
    let finalArr = [];
    for (let i in arr) {
      let patient = {};
      let order = arr[i];
      let found = finalArr.some((j) => j.patientId === order.patientId);
      if (!found) {
        patient.patientId = order.patientId;
        patient.patientName = order.patientName;
        patient.checked = false;
        finalArr.push(patient);
      }
    }
    return finalArr;
  };

  addPatientDetailsWithFilterFromOrder = (arr, ids) => {
    let finalArr = [];
    for (let i in arr) {
      let patient = {};
      let order = arr[i];
      let found = finalArr.some((j) => j.patientId === order.patientId);
      if (!found) {
        patient.patientId = order.patientId;
        patient.patientName = order.patientName;
        for (let k in ids) {
          let id = ids[k];
          if (id === patient.patientId) {
            patient.checked = true;
            break;
          } else {
            patient.checked = false;
          }
        }
        finalArr.push(patient);
      }
    }
    return finalArr;
  };

  filterOrders = (allOrders, val) => {
    let finalArr = [];
    if (allOrders) {
      if (val === 1) {
        finalArr = [...allOrders.currentOrder, ...allOrders.pastOrder];
      } else if (val === 2) {
        finalArr = [...allOrders.currentOrder];
      } else if (val === 3) {
        for (let i in allOrders.pastOrder) {
          let order = allOrders.pastOrder[i];
          if (
            [55, 190, 191, 192, 199, 201, 56, 200, 121, 124].includes(
              order.statusId
            )
          ) {
            finalArr.push(order);
          }
        }
      } else if (val === 4) {
        for (let i in allOrders.pastOrder) {
          let order = allOrders.pastOrder[i];
          if (order.statusId === 57) {
            finalArr.push(order);
          }
        }
      }
    }
    return finalArr;
  };

  filterByPatient = (allOrders, ids) => {
    let finalArr = [];
    if (allOrders?.length > 0 && ids?.length > 0) {
      for (let i in allOrders) {
        let order = allOrders[i];
        for (let j in ids) {
          let id = ids[j];
          if (Number(id) === Number(order.patientId)) {
            finalArr.push(order);
          }
        }
      }
    }
    return finalArr;
  };

  applyFilter = () => {
    let ordersByType = [];
    let ordersByPatient = [];
    ordersByType = this.filterOrders(
      this.props.orderHistory.allOrdersSuccess,
      this.state.orderType?.value
    );
    if (this.props.patientData?.patientFilter?.length > 0) {
      ordersByPatient = this.filterByPatient(
        ordersByType,
        this.props.patientData?.patientFilter
      );
    } else {
      ordersByPatient = ordersByType;
    }
    this.setState({
      allOrders: ordersByPatient,
      myOrdersLoader: false,
    });
    window.scrollTo(0, 0);
  };

  onChangeOrderFilter = (e) => {
    this.setState({ myOrdersLoader: true, orderType: e }, () => {
      this.applyFilter();
    });
  };

  setLoader = (value) => {
    this.setState({ isLoading: value });
  };

  reorderHandler = (e, orderId, patientId) => {
    e.stopPropagation();
    this.setState({ buttonDisabled: true });
    this.setLoader(true);
    this.props
      .reorderThunk({
        customerId: this.props.custId,
        orderId: orderId,
        accessToken: this.props.accessToken,
        patientIdSet: [patientId],
        history: this.props.router,
      })
      .then(() => {
        if (this.props.reorder.reorderError) {
          this.setState({ buttonDisabled: false });
          this.setLoader(false);
          message.success(this.props.reorder.reorderError);
        } else {
          this.props
            .orderStatusThunk({
              access_token: this.props.accessToken,
              orderIds: orderId,
              customerId: this.props.custId,
              history: this.props.router,
            })
            .then(() => {
              this.setState({ buttonDisabled: false });
              this.setLoader(false);
              if (this.props.orderStatus.error) {
                message.error(this.props.orderStatus.error);
              } else if (
                this.props.orderStatus?.OrderStatusData?.productSubsMappingList
                  ?.length > 0
              ) {
                this.props.AddIncompleteOrderMedsAction(
                  this.props.orderStatus?.OrderStatusData
                    ?.productSubsMappingList
                );
                this.props.toCartSectionAction();
                this.props
                  .getOrderStatusDetailsDataAction({
                    isReorder: true,
                    isMyOrder: true,
                    orderType: this.state.orderType,
                    patientFilter: this.props.patientData?.patientFilter,
                  })
                  .then(() => {
                    this.props.router.push("/orderflow");
                  });
                // this.props.router.push(
                //   {
                //     pathname: "/orderflow",
                //     query: {
                //       isReorder: true,
                //       isMyOrder: true,
                //       orderType: this.state.orderType,
                //       patientFilter: this.props.patientData?.patientFilter,
                //     },
                //   },
                //   "/orderflow"
                // );
              }
            });
        }
      });
  };

  checkForSelectedPatient = (arr) => {
    let checked = false;
    for (let i in arr) {
      let patient = arr[i];
      if (patient.checked) {
        checked = true;
        break;
      }
    }
    return checked;
  };

  render() {
    return (
      <>
        {this.state.isLoading ? (
          <MyOrdersCardShimmer />
        ) : (
          <div className="myOrdersMainContainer">
            <div className="myOrdersHeader">
              <h1>My Orders</h1>
              <div className="myOrderFilters">
                {(this.props.orderHistory?.allOrdersSuccess?.currentOrder
                  ?.length > 0 ||
                  this.props.orderHistory?.allOrdersSuccess?.pastOrder?.length >
                    0) &&
                this.state.allPatients?.length > 1 ? (
                  <div
                    style={{
                      pointerEvents:
                        this.state.allPatients?.length > 0 &&
                        !this.state.myOrdersLoader
                          ? ""
                          : "none",
                    }}
                    onClick={() => {
                      window.openSideBar(true, 13, {
                        patientList: this.state.allPatients,
                        patientFilter:
                          this.props.storeData?.orderStatusDetailsData
                            ?.patientFilter,
                      });
                    }}
                    className="myOrderSelectPatient"
                  >
                    <img
                      className="deskImg"
                      src={selectPatient}
                      alt="patient"
                    />
                    <img
                      className="mobImg"
                      src={selectPatientMob}
                      alt="patient"
                    />
                    <span>
                      {this.checkForSelectedPatient(this.state.allPatients)
                        ? "Patient Selected"
                        : "Select Patient"}
                    </span>
                  </div>
                ) : null}
                <div
                  style={{
                    pointerEvents: !this.state.myOrdersLoader ? "" : "none",
                  }}
                  className="myOrderSelectType"
                >
                  <Select
                    className="myOrderTypeFilter"
                    classNamePrefix="myOrderTypePrefix"
                    placeholder="Select Order Type"
                    value={this.state.orderType}
                    onChange={this.onChangeOrderFilter}
                    options={orderFilters}
                    isSearchable={false}
                    onMenuOpen={() => {
                      document.getElementsByClassName(
                        "myOrderTypePrefix__indicator"
                      )[0].children[0].style.transform = "rotate(180deg)";
                      document.getElementsByClassName(
                        "myOrderTypePrefix__control"
                      )[0].style.cssText +=
                        "border-radius: 4px 4px 0px 0px !important";
                    }}
                    onMenuClose={() => {
                      document.getElementsByClassName(
                        "myOrderTypePrefix__indicator"
                      )[0].children[0].style.transform = "rotate(0deg)";
                      document.getElementsByClassName(
                        "myOrderTypePrefix__control"
                      )[0].style.cssText += "border-radius: 4px !important";
                    }}
                  />
                </div>
              </div>
            </div>
            {this.state.myOrdersLoader ? (
              <div className="myOrderLoaderContainer">
                <Lottie
                  options={defaultOptions}
                  width={window.innerWidth > 768 ? 280 : 150}
                  height={window.innerWidth > 768 ? 194 : 150}
                />
                <p>Searching...</p>
              </div>
            ) : this.state.allOrders?.length > 0 ? (
              <div className="myOrdersDetailsContainer">
                {this.state.allOrders.map((order, index) => (
                  <>
                    <OrderCard
                      orderDetails={order}
                      router={this.props.router}
                      setLoader={this.setLoader}
                      reorderHandler={this.reorderHandler}
                      orderType={this.state.orderType}
                      patientFilter={this.props.patientData?.patientFilter}
                    />
                    {index + 1 === this.state.allOrders.length ? null : (
                      <AndroidSeperator showMob={true} colorCustom="#edf2f9" />
                    )}
                  </>
                ))}
              </div>
            ) : (
              <div className="myOrderEmpty">
                <div className="myOrderEmptyFirst">
                  <img src={noOrders} alt="no-order" />
                  <span>No Orders yet. Start Ordering!</span>
                  <Button
                    id="addMedBtn"
                    className="myOrderAddMedBtn"
                    upload
                    onClick={() => {
                      this.props.router.push("/?search=true");
                    }}
                  >
                    Add Medicine
                  </Button>
                </div>
                <div className="myOrderEmptySecond">
                  <UploadCard
                    onClickUpload={() => {
                      this.props.toUploadPrescriptionAction();
                      this.props
                        .getOrderStatusDetailsDataAction({
                          isReorder: true,
                        })
                        .then(() => {
                          this.props.router.push("/orderflow");
                        });
                      // this.props.router.push({
                      //   pathname: "/orderflow",
                      //   query: { isReorder: true },
                      // });
                    }}
                  />
                </div>
              </div>
            )}
          </div>
        )}
      </>
    );
  }
}

MyOrders.getInitialProps = wrapper.getInitialPageProps(
  (store) => async (props) => {
    const cookies = parseCookies(props.req);
    let prop = {};
    prop.accessToken = cookies.token;
    return { ...prop };
  }
);

let mapStateToProps = (state) => ({
  orderHistory: state.myOrder,
  orderTracker: state.trackOrder.TrackOrderData,
  patientData: state.patientData,
  orderStatus: state.orderStatusUpdate,
  custId: state.loginReducer?.verifyOtpSuccess?.CustomerDto?.customerId,
  reorder: state.reorderReducer,
  storeData: state.storeData,
});

export default withRouter(
  connect(mapStateToProps, {
    allOrdersThunk,
    toUploadPrescriptionAction,
    fetchpatientThunk,
    reorderThunk,
    AddIncompleteOrderMedsAction,
    toCartSectionAction,
    orderStatusThunk,
    getOrderStatusDetailsDataAction,
  })(MyOrders)
);

import { getServerSideSitemapIndex } from "next-sitemap";
import React from "react";
// import { GetServerSideProps } from "next";

wrapper.getInitialPageProps((store) => async (props) => {
  return getServerSideSitemapIndex(props, [
    "https://example.com/path-1.xml",
    "https://example.com/path-2.xml",
  ]);
});
/* 
SitemapIndex.getInitialProps = wrapper.getInitialPageProps(
  (store) => async (props) => {
    return getServerSideSitemapIndex(props, [
      "https://example.com/path-1.xml",
      "https://example.com/path-2.xml",
    ]);
  }
); */

// Default export to prevent next.js errors
export default function SitemapIndex() {}

import React, { Component } from "react";
import Router, { withRouter } from "next/router";
import { connect } from "react-redux";
import "../../pageComponents/empty-cart/EmptyCart.css";
import emptyCartIcon from "../../src/Assets/empty-cart.svg";

import UploadCard from "../../components/WebComponents/UploadCard/UploadCard";
import Button from "../../components/WebComponents/Button/Button";
import {
  toCartSectionAction,
  toUploadPrescriptionAction,
} from "../../redux/Timeline/Action";

export class EmptyCart extends Component {
  render() {
    if (this.props.cartContent.length > 0) {
      this.props.toCartSectionAction();
      Router.push("/orderflow");
    }
    return (
      <div>
        <div className="empty-cart-page-container">
          <div className="empty-cart-page-left">
            <p>Your cart is empty</p>
            <div>
              <img src={emptyCartIcon} alt="empty-cart" />
              <Button
                id="addMedBtn"
                className="addMedBtn"
                upload
                onClick={() => {
                  // window.location.href = "/?search=true";
                  Router.push("/?search=true");
                }}
              >
                Add Medicine
              </Button>
            </div>
          </div>
          <div className="empty-cart-page-right">
            <UploadCard
              onClickUpload={() => {
                this.props.toUploadPrescriptionAction();
                Router.push("/orderflow");
              }}
            />
          </div>
        </div>
        {/* {window.innerWidth > 768 ? <Footer /> : ""} */}
      </div>
    );
  }
}

const mapStateToProps = (state) => ({
  cartContent: state.cartData?.cartItems,
});

export default withRouter(
  connect(mapStateToProps, { toCartSectionAction, toUploadPrescriptionAction })(
    EmptyCart
  )
);

import { Button, Divider, Input, message, Modal, Result, Space } from "antd";
import React, { Component } from "react";
import WalletItemLoaderCard from "../../../pageComponents/wallet/SubComponentNew/WalletItemLoader/WalletItemLoaderCard";
import walltImg from "../../../pageComponents/wallet/SubComponentNew/Images/WalletImg_alt.png";
import {
  SectionTitle,
  WalletWrapper,
} from "../../../pageComponents/wallet/TMWallet.style";
import {
  FirstRow,
  HR,
  RewardTransactionWrapper,
  SecondRow,
  ThirdRow,
  TransactionBox,
  TrasactionItem,
} from "../../../pageComponents/wallet/CreditTransaction.styled";
import { IoCaretBackSharp } from "react-icons/io5";
import { Container } from "@material-ui/core";
import { connect } from "react-redux";
import { wrapper } from "../../../redux/store";
import { parseCookies } from "../../../components/Helper/parseCookies";
import {
  walletHistoryThunk,
  walletThunk,
} from "../../../redux/TMWallet/action";
import { setCurrentPage } from "../../../redux/Pincode/Actions";
import { getFaqCategory, getFaq } from "../../../redux/FAQCall/action";
import filterTransactions from "../../../components/Helper/TMWalletFunction";
import Router, { withRouter } from "next/router";
import { DatePicker, Select } from "antd";
import moment from "moment";

import DividerImg from "../../../pageComponents/wallet/SubComponentNew/Images/separator.svg";
import EmptyTransactionImg from "../../../src/Assets/EmptyTMWallet.svg";
import { FAQSidebar } from "../../../components/WebComponents/FAQSidebar/FAQSidebar";

const { Option } = Select;
const { RangePicker } = DatePicker;

export class CreditTransaction extends Component {
  state = {
    option: 1,
    transactionData: [],
    filterTransactionData: null,
    filterDataShow: false,
    loading: true,
    faqCategory: null,
    faqData: null,
    customModal: false,
    dateRange: null,
    customDateSelected: false,
    dateFrom: null,
    dateTo: null,
  };
  kFormatter = (num) => {
    return Math.abs(num) > 999
      ? Math.sign(num) * (Math.abs(num) / 1000).toFixed(1) + "k"
      : Math.sign(num) * Math.abs(num);
  };

  componentDidMount = () => {
    if (this.props.wallet.tmCashError) {
      message.error("Something went wrong.");
    }
    if (this.props.TransactionError) {
      message.error("Something went wrong.");
    } else {
      this.filterTransactionHistory();
    }

    if (this.props.faqData?.errorMessage) {
      message.error("Something went wrong.");
    }
  };

  //? Sorting Transaction Data functions

  filterSpecificStatusTransaction = (array, status) => {
    let filterFunc = array.filter((data) => data.status === status);
    return filterFunc;
  };
  groupElementofSimilarOrderId = (objectArray, property) => {
    return objectArray.reduce((acc, obj) => {
      const key = obj[property];
      if (!acc[key]) {
        acc[key] = [];
      }
      acc[key].push(obj);
      return acc;
    }, {});
  };

  filterSubElementBasedOnDate = (result) => {
    let tempStorage = [];
    for (const iterator in result) {
      if (result[iterator].length > 1) {
        let a = result[iterator].reduce((a, b) => {
          return new Date(a.transactionDate) > new Date(b.transactionDate)
            ? a
            : b;
        });
        tempStorage.push(a);
      } else {
        tempStorage.push(result[iterator][0]);
      }
    }
    return tempStorage;
  };

  filterFinalArrayBasedOnDate = (array) => {
    let sortedArray = array.sort(
      (a, b) => new Date(b.transactionDate) - new Date(a.transactionDate)
    );
    return sortedArray;
  };

  filterTransactionHistory = () => {
    let { TransactionList } = this.props;
    if (TransactionList?.payload?.ledger?.length > 0) {
      this.setState(
        {
          transactionData: filterTransactions(
            TransactionList.payload.ledger,
            "TM_CREDIT"
          ),
          loading: false,
        },
        () => {
          this.filterTransaction("recent");
        }
      );
    }
  };

  filterTransaction = (value) => {
    if (value == "weekFilter") {
      this.setState({ filterDataShow: true, customDateSelected: false });

      // const weekvalue = new Date().valueOf() - 604800000;

      const weekValue = moment().day(-7).valueOf();

      this.setState({
        filterTransactionData: this.state.transactionData.filter(
          (item) => item.transDate > weekValue
        ),
      });
    } else if (value == "lastMonth") {
      this.setState({ filterDataShow: true, customDateSelected: false });

      // var date = new Date();
      // var firstDay = new Date(date.getFullYear(), date.getMonth() - 1, 1);
      // var lastDay = new Date(date.getFullYear(), date.getMonth() - 1 + 1, 0);

      var lastDay = moment()
        .subtract(1, "months")
        .endOf("month")
        .format("DD-MM-YYYY H:mm:s");

      var firstDay = moment()
        .subtract(1, "months")
        .startOf("month")
        .format("DD-MM-YYYY H:mm:s");

      this.setState({
        filterTransactionData: this.state.transactionData.filter(
          (item) =>
            item.transDate > moment(firstDay, "DD-MM-YYYY H:mm:s").valueOf() &&
            item.transDate < moment(lastDay, "DD-MM-YYYY H:mm:s").valueOf()
        ),
      });
    } else if (value == "currentMonth") {
      this.setState({ filterDataShow: true, customDateSelected: false });

      var lastDay = moment().endOf("month").format("DD-MM-YYYY H:mm:s");

      var firstDay = moment().startOf("month").format("DD-MM-YYYY H:mm:s");

      this.setState({
        filterTransactionData: this.state.transactionData.filter(
          (item) =>
            item.transDate > moment(firstDay, "DD-MM-YYYY H:mm:s").valueOf() &&
            item.transDate < moment(lastDay, "DD-MM-YYYY H:mm:s").valueOf()
        ),
      });
    } else if (value == "custom") {
      // this.setState({ filterDataShow: true });

      this.setState({ customModal: true });
    } else if (value == "recent") {
      this.setState({ filterDataShow: true, customDateSelected: false });

      this.setState({
        filterTransactionData: this.state.transactionData.slice(0, 20),
      });
    }
  };

  customFilter = () => {
    this.setState({ customDateSelected: false });

    if (this.state.dateFrom && this.state.dateTo) {
      this.setState({ filterDataShow: true });
      // var firstDate = new Date(`${this.state.dateFrom}, 12:00:00 AM`)
      //   .getTime()
      //   .valueOf();

      var firstDate = moment(
        `${this.state.dateFrom}, 00:00:00 `,
        "DD-MM-YYYY H:mm:s"
      )
        .startOf(`${this.state.dateFrom} , 00:00:00`, "DD-MM-YYYY H:mm:s")
        .valueOf();

      var lastDate = moment(
        `${this.state.dateTo}, 23:59:59`,
        "DD-MM-YYYY H:mm:s"
      )
        .endOf(`${this.state.dateTo} , 23:59:59`, "DD-MM-YYYY H:mm:s")
        .valueOf();

      // var lastDate = new Date(`${this.state.dateTo}, 11:59:59 PM`)
      //   .getTime()
      //   .valueOf();

      // .format("D");

      this.setState(
        {
          filterTransactionData: this.state.transactionData.filter(
            (item) => item.transDate > firstDate && item.transDate < lastDate
          ),
          customModal: false,
        },
        () => {
          message.success("Transaction List Updated");
          this.setState({ customDateSelected: true });
        }
      );
    } else {
      message.error("Please select valid dates");
    }
  };

  render() {
    let Info = this.props.wallet.tmCash;

    return (
      <Container>
        <WalletWrapper>
          <RewardTransactionWrapper>
            {Info ? (
              <div className="topWalletSection">
                <div className="walletBox">
                  <div className="walletText">
                    <h5>TM Credit balance</h5>

                    <h1>₹{Number(Info.payload.truemedsCredit).toFixed(2)}</h1>
                  </div>

                  <div className="walletBoxImg">
                    <img src={walltImg} alt="wallet_img" />
                  </div>
                </div>
                <Divider
                  style={{ borderTop: "1px solid rgba(255, 255, 255, 0.25)" }}
                />

                <p>
                  TM credit is only added after a successful refund. Tm credit
                  amount will be deducted after order confirmation.
                </p>
              </div>
            ) : (
              ""
            )}

            <TransactionBox
              className={`${
                this.state.transactionData &&
                this.state.transactionData.length > 7
                  ? "scrollable"
                  : ""
              }`}
            >
              <div className="TransactionFlexRow">
                <SectionTitle>Transaction Log</SectionTitle>
                <Select
                  placeholder="Filter"
                  onSelect={(value) => this.filterTransaction(value)}
                  style={{ width: 120 }}
                  disabled={
                    this.state.transactionData &&
                    this.state.transactionData.length == 0
                      ? true
                      : false
                  }
                  defaultValue="recent"
                >
                  <Option value="recent">Recent</Option>
                  <Option value="weekFilter">Last 7 days</Option>
                  <Option value="currentMonth">This Month</Option>
                  <Option value="lastMonth">Last Month</Option>
                  <Option value="custom">Custom </Option>
                </Select>
              </div>

              {this.state.customDateSelected &&
                this.state.dateTo &&
                this.state.dateFrom && (
                  <div className="flexDiv">
                    <p>
                      From :{" "}
                      <Button
                        type="link"
                        onClick={() => this.setState({ customModal: true })}
                        style={{
                          fontWeight: 700,
                          fontSize: "medium",
                          paddingLeft: 0,
                        }}
                      >
                        {this.state.dateFrom}
                      </Button>
                      To :{" "}
                      <Button
                        type="link"
                        onClick={() => this.setState({ customModal: true })}
                        style={{
                          fontWeight: 700,
                          fontSize: "medium",
                          paddingLeft: 0,
                        }}
                      >
                        {this.state.dateTo}
                      </Button>
                    </p>
                  </div>
                )}

              {this.state.loading ? (
                <WalletItemLoaderCard />
              ) : !this.state.filterDataShow &&
                this.state.transactionData &&
                this.state.transactionData.length > 0 ? (
                this.state.transactionData?.map((data, index) => (
                  <>
                    <TrasactionItem>
                      <FirstRow>
                        <p>
                          {data.transaction.includes("|")
                            ? data.transaction.split("|")[0]
                            : data.transaction}
                        </p>
                        {!data.transaction.includes("|") ? (
                          <p className="transaction-info">
                            Order ID :{data.orderId}
                          </p>
                        ) : (
                          <p className="transaction-info">
                            {data.transaction.split("|")[1]}
                          </p>
                        )}
                      </FirstRow>
                      <SecondRow>
                        <p
                          className={
                            data?.spent === 0 ? "earned-text" : "spent-text"
                          }
                        >
                          ₹
                          {data.spent === 0
                            ? data?.earned.toFixed(2)
                            : data?.spent.toFixed(2)}
                        </p>
                        <p className="transaction-date">
                          {data.transactionDate.slice(0, 12)}
                        </p>
                      </SecondRow>
                    </TrasactionItem>
                    {data.length - 1 === index ? null : <HR />}
                  </>
                ))
              ) : this.state.filterDataShow &&
                this.state.filterTransactionData.length > 0 ? (
                this.state.filterTransactionData.map((data, index) => (
                  <>
                    <TrasactionItem key={index}>
                      <FirstRow>
                        <p>
                          {data.transaction.includes("|")
                            ? data.transaction.split("|")[0]
                            : data.transaction}
                        </p>
                        {!data.transaction.includes("|") ? (
                          <p className="transaction-info">
                            Order ID :{data.orderId}
                          </p>
                        ) : (
                          <p className="transaction-info">
                            {data.transaction.split("|")[1]}
                          </p>
                        )}
                      </FirstRow>
                      <SecondRow>
                        <p
                          className={
                            data?.spent === 0 ? "earned-text" : "spent-text"
                          }
                        >
                          ₹
                          {data.spent === 0
                            ? data?.earned.toFixed(2)
                            : data?.spent.toFixed(2)}
                        </p>
                        <p className="transaction-date">
                          {data.transactionDate.slice(0, 12)}
                        </p>
                      </SecondRow>
                    </TrasactionItem>
                    {data.length - 1 === index ? null : <HR />}
                  </>
                ))
              ) : (
                <Result
                  icon={<img src={EmptyTransactionImg} alt="emptyImg" />}
                  subTitle="No records found"
                  style={{ fontWeight: 600 }}
                />
              )}
            </TransactionBox>
          </RewardTransactionWrapper>
          <img
            src={DividerImg}
            alt="Divider"
            className="TMFAQSeparator"
            style={{ marginTop: "1em" }}
          />

          <FAQSidebar
            tmwalletCondition
            history={Router}
            faqData={this.props.faqData.FAQData.payload}
            loading={this.props.faqData.FAQData.isLoading}
          />
        </WalletWrapper>

        <Modal
          title="Custom Dates"
          visible={this.state.customModal}
          //  onOk={handleOk}
          onCancel={() => this.setState({ customModal: false })}
          footer={null}
          className="customDateModal"
        >
          <div className="customDateModalWrapper">
            <Input.Group
              style={{ display: "flex", flexFlow: "column", gap: "0.25em" }}
            >
              <label style={{ fontWeight: 700 }}>From</label>
              <DatePicker
                format="DD.MM.YY"
                onChange={(dates, dateStrings) =>
                  this.setState({
                    //   dates: dates,
                    dateFrom: dateStrings,
                  })
                }
                disabledDate={(current) => {
                  return current && this.state.dateTo
                    ? current > moment(this.state.dateTo, "DD.MM.YY")
                    : "";
                }}
              />
            </Input.Group>

            <Input.Group>
              <label>To</label>
              <DatePicker
                format="DD.MM.YY"
                onChange={(dates, dateStrings) =>
                  this.setState({
                    //   dates: dates,
                    dateTo: dateStrings,
                  })
                }
                disabledDate={(current) => {
                  return current && this.state.dateFrom
                    ? current < moment(this.state.dateFrom, "DD.MM.YY")
                    : "";
                }}
              />
            </Input.Group>

            <Button
              type="primary"
              className="bbtn"
              style={{ background: "#0071bc", width: "100%" }}
              onClick={this.customFilter}
            >
              Submit
            </Button>
          </div>
          {/* <Space
            direction="vertical"
            size={24}
            align="center"
            style={{ width: "100%", display: "grid" }}
          >
            <p style={{ margin: 0, textAlign: "center", fontWeight: 700 }}>
              Select Start & End Date
            </p>
            <RangePicker
              style={{ width: "100%" }}
              format="YYYY-MM-DD"
              onChange={(dates, dateStrings) =>
                this.setState(
                  {
                    //   dates: dates,
                    dateRange: dateStrings,
                  }
                )
              }
              disabledDate={(current) => {
                return current && current > moment().endOf("day");
              }}
            />
            <Button
              className="bbtn"
              type="primary"
              style={{ background: "#0071bc", width: "100%" }}
              onClick={() => this.customFilter()}
            >
              Submit
            </Button>
          </Space> */}
        </Modal>
      </Container>
    );
  }
}

//? Server side calls

CreditTransaction.getInitialProps = wrapper.getInitialPageProps(
  (store) => async (props) => {
    const cookies = parseCookies(props.req);

    await store.dispatch(setCurrentPage({ currentPage: "tm-wallet" }));

    await store.dispatch(
      walletThunk({
        history: Router,
        access_token: cookies?.token,
        customerId: cookies.customerId,
      })
    );

    await store.dispatch(
      walletHistoryThunk({
        history: Router,
        access_token: cookies?.token,
        customerId: cookies.customerId,
      })
    );

    await store.dispatch(
      getFaqCategory({
        history: Router,
      })
    );

    let value = store
      .getState()
      .faqData.FAQCategory?.payload.filter((i) => i.name == "TM Credit")
      .map((p) => p.id)
      .toString();

    value &&
      (await store.dispatch(
        getFaq({
          history: Router,
          categoryId: value,
        })
      ));

    let prop = {};
    prop.accessToken =
      store.getState().loginReducer.verifyOtpSuccess?.Response?.access_token;

    prop.TransactionList = store.getState().wallet?.walletHistory;
    prop.customerId =
      store.getState().loginReducer?.verifyOtpSuccess?.CustomerId;
    prop.TransactionError = store.getState().wallet?.walletHistoryError;
    prop.wallet = store.getState().wallet;
    prop.referEarn = store.getState().referEarn;
    prop.customerMobileNo =
      store.getState().loginReducer?.verifyOtpSuccess?.CustomerDto?.mobileNo;
    prop.faqData = store.getState().faqData;

    return { ...prop };
  }
);

const mapStateToProps = (state) => ({
  // TransactionList: state.wallet.walletHistory,
  // customerId: state.loginReducer.verifyOtpSuccess?.CustomerId,
  // accessToken: state.loginReducer.verifyOtpSuccess?.Response?.access_token,
  // TransactionError: state.wallet.walletHistoryError,
  // wallet: state.wallet,
  // referEarn: state.referEarn,
  // customerMobileNo: state.loginReducer?.verifyOtpSuccess?.CustomerDto?.mobileNo,
  // faqData: state.faqData,
});

export default withRouter(
  connect(mapStateToProps, {
    walletThunk,
    walletHistoryThunk,
    setCurrentPage,
    getFaqCategory,
    getFaq,
  })(CreditTransaction)
);

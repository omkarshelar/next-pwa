import Router, { withRouter } from "next/router";
import React, { Component } from "react";
import {
  MedSearchLoader,
  MedSearchWrapper,
  MedResultCartSection,
  MedResult,
  NoResultFound,
} from "../../pageComponents/search/MedSearch.style";
import EmptyCart from "../../components/WebComponents/EmptyCart/EmptyCart";
import UploadCard from "../../components/WebComponents/UploadCard/UploadCard";
import ViewCart from "../../components/WebComponents/ViewCart/ViewCart";

import { connect } from "react-redux";
import window from "global";

import { wrapper } from "../../redux/store";
import {
  fetchMedicineThunk,
  fetchMedicineThunkOnSearch,
} from "../../redux/SearchMedicine/Action";
import { setCurrentPage } from "../../redux/Pincode/Actions";
import {
  notOnOrderSummaryAction,
  toAddressDetailsAction,
  toCartSectionAction,
  toOrderSummaryAction,
  toPatientDetailsAction,
  toUploadPrescriptionAction,
} from "../../redux/Timeline/Action";
import { fetchProductDetailsThunk } from "../../redux/ProductDetails/Action";
import { setPaymentModeThunk } from "../../redux/MyOrder/Action";
import { incompleteOrderThunk } from "../../redux/IncompleteOrder/Action";
import {
  clearMedDetailsAction,
  comfirmMedicineThunk,
} from "../../redux/ConfirmMedicine/Action";
import { lastImageClearAction } from "../../redux/UploadImage/Action";
import { changePaymentAction } from "../../redux/Payment/Action";

import noMedsFound from "../../src/Assets/noMedsFound.svg";
// import Lottie from "react-lottie";
import animationData from "../../src/Assets/loaderSearchPage.json";
import {
  eventSearchListingNoMedicineFound,
  eventSearchListingPDPage,
  eventSearchListingViewCart,
  eventSearchListingViewed,
} from "../../src/Events/Events";
import SearchMedicineCard from "../../pageComponents/search/SearchMedicineCard/SearchMedicineCard";
import convertProductTitle from "../../components/Helper/convertProductTitle";
import { message } from "antd";
import dynamic from "next/dynamic";

//dynamic Imports
const Lottie = dynamic(() => import("react-lottie"));

const defaultOptions = {
  loop: true,
  autoplay: true,
  animationData: animationData,
  rendererSettings: {
    preserveAspectRatio: "xMidYMid slice",
  },
};

export class MedSearch extends Component {
  state = {
    inputMedicine: this.props.inputMedicine ? this.props.inputMedicine : "",
    selectedInfo: "",
    medHits: this.props.medicineList ? this.props.medicineList.hits : [],
    showLoader: false,
  };

  setInputSearch = (e) => {
    this.setState({ inputMedicine: e.target.value });
  };

  clearSearchHandler = () => {
    this.setState({ inputMedicine: "" });
  };
  // Update med search
  componentDidMount() {
    this.props.setCurrentPage({ currentPage: "MedSearch" });

    /* if (this.props.history?.location?.state?.medname) {
      this.setState({
        inputMedicine: this.props.history?.location?.state?.medname,
      });
    }

    if (this.props.history?.location?.search) {
      const params = new URLSearchParams(this.props.history?.location?.search);

      if (params.get("type")) {
        this.setState({
          searchType: params.get("type"),
        });
      }
    } */
  }

  componentDidUpdate(prevprop, prevstate) {
    // message.loading("Fetching medicines...", 0);
    if (this.props.router?.query?.term != prevprop.router?.query?.term) {
      this.setState({
        showLoader: true,
        inputMedicine: this.props.router?.query?.term,
      });
      this.handleCheck();
    }
    /* if (prevprop.match.params.id !== this.props.match.params.id) {
      this.setState({
        showLoader: true,
      });
      this.handleCheck();
    } */
    if (
      prevprop.pincodeDetails?.warehouseId !=
      this.props.pincodeDetails?.warehouseId
    ) {
      this.handleCheck();
    }

    /* if (prevprop.location?.search != this.props.location?.search) {
      if (!this.props.location?.search) {
        this.setState({
          searchType: undefined,
        });
      }
    } */
  }

  handleCheck = () => {
    clearTimeout(this.timer);
    this.timer = setTimeout(() => {
      this.triggerCheck();
    }, 300);
  };

  triggerCheck = () => {
    /* let data = {
      medicine: {
        size: 20,
        query: {
          multi_match: {
            query: this.state.inputMedicine,
            type: "bool_prefix",
            operator: "and",
            fields: ["original_sku_name"],
          },
        },
        sort: [
          { original_supplied_bytm: "desc" },
          {
            original_available: "desc",
          },
          { prod_searched_count: "desc" },
        ],
        post_filter: {
          term: {
            original_is_searchable: "true",
          },
        },
        suggest: {
          skuNameSuggester: {
            text: this.state.inputMedicine,
            term: {
              field: "original_sku_name",
              sort: "frequency",
              size: 20,
            },
          },
        },
      },
      history: this.props.history,
      warehouseId: this.props.pincodeDetails?.warehouseId
        ? this.props.pincodeDetails.warehouseId
        : 3,
      access_token: this.props.access_token ? this.props.access_token : null,
    }; */
    let { searchType } = this.state;
    this.setState({
      showLoader: true,
    });
    // message.loading("Fetching medicines...", 0);
    let data = {};
    data.access_token = this.props.access_token;
    try {
      data.searchString = decodeURIComponent(this.state.inputMedicine);
    } catch (error) {
      data.searchString = this.state.inputMedicine;
    }

    if (this.props.history?.location?.search) {
      const params = new URLSearchParams(this.props.history?.location?.search);
      if (params.get("type")) {
        searchType = params.get("type");
      }
    }

    data.isMultiSearch = searchType ? false : true;
    data.elasticSearchType = searchType
      ? "ORIGINAL_DRUG_TYPE"
      : "SKU_BRAND_SEARCH";
    data.warehouseId = this.props.pincodeDetails?.warehouseId
      ? this.props.pincodeDetails?.warehouseId
      : 3;

    this.props.fetchMedicineThunkOnSearch(data).then(() => {
      if (this.props.medDataSearchFailure) {
        message.error(this.props.medDataSearchFailure);
        this.setState({
          medHits: [],
          showLoader: false,
        });
        eventSearchListingNoMedicineFound();
      } else {
        eventSearchListingViewed();
        this.setState({
          medHits: this.props.medicineList.hits,
          showLoader: false,
        });
      }
    });
  };

  renderCartandMedItems = (apiData, cartData) => {
    let updatedApiData = [];
    for (const apiItem in apiData) {
      for (const cardItem in cartData) {
        if (
          cartData[cardItem]._source.original_product_code ===
          apiData[apiItem]._source.original_product_code
        ) {
          updatedApiData.push({ dataIndex: apiItem, ...cartData[cardItem] });
        }
      }
    }
    for (const updatedItem of updatedApiData) {
      apiData.splice(Number(updatedItem.dataIndex), 1, updatedItem);
    }
    return apiData;
  };

  filterMedDataUpdate = (MedData, inputMed) => {
    if (this.props.medicineList.hits) {
      let filterMedName = MedData.filter(
        (content) => content._source.original_sku_name
      );
      // let filterMedName = MedData.filter((content) =>
      //   content._source.original_sku_name
      //     .toLowerCase()
      //     .includes(inputMed.toLowerCase())
      // ).slice(0, 10);
      this.renderCartandMedItems(filterMedName, this.props.cartContent);

      return filterMedName;
    }
  };

  getPaymentIdFromType = (type) => {
    // setting payment id based on the paymnet type we get from coupon details
    let paymentId = this.props.payment.isOnline ? 16 : 17;
    if (type === "Cod Payment") {
      paymentId = 17;
      this.onClickSelectPaymentType(false);
    } else if (type === "Online Payment") {
      paymentId = 16;
      this.onClickSelectPaymentType(true);
    }
    return paymentId;
  };

  onClickSelectPaymentType = (isOnline) => {
    if (!this.props.appliedCoupon) {
      this.props.changePaymentAction(isOnline);
    } else {
      if (
        this.props.appliedCoupon.applicableOn === "Online Payment" &&
        !isOnline
      ) {
        customNotification(
          "Only Online payment allowed with selected Coupon",
          "customNotify"
        );
      } else if (
        this.props.appliedCoupon.applicableOn === "Cod Payment" &&
        isOnline
      ) {
        customNotification(
          "Only COD payment allowed with selected Coupon",
          "customNotify"
        );
      } else {
        this.props.changePaymentAction(isOnline);
      }
    }
  };

  medDataList = () => {
    let medData = [];

    if (this.props.cartContent) {
      this.props.cartContent.map((e) => {
        medData.push({
          productCode: e._source.original_product_code,
          quantity: e.quantity,
        });
      });
    }

    return medData;
  };

  proceedOrder = async () => {
    if (this.props.medConfirm?.ConfirmMedData?.orderId) {
      // if coupon is applied, set payment mode
      if (this.props.appliedCoupon?.offerId) {
        await this.props.setPaymentModeThunk({
          orderId: this.props.medConfirm.ConfirmMedData.orderId,
          access_token: this.props.accessToken?.Response.access_token,
          paymentId: this.getPaymentIdFromType(
            this.props.appliedCoupon.applicableOn
          ),
        });
      }
      this.props
        .incompleteOrderThunk({
          orderIds: this.props.medConfirm.ConfirmMedData.orderId,
          accessToken: this.props.accessToken.Response.access_token,
          medSet: this.medDataList(),
          offerId: this.props.appliedCoupon?.offerId
            ? this.props.appliedCoupon.offerId
            : 0,
          history: Router,
          pincode: this.props.pincodeDetails.pincode,
        })
        .then(() => {
          if (this.props.medIncomplete.incompleteOrderError) {
            if (
              this.props.medIncomplete.incompleteOrderError ===
              "Sorry order cannot be confirmed. Please contact customer support."
            ) {
              this.props.clearMedDetailsAction().then(() => {
                this.props.lastImageClearAction().then(() => {
                  this.props.notOnOrderSummaryAction();
                });
              });
            } else {
              message.error(this.props.medIncomplete.incompleteOrderError);
            }
          } else {
            this.props.toOrderSummaryAction();
            Router.push("/orderflow");
          }
        });
    } else if (this.props.imgUpload?.orderId) {
      // if coupon is applied, set payment mode
      if (this.props.appliedCoupon?.offerId) {
        await this.props.setPaymentModeThunk({
          orderId: this.props.imgUpload.orderId,
          access_token: this.props.accessToken?.Response.access_token,
          paymentId: this.getPaymentIdFromType(
            this.props.appliedCoupon.applicableOn
          ),
        });
      }
      this.props
        .incompleteOrderThunk({
          orderIds: this.props.imgUpload.orderId,
          accessToken: this.props.accessToken.Response.access_token,
          medSet: this.medDataList(),
          offerId: this.props.appliedCoupon?.offerId
            ? this.props.appliedCoupon.offerId
            : 0,
          history: Router,
          pincode: this.props.pincodeDetails.pincode,
        })
        .then(() => {
          if (this.props.medIncomplete.incompleteOrderError) {
            if (
              this.props.medIncomplete.incompleteOrderError ===
              "Sorry order cannot be confirmed. Please contact customer support."
            ) {
              this.props.clearMedDetailsAction().then(() => {
                this.props.lastImageClearAction().then(() => {
                  this.props.notOnOrderSummaryAction();
                  // this.setState({ duplicateOrderId: true });
                });
              });
            } else {
              message.error(this.props.medIncomplete.incompleteOrderError);
            }
          } else {
            this.props.toOrderSummaryAction();
            Router.push("/orderflow");
          }
        });
    } else {
      this.props.toCartSectionAction();
      Router.push("/orderflow");
    }
  };

  onClickUploadPrescription = () => {
    eventSearchListingViewCart();
    if (
      this.props.cartContent?.length > 0 &&
      !this.props.medConfirm?.ConfirmMedData?.orderId
    ) {
      this.setState(
        {
          medData: this.props.cartContent.map((data) => {
            return {
              productCode: data._source.original_product_code,
              quantity: data.quantity,
            };
          }),
        },
        () => {
          let data = {
            orderIds: 0,
            accessToken: this.props.accessToken?.Response.access_token,
            medSet: this.state.medData,
            customerId: this.props.custId,
            offerId: this.props.appliedCoupon?.offerId
              ? this.props.appliedCoupon.offerId
              : 0,
            pincode: this.props.pincodeDetails?.pincode,
            history: Router,
          };
          this.props.comfirmMedicineThunk(data).then(() => {
            if (this.props.medConfirm?.ConfirmMedError) {
              message.error(this.props.medConfirm.ConfirmMedError);
            } else {
              this.props.toUploadPrescriptionAction();
              Router.push("/orderflow");
            }
          });
        }
      );
    } else {
      this.props.toUploadPrescriptionAction();
      Router.push("/orderflow");
    }
  };

  render() {
    let filteredMedUpdate = [];
    let {
      cartContent,
      toUploadPrescriptionAction,
      toPatientDetailsAction,
      toAddressDetailsAction,
      toCartSectionAction,
      history,
      medicineList,
    } = this.props;

    if (medicineList?.hits) {
      filteredMedUpdate = this.filterMedDataUpdate(
        this.state.medHits,
        this.state.inputMedicine
      );
    }
    let decodeValue = "";
    try {
      decodeValue = decodeURIComponent(this.state.inputMedicine?.toLowerCase());
    } catch (error) {
      decodeValue = this.state.inputMedicine?.toLowerCase();
    }
    return (
      <>
        {/* <Navigation
          inputMedicine={this.state.inputMedicine}
          setInputSearch={this.setInputSearch}
          clearSearchHandler={this.clearSearchHandler}
        /> */}

        {this.state.showLoader ? (
          <MedSearchLoader>
            <div className="lottieWrapper">
              <Lottie
                options={defaultOptions}
                height={window.innerWidth > 768 ? 220 : 150}
                width={window.innerWidth > 768 ? 220 : 150}
              />
              <div className="lottieWrapperText">Searching...</div>
            </div>
          </MedSearchLoader>
        ) : (
          <MedSearchWrapper>
            <MedResultCartSection>
              {/* {this.props.recentSearch.length > 0 && (
              <>
                <RecentSearch>
                  <div className="recent-search-head">
                    <h6>Recently Searched</h6>
                    <span onClick={() => this.props.removeRecentSearchAction()}>
                      Clear
                    </span>
                  </div>

                  <HistoryWrapper>
                    {this.props.recentSearch.map((data) => (
                      <Continue
                        BtnHistory
                        onClick={() => this.setState({ inputMedicine: data })}
                      >
                        {data}
                      </Continue>
                    ))}
                  </HistoryWrapper>
                </RecentSearch>
              </>
            )} */}
              {
                filteredMedUpdate.length > 0 &&
                this.state.inputMedicine.length > 0 ? (
                  <MedResult>
                    {filteredMedUpdate.length > 0 && (
                      <p className="result-med-p">
                        Showing all results for{" "}
                        <span
                          style={{
                            fontWeight: "600",
                            color: "#40464D",
                            textTransform: "capitalize",
                          }}
                        >
                          {decodeValue}
                        </span>
                      </p>
                    )}
                    {filteredMedUpdate.map((content, index) => (
                      <SearchMedicineCard
                        content={content}
                        key={content._id}
                        lastIndex={filteredMedUpdate.length !== index + 1}
                        // recentMedSearch={this.props.addRecentSearchAction}
                        getAddress={() => {
                          this.props
                            .fetchProductDetailsThunk({
                              productCd:
                                content?._source?.original_product_code,
                              history: Router,
                            })
                            .then(() => {
                              this.setState({
                                selectedInfo:
                                  content?._source?.original_product_code,
                              });
                            });
                        }}
                        isInfo={this.state.selectedInfo}
                        closeToolTip={() => this.setState({ selectedInfo: "" })}
                        companyAddress={
                          this.props.productDetails?.allProductDetails
                            ?.MedicineDetails?.companyAddress
                        }
                        goToDetails={(e) => {
                          eventSearchListingPDPage();
                          Router.push(
                            "/medicine/" +
                              convertProductTitle(
                                content?._source?.original_sku_name,
                                content?._source?.original_product_code
                              )
                          );
                        }}
                      />
                    ))}
                    <div
                      className={
                        cartContent.length > 0
                          ? "end-list-container"
                          : "end-list-no-container"
                      }
                    >
                      <span>You have reached the end of the list</span>
                    </div>
                  </MedResult>
                ) : (
                  <>
                    <NoResultFound>
                      <div className="medsIcons">
                        <img src={noMedsFound} />
                      </div>
                      <div className="medsNRFText">
                        {"Oops! No matching medicines found"}
                      </div>
                    </NoResultFound>
                  </>
                )
                // this.props.recentSearch.length === 0 && (
                //   <SearchProductImage>
                //     <img
                //       src={empty}
                //       className="empty-cart"
                //       alt="empty-cart"
                //     ></img>
                //   </SearchProductImage>
                // )
              }
            </MedResultCartSection>
            <div className="med-search-right-container">
              {cartContent?.length > 0 ? (
                <ViewCart
                  items={cartContent.length}
                  onClickView={() => {
                    eventSearchListingViewCart();
                    if (this.props.Stepper.toOrderSummary) {
                      this.proceedOrder();
                    } else {
                      this.props.toCartSectionAction();
                      Router.push("/orderflow");
                    }
                  }}
                />
              ) : (
                <EmptyCart />
              )}
              <UploadCard onClickUpload={this.onClickUploadPrescription} />
            </div>
            {window.innerWidth < 969 && cartContent?.length > 0 ? (
              <ViewCart
                items={cartContent.length}
                onClickView={() => {
                  if (this.props.Stepper.toOrderSummary) {
                    this.proceedOrder();
                  } else {
                    this.props.toCartSectionAction();
                    Router.push("/orderflow");
                  }
                }}
              />
            ) : null}
          </MedSearchWrapper>
        )}
        {/* {window.innerWidth > 768 ? <Footer /> : ""} */}
      </>
    );
  }
}

//server calls
MedSearch.getInitialProps = wrapper.getInitialPageProps(
  (store) => async (props) => {
    let term = props.query.term;
    let searchType;

    if (props.query?.type) {
      searchType = props.query.type;
    }

    let data = {};
    data.isMultiSearch = searchType ? false : true;
    data.elasticSearchType = searchType
      ? "ORIGINAL_DRUG_TYPE"
      : "SKU_BRAND_SEARCH";
    data.warehouseId = store.getState()?.pincodeDetails?.warehouseId
      ? store.getState()?.pincodeDetails?.warehouseId
      : 3;
    data.searchString = term;

    await store.dispatch(fetchMedicineThunkOnSearch(data));

    let prop = {};
    prop.searchType = searchType;
    prop.inputMedicine = term;
    prop.medicineList = store.getState()?.searchMedicine?.medDataSearchSuccess;

    return { ...prop };
  }
);

let mapStateToProps = (state) => ({
  Stepper: state.stepper,
  cartContent: state.cartData?.cartItems,
  recentSearch: state.recentSearch?.searchHistory,
  productDetails: state.productDetails,
  pincodeDetails: state.pincodeData?.pincodeData,
  currentPage: state.pincodeData?.currentPage?.currentPage,
  access_token: state.loginReducer?.verifyOtpSuccess?.Response?.access_token,
  medDataSearchFailure: state.searchMedicine?.medDataSearchFailure,
  medConfirm: state.confirmMedicineReducer,
  appliedCoupon: state.cartData?.appliedCoupon,
  medIncomplete: state.incompleteOrderReducer,
  payment: state.paymentReducer,
  accessToken: state.loginReducer?.verifyOtpSuccess,
  imgUpload: state.uploadImage,
  custId: state.loginReducer?.verifyOtpSuccess?.CustomerId,
  medicineList: state.searchMedicine.medDataSearchSuccess,
});

export default withRouter(
  connect(mapStateToProps, {
    setCurrentPage,
    fetchMedicineThunk,
    toCartSectionAction,
    toAddressDetailsAction,
    toPatientDetailsAction,
    toUploadPrescriptionAction,
    toOrderSummaryAction,
    fetchProductDetailsThunk,
    fetchMedicineThunkOnSearch,
    setPaymentModeThunk,
    changePaymentAction,
    incompleteOrderThunk,
    clearMedDetailsAction,
    lastImageClearAction,
    notOnOrderSummaryAction,
    comfirmMedicineThunk,
  })(MedSearch)
);

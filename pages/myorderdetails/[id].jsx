import { connect } from "react-redux";
import React from "react";
import Router, { withRouter } from "next/router";
import { message } from "antd";
import Axios from "axios";
import { renderToStaticMarkup } from "react-dom/server";
import window from "global";
import { wrapper } from "../../redux/store";

//helper
import { parseCookies } from "../../components/Helper/parseCookies";

//actions
import { orderStatusThunk } from "../../redux/OrderDetail/Action";
import { checkPincodeThunk } from "../../redux/AddressDetails/Action";
import { fetchPatientByIdThunk } from "../../redux/PatientDetails/Action";
import { getDoctorRatingDetailsThunk } from "../../redux/DoctorRating/Action";
import { fetchOrderStatusThunk } from "../../redux/MyOrder/Action";
import { AddIncompleteOrderMedsAction } from "../../redux/Cart/Action";
import { toCartSectionAction } from "../../redux/Timeline/Action";
import { reorderThunk } from "../../redux/Reorder/Action";
import { getAllCouponsThunk } from "../../redux/Cart/Action";
import { compareOrderDetailsThunk } from "../../redux/PatientDetails/Action";
import { fetchMedicineWithImagesThunk } from "../../redux/SearchMedicine/Action";

//styles
import "../../pageComponents/myorderdetails/myOrderDetails.module.css";

//components
import OrderHeader from "../../components/WebComponents/WaitingScreen/SubComponents/OrderHeader";
import SavingsCard from "../../components/WebComponents/WaitingScreen/SubComponents/SavingsCard";
import BillDetails from "../../components/WebComponents/BillDetails/BillDetails";
import DoctorConsultationCard from "../../components/WebComponents/WaitingScreen/SubComponents/DoctorConsultationCard";
import MedicineCardAccordian from "../../components/WebComponents/MedicineCardAccordian/MedicineCardAccordian";
import PaymentModeWs from "../../components/WebComponents/WaitingScreen/SubComponents/PaymentModeWS";
import AddressSection from "../../components/WebComponents/WaitingScreen/SubComponents/AddressSection";
import AndroidSeperator from "../../components/WebComponents/AndroidSeperator/AndroidSeperator";
import PrescriptionSection from "../../components/WebComponents/WaitingScreen/SubComponents/PrescriptionSection";
import MyOrdersShimmer from "../../components/WebComponents/Shimmer/MyOrdersShimmer";
import ReorderComponent from "../../pageComponents/myorderdetails/SubComponents/ReorderComponent";
import StatusCard from "../../pageComponents/myorderdetails/SubComponents/StatusCard";
import ModificationLog from "../../pageComponents/myorderdetails/SubComponents/ModificationLog";
import DoctorViewMobCard from "../../pageComponents/myorderdetails/SubComponents/DoctorViewMobCard";
import ReturnComponent from "../../pageComponents/myorderdetails/SubComponents/ReturnComponent";
import TimelineSummary from "../../pageComponents/myorderdetails/SubComponents/TimelineSummary";

//consts
const viewCountConst = 3;

class MyOrderDetails extends React.Component {
  state = {
    viewMore: false,
    viewCount: viewCountConst,
    orderStatusLoader: true,
    patientDetailsLoader: true,
    updateDetail: this.props.updateDetail ? this.props.updateDetail : {},
  };

  componentDidMount = () => {
    if (this.props.updateDetail?.productSubsMappingList?.length > 0) {
      if (
        this.props.updateDetail?.orderStatus?.serialId === 49 ||
        this.props.updateDetail?.orderStatus?.serialId === 39 ||
        this.props.updateDetail?.orderStatus?.serialId === 2
      ) {
        this.props.getAllCouponsThunk({
          access_token: this.props.accessToken,
          customerId: this.props.forCustId,
          history: Router,
        });
      }
      this.addImagesForMeds(this.props.updateDetail.productSubsMappingList);
    }

    // window.history.pushState(null, null, window.location.pathname);
    // window.addEventListener("popstate", this.onBackButtonEvent);
  };

  // Clear State after unmounting
  // componentWillUnmount = () => {
  //   window.removeEventListener("popstate", this.onBackButtonEvent);
  // };

  // On Back Button click Event.
  /*  onBackButtonEvent = (e) => {
    e.preventDefault();
    if (!this.isBackButtonClicked) {
      if (window && this.props.history?.location?.state?.isMyOrder) {
        this.isBackButtonClicked = true;
        Router.push({
          pathname: "/myorders",
          state: {
            isMyOrder: true,
            orderType: this.props.history?.location?.state?.orderType,
            patientFilter: this.props.history?.location?.state?.patientFilter,
          },
        });
      } else {
        window.history.pushState(null, null, this.props.history?.goBack());
        this.isBackButtonClicked = false;
      }
    }
  }; */

  componentDidUpdate = (prevprops) => {
    if (prevprops.updateDetail != this.props.updateDetail) {
      if (this.props.updateDetail?.productSubsMappingList?.length > 0) {
        if (
          this.props.updateDetail?.orderStatus?.serialId === 49 ||
          this.props.updateDetail?.orderStatus?.serialId === 39 ||
          this.props.updateDetail?.orderStatus?.serialId === 2
        ) {
          this.props.getAllCouponsThunk({
            access_token: this.props.accessToken,
            customerId: this.props.forCustId,
            history: Router,
          });
        }
        this.addImagesForMeds(this.props.updateDetail.productSubsMappingList);
      }
    }

    if (prevprops.medicineList != this.props.medicineList) {
      let props = this.props;
      if (
        props.medicineList?.length > 0 &&
        props.updateDetail?.productSubsMappingList?.length > 0
      ) {
        let medArr = props.updateDetail.productSubsMappingList;
        let medNewArr = props.medicineList;
        for (let i in medArr) {
          let med = medArr[i];
          for (let j in medNewArr) {
            let medNew = medNewArr[j];
            if (
              medNew._source.original_product_code ===
              (props.updateDetail?.orderStatus?.serialId === 39 ||
              props.updateDetail?.orderStatus?.serialId === 49 ||
              props.updateDetail?.orderStatus?.serialId === 2
                ? med.orgProductCd
                : med.subsProductCd)
            ) {
              med.productImages = medNew._source.product_image_urls;
              break;
            }
          }
        }
        let updatedOrderDetails = props.updateDetail;
        updatedOrderDetails.productSubsMappingList = medArr;
        this.setState({ updateDetail: updatedOrderDetails });
      }
    }
  };

  getAllProductCodes = (arr) => {
    let productCodes = [];
    for (let i in arr) {
      let med = arr[i];
      productCodes.push(
        this.props.updateDetail?.orderStatus?.serialId === 39 ||
          this.props.updateDetail?.orderStatus?.serialId === 49 ||
          this.props.updateDetail?.orderStatus?.serialId === 2
          ? med.orgProductCd
          : med.subsProductCd
      );
    }
    return productCodes;
  };

  addImagesForMeds = (medArr) => {
    let productCodes = this.getAllProductCodes(medArr);
    let data = {
      access_token: this.props.accessToken?.Response?.access_token,
      medicine: {
        query: {
          bool: {
            should: [
              {
                terms: {
                  "original_product_code.keyword": productCodes,
                },
              },
            ],
          },
        },
      },
      warehouseId: 3,
    };
    this.props.fetchMedicineWithImagesThunk(data);
  };

  setLoader = (type = 1) => {
    switch (type) {
      case 1:
        this.setState({
          orderStatusLoader: false,
        });
        break;
      case 2:
        this.setState({
          patientDetailsLoader: false,
        });
        break;
      default:
        this.setState({
          orderStatusLoader: false,
        });
        break;
    }
  };

  getAllOrderDetails = async () => {
    // Updated APi
    if (this.props.accessToken && this.props.orderId && this.props.forCustId) {
      await this.props
        .orderStatusThunk({
          access_token: this.props.accessToken,
          orderIds: this.props.orderId,
          customerId: this.props.forCustId,
          history: Router,
        })
        .then(() => {
          this.setLoader(1);
          this.setState({
            updateDetail: this.props.updateDetail,
          });
        });
    } else if (
      this.props.accessToken &&
      this.props.imgUpload?.orderId &&
      this.props.forCustId
    ) {
      await this.props
        .orderStatusThunk({
          access_token: this.props.accessToken,
          orderIds: this.props.imgUpload.orderId,
          customerId: this.props.forCustId,
          history: Router,
        })
        .then(() => {
          this.setLoader(1);
          this.setState({
            updateDetail: this.props.updateDetail,
          });
        });
    } else if (
      this.props.accessToken &&
      this.props.medConfirm?.ConfirmMedData?.orderId &&
      this.props.forCustId
    ) {
      await this.props
        .orderStatusThunk({
          access_token: this.props.accessToken,
          orderIds: this.props.medConfirm.ConfirmMedData.orderId,
          customerId: this.props.forCustId,
          history: Router,
        })
        .then(() => {
          this.setLoader(1);
          this.setState({
            updateDetail: this.props.updateDetail,
          });
        });
    }
  };

  getPatientDetails = () => {
    if (this.props.accessToken && this.props.orderId) {
      this.props
        .fetchPatientByIdThunk({
          accessToken: this.props.accessToken,
          orderId: this.props.orderId,
          patientId: 0,
          history: Router,
        })
        .then(() => {
          this.setLoader(2);
        });
    } else if (this.props.accessToken && this.props.imgUpload?.orderId) {
      this.props
        .fetchPatientByIdThunk({
          accessToken: this.props.accessToken,
          orderId: this.props.imgUpload.orderId,
          patientId: 0,
          history: Router,
        })
        .then(() => {
          this.setLoader(2);
        });
    } else if (
      this.props.accessToken &&
      this.props.medConfirm?.ConfirmMedData?.orderId
    ) {
      this.props
        .fetchPatientByIdThunk({
          accessToken: this.props.accessToken,
          orderId: this.props.medConfirm.ConfirmMedData.orderId,
          patientId: 0,
          history: Router,
        })
        .then(() => {
          this.setLoader(2);
        });
    }
  };

  handlePostDoctorRating = (value) => {
    if (value) {
      this.props.orderStatusThunk({
        access_token: this.props.accessToken,
        orderIds: this.props.orderId,
        customerId: this.props.forCustId,
        history: Router,
      });

      this.props.compareOrderDetailsThunk({
        access_token: this.props.accessToken,
        orderId: this.props.orderId,
        history: Router,
      });

      this.props.getDoctorRatingDetailsThunk({
        access_token: this.props.accessToken,
        orderId: this.props.orderId,
        history: Router,
      });
    }
  };

  calcDiscPreProcessing = (billArr) => {
    let filterMeds = billArr.filter(
      (data) => !data.disabled && !data.coldChainDisabled
    );
    let finalPrice = filterMeds.reduce(
      (prev, next) =>
        prev +
        (next.orgMrp -
          next.orgMrp *
            ((100 -
              (next.orgProductCd === next.subsProductCd
                ? ((next.subsMrp - next.subsSellingPrice) * 100) / next.subsMrp
                : next.orgDiscount)) /
              100)),

      0
    );
    return finalPrice.toFixed(2);
  };

  calcTotalPreProcessing = (billArr, mrp, charge, couponValue) => {
    let finalDisc = this.calcDiscPreProcessing(billArr);
    let calcTotal = 0;
    if (couponValue) {
      calcTotal = mrp - finalDisc + charge - couponValue;
    } else {
      calcTotal = mrp - finalDisc + charge;
    }
    return calcTotal.toFixed(2);
  };

  // checking meds for cold chain and med active
  checkMedsForSummary = (arr) => {
    let newArr = [];
    if (arr?.length > 0) {
      for (let i in arr) {
        let med = arr[i];
        if (!med.coldChainDisabled && !med.disabled && med.medActive) {
          newArr.push(med);
        }
      }
    }
    return newArr;
  };

  getTotalMrpForSummary = (arr) => {
    let totalMrp = 0;
    if (arr?.length > 0) {
      for (let i in arr) {
        let med = arr[i];
        totalMrp = totalMrp + med.orgMrp;
      }
    }
    return totalMrp.toFixed(2);
  };

  getTotalDiscountForSummary = (arr) => {
    let totalDiscount = 0;
    if (arr?.length > 0) {
      for (let i in arr) {
        let med = arr[i];
        med.orgProductCd === med.subsProductCd
          ? (totalDiscount =
              totalDiscount + (med.orgMrp - med.subsSellingPrice))
          : (totalDiscount =
              totalDiscount +
              (med.orgMrp -
                (med.orgMrp - med.orgMrp * (med.orgDiscount / 100))));
      }
    }
    return totalDiscount.toFixed(2);
  };

  getTotalSpForSummary = (arr) => {
    let totalSp = 0;
    if (arr?.length > 0) {
      for (let i in arr) {
        let med = arr[i];
        let discount =
          med.orgProductCd !== med.subsProductCd
            ? med.orgDiscount
            : ((med.orgMrp - med.subsSellingPrice) * 100) / med.orgMrp;
        let orgMedSp = med.orgMrp - med.orgMrp * (discount / 100);
        totalSp = totalSp + orgMedSp;
      }
    }
    return totalSp;
  };

  checkForSubs = (arr) => {
    let isSubs = false;
    for (let i in arr) {
      let med = arr[i];
      if (med.statusId === 61) {
        isSubs = true;
        break;
      }
    }
    return isSubs;
  };

  reorderHandler = () => {
    this.props
      .reorderThunk({
        customerId: this.props.custId,
        orderId: this.props.orderId,
        accessToken: this.props.accessToken,
        patientIdSet: [this.props.updateDetail?.patientId],
        history: Router,
      })
      .then(() => {
        if (this.props.reorder.reorderError) {
          message.success(this.props.reorder.reorderError);
        } else {
          this.props
            .orderStatusThunk({
              access_token: this.props.accessToken,
              orderIds: this.props.orderId,
              customerId: this.props.custId,
              history: Router,
            })
            .then(() => {
              if (this.props.orderStatusUpdate.error) {
                message.success(this.props.orderStatusUpdate.error);
              } else if (
                this.props.updateDetail?.productSubsMappingList?.length > 0
              ) {
                this.props.AddIncompleteOrderMedsAction(
                  this.props.updateDetail?.productSubsMappingList
                );
                this.props.toCartSectionAction();
                Router.push("/orderflow");
              }
            });
        }
      });
  };

  returnOrder = async () => {
    const apiUrl = `https://internal.clickpost.in/api/v1/fetch_return_info/`;

    let orderData = {
      domain: "truemeds",
      reference_number: this.props.orderId,
      phone_number: this.props.CustomerDto?.mobileNo,
    };
    Axios.post(apiUrl, orderData, {
      "Content-Type": "application/json",
      "Access-Control-Allow-Origin": "*",
    })
      .then((response) => {
        if (response.data.meta.status === 200) {
          // window.open(
          //   `https://truemeds.clickpost.in/returns?&getAwb=${response.data.result.awb}getShipment_uuid=${response.data.result.shipment_uuid}`
          // );
          window.location.href = `https://truemeds.clickpost.in/returns?&getAwb=${response.data.result.awb}getShipment_uuid=${response.data.result.shipment_uuid}`;
        }
        if (response.data.meta.status === 400) {
          message.error(response.data.meta.message);
          return true;
        }
      })
      .catch((err) => {
        console.error(err.response);
      });
  };

  handleViewToggle = (value) => {
    if (value) {
      this.setState({
        viewMore: value,
        viewCount: this.props.updateDetail?.productSubsMappingList.length,
      });
    } else {
      this.setState({
        viewMore: value,
        viewCount: viewCountConst,
      });
    }
  };

  render() {
    let loader = this.props.isLoading;
    return (
      <div className={`${"orderDetails_myOrderDetailsWrapper"}`}>
        <div className={"orderDetails_leftContainer"}>
          {loader ? (
            <>
              <MyOrdersShimmer />
            </>
          ) : (
            <>
              <StatusCard
                deliveryDate={this.props.updateDetail?.deliveryDate}
                modifiedOn={this.props.updateDetail?.modifiedOn}
                orderStatus={this.props.orderStatus}
                isCancelled={
                  this.props.orderStatus?.orderTracking?.orderStatus.filter(
                    (e) => e.statusId === 57
                  ).length > 0
                }
              />
              <OrderHeader
                orderId={this.props.orderId}
                patientName={this.props.patientData?.patientName}
                orderPlaced={this.props.updateDetail?.orderDate}
                isMyOrderDetails={true}
              />

              {window.innerWidth <= 768 &&
                renderToStaticMarkup(
                  <SavingsCard
                    updateDetail={this.props.updateDetail}
                    medicineList={this.props.medicineList}
                    isLoading={loader}
                    orderStatus={this.props.orderStatus}
                    isCancelled={
                      this.props.orderStatus?.orderTracking?.orderStatus.filter(
                        (e) => e.statusId === 57
                      ).length > 0
                    }
                  />
                ) !== "" && (
                  <div className={"orderDetails_savingsWrapper"}>
                    <SavingsCard
                      updateDetail={this.props.updateDetail}
                      medicineList={this.props.medicineList}
                      isLoading={loader}
                      isOrderDetails={true}
                      isCancelled={
                        this.props.orderStatus?.orderTracking?.orderStatus.filter(
                          (e) => e.statusId === 57
                        ).length > 0
                      }
                    />
                  </div>
                )}
              <ModificationLog
                isMyOrderDetails={true}
                compareOrderData={this.props.compareOrderData}
                medData={this.props.medicineList}
              />
              {this.state.updateDetail?.productSubsMappingList &&
              this.state.updateDetail?.productSubsMappingList.length > 0 ? (
                <div className={"orderDetails_productDetailsWrap"}>
                  <div className={"orderDetails_productTitle"}>
                    <span>{`Ordered ${
                      this.state.updateDetail?.productSubsMappingList.length ===
                      1
                        ? "Item"
                        : "Items"
                    }`}</span>
                    <span className={"orderDetails_countProductTitle"}>
                      {`(${this.state.updateDetail?.productSubsMappingList.length})`}
                    </span>
                  </div>
                  <div className={"orderDetails_medicineWrap"}>
                    {this.state.updateDetail?.productSubsMappingList &&
                      this.state.updateDetail?.productSubsMappingList?.length >
                        0 &&
                      this.state.updateDetail.productSubsMappingList.map(
                        (content, index) => {
                          if (index < this.state.viewCount) {
                            return (
                              <MedicineCardAccordian
                                updateDetails={this.state.updateDetail}
                                details={true}
                                summary={false}
                                content={content}
                                {...this.props}
                                index={index}
                                isOrderSummary={true}
                                isViewDetails={true}
                                viewOriginal={true}
                                isMyOrderDetails={true}
                                hideLast={
                                  this.state.updateDetail
                                    ?.productSubsMappingList?.length <
                                  this.state.viewCount
                                    ? index + 1 ===
                                      this.state.updateDetail
                                        ?.productSubsMappingList?.length
                                    : index + 1 === this.state.viewCount
                                }
                              />
                            );
                          }
                        }
                      )}
                  </div>
                  {this.state.updateDetail?.productSubsMappingList &&
                  this.state.updateDetail?.productSubsMappingList.length >
                    viewCountConst ? (
                    <div
                      className={"orderDetails_viewMoreBtn"}
                      onClick={() => {
                        this.handleViewToggle(!this.state.viewMore);
                      }}
                    >
                      <div>
                        {`View ${
                          this.state.viewMore
                            ? "Less"
                            : "More (" +
                              (this.state.updateDetail?.productSubsMappingList
                                .length -
                                viewCountConst) +
                              ")"
                        }`}
                      </div>
                    </div>
                  ) : (
                    ""
                  )}
                </div>
              ) : null}
              {this.props.updateDetail?.ImageMasterDto &&
                this.props.updateDetail?.ImageMasterDto.length > 0 && (
                  <>
                    {this.props.updateDetail?.productSubsMappingList.length >
                    0 ? (
                      <div className={"orderDetails_customMarginBottom"}>
                        <AndroidSeperator
                          showWeb={true}
                          showMob={
                            this.props.updateDetail?.productSubsMappingList
                              ?.length > 3
                              ? false
                              : true
                          }
                        />
                      </div>
                    ) : null}
                    <PrescriptionSection
                      updateDetail={this.props.updateDetail}
                      isMyOrderDetails={true}
                    />
                  </>
                )}
              {window.innerWidth <= 768 &&
                (this.props.orderStatus?.orderTracking?.orderStatus.filter(
                  (e) => e.statusId === 57
                ).length > 0 &&
                this.props.updateDetail?.productSubsMappingList?.length === 0 &&
                this.props.updateDetail?.ImageMasterDto?.length > 0 ? null : (
                  <div className={"orderDetails_billWrapper"}>
                    <AndroidSeperator
                      showMob={
                        this.props.updateDetail?.productSubsMappingList.length >
                        3
                          ? this.props.updateDetail?.ImageMasterDto &&
                            this.props.updateDetail?.ImageMasterDto.length > 0
                            ? true
                            : false
                          : true
                      }
                    />
                    <BillDetails
                      details={true}
                      isOrderStatus={true}
                      updateDetail={this.props.updateDetail}
                      baseDiscount={
                        this.props.masterDetails?.baseDiscount
                          ? Number(this.props.masterDetails.baseDiscount)
                          : 20
                      }
                      deliveryCharge={
                        this.props.masterDetails?.deliveryCharge
                          ? Number(this.props.masterDetails.deliveryCharge)
                          : 0
                      }
                      deliveryOnAmount={
                        this.props.masterDetails?.deliveryOnAmount
                          ? Number(this.props.masterDetails.deliveryOnAmount)
                          : 0
                      }
                      deliveryChargeSubs={
                        this.props.masterDetails?.deliveryChargeSubs
                          ? Number(this.props.masterDetails.deliveryChargeSubs)
                          : 0
                      }
                      deliveryOnAmountSubs={
                        this.props.masterDetails?.deliveryOnAmountSubs
                          ? Number(
                              this.props.masterDetails.deliveryOnAmountSubs
                            )
                          : 0
                      }
                      calcDiscPreProcessing={this.calcDiscPreProcessing}
                      calcTotalPreProcessing={this.calcTotalPreProcessing}
                      getTotalSpForSummary={this.getTotalSpForSummary}
                      checkMedsForSummary={this.checkMedsForSummary}
                      getTotalMrpForSummary={this.getTotalMrpForSummary}
                      getTotalDiscountForSummary={
                        this.getTotalDiscountForSummary
                      }
                      checkForSubs={this.checkForSubs}
                      isLoading={loader}
                      promoCode={this.props.updateDetail?.Promocode}
                      promoCodeValue={
                        this.props.updateDetail?.orderStatus?.serialId === 49 ||
                        this.props.updateDetail?.orderStatus?.serialId === 39 ||
                        this.props.updateDetail?.orderStatus?.serialId === 2
                          ? 0
                          : 0
                      }
                    />
                  </div>
                ))}
              {this.props.updateDetail?.AddressDetails && (
                <AddressSection
                  addressDetails={this.props.updateDetail?.AddressDetails}
                  showAltContact={true}
                  title={"Address"}
                  altContact={this.props.updateDetail?.alternateNumber}
                />
              )}

              {this.props.updateDetail?.paymentMode && (
                <PaymentModeWs
                  spaceBetween={true}
                  paymentMode={this.props.updateDetail?.paymentMode}
                />
              )}
              {window.innerWidth <= 768 &&
              this.props.doctorRatingData?.getDoctorRatingSuccess ? (
                <DoctorViewMobCard
                  doctorData={
                    this.props.doctorRatingData?.getDoctorRatingSuccess
                  }
                  orderId={this.props.orderId}
                  customerId={this.props.forCustId}
                  serviceCall={(value) => this.handlePostDoctorRating(value)}
                />
              ) : null}
              <TimelineSummary
                updateDetail={this.props.updateDetail}
                orderStatus={this.props.orderStatus}
              />
              <ReturnComponent
                updateDetail={this.props.updateDetail}
                orderStatus={this.props.orderStatus}
                reorderHandler={() => this.reorderHandler()}
                returnOrder={() => this.returnOrder()}
              />
              {window.innerWidth <= 768 ? (
                <ReorderComponent
                  updateDetail={this.props.updateDetail}
                  orderStatus={this.props.orderStatus}
                  reorderHandler={() => this.reorderHandler()}
                  returnOrder={() => this.returnOrder()}
                  isLoading={loader}
                />
              ) : null}
            </>
          )}
        </div>
        <div className={"orderDetails_rightContainer"}>
          <div className={"orderDetails_rightContainerSticky"}>
            {window.innerWidth > 768 &&
              renderToStaticMarkup(
                <SavingsCard
                  updateDetail={this.props.updateDetail}
                  medicineList={this.props.medicineList}
                  isLoading={loader}
                  orderStatus={this.props.orderStatus}
                  isCancelled={
                    this.props.orderStatus?.orderTracking?.orderStatus.filter(
                      (e) => e.statusId === 57
                    ).length > 0
                  }
                />
              ) !== "" && (
                <div className={"orderDetails_rightItem"}>
                  <SavingsCard
                    updateDetail={this.props.updateDetail}
                    medicineList={this.props.medicineList}
                    isLoading={loader}
                    orderStatus={this.props.orderStatus}
                    isCancelled={
                      this.props.orderStatus?.orderTracking?.orderStatus.filter(
                        (e) => e.statusId === 57
                      ).length > 0
                    }
                  />
                </div>
              )}
            {window.innerWidth > 768 && (
              <div
                className={`${"orderDetails_rightItem"} ${"orderDetails_customBillMargin"} ${
                  renderToStaticMarkup(
                    <SavingsCard
                      updateDetail={this.props.updateDetail}
                      medicineList={this.props.medicineList}
                      isLoading={loader}
                      orderStatus={this.props.orderStatus}
                      isCancelled={
                        this.props.orderStatus?.orderTracking?.orderStatus.filter(
                          (e) => e.statusId === 57
                        ).length > 0
                      }
                    />
                  ) === ""
                    ? "orderDetails_BillDetailsTop"
                    : null
                }`}
              >
                {this.props.orderStatus?.orderTracking?.orderStatus.filter(
                  (e) => e.statusId === 57
                ).length > 0 &&
                this.props.updateDetail?.productSubsMappingList?.length === 0 &&
                this.props.updateDetail?.ImageMasterDto?.length > 0 ? null : (
                  <BillDetails
                    details={true}
                    isOrderStatus={true}
                    updateDetail={this.props.updateDetail}
                    baseDiscount={
                      this.props.masterDetails?.baseDiscount
                        ? Number(this.props.masterDetails.baseDiscount)
                        : 20
                    }
                    deliveryCharge={
                      this.props.masterDetails?.deliveryCharge
                        ? Number(this.props.masterDetails.deliveryCharge)
                        : 0
                    }
                    deliveryOnAmount={
                      this.props.masterDetails?.deliveryOnAmount
                        ? Number(this.props.masterDetails.deliveryOnAmount)
                        : 0
                    }
                    deliveryChargeSubs={
                      this.props.masterDetails?.deliveryChargeSubs
                        ? Number(this.props.masterDetails.deliveryChargeSubs)
                        : 0
                    }
                    deliveryOnAmountSubs={
                      this.props.masterDetails?.deliveryOnAmountSubs
                        ? Number(this.props.masterDetails.deliveryOnAmountSubs)
                        : 0
                    }
                    calcDiscPreProcessing={this.calcDiscPreProcessing}
                    calcTotalPreProcessing={this.calcTotalPreProcessing}
                    getTotalSpForSummary={this.getTotalSpForSummary}
                    checkMedsForSummary={this.checkMedsForSummary}
                    getTotalMrpForSummary={this.getTotalMrpForSummary}
                    getTotalDiscountForSummary={this.getTotalDiscountForSummary}
                    checkForSubs={this.checkForSubs}
                    isLoading={loader}
                    promoCode={this.props.updateDetail?.Promocode}
                    promoCodeValue={
                      this.props.updateDetail?.orderStatus?.serialId === 49 ||
                      this.props.updateDetail?.orderStatus?.serialId === 39 ||
                      this.props.updateDetail?.orderStatus?.serialId === 2
                        ? 0
                        : 0
                    }
                  />
                )}
              </div>
            )}
            {this.props.doctorRatingData?.getDoctorRatingSuccess &&
            window.innerWidth > 768 ? (
              (this.props?.orderStatus?.orderTracking?.orderStatus
                ?.map((e) => e.statusId)
                .includes(142) ||
                this.props?.orderStatus?.orderTracking?.drCallTime) &&
              ![39, 2, 49].includes(
                this.props?.orderStatus?.orderTracking?.statusId
              ) ? (
                <div
                  style={{ border: "none" }}
                  className={`${"orderDetails_rightItem"} `}
                >
                  {this.props.isLoading ? null : (
                    <div className={"orderDetails_doctorTitle"}>
                      Doctor Details
                    </div>
                  )}

                  <DoctorConsultationCard
                    doctorData={
                      this.props.doctorRatingData?.getDoctorRatingSuccess
                    }
                    orderId={this.props.orderId}
                    customerId={this.props.forCustId}
                    serviceCall={(value) => this.handlePostDoctorRating(value)}
                    isLoading={this.props.isLoading}
                    callType={2}
                  />
                </div>
              ) : null
            ) : null}
            {window.innerWidth > 768 ? (
              <ReorderComponent
                updateDetail={this.props.updateDetail}
                orderStatus={this.props.orderStatus}
                reorderHandler={() => this.reorderHandler()}
                returnOrder={() => this.returnOrder()}
                isLoading={loader}
              />
            ) : null}
          </div>
        </div>
      </div>
    );
  }
}

//server calls
MyOrderDetails.getInitialProps = wrapper.getInitialPageProps(
  (store) => async (props) => {
    let orderId = props.query?.id;

    const cookies = parseCookies(props.req);

    const p1 = new Promise((resolve, reject) => {
      store
        .dispatch(
          orderStatusThunk({
            orderIds: orderId,
            customerId: cookies?.customerId,
            access_token: cookies?.token,
            history: Router,
          })
        )
        .then(() => {
          resolve();
        });
    });

    const p2 = new Promise((resolve, reject) => {
      store
        .dispatch(
          fetchPatientByIdThunk({
            accessToken: cookies?.token,
            orderId: orderId,
            patientId: 0,
            history: Router,
          })
        )
        .then(() => {
          resolve();
        });
    });

    const p3 = new Promise((resolve, reject) => {
      store
        .dispatch(
          getDoctorRatingDetailsThunk({
            access_token: cookies?.token,
            orderId: orderId,
            history: Router,
          })
        )
        .then(() => {
          resolve();
        });
    });

    const p4 = new Promise((resolve, reject) => {
      store
        .dispatch(
          fetchOrderStatusThunk({
            accessToken: cookies?.token,
            orderId: orderId,
            history: Router,
          })
        )
        .then(() => {
          resolve();
        });
    });
    const p5 = new Promise((resolve, reject) => {
      store
        .dispatch(
          compareOrderDetailsThunk({
            access_token: cookies?.token,
            orderId: orderId,
            history: Router,
          })
        )
        .then(() => {
          resolve();
        });
    });

    let prop = {};
    await Promise.all([p1, p2, p3, p4, p5]).then(() => {
      prop.accessToken = cookies?.token;
      prop.forCustId = cookies?.customerId;
      prop.custId = cookies?.customerId;
      prop.updateDetail = store.getState().orderStatusUpdate?.OrderStatusData;
      prop.patientData = store.getState().patientData?.fetchPatientByIdSuccess;
      prop.CustomerDto =
        store.getState().loginReducer?.verifyOtpSuccess?.CustomerDto;
      prop.orderStatus = store.getState().myOrder?.orderStatusSuccess;
      prop.orderStatusUpdate = store.getState().orderStatusUpdate;
      prop.compareOrderData =
        store.getState().patientData?.compareOrderDetailsSuccess;
      prop.doctorRatingData = store.getState().doctorRatings;
      prop.isLoading = false;
      prop.orderId = orderId;
      prop.details = store.getState().patientData;
    });
    return { ...prop };
  }
);

let mapStateToProps = (state) => ({
  Stepper: state.stepper,
  accessToken: state.loginReducer?.verifyOtpSuccess?.Response?.access_token,
  forCustId: state.loginReducer?.verifyOtpSuccess?.CustomerId,
  updateDetail: state.orderStatusUpdate?.OrderStatusData,
  patientData: state.patientData?.fetchPatientByIdSuccess,
  masterDetails: state.masterDetails?.MasterDetails,
  CustomerDto: state.loginReducer?.verifyOtpSuccess?.CustomerDto,
  orderStatus: state.myOrder.orderStatusSuccess,
  custId: state.loginReducer?.verifyOtpSuccess?.CustomerDto?.customerId,
  reorder: state.reorderReducer,
  orderStatusUpdate: state.orderStatusUpdate,
  compareOrderData: state.patientData?.compareOrderDetailsSuccess,
  medicineList: state.searchMedicine?.medDataWithImgSuccess?.hits,
  doctorRatingData: state.doctorRatings,
  isLoading: state.loader?.isLoading,
});

export default withRouter(
  connect(mapStateToProps, {
    orderStatusThunk,
    checkPincodeThunk,
    fetchPatientByIdThunk,
    fetchOrderStatusThunk,
    getDoctorRatingDetailsThunk,
    reorderThunk,
    AddIncompleteOrderMedsAction,
    toCartSectionAction,
    compareOrderDetailsThunk,
    getAllCouponsThunk,
    fetchMedicineWithImagesThunk,
  })(MyOrderDetails)
);

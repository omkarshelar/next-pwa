import React from "react";
import { connect } from "react-redux";
import { wrapper } from "../../redux/store";
import Head from "next/head";
import Router, { withRouter } from "next/router";
import dynamic from "next/dynamic";
import Image from "next/image";
// import Select from "react-select";

//action imports
import { FeaturedMedThunk } from "../../redux/FeaturedMedicine/Action";
import {
  addItemCartAction,
  removeItemCartAction,
  removeMedThunk,
} from "../../redux/Cart/Action";
import { fetchMedicineThunk } from "../../redux/SearchMedicine/Action";
import { fetchProductDetailsThunk } from "../../redux/ProductDetails/Action";
import { qtyArrWithout0 } from "../../redux/Cart/Util/CartUtil";
import { setPaymentModeThunk } from "../../redux/MyOrder/Action";
import { changePaymentAction } from "../../redux/Payment/Action";
import { incompleteOrderThunk } from "../../redux/IncompleteOrder/Action";
import { clearMedDetailsAction } from "../../redux/ConfirmMedicine/Action";
import { lastImageClearAction } from "../../redux/UploadImage/Action";
import {
  notOnOrderSummaryAction,
  toOrderSummaryAction,
  toCartSectionAction,
} from "../../redux/Timeline/Action";
import { setCurrentPage } from "../../redux/Pincode/Actions";

//css imports
import {
  Cart,
  CartDetails,
  CartHeader,
  Cartitem,
} from "../../pageComponents/search/MedSearch.style";
import "../../components/styles/MedicineCard.css";
import "../../components/styles/productDetails.css";

//component or helper imports
import optimizeImage from "../../components/Helper/imageHelper";
import { customNotification } from "../../components/Helper/helperFunction";
import ProductDetailsLoader from "../../components/Loaders/ProductDetailsLoader";
import Continue from "../../components/WebComponents/Button/Button";
import Button from "../../components/WebComponents/Button/Button";
import FeaturedMedicine from "../../components/WebComponents/FeaturedMedicine/FeaturedMedicine";
import DoctorShimmer from "../../components/WebComponents/Shimmer/PaymentLoader";
// import CartDetailCard from "../../pageComponents/search/CartDetailCard/CartDetailCard";

//plugin imports
import ImageGallery from "react-image-gallery";
import {
  FacebookShareButton,
  TwitterShareButton,
  WhatsappShareButton,
} from "react-share";
import { HiPlusSm } from "react-icons/hi";
import AndroidSeperator from "../../components/WebComponents/AndroidSeperator/AndroidSeperator";
import ExpandMoreIcon from "@material-ui/icons/ExpandMore";
import Accordion from "@material-ui/core/Accordion";
import AccordionSummary from "@material-ui/core/AccordionSummary";
import AccordionDetails from "@material-ui/core/AccordionDetails";

//imageImports
import share from "../../src/Assets/product-share.svg";
import left from "../../src/Assets/left-arrow.svg";
import right from "../../src/Assets/right-arrow.svg";
import intro from "../../src/Assets/product-intro.svg";
import activity from "../../src/Assets/product-activity.svg";
import uses from "../../src/Assets/product-uses.svg";
import directions from "../../src/Assets/product-directions.svg";
import effects from "../../src/Assets/product-effects.svg";
import faqs from "../../src/Assets/product-faqs.svg";
import productInfo from "../../src/Assets/product-info.svg";
import ribbonIcon from "../../src/Assets/ribbon.png";
import removeIcon from "../../src/Assets/remove.png";
import copyIcon from "../../src/Assets/link.svg";
import closeIcon from "../../src/Assets/close-share.svg";
import tablet from "../../src/Assets/product-tablet.svg";
import syrup from "../../src/Assets/product-syrup.svg";
import injection from "../../src/Assets/product-injection.svg";
import savingsIcon from "../../src/Assets/savings-percent.svg";
import empty from "../../src/Assets/empty-cart.svg";
import whatsappIcon from "../../src/Assets/whatsapp.svg";
import facebookIcon from "../../src/Assets/facebook.svg";
import twitterIcon from "../../src/Assets/twitter.svg";
import orderBanner from "../../src/Assets/adrt.png";

//dynamic Import
const CartDetailCard = dynamic(() =>
  import("../../pageComponents/search/CartDetailCard/CartDetailCard")
);
const DynamicViewer = dynamic(() => import("react-viewer"), { ssr: false });
const Select = dynamic(() => import("react-select"), { ssr: false });
class ProductDetails extends React.Component {
  constructor(props) {
    super(props);
    let medDetails =
      this.props.medicineList && this.props.medicineList.length > 0
        ? this.props.medicineList[0]._source
        : [];
    this.state = {
      allProductDetails:
        this.props.productDetails?.allProductDetails?.MedicineDetails,
      doctorDetails: this.props.productDetails?.allProductDetails?.DoctorList,
      currentMed: {},
      productImages:
        medDetails?.product_image_urls?.split(",")?.length > 0
          ? this.separateImageHandler(medDetails?.product_image_urls.split(","))
          : [],
      productImgModal: false,
      medDetails: medDetails,
      activeImageIndex: 0,
      readMore: false,
      productName: this.props.router?.query?.medName,
      isShare: false,
      medicineList:
        this.props.medicineList && this.props.medicineList.length > 0
          ? this.props.medicineList[0]
          : null,
      isStateLoading: false,
    };
  }

  compareCart = (productCode, cart) => {
    let currentMed = {};
    for (let i in cart) {
      let med = cart[i];
      if (med._source.original_product_code === productCode) {
        currentMed = med;
      }
    }
    return currentMed;
  };

  triggerCheck = (productCode) => {
    let data = {
      medicine: {
        size: 1,
        query: {
          multi_match: {
            query: productCode,
            type: "bool_prefix",
            operator: "and",
            fields: ["original_product_code"],
          },
        },
        post_filter: {
          term: {
            original_is_searchable: "true",
          },
        },
        suggest: {
          skuNameSuggester: {
            text: productCode,
            term: {
              field: "original_product_code",
              sort: "frequency",
              size: 1,
            },
          },
        },
      },
      history: Router,
      warehouseId: this.props.pincodeDetails?.warehouseId
        ? this.props.pincodeDetails.warehouseId
        : 3,
      access_token: this.props.accessToken ? this.props.accessToken : null,
    };
    this.props.fetchMedicineThunk(data);
  };

  getProductCode = (str) => {
    let id = str.split("TM-")[1];
    return ["TM", id].join("-");
  };

  componentDidUpdate(prevProp) {
    if (
      (prevProp.medicineList !== this.props.medicineList &&
        this.props.medicineList?.length > 0 &&
        this.props.medicineList[0]._source &&
        !this.props.medicineListError) ||
      (prevProp.cartContent !== this.props.cartContent &&
        this.props.medicineList &&
        this.props.medicineList?.length > 0)
    ) {
      this.setState(
        {
          medDetails: {
            ...this.state.medDetails,
            ...this.props.medicineList[0]._source,
          },
          medicineList: this.props.medicineList[0],
        },
        () => {
          this.setState({
            currentMed: this.compareCart(
              this.state.medDetails.original_product_code,
              this.props.cartContent
            ),
            productImages:
              this.state.medDetails?.product_image_urls?.split(",")?.length > 0
                ? this.separateImageHandler(
                    this.state.medDetails?.product_image_urls.split(",")
                  )
                : [],
          });
        }
      );
    }
    if (
      prevProp.pincodeDetails?.warehouseId !=
        this.props.pincodeDetails?.warehouseId ||
      (this.props.medId && prevProp.medId !== this.props.medId)
    ) {
      this.setState({ isStateLoading: true });
      // if (this.state.productName) {
      this.triggerCheck(
        this.getProductCode(
          this.props.medId ? this.props.medId : this.props.match?.params?.id
        )
      );
      // }
      this.props
        .fetchProductDetailsThunk({
          productCd: this.getProductCode(
            this.props.medId ? this.props.medId : this.props.match?.params?.id
          ),
          history: Router,
          warehouseId: this.props.pincodeDetails?.warehouseId
            ? this.props.pincodeDetails.warehouseId
            : 3,
        })
        .then(() => {
          this.setState({
            isStateLoading: false,
            allProductDetails:
              this.props.productDetails?.allProductDetails?.MedicineDetails,
            doctorDetails:
              this.props.productDetails?.allProductDetails?.DoctorList,
          });
        });
    }
  }

  getMedNameFromUrl = (url) => {
    let finalUrl = url;
    finalUrl = finalUrl.substr(0, finalUrl.indexOf("-TM"));
    finalUrl = finalUrl.replaceAll("-", " ");
    let arr = finalUrl.split(" ");
    for (var i = 0; i < arr.length; i++) {
      arr[i] = arr[i].charAt(0).toUpperCase() + arr[i].slice(1);
    }
    finalUrl = arr.join(" ");
    return finalUrl;
  };

  componentDidMount = () => {
    this.props.setCurrentPage({ currentPage: "productDetails" });

    if (this.props.medId) {
      this.setState({
        productName: this.getMedNameFromUrl(this.props.medId),
      });
      this.triggerCheck(this.getProductCode(this.props.medId));
      this.props
        .fetchProductDetailsThunk({
          productCd: this.getProductCode(this.props.medId),
          history: Router,
          warehouseId: this.props.pincodeDetails?.warehouseId
            ? this.props.pincodeDetails.warehouseId
            : 3,
        })
        .then(() => {
          this.setState({
            allProductDetails:
              this.props.productDetails?.allProductDetails?.MedicineDetails,
            doctorDetails:
              this.props.productDetails?.allProductDetails?.DoctorList,
          });
        });
    }
  };

  separateImageHandler = (urlArray) => {
    let temparray = [];
    if (urlArray?.length === 1) {
      temparray = urlArray;
    } else {
      let one = urlArray.find((data) => {
        let lastIndexOfSlash = data.lastIndexOf("/");
        return data.substring(lastIndexOfSlash + 1).includes("_1");
      });

      let two = urlArray.find((data) => {
        let lastIndexOfSlash = data.lastIndexOf("/");
        return data.substring(lastIndexOfSlash + 1).includes("_2");
      });
      let three = urlArray.find((data) => {
        let lastIndexOfSlash = data.lastIndexOf("/");
        return data.substring(lastIndexOfSlash + 1).includes("_3");
      });
      let four = urlArray.find((data) => {
        let lastIndexOfSlash = data.lastIndexOf("/");
        return data.substring(lastIndexOfSlash + 1).includes("_4");
      });
      if (one) {
        temparray.push(one);
      }
      if (two) {
        temparray.push(two);
      }
      if (three) {
        temparray.push(three);
      }
      if (four) {
        temparray.push(four);
      }
    }
    return temparray;
  };

  convertImages = (imageArr) => {
    let finalImageArr = [];
    for (let i in imageArr) {
      let image = imageArr[i];
      finalImageArr.push({
        original: optimizeImage(image, "250"),
        thumbnail: optimizeImage(image, "50"),
      });
    }
    return finalImageArr;
  };

  checkMedType = (type) => {
    let img = tablet;
    if (type === "SYRUP") {
      img = syrup;
    } else if (type === "INJECTION") {
      img = injection;
    }
    return img;
  };

  capitalizeFirstLetter = (str) => {
    var splitStr = str ? str.toLowerCase().split(" ") : "";
    for (var i = 0; i < splitStr.length; i++) {
      splitStr[i] =
        splitStr[i].charAt(0).toUpperCase() + splitStr[i].substring(1);
    }
    return splitStr.join(" ");
  };

  removeMedOnClick = async (data) => {
    if (this.props.medConfirm?.ConfirmMedData?.orderId) {
      await this.props.removeMedThunk({
        orderId: this.props.medConfirm.ConfirmMedData.orderId,
        access_token: this.props.accessToken,
        medData: data,
        history: Router,
      });
    } else if (this.props.imgUpload?.orderId) {
      await this.props.removeMedThunk({
        orderId: this.props.imgUpload.orderId,
        access_token: this.props.accessToken,
        medData: data,
        history: Router,
      });
    }
  };

  addNewQuantity = (obj, qty) => {
    obj.newQty = Number(qty);
    return obj;
  };

  addToCart = () => {
    this.props
      .addItemCartAction(this.addNewQuantity(this.state.medicineList, 1))
      .then(() => {
        // eventPdPageAddToCart();
      });
  };
  setmaxQty = (arr, maxQty) => {
    let newArr = [];
    for (let i in arr) {
      if (arr[i] <= maxQty) {
        newArr.push(Number(arr[i]));
      }
    }
    return newArr;
  };

  quantityHandleChange = (qty) => {
    this.setState({
      currentMed: { ...this.state.currentMed, newQty: qty.value },
    });
    this.props.addItemCartAction(
      this.addNewQuantity(this.state.currentMed, qty.value)
    );
  };

  getPaymentIdFromType = (type) => {
    // setting payment id based on the paymnet type we get from coupon details
    let paymentId = this.props.payment.isOnline ? 16 : 17;
    if (type === "Cod Payment") {
      paymentId = 17;
      this.onClickSelectPaymentType(false);
    } else if (type === "Online Payment") {
      paymentId = 16;
      this.onClickSelectPaymentType(true);
    }
    return paymentId;
  };

  onClickSelectPaymentType = (isOnline) => {
    if (!this.props.appliedCoupon) {
      this.props.changePaymentAction(isOnline);
    } else {
      if (
        this.props.appliedCoupon.applicableOn === "Online Payment" &&
        !isOnline
      ) {
        customNotification(
          "Only Online payment allowed with selected Coupon",
          "customNotify"
        );
      } else if (
        this.props.appliedCoupon.applicableOn === "Cod Payment" &&
        isOnline
      ) {
        customNotification(
          "Only COD payment allowed with selected Coupon",
          "customNotify"
        );
      } else {
        this.props.changePaymentAction(isOnline);
      }
    }
  };

  medDataList = () => {
    let medData = [];

    if (this.props.cartContent) {
      this.props.cartContent.map((e) => {
        medData.push({
          productCode: e._source.original_product_code,
          quantity: e.quantity,
        });
      });
    }

    return medData;
  };

  proceedOrder = async () => {
    if (this.props.medConfirm?.ConfirmMedData?.orderId) {
      // if coupon is applied, set payment mode
      if (this.props.appliedCoupon?.offerId) {
        await this.props.setPaymentModeThunk({
          orderId: this.props.medConfirm.ConfirmMedData.orderId,
          access_token: this.props.accessToken,
          paymentId: this.getPaymentIdFromType(
            this.props.appliedCoupon.applicableOn
          ),
        });
      }
      this.props
        .incompleteOrderThunk({
          orderIds: this.props.medConfirm.ConfirmMedData.orderId,
          accessToken: this.props.accessToken,
          medSet: this.medDataList(),
          offerId: this.props.appliedCoupon?.offerId
            ? this.props.appliedCoupon.offerId
            : 0,
          history: Router,
          pincode: this.props.pincodeDetails.pincode,
        })
        .then(() => {
          if (this.props.medIncomplete.incompleteOrderError) {
            if (
              this.props.medIncomplete.incompleteOrderError ===
              "Sorry order cannot be confirmed. Please contact customer support."
            ) {
              this.props.clearMedDetailsAction().then(() => {
                this.props.lastImageClearAction().then(() => {
                  this.props.notOnOrderSummaryAction();
                });
              });
            } else {
              message.error(this.props.medIncomplete.incompleteOrderError);
            }
          } else {
            this.props.toOrderSummaryAction();
            Router.push("/orderflow");
          }
        });
    } else if (this.props.imgUpload?.orderId) {
      // if coupon is applied, set payment mode
      if (this.props.appliedCoupon?.offerId) {
        await this.props.setPaymentModeThunk({
          orderId: this.props.imgUpload.orderId,
          access_token: this.props.accessToken,
          paymentId: this.getPaymentIdFromType(
            this.props.appliedCoupon.applicableOn
          ),
        });
      }
      this.props
        .incompleteOrderThunk({
          orderIds: this.props.imgUpload.orderId,
          accessToken: this.props.accessToken,
          medSet: this.medDataList(),
          offerId: this.props.appliedCoupon?.offerId
            ? this.props.appliedCoupon.offerId
            : 0,
          history: Router,
          pincode: this.props.pincodeDetails.pincode,
        })
        .then(() => {
          if (this.props.medIncomplete.incompleteOrderError) {
            if (
              this.props.medIncomplete.incompleteOrderError ===
              "Sorry order cannot be confirmed. Please contact customer support."
            ) {
              this.props.clearMedDetailsAction().then(() => {
                this.props.lastImageClearAction().then(() => {
                  this.props.notOnOrderSummaryAction();
                  // this.setState({ duplicateOrderId: true });
                });
              });
            } else {
              message.error(this.props.medIncomplete.incompleteOrderError);
            }
          } else {
            this.props.toOrderSummaryAction();
            Router.push("/orderflow");
          }
        });
    } else {
      this.props.toCartSectionAction();
      Router.push("/orderflow");
    }
  };

  continueBtn = () => {
    if (this.props.Stepper.toOrderSummary) {
      this.proceedOrder();
    } else {
      this.props.toCartSectionAction();
      Router.push("/orderflow");
    }
  };

  convertToFullScreenImages = (imgArr) => {
    let fullScreenImg = [];
    for (let i in imgArr) {
      let img = imgArr[i];
      fullScreenImg.push({
        src: optimizeImage(img),
        alt: this.state.medDetails?.original_sku_name,
      });
    }
    return fullScreenImg;
  };

  render() {
    // console.log(this.props, "Product Details");
    return (
      <>
        <Head>
          <title>{this.state.medDetails.original_sku_name}</title>
          <meta
            name="description"
            content={this.state.medDetails.original_sku_name}
          ></meta>
          <meta property="og:locale" content="en_US" />
          <meta
            property="og:title"
            content={this.state.medDetails.original_sku_name}
          />
          <meta
            property="og:description"
            content={this.state.medDetails.original_sku_name}
          />
          <meta property="og:url" content="https://truemeds.in/" />
          <meta property="og:site_name" content="Truemeds" />
          {this.state.productImages.length > 0 ? (
            <>
              <meta property="og:image" content={this.state.productImages[0]} />
              <meta
                name="twitter:image"
                content={this.state.productImages[0]}
              />
            </>
          ) : null}
          <meta name="twitter:card" content="summary_large_image" />
          <meta
            name="twitter:title"
            content={this.state.medDetails.original_sku_name}
          />
          <meta
            name="twitter:description"
            content={this.state.medDetails.original_sku_name}
          />
        </Head>
        {this.state.productImgModal && typeof window !== undefined ? (
          <DynamicViewer
            visible={this.state.productImgModal}
            onClose={() => this.setState({ productImgModal: false })}
            images={this.convertToFullScreenImages(this.state.productImages)}
            rotatable={false}
            scalable={false}
            noImgDetails={true}
            showTotal={false}
            minScale="1"
            maxScale="3"
            zoomSpeed="0.5"
            zIndex="10000"
            drag={false}
            activeIndex={this.state.activeImageIndex}
            disableMouseZoom={true}
          />
        ) : (
          ""
        )}

        <div>
          <div
            style={{
              width: this.props.medId ? "100%" : "",
              padding: this.props.medId ? "16px" : "",
              margin: this.props.medId ? "0" : "",
              flexDirection: this.props.medId ? "column" : "",
            }}
            className="productDetailsMainContainer"
          >
            {this.state.isStateLoading ||
            (this.props.isLoading &&
              !this.state.medDetails?.original_sku_name) ? (
              <div style={{ width: "100%" }}>
                <ProductDetailsLoader />
              </div>
            ) : (
              <div
                style={{
                  flexDirection: this.props.medId ? "column" : "",
                  width: this.props.medId ? "100%" : "",
                  maxWidth: this.props.medId ? "100%" : "",
                }}
                className="productDetailsFirstContainer"
              >
                {this.props.medId
                  ? null
                  : this.state.medDetails?.original_sku_name && (
                      <div className="productDetailsBreadcrumbs">
                        <a href="/">Home</a>
                        <span>
                          {" "}
                          / {this.state.medDetails.original_sku_name}
                        </span>
                      </div>
                    )}
                {this.state.medDetails?.original_sku_name && (
                  <div
                    style={{
                      flexDirection: this.props.medId ? "column" : "",
                      paddingBottom: this.props.viewOriginal ? "0" : "",
                    }}
                    className="productDetailsSlideShowContainer"
                    id={
                      this.props.medId
                        ? "productDetailsSlideShowContainerId"
                        : ""
                    }
                  >
                    {this.state.productImages?.length > 0 ? (
                      <ImageGallery
                        style={{ width: this.props.medId ? "100%" : "" }}
                        items={this.convertImages(this.state.productImages)}
                        showPlayButton={false}
                        useBrowserFullscreen={false}
                        showFullscreenButton={false}
                        renderLeftNav={(onClick, disabled) => {
                          return (
                            <img
                              className="productDetailsLeftArrow"
                              src={left}
                              alt="left-arrow"
                              disabled={disabled}
                              onClick={onClick}
                            />
                          );
                        }}
                        renderRightNav={(onClick, disabled) => {
                          return (
                            <img
                              className="productDetailsRightArrow"
                              src={right}
                              alt="right-arrow"
                              disabled={disabled}
                              onClick={onClick}
                            />
                          );
                        }}
                        onClick={() => this.setState({ productImgModal: true })}
                        onThumbnailClick={(event, index) =>
                          this.setState({ activeImageIndex: index })
                        }
                        onSlide={(currentIndex) =>
                          this.setState({ activeImageIndex: currentIndex })
                        }
                      />
                    ) : (
                      <img
                        style={{ display: this.props.medId ? "none" : "" }}
                        className="productDetailsStockImg"
                        src={this.checkMedType(
                          this.state.medDetails?.original_drug_type
                        )}
                        alt="product-image"
                      />
                    )}
                    <div
                      style={{ width: this.props.medId ? "100%" : "" }}
                      className="productDetailsMedicineContainer"
                    >
                      <div className="productDetailsMedicineFirstContainer">
                        <h1>{this.state.medDetails.original_sku_name}</h1>
                        <Image
                          src={this.state.isShare ? closeIcon : share}
                          alt="share"
                          onClick={() =>
                            this.setState({ isShare: !this.state.isShare })
                          }
                          width={30}
                          height={30}
                        />
                        {this.state.isShare && (
                          <div className="productDetailsShareOptions">
                            <Image
                              src={copyIcon}
                              alt="copy"
                              onClick={() => {
                                if (navigator?.clipboard) {
                                  navigator.clipboard.writeText(
                                    this.props.hostName +
                                      (this.props.medId
                                        ? "/medicine/" + this.props.medId
                                        : this.props.router?.asPath)
                                  );
                                  customNotification("Copied");
                                }
                              }}
                              width={30}
                              height={30}
                            />
                            <WhatsappShareButton
                              url={
                                this.props.hostName +
                                (this.props.medId
                                  ? "/medicine/" + this.props.medId
                                  : this.props.router?.asPath)
                              }
                              title={`${this.state.productName} - Uses, Side Effects, Dosage, Price | Truemeds`}
                            >
                              <Image
                                width={30}
                                height={30}
                                src={whatsappIcon}
                                alt="whatsapp"
                              />
                            </WhatsappShareButton>
                            <FacebookShareButton
                              url={
                                this.props.hostName +
                                (this.props.medId
                                  ? "/medicine/" + this.props.medId
                                  : this.props.router?.asPath)
                              }
                              title={`${this.state.productName} - Uses, Side Effects, Dosage, Price | Truemeds`}
                            >
                              <Image
                                width={30}
                                height={30}
                                src={facebookIcon}
                                alt="facebook"
                              />
                            </FacebookShareButton>
                            <TwitterShareButton
                              url={
                                this.props.hostName +
                                (this.props.medId
                                  ? "/medicine/" + this.props.medId
                                  : this.props.router?.asPath)
                              }
                              title={`${this.state.productName} - Uses, Side Effects, Dosage, Price | Truemeds`}
                            >
                              <Image
                                width={30}
                                height={30}
                                src={twitterIcon}
                                alt="twitter"
                              />
                            </TwitterShareButton>
                          </div>
                        )}
                      </div>
                      <div className="productDetailsOthersSection">
                        {this.state.medDetails?.original_company_nm && (
                          <div className="productDetailsOthersContainer">
                            <span>Manufacturer</span>
                            <span>
                              {this.state.medDetails.original_company_nm}
                            </span>
                          </div>
                        )}
                        {this.state.medDetails?.original_country_nm && (
                          <div className="productDetailsOthersContainer">
                            <span>Country Of Origin</span>
                            <span>
                              {this.capitalizeFirstLetter(
                                this.state.medDetails.original_country_nm
                              )}
                            </span>
                          </div>
                        )}
                      </div>
                      {this.state.medDetails?.original_composition && (
                        <div className="productDetailsOthersContainer">
                          <span>Salt Composition</span>
                          {this.state.medDetails.original_composition.length <
                          80 ? (
                            <span>
                              {this.capitalizeFirstLetter(
                                this.state.medDetails.original_composition
                              )}
                            </span>
                          ) : (
                            <>
                              <span>
                                {this.state.readMore
                                  ? this.capitalizeFirstLetter(
                                      this.state.medDetails.original_composition
                                    )
                                  : this.capitalizeFirstLetter(
                                      this.state.medDetails.original_composition
                                    ).slice(0, 80) + "..."}
                              </span>
                              <span
                                className="productDetailsReadMore"
                                onClick={() =>
                                  this.setState({
                                    readMore: !this.state.readMore,
                                  })
                                }
                              >
                                {this.state.readMore
                                  ? "Read Less"
                                  : "Read More"}
                              </span>
                            </>
                          )}
                        </div>
                      )}
                      <div className="productDetailsPriceContainer">
                        {this.state.medDetails.original_pack &&
                          this.state.medDetails.original_unit && (
                            <span className="productDetailsPriceContainerSize">
                              Pack: {this.state.medDetails.original_pack}{" "}
                              {this.state.medDetails.original_unit}
                            </span>
                          )}
                        <div className="productDetailsPriceSection">
                          <span>
                            ₹
                            {this.props.router?.query?.subsSp
                              ? this.props.router?.query?.subsSp
                              : this.props.subsDetails?.subsSp
                              ? this.props.subsDetails?.subsSp
                              : this.state.medDetails?.original_product_code ===
                                this.state.medDetails?.subs_product_code
                              ? (this.props.medId && this.props.medQty
                                  ? this.state.medDetails?.subs_selling_price *
                                    this.props.medQty
                                  : this.state.medDetails?.subs_selling_price
                                ).toFixed(2)
                              : (this.props.medId && this.props.medQty
                                  ? (this.state.medDetails?.original_mrp -
                                      this.state.medDetails?.original_mrp *
                                        (this.state.medDetails
                                          ?.original_base_discount /
                                          100)) *
                                    this.props.medQty
                                  : this.state.medDetails?.original_mrp -
                                    this.state.medDetails?.original_mrp *
                                      (this.state.medDetails
                                        ?.original_base_discount /
                                        100)
                                ).toFixed(2)}
                          </span>
                          <span>
                            MRP{" "}
                            <del>
                              ₹
                              {this.props.router?.query?.subsMrp
                                ? this.props.router?.query?.subsMrp
                                : this.props.subsDetails?.subsMrp
                                ? this.props.subsDetails?.subsMrp
                                : (this.props.medId && this.props.medQty
                                    ? this.state.medDetails?.original_mrp *
                                      this.props.medQty
                                    : this.state.medDetails?.original_mrp
                                  ).toFixed(2)}
                            </del>
                          </span>
                          <div className="productDetailsRibbonDiscountContainer">
                            <img src={ribbonIcon} alt="ribbon" />
                            <span style={{ fontWeight: "600" }}>
                              {this.props.router?.query?.subsDiscount
                                ? Math.round(
                                    this.props.router?.query?.subsDiscount
                                  )
                                : this.props.subsDetails?.subsDiscount
                                ? this.props.subsDetails?.subsDiscount
                                : this.state.medDetails
                                    ?.original_product_code !==
                                  this.state.medDetails?.subs_product_code
                                ? this.state.medDetails?.original_base_discount
                                : Math.round(
                                    ((this.state.medDetails?.original_mrp -
                                      this.state.medDetails
                                        ?.subs_selling_price) *
                                      100) /
                                      this.state.medDetails?.original_mrp
                                  )}
                              % OFF
                            </span>
                          </div>
                        </div>
                        <span className="productDetailsPriceInclusive">
                          *MRP inclusive of all taxes
                        </span>
                        {this.props.medQty && (
                          <div
                            style={{
                              border: this.props.viewOriginal ? "none" : "",
                              padding: this.props.viewOriginal ? "0" : "",
                            }}
                            className="productDetailsSidePanelQty"
                          >
                            {`Qty: ${this.props.medQty}`}
                          </div>
                        )}
                      </div>
                      {this.props.medId ? null : (
                        <>
                          {this.props.router?.query?.isSubs ? (
                            <div
                              style={{
                                width: "100%",
                              }}
                              className="productDetailsButtonContainer"
                            >
                              <span
                                style={{ color: "#333", fontStyle: "italic" }}
                              >
                                This has been added as a recommendation to our
                                doctor, confirm the same during the DR call.
                              </span>
                            </div>
                          ) : this.state.currentMed?.quantity ? (
                            <div className="productDetailsQuantityContainer">
                              <div className="medCardQuantityContainer">
                                <Select
                                  placeholder="QTY"
                                  value={{
                                    label: this.state.currentMed?.quantity,
                                    value: this.state.currentMed?.quantity,
                                  }}
                                  onChange={this.quantityHandleChange}
                                  options={this.setmaxQty(
                                    qtyArrWithout0,
                                    this.state.medDetails?.max_capped_qty
                                      ? this.state.medDetails?.max_capped_qty
                                      : 20
                                  ).map((qty) => {
                                    return {
                                      value: qty,
                                      label: qty,
                                    };
                                  })}
                                  isSearchable={false}
                                />
                              </div>
                              <div
                                onClick={() => {
                                  let data = [
                                    {
                                      medicineName:
                                        this.state.medDetails
                                          ?.original_sku_name,
                                      medicineQty: "0",
                                      medicineId:
                                        this.state.medDetails
                                          ?.original_product_code,
                                    },
                                  ];
                                  this.removeMedOnClick(data);
                                  this.props.removeItemCartAction(
                                    this.state.currentMed
                                  );
                                }}
                                className="medCardRemoveContainer"
                              >
                                <img src={removeIcon} alt="remove" />
                                <span>Remove</span>
                              </div>
                            </div>
                          ) : (
                            <div
                              // style={{
                              //   width: this.props.router?.query?.isSubs
                              //     ? "100%"
                              //     : "",
                              // }}
                              className="productDetailsButtonContainer"
                            >
                              {
                                // this.props.router?.query?.isSubs ? (
                                //   <span style={{ color: "#333", fontStyle: "italic" }}>
                                //     This has been added as a recommendation to our
                                //     doctor, confirm the same during the DR call.
                                //   </span>
                                // ) :
                                !this.state.medDetails?.original_available &&
                                !this.state.medDetails?.subs_found ? (
                                  <span>Not In Stock</span>
                                ) : this.state.medDetails
                                    ?.original_supplied_bytm ? (
                                  <Button
                                    ProductAdd
                                    onClick={() => this.addToCart()}
                                    id={"addtocart"}
                                  >
                                    <HiPlusSm className="plusIcon" />
                                    Add To Cart
                                  </Button>
                                ) : (
                                  <span>Not sold by Truemeds</span>
                                )
                              }
                            </div>
                          )}
                        </>
                      )}
                    </div>
                  </div>
                )}
                {this.props.viewOriginal && this.props.medSavings && (
                  <div className="pdSaveCardDesWrap">
                    <AndroidSeperator showWeb={true} showMob={false} />
                    <div className="pdSaveCard">
                      <img src={savingsIcon} />
                      <div>
                        <div>
                          <span>You saved</span>
                          <span>{this.props.medSavings}%</span>
                        </div>
                        <span>
                          By going with truemeds recommended medicine instead of
                          this item
                        </span>
                      </div>
                    </div>
                    <AndroidSeperator showWeb={true} showMob={false} />
                  </div>
                )}
                {this.props.isMobile && (
                  <div className="productDetailsMobileContainer">
                    {this.state.allProductDetails?.medIntroduction && (
                      <>
                        <Accordion>
                          <AccordionSummary
                            expandIcon={<ExpandMoreIcon />}
                            aria-controls="panel1a-content"
                            id="panel1a-header"
                          >
                            <div className="productDetailsMobile">
                              <img
                                className="productDetailsImg"
                                src={intro}
                                alt="product-introduction"
                              />
                              <span>Introduction</span>
                            </div>
                          </AccordionSummary>
                          <AccordionDetails>
                            <Description
                              details={
                                this.state.allProductDetails?.medIntroduction
                              }
                            />
                          </AccordionDetails>
                        </Accordion>
                        <hr />
                      </>
                    )}
                    {this.state.allProductDetails?.medActivity && (
                      <>
                        <Accordion>
                          <AccordionSummary
                            expandIcon={<ExpandMoreIcon />}
                            aria-controls="panel1a-content"
                            id="panel1a-header"
                          >
                            <div className="productDetailsMobile">
                              <img
                                className="productDetailsImg"
                                src={activity}
                                alt="product-activity"
                              />
                              <span>Medicine Activity</span>
                            </div>
                          </AccordionSummary>
                          <AccordionDetails>
                            <Description
                              details={this.state.allProductDetails.medActivity}
                            />
                          </AccordionDetails>
                        </Accordion>
                        <hr />
                      </>
                    )}
                    {this.state.allProductDetails?.medUses && (
                      <>
                        <Accordion>
                          <AccordionSummary
                            expandIcon={<ExpandMoreIcon />}
                            aria-controls="panel1a-content"
                            id="panel1a-header"
                          >
                            <div className="productDetailsMobile">
                              <img
                                className="productDetailsImg"
                                src={uses}
                                alt="product-uses"
                              />
                              <span>Uses</span>
                            </div>
                          </AccordionSummary>
                          <AccordionDetails>
                            <Description
                              details={this.state.allProductDetails.medUses}
                            />
                          </AccordionDetails>
                        </Accordion>
                        <hr />
                      </>
                    )}
                    {this.state.allProductDetails?.medDirectionForUse && (
                      <>
                        <Accordion>
                          <AccordionSummary
                            expandIcon={<ExpandMoreIcon />}
                            aria-controls="panel1a-content"
                            id="panel1a-header"
                          >
                            <div className="productDetailsMobile">
                              <img
                                className="productDetailsImg"
                                src={directions}
                                alt="product-directions-for-use"
                              />
                              <span>Directions for use</span>
                            </div>
                          </AccordionSummary>
                          <AccordionDetails>
                            <Description
                              details={
                                this.state.allProductDetails.medDirectionForUse
                              }
                            />
                          </AccordionDetails>
                        </Accordion>
                        <hr />
                      </>
                    )}
                    {this.state.allProductDetails?.medSideEffects && (
                      <>
                        <Accordion>
                          <AccordionSummary
                            expandIcon={<ExpandMoreIcon />}
                            aria-controls="panel1a-content"
                            id="panel1a-header"
                          >
                            <div className="productDetailsMobile">
                              <img
                                className="productDetailsImg"
                                src={effects}
                                alt="product-side-effects"
                              />
                              <span>Side effects</span>
                            </div>
                          </AccordionSummary>
                          <AccordionDetails>
                            <Description
                              details={
                                this.state.allProductDetails.medSideEffects
                              }
                            />
                          </AccordionDetails>
                        </Accordion>
                        <hr />
                      </>
                    )}
                    {this.state.allProductDetails?.medFaqs && (
                      <>
                        <Accordion>
                          <AccordionSummary
                            expandIcon={<ExpandMoreIcon />}
                            aria-controls="panel1a-content"
                            id="panel1a-header"
                          >
                            <div className="productDetailsMobile">
                              <img
                                className="productDetailsImg"
                                src={faqs}
                                alt="product-faqs"
                              />
                              <span>FAQ’s</span>
                            </div>
                          </AccordionSummary>
                          <AccordionDetails>
                            <Description
                              details={this.state.allProductDetails.medFaqs}
                            />
                          </AccordionDetails>
                        </Accordion>
                        <hr />
                      </>
                    )}
                  </div>
                )}
                {/* Only for desktop & tablets */}
                {this.props.medId
                  ? null
                  : !this.props.isMobile && (
                      <div className="productDetailsDesktopContainer">
                        {this.state.allProductDetails?.medIntroduction &&
                          this.state.allProductDetails?.medIntroduction !=
                            "<p><br></p>" && (
                            <div className="productDetailsContainer">
                              <img
                                className="productDetailsImg"
                                src={intro}
                                alt="product-introduction"
                              />
                              <div className="productDetailsInsideContainer">
                                <span>Introduction</span>

                                <Description
                                  details={
                                    this.state.allProductDetails.medIntroduction
                                  }
                                />
                              </div>
                            </div>
                          )}
                        {this.state.allProductDetails?.medActivity &&
                          this.state.allProductDetails?.medActivity !=
                            "<p><br></p>" && (
                            <div className="productDetailsContainer">
                              <img
                                className="productDetailsImg"
                                src={activity}
                                alt="product-activity"
                              />
                              <div className="productDetailsInsideContainer">
                                <span>Medicine Activity</span>
                                <Description
                                  details={
                                    this.state.allProductDetails.medActivity
                                  }
                                />
                              </div>
                            </div>
                          )}
                        {this.state.allProductDetails?.medUses &&
                          this.state.allProductDetails?.medUses !=
                            "<p><br></p>" && (
                            <div className="productDetailsContainer">
                              <img
                                className="productDetailsImg"
                                src={uses}
                                alt="product-uses"
                              />
                              <div className="productDetailsInsideContainer">
                                <span>Uses</span>
                                <Description
                                  details={this.state.allProductDetails.medUses}
                                />
                              </div>
                            </div>
                          )}
                        {this.state.allProductDetails?.medDirectionForUse &&
                          this.state.allProductDetails?.medDirectionForUse !=
                            "<p><br></p>" && (
                            <div className="productDetailsContainer">
                              <img
                                className="productDetailsImg"
                                src={directions}
                                alt="product-directions-for-use"
                              />
                              <div className="productDetailsInsideContainer">
                                <span>Directions for use</span>
                                <Description
                                  details={
                                    this.state.allProductDetails
                                      .medDirectionForUse
                                  }
                                />
                              </div>
                            </div>
                          )}
                        {this.state.allProductDetails?.medSideEffects &&
                          this.state.allProductDetails?.medSideEffects !=
                            "<p><br></p>" && (
                            <div className="productDetailsContainer">
                              <img
                                className="productDetailsImg"
                                src={effects}
                                alt="product-side-effects"
                              />
                              <div className="productDetailsInsideContainer">
                                <span>Side effects</span>
                                <Description
                                  details={
                                    this.state.allProductDetails.medSideEffects
                                  }
                                />
                              </div>
                            </div>
                          )}
                        {this.state.allProductDetails?.medFaqs &&
                          this.state.allProductDetails?.medFaqs !=
                            "<p><br></p>" && (
                            <div className="productDetailsContainer">
                              <img
                                className="productDetailsImg"
                                src={faqs}
                                alt="product-faqs"
                              />
                              <div className="productDetailsInsideContainer">
                                <span>FAQ’s</span>
                                <Description
                                  details={this.state.allProductDetails.medFaqs}
                                />
                              </div>
                            </div>
                          )}
                        {this.state.medDetails?.original_company_addr && (
                          <div className="productDetailsContainer">
                            <img
                              className="productDetailsImg"
                              src={productInfo}
                              alt="aditional-information"
                            />
                            <div className="productDetailsInsideContainer">
                              <span>Additional Information</span>
                              <span>
                                Manufacturer address:{" "}
                                {this.state.medDetails.original_company_addr}
                              </span>
                            </div>
                          </div>
                        )}
                      </div>
                    )}
              </div>
            )}
            <div
              style={{ margin: this.props.medId ? "0" : "" }}
              className="productDetailsSecondContainer"
            >
              {this.props.medId ? null : (
                <Cart style={{ margin: "0px" }}>
                  <Cartitem>
                    <CartHeader>
                      Cart Details{" "}
                      <span>
                        {this.props.cartContent &&
                          this.props.cartContent.length > 0 &&
                          (this.props.cartContent?.length === 1
                            ? "(1 Item added)"
                            : `(${this.props.cartContent.length} Items added)`)}
                      </span>
                    </CartHeader>
                    <CartDetails>
                      {this.props.cartContent.length > 0 ? (
                        this.props.cartContent.map((content, index) => (
                          <CartDetailCard
                            content={content}
                            key={content._id}
                            lastIndex={
                              this.props.cartContent.length !== index + 1
                            }
                          />
                        ))
                      ) : (
                        <img
                          src={empty}
                          className="empty-cart"
                          alt="empty-cart"
                        />
                      )}
                    </CartDetails>
                  </Cartitem>
                  {this.props.cartContent.length > 0 && (
                    <div className="productDetailsButton">
                      <Continue
                        BtnContinue
                        onClick={() => {
                          this.continueBtn();
                        }}
                      >
                        Continue
                      </Continue>
                    </div>
                  )}
                </Cart>
              )}
              {this.props.medId
                ? null
                : this.props.productDetails?.allProductDetails
                    ?.MedicineDetails && (
                    <img
                      id="addMedBtn"
                      className="orderBanner"
                      src={orderBanner}
                      alt="order-medicines-banner"
                      onClick={() => Router.push("/?search=true")}
                    />
                  )}
              {this.props.isLoading ? (
                <DoctorShimmer />
              ) : (
                this.state.doctorDetails &&
                this.state.doctorDetails.length > 0 && (
                  <div className="productDetailsDoctorsMainSection">
                    {this.state.doctorDetails[1] &&
                      this.state.doctorDetails[1].doctorName && (
                        <div className="productDetailsDoctorsSection">
                          <img
                            src={optimizeImage(
                              this.state.doctorDetails[1].image,
                              "60"
                            )}
                            alt="doctor"
                          />
                          <div>
                            <span>Written By</span>
                            <span>
                              {this.state.doctorDetails[1].doctorName}
                            </span>
                            <span>
                              {this.state.doctorDetails[1].specialist.map(
                                (data) => data + " "
                              ) +
                                "| " +
                                this.state.doctorDetails[1].experience +
                                " " +
                                this.state.doctorDetails[1].qualification.map(
                                  (data) => data + " "
                                )}
                            </span>
                          </div>
                        </div>
                      )}
                    {this.state.doctorDetails[0] &&
                      this.state.doctorDetails[0].doctorName && (
                        <div className="productDetailsDoctorsSection">
                          <img
                            src={optimizeImage(
                              this.state.doctorDetails[0].image,
                              "60"
                            )}
                            alt="doctor"
                          />
                          <div>
                            <span>Reviewed By</span>
                            <span>
                              {this.state.doctorDetails[0].doctorName}
                            </span>
                            <span>
                              {this.state.doctorDetails[0].specialist.map(
                                (data) => data + " "
                              ) +
                                "| " +
                                this.state.doctorDetails[0].experience +
                                " " +
                                this.state.doctorDetails[0].qualification.map(
                                  (data) => data + " "
                                )}
                            </span>
                          </div>
                        </div>
                      )}
                  </div>
                )
              )}
            </div>
            {/* Only for mobiles */}
            {this.state.medDetails?.original_company_addr &&
              (this.props.isMobile || this.props.medId) && (
                <div className="additionalDetailsMobile">
                  <Accordion>
                    <AccordionSummary
                      expandIcon={<ExpandMoreIcon />}
                      aria-controls="panel1a-content"
                      id="panel1a-header"
                    >
                      <div className="productDetailsMobile">
                        <img
                          className="productDetailsImg"
                          src={productInfo}
                          alt="aditional-information"
                        />
                        <span>View Additional Information</span>
                      </div>
                    </AccordionSummary>
                    <AccordionDetails>
                      <span>
                        Manufacturer address:{" "}
                        {this.state.medDetails.original_company_addr}
                      </span>
                    </AccordionDetails>
                  </Accordion>
                </div>
              )}
          </div>
          {this.props.medId ? null : (
            <div className="productDetailsFeaturedMeds">
              <FeaturedMedicine
                isMobile={this.props.isMobile}
                SuggestMedicine={this.props.SuggestMedicine}
                router={this.props.router}
              />
            </div>
          )}
          {this.props.medId
            ? null
            : this.props.cartContent.length > 0 && (
                <div className="productDetailsButtonsMainContainer">
                  <Button
                    BtnContinue
                    onClick={() => {
                      this.continueBtn();
                    }}
                  >
                    Continue
                  </Button>
                </div>
              )}
        </div>
      </>
    );
  }
}

//server calls
ProductDetails.getInitialProps = wrapper.getInitialPageProps(
  (store) => async (props) => {
    let id = props.query.id;

    id = id?.split("TM-")[1];
    id = ["TM", id].join("-");

    const p1 = new Promise((resolve, reject) => {
      store
        .dispatch(fetchProductDetailsThunk({ productCd: id, warehouseId: 3 }))
        .then(() => {
          resolve();
        });
    });

    let data = {
      medicine: {
        size: 1,
        query: {
          multi_match: {
            query: id,
            type: "bool_prefix",
            operator: "and",
            fields: ["original_product_code"],
          },
        },
        post_filter: {
          term: {
            original_is_searchable: "true",
          },
        },
        suggest: {
          skuNameSuggester: {
            text: id,
            term: {
              field: "original_product_code",
              sort: "frequency",
              size: 1,
            },
          },
        },
      },
      warehouseId: store.getState().pincodeDetails?.warehouseId
        ? store.getState().pincodeDetails.warehouseId
        : 3,
    };

    const p2 = new Promise((resolve, reject) => {
      store.dispatch(fetchMedicineThunk(data)).then(() => {
        resolve();
      });
    });

    let data1 = {};
    data1.warehouseId = store.getState().pincodeDetails?.warehouseId
      ? store.getState().pincodeDetails.warehouseId
      : 3;
    if (store.getState().accessToken) {
      data1.access_token = store.getState().accessToken;
    }

    const p3 = new Promise((resolve, reject) => {
      store.dispatch(FeaturedMedThunk(data1)).then(() => {
        resolve();
      });
    });

    let prop = {};
    prop.queryId = id;
    await Promise.all([p1, p2, p3]).then(() => {
      prop.details = store.getState();
      prop.medicineList = store.getState().searchMedicine?.medDataSuccess?.hits;
      prop.medicineListError = store.getState().searchMedicine?.medDataError;

      props.productDetails = store.getState().productDetails;
      prop.productDetailsAction =
        store.getState().productDetails?.productDetailsSuccess;
      prop.SuggestMedicine =
        store.getState().suggestMed?.featuredMedicineList?.hits;
    });
    return { ...prop };
  }
);

const mapStateToProps = (state) => ({
  Stepper: state.stepper,
  accessToken: state.loginReducer.verifyOtpSuccess?.Response?.access_token,
  cartContent: state.cartData?.cartItems,
  productDetails: state.productDetails,
  medicineList: state.searchMedicine?.medDataSuccess?.hits,
  medicineListError: state.searchMedicine?.medDataError,
  medConfirm: state.confirmMedicineReducer,
  imgUpload: state.uploadImage,
  isLoading: state.loader?.isLoading,
  pincodeDetails: state.pincodeData?.pincodeData,
  currentPage: state.pincodeData?.currentPage?.currentPage,
  appliedCoupon: state.cartData?.appliedCoupon,
  medIncomplete: state.incompleteOrderReducer,
});

const mapDispatchToProps = {
  fetchProductDetailsThunk: fetchProductDetailsThunk,
  addItemCartAction: addItemCartAction,
  removeItemCartAction,
  removeMedThunk,
  setPaymentModeThunk,
  changePaymentAction,
  incompleteOrderThunk,
  clearMedDetailsAction,
  lastImageClearAction,
  notOnOrderSummaryAction,
  toCartSectionAction,
  toOrderSummaryAction,
  fetchMedicineThunk,
  setCurrentPage,
};
export default withRouter(
  connect(mapStateToProps, mapDispatchToProps)(ProductDetails)
);

class Description extends React.Component {
  constructor(props) {
    super(props);
    this.state = {};
  }
  render() {
    return (
      <React.Fragment>
        <span
          dangerouslySetInnerHTML={{
            __html: this.props?.details
              ? this.props.details.replace(/\n/g, "<br />")
              : "",
          }}
        ></span>
      </React.Fragment>
    );
  }
}

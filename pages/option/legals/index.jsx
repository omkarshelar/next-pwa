import React from "react";
import Router, { withRouter } from "next/router";
import { data_Legal } from "../../../pageComponents/option/legals/DataLegals";
import right from "../../../src/Assets/RightArrow.png";
import privacy from "../../../src/Assets/Privacy.svg";
import {
  LegalContainer,
  TopicWrapperPrivacy,
} from "../../../pageComponents/option/legals/Legals.style";
// import {
//   eventPrivacyPolicyViewed,
//   eventTncViewed,
// } from "../../../Events/Events";

function Legals(props) {
  const goToThatPage = (route) => {
    Router.push(`${route}`);
    if (route === "/option/legals/policy") {
      // Event_Truemeds
      //? Singular & firebase connection remain
      // eventPrivacyPolicyViewed();
    } else {
      // Event_Truemeds
      // eventTncViewed();
    }
  };
  return (
    <LegalContainer>
      <span>Policies</span>
      <TopicWrapperPrivacy>
        {data_Legal.map((data) => (
          <div
            style={{ height: "50px" }}
            key={data.id}
            onClick={() => goToThatPage(data.routes)}
          >
            <div
              style={{
                display: "flex",
                width: "15rem",
                fontSize: "smaller",
                justifyContent: "flex-start",
              }}
            >
              <img
                src={privacy}
                alt="privacy"
                style={{ width: "50px", transform: "rotate(0deg)" }}
              />
              <span style={{ fontWeight: "600", alignSelf: "center" }}>
                {data.name}
              </span>
            </div>

            <div
              style={{
                display: "flex",
                justifyContent: "center",
                alignItems: "center",
              }}
            >
              <img src={right} alt="Go"></img>
            </div>
          </div>
        ))}
      </TopicWrapperPrivacy>
    </LegalContainer>
  );
}

export default withRouter(Legals);

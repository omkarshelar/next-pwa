import React, { useEffect } from "react";
import { connect } from "react-redux";
import { myOrderSubsThunk } from "../../../redux/SliderService/Actions";
import {
  MySubInfoStyle,
  CardStyle,
  Header,
  MedNamePrice,
  QTY,
  Company,
  Medname,
} from "../../../pageComponents/option/MySubsInfo.style";
import { withRouter } from "next/router";
import { wrapper } from "../../../redux/store";
import { parseCookies } from "../../../components/Helper/parseCookies";

function MySubsInfo({ ...props }) {
  let { dispatch, location, subsInfo, token, orderid } = props;
  useEffect(() => {
    if (token) {
      dispatch(
        myOrderSubsThunk({
          access_token: token,
          orderid: orderid,
        })
      );
    }
  }, [dispatch, orderid, token]);
  return (
    <MySubInfoStyle>
      {subsInfo &&
        subsInfo.MyMedicines.map((data) => (
          <CardStyle key={data.finalSubsId}>
            <div className="card-header">
              {data.composition.map((data) => data)}
            </div>
            {data.prescribed.productCode !== data.recommended.productCode ? (
              <div className="card-body med-recommended">
                <Header>
                  <span>RECOMMENDED</span>
                  <span>PRICE</span>
                </Header>
                <MedNamePrice>
                  <Medname>{data.recommended.medicineName}</Medname>
                  <div className="price-sec-med">
                    <span>₹{data.recommended.discountedPrice}</span>
                    <span>
                      MRP ₹<del>{data.recommended.price}</del>
                    </span>
                  </div>
                </MedNamePrice>
                <QTY>
                  <span>Qty: {data.recommended.subsQuantity}</span>
                </QTY>
                <Company>{data.recommended.companyName}</Company>
              </div>
            ) : null}
            <div className="card-body">
              <Header>
                <span>PRESCRIBED</span>
                <span>PRICE</span>
              </Header>
              <MedNamePrice>
                <Medname>{data.prescribed.medicineName}</Medname>
                <div className="price-sec-med">
                  <span>₹{data.prescribed.discountedPrice}</span>
                  <span>
                    MRP ₹<del>{data.prescribed.price}</del>
                  </span>
                </div>
              </MedNamePrice>
              <QTY>
                <span>Qty: {data.prescribed.originalQuantity}</span>
              </QTY>
              <Company>{data.prescribed.companyName}</Company>
            </div>
          </CardStyle>
        ))}
    </MySubInfoStyle>
  );
}

//server calls
MySubsInfo.getInitialProps = wrapper.getInitialPageProps(
  (store) => async (props) => {
    const cookies = parseCookies(props.req);
    let token = cookies?.token;
    let prop = {};
    prop.token = token;
    prop.orderid = props.query.orderid;
    return { ...prop };
  }
);

const mapStateToProps = (state) => ({
  subsInfo: state.mySubs?.MySubsOrder,
  token: state.loginReducer?.verifyOtpSuccess?.Response?.access_token,
});
export default withRouter(connect(mapStateToProps)(MySubsInfo));

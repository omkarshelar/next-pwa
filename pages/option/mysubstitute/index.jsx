import Router from "next/router";
import React from "react";
import { connect } from "react-redux";
import { mySubsThunk } from "../../../redux/SliderService/Actions";
import { wrapper } from "../../../redux/store";
import {
  MySubStyle,
  MedSection,
  PriceDate,
  Savings,
  CardStyle,
} from "../../../pageComponents/option/MySubs.style";
import { parseCookies } from "../../../components/Helper/parseCookies";
import Head from "next/head";

function MySubs({ ...props }) {
  let { dispatch, token, subsList } = props;

  /* useEffect(() => {
    if (token) {
      dispatch(
        mySubsThunk({
          access_token: token,
          history: Router,
        })
      );
    }
  }, [token]); */

  let convertEpoch = (epoch) => {
    var date = new Date(epoch);
    return date.toUTCString().slice(0, 16);
  };

  const goToSubsInfo = (orderid) => {
    Router.push(
      {
        pathname: "/option/mysubstitute/info",
        query: {
          orderid: orderid,
        },
      },
      undefined,
      { shallow: true }
    );
  };

  return (
    <MySubStyle>
      <Head>
        <meta name="robots" content="noindex nofollow" />
      </Head>
      {subsList && subsList.orderList ? (
        subsList.orderList.reverse().map((data) => (
          <CardStyle
            key={data.orderId}
            onClick={() => goToSubsInfo(data.orderId)}
          >
            <div className="card-header">ORDER NUMBER - {data.orderId}</div>
            <div className="card-body">
              <MedSection>
                {data.medicines.map(function (item, index) {
                  return (
                    <span key={`demo_snap_${index}`}>
                      {(index ? ", " : "") + item}
                    </span>
                  );
                })}
              </MedSection>
              <PriceDate>
                <span>₹{data.orderamount}</span>
                <span>{convertEpoch(data.date)}</span>
              </PriceDate>
              <Savings>
                <span>Saved ₹{data.totalSaving}</span>
                <span>
                  {data.statusId === 55 ? `Delivered` : `Expected Delivery`}
                </span>
              </Savings>
            </div>
          </CardStyle>
        ))
      ) : (
        <strong style={{ color: "#0071bc" }}>No Data Found</strong>
      )}
    </MySubStyle>
  );
}

//server calls
MySubs.getInitialProps = wrapper.getInitialPageProps(
  (store) => async (props) => {
    const cookies = parseCookies(props.req);
    let token = cookies?.token;
    const p1 = new Promise((resolve, reject) => {
      store
        .dispatch(mySubsThunk({ access_token: token, history: Router }))
        .then(() => {
          resolve();
        });
    });

    let prop = {};
    prop.token = token;
    await Promise.all([p1]).then(() => {
      prop.subsList = store.getState().mySubs.MySubs;
    });
    return { ...prop };
  }
);

const mapStateToProps = (state) => ({
  subsList: state.mySubs.MySubs,
  token: state.loginReducer?.verifyOtpSuccess?.Response?.access_token,
});

export default connect(mapStateToProps)(MySubs);

// Stage Url.
// export const OrderManagementService_URL =
//   "https://stage-services.truemeds.in/OrderManagementService";

// export const ElasticSearch_URL =
//   "https://search-tm-stage-es-nnmfrbwdug6zoksvm6pkvbz7ga.ap-south-1.es.amazonaws.com";

// export const CustomerService_URL =
//   "https://stage-services.truemeds.in/CustomerService";

// export const TrackingService_URL =
//   "https://stage-services.truemeds.in/TrackingService";

// export const ThirdPartySevice_URL =
//   "https://stage-services.truemeds.in/ThirdPartyService";

// export const PWA_URL = "https://stage-pwa.truemeds.in";

// export const PWA_static_URL =
//   "https://www.simpli5infotech.com/tm_rd/index.html";

// export const Article_Service_URL =
//   "https://stage-services.truemeds.in/ArticleService";

// export const Image_URL =
//   "https://tm-storage-bucket-stage.s3.ap-south-1.amazonaws.com/";

// export const Imagekit_URL = "https://assets.truemeds.in/stage";

// export const AdminService_URL =
//   "https://stage-services.truemeds.in/AdminService";

// export const REACT_VAPID_KEY =
//   "BLEVNMMOChkToc67n16yjMCTegWiPIvNGiF7yRIocOzZ33BV5cUkQD73eBNDpGHlS2zQGTwtDeufo0pWRZTugnQ";

// Prod Url.
export const PharmacistService_URL =
  "https://api.tmmumbai.in/PharmacistService";

export const OrderManagementService_URL =
  "https://api.tmmumbai.in/OrderManagementService";

export const CustomerService_URL = "https://api.tmmumbai.in/CustomerService";

export const TrackingService_URL = "https://api.tmmumbai.in/TrackingService";

export const ThirdPartySevice_URL = "https://api.tmmumbai.in/ThirdPartyService";

export const DoctorService_URL = "https://api.tmmumbai.in/DoctorService";

export const AdminService_URL = "https://api.tmmumbai.in/AdminService";

export const CsrService_URL = "https://api.tmmumbai.in/CsrService";

export const Image_URL =
  "https://tm-storage-bucket-prod.s3.ap-south-1.amazonaws.com/";

export const ElasticSearch_URL =
  "https://search-tm-prod-es-fk6ngihhkktmll4mo2h4xyyw4u.ap-south-1.es.amazonaws.com";

export const PWA_static_URL =
  "https://www.simpli5infotech.com/tm_rd/index.html";

export const PWA_URL = "https://truemeds.in";

export const Article_Service_URL = "https://api.tmmumbai.in/ArticleService";

export const Imagekit_URL = "https://assets.truemeds.in";

export const REACT_VAPID_KEY =
  "BMQ-rM-X-uON8XSZh2ZmJ3sOAdCZL1J3RkuGfnBLWeibAPotTdwF8V_6BCSzvLX0P490-d93A3PEmfLD7yT_BuU";

// Uat Url
// export const Image_URL =
//   "https://tm-storage-bucket-uat.s3.ap-south-1.amazonaws.com/";
// export const AdminService_URL = "https://uat-services.truemeds.in/AdminService";
// export const ThirdPartySevice_URL =
//   "https://uat-services.truemeds.in/ThirdPartyService";
// export const PharmacistService_URL =
//   "https://uat-services.truemeds.in/PharmacistService";
// export const OrderManagementService_URL =
//   "https://uat-services.truemeds.in/OrderManagementService";
// export const CustomerService_URL =
//   "https://uat-services.truemeds.in/CustomerService";
// export const DoctorService_URL =
//   "https://uat-services.truemeds.in/DoctorService";
// export const TrackingService_URL =
//   "https://uat-services.truemeds.in/TrackingService";
// export const CsrService_URL = "https://uat-services.truemeds.in/CsrService";
// export const ElasticSearch_URL =
//   "https://search-tm-uat-es-leogrdupczbaam4y3vupjxctti.ap-south-1.es.amazonaws.com";
// export const OauthService_URL = "https://uat-services.truemeds.in/OauthService";
// export const Article_Service_URL =
//   "https://uat-services.truemeds.in/ArticleService";

// export const Imagekit_URL = "https://assets.truemeds.in/stage";

// export const PWA_static_URL =
//   "https://www.simpli5infotech.com/tm_rd/index.html";

// export const PWA_URL = "https://uat-pwa.truemeds.in";

// export const REACT_VAPID_KEY =
//   "BLEVNMMOChkToc67n16yjMCTegWiPIvNGiF7yRIocOzZ33BV5cUkQD73eBNDpGHlS2zQGTwtDeufo0pWRZTugnQ";

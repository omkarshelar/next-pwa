import styled from "styled-components";
import ScrollContainer from "react-indiana-drag-scroll";

// Wrapper parent
export const MedSearchLoader = styled.div`
  width: 100vw;
  flex-flow: column;
  align-items: center;
  justify-content: center;
  min-height: 60vh;
  display: flex;

  .lottieWrapper {
    display: flex;
    align-items: center;
    height: 100%;
    flex-direction: column;
    justify-content: center;
  }

  .lottieWrapperText {
    font-style: normal;
    font-weight: 600;
    font-size: 16px;
    line-height: 24px;
    display: flex;
    align-items: center;
    letter-spacing: 0.02em;
    color: #728a9c;
    margin-top: -15px;
  }
`;

export const MedSearchWrapper = styled.div`
  min-height: 60vh;
  width: 70vw;
  background-color: #ffffff;
  display: flex;
  margin: 0 auto;
  position: relative;
  margin-top: 14px;
  margin-bottom: 20px;
  .med-search-right-container {
    width: 100%;
    display: flex;
    flex-direction: column;
    gap: 20px;
    height: fit-content;
    position: sticky;
    top: 85px;
    @media screen and (max-width: 968px) {
      display: none;
    }
  }

  @media screen and (max-width: 1400px) {
    width: 80vw;
  }
  @media screen and (max-width: 1210px) {
    width: 95vw;
  }

  @media screen and (max-width: 968px) {
    width: 100vw;
    flex-flow: column;
    align-items: center;
  }
`;

// Recent search and  Medicine List wrapper
export const MedResultCartSection = styled.div`
  display: flex;
  flex-flow: column;
  min-width: 70%;
  margin: 0 auto;
  padding: 0px 20px;

  @media screen and (max-width: 968px) {
    width: 94vw;
    padding: 0;
  }

  @media screen and (max-width: 768px) {
    .lottieWrapperText {
      font-size: 14px;
    }
  }
`;

// Recent search wrapper.
export const RecentSearch = styled.div`
  display: flex;
  flex-flow: column;
  width: 100%;
  padding: 5px 0 5px 0px;
  > div[class="recent-search-head"] {
    display: flex;
    justify-content: space-between;
    > h6 {
      /* font-family: "Raleway", sans-serif; */
      font-size: 19px;
      font-style: normal;
      font-weight: 600;
      margin: 5px 5px;
      letter-spacing: 0em;
      text-align: left;
    }
    > span {
      /* font-family: "Raleway", sans-serif; */
      font-size: 17px;
      font-style: normal;
      font-weight: 600;
      margin: 5px 5px;
      letter-spacing: 0em;

      cursor: pointer;
      color: #0071bc;
    }
  }
  @media screen and (max-width: 968px) {
    display: none;
  }
`;

export const HistoryWrapper = styled(ScrollContainer)`
  display: flex;
  max-width: 90%;
`;

// Medicine List
export const MedResult = styled.div`
  width: 100%;
  /* height: 75vh; */
  ::-webkit-scrollbar {
    display: none;
  }
  .result-med-p {
    font-size: 16px;
    font-weight: 400;
    letter-spacing: 0em;
    text-align: left;
    margin-bottom: 0;
    color: #728a9c;
    margin-bottom: 14px;
    @media screen and (max-width: 468px) {
      font-size: 12px;
      margin-bottom: 12px;
    }
  }

  .result-med-p span::first-letter {
    text-transform: capitalize;
  }
  position: relative;
  @media screen and (max-width: 968px) {
    width: 100%;
  }
  .end-list-container {
    margin: 24px 0px 90px 0px;
  }
  .end-list-no-container {
    margin: 24px 0px;
  }
  .end-list-container,
  .end-list-no-container {
    display: none;
    @media screen and (max-width: 968px) {
      display: flex;
      align-items: center;
      justify-content: center;
    }
    span {
      color: #728a9c;
      font-size: 14px;
      font-weight: 600;
    }
    @media screen and (max-width: 468px) {
      span {
        font-size: 12px;
      }
    }
  }
`;

// Cart Detail
export const Cart = styled.div`
  min-width: 30%;
  margin-left: 15px;
  @media screen and (max-width: 968px) {
    width: 100vw;
    padding: 0px;
    display: flex;
    justify-content: center;
    bottom: 0;
    position: fixed;
    margin: 0px;
    background-color: white;
  }
`;

export const CartDetails = styled.div`
  height: 40vh;
  overflow-y: auto;
  ::-webkit-scrollbar {
    display: none;
  }
  position: relative;
  @media screen and (max-width: 968px) {
    display: none;
  }
  img[class="empty-cart"] {
    position: absolute;
    top: 50%;
    left: 50%;
    width: 150px;
    -webkit-transform: translate(-50%, -50%);
    transform: translate(-50%, -50%);
    @media screen and (max-width: 968px) {
      width: 100px;
    }
  }
`;

export const Cartitem = styled.div`
  box-shadow: 0px 20px 50px rgb(49 45 43 / 10%);
  padding: 10px 5px;
  border-radius: 10px;
  @media screen and (max-width: 968px) {
    display: none;
  }
  padding: 20px 25px;
`;

export const CartHeader = styled.span`
  font-size: 16px;
  font-style: normal;
  font-weight: 600;
  line-height: 25px;
  letter-spacing: 0.7px;
  text-align: left;
  margin-bottom: 0.5rem;
  display: block;
  > span {
    font-weight: 500;
  }
`;

export const SearchProductImage = styled.div`
  display: flex;
  justify-content: center;
  align-items: center;
  height: 100%;
  > img {
    width: 300px;
  }
  @media screen and (max-width: 568px) {
    > img {
      width: 150px;
    }
  }
`;

export const NoResultFound = styled.div`
  text-align: center;
  color: #728a9c;
  font-size: 14px;
  min-height: 60vh;
  display: flex;
  flex-direction: column;

  justify-content: center;
  align-items: center;
  .medsIcons {
    margin-bottom: 38px;
  }
  .medsNRFText {
    font-family: "Inter";
    font-style: normal;
    font-weight: 600;
    font-size: 16px;
    line-height: 24px;
    display: flex;
    align-items: center;
    letter-spacing: 0.02em;
    color: #728a9c;
  }
  @media screen and (max-width: 768px) {
    text-align: center;
    color: #728a9c;
    font-size: 12px;
    display: flex;
    flex-direction: column;
    justfy-content: center;
    align-items: center;
    .medsNRFText {
      font-size: 14px;
    }
  }
`;

import React, { Component } from "react";
import removeIcon from "../SearchMedicineCard/remove-icon.svg";
import ribbonIcon from "../../../src/Assets/ribbon.png";
import { withStyles } from "@material-ui/core/styles";
import {
  CartDetailWrapper,
  MedFirstRow,
  MedSecondRow,
  Price,
  MRP,
  Discount,
  Medname,
  Inclusive,
} from "./CartDetailCard.style";
import { connect } from "react-redux";
import {
  addItemCartAction,
  removeItemCartAction,
  removeQuantityAction,
  removeMedThunk,
} from "../../../redux/Cart/Action";
import { RemoveMedStartAction } from "../../../redux/MedSearch/action";
import { HR } from "../SearchMedicineCard/SearchMedicineCard.style";
import { withRouter } from "next/router";
import { qtyArr } from "../../../redux/Cart/Util/CartUtil.js";
import Select from "react-select";

const styles = (theme) => ({
  menuPaper: {
    maxHeight: 120,
  },
});

export class CartDetailCard extends Component {
  state = {
    medData: [],
    uploadPres: false,
    lessTotal: false,
    openRec: false,
    rMedData: [],
    rContent: {},
    removeQuantity: false,
  };
  removeMedOnClick = (data) => {
    if (this.props.medConfirm?.orderId) {
      this.props.removeMedThunk({
        orderId: this.props.medConfirm.orderId,
        access_token: this.props.accessToken,
        medData: data,
        history: this.props.history,
      });
    } else if (this.props.uploadImage?.orderId) {
      this.props.removeMedThunk({
        orderId: this.props.uploadImage.orderId,
        access_token: this.props.accessToken,
        medData: data,
        history: this.props.history,
      });
    }
  };

  addNewQuantity = (obj, qty) => {
    obj.newQty = Number(qty);
    return obj;
  };
  quantityOnHandleChange = (values, content) => {
    let value = values.value;
    if (this.props.cartContent?.length > 0) {
      this.setState(
        {
          medData: this.props.cartContent.map((data) => {
            return {
              productCode: data.orgProductCd,
              quantity: data.orgQuantity,
            };
          }),
        },
        () => {
          this.props.addItemCart(this.addNewQuantity(content, value));
        }
      );
    }
  };

  setmaxQty = (arr, maxQty) => {
    let newArr = [];
    for (let i in arr) {
      if (arr[i] <= maxQty) {
        newArr.push(Number(arr[i]));
      }
    }
    return newArr;
  };

  render() {
    let { content } = this.props;
    return (
      <>
        <CartDetailWrapper>
          <MedFirstRow>
            <Medname>{content._source.original_sku_name}</Medname>
            <div className="quntity-select">
              <Select
                placeholder="QTY"
                value={{ label: content.quantity, value: content.quantity }}
                onChange={(e) => this.quantityOnHandleChange(e, content)}
                options={this.setmaxQty(
                  qtyArr,
                  content._source.max_capped_qty
                ).map((qty) => {
                  return {
                    value: qty,
                    label: qty,
                  };
                })}
                isSearchable={false}
                closeMenuOnScroll={(e) => {
                  if (e.target.className) {
                    return false;
                  } else {
                    return true;
                  }
                }}
                menuPlacement="auto"
                menuPosition="fixed"
              />
            </div>
          </MedFirstRow>
          <MedSecondRow>
            <Price>
              <span>{` ₹${
                content._source.original_product_code ===
                content._source.subs_product_code
                  ? content._source.subs_selling_price.toFixed(2)
                  : (
                      content._source.original_mrp -
                      content._source.original_mrp *
                        (content._source.original_base_discount / 100)
                    ).toFixed(2)
              }`}</span>
              <MRP>
                <span>
                  MRP<del> ₹{content._source.original_mrp}</del>
                </span>
                <Discount>
                  <img src={ribbonIcon} alt="ribbon" />
                  <span>
                    {" "}
                    {content._source.original_product_code !==
                    content._source.subs_product_code
                      ? content._source.original_base_discount
                      : Math.round(
                          ((content._source.original_mrp -
                            content._source.subs_selling_price) *
                            100) /
                            content._source.original_mrp
                        )}
                    % off
                  </span>
                </Discount>
              </MRP>
              <Inclusive>
                <span className="medCardInclusive">Inclusive of all taxes</span>
              </Inclusive>
            </Price>
            <div className="medCardRemoveContainer">
              <img
                src={removeIcon}
                alt="remove"
                style={{ width: "15px" }}
                onClick={() => {
                  let data = [
                    {
                      medicineName: content._source.original_sku_name,
                      medicineQty: "0",
                      medicineId: content._source.original_product_code,
                    },
                  ];
                  this.setState(
                    {
                      rMedData: data,
                      rContent: content,
                      removeQuantity: false,
                    },
                    () => {
                      this.removeMedOnClick(this.state.rMedData);
                      this.props.removeItemCart(this.state.rContent);
                    }
                  );
                }}
              />
            </div>
          </MedSecondRow>
        </CartDetailWrapper>
        {this.props.lastIndex && <HR />}
      </>
    );
  }
}

const mapStateToProps = (state) => ({
  accessToken: state.loginReducer.verifyOtpSuccess?.Response?.access_token,
  medConfirm: state.confirmMedicineReducer?.ConfirmMedData,
  uploadImage: state.uploadImage,
  cartContent: state.cartData?.cartItems,
});

let mapDispatchToProps = (dispatch) => ({
  removeItemCart: (data) => dispatch(removeItemCartAction(data)),
  removeQuantity: (data) => dispatch(removeQuantityAction(data)),
  addItemCart: (data) => dispatch(addItemCartAction(data)),
  removeMedicine: (data) => dispatch(RemoveMedStartAction(data)),
  removeMedThunk: (data) => dispatch(removeMedThunk(data)),
});

export default withRouter(
  connect(
    mapStateToProps,
    mapDispatchToProps
  )(withStyles(styles, { withTheme: true })(CartDetailCard))
);

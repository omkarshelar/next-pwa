import React, { Component } from "react";
import { connect } from "react-redux";
import { withRouter } from "next/router";
import { withStyles } from "@material-ui/core/styles";
import {
  addItemCartAction,
  removeItemCartAction,
  removeQuantityAction,
  removeMedThunk,
} from "../../../redux/Cart/Action";
import {
  AddCart,
  ImgContainer,
  ImgMedDetails,
  Medwrapper,
  Price,
  Quantity,
  FirstRow,
  MedWrapperOutter,
} from "./SearchMedicineCard.style";
import Button from "../../../components/WebComponents/Button/Button";
import { qtyArr } from "../../../redux/Cart/Util/CartUtil";
import Loader from "../../../components/WebComponents/Loader/Loader";
import optimizeImage from "../../../components/Helper/OptimizeImage";
import Select from "react-select";
import {
  eventSearchListingAddToCart,
  eventSearchListingRemove,
} from "../../../src/Events/Events";
import checkMedType from "../../../components/Helper/CheckMedType";

const styles = (theme) => ({
  menuPaper: {
    maxHeight: 120,
  },
});

export class SearchMedicineCard extends Component {
  state = {
    medData: [],
    uploadPres: false,
    lessTotal: false,
    openRec: false,
    rMedData: [],
    rContent: {},
    removeQuantity: false,
  };

  removeMedOnClick = (data) => {
    if (this.props.medConfirm?.orderId) {
      this.props.removeMedThunk({
        orderId: this.props.medConfirm.orderId,
        access_token: this.props.accessToken,
        medData: data,
        history: this.props.history,
      });
    } else if (this.props.uploadImage?.orderId) {
      this.props.removeMedThunk({
        orderId: this.props.uploadImage.orderId,
        access_token: this.props.accessToken,
        medData: data,
        history: this.props.history,
      });
    }
  };

  addNewQuantity = (obj, qty) => {
    obj.newQty = Number(qty);
    return obj;
  };

  quantityOnHandleChange = (values, content) => {
    let value = values.value;
    if (this.props.cartContent?.length > 0) {
      this.setState(
        {
          medData: this.props.cartContent.map((data) => {
            return {
              productCode: data.orgProductCd,
              quantity: data.orgQuantity,
            };
          }),
        },
        () => {
          this.props.addItemCart(this.addNewQuantity(content, value));
        }
      );
      if (value === 0) {
        let data = [
          {
            medicineName: content._source.original_sku_name,
            medicineQty: "0",
            medicineId: content._source.original_product_code,
          },
        ];
        this.setState(
          {
            rMedData: data,
            rContent: content,
            removeQuantity: false,
          },
          () => {
            eventSearchListingRemove();
            this.removeMedOnClick(this.state.rMedData);
            this.props.removeItemCart(this.state.rContent);
          }
        );
      }
    }
  };

  separateImageHandler = (links) => {
    if (links) {
      let result = links.split(",");
      let filter = result.find((data) => {
        let lastIndexOfSlash = data.lastIndexOf("/");
        return data.substring(lastIndexOfSlash + 1).includes("_1");
      });
      if (filter) {
        return filter;
      } else {
        return result[0];
      }
    } else {
      return null;
    }
  };

  setmaxQty = (arr, maxQty) => {
    let newArr = [];
    for (let i in arr) {
      if (arr[i] <= maxQty) {
        newArr.push(Number(arr[i]));
      }
    }
    return newArr;
  };

  render() {
    let { content } = this.props;
    return (
      <>
        {this.props.isLoading && <Loader />}
        <MedWrapperOutter>
          <Medwrapper onClick={(e) => this.props.goToDetails(e)}>
            <ImgContainer>
              {content?._source?.product_image_urls ? (
                <img
                  className="tm-med-image"
                  src={optimizeImage(
                    this.separateImageHandler(
                      content._source.product_image_urls
                    ),
                    "80"
                  )}
                  alt="medicine_image"
                />
              ) : (
                <img
                  className="tm-stock-image"
                  src={checkMedType(content?._source?.original_drug_type)}
                  alt="medicine_image"
                />
              )}
            </ImgContainer>
            <FirstRow>
              <ImgMedDetails>
                <p>{content._source.original_sku_name}</p>
                <span>{content._source.original_company_nm}</span>
              </ImgMedDetails>
              <div className="price-add-container">
                <Price>
                  <div className="main-prices-container">
                    <span className="selling-price">
                      {(content._source.original_product_code !==
                      content._source.subs_product_code
                        ? content._source.original_base_discount
                        : Math.round(
                            ((content._source.original_mrp -
                              content._source.subs_selling_price) *
                              100) /
                              content._source.original_mrp
                          )) > 0
                        ? ""
                        : "MRP"}
                      {` ₹${
                        content._source.original_product_code ===
                        content._source.subs_product_code
                          ? content._source.subs_selling_price.toFixed(2)
                          : (
                              content._source.original_mrp -
                              content._source.original_mrp *
                                (content._source.original_base_discount / 100)
                            ).toFixed(2)
                      }`}
                    </span>
                    {(content._source.original_product_code !==
                    content._source.subs_product_code
                      ? content._source.original_base_discount
                      : Math.round(
                          ((content._source.original_mrp -
                            content._source.subs_selling_price) *
                            100) /
                            content._source.original_mrp
                        )) > 0 ? (
                      <span className="mrp-price">
                        MRP <del>₹{content._source.original_mrp}</del>
                      </span>
                    ) : null}
                  </div>
                  {(content._source.original_product_code !==
                  content._source.subs_product_code
                    ? content._source.original_base_discount
                    : Math.round(
                        ((content._source.original_mrp -
                          content._source.subs_selling_price) *
                          100) /
                          content._source.original_mrp
                      )) > 0 ? (
                    <div className="min-discount-container">
                      <span>
                        Min{" "}
                        {content._source.original_product_code !==
                        content._source.subs_product_code
                          ? content._source.original_base_discount
                          : Math.round(
                              ((content._source.original_mrp -
                                content._source.subs_selling_price) *
                                100) /
                                content._source.original_mrp
                            )}
                        % OFF
                      </span>
                    </div>
                  ) : null}
                </Price>
              </div>
            </FirstRow>
          </Medwrapper>
          <div>
            {content.quantity ? (
              <Quantity>
                <div id="addtocart" className="quantity-select">
                  <Select
                    id="addtocart"
                    placeholder="QTY"
                    value={{ label: content.quantity, value: content.quantity }}
                    onChange={(e) => this.quantityOnHandleChange(e, content)}
                    options={this.setmaxQty(
                      qtyArr,
                      content._source.max_capped_qty
                    ).map((qty) => {
                      return {
                        value: qty,
                        label: qty,
                      };
                    })}
                    isSearchable={false}
                  />
                </div>
              </Quantity>
            ) : (
              <AddCart>
                {!content._source.original_available &&
                !content._source.subs_found ? (
                  <span className="not-in-stock" style={{}}>
                    Not In Stock
                  </span>
                ) : content._source.original_supplied_bytm ? (
                  <Button
                    id="addtocart"
                    CartAdd
                    onClick={(e) => {
                      // e.preventDefault();
                      // e.stopPropagation();
                      // e.nativeEvent.stopImmediatePropagation();
                      this.props.addItemCart(this.addNewQuantity(content, 1));
                      eventSearchListingAddToCart();
                      // this.props.recentMedSearch(
                      //   content._source.original_sku_name
                      // );
                    }}
                  >
                    Add To Cart
                  </Button>
                ) : (
                  <span
                    style={{ alignSelf: "flex-end" }}
                    className="not-available"
                  >
                    Not sold by Truemeds
                  </span>
                )}
              </AddCart>
            )}
          </div>
        </MedWrapperOutter>
      </>
    );
  }
}

const mapStateToProps = (state) => ({
  accessToken: state.loginReducer.verifyOtpSuccess?.Response?.access_token,
  medConfirm: state.confirmMedicineReducer?.ConfirmMedData,
  uploadImage: state.uploadImage,
  cartContent: state.cartData?.cartItems,
  isLoading: state.loader.isLoading,
});

let mapDispatchToProps = (dispatch) => ({
  removeItemCart: (data) => dispatch(removeItemCartAction(data)),
  removeQuantity: (data) => dispatch(removeQuantityAction(data)),
  addItemCart: (data) => dispatch(addItemCartAction(data)),
  removeMedThunk: (data) => dispatch(removeMedThunk(data)),
});

export default withRouter(
  connect(
    mapStateToProps,
    mapDispatchToProps
  )(withStyles(styles, { withTheme: true })(SearchMedicineCard))
);

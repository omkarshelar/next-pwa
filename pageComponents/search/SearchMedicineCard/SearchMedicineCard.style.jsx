import styled from "styled-components";

export const MedWrapperOutter = styled.div`
  position: relative;
`;
// Card container
export const Medwrapper = styled.div`
  box-sizing: border-box;
  width: 100%;
  padding: 16px;
  display: flex;
  background-color: #fff;
  box-shadow: 0px 1px 7px rgba(0, 0, 0, 0.25);
  border-radius: 12px;
  margin-bottom: 16px;
  cursor: pointer;
  @media screen and (max-width: 468px) {
    padding: 12px;
    margin-bottom: 12px;
  }
`;

export const FirstRow = styled.div`
  display: flex;
  flex-direction: column;
  width: 85%;
  .price-add-container {
    display: flex;
    justify-content: space-between;
    align-items: flex-end;
  }
`;

export const ImgContainer = styled.div`
  display: flex;
  align-items: center;
  justify-content: center;
  width: 15%;
  .tm-med-image {
    width: 80px;
    height: 80px;
    object-fit: contain;
    @media screen and (max-width: 468px) {
      width: 40px;
      height: 40px;
    }
  }
  .tm-stock-image {
    width: 60px;
    height: 60px;
    @media screen and (max-width: 468px) {
      width: 35px;
      height: 35px;
    }
  }
`;

// Image, Med Details
export const ImgMedDetails = styled.div`
  > p {
    font-weight: 600;
    font-size: 16px;
    margin: 0;
    color: #40464d;
    @media screen and (max-width: 468px) {
      font-size: 14px;
    }
  }
  > span {
    font-weight: 400;
    font-size: 12px;
    color: #40464d;
  }
`;

export const Price = styled.div`
  display: flex;
  flex-direction: column;
  margin-top: 8px;
  .main-prices-container {
    display: flex;
    align-items: center;
    @media screen and (max-width: 468px) {
      flex-direction: column;
      align-items: flex-start;
    }
  }
  .selling-price {
    color: #40464d;
    font-weight: 700;
    font-size: 16px;
    margin-right: 8px;
    @media screen and (max-width: 468px) {
      font-size: 14px;
    }
    @media screen and (max-width: 320px) {
      font-size: 12px;
    }
  }
  .mrp-price {
    font-weight: 500;
    font-size: 12px;
    color: #728a9c;
    @media screen and (max-width: 320px) {
      font-size: 10px;
    }
  }
  .min-discount-container {
    border-radius: 4px;
    background-color: none;
    width: fit-content;
    padding: 4px;
    margin-top: 4px;
    @media screen and (max-width: 320px) {
      padding: 0px;
    }
  }
  .min-discount-container span {
    color: #2fa46f;
    font-weight: 700;
    font-size: 14px;
    @media screen and (max-width: 320px) {
      font-size: 12px;
    }
  }
`;

export const Quantity = styled.div`
  display: block;
  width: 100%;
  position: absolute;
  bottom: 16px;
  right: 16px;
  @media screen and (max-width: 768px) {
    width: 95%;
  }
  box-sizing: border-box;
  align-items: center;
  > div[class="remove"] {
    order: 2;
    display: flex;
    align-items: center;
    > img {
      width: 15px;
    }
    span {
      color: #dd2200;
      /* font-family: "Raleway", sans-serif; */
      font-size: 14px;
      font-style: normal;
      font-weight: 600;
      line-height: 16px;
      letter-spacing: 0.02em;
      text-align: center;
      padding-left: 4px;
    }
    @media screen and (max-width: 468px) {
      order: 1;
    }
  }

  > div[class="quantity-select"] {
    display: block;
    align-items: center;
    float: right;
    order: 1;

    .css-b62m3t-container {
      width: 100px !important;
      @media screen and (max-width: 468px) {
        width: 80px !important;
      }
    }

    .css-1s2u09g-control,
    .css-1pahdxg-control {
      border: 1.6px solid #0071bc !important;
      border-radius: 6px;
      height: 44px !important;
      @media screen and (max-width: 468px) {
        height: 30px !important;
      }
    }

    .css-1okebmr-indicatorSeparator {
      display: none !important;
    }

    .css-tj5bde-Svg,
    .css-qc6sy-singleValue {
      color: #0071bc !important;
      font-family: "Inter";
    }

    .css-1pahdxg-control:hover {
      border-color: #0071bc !important;
    }

    .css-2613qy-menu div {
      color: #0071bc !important;
    }

    .css-1n7v3ny-option {
      background-color: #f5f5f5;
      color: #0071bc !important;
      font-size: 14px;
      font-weight: 400;
      font-family: "Inter";
    }
    .css-tlfecz-indicatorContainer {
      color: #0071bc !important;
    }

    .css-9gakcf-option {
      background-color: #e6f7ff;
      color: #0071bc !important;
    }

    .css-yt9ioa-option {
      color: #0071bc !important;
      font-size: 14px;
      font-weight: 400;
      font-family: "Inter";
    }

    .css-14el2xx-placeholder {
      color: #0071bc !important;
      font-size: 14px;
      font-weight: 400;
      font-family: "Inter";
    }

    .ant-select {
      width: 100px !important;
      @media screen and (max-width: 468px) {
        width: 80px !important;
      }
    }

    .ant-select:not(.ant-select-customize-input) .ant-select-selector {
      border: 1.6px solid #0071bc !important;
      border-radius: 6px;
      height: 44px !important;
      display: flex !important;
      align-items: center !important;
      @media screen and (max-width: 468px) {
        height: 30px !important;
      }
    }

    .ant-select-arrow {
      color: #0071bc !important;
    }

    .ant-select-focused:not(.ant-select-disabled).ant-select:not(.ant-select-customize-input)
      .ant-select-selector {
      border: 1.6px solid #0071bc !important;
      box-shadow: 0 0 0 2px rgb(0 113 188 / 20%) !important;
    }

    .ant-select-single.ant-select-show-arrow .ant-select-selection-item,
    .ant-select-single.ant-select-show-arrow .ant-select-selection-placeholder {
      font-weight: 700 !important;
      font-size: 16px !important;
      color: #0071bc !important;
      font-family: "Inter", sans-serif !important;
    }

    > i:first-child {
      margin-right: 10px;
    }
    > i:last-child {
      margin-left: 10px;
    }
    > span {
      font-weight: 700;
      font-size: 16px;
      /* font-family: "Century Gothic", sans-serif; */
      color: #333;
    }
    font-family: "Inter", sans-serif;
    font-size: 14px;
    font-style: normal;
    font-weight: 700;
    line-height: 18px;
    letter-spacing: 0em;
    text-align: left;
    @media screen and (max-width: 468px) {
      order: 2;
      .ant-select-single.ant-select-show-arrow .ant-select-selection-item,
      .ant-select-single.ant-select-show-arrow
        .ant-select-selection-placeholder {
        font-size: 14px !important;
      }
    }
  }
`;

// Add to cart/ quantity
export const AddCart = styled.div`
  display: flex;
  width: 100%;
  position: absolute;
  bottom: 16px;
  right: 16px;
  align-items: center;
  justify-content: flex-end;
  > span[class="not-in-stock"] {
    align-self: flex-end;
    color: #999999;
    font-weight: 600;
  }
  > span[class="not-available"] {
    align-self: flex-end;
    color: #999999;
    font-weight: 600;
  }
`;

export const HR = styled.hr`
  display: block;
  width: 100%;
  opacity: 0.3;
  border-top: 1px solid #0071bc;
  margin-top: 0.1rem;
  margin-bottom: 0.1rem;
`;

export const SecondRowWrapper = styled.div`
  display: flex;
  justify-content: space-between;
  width: 100%;
  flex-grow: 1;
  @media screen and (max-width: 768px) {
    justify-content: flex-end;
    width: 40%;
  }
`;

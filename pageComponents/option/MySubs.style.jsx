import { Card } from "react-bootstrap";
import styled from "styled-components";

export const MySubStyle = styled.div`
  display: flex;
  flex-flow: column;
  justify-content: center;
  align-items: center;
  margin: 0 auto;
  width: 60%;
  @media screen and (max-width: 900px) {
    width: 80%;
  }
  @media screen and (max-width: 540px) {
    width: 100%;
  }
`;

export const MedSection = styled.div`
  display: -webkit-box;
  -webkit-line-clamp: 3;
  -webkit-box-orient: vertical;
  overflow: hidden;
  @media screen and (max-width: 468px) {
    font-size: small;
  }
`;
export const PriceDate = styled.div`
  display: flex;
  justify-content: space-between;
  font-weight: 600;
  @media screen and (max-width: 468px) {
    font-size: small;
  }
`;
export const Savings = styled.div`
  display: flex;
  justify-content: space-between;
  span:nth-child(1) {
    color: #44bb91;
  }
  span:nth-child(2) {
    font-weight: 600;
    color: #909090;
  }

  @media screen and (max-width: 468px) {
    font-size: small;
  }
`;

export const CardStyle = styled(Card)`
  width: 95%;
  border-color: #b9d9ec;
  cursor: pointer;
  .card-header {
    padding: 5px;
    background-color: #b9d9ec;
    color: #0071bc;
    border-color: #b9d9ec;
    @media screen and (max-width: 468px) {
      font-size: small;
    }
  }
  margin: 0.5rem 0 0.5rem 0;
`;

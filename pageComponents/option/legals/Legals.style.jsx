import styled from "styled-components";

export const LegalContainer = styled.div`
  margin-top: 10px;
  > span {
    color: #0071bc;
    display: block;
    width: 95%;
    margin: 0 auto;
  }
`;

export const TopicWrapperPrivacy = styled.div`
  flex-flow: column;
  display: flex;
  justify-content: center;
  align-items: center;
  > div {
    display: flex;
    justify-content: space-between;
    width: 95%;
    height: 30px;
    margin: 5px 0 5px 0;
    cursor: pointer;
    border-bottom: 1px #c0c0c0 solid;
  }

  > div > span {
    color: #36454f;
  }
  > div > div > img {
    width: 70%;
    transform: rotate(-90deg);
  }
  > div > div {
    width: 30px;
  }
`;

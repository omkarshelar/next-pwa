import { Card } from "react-bootstrap";
import styled from "styled-components";
export const MySubInfoStyle = styled.div`
  display: flex;
  flex-flow: column;
  justify-content: center;
  align-items: center;
  margin: 0 auto;
  width: 60%;
  @media screen and (max-width: 900px) {
    width: 80%;
  }
  @media screen and (max-width: 540px) {
    width: 100%;
  }
`;

export const CardStyle = styled(Card)`
  width: 95%;
  border-color: #b9d9ec;
  .card-header {
    padding: 5px;
    background-color: #b9d9ec;
    color: #0071bc;
    border-color: #b9d9ec;
    @media screen and (max-width: 468px) {
      font-size: small;
    }
  }
  @media screen and (max-width: 468px) {
    .card-body {
      padding: 5px;
    }
  }
  > .med-recommended {
    background-color: #daf1e9;
  }
  margin: 0.5rem 0 0.5rem 0;
`;

export const Header = styled.div`
  display: flex;
  justify-content: space-between;
  color: #44bb91;
  font-weight: 600;
  font-size: small;
`;

export const MedNamePrice = styled.div`
  display: flex;
  justify-content: space-between;

  > .price-sec-med {
    display: flex;
    flex-flow: column;
    justify-content: flex-end;
    align-items: flex-end;
  }
  > .price-sec-med > span:nth-child(1) {
    font-weight: 600;
  }
  > .price-sec-med > span:nth-child(2) {
    font-weight: 600;
    color: #909090;
  }
  @media screen and (max-width: 468px) {
    > .price-sec-med {
      font-size: small;
    }
  }
`;

export const QTY = styled.div`
  display: flex;
  justify-content: flex-end;
  font-weight: 600;
  color: #909090;
  @media screen and (max-width: 468px) {
    font-size: small;
  }
`;

export const Company = styled.div`
  color: #909090;
  @media screen and (max-width: 468px) {
    font-size: small;
  }
`;

export const Medname = styled.div`
  font-weight: 600;
  @media screen and (max-width: 468px) {
    width: 150px;
    font-size: small;
  }
`;

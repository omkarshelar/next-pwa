import React from "react";
import { connect } from "react-redux";
import "./DeleteModal.Style.css";
import CustomButton from "../../../../../../components/WebComponents/CustomButton/CustomButton";

function DeleteModal({ Msg, handleClose, onClickDeleteUploadImage }) {
  return (
    <div className="customModalWrapper">
      <div className="ModalBody">
        <h6>{Msg}</h6>
        <div>
          <CustomButton onClick={() => handleClose()} delNo>
            No
          </CustomButton>
          <CustomButton onClick={() => onClickDeleteUploadImage()} delYes>
            Yes
          </CustomButton>
        </div>
      </div>
    </div>
  );
}
let mapStateToProps = (state) => ({
  imgUpload: state.uploadImage.uploadHistory,
});

export default connect(mapStateToProps)(DeleteModal);

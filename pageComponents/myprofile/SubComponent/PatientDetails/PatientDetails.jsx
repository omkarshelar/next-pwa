import React, { Component } from "react";
import { connect } from "react-redux";
import Router, { withRouter } from "next/router";
import Button from "../../../../components/WebComponents/Button/Button";
import {
  PatientWrapper,
  PatientList,
  SubmitWrapper,
} from "./PatientDetails.style";
import AddPatientCard from "./SubComponent/PatientDetailsCard/AddPatientCard";
import OwnDetailsCard from "./SubComponent/PatientDetailsCard/OwnDetailsCard";
import PatientDetailsCard from "./SubComponent/PatientDetailsCard/PatientDetailsCard";
import PatientListCard from "./SubComponent/PatientListCard/PatientListCard";
import {
  toCartSectionAction,
  toUploadPrescriptionAction,
  toAddressDetailsAction,
  toOrderSummaryAction,
} from "../../../../redux/Timeline/Action";
import { message } from "antd";
import {
  fetchpatientThunk,
  deletePatientThunk,
  updateProfileThunk,
  savePatientCheckoutThunk,
  addPatientThunk,
  selectPatinetForOrderAction,
} from "../../../../redux/PatientDetails/Action";
import { getPrescriptionByIdThunk } from "../../../../redux/UploadImage/Action";
import Shimmer from "react-shimmer-effect";
import DetailsCardLoader from "../../../../components/WebComponents/Shimmer/DetailsCardLoader";
import {
  eventPatientAdded,
  eventPatientDeleted,
  eventPatientSelected,
  addUserAttributesMoe,
  // hjPageChange,
} from "../../../../Events/Events";
import { HiPlusSm } from "react-icons/hi";
import window from "global";

export class PatientDetails extends Component {
  state = {
    selectedEditPatient: null,
    addPatient: false,
    initialState: {
      fullName: "",
      age: "",
      gender: "8",
      email: null,
      relation: null,
    },
    initialForAddPatient: {
      fullName: "",
      age: "",
      gender: "8",
      relation: null,
    },
    initialForOwnDetails: {
      fullName: "",
      age: "",
      gender: "8",
      email: null,
    },
    loading: false,
  };

  // Fetch existing patient list.
  componentDidMount() {
    window.scrollTo(0, 0);
    window.history.pushState(null, null, window.location.pathname);
    window.addEventListener("popstate", this.onBackButtonEvent);
    this.fetchPatientData();
    if (this.props.profile) {
      this.props.getPrescriptionByIdThunk({
        access_token: this.props.accessToken,
        history: Router,
        customerId: String(this.props.custId),
      });
    }
    //page change for hotjar
    // hjPageChange(window.location.origin + "/orderflow#patient");
  }

  // Clear State after unmounting
  componentWillUnmount = () => {
    window.removeEventListener("popstate", this.onBackButtonEvent);
  };

  // On Back Button click Event.
  onBackButtonEvent = (e) => {
    e.preventDefault();
    if (!this.isBackButtonClicked) {
      if (this.props.imgUpload.length > 0) {
        this.props.toUploadPrescriptionAction();
      } else {
        this.props.toCartSectionAction();
      }
      this.isBackButtonClicked = false;
    }
  };

  fetchPatientData = () => {
    if (this.props.accessToken) {
      let data = {
        accessToken: this.props.accessToken,
        history: this.props.history,
      };
      this.props.fetchpatientThunk(data).then(() => {
        if (this.props.patientContent?.fetchPatientError) {
          message.error("something went wrong");
        } else {
          this.closePatientDetailsSection();
        }
      });
    }
  };

  // Store patientid for edit and update patient.
  togglePatientDetailsSection = (data) => {
    this.setState({
      selectedEditPatient: data.patientId,
      initialState: Object.assign({}, this.state.initialState, {
        fullName: data.patientName,
        age: Number(data.age),
        gender: String(data.gender),
        relation: data.relationId,
        email: data.emailId,
      }),
    });
  };

  // reset state to close section.
  closePatientDetailsSection = () => {
    this.setState({
      selectedEditPatient: null,
      initialState: {
        fullName: "",
        age: "",
        gender: "8",
        email: null,
        relation: null,
      },
      initialForAddPatient: {
        fullName: "",
        age: "",
        gender: "8",
        relation: null,
      },
      initialForOwnDetails: {
        fullName: "",
        age: "",
        gender: "8",
        email: null,
      },
    });
  };

  deletePatientHandler = (patientid, patientData) => {
    if (this.props.patientsList.length > 1 && patientData.relationId !== 5) {
      let data = {
        accessToken: this.props.accessToken,
        patientId: patientid,
        customerId: this.props.user?.customerId,
        history: this.props.history,
      };
      this.props.deletePatientThunk(data).then(() => {
        if (this.props.patientContent?.deletePatientError) {
          message.error("something went wrong");
        } else {
          // Event_Truemeds
          eventPatientDeleted();
          this.fetchPatientData();
        }
      });
    } else {
      message.warn(
        "You cannot edit or delete patient used in an active order."
      );
    }
  };

  updateProfileHandler = (value) => {
    let data = {
      accessToken: this.props.accessToken,
      latestData: {
        customerName: value.fullName,
        emailAddress: value.email,
        age: value.age,
        gender: value.gender,
      },
      history: this.props.history,
    };
    this.props.updateProfileThunk(data).then(() => {
      if (this.props.patientContent?.updateProfileError) {
        message.error("something went wrong");
      } else {
        this.fetchPatientData();
      }
    });
  };

  updateProfileHandlerForOwnDetails = (value) => {
    let data = {
      accessToken: this.props.accessToken,
      latestData: {
        customerName: value.fullName,
        emailAddress: value.email,
        age: value.age,
        gender: value.gender,
      },
      history: this.props.history,
    };
    this.props.updateProfileThunk(data).then(() => {
      if (this.props.patientContent?.updateProfileError) {
        message.error("something went wrong");
      } else {
        addUserAttributesMoe(data.latestData);
        // Event_Truemeds
        eventPatientAdded();
        this.fetchPatientData();
      }
    });
  };

  addNewPatientHandler = (value) => {
    let data = {
      accessToken: this.props.accessToken,
      patientData: {
        patientName: value.fullName,
        age: value.age,
        gender: value.gender,
        relationId: value.relation,
      },
      history: this.props.history,
    };
    this.props.addPatientThunk(data).then(() => {
      if (this.props.patientContent?.newPatientError) {
        message.error("something went wrong");
      } else {
        this.setState({ addPatient: false });
        // Event_Truemeds
        eventPatientAdded();
        this.fetchPatientData();
        message.success("Pateint added successfully!");
      }
    });
  };

  editPatientHandler = (value) => {
    let data = {
      accessToken: this.props.accessToken,
      patientData: {
        patientName: value.fullName,
        age: value.age,
        gender: value.gender,
        relationId: value.relation,
        patientId: this.state.selectedEditPatient,
      },
      history: this.props.history,
    };
    this.props.addPatientThunk(data).then(() => {
      if (this.props.patientContent?.newPatientError) {
        message.error("something went wrong");
      } else {
        this.fetchPatientData();
      }
    });
  };

  selectPatientForCheckout = (patientId) => {
    let data = this.props.patientsList?.find(
      (data) => data.patientId === patientId
    );
    // Event_Truemeds
    eventPatientSelected();
    this.props.selectPatinetForOrderAction(data);
  };

  submitData = () => {
    let {
      accessToken,
      orderTypeOneOrderId,
      orderTypeTwoOrderId,
      patientsList,
    } = this.props;

    let idFound = patientsList.find((data) => data.selected === true);
    if (idFound?.patientId) {
      if (accessToken && (orderTypeOneOrderId || orderTypeTwoOrderId)) {
        let data = {
          accessToken: accessToken,
          patientId: idFound.patientId,
          orderId: orderTypeOneOrderId || orderTypeTwoOrderId,
          history: this.props.history,
        };
        this.setState({ loading: true });
        this.props.savePatientCheckoutThunk(data).then(() => {
          this.setState({ loading: false });
          if (this.props.Stepper.onOrderSummary) {
            this.props.toOrderSummaryAction();
          } else {
            this.props.toAddressDetailsAction();
          }
        });
      }
    } else {
      message.warn("Please select a patient");
    }
  };

  getPrescriptionsByPatientId = (id) => {
    let arr = this.props.allPrescriptions.prescriptionById?.CustomerRx;
    let finalObj = {};
    if (arr?.length > 0) {
      finalObj = arr.find((x) => x.patientId === id);
    }
    return finalObj;
  };

  render() {
    let { patientsList } = this.props;
    return (
      <PatientWrapper>
        {this.props.isLoading ? (
          <Shimmer>
            <div className="patientDetailsAddLoader" />
          </Shimmer>
        ) : (
          patientsList?.length > 0 && (
            <header>
              {this.props.profile ? null : "Select Patient"}
              <span
                style={{
                  margin: this.props.profile ? "0" : "",
                  display: "flex",
                }}
                onClick={() => this.setState({ addPatient: true })}
              >
                <HiPlusSm className="plusIcon" />
                Add Patient
              </span>
            </header>
          )
        )}
        {this.state.addPatient && (
          <AddPatientCard
            closePatientDetailsSection={() =>
              this.setState({ addPatient: !this.state.addPatient })
            }
            header="Add Patient Details"
            submitHandler={this.addNewPatientHandler}
            initialState={this.state.initialForAddPatient}
          />
        )}
        {this.props.isLoading ? (
          <>
            <DetailsCardLoader onlyCard={true} allDetails={true} />
            <DetailsCardLoader onlyCard={true} allDetails={false} />
          </>
        ) : (
          <PatientList>
            {patientsList?.length > 0 ? (
              patientsList.map((data) => (
                <>
                  <PatientListCard
                    prescriptions={
                      this.props.profile
                        ? this.getPrescriptionsByPatientId(data.patientId)
                        : null
                    }
                    profile={this.props.profile}
                    data={data}
                    key={data.patientId}
                    togglePatientDetailsSection={
                      this.togglePatientDetailsSection
                    }
                    deletePatientHandler={this.deletePatientHandler}
                    selectPatientForCheckout={this.selectPatientForCheckout}
                  />
                  {data.patientId === this.state.selectedEditPatient && (
                    <PatientDetailsCard
                      header="Edit Patient Details"
                      closePatientDetailsSection={
                        this.closePatientDetailsSection
                      }
                      submitHandler={this.editPatientHandler}
                      updateProfileHandler={this.updateProfileHandler}
                      initialState={this.state.initialState}
                      data={data}
                    />
                  )}
                </>
              ))
            ) : (
              <OwnDetailsCard
                header="Add Your Own Details"
                closePatientDetailsSection={this.closePatientDetailsSection}
                initialState={this.state.initialForOwnDetails}
                submitHandler={this.updateProfileHandlerForOwnDetails}
              />
            )}
          </PatientList>
        )}

        {!this.props.profile &&
          this.props.patientsList?.length > 0 &&
          !this.state.selectedEditPatient &&
          !this.state.addPatient && (
            <SubmitWrapper>
              <Button
                id="addMedBtn"
                BtnAdd
                onClick={() => {
                  if (this.props.Stepper.onOrderSummary) {
                    Router.push("/?search=true");
                  } else {
                    Router.push({
                      pathname: "/",
                      search: "?search=true",
                      state: {
                        patientDetails: true,
                      },
                    });
                  }
                }}
              >
                Add More Medicines
              </Button>
              <Button
                BtnContinue
                disabled={this.state.loading}
                loading={this.state.loading}
                onClick={() => this.submitData()}
              >
                Continue
              </Button>
            </SubmitWrapper>
          )}
      </PatientWrapper>
    );
  }
}

let mapStateToProps = (state) => ({
  accessToken: state.loginReducer.verifyOtpSuccess?.Response?.access_token,
  patientsList: state.patientData.fetchPatientSuccess,
  user: state.loginReducer.verifyOtpSuccess?.CustomerDto,
  imgUpload: state.uploadImage?.uploadHistory,
  orderTypeOneOrderId: state.uploadImage?.orderId,
  orderTypeTwoOrderId: state.confirmMedicineReducer.ConfirmMedData?.orderId,
  Stepper: state.stepper,
  isLoading: state.loader.isLoading,
  patientContent: state.patientData,
  allPrescriptions: state.uploadImage,
  custId: state.loginReducer.customerDetails?.CustomerDetails?.customerId,
});

export default withRouter(
  connect(mapStateToProps, {
    fetchpatientThunk,
    deletePatientThunk,
    updateProfileThunk,
    savePatientCheckoutThunk,
    addPatientThunk,
    selectPatinetForOrderAction,
    toAddressDetailsAction,
    toCartSectionAction,
    toUploadPrescriptionAction,
    toOrderSummaryAction,
    getPrescriptionByIdThunk,
  })(PatientDetails)
);

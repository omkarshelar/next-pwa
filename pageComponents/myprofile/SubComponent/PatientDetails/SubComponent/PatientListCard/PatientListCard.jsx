import React, { Component } from "react";
import Kebab from "../../../../../../components/WebComponents/KebabMenu/Kebab";
import {
  PatientListWrapper,
  RadioEnable,
  RadioPatient,
  PrescriptionWrapper,
  PatientListMainWrapper,
} from "./PatientListCard.style";
import Tick from "../../../../../../src/Assets/tick.png";
import ScrollContainer from "react-indiana-drag-scroll";
import { Image_URL } from "../../../../../../constants/Urls";
import PreviewModal from "../../../UploadPrescription/UploadSubComponent/PreviewModal/PreviewModal";

export class PatientListCard extends Component {
  state = {
    showPreview: false,
    previewSrc: "",
  };

  openPreview = (image) => {
    this.setState({ showPreview: !this.state.showPreview, previewSrc: image });
  };

  closePreview = () => {
    this.setState({ showPreview: !this.state.showPreview });
  };

  render() {
    let { data, selectPatientForCheckout } = this.props;
    return (
      <>
        {this.state.showPreview && (
          <PreviewModal
            open={this.state.showPreview}
            imageData={this.state.previewSrc}
            onCloseFunc={() => this.closePreview()}
          />
        )}
        <PatientListMainWrapper>
          <PatientListWrapper
            onClick={() => selectPatientForCheckout(data.patientId)}
          >
            <div style={{ display: "flex", alignItems: "center" }}>
              {!this.props.profile && (
                <>
                  {data.selected ? (
                    <RadioEnable>
                      <img style={{ width: "11px" }} src={Tick} alt="Tick" />
                    </RadioEnable>
                  ) : (
                    <RadioPatient />
                  )}
                </>
              )}
              <div className="patient-info">
                <span>{data.patientName}</span>
                <span>
                  {data.relationName}
                  {data.age && `, ${data.age} yrs old, ${data.genderName}`}
                </span>
              </div>
            </div>
            <Kebab
              togglePatientDetailsSection={
                this.props.togglePatientDetailsSection
              }
              patientId={data.patientId}
              data={data}
              deletePatientHandler={this.props.deletePatientHandler}
            />
          </PatientListWrapper>
          {this.props.prescriptions?.ActiveRx?.length > 0 && (
            <PrescriptionWrapper>
              <p>{this.props.prescriptions.ActiveRx.length} Prescriptions</p>
              <ScrollContainer
                horizontal={true}
                hideScrollbars={true}
                style={{ display: "flex", gap: "1rem" }}
              >
                {this.props.prescriptions.ActiveRx.map((img) => (
                  <img
                    src={`${Image_URL}${img.imagePath}`}
                    alt="images"
                    style={{
                      width: "60px",
                      height: "70px",
                      objectFit: "cover",
                      cursor: "pointer",
                    }}
                    onClick={() =>
                      this.openPreview(`${Image_URL}${img.imagePath}`)
                    }
                  />
                ))}
              </ScrollContainer>
            </PrescriptionWrapper>
          )}
        </PatientListMainWrapper>
      </>
    );
  }
}

export default PatientListCard;

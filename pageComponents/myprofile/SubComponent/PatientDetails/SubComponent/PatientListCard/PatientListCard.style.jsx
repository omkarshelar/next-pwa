import styled from "styled-components";

export const PatientListMainWrapper = styled.div`
  background: #ffffff;
  border: 1px solid rgba(0, 0, 0, 0.1);
  box-sizing: border-box;
  border-radius: 4px;
  margin: 10px 0;
  /* padding: 10px; */
  padding: 0px 10px;
`;

export const PatientListWrapper = styled.div`
  height: 70px;
  display: flex;
  align-items: center;
  justify-content: space-between;

  div[class="patient-info"] {
    display: flex;
    flex-flow: column;
    margin-left: 2rem;
    > span:first-child {
      font-weight: 600;
      /* font-family: "Century Gothic", sans-serif; */
    }
    > span:last-child {
      color: #999999;
      /* font-family: "Century Gothic", sans-serif; */
    }
  }
`;

export const RadioPatient = styled.div`
  width: 20px;
  height: 20px;
  border-radius: 50%;
  cursor: pointer;
  border: 1px solid #999999;
  box-sizing: border-box;
`;

export const RadioEnable = styled.div`
  width: 20px;
  height: 20px;
  border-radius: 50%;
  cursor: pointer;
  border: 1px solid #30b94f;
  box-sizing: border-box;
  background-color: #30b94f;
  display: flex;
  justify-content: center;
  align-items: center;
  > img {
    width: 11px;
  }
`;

export const PrescriptionWrapper = styled.div`
  width: 92%;
  margin: 0 auto;
  padding-bottom: 20px;
  > p {
    font-size: 14px;
    font-weight: 600;
    margin: 0px 0px 10px 0px;
    padding: 0px;
    color: #999;
  }
  @media screen and (max-width: 568px) {
    > p {
      font-size: 12px;
    }
  }
`;

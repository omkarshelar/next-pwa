import styled from "styled-components";
import FormControl from "@material-ui/core/FormControl";
import TextField from "@material-ui/core/TextField";

export const PatientDetailsWrapper = styled.div`
  box-sizing: border-box;
  background: #ffffff;
  box-shadow: 0px 20px 50px rgba(87, 100, 173, 0.22);
  border-radius: 4px;
  padding: 30px 15px 15px 15px;
  margin: 15px 0;
  min-height: 270px;
  > header {
    /* font-family: "Raleway", sans-serif; */
    font-style: normal;
    font-weight: 600;
    font-size: 16px;
    line-height: 130.2%;
    margin-bottom: 5px;
    @media screen and (max-width: 568px) {
      font-size: 14px;
    }
  }
  @media screen and (max-width: 568px) {
    padding: 15px 15px;
  }
`;

export const FirstColumnCard = styled.div`
  box-sizing: border-box;
  display: flex;
  justify-content: space-between;
  @media screen and (max-width: 568px) {
    flex-flow: column;
  }
`;

export const ProfileFirstColumnCard = styled.div`
  box-sizing: border-box;
  display: flex;
  align-items: center;
  justify-content: center;
  margin-top: 40px;
  /* justify-content: space-between; */
  gap: 6rem;
  @media screen and (max-width: 1024px) {
    gap: 4rem;
  }
  @media screen and (max-width: 568px) {
    flex-flow: column;
    gap: 1rem;
    margin-top: 10px;
  }
`;

export const CardWrapper = styled.div`
  display: flex;
  flex-flow: column;
  padding: 5px;
`;

export const Relation = styled(FormControl)`
  box-sizing: border-box;
  width: 38%;
  margin-left: 2% !important;
  .MuiInputLabel-outlined.MuiInputLabel-shrink {
    background-color: white;
    padding: 0 5px;
  }
  @media screen and (max-width: 568px) {
    margin-left: 0 !important;
    width: 100%;
    margin-top: 8px !important;
    margin-bottom: 8px !important;
  }
`;

export const FullName = styled(TextField)`
  box-sizing: border-box;
  width: 48%;
  margin-right: 2% !important;
  @media screen and (max-width: 568px) {
    margin-right: 0 !important;
    margin-top: 8px !important;
    margin-bottom: 8px !important;
    width: 100%;
  }
`;

export const ProfileDetailsField = styled(TextField)`
  box-sizing: border-box;
  width: 50%;
  @media screen and (max-width: 568px) {
    margin-right: 0 !important;
    width: 100%;
  }
`;

export const SecondColumnCard = styled.div`
  display: flex;
  margin-top: 10px;
  @media screen and (max-width: 568px) {
    margin-top: 0px;
  }
`;

export const ButtonWrapper = styled.div`
  display: flex;
  gap: 20px;
  margin: 10px 10px 0px 0px;
  width: 45%;
  max-height: 60px;
  @media screen and (max-width: 568px) {
    width: 100%;
  }
`;

export const ProfileButtonWrapper = styled.div`
  display: flex;
  gap: 20px;
  margin-top: 40px;
  width: 45%;
  max-height: 60px;
  @media screen and (max-width: 568px) {
    width: 100%;
    margin-top: 20px;
  }
`;

export const TextFieldCustom = styled(TextField)`
  @media screen and (max-width: 568px) {
    margin-top: 8px !important;
    margin-bottom: 8px !important;
  }
`;

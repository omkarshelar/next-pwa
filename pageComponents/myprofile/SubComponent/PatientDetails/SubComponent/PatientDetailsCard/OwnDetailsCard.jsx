import React, { Component } from "react";
import {
  FirstColumnCard,
  PatientDetailsWrapper,
  CardWrapper,
  SecondColumnCard,
  ButtonWrapper,
  FullName,
  Relation,
  TextFieldCustom,
} from "../PatientDetailsCard/PatientDetailsCard.style";
import "./Material.css";
import * as Yup from "yup";
import { Formik } from "formik";
import FormControlLabel from "@material-ui/core/FormControlLabel";
import Radio from "@material-ui/core/Radio";
import RadioGroup from "@material-ui/core/RadioGroup";
import Button from "../../../../../../components/WebComponents/Button/Button";
import { EMAIL_PATTERN } from "../../RegexPattern/emailPattern";

export class OwnDetailsCard extends Component {
  state = {
    isSubmitted: false,
  };

  handleSubmitHandler = (data) => {
    this.props.submitHandler(data);
  };
  render() {
    let { initialState } = this.props;
    return (
      <PatientDetailsWrapper>
        <header>{this.props.header}</header>
        <Formik
          initialValues={initialState}
          validationSchema={Yup.object().shape({
            age: Yup.number()
              .typeError("Age must be a number")
              .required("Age is required")
              .min(1, "You must be at least 1 years")
              .max(120, "You must be at most 120 years"),
            fullName: Yup.string().required("Name is required"),
            email: Yup.string()
              .matches(EMAIL_PATTERN, "Must be a valid email")
              .email("Must be a valid email")
              .max(255)
              .required("Please provide your email"),
          })}
          onSubmit={(values) => {
            this.handleSubmitHandler(values);
          }}
        >
          {({ errors, handleSubmit, touched, values, handleChange }) => (
            <form noValidate onSubmit={handleSubmit}>
              <CardWrapper>
                <FirstColumnCard>
                  <FullName
                    id="standard-basic"
                    name="fullName"
                    label="Full Name"
                    value={values.fullName}
                    onChange={handleChange}
                    error={Boolean(touched.fullName && errors.fullName)}
                    helperText={touched.fullName && errors.fullName}
                    className="full-name-input"
                    inputProps={{ maxLength: 50 }}
                    variant="outlined"
                  />

                  <TextFieldCustom
                    id="standard-basic"
                    name="age"
                    label="Age"
                    value={values.age}
                    onChange={handleChange}
                    error={Boolean(touched.age && errors.age)}
                    helperText={touched.age && errors.age}
                    style={{ minWidth: "10%" }}
                    inputProps={{ maxLength: 3 }}
                    variant="outlined"
                  />
                  <Relation>
                    <TextFieldCustom
                      id="standard-basic"
                      type="email"
                      name="email"
                      label="Email ID"
                      value={values.email}
                      onChange={handleChange}
                      error={Boolean(touched.email && errors.email)}
                      helperText={touched.email}
                      width={50}
                      variant="outlined"
                    />
                  </Relation>
                </FirstColumnCard>

                <SecondColumnCard>
                  <RadioGroup
                    aria-label="gender"
                    name="gender"
                    onChange={handleChange}
                    value={values.gender}
                    style={{
                      flexDirection: "row",
                      alignItems: "center",
                      color: "#4F4F4F",
                      minWidth: "50%",
                    }}
                  >
                    <FormControlLabel
                      value="8"
                      control={<Radio />}
                      label="Male"
                    />
                    <FormControlLabel
                      value="9"
                      control={<Radio />}
                      label="Female"
                    />
                    <FormControlLabel
                      value="10"
                      control={<Radio />}
                      label="Other"
                    />
                  </RadioGroup>
                </SecondColumnCard>
              </CardWrapper>
              <ButtonWrapper>
                {/* <Button
                  BtnAdd
                  onClick={() => {
                    this.props.closePatientDetailsSection();
                  }}
                >
                  close
                </Button> */}
                <Button BtnContinue type="submit">
                  Save
                </Button>
              </ButtonWrapper>
            </form>
          )}
        </Formik>
      </PatientDetailsWrapper>
    );
  }
}

export default OwnDetailsCard;

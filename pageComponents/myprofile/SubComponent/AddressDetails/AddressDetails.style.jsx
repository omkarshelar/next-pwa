import styled from "styled-components";

export const AddressWrapper = styled.div`
  width: 100%;

  > .addressDetailsAddLoader {
    width: 32%;
    height: 25px;
    border-radius: 4px;
  }

  > header {
    font-weight: 600;
    font-size: large;
    @media screen and (max-width: 568px) {
      margin-left: 10px;
      font-size: 14px;
    }
  }
  > header > span {
    color: #0071bc;
    font-size: medium;
    margin-left: 10px;
    cursor: pointer;
    @media screen and (max-width: 568px) {
      font-size: 14px;
    }
  }
  /* @media screen and (max-width: 568px) {
    padding-left: 10px;
  } */
`;

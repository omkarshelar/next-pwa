import React, { Component } from "react";
import { connect } from "react-redux";
import { AddressWrapper } from "./AddressDetails.style";
import Button from "../../../../components/WebComponents/Button/Button";
import AdressList from "./Subcomponents/AddressList/AdressList";
import AddAdressCard from "./Subcomponents/AddressCard/AddAdressCard";
import {
  toOrderSummaryAction,
  toPatientDetailsAction,
} from "../../../../redux/Timeline/Action";
import Router, { withRouter } from "next/router";
import EditAddressCard from "./Subcomponents/AddressCard/EditAddressCard";
import { message } from "antd";
import {
  fetchAddressThunk,
  checkPincodeThunk,
  deleteAddressThunk,
  addAddressThunk,
  saveAddressThunk,
  selectAdresssForCheckoutAction,
} from "../../../../redux/AddressDetails/Action";
import { SubmitWrapper } from "../PatientDetails/PatientDetails.style";
import Shimmer from "react-shimmer-effect";
import DetailsCardLoader from "../../../../components/WebComponents/Shimmer/DetailsCardLoader";
import {
  eventAddressAdded,
  eventAddressdeleted,
  eventAddressSelected,
  eventSummaryScreen,
  // hjPageChange,
} from "../../../../Events/Events";
import {
  pincodeSectionAction,
  saveSelectedAddress,
} from "../../../../redux/Pincode/Actions";
import { fetchMedicineThunkPostSelect } from "../../../../redux/Pincode/Actions";
import { HiPlusSm } from "react-icons/hi";
import window from "global";

let isLocalUpdate = false;
export class AddressDetails extends Component {
  state = {
    addAddressSection: false,
    selectedAddress: null,
    initialForaddAddress: {
      pincode: null,
      building: "",
      area: "",
      landmark: "",
      addressType: null,
      other: null,
    },
    initialForEditAddress: {
      pincode: null,
      building: "",
      area: "",
      landmark: "",
      addressType: null,
      other: null,
    },
    loading: false,
  };
  fetchAddressData = () => {
    if (this.props.accessToken) {
      let data = {
        accessToken: this.props.accessToken,
        history: this.props.history,
      };
      this.props.fetchAddressThunk(data).then(() => {
        if (this.props.addressData.fetchAddressError) {
          message.warning("something went wrong");
        } else {
          this.closeAddAddressSection();

          if (this.props.pincodeSelectedAddress) {
            this.saveAddressForCheckout(
              this.props.pincodeSelectedAddress.addressId
            );
          }
        }
      });
    }
  };

  componentDidMount() {
    window.scrollTo(0, 0);
    window.history.pushState(null, null, window.location.pathname);
    window.addEventListener("popstate", this.onBackButtonEvent);
    this.fetchAddressData();
    //page change for hotjar
    // hjPageChange(window.location.origin + "/orderflow#address");
  }

  // Clear State after unmounting
  componentWillUnmount = () => {
    window.removeEventListener("popstate", this.onBackButtonEvent);
  };

  componentDidUpdate(prevProps, prevState) {
    if (
      this.props.pincodeDetails.warehouseId &&
      prevProps.pincodeDetails != this.props.pincodeDetails
    ) {
      if (isLocalUpdate && this.props.cartContent.length > 0) {
        isLocalUpdate = false;
        let medList = [];
        this.props.cartContent.map((e) => {
          medList.push(e._source.original_product_code);
        });

        let data = {
          query: {
            bool: {
              should: [{ terms: { "original_product_code.keyword": medList } }],
            },
          },
        };
        let params = {};
        params.medicine = data;
        params.warehouseId = this.props.pincodeDetails.warehouseId;
        params.cartContent = this.props.cartContent;
        if (this.props.accessToken) {
          params.access_token = this.props.accessToken;
        }
        this.props.fetchMedicineThunkPostSelect(params);
      }
    }
    if (
      JSON.stringify(this.props.pincodeSelectedAddress) !=
      JSON.stringify(prevProps.pincodeSelectedAddress)
    ) {
      this.saveAddressForCheckout(this.props.pincodeSelectedAddress.addressId);
    }
  }
  // On Back Button click Event.
  onBackButtonEvent = (e) => {
    e.preventDefault();
    if (!this.isBackButtonClicked) {
      this.props.toPatientDetailsAction();
      this.isBackButtonClicked = false;
    }
  };

  closeAddAddressSection = () => {
    this.setState({
      addAddressSection: false,
      selectedAddress: null,
      initialForaddAddress: {
        pincode: null,
        building: "",
        area: "",
        landmark: "",
        addressType: null,
        other: null,
      },
      initialForEditAddress: {
        pincode: null,
        building: "",
        area: "",
        landmark: "",
        addressType: null,
        other: null,
      },
    });
  };

  saveAddressForCheckout = (addressId) => {
    // let payload = {
    //   accessToken: this.props.accessToken,
    //   addressid: addressId,
    //   history: this.props.history,
    //   orderid: this.props.orderTypeOneOrderId || this.props.orderTypeTwoOrderId,
    // };

    // this.props.saveAddressThunk(payload);

    if (this.props.addressList) {
      let idFound = this.props.addressList.find(
        (data) => data.addressId === addressId
      );

      if (idFound) {
        this.props.selectAdresssForCheckoutAction(idFound);
      }
    }
  };
  // *

  addNewAddressHandler = (data, address) => {
    let { orderTypeOneOrderId, orderTypeTwoOrderId } = this.props;

    let payload = {
      accessToken: this.props.accessToken,
      addressData: {
        addressType: address === "Other" ? data.other : address,
        addressline1: data.building,
        addressline2: data.area,
        landmark: data.landmark,
        pincode: data.pincode,
        addressId: 0,
      },
      history: this.props.history,
      isAppEdit: false,
    };

    if (orderTypeOneOrderId || orderTypeTwoOrderId) {
      payload.orderId = orderTypeOneOrderId
        ? orderTypeOneOrderId
        : orderTypeTwoOrderId;
    }
    // if (orderTypeOneOrderId || orderTypeTwoOrderId) {
    this.props.addAddressThunk(payload).then(() => {
      if (this.props.addressData.newAddressError) {
        message.warning(this.props.addressData?.newAddressError);
      } else if (
        Object.keys(this.props.addressData?.newAddressSuccess).length == 0
      ) {
        message.error("Address addition failed.");
      } else {
        // Event_Truemeds
        eventAddressAdded();
        this.fetchAddressData();
        message.success("Address added successfully");
      }
    });
    // } else {
    //   message.error("OrderId missing");
    // }
  };

  deleteAddressHandler = (addressId) => {
    let payload = {
      accessToken: this.props.accessToken,
      addressId: addressId,
      history: this.props.history,
    };
    if (this.props.addressList.length > 1) {
      this.props.deleteAddressThunk(payload).then(() => {
        if (this.props.addressData.deleteAddressError) {
          message.warning(this.props.addressData?.deleteAddressError);
        } else {
          // Event_Truemeds
          eventAddressdeleted();
          this.fetchAddressData();
          message.success("Address deleted successfully");
        }
      });
    } else {
      message.warn(
        "You cannot edit or delete an Address that is used in an active order."
      );
    }
  };

  routeToSummary = () => {
    this.setState({
      loading: true,
    });
    let { accessToken, orderTypeOneOrderId, orderTypeTwoOrderId, addressList } =
      this.props;

    let idFound = addressList.find((data) => data.selected === true);
    if (idFound?.addressId) {
      if (accessToken && (orderTypeOneOrderId || orderTypeTwoOrderId)) {
        let payload = {
          accessToken: this.props.accessToken,
          addressid: idFound.addressId,
          history: this.props.history,
          orderid:
            this.props.orderTypeOneOrderId || this.props.orderTypeTwoOrderId,
        };

        this.props.saveAddressThunk(payload).then(() => {
          if (this.props.addressData?.checkoutAddressError) {
            message.error(this.props.addressData?.checkoutAddressError);
          } else {
            // Event_Truemeds
            eventAddressSelected();
            // Event_Truemeds
            eventSummaryScreen();
            this.props.toOrderSummaryAction();
          }
        });
      }
    } else {
      message.warn("Please select an Address");
      this.setState({
        loading: false,
      });
    }
  };
  toggleAddressSection = (data) => {
    this.setState({
      selectedAddress: data.addressId,
      initialForEditAddress: Object.assign(
        {},
        this.state.initialForEditAddress,
        {
          pincode: data.pincode,
          building: data.addressline1,
          area: data.addressline2,
          landmark: data.landmark,
          other:
            data.addressType !== "Office" && data.addressType !== "Home"
              ? data.addressType
              : null,
          addressType:
            data.addressType !== "Office" && data.addressType !== "Home"
              ? "Other"
              : data.addressType,
        }
      ),
    });
  };

  editAddressHandler = (content, data) => {
    let { orderTypeOneOrderId, orderTypeTwoOrderId } = this.props;

    let payload = {
      accessToken: this.props.accessToken,
      addressData: {
        addressType:
          this.state.initialForEditAddress.addressType === "Other"
            ? content.other
            : this.state.initialForEditAddress.addressType,
        addressline1: content.building,
        addressline2: content.area,
        landmark: content.landmark,
        pincode: content.pincode,
        addressId: data.addressId,
      },
      isAppEdit: false,
      history: this.props.history,
    };

    if (orderTypeOneOrderId || orderTypeTwoOrderId) {
      payload.orderId = orderTypeOneOrderId
        ? orderTypeOneOrderId
        : orderTypeTwoOrderId;
    }

    this.props.addAddressThunk(payload).then(() => {
      if (this.props.addressData.newAddressError) {
        message.warning(this.props.addressData?.newAddressError);
      } else {
        this.fetchAddressData();
        message.success("Address updated successfully");
      }
    });
  };

  handleChangeForAddressType = (e) => {
    this.setState({
      initialForEditAddress: {
        ...this.state.initialForEditAddress,
        addressType: e.target.value,
      },
    });
  };

  savePincode = (data) => {
    isLocalUpdate = true;
    this.props.pincodeSectionAction(data);
  };
  handleSelectAddress = (addressData, params) => {
    let idFound = this.props.addressList.find(
      (data) => data.addressId === addressData.addressId
    );
    this.props.selectAdresssForCheckoutAction(idFound);
    this.props.saveSelectedAddress(addressData);
    this.savePincode(params);
  };
  render() {
    let { addressList } = this.props;
    return (
      <AddressWrapper>
        {this.props.isLoading ? (
          <Shimmer>
            <div className="addressDetailsAddLoader" />
          </Shimmer>
        ) : (
          addressList?.length > 0 && (
            <header>
              {this.props.profile ? null : "Select Address"}
              <span
                style={{
                  margin: this.props.profile ? "0" : "",
                  display: "flex",
                }}
                onClick={() => this.setState({ addAddressSection: true })}
              >
                <HiPlusSm className="plusIcon" /> Add Address
              </span>
            </header>
          )
        )}
        {this.state.addAddressSection && (
          <AddAdressCard
            initialState={this.state.initialForaddAddress}
            header="Add Address Details"
            closeHandler={this.closeAddAddressSection}
            addNewAddressHandler={this.addNewAddressHandler}
            checkpincode={this.props.checkPincodeThunk}
            accessToken={this.props.accessToken}
            isServiceable={this.props.isServiceable}
            pincodeError={this.props.addressData.pincodeError}
            isNewUser={false}
          />
        )}
        {this.props.isLoading ? (
          <>
            <DetailsCardLoader onlyCard={true} allDetails={true} />
            <DetailsCardLoader onlyCard={true} allDetails={false} />
          </>
        ) : (
          <div>
            {addressList?.length > 0 ? (
              addressList.map((data) => (
                <>
                  <AdressList
                    profile={this.props.profile}
                    data={data}
                    saveAddressForCheckout={this.saveAddressForCheckout}
                    deleteAddressHandler={this.deleteAddressHandler}
                    toggleAddressSection={this.toggleAddressSection}
                    savePincode={this.savePincode}
                    pincodeSelectedAddress={this.props.pincodeSelectedAddress}
                    handleSelectAddress={this.handleSelectAddress}
                  />
                  {data.addressId === this.state.selectedAddress && (
                    <EditAddressCard
                      header="Edit Address"
                      closeAddressSection={this.closeAddAddressSection}
                      submitHandler={this.editAddressHandler}
                      initialState={this.state.initialForEditAddress}
                      data={data}
                      handleChangeForAddressType={
                        this.handleChangeForAddressType
                      }
                    />
                  )}
                </>
              ))
            ) : (
              <AddAdressCard
                initialState={this.state.initialForaddAddress}
                header="Add Address Detailss"
                closeHandler={this.closeAddAddressSection}
                addNewAddressHandler={this.addNewAddressHandler}
                checkpincode={this.props.checkPincodeThunk}
                accessToken={this.props.accessToken}
                isServiceable={this.props.isServiceable}
                pincodeError={this.props.addressData.pincodeError}
                isNewUser={true}
              />
            )}
          </div>
        )}
        {!this.props.profile &&
          addressList?.length > 0 &&
          !this.state.selectedAddress &&
          !this.state.addAddressSection && (
            <SubmitWrapper>
              <Button
                id="addMedBtn"
                BtnAdd
                onClick={() => {
                  if (this.props.Stepper.onOrderSummary) {
                    Router.push("/?search=true");
                  } else {
                    Router.push({
                      pathname: "/",
                      search: "?search=true",
                      state: {
                        addressDetails: true,
                      },
                    });
                  }
                }}
              >
                Add More Medicines
              </Button>
              <Button
                BtnContinue
                loading={this.state.loading}
                disabled={this.state.loading}
                onClick={() => this.routeToSummary()}
              >
                Continue
              </Button>
            </SubmitWrapper>
          )}
      </AddressWrapper>
    );
  }
}

let mapStateToProps = (state) => ({
  accessToken: state.loginReducer.verifyOtpSuccess?.Response?.access_token,
  isServiceable: state.addressData.pincodeSuccess,
  addressList: state.addressData.fetchAddressSuccess,
  addressData: state.addressData,
  orderTypeOneOrderId: state.uploadImage?.orderId,
  orderTypeTwoOrderId: state.confirmMedicineReducer.ConfirmMedData?.orderId,
  Stepper: state.stepper,
  isLoading: state.loader.isLoading,
  cartContent: state.cartData?.cartItems,
  pincodeDetails: state.pincodeData?.pincodeData,
  pincodeSelectedAddress: state.pincodeSelctedAddress?.pincodeSelectedAddress,
});

export default withRouter(
  connect(mapStateToProps, {
    saveSelectedAddress,
    fetchAddressThunk,
    checkPincodeThunk,
    pincodeSectionAction,
    deleteAddressThunk,
    addAddressThunk,
    saveAddressThunk,
    toOrderSummaryAction,
    toPatientDetailsAction,
    selectAdresssForCheckoutAction,
    fetchMedicineThunkPostSelect,
  })(AddressDetails)
);

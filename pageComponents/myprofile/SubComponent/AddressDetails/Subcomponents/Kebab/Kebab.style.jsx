import styled from "styled-components";

// Parent wrapper.
export const KebabContainer = styled.div`
  display: flex;
  flex-flow: column;
  width: 110px;
  background: #2a2b2d;
  box-shadow: 0px 20px 40px rgba(43, 44, 46, 0.08);
  border-radius: 4px;
  padding: 15px;
  box-sizing: border-box;
  right: 0;
  z-index: 900;
  position: absolute;

  > p {
    /* font-family: "Raleway", sans-serif; */
    font-style: normal;
    font-weight: 600;
    font-size: 12px;
    line-height: 14px;
    letter-spacing: 1px;
    text-align: left;
    margin-bottom: 15px;
    color: #fff;
  }
  > p:last-child {
    margin-bottom: 0;
  }
`;

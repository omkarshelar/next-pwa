import React, { useEffect, useRef } from "react";
import IconButton from "@material-ui/core/IconButton";
import MoreVertIcon from "@material-ui/icons/MoreVert";
import { KebabContainer } from "./Kebab.style";
import edit from "./edit-3.svg";
import trash from "./trash-2.svg";

export default function KebabAddress(props) {
  const [anchorEl, setAnchorEl] = React.useState(false);
  const node = useRef();

  const handleClick = (e) => {
    if (node.current?.contains(e.target)) {
      return;
    }
    setAnchorEl(false);
  };

  useEffect(() => {
    document.addEventListener("mousedown", handleClick);
    return () => {
      document.removeEventListener("mousedown", handleClick);
    };
  }, []);
  return (
    <div style={{ position: "relative", zIndex: 1 }}>
      <IconButton
        aria-label="more"
        aria-controls="long-menu"
        aria-haspopup="true"
        id={`drop${props.addressId}`}
        onClick={(e) => setAnchorEl(!anchorEl)}
      >
        <MoreVertIcon />
      </IconButton>
      {anchorEl && (
        <>
          <KebabContainer ref={node}>
            <p
              onClick={() => {
                props.toggleAddressSection(props.data);
                setAnchorEl(false);
              }}
              style={{ cursor: "pointer" }}
            >
              <img src={edit} alt="edit"></img> Edit
            </p>
            <p
              onClick={async () => {
                props.deleteAddressHandler(props.addressId);
                setAnchorEl(false);
              }}
              style={{ cursor: "pointer" }}
            >
              <img src={trash} alt="delete"></img> Delete
            </p>
          </KebabContainer>
        </>
      )}
    </div>
  );
}

import React, { Component } from "react";
import * as Yup from "yup";
import { Formik } from "formik";
import {
  AddAddressWrapper,
  ButtonWrapper,
  FirstColumnCard,
  SecondColumnCard,
  CardWrapper,
  ThirdRow,
  TextFieldCustomAddress,
} from "./AddAddressCard.style";
import FormControlLabel from "@material-ui/core/FormControlLabel";
import Radio from "@material-ui/core/Radio";
import RadioGroup from "@material-ui/core/RadioGroup";
import TextField from "@material-ui/core/TextField";
import Button from "../../../../../../components/WebComponents/Button/Button";
import Home from "../../../../../../src/Assets/HomeNew.png";
import Office from "../../../../../../src/Assets/OfficeNew.png";
import Pin from "../../../../../../src/Assets/PinNew.png";
import "./AdressCard.css";

export class EditAddressCard extends Component {
  state = {
    isSubmitted: false,
  };

  handleSubmitHandler = (content, data) => {
    this.props.submitHandler(content, data);
  };

  render() {
    let { initialState } = this.props;
    return (
      <AddAddressWrapper>
        <header>{this.props.header}</header>
        <Formik
          initialValues={initialState}
          validationSchema={Yup.object().shape({
            pincode: Yup.number().required(),
            building: Yup.string()
              .max(255, "Only 255 characters allowed")
              .required(),
            landmark: Yup.string()
              .max(255, "Only 255 characters allowed")
              .required(),
            area: Yup.string()
              .max(255, "Only 255 characters allowed")
              .required(),
            other:
              this.state.addressType === "Other" && Yup.string().required(),
          })}
          onSubmit={(values) => {
            this.handleSubmitHandler(values, this.props.data);
          }}
        >
          {({ errors, handleSubmit, touched, values, handleChange }) => (
            <form noValidate onSubmit={handleSubmit}>
              <ThirdRow>
                <RadioGroup
                  aria-label="addressType"
                  name="addressType"
                  onChange={this.props.handleChangeForAddressType}
                  value={this.props.initialState.addressType}
                  style={{
                    flexDirection: "row",
                    alignItems: "center",
                    color: "#4F4F4F",
                    minWidth: "50%",
                  }}
                >
                  <FormControlLabel
                    value="Home"
                    control={<Radio />}
                    label={
                      <>
                        <img
                          src={Home}
                          alt="Home"
                          style={{ width: "20px", marginRight: "5px" }}
                        ></img>
                        Home
                      </>
                    }
                  />
                  <FormControlLabel
                    value="Office"
                    control={<Radio />}
                    label={
                      <>
                        <img
                          src={Office}
                          alt="Office"
                          style={{ width: "20px", marginRight: "5px" }}
                        ></img>
                        Office
                      </>
                    }
                  />
                  <FormControlLabel
                    value="Other"
                    control={<Radio />}
                    label={
                      <>
                        <img
                          src={Pin}
                          alt="Pin"
                          style={{ width: "20px", marginRight: "5px" }}
                        ></img>
                        {this.props.initialState.addressType === "Other" ? (
                          <TextField
                            id="standard-basic"
                            name="other"
                            placeholder="Type Here"
                            value={values.other}
                            onChange={handleChange}
                            error={Boolean(touched.other && errors.other)}
                            inputProps={{ maxLength: 10 }}
                            autoComplete="off"
                          />
                        ) : (
                          "Other"
                        )}
                      </>
                    }
                  />
                </RadioGroup>
              </ThirdRow>
              <CardWrapper>
                <FirstColumnCard>
                  <TextFieldCustomAddress
                    variant="outlined"
                    id="standard-basic"
                    name="pincode"
                    label="Pincode"
                    value={values.pincode}
                    onChange={handleChange}
                    error={Boolean(touched.pincode && errors.pincode)}
                    className="full-name-input"
                    disabled={true}
                  />

                  <TextFieldCustomAddress
                    variant="outlined"
                    id="standard-basic"
                    name="area"
                    label="Locality, Area"
                    value={values.area}
                    onChange={handleChange}
                    error={Boolean(touched.area && errors.area)}
                    helperText={touched.area && errors.area}
                    className="full-name-input"
                    autoComplete="off"
                  />
                </FirstColumnCard>

                <SecondColumnCard>
                  <TextFieldCustomAddress
                    variant="outlined"
                    id="standard-basic"
                    name="building"
                    label="Flat No, Building Name"
                    value={values.building}
                    onChange={handleChange}
                    error={Boolean(touched.building && errors.building)}
                    autoComplete="off"
                  />
                  <TextFieldCustomAddress
                    variant="outlined"
                    id="standard-basic"
                    name="landmark"
                    label="Landmark"
                    value={values.landmark}
                    onChange={handleChange}
                    error={Boolean(touched.landmark && errors.landmark)}
                    helperText={touched.landmark && errors.landmark}
                    autoComplete="off"
                  />
                </SecondColumnCard>
              </CardWrapper>
              <ButtonWrapper>
                <Button
                  BtnAdd
                  onClick={() => {
                    this.props.closeAddressSection();
                  }}
                >
                  close
                </Button>
                <Button BtnContinue type="submit">
                  Save
                </Button>
              </ButtonWrapper>
            </form>
          )}
        </Formik>
      </AddAddressWrapper>
    );
  }
}

export default EditAddressCard;

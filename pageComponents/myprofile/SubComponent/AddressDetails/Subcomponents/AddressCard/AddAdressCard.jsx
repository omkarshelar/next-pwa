import React, { Component } from "react";
import * as Yup from "yup";
import { Formik } from "formik";
import {
  AddAddressWrapper,
  ButtonWrapper,
  FirstColumnCard,
  SecondColumnCard,
  CardWrapper,
  ThirdRow,
  TextFieldCustomAddress,
} from "./AddAddressCard.style";
import FormControlLabel from "@material-ui/core/FormControlLabel";
import InputAdornment from "@material-ui/core/InputAdornment";
import Radio from "@material-ui/core/Radio";
import RadioGroup from "@material-ui/core/RadioGroup";
import TextField from "@material-ui/core/TextField";
import Router, { withRouter } from "next/router";

import Button from "../../../../../../components/WebComponents/Button/Button";
import Home from "../../../../../../src/Assets/HomeNew.png";
import Office from "../../../../../../src/Assets/OfficeNew.png";
import Pin from "../../../../../../src/Assets/PinNew.png";
import "./AdressCard.css";
import WarningIcon from "@material-ui/icons/Warning";

export class AddAdressCard extends Component {
  state = {
    isSubmitted: false,
    addressType: "Home",
  };

  handleSubmitHandler = (data) => {
    this.props.addNewAddressHandler(data, this.state.addressType);
  };

  handleChangeForAddressType = (e) => {
    this.setState({ addressType: e.target.value });
  };

  render() {
    let { initialState, isServiceable, pincodeError, isNewUser } = this.props;

    return (
      <AddAddressWrapper>
        <header>
          {this.props.header}{" "}
          {isServiceable?.isServiceable === false && (
            <span>
              We're expanding but currently your location is not serviceable
            </span>
          )}
          {pincodeError && <span>{pincodeError}</span>}
        </header>
        <Formik
          initialValues={initialState}
          validationSchema={Yup.object().shape({
            pincode: Yup.number().required(),
            building: Yup.string()
              .max(255, "Only 255 characters allowed")
              .required(),
            landmark: Yup.string()
              .max(255, "Only 255 characters allowed")
              .required(),
            area: Yup.string()
              .max(255, "Only 255 characters allowed")
              .required(),
            other:
              this.state.addressType === "Other" && Yup.string().required(),
          })}
          onSubmit={(values) => {
            this.handleSubmitHandler(values);
          }}
        >
          {({ errors, handleSubmit, touched, values, handleChange }) => (
            <form noValidate onSubmit={handleSubmit}>
              <ThirdRow>
                <RadioGroup
                  aria-label="addressType"
                  name="addressType"
                  onChange={this.handleChangeForAddressType}
                  value={this.state.addressType}
                  style={{
                    flexDirection: "row",
                    alignItems: "center",
                    color: "#4F4F4F",
                    minWidth: "50%",
                  }}
                >
                  <FormControlLabel
                    value="Home"
                    control={<Radio />}
                    label={
                      <>
                        <img
                          src={Home}
                          alt="Home"
                          style={{ width: "20px", marginRight: "5px" }}
                        ></img>
                        Home
                      </>
                    }
                  />
                  <FormControlLabel
                    value="Office"
                    control={<Radio />}
                    label={
                      <>
                        <img
                          src={Office}
                          alt="Office"
                          style={{ width: "20px", marginRight: "5px" }}
                        ></img>
                        Office
                      </>
                    }
                  />
                  <FormControlLabel
                    value="Other"
                    control={<Radio />}
                    label={
                      <>
                        <img
                          src={Pin}
                          alt="Pin"
                          style={{ width: "20px", marginRight: "5px" }}
                        ></img>
                        {this.state.addressType === "Other" ? (
                          <TextField
                            id="standard-basic"
                            name="other"
                            placeholder="Type Here"
                            value={values.other}
                            onChange={handleChange}
                            error={Boolean(touched.other && errors.other)}
                            inputProps={{ maxLength: 10 }}
                          />
                        ) : (
                          "Other"
                        )}
                      </>
                    }
                  />
                </RadioGroup>
              </ThirdRow>
              <CardWrapper>
                <FirstColumnCard>
                  <TextFieldCustomAddress
                    variant="outlined"
                    id="standard-basic"
                    name="pincode"
                    label="Pincode"
                    value={values.pincode}
                    autoComplete="off"
                    onChange={(e) => {
                      handleChange(e);
                      if (e.target.value.length === 6) {
                        let payload = {
                          accessToken: this.props.accessToken,
                          pincode: e.target.value,
                          history: Router,
                        };
                        this.props.checkpincode(payload);
                      }
                    }}
                    inputProps={{
                      maxLength: 6,
                    }}
                    InputProps={{
                      endAdornment: (
                        <InputAdornment position="start">
                          {values.pincode?.length === 6 && (
                            <span style={{ fontWeight: "700" }}>
                              {isServiceable?.isServiceable ? (
                                isServiceable.pincodeData
                              ) : (
                                <WarningIcon style={{ color: "#e74c3c" }} />
                              )}
                            </span>
                          )}
                        </InputAdornment>
                      ),
                    }}
                  />

                  <TextFieldCustomAddress
                    variant="outlined"
                    id="standard-basic"
                    name="area"
                    label="Locality, Area"
                    value={values.area}
                    autoComplete="off"
                    onChange={handleChange}
                    error={Boolean(touched.area && errors.area)}
                    className="full-name-input"
                    disabled={
                      !isServiceable?.isServiceable ||
                      values.pincode?.length !== 6
                    }
                  />
                </FirstColumnCard>

                <SecondColumnCard>
                  <TextFieldCustomAddress
                    variant="outlined"
                    id="standard-basic"
                    name="building"
                    label="Flat No, Building Name"
                    value={values.building}
                    autoComplete="off"
                    onChange={handleChange}
                    error={Boolean(touched.building && errors.building)}
                    disabled={
                      !isServiceable?.isServiceable ||
                      values.pincode?.length !== 6
                    }
                  />
                  <TextFieldCustomAddress
                    variant="outlined"
                    id="standard-basic"
                    name="landmark"
                    label="Landmark"
                    value={values.landmark}
                    autoComplete="off"
                    onChange={handleChange}
                    error={Boolean(touched.landmark && errors.landmark)}
                    helperText={touched.landmark && errors.landmark}
                    disabled={
                      !isServiceable?.isServiceable ||
                      values.pincode?.length !== 6
                    }
                  />
                </SecondColumnCard>
              </CardWrapper>
              <ButtonWrapper>
                {!isNewUser && (
                  <Button
                    BtnAdd
                    onClick={() => {
                      this.props.closeHandler();
                    }}
                  >
                    close
                  </Button>
                )}
                <Button BtnContinue type="submit">
                  Save
                </Button>
              </ButtonWrapper>
            </form>
          )}
        </Formik>
      </AddAddressWrapper>
    );
  }
}

export default AddAdressCard;

import styled from "styled-components";

export const AddressListWrapper = styled.div`
  height: 70px;
  background: #ffffff;
  border: 1px solid rgba(0, 0, 0, 0.1);
  box-sizing: border-box;
  border-radius: 4px;
  display: flex;
  align-items: center;
  margin: 10px 0;
  padding: 10px;
  justify-content: space-between;

  div[class="patient-info"] {
    display: flex;
    flex-flow: column;
    margin-left: 2rem;

    > span:first-child {
      font-weight: 600;
      /* font-family: "Century Gothic", sans-serif; */
    }
    > span:last-child {
      color: #999999;
      /* font-family: "Century Gothic", sans-serif; */

      @media screen and (max-width: 400px) {
        max-width: 150px;
        overflow: hidden;
        white-space: nowrap;
        text-overflow: ellipsis;
      }
    }
  }
`;

export const RadioPatient = styled.div`
  min-width: 20px;
  width: 20px;
  height: 20px;
  border-radius: 50%;
  cursor: pointer;
  border: 1px solid #999999;
  box-sizing: border-box;
`;

export const RadioEnable = styled.div`
  min-width: 20px;
  width: 20px;
  height: 20px;
  border-radius: 50%;
  cursor: pointer;
  border: 1px solid #30b94f;
  box-sizing: border-box;
  background-color: #30b94f;
  display: flex;
  justify-content: center;
  align-items: center;
  > img {
    width: 11px;
  }
`;

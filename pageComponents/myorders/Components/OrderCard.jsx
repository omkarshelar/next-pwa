import React, { Component } from "react";
import "./OrderCard.css";
import { withRouter } from "next/router";
import { connect } from "react-redux";
import Button from "../../../components/WebComponents/Button/Button";
import orderDelivered from "../../../src/Assets/order-delivered-new.svg";
import doctorCall from "../../../src/Assets/doc-call-pending-new.svg";
import pharmacistCall from "../../../src/Assets/pharma-call-pending-new.svg";
import orderProcessed from "../../../src/Assets/order-processed-new.svg";
import orderDispatched from "../../../src/Assets/order-dispatched-new.svg";
import outForDelivery from "../../../src/Assets/out-for-delivery-new.svg";
import orderCancelled from "../../../src/Assets/order-cancelled-new.svg";
import paymentPending from "../../../src/Assets/payment-pending-new.svg";
import { message } from "antd";
import { fetchOrderStatusThunk } from "../../../redux/MyOrder/Action";
import { PWA_static_URL, ThirdPartySevice_URL } from "../../../constants/Urls";
import Axios from "axios";
import window from "global";
import { getOrderStatusDetailsDataAction } from "../../../redux/storeData/Action";
const arrivingStatusArr = [1, 2, 39, 49, 58, 59, 60, 66, 142, 278];
export class OrderCard extends Component {
  state = {
    buttonDisabled: false,
  };
  convertEpoch = (epoch) => {
    var date = new Date(epoch);
    return date.toUTCString().slice(4, 16);
  };

  getOrderStatus = (id, img) => {
    let order = "";
    let orderStatus = "";
    let orderStatusImg = "";
    if (id === 1 || id === 2) {
      orderStatus = "Pharmacist Call Pending";
      orderStatusImg = pharmacistCall;
    } else if (id === 3 || id === 4) {
      orderStatus = "Order Missing Details";
      orderStatusImg = orderDispatched;
    } else if (id === 39) {
      orderStatus = "Doctor Call Pending";
      orderStatusImg = doctorCall;
    } else if (id === 49) {
      orderStatus = "Incomplete Order";
      orderStatusImg = orderDispatched;
    } else if ([55, 190, 191, 192, 199, 201, 56, 200, 121, 124].includes(id)) {
      orderStatus = "Order Delivered";
      orderStatusImg = orderDelivered;
    } else if (id === 57) {
      orderStatus = "Order Cancelled";
      orderStatusImg = orderCancelled;
    } else if (id === 58) {
      orderStatus = "Payment Pending";
      orderStatusImg = paymentPending;
    } else if (id === 59 || id === 66) {
      orderStatus = "Ready To Ship Order";
      orderStatusImg = orderDispatched;
    } else if (id === 60) {
      orderStatus = "Order Shipped";
      orderStatusImg = orderDispatched;
    } else if (id === 81) {
      orderStatus = "Order On Hold";
      orderStatusImg = orderDispatched;
    } else if (id === 142) {
      orderStatus = "Processing Order";
      orderStatusImg = orderProcessed;
    } else if (id === 278) {
      orderStatus = "Out For Delivery";
      orderStatusImg = outForDelivery;
    } else if (id === 174) {
      orderStatus = "Discarded Order";
      orderStatusImg = orderCancelled;
    }
    if (img) {
      order = orderStatusImg;
    } else {
      order = orderStatus;
    }
    return order;
  };

  getOrderDeliveryStatus = (id) => {
    let orderStatus = "";
    // if (id === 121) {
    //   orderStatus = "Return In-transit";
    // }
    if (id === 124) {
      // orderStatus = "Return Received";
      orderStatus = "Order Returned";
    } else if (id === 56 || id === 200) {
      // orderStatus = "Return Received. Refund Pending";
      orderStatus = "Refund processed";
    } else if (id === 190) {
      // orderStatus = "Return Request Pending Approval";
      orderStatus = "Return Requested";
    } else if (id === 191 || id === 121) {
      orderStatus = "Return Generated";
    } else if (id === 192) {
      orderStatus = "Return Request Declined";
    } else if (id === 199) {
      // orderStatus = "Refund Processed";
      orderStatus = "Order Refunded";
    } else if (id === 201) {
      // orderStatus = "Partial Refund Processed";
      orderStatus = "Partially Refunded";
    }
    return orderStatus;
  };

  goToOrderStatus = (orderId) => {
    this.props
      .getOrderStatusDetailsDataAction({
        isMyOrder: true,
        orderType: this.props.orderType,
        patientFilter: this.props.patientFilter,
      })
      .then(() => {
        this.props.router.push(`/orderstatus/${orderId}`);
      });
  };

  navigateToOrderDetails = (id, status, statusText) => {
    this.props
      .getOrderStatusDetailsDataAction({
        id: id,
        orderStatus: status,
        showEdd: statusText,
        isMyOrder: true,
        orderType: this.props.orderType,
        patientFilter: this.props.patientFilter,
      })
      .then(() => {
        this.props.router.push(`/myorderdetails/${id}`);
      });
    // this.props.router.push(
    //   {
    //     pathname: "/myorderdetails",
    //     query: {
    //       id: id,
    //       orderStatus: status,
    //       showEdd: statusText,
    //       isMyOrder: true,
    //       orderType: this.props.orderType,
    //       patientFilter: this.props.patientFilter,
    //     },
    //   },
    //   "/myorderdetails"
    // );
  };

  cashFreeService = (e, orderId) => {
    e.stopPropagation();
    this.setState({ buttonDisabled: true });
    this.props.setLoader(true);
    const Uri = `${ThirdPartySevice_URL}/createPaymentUrlInCashFree?orderId=${orderId}&returnUrl=${PWA_static_URL}`;
    const config = {
      headers: {
        "Content-Type": "application/json",
        Authorization: "Bearer " + this.props.token?.Response?.access_token,
        "Access-Control-Allow-Origin": "*",
      },
    };
    Axios.get(Uri, config)
      .then((response) => {
        this.setState({ buttonDisabled: false });
        this.props.setLoader(false);
        if (response.status === 200) {
          window.location.href = response.data.paymentLink;
        }
      })
      .catch((err) => {
        this.setState({ buttonDisabled: false });
        this.props.setLoader(false);
        console.error("createPaymentUrlInCashFree", err.response);
      });
  };

  trackOrderOnClick = (e) => {
    e.stopPropagation();
    this.setState({ buttonDisabled: true });
    this.props
      .fetchOrderStatusThunk({
        accessToken: this.props.token?.Response?.access_token,
        orderId: this.props.orderDetails?.orderId,
        history: this.props.router,
      })
      .then(() => {
        this.setState({ buttonDisabled: false });
        if (this.props.trackOrder?.clickPostTrackingUrl) {
          window.location.href = this.props.trackOrder.clickPostTrackingUrl;
        } else {
          message.error("No track url found");
        }
      });
  };

  viewDetailsOnClick = () => {
    if (
      [1, 2, 39, 142, 278, 66, 59, 58, 60].includes(
        this.props.orderDetails?.orderStatusId
      )
    ) {
      this.goToOrderStatus(this.props.orderDetails?.orderId);
    } else if (this.props.orderDetails?.orderStatusId === 57) {
      this.navigateToOrderDetails(
        this.props.orderDetails?.orderId,
        this.props.orderDetails?.orderStatus,
        "Cancelled"
      );
    } else {
      this.navigateToOrderDetails(
        this.props.orderDetails?.orderId,
        this.props.orderDetails?.orderStatus,
        "Delivered"
      );
    }
  };

  render() {
    return (
      <div
        className="orderCardContainer"
        onClick={() => {
          if (window.innerWidth <= 468) {
            this.viewDetailsOnClick();
          }
        }}
      >
        <div className="orderCardStatusContainer">
          <div className="orderCardStatus">
            <img
              className={
                [142, 278].includes(this.props.orderDetails?.orderStatusId)
                  ? "orderCardStatusImg"
                  : ""
              }
              src={this.getOrderStatus(
                this.props.orderDetails?.orderStatusId,
                true
              )}
              alt="status"
            />
            <div>
              <span
                style={{
                  color:
                    this.props.orderDetails?.orderStatusId === 57
                      ? "#F96265"
                      : [
                          55, 190, 191, 192, 199, 201, 56, 200, 121, 124,
                        ].includes(this.props.orderDetails?.orderStatusId)
                      ? "#5FBB78"
                      : "",
                }}
              >
                {this.getOrderStatus(this.props.orderDetails?.orderStatusId)}
              </span>
              {arrivingStatusArr.includes(
                this.props.orderDetails?.orderStatusId
              ) && this.props.orderDetails?.lastModifiedOn ? (
                <span style={{ color: "#8897A2" }}>
                  Arriving on {this.convertEpoch(this.props.orderDetails?.edd)}
                </span>
              ) : this.props.orderDetails?.orderDate ? (
                <span>
                  On{" "}
                  {this.convertEpoch(
                    this.props.orderDetails?.orderStatusId === 57
                      ? this.props.orderDetails?.lastModifiedOn
                      : this.props.orderDetails?.deliveredDate
                  )}
                </span>
              ) : null}
            </div>
          </div>
          <div className="orderCardDates">
            <span>
              Order Placed: On{" "}
              {this.convertEpoch(this.props.orderDetails?.orderDate)}
            </span>
            <span>Order ID: {this.props.orderDetails?.orderId}</span>
          </div>
        </div>
        {[121, 124, 56, 200, 190, 191, 192, 199, 201].includes(
          this.props.orderDetails?.orderStatusId
        ) ? (
          <div className="orderCardDeliveredStatuses">
            <span>
              {this.getOrderDeliveryStatus(
                this.props.orderDetails?.orderStatusId
              )}
            </span>
          </div>
        ) : null}
        {this.props.orderDetails?.myMedidicnes?.length > 0 ? (
          <div className="orderCardmedNameContainer">
            <div className="orderCardMedName">
              {this.props.orderDetails?.myMedidicnes?.length > 2 ? (
                <>
                  {this.props.orderDetails?.myMedidicnes?.map(
                    (med, index) =>
                      index < 2 && (
                        <span>
                          {med.productName.replace(
                            /\w+/g,
                            (w) => w[0].toUpperCase() + w.slice(1).toLowerCase()
                          )}
                        </span>
                      )
                  )}
                  <span>
                    + {this.props.orderDetails?.myMedidicnes?.length - 2}{" "}
                    {this.props.orderDetails?.myMedidicnes?.length - 2 === 1
                      ? " more item"
                      : " more items"}
                  </span>
                </>
              ) : (
                this.props.orderDetails?.myMedidicnes?.map((med, index) => (
                  <span>
                    {med.productName.replace(
                      /\w+/g,
                      (w) => w[0].toUpperCase() + w.slice(1).toLowerCase()
                    )}
                  </span>
                ))
              )}
            </div>
            {this.props.orderDetails?.orderamount ? (
              <span className="orderCardAmount">
                ₹{parseFloat(this.props.orderDetails?.orderamount).toFixed(2)}
              </span>
            ) : null}
          </div>
        ) : (
          <div className="noMedContainer" />
        )}
        <div className="orderCardFooter">
          <div
            style={{
              visibility: this.props.orderDetails?.patientName
                ? "visible"
                : "hidden",
            }}
            className="orderCardPatient"
          >
            <span>{this.props.orderDetails?.patientName}</span>
          </div>
          <div className="orderCardFooterButtons">
            {[56, 191, 199, 200, 201, 57, 60, 55, 58].includes(
              this.props.orderDetails?.orderStatusId
            ) ? (
              <Button viewDetails onClick={this.viewDetailsOnClick}>
                {[58, 60].includes(this.props.orderDetails?.orderStatusId)
                  ? "Track Order"
                  : "View Details"}
              </Button>
            ) : (
              <Button
                style={{
                  pointerEvents: this.state.buttonDisabled ? "none" : "",
                }}
                reorder
                onClick={this.viewDetailsOnClick}
              >
                Track Order
              </Button>
            )}

            {this.props.orderDetails?.orderStatusId === 58 ? (
              <Button
                style={{
                  pointerEvents: this.state.buttonDisabled ? "none" : "",
                }}
                reorder
                onClick={(e) =>
                  this.cashFreeService(e, this.props.orderDetails?.orderId)
                }
              >
                Pay Now
              </Button>
            ) : [56, 191, 199, 200, 201, 57, 60, 55].includes(
                this.props.orderDetails?.orderStatusId
              ) && this.props.orderDetails?.myMedidicnes?.length > 0 ? (
              <Button
                style={{
                  pointerEvents: this.state.buttonDisabled ? "none" : "",
                }}
                reorder
                onClick={(e) =>
                  this.props.reorderHandler(
                    e,
                    this.props.orderDetails?.orderId,
                    this.props.orderDetails?.patientId
                  )
                }
              >
                Reorder
              </Button>
            ) : null}
          </div>
        </div>
      </div>
    );
  }
}

let mapStateToProps = (state) => ({
  token: state.loginReducer?.verifyOtpSuccess,
  custId: state.loginReducer?.verifyOtpSuccess?.CustomerDto?.customerId,
  reorder: state.reorderReducer,
  trackOrder: state.myOrder?.orderStatusSuccess,
});

export default withRouter(
  connect(mapStateToProps, {
    fetchOrderStatusThunk,
    getOrderStatusDetailsDataAction,
  })(OrderCard)
);

import styled from "styled-components";

export const PaginationWrap = styled.div`
  .containerWrap {
    display: flex;
    justify-content: center;
    margin: 15px 0px;
    padding: 0px;
    list-style: none;
  }

  li::marker {
    content: "" !important;
  }

  li.page {
    font-size: 14px;
    padding: 10px;
  }

  a.pageLink {
    color: #333;
  }

  a.pageLink.activeLink {
    color: #0071bc;
    font-weight: 600;
    font-size: 15px;
  }

  li.page.active {
  }

  li.break {
    font-size: 14px;
    padding: 10px;
  }

  li.previous,
  li.next {
    font-size: 14px;
    padding: 10px;
  }

  li.previous.disabled {
  }

  a.previousLink.disabledLink {
    color: #e8e8e8;
    cursor: not-allowed;
  }

  a.previousLink {
    color: #333;
    padding: 10px 5px;
  }

  a.nextLinkClassName {
    color: #333;
    padding: 10px 5px;
  }
  a.nextLinkClassName.disabledLink {
    color: #e8e8e8;
    cursor: not-allowed;
  }
`;

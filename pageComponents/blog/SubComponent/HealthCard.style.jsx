import styled from "styled-components";

export const HealthCardWrapper = styled.div`
  min-width: 200px;
  width: 200px;
  box-sizing: border-box;
  display: flex;
  flex-flow: column;
  margin: 1rem 1rem;
  cursor: pointer;
  > div[class="img-article"] {
    > img {
      width: 200px;
      height: 200px;
      border-radius: 8px;
      object-fit: cover;
    }
  }

  > span[class="tag-article"] {
    margin-top: 10px;
    /* font-family: "Raleway", sans-serif; */
    font-style: normal;
    font-weight: normal;
    font-size: 12px;
    line-height: 14px;
    letter-spacing: 0.02em;
    text-transform: uppercase;
    color: #0071bc;
  }
  > p[class="article-author"] {
    /* font-family: "Raleway", sans-serif; */
    font-style: normal;
    font-weight: normal;
    font-size: 16px;
    line-height: 130.2%;
    color: #333333;
  }
  > p[class="article-brief"] {
    /* font-family: "Raleway", sans-serif; */
    font-style: normal;
    font-weight: normal;
    font-size: 12px;
    line-height: 14px;
    letter-spacing: 0.02em;
    color: #828282;
    flex: auto;
    align-items: flex-end;
    display: flex;
  }
`;

import React, { Component } from "react";
import { AdvertSideBar } from "./ArticleCard.style";
import imgBanner from "./adrt.png";
import vid1 from "./1.mp4";
import vid2 from "./2.mp4";
import vid3 from "./3.mp4";
export class Advert extends Component {
  componentDidMount() {
    document.getElementById("vid").play();
  }
  randomNumber(min, max) {
    return Math.floor(Math.random() * (max - min) + min);
  }
  getVideo() {
    let num = this.randomNumber(1, 6);
    switch (num) {
      case 1:
        return vid1;

      case 2:
        return vid2;

      case 3:
        return vid3;

      case 4:
        return vid1;

      case 5:
        return vid2;

      case 6:
        return vid3;

      default:
        return vid1;
    }
  }
  render() {
    return (
      <AdvertSideBar
        onClick={() => {
          window.location.href = window.location.origin + "/?search=true";
        }}
      >
        {/* <img
          src={imgBanner}
          alt="advert"
          onClick={() => Router.push("/search")}
        ></img> */}
        {/*  <iframe
          style={{
            width: "100%",
            height: "105%",
            border: "0px",
            "background-color": "#fff",
          }}
          allowfullscreen
          allow="controls; loop; autoplay; encrypted-media"
          src={vid1}
        ></iframe> */}
        <video id={"vid"} autoPlay muted loop playsInline>
          <source src={this.getVideo()} type="video/mp4"></source>
        </video>
      </AdvertSideBar>
    );
  }
}

export default Advert;

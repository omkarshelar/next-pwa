import React, { Component } from "react";
import medicine from "./medicine.png";
import { ArticleCardWrapper } from "./ArticleCard.style";

import moment from "moment";
import optimizeImage from "../../../components/Helper/OptimizeImage";

export class ArticleCard extends Component {
  addDefaultSrc(ev) {
    ev.target.src = medicine;
    // ev.target.style = { display: "none" };
  }
  render() {
    let { data } = this.props;
    return (
      <>
        <ArticleCardWrapper onClick={() => this.props.specificArticle(data)}>
          <div className="article-info">
            <header>
              {data.categoryNameList.map((e, i) => {
                if (i + 1 == data.categoryNameList.length) {
                  return e;
                } else {
                  return e + ", ";
                }
              })}
            </header>
            <p className="article-header">{data.name}</p>
            <p
              className="article-subheader"
              dangerouslySetInnerHTML={{ __html: data.description }}
            ></p>
            <p className="article-brief">
              {data._embedded.author[0].name} |{" "}
              {moment(data.date).format("Do MMM YYYY")}
            </p>
            {/* <p className="article-share">
            <img src={share} alt="share"></img>
          </p> */}
          </div>
          <div className="image-wrapper">
            {data._embedded["wp:featuredmedia"]?.length > 0 ? (
              <img
                src={optimizeImage(
                  data._embedded["wp:featuredmedia"][0].source_url,
                  "250"
                )}
                alt="medicine"
                onError={this.addDefaultSrc}
              ></img>
            ) : (
              ""
            )}
          </div>
        </ArticleCardWrapper>
        {this.props.lastIndex && (
          <hr
            style={{
              border: "none",
              borderTop: "1px solid #999999",
              opacity: "0.3",
              margin: "1rem 0px",
              height: "auto",
              "background-color": "#fff",
            }}
          />
        )}
      </>
    );
  }
}

export default ArticleCard;

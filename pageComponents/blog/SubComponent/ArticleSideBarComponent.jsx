import React from "react";
import { connect } from "react-redux";
import Router, { withRouter } from "next/router";
import {
  ArticleSideBar,
  CategoryListWrap,
  InputWrap,
  CatDailoqWrap,
  ArticleCategoryShimmer,
} from "./ArticleSideBarComponent.style";
import Advert from "./Advert";
import { Input, message } from "antd";
import { SearchOutlined } from "@ant-design/icons";
import { FiFilter } from "react-icons/fi";
import Dialog from "@material-ui/core/Dialog";
import Shimmer from "react-shimmer-effect";
import "./ArticleSideBarComponent.css";
import { fetchCategoryList } from "../../../redux/HeadlessHealth/Action";
import Link from "next/link";

import window from "global";
class ArticleSideBarComponent extends React.Component {
  state = {
    searchString: this.props.searchString ? this.props.searchString : "",
    catOpen: false,
    category: this.props.category ? this.props.category : [],
  };

  setTotalCount = () => {
    let totalCount = 0;
    if (this.props.category) {
      this.props.category.map((e) => {
        totalCount += e.count;
      });

      if (!this.props.type && typeof this.props.getTotalCount === "function") {
        this.props.getTotalCount(totalCount);
      }
    }
  };
  componentDidMount = () => {
    if (this.props.category.length === 0) {
      this.props
        .fetchCategoryList({ accessToken: this.props.accessToken })
        .then(() => {
          this.setTotalCount();
        });
    } else {
      this.setTotalCount();
    }
  };
  componentDidUpdate(prevProps, prevState) {
    // eslint-disable-next-line
    if (prevProps.searchString != this.props.searchString) {
      this.setState({
        searchString: this.props.searchString,
      });
    }
  }
  handleSearchEnter = () => {
    if (this.state.searchString) {
      if (typeof this.props.handleSearchEnter === "function") {
        this.props.handleSearchEnter();
      } else {
        Router.push(
          "/blog/search/" + encodeURIComponent(this.state.searchString)
        );
      }
    } else {
      message.error(
        "Please specify some details of the post are you looking for"
      );
    }
  };
  getCategoryList = () => {
    return (
      <>
        <div className="title">Categories</div>
        {this.props.category ? (
          this.props.category?.length > 0 ? (
            this.props.category.map((e) => {
              return (
                <a
                  key={e.id}
                  onClick={() => {
                    Router.push("/blog/category/" + e.slug, undefined, {
                      shallow: true,
                    });
                  }}
                  className={
                    this.props.type &&
                    this.props.type === "category" &&
                    this.props.id === e.slug
                      ? "itemWrap selected"
                      : "itemWrap"
                  }
                >
                  <div>{e.name}</div>
                  <div>{">"}</div>
                </a>
              );
            })
          ) : (
            ""
          )
        ) : (
          <ArticleCategoryShimmer>
            <Shimmer>
              <div className="itemName"></div>
              <div className="itemName"></div>
              <div className="itemName"></div>
              <div className="itemName"></div>
              <div className="itemName"></div>
              <div className="itemName"></div>
              <div className="itemName"></div>
              <div className="itemName"></div>
              <div className="itemName"></div>
            </Shimmer>
          </ArticleCategoryShimmer>
        )}
      </>
    );
  };
  render() {
    return (
      <ArticleSideBar>
        <div className="searchWrap">
          <InputWrap>
            <Input
              placeholder={"Search posts here.."}
              value={this.state.searchString}
              onChange={(e) => {
                this.setState({ searchString: e.target.value });
                if (typeof this.props.handleSearchString === "function") {
                  this.props.handleSearchString(e.target.value);
                }
              }}
              onPressEnter={() => {
                this.handleSearchEnter();
              }}
            />
            <SearchOutlined
              onClick={() => {
                this.handleSearchEnter();
              }}
              className="searchIcon"
            />
          </InputWrap>
          <div
            className="filterIconWrap"
            onClick={() => {
              this.setState({
                catOpen: true,
              });
            }}
          >
            <FiFilter className="filterIcon" />
          </div>
        </div>

        <CategoryListWrap>{this.getCategoryList()}</CategoryListWrap>
        <Advert history={this.props.history} />
        <Dialog
          fullScreen={false}
          open={this.state.catOpen}
          aria-labelledby="responsive-dialog-title"
          classes={{ paper: "blog-rectangle" }}
          id="responsive-dialog-title"
          onClose={() => {
            this.setState({
              catOpen: false,
            });
          }}
        >
          <CatDailoqWrap>{this.getCategoryList()}</CatDailoqWrap>
        </Dialog>
      </ArticleSideBar>
    );
  }
}

let mapStateToProps = (state) => ({
  accessToken: state.loginReducer.verifyOtpSuccess?.Response?.access_token,
  isLoading: state.articleHlDetails?.isLoading,
  category: state.articleHlDetails?.categoryList,
});

export default withRouter(
  connect(mapStateToProps, { fetchCategoryList })(ArticleSideBarComponent)
);

import React from "react";

import blogNotFound from "./blogNotFound.svg";
import { NoPostsWrap } from "./BlogError.style";
export default class BlogError extends React.Component {
  render() {
    switch (this.props.type) {
      case 1:
        return (
          <NoPostsWrap>
            <div className="noPosts">
              <img src={blogNotFound} alt="no posts found" />
              <div className="noPostsText">No Posts Found</div>
            </div>
          </NoPostsWrap>
        );
      default:
        return (
          <NoPostsWrap>
            <div className="noPosts">
              <img src={blogNotFound} alt="no posts found" />
              <div className="noPostsText">No Posts Found</div>
            </div>
          </NoPostsWrap>
        );
    }
  }
}

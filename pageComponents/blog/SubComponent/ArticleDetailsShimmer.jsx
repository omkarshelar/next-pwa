import React from "react";
import Shimmer from "react-shimmer-effect";
import "./ArticleDetailsShimmer.css";
import { ArticleCardShimmer } from "./ArticleCard.style";

export function ArticleDetailsShimmerLine() {
  return (
    <ArticleCardShimmer>
      <Shimmer>
        <h5 className="article-details-info-card-shimmer-header"></h5>
        <p className="article-details-info-card-shimmer-subheader"></p>
        <p className="article-details-info-card-shimmer-timestamp"></p>
      </Shimmer>
    </ArticleCardShimmer>
  );
}

export function ArticleDetailsShimmerImageSection() {
  return (
    <Shimmer>
      <div className="article-details-info-card-shimmer-image-section"></div>
    </Shimmer>
  );
}

export function ArticleDetailsShimmerLineSection() {
  return (
    <div className="article-details-info-card-shimmer-line-container">
      <Shimmer>
        <p className="article-details-info-card-shimmer-line-section"></p>
        <p className="article-details-info-card-shimmer-line-section"></p>
      </Shimmer>
    </div>
  );
}

import styled from "styled-components";

export const NoPostsWrap = styled.div`
  .noPosts {
    width: 100%;
    overflow-x: hidden;
    justify-content: center;
    text-align: center;
    padding-top: 4rem;
  }

  .noPosts img {
    width: 70%;
  }

  .noPostsText {
    font-size: 20px;
    font-weight: 400;
    color: rgb(0, 113, 188);
    padding: 22px;
  }
`;

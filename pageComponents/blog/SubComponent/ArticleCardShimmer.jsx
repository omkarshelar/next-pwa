import React, { Component } from "react";
import Shimmer from "react-shimmer-effect";
import "./ArticleCardShimmer.css";

export class ArticleCardShimmer extends Component {
  render() {
    return (
      <div className="article-card-shimmer-container">
        <div className="article-card-shimmer-wrapper">
          <div className="article-card-shimmer-column-one">
            <Shimmer>
              <p className="article-card-shimmer-line-one"></p>
              <p className="article-card-shimmer-line-two"></p>
              <p className="article-card-shimmer-line-three"></p>
            </Shimmer>
          </div>
          <div className="article-card-shimmer-column-two">
            <Shimmer>
              <div className="article-card-shimmer-column-two-image"></div>
            </Shimmer>
          </div>
        </div>
        <div className="article-card-shimmer-wrapper">
          <div className="article-card-shimmer-column-one">
            <Shimmer>
              <p className="article-card-shimmer-line-one"></p>
              <p className="article-card-shimmer-line-two"></p>
              <p className="article-card-shimmer-line-three"></p>
            </Shimmer>
          </div>
          <div className="article-card-shimmer-column-two">
            <Shimmer>
              <div className="article-card-shimmer-column-two-image"></div>
            </Shimmer>
          </div>
        </div>
        <div className="article-card-shimmer-wrapper">
          <div className="article-card-shimmer-column-one">
            <Shimmer>
              <p className="article-card-shimmer-line-one"></p>
              <p className="article-card-shimmer-line-two"></p>
              <p className="article-card-shimmer-line-three"></p>
            </Shimmer>
          </div>
          <div className="article-card-shimmer-column-two">
            <Shimmer>
              <div className="article-card-shimmer-column-two-image"></div>
            </Shimmer>
          </div>
        </div>
      </div>
    );
  }
}

export default ArticleCardShimmer;

import ScrollContainer from "react-indiana-drag-scroll";
import styled from "styled-components";

export const FirstRow = styled.div`
  display: flex;
  > div[class="article-image-content-section"] {
    width: 100%;
    padding: 0 1rem 1rem 1rem;
    @media screen and (max-width: 468px) {
      padding: 0 0 1rem 0;
    }
  }
  @media screen and (max-width: 468px) {
    flex-flow: column;
  }
`;
export const ShareSocial = styled.div`
  display: flex;
  flex-flow: column;

  @media screen and (max-width: 468px) {
    justify-content: flex-end;
    flex-flow: row;
  }
`;
export const ArticleContent = styled.div`
  box-sizing: border-box;
  margin-top: 2rem;

  > section {
    /* font-family: "Raleway", sans-serif; */
    font-size: 20px;
    font-style: normal;
    font-weight: 400;
    line-height: 30px;
    letter-spacing: 0em;
    text-align: left;
    > img {
      max-width: 100%;
      object-fit: contain;
    }
    > p > img {
      max-width: 100%;
      object-fit: contain;
    }
  }

  > a {
    display: block;
    /* font-family: "Raleway", sans-serif; */
    font-size: 17px;
    font-style: normal;
    font-weight: 600;

    :hover {
      color: #0055cc;
    }
    margin-top: 0.7rem;
  }
  @media screen and (max-width: 468px) {
    padding: 1rem;
  }
`;
export const RelatedArticle = styled.div`
  border-top: 1px solid #999999;
  margin-top: 10rem;
  > p {
    /* font-family: "Raleway", sans-serif; */
    font-style: normal;
    font-weight: normal;
    font-size: 18px;
    line-height: 25px;
    text-align: center;
    color: #4f4f4f;
    margin: 2rem 0;
  }
`;

export const ArticleData = styled(ScrollContainer)`
  display: flex;
  margin: 5px;
`;

export const ArticleContentWrapper = styled.div`
  box-sizing: border-box;
  width: 70%;
  display: flex;
  flex-flow: column;
  justify-content: space-between;
  padding: 10px;
  @media screen and (max-width: 1024px) {
    width: 100%;
  }
  @media screen and (max-width: 468px) {
    padding: 0px;
  }
`;

export const ArticleImage = styled.div`
  /* display: flex; */
  /* justify-content: flex-end; */
  width: 100%;
  background-position: center;
`;

export const TagsWrapper = styled.div`
  > div {
    font-size: 15px;
    color: #555;
    font-weight: 500;
    display: inline-flex;
    padding-right: 10px;
  }
  > a {
    display: inline-flex;
    color: #0071bc;
    font-weight: 600;
    padding: 0px;
    margin: 4px;
    border-radius: 4px;
    font-size: 15px;
  }
`;
export const HeaderBlog = styled.div`
  display: flex;
  flex-flow: column;
  min-height: 200px;
  background: #e5f1f8;

  > div[class="health-header"] {
    width: calc(70% - 120px);
    margin: auto;
    > h1 {
      /* font-family: "Century Gothic", sans-serif; */
      font-style: normal;
      font-weight: bold;
      font-size: 35px;
      color: #333;
      margin-bottom: 0;
      padding: 0px;
    }
    > p[class="article-tags-header"] {
      /* font-family: "Raleway", sans-serif; */
      font-size: 12px;
      font-style: normal;
      font-weight: 400;
      line-height: 14px;
      letter-spacing: 0.02em;
      text-align: left;
      color: #0071bc;
      > a {
        color: #0071bc;
        font-weight: 600;
        font-size: 15px;
      }
    }
    > p {
      margin: 5px 0 0 0;
    }
    @media screen and (max-width: 1300px) {
      width: calc(80% - 120px);
    }
    @media screen and (max-width: 1160px) {
      width: calc(95% - 120px);
    }
    @media screen and (max-width: 468px) {
      width: 96%;

      > h1 {
        font-size: 20px;
        padding: 0px;
      }
    }
  }

  @media screen and (max-width: 468px) {
    min-height: 110px;
  }
`;

import styled from "styled-components";

export const ArticleCardWrapper = styled.div`
  display: flex;
  box-sizing: border-box;
  justify-content: space-between;
  width: 100%;
  margin-bottom: 10px;
  cursor: pointer;
  /* info section */
  div[class="article-info"] {
    box-sizing: border-box;
    padding: 10px;
    > header {
      /* font-family: "Raleway", sans-serif; */
      font-size: 12px;
      font-style: normal;
      font-weight: 400;
      color: #6ec1fd;
      letter-spacing: 0.02em;
      text-align: left;
    }
    > p[class="article-header"] {
      /* font-family: "Raleway", sans-serif; */
      font-size: 19px;
      font-style: normal;
      font-weight: 500;
      letter-spacing: 0em;
      text-align: left;
      margin-bottom: 5px;
    }
    > p[class="article-subheader"] {
      /* font-family: "Raleway", sans-serif; */
      font-size: 14px;
      font-style: normal;
      font-weight: 400;
      letter-spacing: 0em;
      text-align: left;
      color: #999999;
      display: -webkit-box;
      -webkit-line-clamp: 2;
      -webkit-box-orient: vertical;
      overflow: hidden;
    }
    > p[class="article-brief"] {
      /* font-family: "Raleway", sans-serif; */
      font-size: 12px;
      font-style: normal;
      font-weight: 400;
      line-height: 14px;
      letter-spacing: 0.02em;
      text-align: left;
      color: #999999;
    }
    > p[class="article-share"] {
      text-align: left;
      > img {
        width: 20px;
      }
    }
  }

  /* image section */
  div[class="image-wrapper"] {
    > img {
      width: 191px;
      height: 165px;
      border-radius: 10px;
      object-fit: cover;
    }
  }

  @media screen and (max-width: 568px) {
    flex-flow: column;
    div[class="image-wrapper"] {
      order: 1;
      > img {
        width: 100%;
        height: 165px;
        border-radius: 10px;
      }
    }
    div[class="article-info"] {
      order: 2;
    }
  }
`;

/* Subscription section */
export const SubscribeWrapper = styled.div`
  box-sizing: border-box;
  display: flex;
  flex-flow: column;
  width: 265px;
  height: 390px;
  padding: 1.5rem 10px;
  background: linear-gradient(180deg, #0071bc 0%, #6ec1fd 100%);
  border-radius: 8px;
  > header {
    /* font-family: "Century Gothic", sans-serif; */
    font-size: 24px;
    font-style: normal;
    font-weight: 700;
    letter-spacing: 0px;
    text-align: left;
    color: #ffffff;
  }
  > p {
    /* font-family: "Raleway", sans-serif; */
    font-size: 16px;
    font-style: normal;
    font-weight: 400;
    line-height: 21px;
    letter-spacing: 0em;
    text-align: left;
    color: #ffffff;
    margin: 10px 0;
  }
  > div[class="subscribe-input-wrapper"] {
    display: flex;
    height: 35px;
    width: 100%;

    > input[class="subscribe-input"] {
      background: #ffffff;
      border-radius: 2px 0 0 2px;
      outline: none;
      flex-grow: 1;
      border: 1px solid #ffffff;
    }
    > label {
      /* font-family: "Raleway", sans-serif; */
      font-size: 12px;
      font-style: normal;
      font-weight: 600;
      letter-spacing: 0.02em;
      color: #0071bc;
      height: inherit;
      background: #ffffff;
      border-radius: 0 2px 2px 0;
      padding: 0 5px;
      display: flex;
      align-items: center;
    }
  }
  > div[class="image-container-subscribe"] {
    width: 115px;
    height: 115px;
    border-radius: 50%;
    background-color: white;
    border: 1px solid #ffffff;
    margin: 2rem 0;
    align-self: center;
  }
`;

/* popular post css */

export const PostWrapper = styled.div`
  margin-top: 2rem;
  > header {
    /* font-family: "Century Gothic", sans-serif; */
    font-style: normal;
    font-weight: bold;
    font-size: 24px;
    color: #003055;
  }
  hr {
    border-top: 1px solid #003055;
  }
  > div[class="post-container"] {
    display: flex;
    flex-flow: column;
    > div[class="post-popular"] {
      display: flex;
      align-items: center;
      > p[class="number-post"] {
        /* font-family: "Century Gothic", sans-serif; */
        font-size: 24px;
        font-style: normal;
        font-weight: 700;
        line-height: 29px;
        letter-spacing: 0px;
        text-align: left;
        color: #003055;
        opacity: 0.2;
        margin: 5px;
      }
      > p[class="header-post"] {
        /* font-family: "Raleway", sans-serif; */
        font-style: normal;
        font-weight: normal;
        font-size: 16px;
        line-height: 130.2%;
        color: #686868;
        margin: 0;
        margin: 5px;
      }
    }
  }
`;

/* Advertise section css */
export const AdvertSideBar = styled.div`
  width: 265px;
  height: 385px;
  margin-top: 1rem;
  max-width: 100%;
  position: unset;
  top: 5rem;
  > img {
    max-width: 100%;
    max-height: 100%;
    cursor: pointer;
  }
  > video {
    max-width: 100%;
    max-height: 100%;
    cursor: pointer;
  }

  @media screen and (max-width: 768px) {
    display: none;
  }
`;

export const ArticleCardShimmer = styled.div`
  display: flex;
  flex-flow: column;
  .article-details-info-card-shimmer-header {
    width: 250px;
    height: 17px;
    border-radius: 5px;
    box-shadow: 0px 20px 50px rgba(49, 45, 43, 0.1);
  }
  .article-details-info-card-shimmer-subheader {
    width: 180px;
    height: 15px;
    border-radius: 5px;
    box-shadow: 0px 20px 50px rgba(49, 45, 43, 0.1);
  }
  .article-details-info-card-shimmer-timestamp {
    width: 130px;
    height: 15px;
    border-radius: 5px;
    box-shadow: 0px 20px 50px rgba(49, 45, 43, 0.1);
  }
  .Shimmer-shimmer-1-1-1 {
    display: inline-block;
    animation: shimmerAnim 2s infinite linear;
    background: #fff;
    background-size: 800px 104px;
    background-image: linear-gradient(
      to right,
      #fff 40%,
      #eeeeee 50%,
      #fff 60%
    );
    background-repeat: no-repeat;
  }
`;

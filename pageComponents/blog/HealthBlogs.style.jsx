import styled from "styled-components";
import ScrollContainer from "react-indiana-drag-scroll";

export const HelathWrapper = styled.div`
  height: 200px;
  background: #e5f1f8;
  display: flex;
  flex-flow: column;
  justify-content: flex-end;
`;

export const Header = styled.div`
  > h5 {
    /* font-family: "Century Gothic", sans-serif; */
    font-style: normal;
    font-weight: bold;
    font-size: 60px;
    line-height: 74px;
    text-align: center;
    color: #003055;
    margin-bottom: 0;
  }
  display: flex;
  flex-grow: 1;
  justify-content: center;
  align-items: center;
`;

export const HealthHeader = styled.div`
  width: 100%;
`;

export const FliterList = styled(ScrollContainer)`
  display: flex;
  width: 70%;
  margin: 0 auto;
  > div > span {
    cursor: pointer;
    box-sizing: border-box;
    font-style: normal;
    font-size: 14px;
    line-height: 130.2%;
    display: flex;
    justify-content: center;
    align-items: flex-start;
    text-align: center;
    margin: 10px 20px;
    color: #0071bc;
  }

  @media screen and (max-width: 1300px) {
    width: 80%;
  }
  @media screen and (max-width: 1160px) {
    width: 95%;
  }
  @media screen and (max-width: 480px) {
    > div > span {
      font-size: 14px;
      margin: 5px 10px;
    }
  }
`;

export const ArticleWrapper = styled.div`
  box-sizing: border-box;
  width: 70%;
  display: flex;
  margin: 0 auto;
  justify-content: space-between;
  @media screen and (max-width: 1300px) {
    width: 80%;
  }
  @media screen and (max-width: 1160px) {
    width: 95%;
  }
  @media screen and (max-width: 468px) {
    width: 100%;
  }
`;
export const ArticleList = styled.div`
  box-sizing: border-box;
  width: 70%;
  padding: 10px;
  @media screen and (max-width: 1024px) {
    width: 100%;
  }
`;
export const ArticleSideBar = styled.div`
  box-sizing: border-box;
  width: 265px;
  padding: 10px;
  display: flex;
  flex-flow: column;
  position: sticky;
  top: 5rem;
  height: 100%;
  @media screen and (max-width: 1024px) {
    display: none;
  }
`;

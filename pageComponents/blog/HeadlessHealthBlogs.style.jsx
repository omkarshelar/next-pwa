import styled from "styled-components";
import ScrollContainer from "react-indiana-drag-scroll";

export const HealthWrapper = styled.div`
  height: 200px;
  background: #e5f1f8;
  display: flex;
  flex-flow: column;
  justify-content: flex-end;

  @media screen and (max-width: 768px) {
    height: 130px;
  }
  @media screen and (max-width: 468px) {
    height: 70px;
  }
`;

export const Header = styled.div`
  > h5 {
    /* font-family: "Century Gothic", sans-serif; */
    font-style: normal;
    font-weight: 700;
    font-size: 60px;
    line-height: 74px;
    text-align: center;
    color: #003055;
    margin-bottom: 0;
    text-transform: capitalize;

    @media screen and (max-width: 768px) {
      font-size: 35px;
    }

    @media screen and (max-width: 468px) {
      font-size: 22px;
    }
  }
  display: flex;
  flex-grow: 1;
  justify-content: center;
  align-items: center;
`;

export const HealthHeader = styled.div`
  width: 100%;
`;

export const FliterList = styled(ScrollContainer)`
  display: flex;
  width: 70%;
  margin: 0 auto;
  > div > span {
    cursor: pointer;
    box-sizing: border-box;
    font-style: normal;
    font-size: 14px;
    line-height: 130.2%;
    display: flex;
    justify-content: center;
    align-items: flex-start;
    text-align: center;
    margin: 10px 20px;
    color: #0071bc;
    white-space: nowrap;
  }

  @media screen and (max-width: 1300px) {
    width: 80%;
  }
  @media screen and (max-width: 1160px) {
    width: 95%;
  }
  @media screen and (max-width: 480px) {
    > div > span {
      font-size: 14px;
      margin: 5px 10px;
    }
  }
`;

export const ArticleWrapper = styled.div`
  box-sizing: border-box;
  width: 70%;
  display: flex;
  margin: 0 auto;
  justify-content: space-between;
  @media screen and (max-width: 1300px) {
    width: 80%;
  }
  @media screen and (max-width: 1160px) {
    width: 95%;
  }
  @media screen and (max-width: 468px) {
    width: 100%;
  }
`;
export const ArticleList = styled.div`
  box-sizing: border-box;
  width: 70%;
  padding: 10px;
  @media screen and (max-width: 1024px) {
    width: 100%;
  }
`;

export const ViewMoreBtn = styled.div`
  background-color: #e5f1f8;
  border-radius: 154px;
  border: 1px solid #0071bc;
  text-align: center;
  padding: 8px 0px;
  margin-bottom: 25px;
  cursor: pointer;
`;

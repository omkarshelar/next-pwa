import styled from "styled-components";

export const AddMedicineWrap = styled.div`
  cursor: pointer;
`;

export const AddMedicineSection = styled.div`
  display: flex;
  justify-content: space-between;
  padding: 20px 15px 20px 2px;
  .addMoreText {
    color: #016eb9;
    font-weight: 700;
    font-size: 14px;
    width: 100%;
  }

  @media screen and (max-width: 768px) {
    padding: 20px 15px 20px 15px;
  }
`;

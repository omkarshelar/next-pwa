import React from "react";
import CustomButton from "../../../Components/CustomButton/CustomButton";
import { ConfirmMedSection } from "../Cart.style";
import close from "../../../Assets/CircleClose.png";

function AddMedicinePop({
  Header,
  Subheader,
  handleClose,
  yesMethod,
  noMethod,
  yesText,
  noText,
}) {
  return (
    <ConfirmMedSection>
      <div style={{ display: "flex", justifyContent: "space-between" }}>
        <h5>{Header}</h5>
        <img
          src={close}
          alt="cancel"
          style={{ width: "30px", height: "30px" }}
          onClick={handleClose}
        ></img>
      </div>

      <span>{Subheader}</span>
      <div style={{ display: "flex", justifyContent: "space-between" }}>
        <CustomButton wp onClick={yesMethod}>
          {yesText}
        </CustomButton>
        <CustomButton onClick={noMethod} wo>
          {noText}
        </CustomButton>
      </div>
    </ConfirmMedSection>
  );
}

export default AddMedicinePop;

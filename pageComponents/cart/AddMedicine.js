import React from "react";
import AndroidSeperator from "../../components/WebComponents/AndroidSeperator/AndroidSeperator";
import { AddMedicineSection, AddMedicineWrap } from "./AddMedicine.style";
import addMedicineIcon from "../../src/Assets/addMedicineIcon.svg";
const AddMedicine = (props) => {
  return (
    <AddMedicineWrap id="addMedBtn" onClick={() => props.redirectToHandler()}>
      <AndroidSeperator showMob={true} showWeb={true} />
      <AddMedicineSection>
        <div id="addMedBtn" className="addMoreText">
          {props.cartContents?.length > 0
            ? "Add More Medicines"
            : "Add Medicines"}
        </div>
        <div id="addMedBtn">
          <img id="addMedBtn" src={addMedicineIcon} />
        </div>
      </AddMedicineSection>
      <AndroidSeperator showMob={true} showWeb={true} />
    </AddMedicineWrap>
  );
};

export default AddMedicine;

import React, { Component } from "react";
import "./DoctorsBanner.css";
import top30Icon from "../../../Assets/top-30.svg";
import saveUptoIcon from "../../../Assets/save-upto.svg";
import freedeliveryIcon from "../../../Assets/free-delivery.svg";

export default class DoctorsBanner extends Component {
  render() {
    return (
      <div className="doctorsBannerMainContainer">
        <span>Why opt for Truemeds recommendations ?</span>
        <div className="doctorsBannerContainer">
          <div>
            <img src={top30Icon} alt="Top 30 companies" />
            <p>Top 30 companies</p>
          </div>
          <div>
            <img src={saveUptoIcon} alt="Save up to 72% on every order" />
            <p>Save up to 72% on every order</p>
          </div>
          <div>
            <img src={freedeliveryIcon} alt="Get free delivery" />
            <p>Get free delivery</p>
          </div>
        </div>
      </div>
    );
  }
}

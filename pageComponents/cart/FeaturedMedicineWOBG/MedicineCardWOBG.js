import React, { Component } from "react";
import { connect } from "react-redux";
import {
  addItemCartAction,
  removeItemCartAction,
  removeMedThunk,
} from "../../../redux/Cart/Action";
import { FeaturedMedThunk } from "../../../redux/FeaturedMedicine/Action";
import { withStyles } from "@material-ui/core/styles";
import {
  CardActionSection,
  CardImageSection,
  CardInfoSection,
  CardWrap,
} from "./MedicineCardWOBG.style";
import { customNotification } from "../../../components/Helper/helperFunction";
// import { eventCrosssellingAdded } from "../../../src/Events/Events";

const styles = () => ({
  menuPaper: {
    maxHeight: 120,
  },
});

export class MedicineCard extends Component {
  state = {
    medData: [],
    rMedData: [],
    rContent: {},
    removeQuantity: false,
  };
  addNewQuantity = (obj, qty) => {
    obj.newQty = Number(qty);
    return obj;
  };
  removeMedOnClick = (data) => {
    if (this.props.medConfirm?.orderId) {
      this.props.removeMedThunk({
        orderId: this.props.medConfirm.orderId,
        access_token: this.props.accessToken,
        medData: data,
        history: this.props.history,
      });
    } else if (this.props.uploadImage?.orderId) {
      this.props.removeMedThunk({
        orderId: this.props.uploadImage.orderId,
        access_token: this.props.accessToken,
        medData: data,
        history: this.props.history,
      });
    }
  };
  quantityOnHandleChange = (value, content) => {
    if (this.props.cartContent?.length > 0) {
      this.setState(
        {
          medData: this.props.cartContent.map((data) => {
            return {
              productCode: data.orgProductCd,
              quantity: data.orgQuantity,
            };
          }),
        },
        () => {
          this.props.addItemCartAction(this.addNewQuantity(content, value));
        }
      );
    }
  };

  setmaxQty = (arr, maxQty) => {
    let newArr = [];
    for (let i in arr) {
      if (arr[i] <= maxQty) {
        newArr.push(Number(arr[i]));
      }
    }
    return newArr;
  };

  render() {
    let { medicineName, companyName, mrp, images, content, productCode } =
      this.props;
    return (
      <div>
        <CardWrap>
          <CardImageSection>
            {images && <img src={images[0]} />}
          </CardImageSection>
          <CardInfoSection>
            <div className="medName">{medicineName}</div>
            <div className="companyName">{companyName}</div>
            <div className="packSize">
              {content._source?.original_pack +
                " " +
                content._source?.original_unit}
            </div>
            <div className="sellingPrice">
              {"₹" + (mrp - mrp * (this.props.discount / 100)).toFixed(2)}
            </div>
            <div className="costWrap">
              {this.props.discount ? (
                <div className="mrp">
                  <del>{"₹" + mrp}</del>
                </div>
              ) : null}
              {content._source?.original_base_discount ? (
                <div className="discount">
                  {content._source?.original_base_discount}% OFF
                </div>
              ) : null}
            </div>
          </CardInfoSection>
          <CardActionSection>
            <>
              {!content._source.original_available &&
              !content._source.subs_found ? (
                <span className="not-in-stock">Not In Stock</span>
              ) : content._source.original_supplied_bytm ? (
                <button
                  onClick={() => {
                    this.props.addItemCartAction(
                      this.addNewQuantity(content, 1)
                    );
                    // Event_Truemeds
                    // new
                    // eventCrosssellingAdded();
                    customNotification("Item added", "customNotify");
                  }}
                >
                  Add
                </button>
              ) : (
                <span
                  style={{ alignSelf: "flex-end" }}
                  className="not-available"
                >
                  Not sold by Truemeds
                </span>
              )}
            </>
          </CardActionSection>
        </CardWrap>
      </div>
    );
  }
}
MedicineCard.defaultProps = {
  images: null,
};

const mapStateToProps = (state) => ({
  cartContent: state.cartData?.cartItems,
  accessToken: state.loginReducer.verifyOtpSuccess?.Response?.access_token,
  medConfirm: state.confirmMedicineReducer?.ConfirmMedData,
  uploadImage: state.uploadImage,
});

export default connect(mapStateToProps, {
  addItemCartAction,
  removeItemCartAction,
  removeMedThunk,
  FeaturedMedThunk,
})(withStyles(styles, { withTheme: true })(MedicineCard));

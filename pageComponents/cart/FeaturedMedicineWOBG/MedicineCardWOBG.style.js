import styled from "styled-components";

export const CardWrap = styled.div`
  width: 170px;
  margin-right: 15px;
`;

export const CardImageSection = styled.div`
  padding: 15px 0px;
  height: 170px;
  width: 100%;
  display: flex;
  align-items: center;
  img {
    width: 100%;
    object-fit: contain;
    max-height: -webkit-fill-available;
  }
`;

export const CardInfoSection = styled.div`
  height: 120px;
  .medName {
    color: #4f585e;
    font-size: 14px;
    font-weight: 600;
    text-overflow: ellipsis;
    overflow: hidden;
    display: -webkit-box;
    -webkit-line-clamp: 2;
    -webkit-box-orient: vertical;
  }

  .companyName {
    color: #8897a2;
    font-size: 12px;
    font-weight: 400;
    font-family: "Inter";
  }

  .packSize {
    color: #4f585e;
    font-size: 12px;
    font-family: "Inter";
    margin: 5px 0px;
  }

  .sellingPrice {
    color: #4f585e;
    font-size: 15px;
    font-weight: 700;
    font-family: "Inter";
  }

  .costWrap {
    display: flex;
    gap: 5px;
  }

  .mrp {
    color: #8897a2;
    font-size: 12px;
    font-weight: 400;
    font-family: "Inter";
  }

  .discount {
    color: #25b374;
    font-weight: 700;
    font-family: "Inter";
    font-size: 12px;
  }
`;

export const CardActionSection = styled.div`
  button {
    background: #0071bc;
    color: #fff;
    border: none;
    font-size: 14px;
    font-weight: 600;
    font-family: "Inter";
    padding: 8px 40px;
    border-radius: 4px;
    margin-top: 10px;
  }

  @media screen and (max-width: 468px) {
    button {
      background: #0071bc;
      color: #fff;
      border: none;
      font-size: 14px;
      font-weight: 600;
      font-family: "Inter";
      padding: 8px 40px;
      border-radius: 4px;
      margin-top: 20px;
    }
  }
`;

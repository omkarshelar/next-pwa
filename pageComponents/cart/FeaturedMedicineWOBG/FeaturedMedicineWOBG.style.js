import styled from "styled-components";

export const Header = styled.div`
  display: flex;
  justify-content: space-between;
  margin-top: 15px;
  .title {
    color: #40464d;
    font-weight: 700;
    font-size: 16px;
    font-family: "Inter";
  }

  .leftArrowwobg {
    transform: rotate(-90deg);
  }
  .rightArrowwobg {
    transform: rotate(90deg);
  }

  .titleShimmer {
    width: 100px;
    /* height: 25px; */
  }

  .arrowShimmer {
    width: 100px;
    /* height: 25px; */
  }

  .shimmerWrapper {
    display: flex;
    justify-content: space-between;
    width: 100%;
  }
`;

export const MedicineList = styled.div`
  display: flex;
  overflow-x: auto;
  scroll-behavior: smooth;
  transition: scroll-behavior 1s ease;
  min-width: 100%;
  -ms-overflow-style: none; /* IE and Edge */
  scrollbar-width: none; /* Firefox */
  ::-webkit-scrollbar:horizontal {
    height: 0;
    width: 0;
    display: none;
  }

  ::-webkit-scrollbar-thumb:horizontal {
    display: none;
  }
  .myOrdersNoDataContainer {
    padding: 10px;
    display: flex;
    justify-content: center;
    align-items: center;
    margin: 1rem 0 1rem 1rem;
  }

  .myOrdersNoDataContainer img {
    width: 30px;
    margin-right: 10px;
  }

  .myOrdersNoDataContainer span {
    font-size: 18px;
    font-weight: 600;
    color: #333;
  }
  @media screen and (max-width: 568px) {
    .myOrdersNoDataContainer {
      flex-grow: 1;
    }
  }
`;

export const Slider = styled.div`
  display: flex;
  justify-content: flex-end;
  > button > img {
    width: 30px;
  }
  @media screen and (max-width: 768px) {
    margin-right: -15px;
  }
`;

import React, { Component, createRef } from "react";
import { withRouter } from "next/router";
import Button from "../../../components/WebComponents/Button/Button";
import directionArrow from "../../../src/Assets/paymentMethodChevron.svg";
import { connect } from "react-redux";
import disabledDirectionArrow from "../../../src/Assets/chevron_grey.svg";
import {
  FeaturedMedThunk,
  PastPurchaseThunk,
} from "../../../redux/FeaturedMedicine/Action";
import CardFeatureLoader from "../../../components/WebComponents/FeaturedMedicine/CardFeatureLoader";
import MedicineCardWOBG from "./MedicineCardWOBG";
import { Header, MedicineList, Slider } from "./FeaturedMedicineWOBG.style";
import Skeleton from "@mui/material/Skeleton";
let options = {
  root: null,
  rootMargin: "0px",
};
export class FeaturedMedicineWOBG extends Component {
  state = {
    FeaturedType: 1,
    isVisible: false,
    isLeftDisabled: true,
  };
  constructor(props) {
    super(props);
    this.ListRef = createRef();
    this.ListWrapRef = createRef();
  }
  scrollRight = () => {
    this.useIntersection();
    if (this.ListRef.current) {
      let currentValue = this.ListRef.current.scrollLeft + 500;
      this.ListRef.current.scrollLeft = currentValue;
      if (currentValue > 0) {
        this.setState({
          isLeftDisabled: false,
        });
      } else {
        this.setState({
          isLeftDisabled: true,
        });
      }
    }
  };
  scrollLeft = () => {
    this.useIntersection();
    if (this.ListRef.current) {
      let currentValue = this.ListRef.current.scrollLeft - 500;
      this.ListRef.current.scrollLeft = currentValue;
      if (currentValue <= 1) {
        this.setState({
          isLeftDisabled: true,
        });
      } else {
        this.setState({
          isLeftDisabled: false,
        });
      }
    }
  };

  callBackFunction = (e) => {
    const [entry] = e;
    this.setState({ isVisible: entry.isIntersecting });
  };

  useIntersection = () => {
    const observer = new IntersectionObserver(this.callBackFunction, options);
    if (this.ListWrapRef.current) {
      observer.observe(this.ListWrapRef.current);
    }
    return () => {
      if (this.ListWrapRef.current) {
        observer.unobserve(this.ListWrapRef.current);
      }
    };
  };

  renderCartandMedItems = (apiData, cartData) => {
    let updatedApiData = [];
    for (const apiItem in apiData) {
      for (const cardItem in cartData) {
        if (
          cartData[cardItem]._source.original_product_code ===
          apiData[apiItem]._source.original_product_code
        ) {
          updatedApiData.push({
            dataIndex: apiItem,
            ...cartData[cardItem],
          });
        }
      }
    }
    for (const updatedItem of updatedApiData) {
      apiData.splice(Number(updatedItem.dataIndex), 1, updatedItem);
    }
    return apiData;
  };
  callPastMedicineData = () => {
    this.props.PastPurchaseThunk({
      history: this.props.router,
      accessToken: this.props?.accessToken,
      customerId: this.props?.customerId,
    });
  };

  componentDidMount() {
    let that = this;
    let data = {};
    data.warehouseId = this.props.pincodeDetails?.warehouseId
      ? this.props.pincodeDetails.warehouseId
      : 3;
    if (this.props.accessToken) {
      data.access_token = this.props.accessToken;
    }
    this.props.FeaturedMedThunk(data);
    this.useIntersection();
    window.addEventListener("scroll", this.handleScroll);

    document.addEventListener("keydown", function (event) {
      if (event.keyCode === 39) {
        that.scrollRight();
      } else if (event.keyCode === 37) {
        that.scrollLeft();
      }
    });
  }

  handleScroll = () => {
    this.useIntersection();
  };
  render() {
    let { SuggestMedicine, PastMedicine } = this.props;
    let { FeaturedType } = this.state;
    let SuggestMedicineData = this.renderCartandMedItems(
      SuggestMedicine,
      this.props.cart
    );
    let PastMedicineData = this.renderCartandMedItems(
      PastMedicine,
      this.props.cart
    );

    let cartIds = this.props.cart.map((e) => e._id);

    let medData = [];

    if (FeaturedType === 1) {
      SuggestMedicineData?.map((data) => {
        if (!cartIds.includes(data._id)) {
          medData.push(
            <MedicineCardWOBG
              medicineName={data._source.original_sku_name}
              companyName={data._source.original_company_nm}
              mrp={data._source.original_mrp}
              images={data.product_image_urls}
              content={data}
              refreshData={() => {
                this.props.FeaturedMedThunk({
                  warehouseId: this.props.pincodeDetails?.warehouseId
                    ? this.props.pincodeDetails.warehouseId
                    : 3,
                  access_token: this.props.accessToken
                    ? this.props.accessToken
                    : null,
                });
              }}
              discount={data._source.original_base_discount}
              productCode={data?._source?.original_product_code}
            />
          );
        }
      });
    } else if (PastMedicineData?.length > 0) {
      PastMedicineData.map((data) => {
        if (!cartIds.includes(data._id)) {
          medData.push(
            <MedicineCardWOBG
              medicineName={data._source.original_sku_name}
              companyName={data._source.original_company_nm}
              mrp={data._source.original_mrp}
              discount={data._source.original_base_discount}
              content={data}
              refreshData={this.callPastMedicineData}
              productCode={data?._source?.original_product_code}
            />
          );
        }
      });
    } else if (SuggestMedicineData?.length > 0) {
      SuggestMedicineData?.map((data) => {
        if (!cartIds.includes(data._id)) {
          medData.push(
            <MedicineCardWOBG
              medicineName={data._source.original_sku_name}
              companyName={data._source.original_company_nm}
              mrp={data._source.original_mrp}
              images={data.product_image_urls}
              content={data}
              refreshData={() => {
                this.props.FeaturedMedThunk({
                  warehouseId: this.props.pincodeDetails?.warehouseId
                    ? this.props.pincodeDetails.warehouseId
                    : 3,
                  access_token: this.props.accessToken
                    ? this.props.accessToken
                    : null,
                });
              }}
              discount={data._source.original_base_discount}
              productCode={data?._source?.original_product_code}
            />
          );
        }
      });
    }

    medData.push(<div ref={this.ListWrapRef}></div>);
    return (
      <>
        {medData.length > 1 && (
          <>
            {this.props.isLoading ? (
              <Header>
                <div className="titleShimmer">
                  <Skeleton animation="wave" height={40} />
                </div>
                <div className="arrowShimmer">
                  <Skeleton animation="wave" height={40} />
                </div>
              </Header>
            ) : (
              <Header>
                <div className="title">Customers also Bought</div>
                {window.innerWidth > 768 && (
                  <div className="buttonWrap">
                    <Button disabled={this.state.isLeftDisabled}>
                      <img
                        className={`leftArrowwobg`}
                        src={
                          this.state.isLeftDisabled
                            ? disabledDirectionArrow
                            : directionArrow
                        }
                        onClick={this.scrollLeft}
                        alt="left"
                      ></img>
                    </Button>
                    <Button disabled={this.state.isVisible}>
                      <img
                        className={`rightArrowwobg`}
                        src={
                          this.state.isVisible
                            ? disabledDirectionArrow
                            : directionArrow
                        }
                        onClick={this.scrollRight}
                        alt="right"
                      ></img>
                    </Button>
                  </div>
                )}
              </Header>
            )}
            <Slider>
              <MedicineList ref={this.ListRef}>
                {this.props.isLoading || !SuggestMedicineData ? (
                  <CardFeatureLoader />
                ) : (
                  medData
                )}
              </MedicineList>
            </Slider>
          </>
        )}
      </>
    );
  }
}
let mapStateToProps = (state) => ({
  SuggestMedicine: state.suggestMed.featuredMedicineList?.hits,
  PastMedicine: state.suggestMed.PastMedicineList,
  cart: state.cartData.cartItems,
  accessToken: state.loginReducer.verifyOtpSuccess?.Response?.access_token,
  customerId: state.loginReducer.verifyOtpSuccess?.CustomerId,
  isLoading: state.loader?.isLoading,
  pincodeDetails: state.pincodeData?.pincodeData,
});

export default withRouter(
  connect(mapStateToProps, { FeaturedMedThunk, PastPurchaseThunk })(
    FeaturedMedicineWOBG
  )
);

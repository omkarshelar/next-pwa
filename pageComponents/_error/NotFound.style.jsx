import styled from "styled-components";

export const NotFoundWrap = styled.div`
  display: flex;
  justify-content: center;
  align-content: center;
  padding: 3% 0%;

  @media screen and (max-width: 768px) {
    display: block;
    padding: 10% 10%;
  }
`;

export const ImageWrap = styled.div`
  width: 30%;
  > img {
    width: 100%;
    max-width: 700px;
  }
  @media screen and (max-width: 768px) {
    width: 100%;
  }
`;

export const TextWrap = styled.div`
  display: block;
  align-self: center;
  margin-left: 80px;
  @media screen and (max-width: 768px) {
    margin-left: 0px;
    text-align: center;
    margin-top: 15px;
  }
`;

export const Title = styled.div`
  color: #0071bc;
  /* font-family: "Raleway"; */
  font-family: "Inter", sans-serif;
  font-weight: 600;
  font-size: 80px;
  @media screen and (max-width: 768px) {
    font-size: 40px;
  }
`;

export const SubTitle = styled.div`
  font-size: 20px;
  color: #555;
  @media screen and (max-width: 768px) {
    font-size: 18px;
    color: #555;
  }
`;

export const SearchMedBtn = styled.div`
  background: #0071bc;
  color: #fff;
  width: fit-content;
  padding: 15px 40px;
  font-weight: 500;
  font-size: 17px;
  margin-top: 20px;
  cursor: pointer;
  border-radius: 4px;
  @media screen and (max-width: 768px) {
    background: #0071bc;
    color: #fff;
    margin: auto;
    width: -moz-fit-content;
    width: fit-content;
    padding: 15px 40px;
    font-weight: 500;
    font-size: 17px;
    margin-top: 15px;
    cursor: pointer;
    border-radius: 4px;
  }
`;

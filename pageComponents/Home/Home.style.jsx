import styled from "styled-components";

export const HomeWrapper = styled.div`
  width: 100%;
  box-sizing: border-box;

  > div[class="call"] {
    display: flex;
    position: fixed;
    z-index: 10;
    right: 1rem;
    bottom: 0.5rem;
    width: 60px;
    height: 60px;
    display: flex;
    justify-content: center;
    align-items: center;
    background: #0071bc;
    box-shadow: 0px 20px 50px rgb(87 100 173 / 22%);
    border-radius: 50%;
    margin: 10px;
    @media screen and (max-width: 768px) {
      width: 50px;
      height: 50px;
    }

    @media screen and (max-width: 468px) {
      display: none;
    }
  }
`;

export const ButtonUpload = styled.div`
  display: none;
  > button {
    width: 200px;
    margin: 1rem 0 1rem 0;
  }
  > button > span {
    font-size: 12px;
    font-style: normal;
    font-weight: 600;
    line-height: 14px;
    letter-spacing: 0.02em;
    text-align: center;
    color: #0071bc;
    margin-left: 5px;
  }
  @media screen and (max-width: 1068px) {
    display: flex;
    width: 80%;
    margin: 0 auto;
    justify-content: center;
  }
`;

export const PaymentContent = styled.div`
  display: flex;
  flex-flow: column;
  justify-content: center;
  align-items: center;
  > span {
    /* font-family: "Century Gothic", sans-serif; */
    font-size: 20px;
    font-style: normal;
    font-weight: 600;
    line-height: 29px;
    letter-spacing: 0px;
    text-align: center;
  }
`;

export const FooterAnimation = styled.div`
  display: flex;
  justify-content: center;
  height: 250px;
  @media screen and (max-width: 968px) {
    display: none;
  }
`;

export const LoaderWrapper = styled.div`
  width: max-content;
  margin: auto;
`;

import styled from "styled-components";
import {
  dark,
  darkGreen,
  lightAccent,
  referBorderRadius,
  accent,
  referBorderRadiusMobile,
  darkMid,
  darkLight,
  danger,
  altAccent,
  verticalMargin,
  verticalMarginMobile,
  darkAccent,
  separatorColor,
  checkFlexGap,
} from "../ReferEarn.styled";

export const ReferNowWrapper = styled.div`
  width: 100%;
  display: flex;
  flex-flow: column;

  ${checkFlexGap()
    ? `gap: ${verticalMargin}`
    : ` & > div:not(:last-child):not(:first-child) {
        margin: 18px 0;
        }
        `};

  @media only screen and (max-width: 767px) {
    ${checkFlexGap()
      ? `gap: ${verticalMarginMobile};`
      : ` & > div:not(:last-child):not(:first-child) {
        margin: 7px 0;
        }
        `};
  }
`;

export const ReferHeader = styled.div`
  width: 100%;
  height: auto;
  background-color: ${darkGreen};
  color: #fff;
  border-radius: ${referBorderRadius};
  padding: 28px 53px 28px 28px;
  display: grid;
  grid-template-columns: 1fr auto;
  margin-bottom: 80px;
  position: relative;

  ${checkFlexGap()
    ? `gap: 1em 2em;`
    : `
  > div:not(:last-child) {
        margin-right: 2em;
      }
  `}

  @media (min-width: 768px) and (max-width: 992px) {
    margin-bottom: 140px;
    border-radius: ${referBorderRadiusMobile};
  }

  @media only screen and (max-width: 767px) {
    padding: 18px 16px 100px 16px;
    width: calc(100% + 32px);
    margin-left: -16px !important;
    margin-bottom: 100px;
    margin-top: 30px !important ;
    border-radius: 0;
    display: flex;
    align-items: center;

    @media only screen and (max-width: 767px) {
      margin-bottom: 80px;
    }
  }

  .headerText {
    grid-column: 1/2;
    grid-row: 1/2;

    p {
      color: #fff;
      font-weight: 700;
      font-size: 18px;
      margin-bottom: 4px;

      @media only screen and (max-width: 767px) {
        font-size: 14px;
        margin-bottom: 11px;
      }
    }

    h3 {
      color: #fff;
      font-weight: 700;
      font-size: 36px;
      margin: 0;

      @media only screen and (max-width: 767px) {
        font-size: 24px;
      }
    }
  }

  .headerImg {
    grid-column: 2/3;
    grid-row: 1/3;

    @media only screen and (max-width: 767px) {
      img {
        width: 100%;
      }
    }
  }
`;

export const ReferalCodeCard = styled.div`
  width: 50%;
  height: auto;
  padding: 16px;
  border-radius: ${referBorderRadius};
  background: #fff;
  box-shadow: 0px 6px 10px rgba(0, 0, 0, 0.06), 0px 1px 18px rgba(0, 0, 0, 0.04),
    0px 3px 5px rgba(0, 0, 0, 0.08);
  color: ${dark};
  position: absolute;
  left: 2em;
  bottom: -80px;

  @media (min-width: 768px) and (max-width: 992px) {
    bottom: -100px;
  }

  @media only screen and (max-width: 767px) {
    left: 16px;
    padding: 12px 16px 14px 16px;
    width: calc(100% - 32px);
    border-radius: ${referBorderRadiusMobile};

    p {
      font-size: 12px;
      margin-bottom: 10px;
      font-weight: 600;
      color: "#4f585e" !important;
    }
  }

  p {
    text-transform: capitalize;
  }

  button.bbtn {
    padding-top: 8.5px;
    padding-bottom: 8.5px;

    @media only screen and (max-width: 767px) {
      padding-top: 10.5px;
      padding-bottom: 10.5px;
    }
  }

  .referalCopyInput {
    width: 100%;
    height: 40px;
    display: flex;
    flex-flow: row nowrap;
    align-items: center;
    margin-bottom: 1em;

    @media only screen and (max-width: 767px) {
      margin-bottom: 14px;
    }

    .ant-input {
      padding: 0.5em;
      font-weight: 700;
      background-color: #fff;
      color: ${dark};
      cursor: none;
      caret-color: transparent;
      font-size: 14px;
      text-indent: calc(13.37px - 0.5em);

      &:focus,
      &:hover {
        border-color: #d9d9d9;
      }

      @media only screen and (max-width: 767px) {
        text-indent: calc(14.44px - 0.5em);
      }
    }

    img,
    svg {
      margin-left: -2em;
      z-index: 2;
      font-size: large;
      color: ${darkGreen};
    }
  }
`;

export const ReferralWorking = styled.div`
  width: 100%;
  margin: 0;
  order: ${(props) => (props.lowerOrder ? 6 : "unset")};

  /* margin: ${verticalMargin}; */

  @media only screen and (max-width: 767px) {
    /* margin: ${verticalMarginMobile}; */
    display: none;
  }

  .referral-collapse-custom-collapse {
    .referral-collapse-custom-panel {
      /* margin-bottom: 24px; */
      overflow: hidden;
      background: ${lightAccent};
      border: 0px;
      border-radius: ${referBorderRadius};
      border: 1px solid #dce4f1;

      @media only screen and (max-width: 767px) {
        border-radius: ${referBorderRadiusMobile};
      }

      .ant-collapse-header {
        padding: 25px 30px;
        font-size: 18px;
        font-weight: 700;
        line-height: 24px;
        color: ${dark};

        .ant-collapse-arrow {
          right: 2em;
          top: 35px;
          font-size: 14px;
          font-weight: 700;
          color: ${accent};
        }
      }

      .ant-collapse-content-box {
        display: grid;
        grid-template-columns: repeat(3, 1fr);
        padding: 0 30px 30px 30px;

        ${checkFlexGap()
          ? `gap: 30px;`
          : `
        > div:not(:last-child) {
              margin-right: 30px;
            }
        `}

        /* //?Safari 6.00 - 10.00 */
        /* @media screen and (min-color-index: 0) and(-webkit-min-device-pixel-ratio:0) {
          @media {
            > div:not(:last-child) {
              margin-right: 30px;
            }
          }
        } */

        /* //?Safari 9.00+ */
        /* @supports (-webkit-hyphens: none) {
          > div:not(:last-child) {
            margin-right: 30px;
          }
        } */

        div {
          display: flex;
          flex-flow: column;
          justify-content: center;
          align-items: flex-start;

          ${checkFlexGap()
            ? `gap: 27px;`
            : `
          display: block;
              > img {
                margin-right: auto;
                height: 90px;
                margin-bottom: 27px;
              }

              p {
                text-align: left;
              }
          `}

          /* //?Safari 6.00 - 10.00 */
          /* @media screen and (min-color-index: 0) and(-webkit-min-device-pixel-ratio:0) {
            @media {
              display: block;
              > img {
                margin-right: auto;
                height: 90px;
                margin-bottom: 27px;
              }

              p {
                text-align: left;
              }
            }
          } */

          /* //?Safari 9.00+ */
          /* @supports (-webkit-hyphens: none) {
            display: block;
            > img {
              margin-right: auto;
              height: 90px;
              margin-bottom: 27px;
            }

            p {
              text-align: left;
            }
          } */

          p {
            font-weight: 500;
            font-size: 14px;
            margin-bottom: 0;
          }
        }
      }
    }

    .referral-collapse-custom-panel.ant-collapse-item-active {
      .ant-collapse-header {
        padding: 24px 30px 20px 30px;
      }
    }
  }
`;

export const RewardInputWrapper = styled.div`
  width: 100%;
  border: 1px solid ${darkLight};
  border-radius: ${referBorderRadius};
  padding: 24px 30px 20px 30px;
  /* margin: ${verticalMargin}; */

  @media only screen and (max-width: 767px) {
    border-radius: ${referBorderRadiusMobile};
    margin: 0;
    border: none;
    padding: 0;
  }

  .title {
    font-weight: 700;
    font-size: 16px;
    line-height: 30px;
    margin-bottom: 19px;

    @media only screen and (max-width: 767px) {
      margin-bottom: 14px;
    }
  }

  button.bbtn {
    padding-top: 10.5px;
    padding-bottom: 10.5px;
    height: 40px;

    @media only screen and (max-width: 767px) {
      padding-top: 14px;
      padding-bottom: 14px;
      font-size: 12px;
      height: 42px;
    }
  }

  .inputWrapper .claimRefCodeForm {
    display: grid;
    grid-template-columns: 2fr 166px;

    ${checkFlexGap()
      ? ` gap: 8px 18px;`
      : ` 
        input {
          width: calc(100% - 18px);
        }
      
    }`}

    @media only screen and (max-width: 767px) {
      grid-template-columns: 2fr 102px;

      ${checkFlexGap()
        ? ` gap: 8px 16px;`
        : ` 
        input {
          width: calc(100% - 16px);
        }
      
    }`}
    }

    /* //?Safari 6.00 - 10.00 */
    /* @media screen and (min-color-index: 0) and(-webkit-min-device-pixel-ratio:0) {
      @media {
        display: inline-block;
        width: 100%;

        input {
          margin-right: 18px;
          width: 100%;
        }

        @media only screen and (max-width: 767px) {
          input {
            width: 100%;
            margin-right: 16px;
          }
        }
      }
    } */

    /* //?Safari 9.00+ */
    /* @supports (-webkit-hyphens: none) {
      
      width: 100%;
      input {
        margin-right: 18px;
        
        width: 100%;
      }

      @media only screen and (max-width: 767px) {
        input {
          width: 100%;
          margin-right: 16px;
        }
      }
    } */

    .ant-input {
      border-color: #0071bc;
      border-radius: 4px;
    }

    .refCodeErrorWrap {
      height: 22px;

      .refCodeErrorMsg {
        color: ${danger};
        font-weight: 400;
        font-size: 12px;
        line-height: 18px;

        @media only screen and (max-width: 767px) {
          font-size: 12px;
        }
      }
    }
  }
`;

export const ReferralWorkingMob = styled.div`
  width: 100%;
  /* margin: ${verticalMarginMobile}; */
  margin-bottom: 0;
  display: none;
  @media only screen and (max-width: 767px) {
    display: block;
  }

  .ant-collapse-header {
    font-size: 16px;
    font-weight: 700;
    line-height: 24px;
    color: ${dark} !important;
    background: #fff;
  }

  .ant-collapse-content-box {
    background: #fff;
  }

  .ant-list-item-meta {
    align-items: center;
  }

  .ant-list-item {
    padding: 0 0 12px 0;
    border-bottom: none;

    p {
      margin: 0;
      line-height: 18px;
      font-size: 12px;
      font-weight: 600;
    }
  }

  .ant-avatar {
    overflow: visible;
    width: 50px;
    height: auto;
  }

  .ant-collapse-borderless > .ant-collapse-item {
    border-bottom: 8px solid ${separatorColor};
    width: calc(100% + 32px);
    margin-left: -16px;
  }
`;

export const RewardReceivedWrapper = styled.div`
  width: 100%;
  display: flex;
  flex-flow: column;

  align-items: center;
  text-align: center;

  ${checkFlexGap()
    ? `gap: 14px;`
    : `
  > img,
      h5,
      p {
        margin-bottom: 14px;
      }
  `}

  @media only screen and (max-width: 767px) {
    gap: 9.89px;

    ${checkFlexGap()
      ? `gap: 9.89px;`
      : `
  > img,
      h5,
      p {
        margin-bottom: 9.89px;
      }
  `}
  }

  p span {
    font-weight: 900;
  }

  /* //?Safari 6.00 - 10.00 */
  /* @media screen and (min-color-index: 0) and(-webkit-min-device-pixel-ratio:0) {
    @media {
      > img,
      h5,
      p {
        margin-bottom: 14px;

        @media only screen and (max-width: 767px) {
          margin-bottom: 9.89px;
        }
      }
    }
  } */

  /* //?Safari 9.00+ */
  /* @supports (-webkit-hyphens: none) {
    
    > img,
    h5,
    p {
      margin-bottom: 14px;

      @media only screen and (max-width: 767px) {
        margin-bottom: 9.89px;
      }
    }
  } */

  button.bbtn {
    font-weight: 600;
    white-space: pre;
    padding-top: 8px;
    padding-bottom: 8px;
  }

  img {
    width: 160px;
    margin-bottom: 10px;
  }

  h5 {
    line-height: 26px;
    font-weight: 700;
    font-size: 18px;
    margin: 0;
    color: #4f585e;

    @media only screen and (max-width: 767px) {
      font-size: 16px;
      line-height: 24px;
    }
  }

  p {
    line-height: 22px;
    font-weight: 600;
    font-size: 14px;
    margin: 0;
    color: ${dark};

    @media only screen and (max-width: 767px) {
      font-size: 12px;
      line-height: 16px;
      font-weight: 500;
      margin-bottom: 12px;
    }
  }
`;

export const RemindFriendWrapper = styled.div`
  width: 100%;
  height: 160px;
  background: ${altAccent};
  border-radius: ${referBorderRadius};
  /* margin: ${verticalMargin}; */
  padding: 30px;

  @media only screen and (max-width: 767px) {
    height: auto;
    border-radius: ${referBorderRadiusMobile};
    /* margin: ${verticalMarginMobile}; */
    padding: 15px;
  }

  h2 {
    font-weight: 700;
    font-size: 24px;
    line-height: 36px;
    color: #4f585e;
    margin: 0;

    @media only screen and (max-width: 767px) {
      font-size: 16px;
      line-height: 19.36px;
    }
  }

  .flexDiv {
    display: flex;
    flex-flow: row nowrap;
    align-items: center;
    margin-top: 16px;

    ${checkFlexGap()
      ? `gap: 48px;`
      : `
    > p {
          padding-right: 48px;
        }

        button {
          height: auto !important;
        }
    `}

    @media only screen and (max-width: 767px) {
      margin-top: 8px;
      align-items: flex-end;

      ${checkFlexGap()
        ? `gap: 20px;`
        : `
    > p {
          padding-right: 20px;
        }

        button {
          height: auto !important;
        }
    `}
    }

    p {
      margin: 0;
      font-size: 14px;
      font-weight: 500;
      line-height: 20px;
      /* flex-grow: 4; */
      color: #4f585e;

      @media only screen and (max-width: 767px) {
        font-size: 12px;
      }
    }

    button {
      /* flex-shrink: 3; */
      padding-top: 14.5px;
      padding-bottom: 14.5px;
      width: 200px;
      height: 48px;

      @media only screen and (max-width: 767px) {
        font-size: 12px;
        padding: 8.5px 24.5px;
        width: 130px !important;
        height: 32px;
      }
    }

    /* //?Safari 6.00 - 10.00 */
    /* @media screen and (min-color-index: 0) and(-webkit-min-device-pixel-ratio:0) {
      @media {
        > p {
          margin-right: 48px;

          @media only screen and (max-width: 767px) {
            margin-right: 20px;
          }
        }

        button {
          height: auto !important;
        }
      }
    } */

    /* //?Safari 9.00+ */
    /* @supports (-webkit-hyphens: none) {
      > p {
        margin-right: 48px;

        @media only screen and (max-width: 767px) {
          margin-right: 20px;
        }
      }

      button {
        height: auto !important;
      }
    } */
  }
`;

export const TotalRefEarningWrapper = styled.div`
  width: 100%;
  height: 164px;
  background: ${darkAccent};
  border-radius: ${referBorderRadius};
  /* margin: ${verticalMargin}; */
  padding: 30px 79px 30px 30px;
  display: flex;
  flex-flow: row nowrap;
  align-items: center;
  overflow: hidden;
  cursor: pointer;

  ${checkFlexGap()
    ? `gap: 15px;`
    : `> div:not(:last-child) {
        margin-right: 15px;
      }
`}

  @media only screen and (max-width: 767px) {
    height: 111px;
    border-radius: ${referBorderRadiusMobile};
    /* margin: ${verticalMarginMobile}; */
    padding: 15px;
    box-shadow: 0px 3.1px 3.8px -30px rgba(33, 64, 142, 0.1),
      0px 25px 20px -30px rgba(33, 64, 142, 1);
  }

  /* //?Safari 6.00 - 10.00 */
  /* @media screen and (min-color-index: 0) and(-webkit-min-device-pixel-ratio:0) {
    @media {
      > div:not(:last-child) {
        margin-right: 15px;
      }
    }
  } */

  /* //?Safari 9.00+ */
  /* @supports (-webkit-hyphens: none) {
    
    > div:not(:last-child) {
      margin-right: 15px;
    }
  } */

  .leftDiv {
    flex-grow: 1;
  }

  h2.title {
    margin: 0 0 21px 0;
    color: #fff;
    font-weight: 700;
    font-size: 32px;
    line-height: 38.73px;

    @media only screen and (max-width: 767px) {
      font-size: 24px;
      margin-bottom: 10px;
    }
  }

  p {
    color: #fff;
    margin: 0;
    font-weight: 600;
    font-size: 14px;
    line-height: 16.94px;
    height: 25px;

    @media only screen and (max-width: 767px) {
      font-size: 12px;
      line-height: 14.52px;
      height: 14px;
    }
  }

  img {
    margin-bottom: -30px;

    @media only screen and (max-width: 767px) {
      margin-bottom: -20px;
      width: 87px;
    }
  }
`;

export const MobDivider = styled.div`
  display: none;
  width: calc(100% + 32px);
  height: 8px;
  background: ${separatorColor};
  margin-left: -16px !important;

  @media only screen and (max-width: 767px) {
    display: block;
  }
`;

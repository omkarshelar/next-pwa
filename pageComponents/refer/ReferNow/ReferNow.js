///? Default IMPORTS
import React, { Component } from "react";
import { connect } from "react-redux";
import Router, { withRouter } from "next/router";

///? Service IMPORTS
import { redeemCodeThunk } from "../../../redux/ReferEarn/action";

///? Component & Styling IMPORTS
import {
  MobDivider,
  ReferalCodeCard,
  ReferHeader,
  ReferNowWrapper,
  ReferralWorking,
  ReferralWorkingMob,
  RemindFriendWrapper,
  RewardInputWrapper,
  RewardReceivedWrapper,
  SocialShareModalHeading,
  SocialShareSwiperWrapper,
  TotalRefEarningWrapper,
} from "./ReferNow.styled";
import ReferHeaderImg from "../../../src/Assets/ReferBucket/ReferHeaderImg.svg";
import CopyIcon from "../../../src/Assets/ReferBucket/CopyIcon.svg";
import { Avatar, Collapse, Input, List, message, Modal } from "antd";
import { BsCheckCircleFill } from "react-icons/bs";
import HowRef1 from "../../../src/Assets/ReferBucket/HowRefer1.svg";
import HowRef2 from "../../../src/Assets/ReferBucket/HowRefer2.svg";
import HowRef3 from "../../../src/Assets/ReferBucket/HowRefer3.svg";
import RewardReceived from "../../../src/Assets/ReferBucket/RewardReceivedModalImg.svg";
import TotalEarnings from "../../../src/Assets/ReferBucket/TotalEarning.svg";
import HowRefCount1 from "../../../src/Assets/ReferBucket/HowRefCount1.svg";
import HowRefCount2 from "../../../src/Assets/ReferBucket/HowRefCount2.svg";
import HowRefCount3 from "../../../src/Assets/ReferBucket/HowRefCount3.svg";
import { IoClose } from "react-icons/io5";
import { Formik, Field } from "formik";
import { RiArrowDropDownLine, RiArrowDropUpLine } from "react-icons/ri";
import "animate.css";
import ReferModal from "../ReferModal/ReferModal";

///? Constants
const { Panel } = Collapse;

export class ReferNow extends Component {
  state = {
    referModalView: false,
    referInputFocus: false,
    rewardReceivedModalVisible: false,
    activeHowToMod: true,
    invalidRefCode: false,
    invalidRefCodeMsg: null,
    registerUserCount: null,
    deliveredOrdersData: [],
    myReferralData: null,
    activeHowToRef: true,
    hideReferralCode: false,
    howToRefMob: [
      {
        delay: 0,
        img: HowRefCount1,
        value: "Invite your friends to join Truemeds using your referral code",
      },
      {
        delay: 1,
        img: HowRefCount2,
        value: `They receive
          <b>₹200</b> 
          in their TM rewards on successful registeration `,
      },
      {
        delay: 2,
        img: HowRefCount3,
        value: `You earn
          <b>₹200</b> 
          in your TM rewards after their first order is delivered`,
      },
    ],
  };

  componentDidMount = () => {
    window.scrollTo(0, 0);

    if (this.props.referralStatusData) {
      this.setState({ myReferralData: this.props.referralStatusData }, () => {
        //* Register user count collect
        let registerData = this.state.myReferralData.filter(
          (i) => i.status === "Registered"
        );

        if (registerData.length > 0) {
          this.setState({
            registerUserCount: registerData.length,
            activeHowToRef: false,
            activeHowToMod: false,
          });
        }

        //* Successful Referral Data
        let deliveredData = this.state.myReferralData.filter(
          (i) => i.status === "Order Delivered" && i.referralAmt
        );

        if (deliveredData.length > 0) {
          let totalReward = Object.values(deliveredData).reduce(
            (r, { referralAmt }) => r + referralAmt,
            0
          );

          let data = {
            totalEarning: totalReward,
            referralCount: deliveredData.length,
          };

          this.setState({
            deliveredOrdersData: [data],
            activeHowToRef: false,
            activeHowToMod: false,
          });
        }
      });
    }

    if (this.props.info.payload.alreadyRedeemed) {
      this.setState({ hideReferralCode: true });
    }
  };

  copyReferalCode = () => {
    const el = document.createElement("input");
    el.value = this.props.info.payload.referCode.substring(4);
    document.body.appendChild(el);
    el.select();
    document.execCommand("copy");
    document.body.removeChild(el);
    message.success({
      content: `Referral Code Copied Successfully`,
      className: "dark-antMsg",
    });
  };

  submitClaimRefCode = (data) => {
    let val = {
      access_token: this.props.accessToken,
      mobile: this.props.customerMobileNo,
      customerId: "",
      referCode: `ref_${data}`,
    };

    this.props.redeemCodeThunk(val).then(() => {
      if (this.props.referEarn?.redeemError) {
        this.setState({ invalidRefCode: true });
      } else if (this.props.referEarn?.redeem?.Claimed === "OK") {
        this.setState({
          invalidRefCodeMsg: "Referral Code already Redeemed",
          invalidRefCode: true,
        });
      } else if (this.props.referEarn?.redeem?.Conflict === "CONFLICT") {
        this.setState({ invalidRefCode: true });
      } else if (this.props.referEarn?.redeem?.SuccessText) {
        this.setState({
          rewardReceivedModalVisible: true,
        });

        setTimeout(() => {
          this.setState({ rewardReceivedModalVisible: false });
        }, 3000);
      }
    });
  };

  validateRefCode = (value) => {
    this.setState({ invalidRefCode: false });
    let error;
    if (!value) {
      error = "Please enter referral code";
    } else {
      error = "";
    }
    return error;
  };

  closeReferModal = () => {
    this.setState({ referModalView: false });
  };

  render() {
    return this.props.info ? (
      <ReferNowWrapper>
        {/*//* Referral Header & Modal - Start */}
        <ReferHeader>
          <div className="headerText">
            <p>Tell your friend about Truemeds &</p>
            <h3>Earn ₹200</h3>
          </div>

          <div className="headerImg">
            <img src={ReferHeaderImg} alt="referHeader" />
          </div>

          <ReferalCodeCard>
            <p style={{ color: "#4F585E" }}>your referral code</p>

            <div className="referalCopyInput">
              <Input
                value={this.props.info.payload.referCode.substring(4)}
                className="refInput"
                readonly="readonly"
              />

              <img
                style={{ color: "#f99d28" }}
                src={CopyIcon}
                alt=""
                role="button"
                onClick={() => this.copyReferalCode()}
              />
            </div>

            <button
              className="bbtn"
              onClick={() => this.setState({ referModalView: true })}
            >
              Refer a Friend
            </button>
          </ReferalCodeCard>
        </ReferHeader>

        <ReferModal
          visible={this.state.referModalView}
          closeModal={this.closeReferModal}
          payload={this.props.info.payload}
        />

        {/*//* Referral Header & Modal - Ends */}

        {/*//* How to refer mobile - Starts */}
        <ReferralWorkingMob>
          <Collapse
            bordered={false}
            activeKey={this.state.activeHowToMod ? ["1"] : null}
            defaultActiveKey={null}
            onChange={() =>
              this.setState({ activeHowToMod: !this.state.activeHowToMod })
            }
            expandIcon={({ isActive }) =>
              isActive ? (
                <RiArrowDropUpLine
                  style={{
                    color: "#4F585E",
                    fontSize: "28px",
                    paddingTop: "0px",
                    marginTop: "5px",
                  }}
                />
              ) : (
                <RiArrowDropDownLine
                  style={{
                    color: "#4F585E",
                    fontSize: "28px",
                    paddingTop: "0px",
                    marginTop: "5px",
                  }}
                />
              )
            }
            expandIconPosition="right"
          >
            <Panel header="How referral works" key="1">
              <List
                itemLayout="horizontal"
                dataSource={this.state.howToRefMob}
                renderItem={(item) => (
                  <List.Item
                    className={`animate__animated animate__fadeInRight animate__delay-${item.delay}s animate__faster`}
                    style={{ animationDuration: "0.5s" }}
                  >
                    <List.Item.Meta
                      avatar={<Avatar src={item.img} />}
                      title={
                        <p dangerouslySetInnerHTML={{ __html: item.value }} />
                      }
                    />
                  </List.Item>
                )}
              />
            </Panel>
          </Collapse>
        </ReferralWorkingMob>
        {/*//* How to refer mobile - Ends */}

        {/*//* Remind Friend - Starts */}
        {this.state.registerUserCount && (
          <RemindFriendWrapper
            onClick={() => {
              window.innerWidth < 768 && this.props.changeActiveKey("2");
              window.scrollTo(0, 0);
            }}
          >
            <h2 className="title">
              {`${
                this.state.registerUserCount === 1
                  ? "1 of your friend is yet to place an order!"
                  : `${this.state.registerUserCount} of your friends are yet to place an order!`
              }`}
            </h2>

            <div className="flexDiv">
              {window.innerWidth > 767 ? (
                <p>
                  Encourage your{" "}
                  {`${
                    this.state.registerUserCount === 1 ? "friend" : "friends"
                  }`}{" "}
                  to place their first order by sending them a reminder.
                </p>
              ) : (
                <p>
                  Encourage your{" "}
                  {`${
                    this.state.registerUserCount === 1 ? "friend" : "friends"
                  }`}{" "}
                  to place an order
                </p>
              )}

              <button
                className="bbtn"
                onClick={() => {
                  this.props.changeActiveKey("2");
                  window.scrollTo(0, 0);
                }}
              >
                Remind Them
              </button>
            </div>
          </RemindFriendWrapper>
        )}
        {/*//* Remind Friend - Ends */}

        {this.state.registerUserCount && <MobDivider />}

        {/*//* Total Ref Earnings - Starts */}
        {this.state.deliveredOrdersData.length > 0 && (
          <TotalRefEarningWrapper
            onClick={() => {
              this.props.changeActiveKey("2");
              window.scrollTo(0, 0);
            }}
          >
            <div className="leftDiv">
              <p>Total Referral Earnings:</p>
              <h2 className="title">
                Rs {this.state.deliveredOrdersData[0].totalEarning.toFixed(2)}
              </h2>
              <p>
                {this.state.deliveredOrdersData[0].referralCount} successful{" "}
                {this.state.deliveredOrdersData[0].referralCount > 1
                  ? "referrals"
                  : "referral"}
              </p>
            </div>

            <img src={TotalEarnings} alt="totalEarnings" />
          </TotalRefEarningWrapper>
        )}
        {/*//* Total Ref Earnings - Ends */}

        {this.state.deliveredOrdersData.length > 0 && <MobDivider />}

        {/*//* How to refer - Starts */}
        <ReferralWorking
          lowerOrder={
            this.state.deliveredOrdersData.length > 0 ||
            this.state.registerUserCount
              ? true
              : false
          }
        >
          <Collapse
            bordered={false}
            activeKey={this.state.activeHowToRef ? ["1"] : null}
            onChange={() =>
              this.setState({ activeHowToRef: !this.state.activeHowToRef })
            }
            expandIcon={({ isActive }) => (
              <p className="expandText">{`View ${
                isActive ? "Less" : "More"
              }`}</p>
            )}
            expandIconPosition="right"
            className={"referral-collapse-custom-collapse"}
          >
            <Panel
              header="How referral works"
              key="1"
              className="referral-collapse-custom-panel"
            >
              <div>
                <img src={HowRef1} alt="howToRef" />
                <p>
                  Invite your friends to join Truemeds using your referral code
                </p>
              </div>
              <div>
                <img src={HowRef2} alt="howToRef" />
                <p>
                  They receive <b>₹200</b> in their TM rewards on successful
                  registeration
                </p>
              </div>
              <div>
                <img src={HowRef3} alt="howToRef" />
                <p>
                  You earn <b>₹200</b> in your TM rewards after their order is
                  delivered
                </p>
              </div>
            </Panel>
          </Collapse>
        </ReferralWorking>

        {/*//* How to refer - Ends */}

        {/*//* Referral code - Starts */}
        {!this.state.hideReferralCode && (
          <RewardInputWrapper>
            <h5 className="title">Have a referral code?</h5>

            <div className="inputWrapper">
              <Formik
                initialValues={{ refCode: "" }}
                onSubmit={(values) => {
                  let data = values.refCode.toString();
                  this.submitClaimRefCode(data);
                }}
              >
                {({ errors, handleSubmit, touched, values }) => (
                  <form
                    noValidate
                    onSubmit={handleSubmit}
                    className="claimRefCodeForm"
                    autocomplete="off"
                  >
                    <Field
                      label="Enter Referral Code"
                      placeholder={
                        errors.refCode && touched.refCode
                          ? ""
                          : "Enter Referral Code"
                      }
                      className="ant-input"
                      style={
                        (errors.refCode && touched.refCode) ||
                        this.state.invalidRefCode
                          ? {
                              textIndent: "0.5em",
                              fontWeight: "600",
                              borderColor: "#F15959",
                              caretColor: "#0071bd",
                            }
                          : {
                              textIndent: "0.5em",
                              fontWeight: "600",
                              caretColor: "#0071bd",
                            }
                      }
                      autocomplete="off"
                      name="refCode"
                      value={values.refCode}
                      validate={this.validateRefCode}
                    />
                    <button className="bbtn" type="submit">
                      Claim Reward
                    </button>

                    <div className="refCodeErrorWrap">
                      {errors.refCode && touched.refCode ? (
                        <div className="refCodeErrorMsg">
                          {errors.refCode && touched.refCode && errors.refCode}
                        </div>
                      ) : this.state.invalidRefCode ? (
                        <div className="refCodeErrorMsg">
                          {this.state.invalidRefCodeMsg
                            ? this.state.invalidRefCodeMsg
                            : "Please enter a valid code"}
                        </div>
                      ) : (
                        ""
                      )}
                    </div>
                  </form>
                )}
              </Formik>
            </div>
          </RewardInputWrapper>
        )}
        {/*//* Referral code - Ends */}

        {!this.state.hideReferralCode && (
          <MobDivider style={{ marginTop: "-10px" }} />
        )}

        {/*//* Reward Received Modal - Starts */}
        <Modal
          visible={this.state.rewardReceivedModalVisible}
          footer={false}
          onCancel={() => this.setState({ rewardReceivedModalVisible: false })}
          className="rewardReceivedModal"
          closeIcon={<IoClose />}
          centered
          width={350}
        >
          <RewardReceivedWrapper>
            <img src={RewardReceived} alt="rewardReceived" />

            <h5 className="title">You’ve received Rs 200!</h5>

            <p>The amount has been credited to your TM rewards</p>

            <button
              className="bbtn"
              onClick={() => Router.push("/?search=true")}
            >
              Order Now
            </button>
          </RewardReceivedWrapper>
        </Modal>
        {/*//* Reward Received Modal - Ends */}
      </ReferNowWrapper>
    ) : (
      ""
    );
  }
}

const mapStateToProps = (state) => ({
  customerId: state.loginReducer.verifyOtpSuccess?.CustomerId,
  accessToken: state.loginReducer.verifyOtpSuccess?.Response?.access_token,
  referEarn: state.referEarn,
  customerMobileNo: state.loginReducer?.verifyOtpSuccess?.CustomerDto?.mobileNo,
});

const mapDispatchToProps = { redeemCodeThunk };

export default withRouter(
  connect(mapStateToProps, mapDispatchToProps)(ReferNow)
);

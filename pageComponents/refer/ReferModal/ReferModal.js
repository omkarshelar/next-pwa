import { message, Modal } from "antd";
import React, { Component } from "react";
import { connect } from "react-redux";
import { IoClose } from "react-icons/io5";
import {
  SocialShareModalHeading,
  SocialShareSwiperWrapper,
} from "./ReferModal.styled";
import {
  WhatsappShareButton,
  FacebookShareButton,
  TwitterShareButton,
  EmailShareButton,
} from "react-share";
import { Swiper, SwiperSlide } from "swiper/react";
import SwiperCore, { EffectCoverflow, Pagination } from "swiper";
import FBLogo from "../../../src/Assets/ReferBucket/fb.svg";
import WPLogo from "../../../src/Assets/ReferBucket/wp.svg";
import GmailLogo from "../../../src/Assets/ReferBucket/gmail.svg";
import CopyLogo from "../../../src/Assets/ReferBucket/copy.svg";
import TwitterLogo from "../../../src/Assets/ReferBucket/twitter.svg";
import "swiper/swiper-bundle.min.css";
import "swiper/swiper.min.css";
import "swiper/components/pagination/pagination.min.css";
SwiperCore.use([EffectCoverflow, Pagination]);

export class ReferModal extends Component {
  copyReferalUrl = () => {
    const el = document.createElement("input");
    el.value = `Download the Truemeds app to enjoy flat 20% savings on every other.\nUse the link or code below to get ₹200 off on your first order. Code ${this.props.payload.referCode.substring(
      4
    )} or Link - ${this.props.payload?.referLink} `;
    document.body.appendChild(el);
    el.select();
    document.execCommand("copy");
    document.body.removeChild(el);
    message.success({
      content: `Referral URL copied successfully`,
      className: "dark-antMsg",
    });
  };
  render() {
    return (
      <Modal
        visible={this.props.visible}
        footer={false}
        onCancel={() => this.props.closeModal()}
        style={window.innerWidth <= 768 && { bottom: 0 }}
        className="socialShareModal"
        closeIcon={<IoClose />}
        centered={window.innerWidth >= 768 ? true : false}
      >
        <SocialShareModalHeading>
          {this.props.remind ? (
            <h5>Remind your friend</h5>
          ) : (
            <h5>
              Invite your friends to <b>Truemeds</b>
            </h5>
          )}

          <p>Share the code with:</p>
        </SocialShareModalHeading>

        <SocialShareSwiperWrapper>
          <Swiper
            scrollContainer={false}
            spaceBetween={50}
            slidesPerView={1}
            pagination={true}
            style={{ width: "100%" }}
            className="SocialShareSwipper"
          >
            {/* //?Each swiper slide is 6 block grid */}
            <SwiperSlide>
              <WhatsappShareButton
                url={this.props.payload?.referLink}
                title={
                  this.props.payload &&
                  `Hey! Congratulations, You have received a reward of Rs 200 from Truemeds. Order medicines and save upto 72%. Click here to place the order now. Code ${this.props.payload.referCode.substring(
                    4
                  )} or Link - `
                }
              >
                <img src={WPLogo} alt="whatsapp" />
                <p>Whatsapp</p>
              </WhatsappShareButton>

              <EmailShareButton
                url={this.props.payload?.referLink}
                body={
                  this.props.payload &&
                  `Hey! Congratulations, You have received a reward of Rs 200 from Truemeds. Order medicines and save upto 72%. Click here to place the order now. Code ${this.props.payload.referCode.substring(
                    4
                  )} or Link - `
                }
              >
                <img src={GmailLogo} alt="gmail" />
                <p>Gmail</p>
              </EmailShareButton>

              <button
                className="transBtn"
                onClick={() => this.copyReferalUrl()}
              >
                <img src={CopyLogo} alt="copy" />
                <p>Copy Link</p>
              </button>

              <TwitterShareButton
                url={this.props.payload?.referLink}
                title={
                  this.props.payload &&
                  `Hey! Congratulations, You have received a reward of Rs 200 from Truemeds. Order medicines and save upto 72%. Click here to place the order now. Code ${this.props.payload.referCode.substring(
                    4
                  )} or Link - `
                }
              >
                <img src={TwitterLogo} alt="twitter" />
                <p>Twitter</p>
              </TwitterShareButton>

              <FacebookShareButton
                url={this.props.payload?.referLink}
                quote={
                  this.props.payload &&
                  `Hey! Congratulations, You have received a reward of Rs 200 from Truemeds. Order medicines and save upto 72%. Click here to place the order now. Code ${this.props.payload.referCode.substring(
                    4
                  )} or Link - ${this.props.payload?.referLink}`
                }
              >
                <img src={FBLogo} alt="facebook" />
                <p>Facebook</p>
              </FacebookShareButton>
            </SwiperSlide>
          </Swiper>
        </SocialShareSwiperWrapper>
      </Modal>
    );
  }
}

const mapStateToProps = (state) => ({});

const mapDispatchToProps = {};

export default connect(mapStateToProps, mapDispatchToProps)(ReferModal);

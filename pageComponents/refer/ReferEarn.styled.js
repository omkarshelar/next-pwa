import styled from "styled-components";

//? Const Values for Refer & Earn

export const accent = "#0071BD";
export const lightAccent = "#F5F9FF";
export const darkAccent = "#21408E";
export const altAccent = "#E6EDFF";
export const dark = "#40464D";
export const darkMid = "#8897A2";
export const darkLight = "#DFE3E6";
export const darkGreen = "#3E863C";
export const danger = "#F15959";
export const separatorColor = "#E3E6EC";
export const referBorderRadius = "8px";
export const referBorderRadiusMobile = "6px";
export const verticalMargin = "36px 0";
export const verticalMarginMobile = "14px 0";

export function checkFlexGap() {
  // create flex container with row-gap set

  if (process.browser) {
    var flex = document.createElement("div");
    flex.style.display = "flex";
    flex.style.flexDirection = "column";
    flex.style.rowGap = "1px";

    // create two, elements inside it
    flex.appendChild(document.createElement("div"));
    flex.appendChild(document.createElement("div"));

    // append to the DOM (needed to obtain scrollHeight)
    document.body.appendChild(flex);
    var isSupported = flex.scrollHeight === 1; // flex container should be 1px high from the row-gap
    flex.parentNode.removeChild(flex);

    return isSupported;
  }
}

//? CSS Styling

export const ReferEarnWrapper = styled.div`
  width: 100%;
  height: auto;
  display: grid;
  grid-template-columns: 2fr 1fr;
  padding: 24px 0;

  ${checkFlexGap()
    ? `
  gap: 1.5em;
  `
    : `> div:not(:last-child) {
        margin-right: 1.5em;

        @media only screen and (max-width: 767px) {
          display: block;
          margin-right: 0;
          margin-bottom: 0;
        }
      }
  `}

  /* //?Safari 6.00 - 10.00 */
  /* @media screen and (min-color-index: 0) and(-webkit-min-device-pixel-ratio:0) {
    @media {
      > div:not(:last-child) {
        margin-right: 1.5em;

        @media only screen and (max-width: 767px) {
          display: block;
          margin-right: 0;
          margin-bottom: 0;
        }
      }
    }
  } */

  /* //?Safari 9.00+ */
  /* @supports (-webkit-hyphens: none) {
    
    > div:not(:last-child) {
      margin-right: 1.5em;

      @media only screen and (max-width: 767px) {
        display: block;
        margin-right: 0;
        margin-bottom: 0;
      }
    }
  } */

  button.bbtn {
    text-transform: none;
  }

  button.bbtn:hover,
  button.bbtn:focus {
    background: ${accent};
  }

  h2,
  h3,
  h5,
  p,
  span {
    color: ${dark};
    font-family: "Inter";
  }

  button.bbtn {
    font-weight: 700;
    white-space: pre;
    font-size: 16px;
    line-height: 19.36px;
  }

  @media only screen and (max-width: 767px) {
    grid-template-columns: auto;

    .ant-tabs-nav {
      margin-bottom: 0;
      position: fixed;
      top: 85px;
      z-index: 999;
      left: 50%;
      transform: translate(-50%, -50%);
      padding-top: 2em;
      width: 100%;
      background: #fff;
    }

    .ant-tabs-nav-list {
      width: calc(100% - 30px) !important;
    }
  }

  .referEarnTabs {
    @media only screen and (max-width: 767px) {
      overflow: visible;

      .ant-tabs-nav-wrap {
        justify-content: center;
      }
    }

    .ant-tabs-nav {
      margin-bottom: 20px;

      @media only screen and (max-width: 767px) {
        margin-bottom: 0;
      }
    }

    @media only screen and (min-width: 767px) {
      .ant-tabs-nav-list > .ant-tabs-tab:nth-child(2) {
        margin: 0;
        padding-left: 56px;
      }
    }
    /* background-color: ${danger}; */

    .ant-tabs-tab {
      padding-right: 4em;
      font-size: 16px;
      line-height: 24px;
      padding-top: 0;
      color: ${darkMid};
      font-weight: 500;

      @media only screen and (max-width: 767px) {
        display: flex;
        justify-content: center;
        padding: 0 2em 0.5em 2em;
        margin: 0;
        width: 50%;
        /* font-size: 14px; */
      }
    }

    .ant-tabs-tab.ant-tabs-tab-active .ant-tabs-tab-btn {
      color: ${accent};
      font-weight: 600;
    }

    .ant-tabs-ink-bar {
      background: ${accent};
    }
  }
`;

import styled from "styled-components";

export const HelpContainer = styled.div`
  min-height: 70vh;
  flex-flow: column;
  display: flex;
  justify-content: space-between;
`;

export const TopicWrapper = styled.div`
  flex-flow: column;
  display: flex;
  justify-content: center;
  align-items: center;
  > div {
    display: flex;
    justify-content: space-between;
    width: 95%;
    height: 30px;
    margin: 5px 0 5px 0;
    cursor: pointer;
    border-bottom: 1px #c0c0c0 solid;
  }
  > div > span {
    color: #36454f;
  }
  > div > div > img {
    width: 70%;
    transform: rotate(-90deg);
  }
  > div > div {
    width: 30px;
  }
`;
export const HeaderHelp = styled.p`
  color: #1cb976;
  margin: 10px 10px 10px 2.5rem;
  font-weight: 500;
  @media screen and (max-width: 568px) {
    display: flex;
    margin: 10px;
  }
`;

export const EnquiryWrapper = styled.div`
  display: flex;
  margin: 0 auto;
  justify-content: space-between;
  align-items: center;
  width: 95%;
  > .info-enquiry {
    display: flex;
    flex-flow: column;
    justify-content: center;
    align-items: center;
    color: #1cb976;
    font-weight: 500;
  }
  > .info-enquiry {
    > span:nth-child(1) {
      color: #179b62;
    }
  }

  > span {
    color: #1cb976;

    font-weight: 500;
  }
`;
export const EnclosingDiv = styled.div`
  display: flex;
  flex-direction: column;
  justify-content: space-between;
`;
export const EnquiryWrapperBottom = styled.div`
  display: flex;
  justify-content: space-between;
  width: 100vw;
  align-items: center;
  position: absolute;
  bottom: 0;
  padding: 5px 5px;
  background-color: #fff;
  > .info-enquiry {
    display: flex;
    flex-flow: column;
    justify-content: center;
    align-items: center;
    color: #1cb976;
    font-weight: 500;
  }
  > .info-enquiry {
    > span:nth-child(1) {
      color: #179b62;
    }
  }

  > span {
    color: #1cb976;

    font-weight: 500;
  }
`;
export const ViewOrder = styled.div`
  @media screen and (max-width: 468px) {
    display: flex;
    justify-content: flex-end;
  }
`;

export const TicketSavingsWrapper = styled.div`
  display: flex;
  justify-content: space-between;
  color: #1cb976;
  font-weight: 600;

  > p {
    margin-bottom: 0;
  }
  @media screen and (max-width: 468px) {
    > span {
      font-size: smaller;
    }
    > p {
      font-size: small;
    }
  }
`;

export const MyOrderHelpWrapper = styled.div`
  display: flex;
  flex-flow: column;
  border: 1px solid #b8b8b8;
  border-radius: 10px;
  padding: 10px;
  margin: 10px;
  > span:nth-child(1) {
    font-weight: 600;
  }
  > span:nth-child(2) {
    color: #b8b8b8;
  }
  > span:nth-child(3) {
    color: #0071bc;
  }
  span:nth-child(4) {
    font-weight: 700;
    color: #44bb91;
  }
  @media screen and (max-width: 468px) {
    > span:nth-child(1) {
      font-size: smaller;
    }
    > span:nth-child(2) {
      font-size: smaller;
    }
    > span:nth-child(3) {
      font-size: smaller;
    }
    span:nth-child(4) {
      font-size: smaller;
    }
  }
`;

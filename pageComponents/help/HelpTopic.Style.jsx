import styled from "styled-components";

export const TopicHeader = styled.p`
  color: #1cb976;
  margin: 10px 10px 10px 2.5rem;
  font-weight: 500;
  @media screen and (max-width: 568px) {
    display: flex;
    margin: 10px;
  }
`;

export const HelpTopicWrapper = styled.div`
  flex-flow: column;
  display: flex;
  justify-content: center;
  align-items: center;
  > div {
    display: flex;
    justify-content: space-between;
    width: 95%;
    margin: 5px 0 5px 0;
    cursor: pointer;
  }
  > div > span {
    color: #36454f;
  }
  > div > div > img {
    width: 70%;
    transform: rotate(-90deg);
  }
  > div > div {
    width: 30px;
  }
  @media screen and (max-width: 468px) {
    > div > span {
      color: #36454f;
      width: 300px;
      font-size: smaller;
    }
    > div > div > img {
      width: 60%;
      transform: rotate(-90deg);
    }
    > div {
      margin-bottom: 9px;
    }
  }
`;

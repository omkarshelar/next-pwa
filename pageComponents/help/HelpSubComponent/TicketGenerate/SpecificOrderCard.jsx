import React from "react";
import ViewDetailBtn from "./ViewDetailBtn";
import { MyOrderHelpWrapper, TicketSavingsWrapper } from "../../Help.style";

function SpecificOrderCard({ data, ...props }) {
  let convertEpoch = (epoch) => {
    var date = new Date(epoch);
    return date.toUTCString().slice(0, 16);
  };

  return (
    <MyOrderHelpWrapper>
      <span>Order number - {data.orderId}</span>
      <span>Ordered on - {convertEpoch(data.orderDate)}</span>
      <span>Order status - {data.orderStatus}</span>
      <TicketSavingsWrapper>
        {data.totalSaving &&
        (data.orderStatus === "Confirmed" ||
          data.orderStatus === "Order Dispatched" ||
          data.orderStatus === "Delivered") ? (
          <p>Total savings - ₹{data.totalSaving}</p>
        ) : (
          <p></p>
        )}
        {(data.ticketStatusId === 89 || data.ticketStatusId === 90) && (
          <span>
            <i className="fad fa-comment-alt-dots"></i>
            {"  "}
            {data.ticketStatus}
          </span>
        )}
      </TicketSavingsWrapper>
      <div
        style={{
          display: "flex",
          alignItems: "center",
          justifyContent: "center",
        }}
      >
        <ViewDetailBtn
          statusId={data.statusId}
          toggleEvent={props.toggleEvent}
          history={props.history}
          data={data}
        />
      </div>
    </MyOrderHelpWrapper>
  );
}

export default SpecificOrderCard;

import React from "react";
import { Modal } from "react-bootstrap";
import CustomButton from "../../../../../Components/CustomButton/CustomButton";
import {
  ModalFooter,
  ModalStyle,
} from "../../../../../Components/SideBar/LogoutModal/LogoutModal.style";

function NotProcessModal({ handleClose, Msg, handleshow }) {
  return (
    <ModalStyle show={handleshow} onHide={handleClose} centered>
      <Modal.Body style={{ color: "black", textAlign: "center" }}>
        {Msg}
      </Modal.Body>
      <ModalFooter>
        <CustomButton delNo onClick={handleClose}>
          Close
        </CustomButton>
      </ModalFooter>
    </ModalStyle>
  );
}

export default NotProcessModal;

import React, { Component } from "react";
import { connect } from "react-redux";
import { withRouter } from "react-router";
import { allOrdersThunk } from "../../../../ReduxThunk/MyOrder/Action";
import {
  CurrentOrderWrapper,
  MyOrderContainer,
  PastOrderWrapper,
} from "../../../MyOrder/MyOrder.style";
import NotProcessModal from "./NotProcessModal/NotProcessModal";
import SpecificOrderCard from "./SpecificOrderCard";

export class TicketGenerate extends Component {
  state = {
    notProcessModal: false,
  };
  closeProcessModal = () => {
    this.setState({ notProcessModal: !this.state.notProcessModal });
  };
  componentDidMount() {
    if (this.props.accessToken && this.props.accessToken.Response) {
      this.props.allOrdersThunk({
        accessToken: this.props.accessToken.Response.access_token,
        history: this.props.history,
      });
    }
  }
  render() {
    return (
      <>
        {this.state.notProcessModal && (
          <NotProcessModal
            handleClose={() => this.closeProcessModal()}
            Msg="This order is not yet processed. please check again in some time."
            handleshow={this.state.notProcessModal}
          />
        )}
        <MyOrderContainer>
          <CurrentOrderWrapper>
            {this.props.orderHistory?.currentOrder?.length > 0 && (
              <span>Current orders</span>
            )}
            {this.props.orderHistory?.currentOrder?.map((data) => (
              <SpecificOrderCard
                data={data}
                key={data.orderId}
                toggleEvent={() => this.closeProcessModal()}
                history={this.props.history}
              />
            ))}
          </CurrentOrderWrapper>
          <PastOrderWrapper>
            {this.props.orderHistory?.pastOrder?.length > 0 && (
              <span>Past orders</span>
            )}
            {this.props.orderHistory?.pastOrder?.map((data) => (
              <SpecificOrderCard
                data={data}
                key={data.orderId}
                toggleEvent={() => this.closeProcessModal()}
                history={this.props.history}
              />
            ))}
          </PastOrderWrapper>
        </MyOrderContainer>
      </>
    );
  }
}

const mapStateToProps = (state) => ({
  accessToken: state.loginReducer?.verifyOtpSuccess,
  orderHistory: state.myOrder.allOrdersSuccess,
});

export default withRouter(
  connect(mapStateToProps, { allOrdersThunk })(TicketGenerate)
);

import React, { Component } from "react";
import Button from "../../../../WebComponent/Button/Button";

export class ViewDetailBtn extends Component {
  render() {
    return (
      <>
        {[1, 2, 3, 4, 39, 49, 81, 57].find(
          (data) => data === this.props.statusId
        ) ? (
          // <CustomButton viewDetailForMyOrder onClick={this.props.toggleEvent}>
          //   View Details
          // </CustomButton>
          <div style={{ width: "160px" }}>
            <Button BtnContinue onClick={this.props.toggleEvent}>
              View Details
            </Button>
          </div>
        ) : (
          <div style={{ width: "160px" }}>
            <Button
              BtnContinue
              onClick={() =>
                Router.push({
                  pathname: "/help/options",
                  state: {
                    statusId: this.props.statusId,
                    orderId: this.props.data.orderId,
                    edd: this.props.data.orderDate,
                  },
                })
              }
            >
              View Details
            </Button>
          </div>
        )}
      </>
    );
  }
}

export default ViewDetailBtn;

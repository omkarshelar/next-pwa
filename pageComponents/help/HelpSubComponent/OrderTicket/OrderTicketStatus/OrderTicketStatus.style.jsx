import styled from "styled-components";

export const OrderTicketStatusWrapper = styled.div`
  width: 100%;
  @media screen and (max-width: 468px) {
    position: absolute;
    bottom: 0;
  }
  > div {
    padding: 1rem;
    display: flex;
    flex-direction: column;
  }
  > div > p {
    font-size: large;
    font-weight: 500;
    margin: 0;
  }
  > div > div {
    display: flex;
    flex-direction: column;
    align-items: flex-end;
  }
`;

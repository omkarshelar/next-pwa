import Axios from "axios";
import React, { Component } from "react";
import { connect } from "react-redux";
import CustomButton from "../../../../../Components/CustomButton/CustomButton";
import { EndPoints } from "../../../../../Redux/MyOrders/EndPoints";
import { CustomerService_URL } from "../../../../../Url/Urls";
import NotProcessModal from "../../TicketGenerate/NotProcessModal/NotProcessModal";
import { OrderTicketStatusWrapper } from "../OrderTicketStatus/OrderTicketStatus.style";
import window from "global";
export class TrackRaiseOrder extends Component {
  state = {
    shipmentModal: false,
    shipmentMsg: "",
  };
  returnOrder = async (statusId, mobileNo) => {
    const apiUrl = `https://internal.clickpost.in/api/v1/fetch_return_info/`;

    let orderData = {
      domain: "truemeds",
      reference_number: statusId,
      phone_number: mobileNo,
    };

    Axios.post(apiUrl, orderData, {
      "Content-Type": "application/json",
      "Access-Control-Allow-Origin": "*",
    })
      .then((response) => {
        if (response.data.meta.status === 200) {
          // window.open(
          //   `https://truemeds.clickpost.in/returns?&getAwb=${response.data.result.awb}getShipment_uuid=${response.data.result.shipment_uuid}`
          // );
          window.location.href = `https://truemeds.clickpost.in/returns?&getAwb=${response.data.result.awb}getShipment_uuid=${response.data.result.shipment_uuid}`;
        }
        if (response.data.meta.status === 400) {
          this.setState({
            shipmentModal: true,
            shipmentMsg: response.data.meta.message,
          });
          return true;
        }
      })
      .catch((err) => {
        console.error(err.response);
      });
  };

  trackMyOrder = (access_token, orderid) => {
    const apiUrl = `${CustomerService_URL}${EndPoints.Get_Order_Status}?orderId=${orderid}`;
    let config = {
      headers: {
        transactionId: "a",
        "Content-Type": "application/json",
        Authorization: "Bearer " + access_token,
        "Access-Control-Allow-Origin": "*",
      },
    };
    Axios.post(apiUrl, null, config)
      .then((response) => {
        if (response.status === 200) {
          if (response.data.clickPostTrackingUrl) {
            // window.open(response.data.clickPostTrackingUrl);
            window.location.href = response.data.clickPostTrackingUrl;
          } else {
            Router.push({
              pathname: "/order/status",
              state: {
                trackData: response.data,
              },
            });
          }
        }
      })
      .catch((err) => {
        console.error(err.response);
      });
  };

  windowToReturnOrder = () => {
    let someDate = new Date(this.props.history.location.state.edd);
    someDate.toUTCString();
    let presentDate = new Date();
    let numberOfDaysToAdd = 14;
    someDate.setDate(someDate.getDate() + numberOfDaysToAdd);
    if (presentDate > someDate) {
      return false;
    } else {
      return true;
    }
  };

  render() {
    if (!this.props.history?.location?.state?.orderId) {
      Router.push("/");
    }
    return (
      <>
        {this.state.shipmentModal && (
          <NotProcessModal
            Msg={this.state.shipmentMsg}
            handleClose={() => this.setState({ shipmentModal: false })}
            handleshow={this.state.shipmentModal}
          />
        )}
        <OrderTicketStatusWrapper>
          <div>
            <p>What would you like to do for the selected order?</p>
            <div>
              <CustomButton
                style={{ width: "180px" }}
                delYes
                onClick={() => {
                  if (this.props.token && this.props.token.Response) {
                    this.trackMyOrder(
                      this.props.token.Response.access_token,
                      this.props.history?.location?.state?.orderId
                    );
                  }
                }}
              >
                Track my order
              </CustomButton>
              {this.props.history?.location?.state?.statusId === 55 && (
                <>
                  <CustomButton
                    delNo
                    style={{ width: "180px" }}
                    onClick={() => {
                      Router.push({
                        pathname: "/raiseissue",
                        state: {
                          orderId: this.props.history.location.state.orderId,
                          access_token:
                            this.props.token?.Response?.access_token,
                        },
                      });
                    }}
                  >
                    Raise an issue
                  </CustomButton>

                  {this.windowToReturnOrder() && (
                    <CustomButton
                      delNo
                      style={{ width: "180px" }}
                      onClick={() => {
                        if (this.props.userData) {
                          this.returnOrder(
                            this.props.history.location.state.orderId,
                            this.props.userData.mobileNo
                          );
                        }
                      }}
                    >
                      Return this order
                    </CustomButton>
                  )}
                </>
              )}
            </div>
          </div>
        </OrderTicketStatusWrapper>
      </>
    );
  }
}

const mapStateToProps = (state) => ({
  userData: state.loginReducer?.verifyOtpSuccess?.CustomerDto,
  token: state.loginReducer?.verifyOtpSuccess,
});
export default connect(mapStateToProps, {})(TrackRaiseOrder);

import Axios from "axios";
import React, { Component } from "react";
import { CustomerService_URL, Image_URL } from "../../../../../../Url/Urls";
import {
  TicketDetailWrapper,
  HeaderTicket,
  HeaderInfo,
  HeaderCard,
  CartMed,
  HeaderHistory,
  WholeImages,
  TicketImageContainer,
} from "./TicketDetails.style";

export class TicketDetails extends Component {
  state = {
    ticket: {},
  };
  convertEpoch = (epoch) => {
    var date = new Date(epoch);
    return date.toUTCString().slice(0, 16);
  };
  componentDidMount() {
    const apiUrl = `${CustomerService_URL}/getTicketIssueDetails?ticketId=${this.props.history.location.state.ticketid}&customerId=${this.props.history.location.state.customerId}`;
    let config = {
      headers: {
        transactionId: "a",
        "Content-Type": "application/json",
        Authorization: "Bearer " + this.props.history.location.state.token,
        "Access-Control-Allow-Origin": "*",
      },
    };
    Axios.get(apiUrl, config)
      .then((response) => {
        if (response.status === 200) {
          this.setState({ ticket: response.data });
        }
      })
      .catch((err) => {
        console.error(err.response);
      });
  }
  render() {
    let { ticket } = this.state;
    return (
      <TicketDetailWrapper>
        <HeaderTicket>
          <span>Ticket Detail</span>
        </HeaderTicket>
        <HeaderInfo>
          <span>
            <span style={{ fontWeight: "600" }}> Ticket Id :</span>{" "}
            {ticket.ticketId}
          </span>
          <span>
            <span style={{ fontWeight: "600" }}>Status :</span>{" "}
            {ticket.ticketStatus}
          </span>
          <span>
            <span style={{ fontWeight: "600" }}> Created On :</span>{" "}
            {this.convertEpoch(ticket.creationTime)}
          </span>
        </HeaderInfo>
        {this.state.ticket?.medicines?.length > 0 && (
          <>
            <CartMed>
              <HeaderHistory>Medicines</HeaderHistory>
              {this.state.ticket?.medicines.map((content, index) => (
                <HeaderCard key={index}>
                  <span style={{ fontWeight: "600" }}>
                    {content.orderProductName}
                  </span>
                  <span>
                    <span style={{ fontWeight: "600" }}>Issue Category :</span>{" "}
                    {content.issueCategory}
                  </span>
                  <span>
                    <span style={{ fontWeight: "600" }}>Description :</span>{" "}
                    {content.description}
                  </span>
                </HeaderCard>
              ))}
            </CartMed>
          </>
        )}
        <TicketImageContainer>
          {this.state.ticket?.images?.length > 0 &&
            this.state.ticket?.images.map((data, index) => (
              <img src={`${Image_URL}${data}`} alt="ticket" key={index}></img>
            ))}
          <WholeImages></WholeImages>
        </TicketImageContainer>
      </TicketDetailWrapper>
    );
  }
}

export default TicketDetails;

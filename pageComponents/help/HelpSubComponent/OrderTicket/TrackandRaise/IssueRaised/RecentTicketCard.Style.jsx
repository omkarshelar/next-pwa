import styled from "styled-components";

export const RecentTicketContainer = styled.div`
  display: flex;
  flex-flow: column;
  padding: 10px;
  border: 1px #c0c0c0 solid;
  margin: 5px 0;
  border-radius: 5px;
  width: 95%;

  @media screen and (max-width: 468px) {
    width: 95%;
    margin: 5px 0;
  }
`;

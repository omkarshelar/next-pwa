import styled, { css } from "styled-components";

export const TrackContainer = styled.div`
  position: relative;
  min-height: calc(100vh - 75px);
`;

export const TrackerWrapper = styled.div`
  width: 50%;

  margin: 0 auto;
  @media screen and (max-width: 968px) {
    width: 60%;
  }
  @media screen and (max-width: 768px) {
    width: 70%;
  }
  @media screen and (max-width: 568px) {
    width: 95%;
  }
`;

const wrapperInfo = css`
  margin-left: 10px;
  height: 100%;

  > div {
    display: flex;
    flex-flow: column;
  }
  > div > span:nth-child(1) {
    font-size: large;
    color: #1cb976;
  }
  > div > span:nth-child(2) {
    color: #b8b8b8;
  }

  @media screen and (max-width: 468px) {
    > div > span:nth-child(1) {
      font-size: medium;
      font-weight: 700;
    }
    > div > span:nth-child(2) {
      font-size: smaller;
      font-weight: 500;
    }
  }
`;

export const StatusInfo = styled.div`
  ${wrapperInfo}
`;

export const StatusLineTrack = styled.div`
  height: auto;

  > .roundLineWrapper {
    display: flex;
    flex-flow: column;
    align-items: center;
    justify-content: flex-start;
    width: 28px;
    height: 100%;
    box-sizing: border-box;
  }
  .round {
    min-width: 15px;
    min-height: 15px;
    border-radius: 50%;
    border: 5px solid #1cb976;

    background-color: #1cb976;
  }
  .lineVertical {
    border-left: 2px solid #b8b8b8;
    min-height: 100%;
  }
  .lineVertical.blank {
    display: none;
  }
`;
export const StatusWrapper = styled.div`
  display: flex;
  min-height: 50px;
  margin: 10px;
`;
export const SavingWrapper = styled.div`
  width: 50%;
  background-color: #0071bc;
  display: flex;
  margin: 0 auto;
  align-items: center;
  justify-content: space-evenly;
  border-radius: 5px;
  padding: 10px;

  > div {
    width: 100px;
  }
  > p {
    color: white;
    font-weight: 600px;
    margin-bottom: 0;
  }
  > div > img {
    width: 100%;
  }
  @media screen and (max-width: 968px) {
    width: 60%;
  }
  @media screen and (max-width: 768px) {
    width: 70%;
  }
  @media screen and (max-width: 568px) {
    width: 95%;
  }
`;

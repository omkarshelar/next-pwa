import React, { Component } from "react";
import {
  StatusLineTrack,
  TrackerWrapper,
  StatusInfo,
  StatusWrapper,
  SavingWrapper,
  TrackContainer,
} from "./NativeTrack.Style";
import ReferandEarn from "../../../../../../Assets/ReferandEarn.png";

export class NativeTrackOrder extends Component {
  convertEpoch = (epoch) => {
    var date = new Date(epoch);
    return date.toUTCString().slice(0, 16);
  };
  render() {
    let { orderStatus } = this.props.location.state.trackData.orderTracking;
    let {
      savingPrice,
      orderPrice,
    } = this.props.location.state.trackData.orderTracking;

    return (
      <>
        <TrackContainer>
          <TrackerWrapper>
            {orderStatus.map((data, index) => (
              <StatusWrapper>
                <StatusLineTrack>
                  <div className="roundLineWrapper">
                    <div className="round"></div>
                    {orderStatus.length - 1 === index ? (
                      <div className="lineVertical blank"></div>
                    ) : (
                      <div className="lineVertical"></div>
                    )}
                  </div>
                </StatusLineTrack>
                <StatusInfo>
                  <div>
                    <span>{data.status}</span>
                    <span>{this.convertEpoch(data.date)}</span>
                  </div>
                </StatusInfo>
              </StatusWrapper>
            ))}
          </TrackerWrapper>
          <SavingWrapper>
            <p>
              You saved ₹{savingPrice} on this order against your original
              prescription cost of ₹{orderPrice}.
            </p>
            <div>
              <img src={ReferandEarn} alt="saving"></img>
            </div>
          </SavingWrapper>
        </TrackContainer>
      </>
    );
  }
}

export default NativeTrackOrder;

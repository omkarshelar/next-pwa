import { Card } from "react-bootstrap";
import styled from "styled-components";

export const IssueContainer = styled.div`
  position: relative;
  min-height: calc(100vh - 75px);
  display: flex;
  flex-flow: column;
  justify-content: space-between;
`;

export const IssuePageContainer = styled.div`
  width: 50%;
  margin: 0 auto;

  @media screen and (max-width: 968px) {
    width: 60%;
  }
  @media screen and (max-width: 768px) {
    width: 70%;
  }
  @media screen and (max-width: 568px) {
    width: 100%;
  }
`;

export const MedListForReturn = styled.div`
  opacity: ${(props) => (props.disableArea ? 0.7 : 1)};
  pointer-events: ${(props) => (props.disableArea ? "none" : "auto")};
`;

export const SubmitSection = styled.div`
  width: 50%;
  background-color: white;
  padding: 10px;
  bottom: 0;
  position: sticky;
  margin: 0 auto;

  @media screen and (max-width: 968px) {
    width: 60%;
  }
  @media screen and (max-width: 768px) {
    width: 70%;
  }
  @media screen and (max-width: 568px) {
    width: 100%;
  }
`;
export const PageHeader = styled.div`
  background-color: #0071bc;
  padding: 5px;
  > span {
    color: #fff;
  }
`;

export const MedContainer = styled.div`
  padding: 5px;
`;
export const OptionOther = styled.div`
  padding: 5px 10px;
`;

export const AttachImg = styled.div`
  margin-left: 5px;
  font-weight: 600;
`;

export const UploadContainer = styled.label`
  width: 100px;
  height: 100px;
  display: flex;
  flex-flow: column;
  justify-content: center;
  align-items: center;
  border: 1px #c0c0c0 solid;
  cursor: pointer;
  margin: 5px;
  box-shadow: 0 1px 6px rgba(0, 0, 0, 0.12), 0 1px 4px rgba(0, 0, 0, 0.24);
  border-radius: 10px;
  > img {
    width: 60%;
  }
  > label {
    cursor: pointer;
    color: #0071bc;
    font-size: medium;
  }
  @media screen and (max-width: 468px) {
    width: 80px;
    height: 80px;
    > img {
      width: 40%;
    }
    > label {
      font-size: small;
    }
  }
`;

export const RecentTickets = styled.div`
  display: flex;
  flex-flow: column;
  justify-content: center;
  align-items: center;
`;

export const AccordianCard = styled(Card)`
  margin-bottom: 5px;
  flex-direction: column-reverse;
  border: none;
  > .card-header {
    padding: 5px;
    text-align: center;
    color: white;
    width: 100%;
    background-color: #1cb976;
    border-radius: 5px !important;
  }
  @media screen and (max-width: 468px) {
    > .card-header {
      width: 95%;
      margin: 0 auto;
    }
  }
`;

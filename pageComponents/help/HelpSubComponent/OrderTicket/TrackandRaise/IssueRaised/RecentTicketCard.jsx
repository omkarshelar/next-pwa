import React from "react";
import { RecentTicketContainer } from "./RecentTicketCard.Style";

function RecentTicketCard({ data, goToDetails }) {
  let convertEpoch = (epoch) => {
    var date = new Date(epoch);
    return date.toUTCString().slice(0, 16);
  };
  return (
    <RecentTicketContainer onClick={goToDetails}>
      <span style={{ fontWeight: "600" }}>{data.ticketTitle}</span>
      <span>
        Ticket #{data.ticketId} | {convertEpoch(data.creationTime)}
      </span>
      <span style={{ color: "#1cb976", fontWeight: "600" }}>
        <i className="fad fa-comment-alt-dots"></i>
        {"  "}
        {data.ticketStatus}
      </span>
    </RecentTicketContainer>
  );
}

export default RecentTicketCard;

import styled from "styled-components";

export const SpecificIssueContainer = styled.div`
  display: flex;
  flex-flow: column;
  width: 95%;
  margin: 2%;
  > span {
    color: #1cb976;
    margin-bottom: 0.8rem;
    font-weight: 500;
  }
  > p {
    width: 50%;
    color: #36454f;
  }
  @media screen and (max-width: 468px) {
    > p {
      width: 100%;
      font-size: small;
    }
  }
`;

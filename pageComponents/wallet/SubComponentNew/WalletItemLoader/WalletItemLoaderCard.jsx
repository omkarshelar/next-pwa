import React, { Component } from "react";
import Shimmer from "react-shimmer-effect";
import "./WalletItemLoaderCard.css";

export class WalletItemLoaderCard extends Component {
  render() {
    return (
      <div className="transaction-list-shimmer-container">
        <div className="transaction-item-shimmer-container">
          <div className="first-row-shimmer">
            <Shimmer>
              <p className="row-shimmer-line"></p>
              <p className="row-shimmer-line"></p>
            </Shimmer>
          </div>
          <div className="second-row-shimmer">
            <Shimmer>
              <p className="row-shimmer-line"></p>
              <p className="row-shimmer-line"></p>
            </Shimmer>
          </div>
          <div className="third-row-shimmer">
            <Shimmer>
              <p className="row-shimmer-line"></p>
            </Shimmer>
          </div>
        </div>
        <div className="transaction-item-shimmer-container">
          <div className="first-row-shimmer">
            <Shimmer>
              <p className="row-shimmer-line"></p>
              <p className="row-shimmer-line"></p>
            </Shimmer>
          </div>
          <div className="second-row-shimmer">
            <Shimmer>
              <p className="row-shimmer-line"></p>
              <p className="row-shimmer-line"></p>
            </Shimmer>
          </div>
          <div className="third-row-shimmer">
            <Shimmer>
              <p className="row-shimmer-line"></p>
            </Shimmer>
          </div>
        </div>
        <div className="transaction-item-shimmer-container">
          <div className="first-row-shimmer">
            <Shimmer>
              <p className="row-shimmer-line"></p>
              <p className="row-shimmer-line"></p>
            </Shimmer>
          </div>
          <div className="second-row-shimmer">
            <Shimmer>
              <p className="row-shimmer-line"></p>
              <p className="row-shimmer-line"></p>
            </Shimmer>
          </div>
          <div className="third-row-shimmer">
            <Shimmer>
              <p className="row-shimmer-line"></p>
            </Shimmer>
          </div>
        </div>
      </div>
    );
  }
}

export default WalletItemLoaderCard;

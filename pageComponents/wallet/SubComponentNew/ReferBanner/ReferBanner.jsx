import React, { Component } from "react";
import { connect } from "react-redux";
import Router, { withRouter } from "next/router";

//? Styled Imports
import { SectionTitle } from "../../../wallet/TMWallet.style";
import {
  BannerSectionWrapper,
  CarouselItem,
  CarouselStyle,
  Carouselwrapper,
  LinkSection,
  ReferModalWrapper,
  ReferralTextField,
} from "./ReferBanner.styled";
import { message, Modal } from "antd";
import { IoClose } from "react-icons/io5";

//? Images Imports
import testImg from "../Images/Frame11.png";
import testReferalImg from "../Images/illustration.svg";
import next from "../../../../src/Assets/next.svg";
import prev from "../../../../src/Assets/prev.svg";
import copy from "../../../../src/Assets/copy.svg";
import {
  WhatsappShareButton,
  FacebookShareButton,
  TwitterShareButton,
} from "react-share";
import whatsapp from "../../SubComponentNew/Images/whatsapp.svg";
import facebook from "../../SubComponentNew/Images/facebook.svg";
import twitter from "../../SubComponentNew/Images/twitter.svg";

//? Service Imports
import { redeemCodeThunk } from "../../../../redux/ReferEarn/action";
import { Input } from "antd";
import WalletItemLoaderCard from "../WalletItemLoader/WalletItemLoaderCard";
import BannerLoaderCard from "../WalletItemLoader/BannerLoaderCard";
import window from "global";
import router from "next/router";

//? Constants
message.config({
  maxCount: 1,
  // top: "70%",
  // getContainer: document.getElementById("referModal"),
});
export class ReferBanner extends Component {
  state = {
    referModal: false,
    redeemCode: null,
    referInputFocus: false,
  };

  onChange = (e) => {
    this.setState({ redeemCode: e.target.value });
  };

  kFormatter = (num) => {
    return Math.abs(num) > 999
      ? Math.sign(num) * (Math.abs(num) / 1000).toFixed(1) + "k"
      : Math.sign(num) * Math.abs(num);
  };

  onClickReferEarn = () => {
    if (!this.state.redeemCode) {
      message.error("Please enter a Referral Code");
    }
    // else if (!this.props.isNewCustomer) {
    //   message.error("Referral bonus can be redeemed by new users only");
    // }
    else {
      let val = {
        access_token: this.props.accessToken,
        // mobile: this.props.customerMobileNo,
        mobile: "7506567509",
        customerId: "",
        referCode: `ref_${this.state.redeemCode}`,
      };

      this.props.redeemCodeThunk(val).then(() => {
        if (this.props.referEarn?.redeemError) {
          message.error(this.props.referEarn.redeemError);
        } else if (this.props.referEarn?.redeem?.Claimed === "OK") {
          message.error("Referral Code already Redeemed");
        } else if (this.props.referEarn?.redeem?.Conflict === "CONFLICT") {
          message.error("Invalid Referral Code");
        } else if (this.props.referEarn?.redeem?.SuccessText) {
          message.success(this.props.referEarn?.redeem?.SuccessText);
        }
      });
    }
  };
  render() {
    return (
      <BannerSectionWrapper>
        {this.props.transactionData &&
        this.props.transactionData.length === 0 &&
        this.kFormatter(
          this.props.info?.payload?.truemedsCash +
            this.props.info?.payload?.truemedsCredit
        ) === 0 ? (
          <SectionTitle>Earn TM Reward</SectionTitle>
        ) : (
          <SectionTitle>Earn extra TM Reward</SectionTitle>
        )}

        <Carouselwrapper>
          {!this.props.transactionData ? (
            <BannerLoaderCard />
          ) : (
            <CarouselStyle
              indicators={false}
              next={this.props.banners?.length > 1 ? next : false}
              prev={this.props.banners?.length > 1 ? prev : false}
            >
              {/* <CarouselItem>
              <div
                className="banner-wrapper"
                onClick={() => this.setState({ referModal: true })}
              >
                <img src={testImg} alt="testImg" />
              </div>
            </CarouselItem> */}
              {window.innerWidth > 668
                ? this.props.banners &&
                  this.props.banners.map((content, pos) => (
                    <CarouselItem key={pos}>
                      <div
                        className="banner-wrapper"
                        // onClick={() => this.setState({ referModal: true })}
                        onClick={() => Router.push("/refer")}
                      >
                        <img src={content.webUrl} alt="testImg" />
                      </div>
                    </CarouselItem>
                  ))
                : this.props.banners &&
                  this.props.banners.map((content, pos) => (
                    <CarouselItem key={pos}>
                      <div
                        className="banner-wrapper"
                        // onClick={() => this.setState({ referModal: true })}
                        onClick={() => Router.push("/refer")}
                      >
                        <img src={content.image} alt="testImg" />
                      </div>
                    </CarouselItem>
                  ))}
            </CarouselStyle>
          )}
        </Carouselwrapper>

        <Modal
          visible={this.state.referModal}
          footer={null}
          centered
          width={600}
          onCancel={() => this.setState({ referModal: false })}
          closeIcon={<IoClose style={{ fontSize: "x-large" }} />}
          // id="referModal"
        >
          <ReferModalWrapper>
            <div className="leftSide">
              <SectionTitle style={{ marginBottom: "0.5em" }}>
                Refer & Earn
              </SectionTitle>
              <p>
                Your friend will now thank you twice. For referring the Truemeds
                app and for helping them earn <b>₹200</b> on their first
                purchase. As a bonus, you earn Truemeds cash too.
              </p>

              <LinkSection>
                {this.props.info && this.props.info.payload && (
                  <span className="copy-text">
                    referral Code{" "}
                    <strong>
                      {this.props.info.payload.referCode.substring(4)}
                    </strong>
                  </span>
                )}
                <span
                  onClick={() => {
                    const el = document.createElement("input");
                    el.value = this.props.info?.payload?.referCode.substring(4);
                    document.body.appendChild(el);
                    el.select();
                    document.execCommand("copy");
                    document.body.removeChild(el);
                    message.success("Referral Code copied successfully");
                  }}
                >
                  <img src={copy} alt="imgWallet" />
                </span>
              </LinkSection>
              <br />

              <p>
                Enter the referral code below to claim <b>₹200.</b>
              </p>

              <div className="flexDiv">
                <ReferralTextField
                  id="standard-basic"
                  name="redeemCode"
                  label="Enter Referral Code"
                  value={this.state.redeemCode}
                  onChange={(e) => this.onChange(e)}
                  autoComplete="off"
                  variant="outlined"
                  placeholder={`${
                    this.state.referInputFocus ? "" : "Enter Referral Code"
                  } `}
                  onFocus={() => this.setState({ referInputFocus: true })}
                  onBlur={() => this.setState({ referInputFocus: false })}
                />
                <button
                  style={{
                    width: "fit-content",
                    padding: "8px 14px",
                    borderRadius: "5px",
                    backgroundColor: "#0071bc",
                    cursor: "pointer",
                    color: "#fff",
                    fontWeight: "600",
                    whiteSpace: "nowrap",
                    height: "45px",
                    border: "none",
                  }}
                  onClick={this.onClickReferEarn}
                >
                  Claim Referral
                </button>
              </div>
            </div>

            <div className="rightSide">
              <img
                src={testReferalImg}
                alt="referal_img"
                className="referImg"
              />

              <p>Share with others</p>

              <div className="socialShare">
                <WhatsappShareButton
                  url={this.props.info && this.props.info?.payload?.referLink}
                  title={
                    this.props.info &&
                    this.props.info.payload &&
                    `Download the Truemeds app to enjoy flat 20% savings on every other.\nUse the link or code below to get ₹200 off on your first order. Code ${this.props.info.payload.referCode.substring(
                      4
                    )} or Link - `
                  }
                  style={{
                    margin: "0 5px",
                  }}
                >
                  <img
                    src={whatsapp}
                    alt="social"
                    style={{ width: "26px", height: "26px" }}
                  ></img>
                </WhatsappShareButton>
                <FacebookShareButton
                  url={this.props.info && this.props.info?.payload?.referLink}
                  quote={
                    this.props.info &&
                    this.props.info.payload &&
                    `Download the Truemeds app to enjoy flat 20% savings on every other.\nUse the link or code below to get ₹200 off on your first order. Code ${this.props.info.payload.referCode.substring(
                      4
                    )} or Link - ${this.props.info?.payload?.referLink}`
                  }
                  style={{
                    margin: "0 5px",
                  }}
                >
                  <img
                    src={facebook}
                    alt="social"
                    style={{ width: "22px", height: "22px" }}
                  ></img>
                </FacebookShareButton>
                <TwitterShareButton
                  url={this.props.info && this.props.info?.payload?.referLink}
                  title={
                    this.props.info &&
                    this.props.info.payload &&
                    `Download the Truemeds app to enjoy flat 20% savings on every other.\nUse the link or code below to get ₹200 off on your first order. Code ${this.props.info.payload.referCode.substring(
                      4
                    )} or Link - `
                  }
                  style={{
                    margin: "0 5px",
                  }}
                >
                  <img
                    src={twitter}
                    alt="social"
                    style={{ width: "22px", height: "22px" }}
                  ></img>
                </TwitterShareButton>
              </div>
            </div>
          </ReferModalWrapper>
        </Modal>
      </BannerSectionWrapper>
    );
  }
}

const mapStateToProps = (state) => ({
  // customerId: state.loginReducer.verifyOtpSuccess?.CustomerId,
  // accessToken: state.loginReducer.verifyOtpSuccess?.Response?.access_token,
  referEarn: state.referEarn,
  // customerMobileNo: state.loginReducer?.verifyOtpSuccess?.CustomerDto?.mobileNo,
});

export default withRouter(
  connect(mapStateToProps, {
    redeemCodeThunk,
  })(ReferBanner)
);

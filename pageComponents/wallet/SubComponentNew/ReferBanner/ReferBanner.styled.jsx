import styled from "styled-components";
import { Carousel } from "react-bootstrap";
import TextField from "@material-ui/core/TextField";
import { Input } from "antd";

export const Carouselwrapper = styled.div`
  width: 100%;
  position: relative;
  margin: 0 auto;
`;

export const CarouselItem = styled(Carousel.Item)`
  > div[class="banner-wrapper"] {
    width: 100%;
    position: relative;
    display: flex !important;
    cursor: pointer;

    img {
      width: 100%;
    }

    img[alt="banner"],
    video {
      width: 100%;
      cursor: pointer;
    }
  }

  > div[class="caption"] {
    position: absolute;
    width: 340px;
    top: 15%;
    left: 10%;
    > span {
      font-family: "Century Gothic", sans-serif;
      font-size: 40px;
      font-style: normal;
      font-weight: 700;
      line-height: 61px;
      letter-spacing: 0px;
      text-align: left;
      color: #003055;
      @media screen and (max-width: 1068px) {
        font-size: 40px;
      }
      @media screen and (max-width: 968px) {
        font-size: 30px;
      }
      @media screen and (max-width: 768px) {
        font-size: 20px;
      }
      @media screen and (max-width: 568px) {
        display: none;
      }
    }
    > p {
      font-size: 16px;
      font-style: normal;
      font-weight: 400;
      line-height: 21px;
      letter-spacing: 0em;
      text-align: left;
      color: #003055;
      @media screen and (max-width: 1068px) {
        font-size: 14px;
      }
      @media screen and (max-width: 968px) {
        font-size: 12px;
      }
      @media screen and (max-width: 568px) {
        display: none;
      }
    }

    @media screen and (max-width: 568px) {
      bottom: 1rem;
      top: auto;
      left: 50%;
      display: flex;
      align-items: center;
      transform: translate(-50%, -50%);
      justify-content: center;
    }
  }

  @media screen and (max-width: 668px) {
    > div[class="banner-wrapper"] {
      img[alt="banner"] {
        width: 100%;
        cursor: pointer;
      }
    }
  }
`;

export const CarouselStyle = styled(Carousel)`
  .carousel-control-prev-icon {
    width: 50px;
    height: 50px;
    background-image: ${(props) => `url(${props.prev})`};
  }

  .carousel-control-next-icon {
    background-image: ${(props) => `url(${props.next})`};
    width: 50px;
    height: 50px;
  }
  .carousel-control-prev {
    transform: translateX(-50px);
    opacity: 1;
  }
  .carousel-control-next {
    transform: translateX(50px);
    opacity: 1;
  }
  @media screen and (max-width: 1068px) {
    .carousel-control-prev {
      transform: translateX(-30px);
      opacity: 1;
    }
    .carousel-control-next {
      transform: translateX(30px);
      opacity: 1;
    }
  }

  @media only screen and (min-width: 768px) and (max-width: 1220px) {
    .carousel-control-prev-icon {
      width: 40px;
      height: 40px;
    }

    .carousel-control-next-icon {
      width: 40px;
      height: 40px;
    }
  }

  @media screen and (max-width: 668px) {
    .carousel-control-prev-icon {
      width: 30px;
      height: 30px;
    }

    .carousel-control-next-icon {
      width: 30px;
      height: 30px;
    }
    .carousel-control-prev {
      transform: translateX(-5px);
      opacity: 1;
    }
    .carousel-control-next {
      transform: translateX(5px);
      opacity: 1;
    }
  }
`;

//? Accordian Styling

export const ReferModalWrapper = styled.div`
  width: 100%;
  height: auto;
  display: grid;
  grid-template-columns: 1.5fr auto;
  gap: 1em;

  @media only screen and (max-width: 767px) {
    grid-template-columns: auto;
  }

  .flexDiv {
    display: flex;
    justify-content: center;
    align-items: center;
    gap: 1em;
  }

  .rightSide {
    .referImg {
      margin: 2em 0 1em 0;

      @media only screen and (max-width: 767px) {
        display: none;
      }
    }

    p {
      font-weight: 700;
    }
  }

  p b {
    font-size: large;
  }
`;

export const LinkSection = styled.div`
  border-radius: 4px;
  width: 100%;
  border: 2px dashed #0071bc;
  height: 50px;
  background: #e5f1f8;
  display: flex;
  align-items: center;
  justify-content: space-between;
  padding: 10px 15px;
  > span:nth-child(1) {
    color: #808080;
    white-space: nowrap;
    overflow: hidden !important;
    text-overflow: ellipsis;
    margin-top: 2px;
  }
  > span:nth-child(2) {
    display: flex;
    justify-content: flex-end;
    align-items: center;
  }
  > span:nth-child(2) > img {
    width: 100%;
    cursor: pointer;
  }
  > span[class="copy-text"] {
    font-family: "Raleway", sans-serif;
    font-style: normal;
    font-weight: 500;
    font-size: 14px;
    line-height: 16px;
    text-align: center;
    letter-spacing: 0.02em;
    text-transform: capitalize;
    color: #0071bc;
    > span {
      font-weight: 700;
      font-family: "Century Gothic", sans-serif;
    }
  }
`;

export const ReferralTextField = styled(Input)`
  box-sizing: border-box;
  width: 100%;
  background: var(--accent-light);
  border: 1px solid var(--accent);
  border-radius: 5px;
  color: var(--accent);
  font-weight: 600;
  padding: 0.75em;
  /* margin-right: 2% !important; */

  &::placeholder {
    color: var(--accent);
    font-weight: 600;
  }

  .MuiInputLabel-outlined {
    color: var(--accent);
  }
  .MuiOutlinedInput-notchedOutline,
  .MuiOutlinedInput-root:hover .MuiOutlinedInput-notchedOutline,
  .MuiOutlinedInput-root.Mui-focused .MuiOutlinedInput-notchedOutline {
    border-color: var(--accent);
  }
  .MuiFormLabel-root.Mui-focused,
  .MuiInputBase-input {
    color: var(--accent);
  }
  @media screen and (max-width: 568px) {
    margin-right: 0 !important;
    margin-top: 8px !important;
    margin-bottom: 8px !important;
  }
`;

export const BannerSectionWrapper = styled.div`
  padding: 0 2em;

  @media only screen and (max-width: 767px) {
    padding: 0 1em;
  }

  @media only screen and (min-width: 768px) and (max-width: 1220px) {
    padding: 0 1.5em;
  }
`;

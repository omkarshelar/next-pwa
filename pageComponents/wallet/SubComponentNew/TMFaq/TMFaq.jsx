import React, { Component } from "react";

//? Styled Imports
import "./TMFaq.styled";
import Accordion from "@material-ui/core/Accordion";
import AccordionSummary from "@material-ui/core/AccordionSummary";
import AccordionDetails from "@material-ui/core/AccordionDetails";
import ExpandMoreIcon from "@material-ui/icons/ExpandMore";
import { SectionTitle } from "../../TMWallet.style";
import { FAQDiv, List } from "./TMFaq.styled";
import { Button, Divider } from "antd";
import { MdOutlineChevronRight } from "react-icons/md";
import { Dialog, IconButton } from "@material-ui/core";
import DialogContent from "@material-ui/core/DialogContent";
import DialogTitle from "@material-ui/core/DialogTitle";
import { IoClose, IoArrowBack } from "react-icons/io5";
import Slide from "@material-ui/core/Slide";
import WalletItemLoaderCard from "../WalletItemLoader/WalletItemLoaderCard";
import window from "global";

//? Images Imports
//? Service Imports
//? Constants
const TransitionMob = React.forwardRef(function Transition(props, ref) {
  return <Slide direction="up" ref={ref} {...props} />;
});

const TransitionWeb = React.forwardRef(function Transition(props, ref) {
  return <Slide direction="left" ref={ref} {...props} />;
});

export class TMFaq extends Component {
  state = {
    faqModal: false,
  };
  render() {
    return (
      <FAQDiv>
        <SectionTitle>Frequently asked questions</SectionTitle>

        <List>
          <div className="FAQModalBody" style={{ padding: 0 }}>
            {this.props.loading ? (
              <WalletItemLoaderCard />
            ) : this.props.faqData ? (
              this.props.faqData.slice(0, 3).map((item, pos) => (
                <Accordion square key={pos}>
                  <AccordionSummary
                    expandIcon={<ExpandMoreIcon />}
                    aria-controls="panel1a-content"
                    id="panel1a-header"
                  >
                    <span className="faq-list-text">{item.question}</span>
                  </AccordionSummary>
                  <AccordionDetails style={{ paddingLeft: "2.5em" }}>
                    {item.answer}
                  </AccordionDetails>
                </Accordion>
              ))
            ) : (
              ""
            )}
          </div>
          <Button
            type="link"
            seeMore
            style={{ color: "#0071BC" }}
            onClick={() => this.setState({ faqModal: true })}
          >
            View More <MdOutlineChevronRight />
          </Button>

          <Button
            type="link"
            seeMore
            style={{
              color: "#868889",
              textDecoration: "underline",
              fontWeight: 400,
            }}
            className="termsBtn"
            onClick={() =>
              Router.push({
                pathname: "/option/legals/tnc",
                state: { isTMWALLET: true },
              })
            }
          >
            Terms & Conditions
          </Button>

          <Dialog
            fullWidth
            fullHeight
            maxWidth={"xs"}
            open={this.state.faqModal}
            className="FAQModal"
            // onClose={() => this.setState({ faqModal: false })}
            // TransitionComponent={TransitionWeb}
            TransitionComponent={
              window.innerWidth > 767 ? TransitionWeb : TransitionMob
            }
          >
            <div className="FAQModalBody">
              <div className="titleWrapper">
                {/* <IconButton
                  edge="start"
                  color="inherit"
                  onClick={() => this.setState({ faqModal: false })}
                  aria-label="close"
                >
                  <IoClose />
                </IconButton> */}
                <SectionTitle style={{ padding: "0 5px", fontSize: "20px" }}>
                  Frequently asked questions
                </SectionTitle>

                <Button
                  type="text"
                  style={{ fontSize: "x-large" }}
                  onClick={() => this.setState({ faqModal: false })}
                >
                  {window.innerWidth > 767 ? <IoClose /> : <IoArrowBack />}
                </Button>
              </div>

              <div className="faqTitleDivider"></div>
              <DialogContent>
                {this.props.faqData
                  ? this.props.faqData.map((item, pos) => (
                      <Accordion square key={pos}>
                        <AccordionSummary
                          expandIcon={<ExpandMoreIcon />}
                          aria-controls="panel1a-content"
                          id="panel1a-header"
                        >
                          <span className="faq-list-text">{item.question}</span>
                        </AccordionSummary>
                        <AccordionDetails style={{ paddingLeft: "2.5em" }}>
                          {item.answer}
                        </AccordionDetails>
                      </Accordion>
                    ))
                  : ""}
              </DialogContent>
            </div>
          </Dialog>
        </List>
      </FAQDiv>
    );
  }
}

import styled from "styled-components";

export const FAQDiv = styled.div`
  height: max-content;
  position: sticky;
  top: calc(0% + 100px);
`;

export const List = styled.div`
  width: 100%;
  display: flex;
  flex-direction: column;
  justify-content: space-between;
  @media screen and (max-width: 568px) {
    /* width: 95%; */
    margin: auto;
  }

  > button {
    /* align-self: left; */
    padding-left: 0;
    padding-right: 0;
    margin-top: 1em;
    display: flex;
    justify-content: flex-start;
    align-items: center;
    gap: 0.3em;
    font-size: small;
    font-weight: 700;
    svg {
      font-size: medium;
    }

    @media screen and (max-width: 767px) {
      &.termsBtn {
        margin-top: 2em;
      }
    }
  }

  .MuiAccordion-root {
    border-bottom: 1px solid var(--border-clr) !important;

    .faq-list-text {
      font-weight: 600;
      font-size: medium;
      display: flex;

      &::before {
        content: "Q.";
        margin-right: 1em;
        /* font-weight: 700; */
      }
    }
  }

  .MuiAccordionSummary-content.Mui-expanded {
    margin: 12px 0 !important;
  }

  .MuiAccordionSummary-root {
    align-items: flex-start !important;
  }

  .MuiAccordionSummary-root.Mui-expanded {
    min-height: unset;
  }

  .MuiAccordion-root.Mui-expanded {
    margin: unset;
  }
`;

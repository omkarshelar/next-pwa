import styled from "styled-components";

export const TMWalletMainSection = styled.div`
  .topWalletSection {
    width: 100%;
    background: var(--dark-clr);
    color: #fff;
    padding: 2em 2em 5vh 2em;

    @media only screen and (max-width: 767px) {
      padding: 2vh 1em 7vh 1em;
    }

    @media only screen and (min-width: 768px) and (max-width: 1220px) {
      padding: 3vh 1.5em 9vh 1.5em;
    }

    .walletBox {
      background: var(--green);
      border-radius: 1em;
      padding: 4em 2em;
      display: flex;
      justify-content: space-between;
      align-items: center;
      gap: 1em;
      overflow: hidden;
      position: relative;
      margin-bottom: 1.5em;

      @media (max-width: 768px) {
        padding: 2em 1em;
      }

      .walletText {
        display: flex;
        flex-flow: column;
        gap: 0.5em;

        h5,
        h1 {
          color: #fff;
          margin: 0;
          padding: 0;
        }

        h1 {
          font-weight: 700;
          text-shadow: var(--shadow);
        }

        @media (max-width: 768px) {
          h5 {
            font-size: small;
          }

          h1 {
            font-size: xx-large;
          }
        }
      }

      .walletBoxImg {
        z-index: 0;
        img {
          z-index: 99;
          position: absolute;
          right: -2%;
          top: 50%;
          width: 35%;
          min-height: 100%;
          transform: translate(0%, -50%);

          @media (max-width: 768px) {
            right: 0;
            width: 35%;
          }

          @media (min-width: 769px) and (max-width: 1220px) {
            /* width: 100px; */
            position: absolute;
            right: 0;
            top: 50%;
            width: 40%;
            min-height: 100%;
            transform: translate(0%, -50%);
          }
        }

        .walletImgBackground {
          &::before {
            content: "";
            width: 275px;
            height: 275px;
            border-radius: 50%;
            background: #73ce71;
            display: block;
            position: absolute;
            right: -25%;
            top: 50%;
            z-index: -1;
            transform: translate(-50%, -50%);

            @media only screen and (min-width: 768px) and (max-width: 1220px) {
              right: -55%;
            }

            @media only screen and (max-width: 767px) {
              right: -95%;
            }
          }
        }
      }
    }
  }

  .topWalletSection.newuser {
    padding-bottom: 2em;

    @media only screen and (max-width: 767px) {
      padding-bottom: 1em;
    }

    @media only screen and (min-width: 768px) and (max-width: 1220px) {
      padding: 1.5em;
    }
  }

  .transactionSection {
    width: 100%;
    padding: 0 2em;
    display: grid;
    grid-template-columns: repeat(2, 1fr);
    gap: 1em;
    align-items: center;
    margin: -2em 0 3em 0;

    @media only screen and (min-width: 768px) and (max-width: 1220px) {
      padding: 0 1em;
      margin-top: -4em;
    }

    @media only screen and (max-width: 767px) {
      padding: 0 0.5em;
      margin-top: -4em;
      gap: 0.5em;
    }

    .transactionBox {
      background: #fff;
      border-radius: 0.75em;
      border: 1px solid var(--border-clr);
      box-shadow: var(--shadow);
      padding: 1em;
      cursor: pointer;
      transition: all 200ms ease-in-out;

      @media only screen and (max-width: 767px) {
        padding: 0.75em;
      }

      &:hover {
        box-shadow: 0px 3.5px 5.3px rgba(0, 0, 0, 0.061),
          0px 11.8px 17.9px rgba(0, 0, 0, 0.02),
          0px 53px 80px rgba(0, 0, 0, 0.005);

        /* transform: translateY(-5px); */
      }

      h6,
      h4,
      button {
        font-weight: 700;
        margin: 0;
      }

      h6 {
        font-weight: 500;
        margin-bottom: 0.5em;
      }

      button {
        padding-left: 0;
        padding-right: 0;
        margin-top: 1em;
        display: flex;
        justify-content: flex-start;
        align-items: center;
        gap: 0.3em;
        font-size: small;

        svg {
          font-size: medium;
        }
      }
    }
  }
`;

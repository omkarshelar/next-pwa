import styled from "styled-components";

export const TransactionBox = styled.div`
  padding: 0 2em;
  margin-top: 2em;
  max-height: 500px;
  overflow-x: hidden;
  position: relative;

  &.scrollable {
    overflow-y: scroll;
  }

  @media only screen and (max-width: 767px) {
    padding: 0;

    margin-top: 1em;
  }

  .TransactionFlexRow {
    display: flex;
    flex-flow: row wrap;
    gap: 1em;
    justify-content: space-between;
    align-items: center;
    margin-bottom: 2em;
    position: sticky;
    background: #fff;
    top: 0;
    z-index: 10;
    @media only screen and (max-width: 767px) {
      height: 50px;
    }

    h4 {
      margin: 0;
    }
  }
`;

export const RewardTransactionWrapper = styled.div`
  width: 100%;
  height: auto;

  .topWalletSection {
    width: 100%;
    background: var(--dark-clr);
    color: #fff;
    padding: 2em;
    position: relative;

    .backBtn {
      background-color: #ccc;
      padding: 0.25em 0.75em;
      display: flex;
      justify-content: center;
      align-items: center;
      gap: 0.5em;
      font-family: sans-serif;
      font-size: 12px;
      font-weight: 700;
      color: var(--dark-clr);
      position: absolute;
      top: 0;
      left: 0;
      cursor: pointer;
    }

    @media only screen and (max-width: 767px) {
      padding: 1em;
    }

    @media only screen and (min-width: 768px) and (max-width: 1220px) {
      padding: 1.5em;
    }

    .walletBox {
      /* background: var(--green); */
      border-radius: 1em;
      /* padding: 2em; */
      display: flex;
      justify-content: space-between;
      align-items: center;
      gap: 1em;
      overflow: hidden;
      position: relative;
      margin-bottom: 1.5em;

      .walletText {
        display: flex;
        flex-flow: column;
        gap: 0.5em;

        h5,
        h1 {
          color: #fff;
          margin: 0;
          padding: 0;
        }

        h1 {
          font-weight: 700;
        }

        @media (max-width: 768px) {
          h5 {
            font-size: small;
          }

          h1 {
            font-size: xx-large;
          }
        }
      }

      .walletBoxImg {
        z-index: 0;
        img {
          z-index: 99;

          @media only screen and (max-width: 1220px) {
            width: 100px;
          }
        }
      }
    }
  }
`;

export const TrasactionItem = styled.div`
  display: flex;
  width: 100%;
  padding: 1rem 0;
  box-sizing: border-box;
  justify-content: space-between;
  /* @media screen and (max-width: 968px) {
    width: 70%;
  }
  @media screen and (max-width: 868px) {
    width: 80%;
  }
  @media screen and (max-width: 668px) {
    width: 96%;
  } */
`;

export const HR = styled.hr`
  border: none;
  border-top: 1px solid var(--dark-clr);
  opacity: 0.3;
  width: 100%;
  margin: 0.25em;
  /* @media screen and (max-width: 968px) {
    width: 60%;
  }
  @media screen and (max-width: 668px) {
    width: 96%;
  } */
`;
export const FirstRow = styled.div`
  font-family: "Century Gothic", sans-serif;
  font-style: normal;
  font-weight: 700;
  font-size: large;
  /* line-height: 130.2%; */
  color: #333333;
  width: 50%;
  max-width: 50%;
  display: flex;
  flex-flow: column;
  gap: 0.2em;
  p {
    margin: 0;
    font-size: 15px;
  }
  > p[class="transaction-info"] {
    font-family: "Century Gothic", sans-serif;
    font-size: 12px;
    font-style: normal;
    font-weight: 600;
    text-align: left;
    color: #999999;
  }
  @media screen and (max-width: 468px) {
    > p[class="transaction-info"] {
      font-size: 13px;
    }
  }
`;

export const SecondRow = styled.div`
  text-align: right;
  width: 25%;
  max-width: 25%;
  display: flex;
  flex-flow: column;
  align-items: flex-end;
  gap: 0.2em;
  p {
    margin: 0;
    font-size: 15px;
  }
  .earned-text {
    font-family: "Century Gothic", sans-serif;
    font-style: normal;
    font-weight: 700;
    font-size: large;
    line-height: 130.2%;
    color: #4d8f4c;
    font-size: 15px;

    &::before {
      content: "+";
      color: #4d8f4c;
      display: inline-block;
      margin-right: 0.5em;
    }
  }
  .spent-text {
    font-family: "Century Gothic", sans-serif;
    font-style: normal;
    font-weight: 700;
    font-size: large;
    line-height: 130.2%;
    color: #ff755b;
    font-size: 15px;

    &::before {
      content: "-";
      color: #ff755b;
      display: inline-block;
      margin-right: 0.5em;
    }
  }
  .transaction-date {
    font-family: "Raleway", sans-serif;
    color: #999;
    font-style: normal;
    font-weight: 600;
    font-size: 13px;
    /* line-height: 130.2%; */
  }
`;

export const ThirdRow = styled.div`
  width: 25%;
  max-width: 25%;
  text-align: right;
  font-family: "Century Gothic", sans-serif;
  font-style: normal;
  font-weight: bold;
  font-size: 16px;
`;

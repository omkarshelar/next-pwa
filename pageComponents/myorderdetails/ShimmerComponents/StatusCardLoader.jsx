import React from "react";
import Shimmer from "react-shimmer-effect";

import "./StatusCardLoader.module.css";

const StatusCardLoader = (props) => {
  return (
    <div>
      <Shimmer>
        <div className={"orderDetails_cardWrap"}></div>
      </Shimmer>
    </div>
  );
};

export default StatusCardLoader;

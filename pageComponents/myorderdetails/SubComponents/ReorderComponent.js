import React from "react";
import "./ReturnComponent.module.css";
import moment from "moment";
import Button from "../../../components/WebComponents/Button/Button";
import returnCheck from "./returnCheck";
import HeaderLoader from "../../../components/WebComponents/Shimmer/HeaderLoader";

const ReorderComponent = (props) => {
  let returnLastDate = moment(props.updateDetail?.deliveryDate)
    .add(15, "d")
    .format("D MMM");

  return (
    <div>
      {props.isLoading ? (
        <div style={{ marginTop: "24px" }}>
          <HeaderLoader />
        </div>
      ) : (
        <div className={"orderDetails_reorderContainerWrap"}>
          {props.updateDetail?.productSubsMappingList?.length > 0 ? (
            <Button reOrderDetails onClick={() => props.reorderHandler()}>
              Reorder
            </Button>
          ) : null}
          {returnCheck(
            props.updateDetail?.deliveryDate,
            props.orderStatus?.orderTracking?.orderStatus
          ) ||
          props.updateDetail?.orderStatus?.serialId === 57 ||
          props.updateDetail?.orderStatus?.serialId === 199 ? null : (
            <div className={"orderDetails_reorderWindowWrap"}>
              <span>Return window closed on {returnLastDate}</span>
            </div>
          )}
        </div>
      )}
    </div>
  );
};

export default ReorderComponent;

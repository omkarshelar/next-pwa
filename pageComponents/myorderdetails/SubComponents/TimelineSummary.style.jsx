import styled from "styled-components";

export const TimelineSummaryWrapper = styled.div`
  margin-bottom: 24px;
  .MuiAccordionSummary-root {
    min-height: 48px !important;
    padding: 0px 16px !important;
    height: 48px;
    background: rgb(237, 242, 249);
    margin: 0px;
    border-radius: 4px;
  }
  .MuiCollapse-entered {
    margin-bottom: -8px;
  }

  .accordianTitle {
    font-style: normal;
    font-weight: 700;
    font-size: 16px;
    line-height: 24px;
    color: #40464d;
  }

  .accordianDate {
    font-style: normal;
    font-weight: 500;
    font-size: 14px;
    line-height: 20px;
    text-align: right;
    text-transform: capitalize;
    color: #40464d;
    margin-left: 4px;
  }
  .MuiAccordionSummary-content {
    align-items: baseline;
  }

  .MuiAccordion-root.Mui-expanded {
    margin: 0px;
  }

  @media screen and (max-width: 768px) {
    margin-bottom: 16px;
    .MuiAccordion-root.Mui-expanded {
      margin: 0px;
    }
    .MuiAccordionSummary-root {
      min-height: 56px !important;
      padding: 0px 16px !important;
      height: 56px;
      background: rgb(255, 255, 255);
      margin: 0px;
      border-radius: 4px;
    }

    .accordianDate {
      line-height: 24px;
    }

    .MuiCollapse-entered {
      margin-bottom: 0px;
    }
  }
`;

export const TimelineDetails = styled.div`
  padding: 16px 0px 14px 0px;
  @media screen and (max-width: 768px) {
    padding: 0px 16px 16px 16px;
  }
`;

export const TimelineSummaryTileWrap = styled.div`
  display: flex;
  justify-content: space-between;
  align-items: center;
  margin-left: 4px;

  .connectionPoint {
    width: 8px;
    height: 8px;
    border-radius: 50%;
    background: #7da0c0;
    margin-right: 20px;
  }

  .tickPoint {
    margin-left: -4px;
    margin-right: 16px;
  }

  .topPad {
    padding-top: 3px;
  }
  .tileTitle {
    font-style: normal;
    font-weight: 700;
    font-size: 14px;
    height: 18px;
    text-transform: capitalize;
    color: #6a737a;
  }

  .dateFormat {
    font-style: normal;
    font-weight: 700;
    font-size: 14px;
    line-height: 18px;
    text-align: right;
    text-transform: capitalize;
    color: #40464d;
  }

  .leftWrap {
    display: flex;
    align-items: center;
  }

  @media screen and (max-width: 768px) {
    .tileTitle {
      font-size: 12px;
    }

    .dateFormat {
      font-size: 12px;
    }
  }
`;

export const TimelineSummaryTileConnector = styled.div`
  border-left: 2px solid #7da0c0;
  height: 22px;
  margin-left: 7px;
`;

export const TimelineSummaryTileConnectorSmall = styled.div`
  border-left: 2px solid #7da0c0;
  height: 18px;
  margin-left: 7px;
`;

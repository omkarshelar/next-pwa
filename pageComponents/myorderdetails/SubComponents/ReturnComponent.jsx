import React from "react";
import "./ReturnComponent.module.css";
import moment from "moment";
import returnCheck from "./returnCheck";
import window from "global";

const ReturnComponent = (props) => {
  let returnLastDate = moment(props.updateDetail?.deliveryDate)
    .add(15, "d")
    .format("D MMM");

  return (
    <div className={"orderDetails_returnContainerWrap"}>
      <div className={"orderDetails_returnWrap"}>
        <div className={"orderDetails_returnButtonWrap"}>
          {returnCheck(
            props.updateDetail?.deliveryDate,
            props.orderStatus?.orderTracking?.orderStatus
          ) && props.updateDetail?.orderStatus?.serialId !== 199 ? (
            <div
              className={"orderDetails_returnTitle"}
              onClick={() => props.returnOrder()}
            >
              Return Order
            </div>
          ) : null}
          {returnCheck(
            props.updateDetail?.deliveryDate,
            props.orderStatus?.orderTracking?.orderStatus
          ) && props.updateDetail?.orderStatus?.serialId !== 199 ? (
            <span>Return available till {returnLastDate}</span>
          ) : window.innerWidth <= 768 &&
            props.updateDetail?.orderStatus?.serialId !== 57 &&
            props.updateDetail?.orderStatus?.serialId !== 199 ? (
            <span style={{ fontWeight: "700" }}>
              Return window closed on {returnLastDate}
            </span>
          ) : null}
        </div>
        {props.updateDetail?.cancelledReason ? (
          <div className={"orderDetails_canReasonWrap"}>
            <div className={"orderDetails_canReasonTitle"}>
              {"Cancellation reason: "}
            </div>
            <div className={"orderDetails_canReasonValue"}>
              {props.updateDetail?.cancelledReason}
            </div>
          </div>
        ) : null}
      </div>
    </div>
  );
};

export default ReturnComponent;

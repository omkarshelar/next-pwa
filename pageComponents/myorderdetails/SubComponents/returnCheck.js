const returnCheck = (deliveryDate, orderStatus) => {
  let today = new Date().getTime();
  return (
    Number(today) >= Number(new Date(deliveryDate).getTime()) &&
    Number(today) <= Number(new Date(deliveryDate).getTime()) + 86400000 * 15 &&
    orderStatus?.filter((e) => e.statusId === 57).length === 0
  );
};

export default returnCheck;

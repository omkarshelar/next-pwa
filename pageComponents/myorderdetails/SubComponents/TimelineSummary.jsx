import React, { useState } from "react";
import Accordion from "@material-ui/core/Accordion";
import AccordionSummary from "@material-ui/core/AccordionSummary";
import {
  TimelineDetails,
  TimelineSummaryTileConnector,
  TimelineSummaryTileConnectorSmall,
  TimelineSummaryTileWrap,
  TimelineSummaryWrapper,
} from "./TimelineSummary.style";
import moment from "moment";
import blueChevron from "../../../src/Assets/blueChevron.svg";
import AndroidSeperator from "../../../components/WebComponents/AndroidSeperator/AndroidSeperator";
import timelineTick from "../../../src/Assets/timelineTileTickSummary.svg";
import timelineCancelled from "../../../src/Assets/timelineCancelled.svg";
import returnCheck from "./returnCheck";

const TimelineSummary = (props) => {
  const [accordianOpen, setAccordianOpen] = useState(false);
  let cancelledList = props.orderStatus?.orderTracking?.orderStatus.filter(
    (e) => e.statusId === 57
  );
  return (
    <>
      <TimelineSummaryWrapper>
        <AndroidSeperator showMob={true} />
        <Accordion
          square
          defaultExpanded={accordianOpen}
          onChange={(e) =>
            setAccordianOpen((prevState) => {
              return !prevState;
            })
          }
        >
          <AccordionSummary
            className={"accordianHeaderHeight"}
            expandIcon={
              <img
                src={blueChevron}
                style={{
                  width: "20px",
                  transform: "rotate(180deg)",
                }}
                alt="chevron"
              />
            }
            aria-controls="panel1a-content"
            id="panel1a-header"
            onClick={() => {
              window.scrollTo(0, 999999999);
              // Event_Truemeds
            }}
          >
            <span className="accordianTitle">
              {cancelledList?.length > 0
                ? "Order Cancelled"
                : "Package Delivered"}
            </span>
            {cancelledList?.length > 0 ? (
              <span className="accordianDate">{` On ${moment(
                cancelledList[0].modifiedOn
              ).format("D MMM YYYY")}`}</span>
            ) : props.updateDetail?.deliveryDate ? (
              <span className="accordianDate">{` On ${moment(
                props.updateDetail?.deliveryDate
              ).format("D MMM YYYY")}`}</span>
            ) : null}
          </AccordionSummary>
          <>
            <TimelineDetails>
              <TimelineSummaryTile
                first={true}
                title="Order Placed"
                active={false}
                show={true}
                date={props.updateDetail?.orderDate}
              />
              {props?.orderStatus?.orderTracking?.orderTypeStatus === 52 && (
                <TimelineSummaryTile
                  title="Pharmacist Call"
                  date={
                    props.orderStatus?.orderTracking?.orderStatus?.filter(
                      (e) => e.statusId === 39
                    ).length > 0 &&
                    props.orderStatus?.orderTracking?.orderStatus?.filter(
                      (e) => e.statusId === 39
                    )[0]?.modifiedOn
                  }
                  active={false}
                  show={
                    props.orderStatus?.orderTracking?.orderStatus?.filter(
                      (e) => e.statusId === 39
                    ).length > 0
                  }
                />
              )}
              {props.updateDetail?.WorkflowStatusId === null ? (
                <TimelineSummaryTile
                  title="Doctor Consultation"
                  date={
                    props.orderStatus?.orderTracking?.orderStatus?.filter(
                      (e) => e.statusId === 142
                    ).length > 0 &&
                    props.orderStatus?.orderTracking?.orderStatus?.filter(
                      (e) => e.statusId === 142
                    )[0]?.modifiedOn
                  }
                  show={
                    props.orderStatus?.orderTracking?.orderStatus?.filter(
                      (e) => e.statusId === 142
                    ).length > 0
                  }
                  active={false}
                />
              ) : null}

              <TimelineSummaryTile
                title="Order Processed"
                date={
                  props.orderStatus?.orderTracking?.orderStatus?.filter(
                    (e) => e.statusId === 66
                  ).length > 0 &&
                  props.orderStatus?.orderTracking?.orderStatus?.filter(
                    (e) => e.statusId === 66
                  )[0]?.modifiedOn
                }
                show={
                  props.orderStatus?.orderTracking?.orderStatus?.filter(
                    (e) => e.statusId === 66
                  ).length > 0
                }
                active={false}
              />
              <TimelineSummaryTile
                title="Order Dispatched"
                date={
                  props.orderStatus?.orderTracking?.orderStatus?.filter(
                    (e) => e.statusId === 60
                  ).length > 0 &&
                  props.orderStatus?.orderTracking?.orderStatus?.filter(
                    (e) => e.statusId === 60
                  )[0]?.modifiedOn
                }
                show={
                  props.orderStatus?.orderTracking?.orderStatus?.filter(
                    (e) => e.statusId === 60
                  ).length > 0
                }
                active={false}
              />
              <TimelineSummaryTile
                title="Out For Delivery"
                date={
                  props.orderStatus?.orderTracking?.orderStatus?.filter(
                    (e) => e.statusId === 275
                  ).length > 0 &&
                  props.orderStatus?.orderTracking?.orderStatus?.filter(
                    (e) => e.statusId === 275
                  )[0]?.modifiedOn
                }
                show={
                  props.orderStatus?.orderTracking?.orderStatus?.filter(
                    (e) => e.statusId === 275
                  ).length > 0
                }
                active={false}
              />
              <TimelineSummaryTile
                title="Order Delivered"
                date={props.updateDetail?.deliveryDate}
                active={true}
                show={
                  props.orderStatus?.orderTracking?.orderStatus?.filter(
                    (e) => e.statusId === 55
                  ).length > 0 &&
                  props.orderStatus?.orderTracking?.orderStatus?.filter(
                    (e) => e.statusId === 57
                  ).length === 0
                }
              />
              <TimelineSummaryTile
                title="Order Cancelled"
                date={
                  cancelledList &&
                  cancelledList.length > 0 &&
                  cancelledList[0].modifiedOn
                }
                active={true}
                show={
                  props.orderStatus?.orderTracking?.orderStatus?.filter(
                    (e) => e.statusId === 57
                  ).length > 0
                }
                cancelled={true}
              />
            </TimelineDetails>
            {(returnCheck(
              props.updateDetail?.deliveryDate,
              props.orderStatus?.orderTracking?.orderStatus
            ) &&
              props.updateDetail?.orderStatus?.serialId !== 199) ||
            (props.updateDetail?.orderStatus?.serialId === 57 &&
              props.updateDetail.cancelledReason) ? (
              <AndroidSeperator showWeb={true} showMob={false} />
            ) : null}
          </>
        </Accordion>
        {(props.updateDetail?.orderStatus?.serialId !== 199 &&
          props.updateDetail?.orderStatus?.serialId !== 57) ||
        (props.updateDetail?.orderStatus?.serialId === 57 &&
          props.updateDetail.cancelledReason) ? (
          <AndroidSeperator showMob={true} />
        ) : null}
      </TimelineSummaryWrapper>
    </>
  );
};

export default TimelineSummary;

const TimelineSummaryTile = (props) => {
  if (props.show) {
    return (
      <>
        {!props.first ? (
          props.active ? (
            <TimelineSummaryTileConnectorSmall />
          ) : (
            <TimelineSummaryTileConnector />
          )
        ) : null}
        <TimelineSummaryTileWrap>
          <div className={`leftWrap ${props.active ? "topPad" : ""}`}>
            {props.active ? (
              <div className="tickPoint">
                <img
                  src={props.cancelled ? timelineCancelled : timelineTick}
                  alt="tick"
                />
              </div>
            ) : (
              <div className="connectionPoint"></div>
            )}

            <div className="tileTitle">{props.title}</div>
          </div>
          <div className="dateFormat">
            {props.date ? moment(props.date).format("D MMM, hh:mm A") : "NA"}
          </div>
        </TimelineSummaryTileWrap>
      </>
    );
  } else {
    return <div></div>;
  }
};

import React from "react";
import "./DoctorViewMobCard.module.css";
import doctorMobCard from "../../../src/Assets/doctormobCard.svg";
import chevron_rate_doctor from "../../../src/Assets/chevronMwebRating.svg";

const DoctorViewMobCard = (props) => {
  return (
    <div className={"orderDetails_doctorCardWrap"}>
      <div className={"orderDetails_docTitle"}>Doctor Details</div>
      <div className={"orderDetails_detailsWrap"}>
        <div className={"orderDetails_imgWrap"}>
          <img src={doctorMobCard} alt="doctor display picture" />
        </div>
        <div className={"orderDetails_docDetailsSection"}>
          <div className={"orderDetails_detailsSectionTitleSection"}>
            <div className={"orderDetails_detailsSectionTitle"}>
              Consulted with
            </div>
            <div className={"orderDetails_detailsSectionValue"}>
              {props.doctorData?.doctorName}
            </div>
          </div>
          <div
            className={"orderDetails_ctaSection"}
            onClick={() =>
              window.openSideBar(true, 11, {
                doctorData: props.doctorData,
                orderId: props.orderId,
                customerId: props.customerId,
                ratings: 0,
                serviceCall: props.serviceCall,
              })
            }
          >
            <div className={"orderDetails_ctaTitle"}>View doctor details</div>
            <img alt="arrow" src={chevron_rate_doctor} />
          </div>
        </div>
      </div>
    </div>
  );
};

export default DoctorViewMobCard;

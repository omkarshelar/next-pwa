import React from "react";
import "./StatusCard.module.css";
import statusIconDelivered from "../../../src/Assets/deliveredStatusIcon.svg";
import moment from "moment";
import cancelledStatusIcon from "../../../src/Assets/cancelledStatusIcon.svg";

const StatusCard = (props) => {
  return (
    <div
      className={`${"orderDetails_statusCardWrap"} ${
        props.isCancelled ? "orderDetails_cancelledWrap" : null
      }`}
    >
      <div className={"orderDetails_statusWrapLeft"}>
        <div className={"orderDetails_Icon"}>
          <img
            src={props.isCancelled ? cancelledStatusIcon : statusIconDelivered}
            alt="icon"
          />
        </div>
        <div className={"orderDetails_statusTitle"}>
          {props.isCancelled ? "Cancelled" : "Delivered"}
        </div>
      </div>
      {(props.isCancelled ? props.modifiedOn : props.deliveryDate) ? (
        <div className={"orderDetails_dateWrap"}>{`On ${
          props.isCancelled
            ? props.modifiedOn && moment(props.modifiedOn).format("D MMM YYYY")
            : props.deliveryDate &&
              moment(props.deliveryDate).format("D MMM YYYY")
        }`}</div>
      ) : null}
    </div>
  );
};

export default StatusCard;

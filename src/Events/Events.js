import { analytics, events_Analytics } from "../Firebase/AnalyticsEvents";
// import { singularSdk } from "singular-sdk";
// import { hotjar } from "react-hotjar";

//hotjar page change for SPA
// export let hjPageChange = (url) => {
//   hotjar.stateChange(url);
// };

// homepage events
export let eventHomepageLogo = () => {
  // analytics.logEvent(events_Analytics.Homepage_logo);
  // window.Moengage.track_event(events_Analytics.Homepage_logo);
};

export let eventHomepageLocation = () => {
  // analytics.logEvent(events_Analytics.Homepage_location);
  // window.Moengage.track_event(events_Analytics.Homepage_location);
};

export let eventHomepageLocationLogin = () => {
  // analytics.logEvent(events_Analytics.Homepage_locationlogin);
  // window.Moengage.track_event(events_Analytics.Homepage_locationlogin);
};

export let eventHomepageSearch = () => {
  // analytics.logEvent(events_Analytics.Homepage_search);
  // window.Moengage.track_event(events_Analytics.Homepage_search);
};

export let eventHomepageLogin = () => {
  // analytics.logEvent(events_Analytics.Homepage_login);
  // window.Moengage.track_event(events_Analytics.Homepage_login);
};

export let eventHomepageCart = () => {
  // analytics.logEvent(events_Analytics.Homepage_Cart);
  // window.Moengage.track_event(events_Analytics.Homepage_Cart);
};

export let eventHomepageCallHeader = () => {
  // analytics.logEvent(events_Analytics.Homepage_Call_header);
  // window.Moengage.track_event(events_Analytics.Homepage_Call_header);
};

export let eventHomepageHealthArticles = () => {
  // analytics.logEvent(events_Analytics.Homepage_Healtharticles);
  // window.Moengage.track_event(events_Analytics.Homepage_Healtharticles);
};

export let eventHomepageNeedHelp = () => {
  // analytics.logEvent(events_Analytics.Homepage_needhelp);
  // window.Moengage.track_event(events_Analytics.Homepage_needhelp);
};

export let eventHomepageBanner1 = () => {
  // analytics.logEvent(events_Analytics.Homepage_Banner1);
  // window.Moengage.track_event(events_Analytics.Homepage_Banner1);
};

export let eventHomepageBanner2 = () => {
  // analytics.logEvent(events_Analytics.Homepage_Banner2);
  // window.Moengage.track_event(events_Analytics.Homepage_Banner2);
};

export let eventHomepageBanner3 = () => {
  // analytics.logEvent(events_Analytics.Homepage_Banner3);
  // window.Moengage.track_event(events_Analytics.Homepage_Banner3);
};

export let eventHomepageCrossellingAdded = () => {
  // analytics.logEvent(events_Analytics.Homepage_crossellingadded);
  // window.Moengage.track_event(events_Analytics.Homepage_crossellingadded);
};

export let eventHomepageCrossellingRemoved = () => {
  // analytics.logEvent(events_Analytics.Homepage_crossellingremoved);
  // window.Moengage.track_event(events_Analytics.Homepage_crossellingremoved);
};

export let eventHomepageReadArticles = () => {
  // analytics.logEvent(events_Analytics.Homepage_readarticles);
  // window.Moengage.track_event(events_Analytics.Homepage_readarticles);
};

export let eventHomepageFaq = () => {
  // analytics.logEvent(events_Analytics.Homepage_FAQ);
  // window.Moengage.track_event(events_Analytics.Homepage_FAQ);
};

export let eventHomepageViewMoreFaq = () => {
  // analytics.logEvent(events_Analytics.Homepage_viewmoreFAQ);
  // window.Moengage.track_event(events_Analytics.Homepage_viewmoreFAQ);
};

export let eventHomepageCallIcon = () => {
  // analytics.logEvent(events_Analytics.Homepage_Call_icon);
  // window.Moengage.track_event(events_Analytics.Homepage_Call_icon);
};

export let eventHomepageCallDetails = () => {
  // analytics.logEvent(events_Analytics.Homepage_Call_details);
  // window.Moengage.track_event(events_Analytics.Homepage_Call_details);
};

export let eventHomepageEmail = () => {
  // analytics.logEvent(events_Analytics.Homepage_email);
  // window.Moengage.track_event(events_Analytics.Homepage_email);
};

export let eventHomepageViewDetails = () => {
  // analytics.logEvent(events_Analytics.Homepage_viewdetails);
  // window.Moengage.track_event(events_Analytics.Homepage_viewdetails);
};

export let eventHomepageReorder = () => {
  // analytics.logEvent(events_Analytics.Homepage_reorder);
  // window.Moengage.track_event(events_Analytics.Homepage_reorder);
};

export let eventHomepageMyOrders = () => {
  // analytics.logEvent(events_Analytics.Homepage_myorders);
  // window.Moengage.track_event(events_Analytics.Homepage_myorders);
};

export let eventHomepageTmWallet = () => {
  // analytics.logEvent(events_Analytics.Homepage_TMwallet);
  // window.Moengage.track_event(events_Analytics.Homepage_TMwallet);
};

export let eventHomepageSearchTyping = () => {
  // analytics.logEvent(events_Analytics.Homepage_search_typing);
  // window.Moengage.track_event(events_Analytics.Homepage_search_typing);
};

// pg page events
export let eventPdPageViewed = () => {
  // analytics.logEvent(events_Analytics.PDpage_viewed);
  // window.Moengage.track_event(events_Analytics.PDpage_viewed);
};

export let eventPdPageAddToCart = () => {
  // analytics.logEvent(events_Analytics.PDpage_addtocart);
  // window.fbq("trackCustom", events_Analytics.PDpage_addtocart);
  // window.Moengage.track_event(events_Analytics.PDpage_addtocart);
};

export let eventPdPageViewCart = () => {
  // analytics.logEvent(events_Analytics.PDpage_viewcart);
  // window.Moengage.track_event(events_Analytics.PDpage_viewcart);
};

export let eventPdPageRemove = () => {
  // analytics.logEvent(events_Analytics.PDpage_remove);
  // window.Moengage.track_event(events_Analytics.PDpage_remove);
};

// registration events
export let eventOtpRegistration = () => {
  // analytics.logEvent(events_Analytics.Registration_OTPrequested);
  // window.Moengage.track_event(events_Analytics.Registration_OTPrequested);
};

export let eventCompleteRegistration = (customerId) => {
  // analytics.logEvent(events_Analytics.Registration_successful);
  // window.fbq("trackCustom", events_Analytics.Registration_successful);
  // window.Moengage.track_event(events_Analytics.Registration_successful);
  // window.Moengage.add_unique_user_id(customerId);
  // singularSdk.login(localStorage.getItem("UserPhone"));
};

// login events
export let eventOtpLogin = () => {
  // analytics.logEvent(events_Analytics.Login_OTPrequested);
  // window.Moengage.track_event(events_Analytics.Login_OTPrequested);
};

export let eventCompleteLogin = (customerId) => {
  // analytics.logEvent(events_Analytics.Login_successful);
  // window.Moengage.track_event(events_Analytics.Login_successful);
  // window.Moengage.add_unique_user_id(customerId);
  // singularSdk.login(localStorage.getItem("UserPhone"));
};

//Search Listing
export let eventSearchListingViewed = () => {
  // analytics.logEvent(events_Analytics.Searchlisting_viewed);
  // window.Moengage.track_event(events_Analytics.Searchlisting_viewed);
};

export let eventSearchListingAddToCart = () => {
  // analytics.logEvent(events_Analytics.Searchlisting_addtocart);
  // window.fbq("trackCustom", events_Analytics.Searchlisting_addtocart);
  // // window.Moengage.track_event(
  //   events_Analytics.SearcSearchlisting_addtocarthlisting_viewed
  // );
};

export let eventSearchListingRemove = () => {
  // analytics.logEvent(events_Analytics.Searchlisting_remove);
  // window.Moengage.track_event(events_Analytics.Searchlisting_remove);
};

export let eventSearchListingViewCart = () => {
  // analytics.logEvent(events_Analytics.Searchlisting_viewcart);
  // window.Moengage.track_event(events_Analytics.Searchlisting_viewcart);
};

export let eventSearchListingPrescription = () => {
  // analytics.logEvent(events_Analytics.Searchlisting_prescription);
  // window.Moengage.track_event(events_Analytics.Searchlisting_prescription);
};

export let eventSearchListingPDPage = () => {
  // analytics.logEvent(events_Analytics.Searchlisting_PDpage);
  // window.Moengage.track_event(events_Analytics.Searchlisting_PDpage);
};

export let eventSearchListingNoMedicineFound = () => {
  // analytics.logEvent(events_Analytics.Searchlisting_nomedicinefound);
  // window.Moengage.track_event(events_Analytics.Searchlisting_nomedicinefound);
};

//Cart Events
export let eventCartRemove = () => {
  // analytics.logEvent(events_Analytics.Cart_remove);
  // window.Moengage.track_event(events_Analytics.Cart_remove);
};

export let eventCartAddMedicines = () => {
  // analytics.logEvent(events_Analytics.Cart_addmedicines);
  // window.Moengage.track_event(events_Analytics.Cart_addmedicines);
};

export let eventCartCrossSellingAdded = () => {
  // analytics.logEvent(events_Analytics.Cart_crossellingadded);
  // window.Moengage.track_event(events_Analytics.Cart_crossellingadded);
};

export let eventCartApplyCoupon = () => {
  // analytics.logEvent(events_Analytics.Cart_applycoupon);
  // window.Moengage.track_event(events_Analytics.Cart_applycoupon);
};

export let eventCartProceed = () => {
  // analytics.logEvent(events_Analytics.Cart_proceed);
  // window.fbq("trackCustom", events_Analytics.Cart_proceed);
  // window.Moengage.track_event(events_Analytics.Cart_proceed);
};

//Order prescription
export let eventNoPrescription = () => {
  // analytics.logEvent(events_Analytics.Order_noprescription);
  // window.Moengage.track_event(events_Analytics.Order_noprescription);
};

export let eventOrderPrescription = () => {
  // analytics.logEvent(events_Analytics.Order_prescription);
  // window.Moengage.track_event(events_Analytics.Order_prescription);
};

//Order Summary

export let eventOrderSummaryAddAddress = () => {
  // analytics.logEvent(events_Analytics.Ordersummary_addaddress);
  // window.Moengage.track_event(events_Analytics.Ordersummary_addaddress);
};

export let eventOrderSummaryChange = () => {
  // analytics.logEvent(events_Analytics.Ordersummary_change);
  // window.Moengage.track_event(events_Analytics.Ordersummary_change);
};

export let eventOrderSummaryRemove = () => {
  // analytics.logEvent(events_Analytics.Ordersummary_remove);
  // window.Moengage.track_event(events_Analytics.Ordersummary_remove);
};

export let eventOrderSummaryAddMedicine = () => {
  // analytics.logEvent(events_Analytics.Ordersummary_addmedicine);
  // window.Moengage.track_event(events_Analytics.Ordersummary_addmedicine);
};

export let eventOrderSummaryPrescription = () => {
  // analytics.logEvent(events_Analytics.Ordersummary_prescription);
  // window.Moengage.track_event(events_Analytics.Ordersummary_prescription);
};

export let eventOrderSummaryApplyCoupon = () => {
  // analytics.logEvent(events_Analytics.Ordersummary_applycoupon);
  // window.Moengage.track_event(events_Analytics.Ordersummary_applycoupon);
};

export let eventOrderSubmissionSearch = (amount) => {
  // analytics.logEvent(events_Analytics.Ordersubmission_Search, {
  //   revenue: amount,
  // });
  // window.fbq("trackCustom", events_Analytics.Ordersubmission_Search, {
  //   revenue: amount,
  // });
  // singularSdk.revenue("order_submission", "INR", amount ? amount : 0);
  // window.Moengage.track_event(events_Analytics.order_submission);
};

export let eventOrderSubmissionPrescription = () => {
  // analytics.logEvent(events_Analytics.Ordersubmission_prescription);
  // window.fbq("trackCustom", events_Analytics.Ordersubmission_prescription);
  // window.Moengage.track_event(events_Analytics.Ordersubmission_prescription);
};

// Prescription Page
export let eventPrescriptionUploaded = () => {
  // analytics.logEvent(events_Analytics.Prescription_upload);
  // window.Moengage.track_event(events_Analytics.Prescription_upload);
};
export let eventPrescriptionPast = () => {
  // analytics.logEvent(events_Analytics.Prescription_past);
  // window.Moengage.track_event(events_Analytics.Prescription_past);
};
export let eventPrescriptionProceed = () => {
  // analytics.logEvent(events_Analytics.Prescription_proceed);
  // window.Moengage.track_event(events_Analytics.Prescription_proceed);
};
export let eventPrescriptionRemove = () => {
  // analytics.logEvent(events_Analytics.Prescription_Remove);
  // window.Moengage.track_event(events_Analytics.Prescription_Remove);
};
export let eventPrescriptionCamera = () => {
  // analytics.logEvent(events_Analytics.Prescription_camera);
  // window.Moengage.track_event(events_Analytics.Prescription_camera);
};

//Order STatus

export let eventOrderStatusOrderPharmacistCall = () => {
  // analytics.logEvent(events_Analytics.Order_PharmacistCall);
  // window.Moengage.track_event(events_Analytics.Order_PharmacistCall);
};

export let eventOrderStatusOrderDoctorCall = () => {
  // analytics.logEvent(events_Analytics.Order_DoctorCall);
  // window.Moengage.track_event(events_Analytics.Order_DoctorCall);
};

export let eventOrderStatusOrderProcessing = () => {
  // analytics.logEvent(events_Analytics.Order_Processing);
  // window.Moengage.track_event(events_Analytics.Order_Processing);
};

export let eventOrderStatusOrderDispatched = () => {
  // analytics.logEvent(events_Analytics.Order_Dispatched);
  // window.Moengage.track_event(events_Analytics.Order_Dispatched);
};

export let eventOrderStatusOutForDelivery = () => {
  // analytics.logEvent(events_Analytics.Order_OutforDelivery);
  // window.Moengage.track_event(events_Analytics.Order_OutforDelivery);
};

export let eventOrderStatusOriginalAddAlternateContact = () => {
  // analytics.logEvent(events_Analytics.Orderstatus_addalternatecontact);
  // window.Moengage.track_event(events_Analytics.Orderstatus_addalternatecontact);
};

export let eventOrderStatusOriginalCancelOrder = () => {
  // analytics.logEvent(events_Analytics.Orderstatus_cancelorder);
  // window.Moengage.track_event(events_Analytics.Orderstatus_cancelorder);
};

export let eventOrderStatusOriginalCancelConfirm = () => {
  // analytics.logEvent(events_Analytics.Orderstatus_cancelconfirm);
  // window.Moengage.track_event(events_Analytics.Orderstatus_cancelconfirm);
};

export let eventOrderStatusOriginalDontCancel = () => {
  // analytics.logEvent(events_Analytics.Orderstatus_dontcancel);
  // window.Moengage.track_event(events_Analytics.Orderstatus_dontcancel);
};

export let eventOrderStatusOriginalViewModification = () => {
  // analytics.logEvent(events_Analytics.Orderstatus_viewmodification);
  // window.Moengage.track_event(events_Analytics.Orderstatus_viewmodification);
};

export let eventOrderStatusOriginalViewOriginal = () => {
  // analytics.logEvent(events_Analytics.Orderstatus_vieworiginal);
  // window.Moengage.track_event(events_Analytics.Orderstatus_vieworiginal);
};

export let eventOrderStatusOriginalMedicineCard = () => {
  // analytics.logEvent(events_Analytics.Orderstatus_Originalmedicinecard);
  // window.Moengage.track_event(
  //   events_Analytics.Orderstatus_Originalmedicinecard
  // );
};

export let eventOrderStatusSubsMedicineCard = () => {
  // analytics.logEvent(events_Analytics.Orderstatus_subsmedicinecard);
  // window.Moengage.track_event(events_Analytics.Orderstatus_subsmedicinecard);
};

export let eventOrderStatusRateYourDoctor = () => {
  // analytics.logEvent(events_Analytics.Orderstatus_rateyourdoctor);
  // window.Moengage.track_event(events_Analytics.Orderstatus_rateyourdoctor);
};

export let eventOrderStatusSave = () => {
  // analytics.logEvent(events_Analytics.Orderstatus_save);
  // window.Moengage.track_event(events_Analytics.Orderstatus_save);
};

// my orders events
export let eventMyOrdersViewed = () => {
  // window.Moengage.track_event(events_Analytics.MyOrders_viewed);
};

export let eventMyOrdersViewDetails = () => {
  // window.Moengage.track_event(events_Analytics.MyOrders_viewdetails);
};

export let eventMyOrdersReturn = () => {
  // window.Moengage.track_event(events_Analytics.MyOrders_return);
};

export let eventMyOrdersRate = () => {
  // window.Moengage.track_event(events_Analytics.MyOrders_rate);
};

export let eventMyOrdersViewOriginal = () => {
  // window.Moengage.track_event(events_Analytics.MyOrders_vieworiginal);
};

export let eventMyOrdersSelectPatient = () => {
  // window.Moengage.track_event(events_Analytics.MyOrders_selectpatient);
};

export let eventMyOrdersReorder = () => {
  // window.Moengage.track_event(events_Analytics.MyOrders_reorder);
};

export let eventMyOrdersModificationLog = () => {
  // window.Moengage.track_event(events_Analytics.MyOrders_modificationlog);
};

export let eventMyOrdersTrack = () => {
  // window.Moengage.track_event(events_Analytics.MyOrders_track);
};

import firebase from "./Firebase";
import window from "global";
export let events_Analytics = {
  waiting_order_viewed: "waiting_order_viewed",
  summary_screen: "summary_screen",
  order_cancelled: "order_cancelled",
  medicine_confirmed: "medicine_confirmed",
  order_submission: "order_submission",
  substitute_viewed: "substitute_viewed",
  prescription_uploaded: "prescription_uploaded",
  search_prescription: "search_prescription",
  search_medicines: "search_medicines",
  registration_completion: "registration_completion",
  cart_viewed: "cart_viewed",
  past_prescription_uploaded: "past_prescription_uploaded",
  prescription_viewed: "prescription_viewed",
  privacy_policy_viewed: "privacy_policy_viewed",
  tnc_viewed: "tnc_viewed",
  article_viewed: "article_viewed",
  patient_selected: "patient_selected",
  patient_added: "patient_added",
  patient_deleted: "patient_deleted",
  address_selected: "address_selected",
  address_added: "address_added",
  address_deleted: "address_deleted",
  homepage_call_icon: "homepage_call_icon",
  homepage_call_navbar: "homepage_call_navbar",
  order_discarded: "order_discarded",
  crossselling_added: "crossselling_added",
  crossselling_removed: "crossselling_removed ",
  // home page events - done
  Homepage_logo: "Homepage_logo",
  Homepage_location: "Homepage_location",
  Homepage_locationlogin: "Homepage_locationlogin",
  Homepage_search: "Homepage_search",
  Homepage_login: "Homepage_login",
  Homepage_Cart: "Homepage_Cart",
  Homepage_Call_header: "Homepage_Call_header",
  Homepage_Healtharticles: "Homepage_Healtharticles",
  Homepage_needhelp: "Homepage_needhelp",
  Homepage_Banner1: "Homepage_Banner1",
  Homepage_Banner2: "Homepage_Banner2",
  Homepage_Banner3: "Homepage_Banner3",
  Homepage_crossellingadded: "Homepage_crossellingadded",
  Homepage_crossellingremoved: "Homepage_crossellingremoved",
  Homepage_readarticles: "Homepage_readarticles",
  Homepage_FAQ: "Homepage_FAQ",
  Homepage_viewmoreFAQ: "Homepage_viewmoreFAQ",
  Homepage_Call_icon: "Homepage_Call_icon",
  Homepage_Call_details: "Homepage_Call_details",
  Homepage_email: "Homepage_email",
  Homepage_viewdetails: "Homepage_viewdetails",
  Homepage_reorder: "Homepage_reorder",
  Homepage_myorders: "Homepage_myorders",
  Homepage_TMwallet: "Homepage_TMwallet",
  Homepage_search_typing: "Homepage_search_typing",
  // search listing events
  Searchlisting_viewed: "Searchlisting_viewed",
  Searchlisting_addtocart: "Searchlisting_addtocart",
  Searchlisting_remove: "Searchlisting_remove",
  Searchlisting_viewcart: "Searchlisting_viewcart",
  Searchlisting_prescription: "Searchlisting_prescription",
  Searchlisting_PDpage: "Searchlisting_PDpage",
  Searchlisting_nomedicinefound: "Searchlisting_nomedicinefound",
  // pd page events - done
  PDpage_viewed: "PDpage_viewed",
  PDpage_addtocart: "PDpage_addtocart",
  PDpage_viewcart: "PDpage_viewcart",
  PDpage_remove: "PDpage_remove",
  // cart events
  Cart_remove: "Cart_remove",
  Cart_addmedicines: "Cart_addmedicines",
  Cart_crossellingadded: "Cart_crossellingadded",
  Cart_applycoupon: "Cart_applycoupon",
  Cart_proceed: "Cart_proceed",
  // order prescription events
  Order_noprescription: "Order_noprescription",
  Order_prescription: "Order_prescription",
  // summary events
  Ordersummary_addaddress: "Ordersummary_addaddress",
  Ordersummary_change: "Ordersummary_change",
  Ordersummary_remove: "Ordersummary_remove",
  Ordersummary_addmedicine: "Ordersummary_addmedicine",
  Ordersummary_prescription: "Ordersummary_prescription",
  Ordersummary_remove: "Ordersummary_remove",
  Ordersummary_applycoupon: "Ordersummary_applycoupon",
  Ordersubmission_Search: "Ordersubmission_Search",
  Ordersubmission_prescription: "Ordersubmission_prescription",
  // prescription events
  Prescription_upload: "Prescription_upload",
  Prescription_past: "Prescription_past",
  Prescription_proceed: "Prescription_proceed",
  Prescription_Remove: "Prescription_Remove",
  Prescription_camera: "Prescription_camera",
  // registration events - done
  Registration_OTPrequested: "Registration_OTPrequested",
  Registration_successful: "Registration_successful",
  // login events - done
  Login_OTPrequested: "Login_OTPrequested",
  Login_successful: "Login_successful",
  // order status events
  Order_PharmacistCall: "Order_PharmacistCall",
  Order_DoctorCall: "Order_DoctorCall",
  Order_Processing: "Order_Processing",
  Order_Dispatched: "Order_Dispatched",
  Order_OutforDelivery: "Order_OutforDelivery",
  Orderstatus_addalternatecontact: "Orderstatus_addalternatecontact",
  Orderstatus_cancelorder: "Orderstatus_cancelorder",
  Orderstatus_cancelconfirm: "Orderstatus_cancelconfirm",
  Orderstatus_dontcancel: "Orderstatus_don’tcancel",
  Orderstatus_viewmodification: "Orderstatus_viewmodification",
  Orderstatus_vieworiginal: "Orderstatus_vieworiginal",
  Orderstatus_Originalmedicinecard: "Orderstatus_Originalmedicinecard",
  Orderstatus_subsmedicinecard: "Orderstatus_subsmedicinecard",
  Orderstatus_rateyourdoctor: "Orderstatus_rateyourdoctor",
  Orderstatus_save: "Orderstatus_save",
  // my orders events
  MyOrders_viewed: "MyOrders_viewed",
  MyOrders_viewdetails: "MyOrders_viewdetails",
  MyOrders_return: "MyOrders_return",
  MyOrders_rate: "MyOrders_rate",
  MyOrders_vieworiginal: "MyOrders_vieworiginal",
  MyOrders_selectpatient: "MyOrders_selectpatient",
  MyOrders_reorder: "MyOrders_reorder",
  MyOrders_modificationlog: "MyOrders_modificationlog",
  MyOrders_track: "MyOrders_track",
};
let temp;
if (typeof window !== "undefined") {
  temp = firebase.analytics();
}
export let analytics = temp;

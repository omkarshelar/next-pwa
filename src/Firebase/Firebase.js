import firebase from "firebase/compat/app";
import "firebase/compat/analytics";
import "firebase/compat/messaging";
import window from "global";

let firebaseConfig = {
  apiKey: "AIzaSyA9UUyvk3JJ09tMKamNwpnjhfHgXDz0isw",
  authDomain: "truemeds-cf191.firebaseapp.com",
  databaseURL: "https://truemeds-cf191.firebaseio.com",
  projectId: "truemeds-cf191",
  storageBucket: "truemeds-cf191.appspot.com",
  messagingSenderId: "877511862618",
  appId: "1:877511862618:web:a65b5ddb6ea8e864582b63",
  measurementId: "G-JMJBVM7M9R",
};

let DATA_ONE = {
  DATA_BASE: firebaseConfig,
};

// Initialize Firebase
firebase.initializeApp(DATA_ONE.DATA_BASE);
if (typeof window !== "undefined") {
  firebase.analytics();
}
let messaging;
if (typeof window !== "undefined" && firebase.messaging.isSupported()) {
  messaging = firebase.messaging();
}

export default firebase;

const withCSS = require("@zeit/next-css");
const withPlugins = require("next-compose-plugins");
const withBundleAnalyzer = require("@next/bundle-analyzer")({
  enabled: process.env.ANALYZE === "true",
  openAnalyzer: true,
});
module.exports = withPlugins(
  [
    withCSS({
      webpack: function (config) {
        config.module.rules.push({
          test: /\.(eot|woff|woff2|ttf|svg|png|jpg|gif|webp|mp4)$/,
          use: {
            loader: "url-loader",
            options: {
              limit: 100000,
              name: "[name].[ext]",
            },
          },
        });
        return config;
      },
      images: {
        domains: [
          "assets.truemeds.in",
          "13.126.37.14",
          "tm-storage-bucket-prod.s3.ap-south-1.amazonaws.com",
          "truemedsblog.in",
        ],
      },
    }),
    [withBundleAnalyzer],
  ],
  {
    webpack5: false,
  }
);
